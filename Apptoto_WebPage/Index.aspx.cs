﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataLayer;
using System.Data;
using System.IO;

public partial class Index : System.Web.UI.Page
{
    DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());
    string EventID = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {



        if (Request.QueryString["UID"] != "" && Request.QueryString["UID"] != null)
        {

            try
            {
                GetSigna(Request.QueryString["UID"].ToString());
            }
            catch
            {

            }

            try
            {
                getTemplateField(Request.QueryString["UID"].ToString());
            }
            catch
            {
            }


        }



    }

    public void GetSigna(string EventID)
    {

        DataSet dsSign = new DataSet();
        string s = "exec Sp_GetSigAndLogo_Webpage '" + EventID + "'";

        dsSign = dal.GetData(s);

        if (dsSign != null && dsSign.Tables != null && dsSign.Tables[0].Rows.Count > 0)
        {

            try
            {
                Base64ToImage(dsSign.Tables[0].Rows[0]["CompanyLogo"].ToString());
            }
            catch
            { }

            imgCompanyLogo.ImageUrl = "~/Company.jpg";



        }

    }

    public void Base64ToImage(string base64)
    {


        byte[] imageBytes = Convert.FromBase64String(base64);
        MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
        ms.Write(imageBytes, 0, imageBytes.Length);
        System.Drawing.Image CompanyImage = System.Drawing.Image.FromStream(ms, true);

        string FilePath = string.Empty;

        try
        {
            FilePath = Server.MapPath("~/") + "Company.jpg";
            CompanyImage.Save(FilePath);
        }
        catch
        { }

    }

    private void getTemplateField(string EventID)
    {
        DataSet dsTemplateField = new DataSet();
        string s = "EXEC Sp_GetWebPageDetails '" + EventID + "'";
        dsTemplateField = dal.GetData(s);

        string absoluteURL = HttpContext.Current.Request.Url.AbsoluteUri;
        Uri myUri = new Uri(absoluteURL);
        string languageParam = HttpUtility.ParseQueryString(myUri.Query).Get("lng");
        if (dsTemplateField != null && dsTemplateField.Tables[0].Rows.Count > 0)
        {
            string strIsConfirmable = string.Empty;

            lblEventTitle.Text = dsTemplateField.Tables[0].Rows[0]["EventTitle"].ToString();
            lblEventDateTime.Text = dsTemplateField.Tables[0].Rows[0]["EventDate"].ToString();
            lblEventMobile.Text = dsTemplateField.Tables[0].Rows[0]["Phone"].ToString();
            hdnEventNo.Value = dsTemplateField.Tables[0].Rows[0]["EventID"].ToString();
            strIsConfirmable = dsTemplateField.Tables[0].Rows[0]["IsConfirmable"].ToString();

            //if (strIsConfirmable == "1")
            //{

            //    rdoStatus.Items.Add(new ListItem("Confirm", "1"));
            //    rdoStatus.Items.Add(new ListItem("Request a Call back", "4"));


            //}
            //else
            //{
            //    rdoStatus.Items.Add(new ListItem("Confirm", "1"));
            //    rdoStatus.Items.Add(new ListItem("Cancel", "2"));
            //    rdoStatus.Items.Add(new ListItem("Reschedule", "3"));
            //    rdoStatus.Items.Add(new ListItem("Request a Call back", "4"));
            //}
            if (languageParam == "9")
            {
                if (strIsConfirmable == "1")
                {
                    rdoStatus.Items.Add(new ListItem("confirmar", "1"));
                    rdoStatus.Items.Add(new ListItem("solicitar una devolución de llamada", "4"));
                }
                else
                {
                    rdoStatus.Items.Add(new ListItem("confirmar", "1"));
                    rdoStatus.Items.Add(new ListItem("cancelar", "2"));
                    rdoStatus.Items.Add(new ListItem("reprogramar", "3"));
                    rdoStatus.Items.Add(new ListItem("solicitar una devolución de llamada", "4"));
                }
            }
            else
            {
                if (strIsConfirmable == "1")
                {
                    rdoStatus.Items.Add(new ListItem("Confirm", "1"));
                    rdoStatus.Items.Add(new ListItem("Request a Call back", "4"));
                }
                else
                {
                    rdoStatus.Items.Add(new ListItem("Confirm", "1"));
                    rdoStatus.Items.Add(new ListItem("Cancel", "2"));
                    rdoStatus.Items.Add(new ListItem("Reschedule", "3"));
                    rdoStatus.Items.Add(new ListItem("Request a Call back", "4"));
                }
            }

        }

        else
        {

        }

    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        string absoluteURL = HttpContext.Current.Request.Url.AbsoluteUri;
        Uri myUri = new Uri(absoluteURL);
        string languageParam = HttpUtility.ParseQueryString(myUri.Query).Get("lng");

        DataSet dsStatusUpdate = new DataSet();
        string s = "EXEC SP_TwilioStatus_New '" + hdnEventNo.Value + "','" + rdoStatus.SelectedValue + "'";
        dsStatusUpdate = dal.GetData(s);
        if (dsStatusUpdate.Tables[0].Rows[0]["Result"].ToString() == "1")
        {
            //if (rdoStatus.SelectedValue == "1")
            //{
            //    Response.Redirect("Index.html");
            //}
            
            //if (rdoStatus.SelectedValue == "3")
            //{
            //    Response.Redirect("calender.html");
            //}

            hdnEventNo.Value = null;
            dsStatusUpdate = null;
            Response.Redirect("Thankyou.aspx?lng=" + languageParam);
        }
        else
        {

        }

    }

    protected void btnsubmitSpanish_Click(object sender, EventArgs e)
    {
        string absoluteURL = HttpContext.Current.Request.Url.AbsoluteUri;
        Uri myUri = new Uri(absoluteURL);
        string languageParam = HttpUtility.ParseQueryString(myUri.Query).Get("lng");

        DataSet dsStatusUpdate = new DataSet();
        string s = "EXEC SP_TwilioStatus_New '" + hdnEventNo.Value + "','" + rdoStatus.SelectedValue + "'";
        dsStatusUpdate = dal.GetData(s);
        if (dsStatusUpdate.Tables[0].Rows[0]["Result"].ToString() == "1")
        {
            //if (rdoStatus.SelectedValue == "1")
            //{
            //    Response.Redirect("Index.html");
            //}

            //if (rdoStatus.SelectedValue == "3")
            //{
            //    Response.Redirect("calender.html");
            //}

            hdnEventNo.Value = null;
            dsStatusUpdate = null;
            Response.Redirect("Thankyou.aspx?lng=" + languageParam);
        }
        else
        {

        }

    }
}