﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>SNS - Synergy Notification System</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <script src="bootstrap.js"></script>
    <link rel="stylesheet" type="text/css" href="main.css">

    <!-- Website Font style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

    <link href="http://addtocalendar.com/atc/1.5/atc-style-blue.css" rel="stylesheet" type="text/css">

    <style type="text/css">
        .rbl input[type="radio"] {
            margin-left: 10px;
            margin-right: 8px;
        }
    </style>
    <script type="text/javascript">
        function reloadPage() {

            window.location.reload()
        }
    </script>


</head>
<body>
    <script type="text/javascript">(function () {
    if (window.addtocalendar) if (typeof window.addtocalendar.start == "function") return;
    if (window.ifaddtocalendar == undefined) {
        window.ifaddtocalendar = 1;
        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
        s.type = 'text/javascript'; s.charset = 'UTF-8'; s.async = true;
        s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://addtocalendar.com/atc/1.5/atc.min.js';
        var h = d[g]('body')[0]; h.appendChild(s);
    }
})();
    </script>

    <div id="google_translate_element"></div>
    <script type="text/javascript">
        function googleTranslateElementInit() {
            new google.translate.TranslateElement({ pageLanguage: 'en', includedLanguages: 'en,es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE }, 'google_translate_element');
        }
    </script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <form id="form1" runat="server">
        <div class="container">
            <%
        string absoluteURL = HttpContext.Current.Request.Url.AbsoluteUri;
        Uri myUri = new Uri(absoluteURL);
        string languageParam = HttpUtility.ParseQueryString(myUri.Query).Get("lng");
        string mainHeader = "Your Appointment reminder";
        string eventTitle = "Title";
        string eventWhen = "When";
        string eventInfo = "Info";
        string eventRadioTitle = "Are you planning to make it?";
        string submitBtn = "Submit";
         if (languageParam == "9")
               {
                   mainHeader = "Su recordatorio de cita";
                   eventTitle = "Título";
                   eventWhen = "cuándo";
                   eventInfo = "información";
                   eventRadioTitle = "¿Estás planeando hacerlo?";
                   submitBtn = "enviar";
            }
        %>
            <div class="row main">
                <div class="panel-heading">
                    <div class="panel-title text-center">

                        <asp:Image ID="imgCompanyLogo" runat="server" class="img-fluid" Style="height: 80px" />

                    </div>
                </div>
                <div class="panel-title text-center">
                    <hr />
                    <h5 class="title"><%=mainHeader %></h5>
                    <hr />
                </div>

                <div class="col-md-6 col-md-offset-3 main-login main-center">
                    <div class="row">


                        <div class="row form-group">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="name" class="control-label"><%=eventTitle %>: </label>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <div class="input-group">

                                    <asp:Label ID="lblEventTitle" runat="server" Text=""></asp:Label>
                                </div>

                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="name" class="control-label"><%=eventWhen %>: </label>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <div class="input-group">
                                    <asp:Label ID="lblEventDateTime" runat="server" Text=""></asp:Label>
                                </div>

                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <label for="name" class="control-label"><%=eventInfo %>: </label>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-9">
                                <div class="input-group">
                                    <asp:Label ID="lblEventMobile" runat="server" Text=""></asp:Label>
                                </div>

                            </div>
                        </div>





                        <div class="form-group">
                            <label for="username" class="cols-sm-4 control-label"><%=eventRadioTitle %></label>
                            <div class="cols-sm-12 ">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <asp:RadioButtonList ID="rdoStatus" CssClass="rbl" runat="server" RepeatDirection="Vertical" Width="300px">
                                    </asp:RadioButtonList>
                                </div>

                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-xs-12">
                                    <asp:HiddenField runat="server" ID="hdnEventNo" Visible="false"></asp:HiddenField>
                                    <% if(languageParam == "9") { %>
                                    <asp:Button ID="btnsubmitSpanish" class="btn btn-primary btn-md btn-block login-button" runat="server" Text="enviar" OnClick="btnsubmit_Click" />
                                    <% } else { %>
                                    <asp:Button ID="btnsubmit" class="btn btn-primary btn-md btn-block login-button" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                                    <% } %>
                                    <%--<asp:Label ID="btnsubmit" class="btn btn-primary btn-md btn-block login-button" runat="server"  OnClick="btnsubmit_Click" ><%=submitBtn %></asp:Label>--%>
                                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" style="display:none;" data-target="#myModal">Open Modal</button>
                                </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    </form>
</body>
</html>

