﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Thankyou.aspx.cs" Inherits="Thankyou" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RS - Thank You</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="main.css">

    <!-- Website Font style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

    <style type="text/css">
        .rbl input[type="radio"] {
            margin-left: 10px;
            margin-right: 8px;
        }
    </style>

</head>
<body>
    <%
        
        //string ThankYouText = "Your Appointment reminder";
        
        // if (languageParam == "9")
        //       {
        //           ThankYouText = "Gracias por su respuesta!";
        //    }
        %>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row main">
                <div class="panel-heading">
                    <div class="panel-title text-center">
                        <asp:Image ID="imgCompanyLogo" runat="server" ImageUrl="~/Company.jpg" class="img-fluid" Style="height: 80px" />
                    </div>
                </div>
                <div class="panel-title text-center">
                    <hr />
                    <% 
                        string absoluteURL = HttpContext.Current.Request.Url.AbsoluteUri;
                        Uri myUri = new Uri(absoluteURL);
                        string languageParam = HttpUtility.ParseQueryString(myUri.Query).Get("lng");
                        
                        if(languageParam =="9") { %>
                    <h5 class="title">Gracias por su respuesta!</h5>
                    <% } else  { %>
                    <h5 class="title">Your Appointment reminder</h5>
                    <% } %>
                    <hr />
                </div>
            </div>
        </div>
    </form>
</body>
</html>
