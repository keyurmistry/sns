USE [master]
GO
/****** Object:  Database [LRS_Gblaw]    Script Date: 01/24/2018 10:54:45 ******/
CREATE DATABASE [LRS_Gblaw] ON  PRIMARY 
( NAME = N'LRS_Gblaw', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\LRS_Gblaw.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LRS_Gblaw_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\LRS_Gblaw_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LRS_Gblaw] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LRS_Gblaw].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LRS_Gblaw] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [LRS_Gblaw] SET ANSI_NULLS OFF
GO
ALTER DATABASE [LRS_Gblaw] SET ANSI_PADDING OFF
GO
ALTER DATABASE [LRS_Gblaw] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [LRS_Gblaw] SET ARITHABORT OFF
GO
ALTER DATABASE [LRS_Gblaw] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [LRS_Gblaw] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [LRS_Gblaw] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [LRS_Gblaw] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [LRS_Gblaw] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [LRS_Gblaw] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [LRS_Gblaw] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [LRS_Gblaw] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [LRS_Gblaw] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [LRS_Gblaw] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [LRS_Gblaw] SET  DISABLE_BROKER
GO
ALTER DATABASE [LRS_Gblaw] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [LRS_Gblaw] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [LRS_Gblaw] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [LRS_Gblaw] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [LRS_Gblaw] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [LRS_Gblaw] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [LRS_Gblaw] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [LRS_Gblaw] SET  READ_WRITE
GO
ALTER DATABASE [LRS_Gblaw] SET RECOVERY SIMPLE
GO
ALTER DATABASE [LRS_Gblaw] SET  MULTI_USER
GO
ALTER DATABASE [LRS_Gblaw] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [LRS_Gblaw] SET DB_CHAINING OFF
GO
USE [LRS_Gblaw]
GO
/****** Object:  UserDefinedFunction [dbo].[SplitBySpace]    Script Date: 01/24/2018 10:54:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitBySpace](@String varchar(8000), @Delimiter char(1))       
    returns @temptable TABLE (NewColumn varchar(8000))       
    as       
    begin       
        declare @idx int       
        declare @slice varchar(8000)       
          
        select @idx = 1       
            if DATALENGTH(@String)<1 or @String is null  return       
          
        while @idx!= 0       
        begin       
            set @idx = charindex(@Delimiter,@String)       
            if @idx!=0       
                set @slice = left(@String,@idx - 1)       
            else       
                set @slice = @String       
              
            if(DATALENGTH(@slice)>0)  
                insert into @temptable(NewColumn) values(@slice)       
      
            set @String = right(@String,DATALENGTH(@String) - @idx)       
            if DATALENGTH(@String) = 0 break       
        end   
    return       
    end
GO
/****** Object:  UserDefinedFunction [dbo].[SplitByJEM]    Script Date: 01/24/2018 10:54:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[SplitByJEM] (@CommadelimitedString   varchar(MAX))
RETURNS   @Result TABLE (Column1   VARCHAR(MAX))
AS
BEGIN
        DECLARE @IntLocation INT
        WHILE (CHARINDEX(':',    @CommadelimitedString, 0) > 0)
        BEGIN
              SET @IntLocation =   CHARINDEX(':',    @CommadelimitedString, 0)      
              INSERT INTO   @Result (Column1)
              --LTRIM and RTRIM to ensure blank spaces are   removed
              SELECT RTRIM(LTRIM(SUBSTRING(@CommadelimitedString,   0, @IntLocation)))   
              SET @CommadelimitedString = STUFF(@CommadelimitedString,   1, @IntLocation,   '') 
        END
        INSERT INTO   @Result (Column1)
        SELECT  RTRIM(LTRIM(@CommadelimitedString))--LTRIM and RTRIM to ensure blank spaces are removed
        RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[SplitByComma]    Script Date: 01/24/2018 10:54:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create FUNCTION [dbo].[SplitByComma] (@CommadelimitedString   varchar(MAX))
RETURNS   @Result TABLE (Column1   VARCHAR(MAX))
AS
BEGIN
        DECLARE @IntLocation INT
        WHILE (CHARINDEX(',',    @CommadelimitedString, 0) > 0)
        BEGIN
              SET @IntLocation =   CHARINDEX(',',    @CommadelimitedString, 0)      
              INSERT INTO   @Result (Column1)
              --LTRIM and RTRIM to ensure blank spaces are   removed
              SELECT RTRIM(LTRIM(SUBSTRING(@CommadelimitedString,   0, @IntLocation)))   
              SET @CommadelimitedString = STUFF(@CommadelimitedString,   1, @IntLocation,   '') 
        END
        INSERT INTO   @Result (Column1)
        SELECT  RTRIM(LTRIM(@CommadelimitedString))--LTRIM and RTRIM to ensure blank spaces are removed
        RETURN 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetEvent_AddressBookContact]    Script Date: 01/24/2018 10:54:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec SP_GetEvent_AddressBookContact 'tony Clark',0        
CREATE Proc [dbo].[SP_GetEvent_AddressBookContact]                 
@SearchFilter Nvarchar(500) = NUll,        
@AddId int =0               
                
As                        
                        
begin                  
                
DECLARE @qry as VARCHAR(MAX)        
                
If(@AddId <= 0)   
Begin     
        
SET @qry ='         
          
select Convert(Nvarchar(50),CCa.caseno) as [Case No],Convert(Nvarchar(50),C.firmcode) as firmcode,CCa.[Type] as [Type],                                           
ISNULL(LTRIM(RTRIM(c.salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''') AS [Name],                                                        
email as Email,                          
case when    ( case  when isnull(LTRIM(RTRIM(C.car)),'''')= '''' THEN isnull(LTRIM(RTRIM(C.home)),'''')       
else isnull(LTRIM(RTRIM(C.car)),'''') end)='''' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'''')= ''''      
then isnull(LTRIM(RTRIM(C.business)),'''') else '''' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'''')= '''' THEN isnull(LTRIM(RTRIM(C.home)),'''')       
else isnull(LTRIM(RTRIM(C.car)),'''') end) end as Phone,      
              
ISNULL(LTRIM(RTRIM(c.first)),'''')as FirstName,ISNULL(LTRIM(RTRIM(c.last)),'''') as LastName, C.salutation,                                           
ISNULL(LTRIM(RTRIM(c.middle)),'''')  as MiddleName ,birth_date as DOB,C.comments as Note,                           
CC.Zip as Zipcode,CC.CountryCode,MobileType,EmailType,Convert(Nvarchar(50),CCa.cardcode) AS cardcode                          
                                                 
from [card] C                                                       
Inner Join casecard CCa On CCa.cardcode = C.cardcode                             
Inner join card2 CC On CC.firmcode =C.firmcode                  
                  
where                                                         
(                           
                        
(CCa.caseno like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'' )                                                          
OR                                  
(C.first like '''+ LTRIM(RTRIM(@SearchFilter)) +'%'' )                                                          
OR                                  
(C.last like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'' )                                                          
OR                                                     
(C.Email like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'' )                        
                        
                        
 OR                             
(ISNULL(LTRIM(RTRIM(c.first)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''') like  '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                           
OR                           
(ISNULL(LTRIM(RTRIM(c.last)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''') like  '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                               
OR                           
(ISNULL(LTRIM(RTRIM(C.salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''') like   '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                          
                         
OR                           
(ISNULL(LTRIM(RTRIM(C.salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                             
                        
                        
OR                           
(ISNULL(LTRIM(RTRIM(replace(C.salutation,''.'',''''))),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                                 
OR                           
(ISNULL(LTRIM(RTRIM(replace(C.salutation,''.'',''''))),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                    
                        
                    
) '    
  SET @qry +='    
    
UNION ALL                  
                  
SELECT RC.[Case],RC.Firmcode,''RS'',                  
ISNULL(LTRIM(RTRIM(salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(FName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(LName)),'''') AS [Name],                  
Email,PhoneNo,FName,LName,Salutation,MName,DOB,Notes,Zipcode,CountryCode,MobileType,EmailType,RC.CardCode                   
from tbl_RS_Contactlist RC                   
                    
 where                                   
isnull(RC.Isactive,0)<>1   and                     
                        
(                                
(RC.FName like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'')                                         
OR                                  
(RC.LName like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'')                                                          
OR                                                     
(RC.Email like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'')     
 OR                             
(ISNULL(LTRIM(RTRIM(RC.FName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''') like    '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                           
OR                           
(ISNULL(LTRIM(RTRIM(RC.LName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''') like   '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                               
OR                           
(ISNULL(LTRIM(RTRIM(RC.Salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''') like    '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                          
                         
OR                           
(ISNULL(LTRIM(RTRIM(RC.Salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                             
                        
                        
OR                           
(ISNULL(LTRIM(RTRIM(replace(RC.Salutation,''.'',''''))),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''') like      '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                                    
OR                           
(ISNULL(LTRIM(RTRIM(replace(RC.Salutation,''.'',''''))),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                                  
                    
)                       
     
    
'                  
          
      
 print(@qry)        
 EXEC (@qry)            
END     
ELSE  
BEGIN  
  
SET @qry ='         
          
select Convert(Nvarchar(50),CCa.caseno) as [Case No],Convert(Nvarchar(50),C.firmcode) as firmcode,CCa.[Type] as [Type],                                           
ISNULL(LTRIM(RTRIM(c.salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''') AS [Name],                                                        
email as Email,                          
case when    ( case  when isnull(LTRIM(RTRIM(C.car)),'''')= '''' THEN isnull(LTRIM(RTRIM(C.home)),'''')       
else isnull(LTRIM(RTRIM(C.car)),'''') end)='''' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'''')= ''''      
then isnull(LTRIM(RTRIM(C.business)),'''') else '''' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'''')= '''' THEN isnull(LTRIM(RTRIM(C.home)),'''')       
else isnull(LTRIM(RTRIM(C.car)),'''') end) end as Phone,      
              
ISNULL(LTRIM(RTRIM(c.first)),'''')as FirstName,ISNULL(LTRIM(RTRIM(c.last)),'''') as LastName, C.salutation,                                           
ISNULL(LTRIM(RTRIM(c.middle)),'''')  as MiddleName ,birth_date as DOB,C.comments as Note,                           
CC.Zip as Zipcode,CC.CountryCode,MobileType,EmailType,Convert(Nvarchar(50),CCa.cardcode) AS cardcode                          
                                                 
from [card] C                                                       
Inner Join casecard CCa On CCa.cardcode = C.cardcode                             
Inner join card2 CC On CC.firmcode =C.firmcode                  
inner join tbl_LRS_AddressBookDetails LA on Convert(nvarchar(50),LA.CardCode) = Convert(nvarchar(50),C.cardcode)                         
where   
LA.AddId=  '''+ LTRIM(RTRIM(convert(Nvarchar(50),@AddId))) +'''     and                                                
(                           
                        
(CCa.caseno like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'' )                                                          
OR                                  
(C.first like '''+ LTRIM(RTRIM(@SearchFilter)) +'%'' )                                                          
OR                                  
(C.last like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'' )                                                          
OR                                                     
(C.Email like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'' )                        
                        
                        
 OR                             
(ISNULL(LTRIM(RTRIM(c.first)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''') like  '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                           
OR                           
(ISNULL(LTRIM(RTRIM(c.last)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''') like  '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                               
OR                           
(ISNULL(LTRIM(RTRIM(C.salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''') like   '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                          
                         
OR                           
(ISNULL(LTRIM(RTRIM(C.salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                             
                        
                        
OR                           
(ISNULL(LTRIM(RTRIM(replace(C.salutation,''.'',''''))),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                                 
OR                           
(ISNULL(LTRIM(RTRIM(replace(C.salutation,''.'',''''))),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.last)),'''')+'' ''+ISNULL(LTRIM(RTRIM(c.first)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                    
                        
                    
) '    
  SET @qry +='    
    
UNION ALL                  
                  
SELECT RC.[Case],RC.Firmcode,''RS'',                  
ISNULL(LTRIM(RTRIM(salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(FName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(LName)),'''') AS [Name],                  
Email,PhoneNo,FName,LName,Salutation,MName,DOB,Notes,Zipcode,CountryCode,MobileType,EmailType,RC.CardCode                   
from tbl_RS_Contactlist RC                   
  
inner join tbl_LRS_AddressBookDetails LA on Convert(nvarchar(50),LA.CardCode) = Convert(nvarchar(50),RC.CardCode)   
                    
 where                                   
                   
isnull(RC.Isactive,0)<>1   and   LA.AddId=  '''+ LTRIM(RTRIM(convert(Nvarchar(50),@AddId))) +'''   and                    
(                                
(RC.FName like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'')                                         
OR                                  
(RC.LName like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'')                                                          
OR                                                     
(RC.Email like  '''+ LTRIM(RTRIM(@SearchFilter)) +'%'')     
 OR                             
(ISNULL(LTRIM(RTRIM(RC.FName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''') like    '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                           
OR                           
(ISNULL(LTRIM(RTRIM(RC.LName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''') like   '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                               
OR                           
(ISNULL(LTRIM(RTRIM(RC.Salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''') like    '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                          
                         
OR                           
(ISNULL(LTRIM(RTRIM(RC.Salutation)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                             
                        
                        
OR                           
(ISNULL(LTRIM(RTRIM(replace(RC.Salutation,''.'',''''))),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''') like      '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                                    
OR                           
(ISNULL(LTRIM(RTRIM(replace(RC.Salutation,''.'',''''))),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.LName)),'''')+'' ''+ISNULL(LTRIM(RTRIM(RC.FName)),'''') like     '''+ LTRIM(RTRIM(REPLACE(@SearchFilter,''',''',''''))) +'%'')                                  
                    
)                       
    
    
    
'                  
          
      
 print(@qry)        
 EXEC (@qry)   
  
END              
                  
End
GO
/****** Object:  UserDefinedTableType [dbo].[TwilioStatusUpdateBulk]    Script Date: 01/24/2018 10:54:47 ******/
CREATE TYPE [dbo].[TwilioStatusUpdateBulk] AS TABLE(
	[ClientKey] [nvarchar](50) NULL,
	[MainEventID] [nvarchar](50) NULL,
	[EventID] [nvarchar](50) NULL,
	[IsTwilioStatus] [int] NULL,
	[SID] [nvarchar](50) NULL,
	[ConfirmDatetime] [nvarchar](100) NULL,
	[Confirmtime] [nvarchar](100) NULL
)
GO
/****** Object:  Table [dbo].[testnew]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[testnew](
	[Phone] [float] NULL,
	[EventDatetime] [datetime] NULL,
	[IsTwilioStatus] [float] NULL,
	[MainEventID] [float] NULL,
	[Event] [nvarchar](255) NULL,
	[ReminderType] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[UrlEncode]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.UrlEncode('Hello Chintan, This is Autometed reminder Call.')


CREATE FUNCTION [dbo].[UrlEncode](@url NVARCHAR(MAX))
RETURNS NVARCHAR(MAX)
AS
BEGIN
    DECLARE @count INT, @c NCHAR(1), @i INT, @urlReturn NVARCHAR(3072)
    SET @count = LEN(@url)
    SET @i = 1
    SET @urlReturn = ''    
    WHILE (@i <= @count)
     BEGIN
        SET @c = SUBSTRING(@url, @i, 1)
        IF @c LIKE N'[A-Za-z0-9()''*\-._!~]' COLLATE Latin1_General_BIN ESCAPE N'\' COLLATE Latin1_General_BIN
         BEGIN
            SET @urlReturn = @urlReturn + @c
         END
        ELSE
         BEGIN
            SET @urlReturn = 
                   @urlReturn + '%'
                   + SUBSTRING(sys.fn_varbintohexstr(CAST(@c AS VARBINARY(MAX))),3,2)
                   + ISNULL(NULLIF(SUBSTRING(sys.fn_varbintohexstr(CAST(@c AS VARBINARY(MAX))),5,2), '00'), '')
         END
        SET @i = @i +1
     END
    RETURN @urlReturn
END
GO
/****** Object:  UserDefinedFunction [dbo].[RS_GetClientID]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.[RS_GetClientID] ()


CREATE FUNCTION [dbo].[RS_GetClientID] (@Id  int)
RETURNS VARCHAR(300)
AS 
BEGIN
 
Declare @NewClientNo varchar(20)
    
                            
                             
                  
   declare @gd datetime                  
   set @gd =GETDATE()                 
                 
   set @NewClientNo ='RS' + convert(varchar,right(Year(@gd),2))         
   + Convert(varchar,month(@gd),2) +convert(varchar,@Id)+Convert(varchar,DATEPART(dd,Getdate()))
              
                             
     


return @NewClientNo 
END
GO
/****** Object:  Table [dbo].[casecard]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[casecard](
	[caseno] [bigint] NULL,
	[cardcode] [bigint] NULL,
	[orderno] [bigint] NULL,
	[officeno] [char](25) NULL,
	[side] [char](15) NULL,
	[type] [char](25) NULL,
	[flags] [bigint] NULL,
	[notes] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[case]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[case](
	[caseno] [bigint] NOT NULL,
	[casetype] [char](10) NULL,
	[casestat] [char](75) NULL,
	[yourfileno] [char](15) NULL,
	[venue] [bigint] NULL,
	[location] [char](15) NULL,
	[atty_resp] [char](3) NULL,
	[atty_hand] [char](3) NULL,
	[para_hand] [char](3) NULL,
	[sec_hand] [char](3) NULL,
	[dateenter] [datetime] NULL,
	[dateopen] [datetime] NULL,
	[dateclosed] [datetime] NULL,
	[d_r] [datetime] NULL,
	[rb] [bigint] NULL,
	[followup] [datetime] NULL,
	[ps] [char](1) NULL,
	[psdate] [datetime] NULL,
	[protected] [bit] NULL,
	[photototal] [char](1) NULL,
	[relatotal] [char](1) NULL,
	[datepdf1] [datetime] NULL,
	[caption1] [text] NULL,
	[udfall] [text] NULL,
	[udfall2] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[card2]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[card2](
	[firmcode] [bigint] NOT NULL,
	[firm] [text] NULL,
	[venue] [char](15) NULL,
	[tax_id] [char](15) NULL,
	[address1] [text] NULL,
	[address2] [text] NULL,
	[city] [char](30) NULL,
	[state] [char](5) NULL,
	[zip] [char](10) NULL,
	[phone1] [char](25) NULL,
	[phone2] [char](25) NULL,
	[fax] [char](15) NULL,
	[fax2] [char](15) NULL,
	[firmkey] [char](10) NULL,
	[lastchg] [char](3) NULL,
	[origchg] [char](3) NULL,
	[origdt] [datetime] NULL,
	[lastdt] [datetime] NULL,
	[comments] [text] NULL,
	[color] [decimal](18, 0) NULL,
	[eamsref] [bigint] NULL,
	[mailing1] [text] NULL,
	[mailing2] [text] NULL,
	[mailing3] [text] NULL,
	[mailing4] [text] NULL,
	[spinst1] [text] NULL,
	[CountryCode] [nvarchar](10) NULL,
	[MobileType] [nvarchar](50) NULL,
	[EmailType] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[card]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[card](
	[cardcode] [bigint] NOT NULL,
	[firmcode] [nvarchar](max) NULL,
	[letsal] [text] NULL,
	[salutation] [char](5) NULL,
	[first] [char](30) NULL,
	[middle] [char](10) NULL,
	[last] [char](40) NULL,
	[suffix] [char](8) NULL,
	[social_sec] [char](15) NULL,
	[type] [char](25) NULL,
	[title] [char](30) NULL,
	[home] [char](20) NULL,
	[business] [char](30) NULL,
	[fax] [char](20) NULL,
	[car] [char](30) NULL,
	[beeper] [char](20) NULL,
	[email] [char](50) NULL,
	[birth_date] [datetime] NULL,
	[interpret] [char](1) NULL,
	[language] [char](10) NULL,
	[licenseno] [char](15) NULL,
	[specialty] [char](40) NULL,
	[mothermaid] [char](15) NULL,
	[protected] [bit] NULL,
	[lastchg] [char](3) NULL,
	[origchg] [char](3) NULL,
	[origdt] [datetime] NULL,
	[lastdt] [datetime] NULL,
	[comments] [text] NULL,
	[fld1] [text] NULL,
	[fld2] [text] NULL,
	[fld3] [text] NULL,
	[fld4] [text] NULL,
	[fld5] [text] NULL,
	[fld6] [text] NULL,
	[fld7] [text] NULL,
	[fld8] [text] NULL,
	[fld9] [text] NULL,
	[fld10] [text] NULL,
	[fld11] [text] NULL,
	[fld12] [text] NULL,
	[fld13] [text] NULL,
	[fld14] [text] NULL,
	[fld15] [text] NULL,
	[fld16] [text] NULL,
	[fld17] [text] NULL,
	[fld18] [text] NULL,
	[fld19] [text] NULL,
	[fld20] [text] NULL,
	[spinst1] [text] NULL,
	[ContactType] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[calaudit1]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[calaudit1](
	[eventno] [bigint] NULL,
	[macroid] [bigint] NULL,
	[caseno] [bigint] NULL,
	[calendar] [bigint] NULL,
	[initials0] [char](3) NULL,
	[initials] [char](3) NULL,
	[calstat] [char](15) NULL,
	[date] [datetime] NULL,
	[tickle] [char](1) NULL,
	[tickledate] [datetime] NULL,
	[whofrom] [char](3) NULL,
	[whoto] [char](3) NULL,
	[tottime] [bigint] NULL,
	[event] [char](60) NULL,
	[first] [char](25) NULL,
	[last] [char](25) NULL,
	[defendant] [char](40) NULL,
	[venue] [char](15) NULL,
	[judge] [char](15) NULL,
	[attyass] [char](11) NULL,
	[location] [text] NULL,
	[notes] [text] NULL,
	[todo] [bit] NULL,
	[protected] [bit] NULL,
	[color] [decimal](18, 0) NULL,
	[rollover] [bit] NULL,
	[chglast] [datetime] NULL,
	[chgwho] [char](3) NULL,
	[reminder1] [bit] NULL,
	[doctor1] [text] NULL,
	[other1] [text] NULL,
	[email] [char](50) NULL,
	[phonehome] [char](20) NULL,
	[phonebus] [char](30) NULL,
	[phonefax] [char](20) NULL,
	[phonecar] [char](30) NULL,
	[phonebeep] [char](20) NULL,
	[phone1] [char](25) NULL,
	[phone2] [char](25) NULL,
	[attyh1] [char](15) NULL,
	[actionp] [char](10) NULL,
	[actiont] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cal1]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cal1](
	[eventno] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[macroid] [bigint] NULL,
	[caseno] [bigint] NULL,
	[calendar] [bigint] NULL,
	[initials0] [nvarchar](50) NULL,
	[initials] [nvarchar](50) NULL,
	[calstat] [nvarchar](50) NULL,
	[date] [datetime] NULL,
	[tickle] [nvarchar](50) NULL,
	[tickledate] [datetime] NULL,
	[whofrom] [nvarchar](50) NULL,
	[whoto] [nvarchar](50) NULL,
	[tottime] [bigint] NULL,
	[event] [nvarchar](max) NULL,
	[first] [nvarchar](50) NULL,
	[last] [nvarchar](50) NULL,
	[defendant] [nvarchar](50) NULL,
	[venue] [nvarchar](50) NULL,
	[judge] [nvarchar](50) NULL,
	[attyass] [nvarchar](50) NULL,
	[location] [text] NULL,
	[notes] [text] NULL,
	[todo] [bit] NULL,
	[protected] [bit] NULL,
	[color] [decimal](2, 0) NULL,
	[rollover] [bit] NULL,
	[chglast] [datetime] NULL,
	[chgwho] [char](3) NULL,
	[Enddttm] [datetime] NULL,
	[SMS] [int] NULL,
	[CALL] [int] NULL,
	[EMAIL] [int] NULL,
	[PartiesFirmCode] [nvarchar](max) NULL,
 CONSTRAINT [PK_cal1] PRIMARY KEY CLUSTERED 
(
	[eventno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedTableType [dbo].[BulkImportTwilioLog]    Script Date: 01/24/2018 10:54:49 ******/
CREATE TYPE [dbo].[BulkImportTwilioLog] AS TABLE(
	[FromNumber] [nvarchar](500) NULL,
	[ToNumber] [nvarchar](500) NULL,
	[Direction] [nvarchar](500) NULL,
	[Duration] [nvarchar](500) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [nvarchar](500) NULL,
	[CreatedDttm] [datetime] NULL,
	[MdifiedDttm] [datetime] NULL,
	[SID] [nvarchar](500) NULL,
	[LogType] [nvarchar](500) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[BulkImportAppointment]    Script Date: 01/24/2018 10:54:49 ******/
CREATE TYPE [dbo].[BulkImportAppointment] AS TABLE(
	[EventTitle] [nvarchar](500) NULL,
	[AppointmentDatetime] [nvarchar](500) NULL,
	[EventBody] [nvarchar](500) NULL,
	[Firstname] [nvarchar](500) NULL,
	[Lastname] [nvarchar](500) NULL,
	[Mobile] [nvarchar](500) NULL,
	[EmailID] [nvarchar](500) NULL,
	[AppointmentType] [nvarchar](500) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[APIServerReminderImport_Promo]    Script Date: 01/24/2018 10:54:49 ******/
CREATE TYPE [dbo].[APIServerReminderImport_Promo] AS TABLE(
	[Account] [nvarchar](500) NULL,
	[Last] [nvarchar](500) NULL,
	[First] [nvarchar](500) NULL,
	[Home] [nvarchar](500) NULL,
	[Work] [nvarchar](500) NULL,
	[Appointment] [nvarchar](500) NULL,
	[Apptdate] [nvarchar](500) NULL,
	[ApptTime] [nvarchar](500) NULL,
	[ApptDoctor] [nvarchar](500) NULL,
	[ApptRoom] [nvarchar](500) NULL,
	[ApptRoomLocation] [nvarchar](500) NULL,
	[ApptRoomClass] [nvarchar](500) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[APIServerReminderImpor_New]    Script Date: 01/24/2018 10:54:49 ******/
CREATE TYPE [dbo].[APIServerReminderImpor_New] AS TABLE(
	[Account] [nvarchar](500) NULL,
	[Last] [nvarchar](500) NULL,
	[First] [nvarchar](500) NULL,
	[Home] [nvarchar](500) NULL,
	[Work] [nvarchar](500) NULL,
	[Appointment] [nvarchar](500) NULL,
	[Apptdate] [nvarchar](500) NULL,
	[ApptTime] [nvarchar](500) NULL,
	[ApptDoctor] [nvarchar](500) NULL,
	[ApptRoom] [nvarchar](500) NULL,
	[ApptRoomLocation] [nvarchar](500) NULL,
	[ApptRoomClass] [nvarchar](500) NULL
)
GO
/****** Object:  Table [dbo].[AddParties]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddParties](
	[Firmcode] [nvarchar](max) NULL,
	[Name] [nvarchar](500) NULL,
	[LName] [nvarchar](500) NULL,
	[CaseNo] [nvarchar](500) NULL,
	[isstatus] [int] NULL,
	[MACID] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[ExplodeDatesDay]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ExplodeDatesDay](@startdate datetime, @enddate datetime)
returns table as
return (
with 
 N0 as (SELECT 1 as n UNION ALL SELECT 1)
,N1 as (SELECT 1 as n FROM N0 t1, N0 t2)
,N2 as (SELECT 1 as n FROM N1 t1, N1 t2)
,N3 as (SELECT 1 as n FROM N2 t1, N2 t2)
,N4 as (SELECT 1 as n FROM N3 t1, N3 t2)
,N5 as (SELECT 1 as n FROM N4 t1, N4 t2)
,N6 as (SELECT 1 as n FROM N5 t1, N5 t2)
,nums as (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) as num FROM N6)
SELECT DATEADD(day,num-1,@startdate) as [FindDate],(DATENAME(dw,CAST(DATEPART(m, DATEADD(day,num-1,@startdate)) AS VARCHAR)+ '/'+ CAST(DATEPART(d, DATEADD(day,num-1,@startdate)) AS VARCHAR) 
  + '/'+ CAST(DATEPART(yy, DATEADD(day,num-1,@startdate)) AS VARCHAR))) as [FindDay]
FROM nums
WHERE num <= DATEDIFF(day,@startdate,@enddate) + 1
);
GO
/****** Object:  UserDefinedFunction [dbo].[ExplodeDates]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ExplodeDates](@startdate datetime, @enddate datetime)
returns table as
return (
with 
 N0 as (SELECT 1 as n UNION ALL SELECT 1)
,N1 as (SELECT 1 as n FROM N0 t1, N0 t2)
,N2 as (SELECT 1 as n FROM N1 t1, N1 t2)
,N3 as (SELECT 1 as n FROM N2 t1, N2 t2)
,N4 as (SELECT 1 as n FROM N3 t1, N3 t2)
,N5 as (SELECT 1 as n FROM N4 t1, N4 t2)
,N6 as (SELECT 1 as n FROM N5 t1, N5 t2)
,nums as (SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) as num FROM N6)
SELECT DATEADD(day,num-1,@startdate) as Thedate
FROM nums
WHERE num <= DATEDIFF(day,@startdate,@enddate) + 1
);
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[ID] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [nvarchar](50) NULL,
	[contactno] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[DyTab]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec DyTab 'Cal1'

CREATE  Proc [dbo].[DyTab]
@TableName Nvarchar(1000)
as
Begin

	
DECLARE @sql AS NVARCHAR(MAX)
    SELECT @sql = 'SELECT * FROM [' + @TableName + ']'


	
	exec @sql

	
	


ENd
GO
/****** Object:  Table [dbo].[dbo.tbl_PhoneBlank_New]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dbo.tbl_PhoneBlank_New](
	[Phone] [nvarchar](255) NULL,
	[MainEventID] [float] NULL,
	[F3] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[GetVerionDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetVerionDetails]
as
Begin
select TOP 1 ID,VersionNo from ApplicationVersion order by ID desc;

End
GO
/****** Object:  UserDefinedFunction [dbo].[REMOVE_SPECIAL_CHARACTER]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select dbo.[REMOVE_SPECIAL_CHARACTER] ()


CREATE FUNCTION [dbo].[REMOVE_SPECIAL_CHARACTER] (  
@INPUT_STRING varchar(300))
RETURNS VARCHAR(300)
AS 
BEGIN
 
--declare @testString varchar(100),
DECLARE @NEWSTRING VARCHAR(100) 
-- set @teststring = '@san?poojari(darsh)'
 SET @NEWSTRING = @INPUT_STRING ; 
With SPECIAL_CHARACTER as
(
SELECT '>' as item
UNION ALL 
SELECT '<' as item
UNION ALL 
SELECT '(' as item
UNION ALL 
SELECT ')' as item
UNION ALL 
SELECT '!' as item
UNION ALL 
SELECT '''' as item
UNION ALL 
SELECT '?' as item
UNION ALL 
SELECT '@' as item
UNION ALL 
SELECT '*' as item
UNION ALL 
SELECT '%' as item
UNION ALL 
SELECT '!' as item
UNION ALL 
SELECT '#' as item
UNION ALL 
SELECT '$' as item
UNION ALL 
SELECT '~' as item
UNION ALL 
SELECT '^' as item
UNION ALL 
SELECT '&' as item
UNION ALL 
SELECT '`' as item
UNION ALL 
SELECT '-' as item
UNION ALL 
SELECT ' ' as item

 )
SELECT @NEWSTRING = Replace(@NEWSTRING, ITEM, '') FROM SPECIAL_CHARACTER  
return @NEWSTRING 
END
GO
/****** Object:  Table [dbo].[lst_TwilioLanguage]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lst_TwilioLanguage](
	[LID] [int] NULL,
	[Language] [nvarchar](500) NULL,
	[LanguageValue] [nvarchar](50) NULL,
	[isActive] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lst_SMS_Timing]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lst_SMS_Timing](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[smsTime] [nvarchar](50) NULL,
	[Isactive] [int] NULL,
	[Createddatetime] [datetime] NULL,
	[Modifieddatetime] [datetime] NULL,
 CONSTRAINT [PK_lst_SMS_Timing] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lst_Recall_Time]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lst_Recall_Time](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HMTime] [nvarchar](50) NULL,
	[Isactive] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_lst_Recall_Time] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lst_Recall_Interval]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lst_Recall_Interval](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Intervaltime] [nvarchar](50) NULL,
	[isactive] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_lst_Recall_Interval] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lst_Log_Responce_Color]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lst_Log_Responce_Color](
	[ColorID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [nvarchar](50) NULL,
	[R] [int] NULL,
	[G] [int] NULL,
	[B] [int] NULL,
	[StatusID] [int] NULL,
 CONSTRAINT [PK_lst_Log_Responce_Color] PRIMARY KEY CLUSTERED 
(
	[ColorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lst_EventTemplete]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lst_EventTemplete](
	[TID] [int] IDENTITY(1,1) NOT NULL,
	[Template] [nvarchar](500) NULL,
 CONSTRAINT [PK_lst_EventTemplete] PRIMARY KEY CLUSTERED 
(
	[TID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lst_CountryCode]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lst_CountryCode](
	[CID] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](255) NULL,
	[CountryCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_lst_CountryCode] PRIMARY KEY CLUSTERED 
(
	[CID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[lst_Appointment_Responce_Color]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lst_Appointment_Responce_Color](
	[ColorID] [int] IDENTITY(1,1) NOT NULL,
	[Status] [nvarchar](50) NULL,
	[R] [int] NULL,
	[G] [int] NULL,
	[B] [int] NULL,
	[StatusID] [int] NULL,
 CONSTRAINT [PK_lst_Appointment_Responce_Color] PRIMARY KEY CLUSTERED 
(
	[ColorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedTableType [dbo].[LRSImportContact]    Script Date: 01/24/2018 10:54:49 ******/
CREATE TYPE [dbo].[LRSImportContact] AS TABLE(
	[Salutations] [nvarchar](500) NULL,
	[Fname] [nvarchar](500) NULL,
	[Mname] [nvarchar](500) NULL,
	[Lname] [nvarchar](500) NULL,
	[Phone] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[Zipcode] [nvarchar](500) NULL,
	[Note] [nvarchar](500) NULL,
	[Phonetype] [nvarchar](500) NULL
)
GO
/****** Object:  Table [dbo].[LocalDBVersion]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LocalDBVersion](
	[ID] [int] NOT NULL,
	[VersionNo] [nvarchar](50) NOT NULL,
	[Build] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Temp_CrashserverRecord_23062017]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temp_CrashserverRecord_23062017](
	[RID] [int] IDENTITY(1,1) NOT NULL,
	[EventID] [nvarchar](50) NULL,
	[Event] [nvarchar](max) NULL,
	[EventTitle] [nvarchar](500) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[FromPhoneNo] [nvarchar](50) NULL,
	[AccountSID] [nvarchar](500) NULL,
	[AuthToken] [nvarchar](500) NULL,
	[EventDatetime] [datetime] NULL,
	[SMSStatus] [int] NULL,
	[EmailStatus] [int] NULL,
	[CallStatus] [int] NULL,
	[ClientKey] [nvarchar](500) NULL,
	[RedirectLink] [nvarchar](max) NULL,
	[EmailFrom] [nvarchar](500) NULL,
	[EmailPassword] [nvarchar](500) NULL,
	[EmailFromTitle] [nvarchar](500) NULL,
	[EmailPort] [nvarchar](50) NULL,
	[EmailSMTP] [nvarchar](100) NULL,
	[CompanyLO] [nvarchar](max) NULL,
	[IsStatus] [int] NULL,
	[MainEventID] [int] NULL,
	[IsTwilioStatus] [int] NULL,
	[SID] [nvarchar](200) NULL,
	[CreatedDttm] [datetime] NULL,
	[ModifiedDttm] [datetime] NULL,
	[AppointmentDatetime] [datetime] NULL,
	[IsConfirmable] [int] NULL,
	[TwilioLang] [nvarchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUploadedTwilioLog]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUploadedTwilioLog](
	[FromNumber] [nvarchar](500) NULL,
	[ToNumber] [nvarchar](500) NULL,
	[Direction] [nvarchar](500) NULL,
	[Duration] [nvarchar](500) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [nvarchar](500) NULL,
	[CreatedDttm] [datetime] NULL,
	[MdifiedDttm] [datetime] NULL,
	[SID] [nvarchar](500) NULL,
	[LogType] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUploadedExcelAppointment]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUploadedExcelAppointment](
	[EventTitle] [nvarchar](500) NULL,
	[AppointmentDatetime] [nvarchar](500) NULL,
	[EventBody] [nvarchar](500) NULL,
	[Firstname] [nvarchar](500) NULL,
	[Lastname] [nvarchar](500) NULL,
	[Mobile] [nvarchar](500) NULL,
	[EmailID] [nvarchar](500) NULL,
	[AppointmentType] [nvarchar](500) NULL,
	[IsCorrect] [int] NULL,
	[ErrorMsg] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUploadedExcel]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUploadedExcel](
	[Salutations] [nvarchar](500) NULL,
	[Fname] [nvarchar](500) NULL,
	[Mname] [nvarchar](500) NULL,
	[Lname] [nvarchar](500) NULL,
	[Phone] [nvarchar](500) NULL,
	[Email] [nvarchar](500) NULL,
	[Zipcode] [nvarchar](500) NULL,
	[Note] [nvarchar](500) NULL,
	[Phonetype] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblTesttoTemp]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblTesttoTemp](
	[ClientKey] [nvarchar](50) NULL,
	[MainEventID] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[TEstget]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[TEstget]
as
Begin
select * from tbltest1
ENd
GO
/****** Object:  Table [dbo].[tblAPIAppointment_promotional]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAPIAppointment_promotional](
	[Account] [nvarchar](500) NULL,
	[Last] [nvarchar](500) NULL,
	[First] [nvarchar](500) NULL,
	[Home] [nvarchar](500) NULL,
	[Work] [nvarchar](500) NULL,
	[Appointment] [nvarchar](500) NULL,
	[Apptdate] [nvarchar](500) NULL,
	[ApptTime] [nvarchar](500) NULL,
	[ApptDoctor] [nvarchar](500) NULL,
	[ApptRoom] [nvarchar](500) NULL,
	[ApptRoomLocation] [nvarchar](500) NULL,
	[ApptRoomClass] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAPIAppointment_promo]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAPIAppointment_promo](
	[Account] [nvarchar](500) NULL,
	[Last] [nvarchar](500) NULL,
	[First] [nvarchar](500) NULL,
	[Home] [nvarchar](500) NULL,
	[Work] [nvarchar](500) NULL,
	[Appointment] [nvarchar](500) NULL,
	[Apptdate] [nvarchar](500) NULL,
	[ApptTime] [nvarchar](500) NULL,
	[ApptDoctor] [nvarchar](500) NULL,
	[ApptRoom] [nvarchar](500) NULL,
	[ApptRoomLocation] [nvarchar](500) NULL,
	[ApptRoomClass] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAPIAppointment_New]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAPIAppointment_New](
	[Account] [nvarchar](500) NULL,
	[Last] [nvarchar](500) NULL,
	[First] [nvarchar](500) NULL,
	[Home] [nvarchar](500) NULL,
	[Work] [nvarchar](500) NULL,
	[Appointment] [nvarchar](500) NULL,
	[Apptdate] [nvarchar](500) NULL,
	[ApptTime] [nvarchar](500) NULL,
	[ApptDoctor] [nvarchar](500) NULL,
	[ApptRoom] [nvarchar](500) NULL,
	[ApptRoomLocation] [nvarchar](500) NULL,
	[ApptRoomClass] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAPIAppointment_Final]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAPIAppointment_Final](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Account] [nvarchar](500) NULL,
	[Last] [nvarchar](500) NULL,
	[First] [nvarchar](500) NULL,
	[Home] [nvarchar](500) NULL,
	[Work] [nvarchar](500) NULL,
	[Appointment] [nvarchar](500) NULL,
	[Apptdate] [nvarchar](500) NULL,
	[ApptTime] [nvarchar](500) NULL,
	[ApptDoctor] [nvarchar](500) NULL,
	[ApptRoom] [nvarchar](500) NULL,
	[ApptRoomLocation] [nvarchar](500) NULL,
	[ApptRoomClass] [nvarchar](500) NULL,
	[CreatedDttm] [datetime] NULL,
	[ModifiedDttm] [datetime] NULL,
 CONSTRAINT [PK_tblAPIAppointment_Final] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblAPIAppointment]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAPIAppointment](
	[Account] [nvarchar](500) NULL,
	[Last] [nvarchar](500) NULL,
	[First] [nvarchar](500) NULL,
	[Home] [nvarchar](500) NULL,
	[Work] [nvarchar](500) NULL,
	[Appointment] [nvarchar](500) NULL,
	[Apptdate] [nvarchar](500) NULL,
	[ApptTime] [nvarchar](500) NULL,
	[ApptDoctor] [nvarchar](500) NULL,
	[ApptRoom] [nvarchar](500) NULL,
	[ApptRoomLocation] [nvarchar](500) NULL,
	[ApptRoomClass] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Upgrade_Management]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Upgrade_Management](
	[LID] [int] IDENTITY(1,1) NOT NULL,
	[RID] [int] NULL,
	[UID] [int] NULL,
	[IsActive] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_Upgrade_Management] PRIMARY KEY CLUSTERED 
(
	[LID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Upgrade_Application]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Upgrade_Application](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationVersion] [nvarchar](50) NULL,
	[ApplicationPath] [nvarchar](max) NULL,
	[isActive] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_Upgrade_Application] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_TwowaySyncCompar]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_TwowaySyncCompar](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventFK] [int] NULL,
	[LastSyncStatus] [int] NULL,
	[CreatedDttm] [datetime] NULL,
	[ModifiedDttm] [datetime] NULL,
 CONSTRAINT [PK_tbl_TwowaySyncCompar] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_TwilioStatusAccess]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_TwilioStatusAccess](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientKey] [nvarchar](50) NULL,
	[LastAccessDateTime] [datetime] NULL,
 CONSTRAINT [PK_tbl_TwilioStatusAccess] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_TwilioLog]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_TwilioLog](
	[LID] [int] IDENTITY(1,1) NOT NULL,
	[FromNumber] [nvarchar](50) NULL,
	[ToNumber] [nvarchar](50) NULL,
	[Direction] [nvarchar](50) NULL,
	[Duration] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [nvarchar](50) NULL,
	[CreatedDttm] [datetime] NULL,
	[MdifiedDttm] [datetime] NULL,
	[SID] [nvarchar](100) NULL,
	[LogType] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_TwilioLog] PRIMARY KEY CLUSTERED 
(
	[LID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Twilio_SID]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Twilio_SID](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EventFK] [int] NULL,
	[SID] [nvarchar](500) NULL,
 CONSTRAINT [PK_tbl_Twilio_SID] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Twilio_LogAccess]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Twilio_LogAccess](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LogAccessDateTime] [datetime] NULL,
	[MessageLogCount] [int] NULL,
	[CallLogCount] [int] NULL,
 CONSTRAINT [PK_tbl_Twilio_LogAccess] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_TempFirmCode]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_TempFirmCode](
	[FirmCode] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_TempData]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_TempData](
	[ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Title] [nvarchar](500) NULL,
	[A] [nvarchar](10) NULL,
	[R] [nvarchar](10) NULL,
	[G] [nvarchar](10) NULL,
	[B] [nvarchar](10) NULL,
 CONSTRAINT [PK_tbl_TempData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RS_SuparAdmin]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RS_SuparAdmin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SuparAdminMAC] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_RS_SuparAdmin] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RS_Registration]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RS_Registration](
	[RID] [int] IDENTITY(1,1) NOT NULL,
	[RSClientID] [nvarchar](50) NULL,
	[CompanyName] [nvarchar](500) NULL,
	[Mobile] [nvarchar](20) NULL,
	[Email] [nvarchar](200) NULL,
	[ProductKey] [nvarchar](50) NULL,
	[MacID] [nvarchar](50) NULL,
	[ISActivated] [int] NULL,
	[LogoImage] [nvarchar](max) NULL,
	[CreatedDtTm] [datetime] NULL,
	[ModifiedDtTm] [datetime] NULL,
	[InstalledVersion] [nvarchar](20) NULL,
	[LastUseDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_RS_Registration] PRIMARY KEY CLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_RS_Contactlist]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_RS_Contactlist](
	[CID] [int] IDENTITY(1,1) NOT NULL,
	[Salutation] [nvarchar](50) NULL,
	[FName] [nvarchar](500) NULL,
	[MName] [nvarchar](500) NULL,
	[LName] [nvarchar](500) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[Email] [nvarchar](500) NULL,
	[Zipcode] [nvarchar](50) NULL,
	[Notes] [nvarchar](max) NULL,
	[DOB] [datetime] NULL,
	[CountryCode] [nvarchar](10) NULL,
	[MobileType] [nvarchar](50) NULL,
	[EmailType] [nvarchar](100) NULL,
	[Isactive] [int] NULL,
	[Case] [nvarchar](50) NULL,
	[Firmcode] [nvarchar](50) NULL,
	[CardCode] [nvarchar](50) NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_RS_Contactlist] PRIMARY KEY CLUSTERED 
(
	[CID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Notification_TimingSetting]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Notification_TimingSetting](
	[NotifyID] [int] IDENTITY(1,1) NOT NULL,
	[EventDateTime] [datetime] NULL,
	[EventTitle] [nvarchar](max) NULL,
	[phone] [nvarchar](50) NULL,
	[Email] [nvarchar](500) NULL,
	[Subject] [nvarchar](500) NULL,
	[isCompleted] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
	[ReminderType] [nvarchar](10) NULL,
 CONSTRAINT [PK_tbl_Notification_TimingSetting] PRIMARY KEY CLUSTERED 
(
	[NotifyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_NextGenerateEventID]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_NextGenerateEventID](
	[NextEventID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Medical_RoomLink]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Medical_RoomLink](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TID] [int] NULL,
	[Block] [nvarchar](255) NULL,
	[RoomNo] [nvarchar](50) NULL,
	[Roomdescription] [nvarchar](255) NULL,
	[isdelete] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_Medical_RoomLink] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_LRS_Reminder_Timing]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_LRS_Reminder_Timing](
	[TimingID] [int] IDENTITY(1,1) NOT NULL,
	[EventFK] [int] NULL,
	[ReminderDatetime] [datetime] NULL,
	[ReminderWhen] [nvarchar](50) NULL,
	[ReminderType] [nvarchar](50) NULL,
	[Isactive] [int] NULL,
	[Createddatetime] [datetime] NULL,
	[Modifieddatetime] [datetime] NULL,
	[IVRStatus] [int] NULL,
 CONSTRAINT [PK_tbl_LRS_Reminder_Timing] PRIMARY KEY CLUSTERED 
(
	[TimingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_LRS_Reminder]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_LRS_Reminder](
	[eventno] [int] IDENTITY(1,1) NOT NULL,
	[CardcodeFK] [nvarchar](50) NULL,
	[date] [datetime] NULL,
	[event] [nvarchar](max) NULL,
	[first] [nvarchar](500) NULL,
	[last] [nvarchar](500) NULL,
	[location] [nvarchar](max) NULL,
	[SMS] [int] NULL,
	[CALL] [int] NULL,
	[EMAIL] [int] NULL,
	[EventParties] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[Caseno] [nvarchar](50) NULL,
	[whenSMS] [nvarchar](500) NULL,
	[whenCALL] [nvarchar](500) NULL,
	[whenEMAIL] [nvarchar](500) NULL,
	[LastUpdatedStatusFK] [nvarchar](10) NULL,
	[EventHTML] [nvarchar](max) NULL,
	[IsPrivate] [int] NULL,
	[CreatedUserFK] [nvarchar](50) NULL,
	[IsConfirmable] [int] NULL,
	[AppointmentType] [nvarchar](max) NULL,
	[TwilioLang] [nvarchar](20) NULL,
 CONSTRAINT [PK_tbl_LRS_Reminder] PRIMARY KEY CLUSTERED 
(
	[eventno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_LRS_NotificationSetting]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_LRS_NotificationSetting](
	[NotificationID] [int] IDENTITY(1,1) NOT NULL,
	[EmailSend] [nvarchar](50) NULL,
	[EmailCancel] [int] NULL,
	[EmailReschedule] [int] NULL,
	[EmailWhen] [nvarchar](50) NULL,
	[SMSSend] [nvarchar](50) NULL,
	[SMSCancel] [int] NULL,
	[SMSReschedule] [int] NULL,
	[SMSWhen] [nvarchar](50) NULL,
	[isactive] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
	[ClientKey] [nvarchar](1000) NULL,
 CONSTRAINT [PK_tbl_LRS_NotificationSetting] PRIMARY KEY CLUSTERED 
(
	[NotificationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_LRS_ManuallyUpadatedReminder]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_LRS_ManuallyUpadatedReminder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventNo] [int] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[TwillioStatus] [int] NOT NULL,
 CONSTRAINT [PK_tbl_LRS_ManualllyUpadatedReminder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_LRS_Calaudit]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_LRS_Calaudit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Eventno] [int] NULL,
	[Caseno] [nvarchar](50) NULL,
	[Cardcode] [nvarchar](50) NULL,
	[IsActive] [int] NULL,
	[eventFK] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
	[chglast] [datetime] NULL,
 CONSTRAINT [PK_tbl_LRS_Calaudit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_LRS_AddressBookDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_LRS_AddressBookDetails](
	[AddDetailId] [int] IDENTITY(1,1) NOT NULL,
	[AddId] [int] NOT NULL,
	[CardCode] [nvarchar](50) NULL,
	[CreatedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_tbl_LRS_AddressBookDetails] PRIMARY KEY CLUSTERED 
(
	[AddDetailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_LRS_AddressBook]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_LRS_AddressBook](
	[AddId] [int] IDENTITY(1,1) NOT NULL,
	[AddName] [varchar](100) NOT NULL,
	[Description] [varchar](100) NULL,
	[Status] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_LRS_AddressBook] PRIMARY KEY CLUSTERED 
(
	[AddId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_GoogleSync_Contact]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_GoogleSync_Contact](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Salutation] [nvarchar](50) NULL,
	[FName] [nvarchar](500) NULL,
	[MName] [nvarchar](500) NULL,
	[LName] [nvarchar](500) NULL,
	[PhoneNo] [nvarchar](50) NULL,
	[Email] [nvarchar](100) NULL,
	[Zipcode] [nvarchar](50) NULL,
	[Notes] [nvarchar](max) NULL,
	[DOB] [datetime] NULL,
	[CountryCode] [nvarchar](10) NULL,
	[MobileType] [nvarchar](50) NULL,
	[EmailType] [nvarchar](50) NULL,
	[ContactType] [nvarchar](50) NULL,
	[ClientKey] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbl_GoogleSync_Contact] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_FinalReminderDetails_Log]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_FinalReminderDetails_Log](
	[LRID] [int] IDENTITY(1,1) NOT NULL,
	[RID] [int] NOT NULL,
	[EventID] [nvarchar](50) NULL,
	[Event] [nvarchar](max) NULL,
	[EventTitle] [nvarchar](500) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[FromPhoneNo] [nvarchar](50) NULL,
	[AccountSID] [nvarchar](500) NULL,
	[AuthToken] [nvarchar](500) NULL,
	[EventDatetime] [datetime] NULL,
	[SMSStatus] [int] NULL,
	[EmailStatus] [int] NULL,
	[CallStatus] [int] NULL,
	[ClientKey] [nvarchar](500) NULL,
	[RedirectLink] [nvarchar](max) NULL,
	[EmailFrom] [nvarchar](500) NULL,
	[EmailPassword] [nvarchar](500) NULL,
	[EmailFromTitle] [nvarchar](500) NULL,
	[EmailPort] [nvarchar](50) NULL,
	[EmailSMTP] [nvarchar](100) NULL,
	[CompanyLO] [nvarchar](max) NULL,
	[IsStatus] [int] NULL,
	[MainEventID] [int] NULL,
	[IsTwilioStatus] [int] NULL,
	[SID] [nvarchar](200) NULL,
	[CreatedDttm] [datetime] NULL,
	[ModifiedDttm] [datetime] NULL,
	[AppointmentDatetime] [datetime] NULL,
	[IsConfirmable] [int] NULL,
	[TwilioLang] [nvarchar](20) NULL,
 CONSTRAINT [PK_tbl_FinalReminderDetails_Log] PRIMARY KEY CLUSTERED 
(
	[LRID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_FinalReminderDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_FinalReminderDetails](
	[RID] [int] IDENTITY(1,1) NOT NULL,
	[EventID] [nvarchar](50) NULL,
	[Event] [nvarchar](max) NULL,
	[EventTitle] [nvarchar](500) NULL,
	[Phone] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[FromPhoneNo] [nvarchar](50) NULL,
	[AccountSID] [nvarchar](500) NULL,
	[AuthToken] [nvarchar](500) NULL,
	[EventDatetime] [datetime] NULL,
	[SMSStatus] [int] NULL,
	[EmailStatus] [int] NULL,
	[CallStatus] [int] NULL,
	[ClientKey] [nvarchar](500) NULL,
	[RedirectLink] [nvarchar](max) NULL,
	[EmailFrom] [nvarchar](500) NULL,
	[EmailPassword] [nvarchar](500) NULL,
	[EmailFromTitle] [nvarchar](500) NULL,
	[EmailPort] [nvarchar](50) NULL,
	[EmailSMTP] [nvarchar](100) NULL,
	[CompanyLO] [nvarchar](max) NULL,
	[IsStatus] [int] NULL,
	[MainEventID] [int] NULL,
	[IsTwilioStatus] [int] NULL,
	[SID] [nvarchar](200) NULL,
	[CreatedDttm] [datetime] NULL,
	[ModifiedDttm] [datetime] NULL,
	[AppointmentDatetime] [datetime] NULL,
	[IsConfirmable] [int] NULL,
	[TwilioLang] [nvarchar](20) NULL,
 CONSTRAINT [PK_tbl_FinalReminderDetails] PRIMARY KEY CLUSTERED 
(
	[RID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Email_Setting]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Email_Setting](
	[EmailID] [int] IDENTITY(1,1) NOT NULL,
	[EmailFrom] [nvarchar](50) NULL,
	[EmailPassword] [nvarchar](50) NULL,
	[EmailFromTitle] [nvarchar](500) NULL,
	[EmailPort] [nvarchar](50) NULL,
	[EmailSMTP] [nvarchar](50) NULL,
	[Twilio_CALL] [nvarchar](100) NULL,
	[Twilio_SMS] [nvarchar](100) NULL,
	[Twilio_AccountSid] [nvarchar](100) NULL,
	[Twilio_AuthToken] [nvarchar](100) NULL,
	[CompanyLogo] [nvarchar](max) NULL,
	[Signature] [nvarchar](max) NULL,
	[ClientKey] [nvarchar](1000) NULL,
	[TimeZone] [nvarchar](20) NULL,
	[A1Sync] [int] NULL,
	[RecallInterval] [int] NULL,
	[RecallHMT] [int] NULL,
	[ReCallEnable] [int] NULL,
	[IsCSVstatus] [int] NULL,
	[CSVfilePath] [nvarchar](500) NULL,
	[BatfilePath] [nvarchar](500) NULL,
 CONSTRAINT [PK_tbl_Email_Setting] PRIMARY KEY CLUSTERED 
(
	[EmailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Email_Login]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Email_Login](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[isdeleted] [int] NULL,
	[Createddatetime] [datetime] NULL,
	[Modifieddatetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_Email_Login] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Capgemini]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Capgemini](
	[ID] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[ManagerID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Calaudit_Last_Sync]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Calaudit_Last_Sync](
	[SyncID] [int] IDENTITY(1,1) NOT NULL,
	[LastSync] [datetime] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_Calaudit_Last_Sync] PRIMARY KEY CLUSTERED 
(
	[SyncID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_bulk_AppointmentType]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_bulk_AppointmentType](
	[TID] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NULL,
	[IsActive] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_bulk_AppointmentType] PRIMARY KEY CLUSTERED 
(
	[TID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_bulk_AppointmentTiming]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_bulk_AppointmentTiming](
	[PID] [int] IDENTITY(1,1) NOT NULL,
	[TID_FK] [int] NULL,
	[Timing] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NULL,
	[IsActive] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_bulk_AppointmentTiming] PRIMARY KEY CLUSTERED 
(
	[PID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_bulk_Appointment]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_bulk_Appointment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TID_FK] [int] NULL,
	[SMS] [int] NULL,
	[CALL] [int] NULL,
	[EMAIL] [int] NULL,
	[SMSWhen] [nvarchar](50) NULL,
	[CALLWhen] [nvarchar](50) NULL,
	[EMAILWhen] [nvarchar](50) NULL,
	[IsActive] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
 CONSTRAINT [PK_tbl_bulk_Appointment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_badge_Notification]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_badge_Notification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MACid] [nvarchar](50) NULL,
	[OpenDatetime] [datetime] NULL,
	[Createddate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_badge_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Apptotosedulingstatus]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Apptotosedulingstatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[updatedttm] [datetime] NULL,
	[eventno] [nvarchar](50) NULL,
	[caseno] [nvarchar](50) NULL,
	[remindermessage] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_Apptotosedulingstatus] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Apptoto_UserContact]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Apptoto_UserContact](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AppToto_UserFK] [int] NULL,
	[ContactEmail] [nvarchar](max) NULL,
	[Isdelete] [int] NULL,
 CONSTRAINT [PK_tbl_Apptoto_UserContact] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Apptoto_User]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Apptoto_User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Isdelete] [int] NULL,
 CONSTRAINT [PK_Apptoto_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_Apptoto_AddSmsEmail]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Apptoto_AddSmsEmail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Purpose] [nvarchar](50) NULL,
	[AppointMentWhen] [nvarchar](500) NULL,
	[Body] [nvarchar](max) NULL,
	[EmailSubject] [nvarchar](max) NULL,
	[Category] [nvarchar](50) NULL,
	[Status] [int] NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
	[TwilioLang] [nvarchar](20) NULL,
	[isSMS] [int] NULL,
	[isCALL] [int] NULL,
	[isEMAIL] [int] NULL,
	[whenSMS] [nvarchar](max) NULL,
	[whenCALL] [nvarchar](max) NULL,
	[whenEMAIL] [nvarchar](max) NULL,
	[IsReschedule] [int] NULL,
 CONSTRAINT [PK_tbl_Apptoto_AddSmsEmail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_A1lawVSDataloader]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_A1lawVSDataloader](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ServerMACID] [nvarchar](50) NULL,
	[Card] [nvarchar](50) NULL,
	[Card2] [nvarchar](50) NULL,
	[Case] [nvarchar](50) NULL,
	[Casecard] [nvarchar](50) NULL,
	[CreatedDatetime] [datetime] NULL,
	[ModifiedDatetime] [datetime] NULL,
	[Isstatus] [int] NULL,
	[StatusLog] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbl_A1lawVSDataloader] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedTableType [dbo].[SyncCloudStatusA]    Script Date: 01/24/2018 10:54:49 ******/
CREATE TYPE [dbo].[SyncCloudStatusA] AS TABLE(
	[MainEventID] [nvarchar](50) NULL,
	[ClientKey] [nvarchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[SyncCloudStatus]    Script Date: 01/24/2018 10:54:49 ******/
CREATE TYPE [dbo].[SyncCloudStatus] AS TABLE(
	[MainEventID] [nvarchar](50) NULL,
	[ClientKey] [nvarchar](50) NULL
)
GO
/****** Object:  StoredProcedure [dbo].[sproc_wsTwilioLog_SelectByField]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [sproc_wsTwilioLog_SelectByField] 'D1A9-8252-0B01-4A82-B21B'  
CREATE PROCEDURE [dbo].[sproc_wsTwilioLog_SelectByField]    
    
 @ClientKey varchar(100)    
AS    
Begin    

If(@ClientKey='D1A9-8252-0B01-4A82-B21B')
Begin
 SELECT  [LID], [FromNumber], [ToNumber], [Direction], [Duration], [StartDate], [EndDate], [Status], T.[CreatedDttm], T.[MdifiedDttm], T.[SID], [LogType]     
 FROM [dbo].[tbl_TwilioLog] T    
  INNER JOIN tbl_FinalReminderDetails R ON T.SID = R.SID    
 WHERE     
  R.ClientKey = @ClientKey    
    
  order by 1 desc
  
END
Else
Begin

 SELECT top 100 [LID], [FromNumber], [ToNumber], [Direction], [Duration], [StartDate], [EndDate], [Status], T.[CreatedDttm], T.[MdifiedDttm], T.[SID], [LogType]     
 FROM [dbo].[tbl_TwilioLog] T    
  INNER JOIN tbl_FinalReminderDetails R ON T.SID = R.SID    
 WHERE     
  R.ClientKey = @ClientKey    
    
  order by 1 desc

END
END
GO
/****** Object:  StoredProcedure [dbo].[sproc_wsRS_Upgrade_Details_Temp]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[sproc_wsRS_Upgrade_Details_Temp]  
@ProductKey Nvarchar(100)  
as    
Begin    
    
SELECT  TOP 1    TUA.UID, ApplicationVersion, ApplicationPath, TUA.isActive, TUA.CreatedDatetime, TUA.ModifiedDatetime    
FROM  tbl_Upgrade_Application TUA  
INNER Join tbl_Upgrade_Management TUM on TUM.UID =TUA.UID
INNER Join tbl_RS_Registration TRR on TRR.RID =TUM.RID
where isnull(TUA.isActive,0) =0  
and TRR.ProductKey=@ProductKey
ORDER By TUA.UID DESC  
    
End
GO
/****** Object:  StoredProcedure [dbo].[sproc_wsRS_Upgrade_Details]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Proc [dbo].[sproc_wsRS_Upgrade_Details]                
@ClientKey Nvarchar(500) =NUll                
as                      
Begin                      
                
                
                
 if('83B7-2CE8-95DF-4F46-845C' = @ClientKey) --GBLAW                
 BEGIN                
                       
  SELECT  TOP 1    UID, '1.1.3.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/GBLAW/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime                      
  FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0                    
  ORDER By TUA.UID DESC                      
                       
 End                  
                 
 else if('D1A9-8252-0B01-4A82-B21B' = @ClientKey) --EBLAW                
 BEGIN                
                       
  SELECT  TOP 1    UID, '1.1.3.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/EBLAW/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime                      
  FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0                    
  ORDER By TUA.UID DESC                      
                       
 End              
 else if('49FB-6DED-9841-4939-A202' = @ClientKey) --Chirag          
 BEGIN          
                 
  SELECT  TOP 1    UID, '1.1.2.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/YOUNESSI/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime                      
  FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0              
  ORDER By TUA.UID DESC                
                 
 End         
                 
 else if('A819-0B08-CBA2-4BEE-B199' = @ClientKey) --YOUNESSI                
 BEGIN                
                       
  SELECT  TOP 1    UID, '1.1.2.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/YOUNESSI/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime                      
  FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0                    
  ORDER By TUA.UID DESC                      
                       
 End                  
                 
 else if('1A92-A3C7-6D86-42FE-80B3' = @ClientKey) --SSHYLAW                
 BEGIN                
                       
  SELECT  TOP 1    UID, '1.1.3.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/SSHYLAW/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime                      
  FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0                    
  ORDER By TUA.UID DESC                      
                       
 End                  
                 
       
  else if('DA0C-E16D-9DF0-4BA3-B2C8' = @ClientKey) --SFF                
 BEGIN                
                       
  SELECT  TOP 1    UID, '1.1.2.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/SFF/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime                      
  FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0                    
  ORDER By TUA.UID DESC                      
                       
 End      
 else if('4777-E025-EAC7-4838-BE50' = @ClientKey) --G&B                
 BEGIN                
                       
  SELECT  TOP 1    UID, '1.1.3.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/G&B/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime                      
  FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0                    
  ORDER By TUA.UID DESC                      
                       
 End    
   else if('C73F-DDDA-BB96-4ACF-BA6D' = @ClientKey) --Chirag        
 BEGIN        
               
  SELECT  TOP 1    UID, '1.1.3.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/SFF/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime              
  FROM  tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0            
  ORDER By TUA.UID DESC              
               
 End                      
  else if('F195-D255-B7E5-46D4-8ECC' = @ClientKey) --ACS        
 BEGIN        
               
  SELECT  TOP 1    UID, '1.1.2.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/SFF/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime              
  FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0            
  ORDER By TUA.UID DESC              
               
 End      
   else if('931E-6EF7-E047-42C5-9B53' = @ClientKey) --API        
 BEGIN        
               
  SELECT  TOP 1    UID, '1.1.3.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/API/' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime              
  FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0            
  ORDER By TUA.UID DESC              
               
 End                   
 else                
 BEGIN -- STANDALONE                 
                 
            
 SELECT  TOP 1    UID,'1.1.2.0' as ApplicationVersion, 'http://108.222.150.123/ReminderSystemApp/STANDALONE' as ApplicationPath, isActive, CreatedDatetime, ModifiedDatetime                      
 FROM         tbl_Upgrade_Application TUA  where isnull(TUA.isActive,0) =0                    
 ORDER By TUA.UID DESC                 
                  
                  
                  
                 
 END                
                 
                 
                
                
END
GO
/****** Object:  StoredProcedure [dbo].[sproc_wsRS_Registration_Update]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_wsRS_Registration_Update]    
 @RSClientID nvarchar(100) = '',    
 @CompanyName nvarchar(1000) =  '',    
 @Mobile nvarchar(40) =  '',    
 @Email nvarchar(400) =  '',    
 @ProductKey nvarchar(100)=   '',    
 @MacID nvarchar(100) =  '',    
 @ISActivated int,    
 @LogoImage nvarchar (max) =  '',    
 @CreatedDtTm datetime,    
 @ModifiedDtTm datetime    
    
AS    
    
IF EXISTS(SELECT 1 FROM tbl_RS_Registration WHERE ProductKey = @ProductKey)    
 BEGIN    
  UPDATE [dbo].[tbl_RS_Registration]    
  SET    
   RSClientID = @RSClientID,    
   CompanyName = @CompanyName,    
   Mobile = @Mobile,    
   Email = @Email,    
   ProductKey = @ProductKey,    
   MacID = @MacID,    
   ISActivated = @ISActivated,    
   LogoImage = @LogoImage,    
   CreatedDtTm = @CreatedDtTm,    
   ModifiedDtTm = @ModifiedDtTm    
   WHERE     
   ProductKey = @ProductKey    
 END    
ELSE    
 BEGIN    
   
  DECLARE @LastProductKey NVARCHAR(100),@LastRID NVARCHAR(10)  
   
  INSERT [dbo].[tbl_RS_Registration]    
  (    
   RSClientID,    
   CompanyName,    
   Mobile,    
   Email,    
   ProductKey,    
   MacID,    
   ISActivated,    
   LogoImage,    
   CreatedDtTm,    
   ModifiedDtTm    
  )    
  VALUES    
  (    
   @RSClientID,    
   @CompanyName,    
   @Mobile,    
   @Email,    
   @ProductKey,    
   @MacID,    
   @ISActivated,    
   @LogoImage,    
   @CreatedDtTm,    
   @ModifiedDtTm    
  )  
    
  -- // Default Email Setting And Signacture Set when New User Registration  
      
   
 SET @LastRID =SCOPE_IDENTITY()  
   
 SET @LastProductKey =(SELECT ProductKey FROM tbl_RS_Registration where RID=@LastRID)  
   
 IF(@LastProductKey != '' and @LastProductKey is NOT NULL )  
 Begin  
  
  Insert into tbl_Email_Setting(  
  EmailFrom, EmailPassword, EmailFromTitle, EmailPort,   
  EmailSMTP, Twilio_CALL,   
  Twilio_SMS, Twilio_AccountSid,   
  Twilio_AuthToken, CompanyLogo,   
  Signature, ClientKey)  
  
  VALUES  
  (  
  'inforemindersystem@gmail.com' ,'blpetrriuwzpyalv','Reminder System','587','smtp.gmail.com','+18442859779','+18442859779',  
  'AC11c42ab4fdeaf09fb087ee755c3692a2','34567f60d2a169206ea9a8f8b5a859ce',  
  'iVBORw0KGgoAAAANSUhEUgAAAUUAAACTCAYAAAAZZAvbAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8i
IHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnM
uYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSU
Q9InhtcC5paWQ6RUI2QjJENUY1MzJEMTFFNkJEMkFGMTU0QjFCMjFEOUMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RUI2QjJENjA1MzJEMTFFNkJEMkFGMTU0QjFCMjFEOUMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpFQjZCMkQ1RDUzMkQxMUU2QkQyQUYxNTRCMUIyMUQ5QyIgc3RSZWY6Z
G9jdW1lbnRJRD0ieG1wLmRpZDpFQjZCMkQ1RTUzMkQxMUU2QkQyQUYxNTRCMUIyMUQ5QyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoM5YvEAAG5GSURBVHja7H0HgBzVle2rTpNzDtJkhVHOQgIBQhJCpDUgOSdsg9feXcc1eL/3f2/yguP//rb/gtde1mCDBRgwIkoi
SEhIo5xGcWakyTnHDtX/nlvv9VT3ZElgaVRPFJ2qK/W8U+emczW/3y+scfnG6ptuilyxcmWBTdNupJc36rr+5See/F17dVW1z7o61rDGlT8c1iW4POMjd9+dERMTs/Tuv7p79vU33DBD02xzNU2b6/N633z2+eefp1VaQ7/zyY9/PDbM5ZorNJFCL9NpyVCf4V6FG5bfrwsdj7qfHvXAo/k9Xcd6tKbu6/L5fPU+XT9RVVVdXnriRIf1y1jDGhY
ofmgjOTk5fO6c2bn0dMo3vvHN+VnZ2esJCOfT60S1jt1uv5/WOTllypSDVVVVvXiPwNO5dMmS/B/+6EfLEhMT1tJ3phCm5eo+PUd9D2Dn9XiF2+OhR4/w0ILnbje99hqv1eIeGDAe3e7Wvv7+Cx63Z8+x40f3LVq86PjAgPvc8WPH2qxfyxrWGN/QLPN5YiMnJ8cRHh4eTU/js7Oypv74Jz/5K6JptwhNm0HvuYb9kt//w29/+9u/3f7WW6fj4+
MjimfOKPzv3/3uM+HhEZ+g658y4B5wegnsiOoFvkJmt/D4vAR2XgME3V5B6wWA0Oulz9x47jaAE89pPfye+C6BZ2P/QN/OlpbWP7zw4ovbKy9csFijNaxhMcXLN5wul0YPtn/9p39KvmXt2nXEAO+h1yuI5aUQII76XbrtfM5m0/bSNs4uX7p09mOPP/5PUVHRtxGzEw0NDf62tnZfb2+Pn0zfQVD0ESh63QR+PgMAafExOEqG6PXy+2CTAEO8B
qg6nU4tIiLCRsCdGhYWfm9SUvLKT37iE9+Mio7a3NPdY/k1rWENiyle+khJSXG8+eabS+jpgwSCMJHThpLBMa/jawSC+2yalp2QmHh/T0+vqKioEI2NDW8NDAz8v3feeXvbc889336px3rfxo3z09PS/iE6JubW6OjoWIfdLnp7e0VHe0fhSy//uep8RYXb+kWtYQ0LFC9qrF1zy8wvfOFLGxwO+8qiadMW01txtETRYp/otsik7dF9vn4CVIdm
s8WVlZeJqsqqnzz99NO/r2uoP0vA1dvS1Kxf6jEnp6SELV++PL+woOCu2NjYTZGRkQu9xEBbW1sff+GFP/2gorzigvXLWsMalvk87vHQ3/99YlRU1Dwyia//1Kc+vax41qxpIIu0xE9kO4gamweZxlFk3kbhFtTT2elpa20tJ0B84szZs+fa29r6L9fxNzc1DUzNySkjEH5pzuzZUWHh4QvDXE4EfFbSPTDa+oWtYQ0LFMccBQX5ETfecEOmX2j
Tv/ilL60ls7NA02zF9FE2LWEXRcE1W9Br+A/9Mn2GWFt/d3fPjhOlpWd7ursH2ERPTU0iM31BTEz06oyMDOEecIuu7m5hs9lEZEQ4LZHC4XCIto4O0S3ftzvsvJ58rR85fPh72FblhQvugsLCcpfTeWzJ0qXdYWHh0WS251zsuVjDGhYoXiNj/rx5AKKcW26+edpX//bv5hGQIZ1mzQdxXRAggRlLJrQAENI4CKtafR4dE52YmZGxigDxu6tWrR
KtLS2isqqKgTA1NVVkZmaK+Lh4ceToEVFTU4OAinC5XKKrq0vU1tbRenZs63tqe2XnzrkXLFzYvGz58qqIiIiZ2AUtNutP3hrWsEBxyCieOTNV07TET3/604tuv/POdUThVhG1y72UbSKRerSBvEOkz3iNdBq/T9dhMn/QDl230+Foc5H5rHO6j+U/toY1LFCUIzIiAiwJARL7uzt2fJZMyk9qNq1Io4/GkVIztrlsG30bNpudU2j6+/uFzW4LI
9N2YXhY2DP0kfcD+WEdDm3x4kVpsbGxK5DWQyDssyDRGtawQJHHZz71qdg9e0sWE3B9hqDrsxP9vnYZjiE8PEy0t/tEd0+PsNsdseHh4X/75htvvDh3zpySo8eOdV/uc169evXMO++887rExCRRV1cr+vv6/qzrfquyxRrWuJZB8Wc//WlxTk7uJz//hS+sttttCDRcUvT1UtKXIiIihcvpYrYIv2JYWJg4cvToT++5556vL1myZO/lPO+N9923
YMNtt30yLS31PiKIorGxUQwMDBz263qv9SdvDWuMQYImW55iXl5u/ANfemA1sbu7brzpppy4uLh8Mo9TQdYuFdgudSBKXF1VLSqrKgkkI8AY+71ez9GqqsqS/r7+9tjYmOzwiMii5ORksX//vqL6hoYEv+53kpktHE676B3oFZW1VQRwbjolTYRFhPVmpWc2zMyfWUuv/a2tLa84na6E6dOnz83MyiomUz2jpqbWX1l5oWLr9u1/VV1ZWdHT0zN
g/dlbwxqTnCluWL8+KisraxrhxJyPbtxUfNfddy8jUFxMQBEpQiOu2viMYTN4avI7lwqokZGRIik5UbS2tYm2tlYRHhERHuZyLS0sLJoa5grrcDgdnGqDoIjX6wsjoLQhlQcVKT6/T7R3t4uyqrLA9pKSk+1Tp2ZHFRYVJeP1DMeMzzldrig63qSW5paIpuYmT1NTU3V7e/v/rSgrO0vbssr8rGGNyQqK4eHh2k2rVgEMpn/q05+evmDhwiU2TU
PVSREtsSNS43FzaM3Q76JHfOdy8EsAXlxsnMjJmcoiD319fcJLoEdAlu7zetMdTmcAfBmAtTG3F2bTbKkejwdMWPi8PtFDAAo22NrScqGmtvZkVU3N7v6+3j/ouq5bf+7WsMYkBMW0tLTItNTUxLlzZif++Gc/AxD+FTGj60CcJrShMVifFsoqaX0thDnqF8EcXWQKI+cwKTFRHDt2DIncorenB6Z1v8/n6wWwESQCMKMI8AgvXTanwyH8tEuv3
yOSE5KFT/cJt8cN34evu7tnoLq6mitiCPfc9L3m5paWxq6urn319XWv7t2zd4f1Z24Na0wyUIyJidFQM4zjXbfmlrn/8q//dh8h08303sKxTN9RmeBoIBnyuTaM2T3eTOhQ8MS2yHQWS5Yu5WNtaGgQBGw1TY0NR9v6DIWviIiIhXa7Pd3hcITB7A6PCBdur1vk5uWKzu4u0djSgC31REfFnCemWIbteD3emo6OjicPHz58pqKiosvn81rs0BrW
mOC4KgIt27dvz8zMyPgYocld9HIegUr8xW5rtPMdDviGXX+UbbDpq0CTtqcAUZNmuHkgZ7Gzq4vZYlNTE1enmM3ngBmt3pMJ4rqQ75uOA8/wuV/3HaHP3vb4fG+cPn1633PPPtti/ZlbwxqTABQXLVwY+y//8s+z6fBuyc3L+xwxJqhZR9DiHC9J848BXgAq+PZ8Pp1TZC6KcQ63bePCDrJNuR0ETAB8ff0Dorm5SXR2dnJ6jk/XObiCZbxgqI6
MgVCJTxjve2idfixut7uT9llFn7xF2z788pYtOy6cP2+JzVrDGlcTKG66795py5dfd31iUuIN161YOQvK1AReU8VF1O2OdW4AxS4CpuaWFuTxienTp1868wxhb5oEw6bmZtFCC56jBhpRZeQsmgGQzGURGxsrwsMj+Ns6g6FfRJH5HB9vkGMAZ1tbu+jt6yMw9w6CJC2oifbLY5IMlR78ffRGEz120Of7iZ1uKa+oeH9fSUmD9edvDWtcgaCYkp
Jiu/eej8TaNNtcern4pptvnjNv/vxZ9LyAMCtxYgA1NngpE5mDG7T09HTzIwIcM2fOEC5X2Igm9XDXaiTzGu9DDbu9rY19hh0dHcQQ+4OAEAuEHWJiYljsISEhXqQkJwknWKt/EFhRDRMTHRNoNQD1HPRlgVK3sX9jW5WVlQyYbbRPmOX9A/3CIJF+xTib6f/H6xsaDtXUVO/u7x94872dOzutaWANa1wBoFg8c6YzNzc3OSE+fvr/+Md/XEIsa
RHhz3LN6GjnGs4kvVRgVAMgBXBqbzeErsNcLgaQpMQkkZaeHgDCUB/jeEERog9I1AYDhb8QIKVaDSAtB6Y6luioKGaGyUlJkA4TiYmJIjYubsTjBnFEhQraDuj+Qc1GAxd1YqE9orGhkc+vqbFRtNH5AfD7+/pFBzFiNq4NE7uDzPbDxDZfKCkp2UvnUHr+/Pme1pYWK4/RGhYoftigWFw8MyrM6Uq75ZZbsu//4hfnEBjeZrdpt4/HTL3YI1V9
TDBQSXL06DFmhL19vWyagoW1d7QLpL7kFxQwcI0H/ELfA1gNELgCBOsJnOrr6wOMEADrdDlFHO0LwJtEQDg1J4fN4uH8mejDgtQbgBhYbH9/n+js7EJ+pogmZontBYIt9NyOtB3pk8QAk0SSeF1dHQPkmTNnGTT7yWzXJcMk27qPvvNnYp9/2v3++/vLy8trCRj7rWlhDQsUP+DhcDq12JgYsL+wXz/++Ly5c+d8nib1OnqddTn3o5sACJFfr8+
Qy2pv7xDNzc3MDgmUed2zZ8sYwOD1A1NMTEwQiUlJkiEOzzxHulYK+AbIPK44f17UVFdzhz3jWAzBWYBxdna2mDFjuigqmjb02HVfgA16PW7a1gD7ORGEQUCm7Nw5cfTYMZGVlS2WL18qIsIj+CYBcxoKPWC5Zv+kAmn1Xk1trSjZu5c1Gnu6e4RXN7QdsRGjd7T+6L4D+/9w+PDhkwN9/R5raljjWh0fSp5iUWFB8ksvvfQZAqpvEECkXep+Q6
X+MQbcHlRyEKvyiqRkA9yqq2vIjO0SMTGxAgkxVZWVAVBMTIgXkVGRDFaj3jVMAeTRzGmk1Jw5c4Z9h+bPoqKixcyZM8Ws4mI2kYcbPWRqo7rFLduXHj92XCxduoR9jE5itJ3EYvcSoD3ywx+KO++4XSxdslgk07YAerU1NQzEhYUFgX3CRxkWFh50s0gndnrnXXcRe2wVx48eF8dPHOcbhQlIH1q0cNGSqMio/0fPn7OmhjUsULzM43v/8A+Jq
1evXkVP7/v1Y4/f4rA7UIfMTZ8ulZ2GSv0DFODDowkt4glILlyoEvl5uWSiJkKan0zjDpGVmSXmzpvHDBIDn6kSPjPYqbzC1pZWDozAvI6Ljx/VZAb7ApPr6+/jzxFFhlm8YMECUZCXhzI+I3hiYoVdZArXNzQID5mzyQTi8cT0HHYbN7tPTUshkHcz03UGfIZDrxmCRQjmoDomKSk50De6lkxmAK3T6WAfZWzsoJ8ynp4DcKfPmEaAWitOlJaK
8vIyTkuifazMz89P2HD77XmvvvLKj6zpYQ0LFC9xREdH2x75wQ/QFP629betX5mZmYU6ZAROkoJB7dIUCuEf7Gjv4Hy/jPRUAhMHm8H9ZG52k2kImX6DpUWK2bOKDV9iJHqcDDbhC/UbcqpLu8HyOJeQmBv8dwA4mNl4rtiiAkb4+S4Q6IKRYn1sA5UqUzIzxZw5c9h3GBM7WIaN4xtAvbPPK8KJycUTYJ2/cF7EeeKEi8Ug7LwPBF/gl4yJjhZ
h0kxGeSAB1jCMmUx0OhdODrfb+djc7oEAS4WZDfaK7UZFRggbrYNtIaiDShko8qANanlFBdKFwuj5jLTU1E/cfscdmtfj+a/397zf0tnRaVXGWMMCxYmM+fPnZd54/Q0LH/zSF2cQy1hKbyGlJkezaVGjmsG6f4Jg6GUGBOaHPD+AR11dg4C6THpaKoshQH0mPj6B8wJhIiclJ4+6TQAUcgdp4nP+IHyL8OfhPcMv6RM2nz2IqBm5gm2imhhik8
w9BBjhePJycsSMGTNERvYUgS56fNwwiY8e5fSbXGKOaDgFYIoDSJ/3cx2z2wOgtLO5Du3F9o46Q3tRBlvQUiCFzgWgpkmGCnNbNyWey1I/Pn5cE5jRvC+viyPPhw4dFmnEQgHWdrqRAOjxfAmZ4wBImP+tbW0RDodjOpnbn4NIxfz5C/53WkZ6XUNdveVntIYFiqON2269NdIV5iqkp7l333Hn3M9+/vMr6fkMAsLccZvBNm1MYMTER4oLcvUQG
MBr6AmCpcUR04KPLIuYGWqDYe4CsKIiwsddjQIQQVQXaSxQqSGWxEwL74OBISKt1GvU6CRQrqqsErW1NQzU2FcCmcsFBQWc65hFgKhGHydZ+4gRXhDdxEABoEuXL+fPAJJgc9yRr6uTQDEFFwWMW/T3DRjJ2bK3itPpYnYHM2imvDzbB0ERM56BdlmTH6ToTDydvnPEe6HocPHhSnT58mNqpxQAnmsmLO2VOmMsPFADC2dXRE6B7PDGLTGbk5
Oa01tbVP0kdWv2hrWKAYOhKTEh15OblQr8741a9+NT01LfUWen49LYU0UYeoWo8HmEbqbYKvgmExKyTQSE5KZJAC+4G5V11TS2Zhpwl8DEAEm0JAY9gUGsna+vt6mU1FKDCRSdEwvTmRm0AMQRiYt9Ex0UEsEaBTXV0t6hvqGaAx4oghziwuRu6lSExOCfgNAd6IIIP9gUWeOH6cvztn3rxAgCclJYW3BeYZn5DIJjT8gCg/xPfBFsEOI2l9nDt
McOX3NJikl4B7ELQ7CFzBUAFyftNv8Mprr4opBNZwI+AtMG2kg9r4+mvsk1ywcCGzx9KTJ3GT0Gj78fS7foPOr5qA9OXWlharjtoaFijKSe8ICw8Lmzd7dvJvfvvbRQROn6PJcqsYJsn6UnyHuj5YdeLzGQnQ9XX1HOzoJjCKJVCIJCDDVsEcsXkwJ6TcxMXHsSRXmGvwkJDnZ6TnGP5DBDVg7tYwqDWInKk5Ir8gT6SmpjDwING6q6tb5ObmEo
OLYtPTOB65PWJX5ysqOL2lr7ePa/jAxm666SYxdepUEUZABDD00HoeYpvw5SHgEkFM7ZY1a8TxI4fFMQLGc2fPiDlz53H6DQAQ5XndtN9++DEBZmC7tH+U8oFpRkfFiITEJAbDRjpu7iHNQrReNp8RyDEqVnTRT9cJSt0R4eGBGxOi8vv27Rfr1qwV06ZNY18kWCaYK0xvp7xmyJ1cumwZM8mtb77Jyee4F86aNetBu93eTzeRF+m8rTxGa1ig+
Ndf/vL8L3/lr++lp3cTyMwc0Ud4iVFl9Drm5u8ECEitAXtSwYWammoWWQhPRWR2gMEBScyzZ89iYAod3V29orLyAoMLKkbgU6uurhXNzY1i5YqVYtfu3Rz1ReI2RmZGhshITzPAeYTzQLUIAxiBDEZURKRYRiCSX1gkfaQGUCE9p5aAE3mJYQROsoRZ3H3vvcJPAP3QQw+JP295xTChCZDgBujp7RHtba0ixZXB7wPcWSyCtge2iURzbP/w4cMB
HybyImG+wx+ochLBrNPT0gJACXK3+dlnxdq160RqenognQn+UrBEmPUAdBWcwrXMy8sV6269VTz1+98bghPCv3xaURGR1XA4aH9hTRtrTOZh//73vz/ih3/84x/vaWlu+sny65Z/LSws/CbN6HViG40ZTnSBqdrU1MzNleATbIdJTBOXTHUyOWPYP4b1ouBno88BEAlkZmZkZoh0AjH42swD61RWVjM7mjplCrNN2IsJCQkcjQbbgw8RrBI+OjS
bDzawB89DmNoQtLe1i70lJRzMwWuU6BUVFYmlS5agZSl/Fyky2H9YmIsjx/DPwUepkUmMc8L+O4nVnj1XxtUuc2fP5ug2lp6eXma8qZzLaOQO4tgBVh3t7Zw6A6A9e/Ys51pmEIgbohI+Bne+qbS2sBsB7BRAiWM5VXpS/Oa3vxEPExDDrYBTQoVMV3eXqKtvEOm0HQ7QELjiM9yUDGGKGL5hnD9/nn26tE5SVGRU/H/8x390ffazny21po41Ju
sYVXlmX0lJh8PhzI6JiUsnhIikxWHovoy1jD1amls4WIGJC5YDloagBnx7MBthTirAglkJNRtMVkx2MByYfQAMTGT43uppglfS9gB4vA0CL/j8YF7je8y+iLVNIfaGfcWTue102GXyss6LOf8xoDaDbRFgoZqkXwZyYgksZpAZunDBfC6vY18j2CN9CccFlRswVCRMHydzubuzwwB3m0MUExCuueUW8dSTT4qqqioG+TAyr5FTCGDq7Ta6nQJ0+
7i1QLdoppsGSvXUQMkeWDD8sZHSL4pjbW1tY9+jYn3InSzZVyLuI4aKXEvlBgAgwh8LHyWOE35E+Gmxf4/0k4IxIuE8NyeHz4m276AbxTxitV+5efXNN1pTxxrXJChu3b698nxFxVG3x90yMf/gyIBZX1vHkxzAB9bV3NwqYuNi2QwEuLUR26mrqWUmhcBHOLGuaGJYcbROHMtqhQeYHJuHBK7wjRnvGcnPKSlJzAJhfqPelytFaPt2u1Fuh8XB
UVybZIa2IQnhKqnbQ9tDMjTMUAAbADaTWGpBUaGII8bql77KbglmABMAJczX9LR03hefCwQZCHjB1hYtXsyR6s3PPCOqyHxFcCWagBbrYl9dHe0cAQdowWfYQaA6wIERY1yorOTkctRvh4WHcV6lsXSz6wEAC8YIlnfy5Emxdt06PncMlAxiPZwfGCXMaGardMxgkPBh9verPMcosXTpUpGUmGA01PLrMTa7fX5CfMKD1tSxxjUJikePHGn405/
+tOXwgYMnaUJ7LibpGnmAnFtIJiCWZmIzPCm5mkTjvEIAWW9vH7MURJgx0cM4kKIx68FEh98NEWGABQIiitHA5AbAwgcH8ARQxsXFM9fr5qoOFwceQg99POeCbQFsLhC44Bj9uo/Tf1BSl5KSyj4/mJZgiTimFpivvUZrZZxHBB0PfJU4xhb0YqHjAfhkEVv9+Mc+Jt7esUOcPn2KfYl2GVkHoMGlCaBEZBrnDMA2EtSNAbMZxx8bE81VPEosI4
ZeOxjcdA7m1NfXMStOleWFAEokvYMRwh8bbgrGgLFCkgzXjGuufYbvEedaXDyLU44A0LrXG0vfX1tQWDg/DNEua1jjWgJFWIV/evHFHXtL9u6jidg4Ub8h/IUwPQEIDcSWjMoKnQUIVFAGzARVIUACAEAOkp9nzqDHXAYJxTDB9LA9RH0bG5u4TA77iI6OEi5iZzAldalS09XVyT5FTHokdcM/OVwwZmgtc3DhBsx2+DoRsQYgIqm6GCZlXh6DD
/bXLzUSwbIAjM0tzQx+CJDALRAdG8dMGMDZTKCJipbIqGix5tb1fGYweaGBCBYKk3nK1Bz2n2IBcAHgUXWSIEsNYcqihDEtLY1zHKPo/PmRvpuVncWsz+N1i7raWgbTTR/9WCDaDObsljmfuLaq7hm/AW5YuLZ+9uGaVHpoLF6ymMzoXL6GfQMD9t7e3kQC+8/S9Y2zppA1JttwjMGUvAQcnWQ67qmqrCoumjYtQwHpeCLNAAz4xDg3kEzjzKws
9vXBTMYE5MgpmYwAHvjfEAU1gxf2ACaG9xCgcIUZ7BGmHMzAuIR4DqAI+g8gCRO3vb2NASAnN4e3zZ1KdT+nyqiosmJdQYoyIYCIT7C9qurqgPJOYWERmb8pBIhOfo8jw3SOCWSqx9NxwEcInyYSvxFgCY+MRC62yM6ewmlAMPPr6ViysgxxoIce/i6DOoIycVJH0Sz/pczxXDoXRNuPl55g1nfXXXeLwqIiCWocluHnOK7oaCdfbwB3UkqyyJ4
yhVOdcEMCACfEx3HFz2B/aS8LQ8BkxmdKzxH5kMLj57QkiEvk5eeJqpoqliIjJqnReX+M9vkb2k6LriR+LsP4zu6frYHn5gqfNw+bnm/74YpvHLicG6dr8IDgv2qxRi6XMsrp+AosqLtMoCiBo5sAYUd3V/e0H/3kx9fZ7Y6E8ZqfYCMq4RjCDABEgBiUXWCmwacFoEHuHJhR6ICv7vjxE2Lu3DksttDKPsJeBiMCaE6RQY4h2BF8eJDVKiwsDA
I8juISMCOwwL4yep1PE3y04wfDgrpOLfyfBBhYF/4+RH2VAnY/+94MQFT7QQoOzNOGhnpReuq0mDWrOFCCh2g3QBNAi+g0M7DFC4XLFS5bTI+s1wjAzchI51xK1FQj8gyw1M19XEzPcT3IvGVw7OvtoUcv53tCLAMBF/4us9w+rtsG600jRo3cRkTScRMJdzmljzKSmXoBmdEXzleII0eO0rVsJ1AcSCcw/xTdtH5DK5y9xubNIyEghodHaXn2Y
gGStoF59ZBcrHEFm88BM7q9s2PbvpJ9vxnJVB7PgHkIUxEpNfPmzxOzaYIrU9G8LURaz549x/XIZWXnOPKckZHJoIPocVp6GucCcr6eAWMcWTb7yNQACwUgqiACEAg5gT4TuVEsUQVc8Pp8eQWnwPhlv2eAESpcADhgc+VlZWLbtm1c6mfeH0zlKcTOEJwoKdlHx9moUlrYBAbTA1ihggQMbLhLF2hXANYol2AW6w9oR5qX0MZXCK7AH4vKHSS3
I1iFmwfKB3FNq6qqGRABuIjqc/CKABF5oOcvVIpf/vKXHNWHaY+IetG06WL2rFl8PdHugNj6x91ut8VCjAEw20/g9pgEuIkA4iJ81wLEqwsU+44ePXb+ySd/d4hMp4rh2dUo/kW5DnyL2dlZImtKNstZgbHAjMNSXV3FYAhWCXADm0LqCJK3oWKNgAcmcBh/FsYTGVFgu83IEUQ1sGJcWA4fPsJ5fxrXEkdxjiD2A8GHmppa4RkYjOYiqIFFgXu
XbCWgkrTD2Zc4k0UWsG1EtI8cPix+/4ffi09+4pPij08/I9ro3BQ44vgAftPIxAXwoIwPDBIHiHI/CMJqphzIIGDz+YKAMLS7X8C2DwVQE1NUQG8819isTk5JIXAzqlsQsYcPEUwdDBG/gwLnc2fPiv/67X+Jb3/7W8R2T7Fv0j1gFLHgpgRQBNjCfUE3mQwC4iV0LrnWVAqMByQ45o8TEPOluyDfunRXifksJ5pOf/hdZeUVR195+eU/3Ltx4/
8YZd0h7yGiisAAm9QECnYp2wXGBF8X2BTyCeHzgonIQQZawOxgbgLY8BwBFYgYxHDkNMK0r8F9wkeGgIcBtDXEyBLYZ+e2uWWqSh9HV5WfcLjjriOzGceFbcFszsmZyikzGDonOvs4pWbd6ltE+fnz4rU3Xhdvv7Vd3Lz6ZnH9qlUik1itS6p5A+QRWdckIIaHG+AcAK9QFhiq1xjwGw6e6ZAWqKGK23598HvyPHHt8AbAzMu13VF8s1A+3AZih
K++soWVdPqIKc4qniVmFxczuPv9YlAWbepUTin680t/5p+WloW0vE/LeWs6BQYAbjM8JONY9zHpP7TGVcYUMdzNrS3Vr77++hvd3d2lNNnG7VwH4IGVYEF0GKDEfUyQk9fVxYEAwBPSbRCMUMEV+PXgt4MgLMAJgIn3jRagkn02N/NE97CMls+Imvp0jtgisVlFiBEFhx8TAR7U+NqdjhFAXWfRBqwLQEGqD6TA1OAWBgQys8ic/sz9nxf3f/GL
Ip9As5PW37p9u/jNr38t6MZhSI8R+CH6jW1AtIFzIyUQhprFQ4BNtjhlM1mYGt/7/YEa8fEAYvDNycngiOhzJOtLOrg1wc53d3Iyecm+/RydLp4+XWzatEncs3EjV7xgwOTGRnFdb127zmwJzKel0JpKQ8YiYoEPjcESL0cgxRp/CaZoYos9B30HL5Dp+MKSJUu+6goLiw0F1rH8i23EBlE9AdCBmRodFc2+NiQnw8wDQ4NUF0QUAGZI0Zk6dQo
Dq9+kDgMzEP491PpicqtgCMxFBBPcXA4YzyWE2I8uJbgAlBlyoqugKUzngJ+gr59NSzA8rsMm8EC02B9oaN/HLNDJ1TQaB3b+4Xv/U7z++mvi9dde5UAEBCNwXLNmzeaoLVgv9stSXiOo9wgJZGZQDDKHQ72KQ8xmPZhZBgHl4LpGrqebA0V1tTViX8k+cfDQIVYPz8vNEauJ/V5/w/VBLJObZ/mQqqNzmtCSpUs51xJ+XbqB5dJ6BXSt4ukat1
tTaoif8dFRPt9oXaKrmykyHg143E1bXnnl1ba21gs0EQcmWusMBgXAALAgcgxgOnP2LJvFXObW18vpNQA3MLSFCxcyWCow5PQeAssL5y9w9BmVK9guPgM4AnwiCVA5GZm2AwBA8jZMYGwLgMgmMMRloULTb+gWKmCpqzMkwQCi8A0iWq4ABXmSAG8sBiP18XrIu7x1/Xrxs//zc3H/5+8XHjKvf/GrX4lnNv+RezFDIFal8Qy78DYMRhj4FxJAY
VDHMaDF6RAT2hxgGRkQA029COwR8Hpv127x+H/+J/tz77vnHvG3f/t34oZVNwx+F/uFKIVMUsc1Nm4iNrFwwQJ2B+A5Xfd8WuZY02nISCA2uNECxUnKFOUE89OkGnjyySfP3XXXXX+KjY+/PzIiMmci25hOphlScjCiuYLDxaBYXl7BwAUlaFRpDDcqKiqYJSH3DoETgCoSub0S1MzMCj4zSI6lpg3VVuyRFShKcAE9TjjfUQj2byIVCMwoilli
duD7HR3tXKIIrUPkQMbCJRAXp2qDeZ21628V19+4isvr/vjHP/KxOkzpM+P1w5p9hh7u7tcnpcPo+KCpGBQ995us6+GCLuo9lc/oYAaMihmYyR/76EeZmZuBFSAM1o5STKT0IPCFBHGcC9wYq1atEkePHmXBXWG0nEAUeueH+Lf7+A9XfONDKTcMMYMnavIisvzsMNtMmIAv8XFayuU5t1mwdQWBosmMbvr8/ff/4ac//vGKDRs2pNrstoixv2c
2sUXA/MSkQpQWeXWIamrD9Fw+duwYBzf6+nvZL9fa3MI9k0+cKGXxWeH2c+pIU1MLp8KggiU6KjLIlAfLQVI3cvFgUgMMkTAOX2M76x4aScuQ0uqXgRiAXWJSgmRiBrjk5eUyS0TVDKTOkH8JXykAJpo1GA2Zf1S+/M3ffJWBxNzXZTzAGApqyBdE5B7MDQITKBsMXn84ABTDAqJ6D37VGwm8kV4DH6MBvl4ORHXTNUGEHTcsBGMY9FW6j/T/3n
jjjeKxxx5TFsBMAmy0oXhiMk4SAiKzCfzoBBPM80cBy/GMTbT/Zy2ouoJB0cQY2xrq67e0tbenJCUlLhzrO2ZXI9JqmpuajNabaekMIi7XYCUL/HbtbW2cEpOdlcWA5aIJ2tDYwGYxKmHAYrKzMkQnAQbL79M2AEDIE1S+TSUYARBhM0/69DIz0kUTvQ82hDQbzHIf911pZ/bI4q1+P/sAUWY3CCZG0SFYFUAVaUU4HgAJKj0wADAQaoA7IC0tf
dRe0eN5f9CENqtt60E3Gv8ISdyDj8Ok9eDHRxMrp5GkjWvkYdEJjW5YTgZ5+GxVyhSuuWBT2huQFyueOUMM0A2iubExhraVS7/rtIH+/jOTfdIQSG0jYERVyyOXAIrjScEptwDxKgFFObpeevnPu6Kjo+fft2lTLoFA4jgBlQMPyF7Ec24vQMAEPyHABHlySAxGtzs0dELQAjmJmM52VoIeCJTYGeaciyc9JioHQEKCGYYPckD643T2M4LtIdLd
19cr7DY77x+MCeCgaqyh8g2TGqYmNgdfnl22TgDgAvSwAATDZeI4zFyU+GHbuu40Er0vGRRlvmHQ+/4h7HAkcDQzxEFfoT8gIoHrhUohVhCSEWnj3JyD28K1p+uL64frrslrPHfuXE7ybmlqstM1iXUYZvSZa2TubBuvX/ES9nHAgqgPf9gu9os0Kdz79x84T+bcrvq6uuPj/R4AxTBLE3lBkALsDL5BiEbAh+WVzaDADlmZRfezo9+IBkdxZQZ
8hg4CQYAqGBuAFmAGZexTJ08F9meXbAh+R9RFY/KDhWLbaFOA3EX4DgUnZfca+5OmMytaS71FgLZZdNZ8Poh6I+CTmJjEkW98L6iGe5jgynBgODS1Rg/Yx/4QV8RIeYojAeJgXqQ/cE7YEMx9Q1ItkvM5cey4NkPFMoybigq2YKSnZ/BNQbLJcPpO8rUycSZQzncpSdmW//AqY4oY3efKy0t27dpV+JF77plBJnDqeIERA0BjtCCwM0tzyracCF
5w5QgkwWjSoVrEweAVwQnE3PjdBAYw6fqkEG1ra7uor68V02dM531gmx6PXfrMPMyEoNMYFx/LfjIwInS7QyMrBFIQwDDedzDzVEwJzbOwH+gragQidjYhHQHlbAALFgSAAIioKx6PaEaoEMWgWRziYwypaBmNJQoxDHMMym3UhVoLgBYuq4Tgz+WWCj6vkSMJc1n6En0yCh3wjwaOJyAsHEnbyrrG5k+5sCpRLFAMYQ/QWKwOc7l2L1myZFpBY
eF9E/k+fIcIkEyZOoVzCpGkXVFeITzcQzlG1NXXGTOXJiI+T0lLY7NaAVX/QD+DEwIoDdzQycOmNnQFfdwFz8ETXYnDIoUE9cxoQ8riB9x8Xg8AJvs5ZfoLwBnszwwsUMBRAqwAWwAJN35CUnSYi5XDWTLN6xtkY+NwJwwPikHGspEOJANAMOV12aZhLHNZAWJQ+o4IzmcEO8b541qCCQ5w9N3HPkYEX3Q+D8NlwI3B5P4AqJrs9UIjVtdtVh20
Na55pshssbG5+cgbb7zx1leLiu4bzV8WOlAdkSyb1aOfCX9P6TD29YvMzCwupzP71tSmda9HnD51mr8PMEXqTGdnB0dNIRoBIQju8xITyxMXoAh5rhS5P2WesjwZ9ywx6p0Hcw/NogvGeihxs0vJLVS2ANQNebQeZqoAENWJLy0jzajU0UYDxLGBEs/BUqHpWHrihFENRMwZN4QwV1gQdA5rhuvD5DOGmOBg5L107gBHo2rIYMBwC0RFG6rcNpk
LqsR9MeAqAFDaZMaA7vdZM+ryjkXWJbgKQRHlfmRqtmh+/8F779v4THp62scuRqHbrLaDCpdsAiAt6HNMPJ2rTVBJgQmNulzURsPUhZ8RrA0VKZjqZWVlRhAnwsF1y6icMY+W1jb2JcI8R8AFLU+RptLOQrj+ABsKMCpdqtbI40SpobncUA1InAFcogMiriNX+Ywn2PKzH/1IvPrmm6Li/HlmbgDkz91/PwtlbLznI+JzX/jCiKYyBoJVSEhHWS
Uqc3BNzAEgrnIh8OPWBFyTPdSs95tKDBUTBXvEMYS7wgLnZ7fZrBl1mUER6T+IdluX4upiimBX7rPnzpV9/nOf+c2WV15dR5MOTdRtE2l5iny/PJpkGZmZnO9nCwGSk6VGAzlMaiRbw++FyY4SwJbmVunz8rIZDeHXOXPncOqP8nqBsuE7SLtBMAGPkCaDP5E/pkNF9Qm2AVABS4pmjUc/J2ufPFlqqGmbPGnDDZjoqBfGd/1jgN9YgIhrUkZgi
OPCOQWAl5gd9185fZrL9JKSk4fdhl8Gj5DbCDZ79MhRll3Ly82jY0w3tB4lO6+uqqYbTLMRUAoFWSlkixxRpDyhRYHZpch9tTWRQq+WX2PzZzz+xLZR/JHjGVsJGJEn2RaSL2mNKxkUQeG6u7u7zpWVnyVQeis9LW0DmVyRE2GMEJqN5zakUYbUP1oP9Bs+RwQCoNwNkxWfRcdEi+a6Vvb54XMIQiCvDn7IdK6IiRzCDFW5HMABYKiCIypwAD8a
wEaZl0o4QTWT3/Lyy+JCVSX7C0cbc+bMFutvXR9QBboI5h14fvDgQTabAWy4WSxbupTff/vdd0UHMb8TiLI/+ywL2A7xQio3AypTurr4poFRU1stTp88yeK4+QUF3AYWatvvvL1dHCPzHKlJg8Z4sGmOazpjWhG7NNRxsjAcd4PVXLRcM+0JJqCZeKmgiPGQ3OcjH+IpXrNVNJcFFGUyt2dgYKD1Zz/5yfNf/+Y3Z2dlZuZpNtu4Gxvl5OYaDsr
u7kAwAwwN/VsgCoHKCjj04bfrlL2hwdzg54LpjOAKepXApEUiuDIhoZzN+ZDE3BDBBgttaGziPESvjFpzwrLUSlSgqGqE+fx8RtvVdtn0abQ2rohsm/1u47p+wh+KQvwuWjmoEkaAW2C70rStIpAGYMaXl09of7ghwLeKZHb0wsF59vf1cyJ8W3v7iEANH2mfibGaXR+SjF9LY7x1y8P+OAQ05QRyAJsrVTbsAfn4CB0nEsgfxjFboDgxYET5X/
fvnnpqx4bbb99BLC6OWF/GWP6zEDOccwjBVhDNNXo8G9UTMIVVg3iIPKSlpxJIdXIvlBhiiEaStaE2DUBEW1Dk20HgFUCG6hJisOw3BOv0S5DBgtQbpAHZZPSVgxv0PnrJuDiyHCYWL1wopmZlGf2btaEGtHqFYAxYa3DkWRsWCgevzfCfoVlULNeHO1ki7f333+f3kYCOY86gc8L5j4jRtBkko6NtKnyKOA+w62TUetONBL5baD7CV1k8azYnb
4N1Y3O6VBwPRKpZYSiK++iE5mnKhX4+/8A1whLzxfhVskfLZ3zWBD5X+g1gI533NVFy6LicG5OMsXXP3j3PZ2Vlz5o+Y3oyvXaa2cRog5VYoMhC7EjzGPlzM4uLOf8Q33dziohbRBAbRK8U5CsaEvw6m7UIKuj06CRAheI12h5wnxKWA2tjAEDaTDoBal1dg6EpKDUaAXYQomhHriJMZmJ82C9ABL639Rs2SFUYTQx3Puo1VHf8vuCUmRERawwT
evmK68isfVucPn2GU4kgYqGYXmZWprjrjjvEA1/+cgCAdVM5ohoQ3/jlL38hzpw5R4w6RiyYP1+suP4GMXv27MBR4AawjPa14vqVwfmO/sGgClf1wG9rSt5WeC/V1fvosWGSg+ED0o84kbYBo4Hio1cJKKqxma7B2ske+HFc7g3SJOqnyXE8Mz1jd25uTgaxj3EntyIxOy8vjyPK8BVCObuwMJ8nJloVgBWlps2R/kFfoDcxBvx+lbIx1Lx585g
RIqrabLMzC0QEGtvMyjL8b+npaUEgBHFbMDyYj2CREIUwhCFChGD1QZNXsw0FPbwF751HCuWOBYBj+RWJdaOtqHj1tdfYFYABoN6wfr246eabg9rFhtZCYyBC/7Wvf4OA9TQDYbRsEBYIAsmUHS20bjoEEAPvhZrOIpA1ALmckx+meSdB6koeo9YuSxN6vDXUV8qAUniBBYoTGJoxQ9p27t71THZ29pR169fnT+C7gUdUhiBhGx31EHyZPn1GoA
uesQ6CJIY+4PHjpWz2ghkC2M6fv8Cd9FTuI5gQd9FzuYYFD+T+Ic0HXf90fVAjUTfl+DEgoNpFCtJ6vAYD5e9DTEGyXFS8QFAB+ZMw6Sd8U9GDYaeYgGza9OnigS99SZTs38/Hc/3KFSImJtbovBciGxYKikZfmkSxfPnyII46WOGis18V0XijKscua7sdAREN2wiM99Chg6KXzt38u1ljCBMcdSCibOrkdzWMfNyM6Lgft0BxHGAIP9et69fbp
k6Z4j158mTd4cOH9ixfsWJ6XFzcgvGwIjUAMG3tHTxZ8/PyufewUtdW7UWhSgNzkcUiAgKqgicxVHUAgqpONzEhQSSQ6WzTbEP8d11GVzpZkx0e0BIEUBoahpIX0geVVdWB47VLsxsDjwH/p8PJbJEZln7xDNGEkpwgjR7ON6xcyW9FSzNeVdsNB4ZmZheq3m2ww0GlbtwsEGnn0j5Z0aOCOj2yeZfBgm1G9Y7p5qJJ4GXzXdctGBwc28YLHLTe
wwQ02yRjvBoStiGdZoHiaGA4o7hY3LdxI1mTmpaZmWlLTU2x9RNy9fT2HSnZu3ff6ltuKSIQiR6NHZoHKk+YqaCqIjJCsjmjtwuiu0jgRuAAExp+R/j6kMKDydmLbnUtzdzwHYvL6WAJfgDecOCD0j6YpUhr8TM7NJgiAjowpacqRWz6TDWsN4BA4+NjUCQghCkN9golHS+Z9T73wITyExVrC/I4hjC+mLi4wKe6PlR/MWDeDgeGQYwyuFMgfgO
cA05MVfM4fU5T29TB/Zh/Lk0LSrpvoWM8bGFhwI+4aSJfkH66xTKIoyLbYI8JVygoWubzcGPJ0qXa6tWrtfT0dG3mzGKNTF4bsTtbRESEvaioyN/Y2FR95vTpgwsWLVyQkpS8ZExQkI9gXaxQQ/8gw4WBZu4obXMPeFhn0chZdDI4oo5ZIFgiy9Di4+LZ5FZqNZi0Bvtxc2kegi0Rss8xotp4jrppVemhkp7hZ1QHZoji2gJmZTA4DNYgI2UIfZ
MhIeaP1GUlzugg6B+DNQ6phfYPBcOAWewfvrfLSICI4AncCxq3FTBK9pDIbrcNlT3TOQg2mKeJNB59sCtiLy11Fh5y6spFJ1nLtJdHx2t+X64he09vHKcZjzYL+ZM1ReeiQHHBggVaWFiYdt3y5VpSUhKKV2wul9NGTIrIk81uo/+SU1Jt7gF3T3tH+8kdb72985777p1L74eNQZWCJ6Guc24gM7rmFmYySr0GQq9gjGCP6j2YzgBBRJnNkVgjA
t1n1Cv39LE5jgFAhA8S1SuohEEgp7S0NJDkDVBUwAAVatRVKxEElQfDtdNuQ0FHKct4pNkOxmi3T9BcHhYE/cO/NgU+RgLD4cxls3mNiL+Qghg4dsV4zWWX/FuYRCDUgAsDGQHSlIdC7bXauKpNAthVm+gspdAOENgB6B4bDzBaTFFwk3ctMytLfPELX9Cys7NtTU1NtoEBN4CQQNFlIzCxxcTG2gf6+uk9hy17yhT97JnTNa+/8cbuG1fffFtK
csrMMUzxIdQR5jAmHmqUEU3GlAYjRC+Xd955R6SyQrSN8wmhsQgWqPqXIM8QQQKAG2qadSnWAB8insPXCDYJn1pkRKSYM3eueG/Xe+yLhK+xtbWFgVQJLwRUZYz8lABQ9DBzHZBCt05O4cExARxlRtIl+Rb9I1BF/3CgaAJCMyMd4neU5+GTDbrAFhFt5z8KCM1K1W0hk9gBmH6pRq5Y45kzp9nNQPDp1f16M7Htcx/i3+7jEowe+gsBYOA4JlP
FB/ygsnImwQLFMfyGeJyam6utXLFCS05OtlVWVtrj4uO1uLh4O7EkAscBOy22SHukze5w2pDLTezDFhUV7U3PzDz/9tvv/PKjmzb9YiLAgMkJ0QFEkRFQARMDo0H7UrwH0QU6JgZC5DYiV1GX5h3m/oXz59mXGB0TxSZ2C4EcSgD7yDTGApBFGhAYY6ZM1UlNSRVV1dUMEAj2XLhQyW1MzSanm4CyTyrkAFRQJYMKEZuJFiKtpwd9TiYYgR4JAM
2MbVgTewQwNB+3eXuc0kTr4qYBFZwImfzO5jTdTHoG+vh5eJiL/bZDkIGuDa4TzpEMhXpiy6ebmhqbP+QJ/LCstnjkEvxcD1s1xUMG2OI1q9DjGA8YLr/uOq2woMBGE1/Ly8+H/WTPzMy09/b2IqBip4lFeIAHzUasTiNAsNGE0jDJEpOSyCYTbZs3bz542223lRKAoEdw2DiAOOg56m4BVGCOAJ9ly5YZAqiQyEeQRbcJv83P4q6VF6pEV3cXp
6KA8YG9gSGCOSJvD4o4VVVVhop3fPygP043lHfKKyo4cbuqplocOXw4IF8GE721tU36PSNEcmyscJjMSXNze1SgILdw5crrRfaULG61MBHXQSgTFCMwQhEChsOp24SK1OqyletBOreszExusmWXgG6XwS38w4DbAWro+A0Go85+UbJ3D9+EbIapXU3LX6QNgTT71sqcxfEwnNDxiPzuw1Y/lCA2LCxQHAYM161bB3NYy83J0eLILBYG+OEzO00M
MpptNn4kQEQxi8fj1nTdoalNACXxSKapnpeX1/HEfz3xq49+dNP30qHaMIERFR3NTatQ15xCoIU+znwQMOvkJEVrUgRRomQXPyjpAPzMIrCIQqPSBWZzZkYmbTcqCBDZDwiGRyYhwBRM8Fzk2QAAcTQ8Ipz363QZx8BpKCYAP3b0mNi9e5eoqCgXs2bNYTUa5Wu0DSOt5Rejl0AOxwYD/sTRZL5CwHAQTP2yRzbdKCIixX8/8QSbyvfee5+YM28
ep+aEuEwC+ZZKGgw3oaPHjrF7wJAb88Ns/otGnqXZp1jjRJO6EfHdLNNiHp5AqwFrTHZQBIJBOWXTRzcxsBUVFNqdLqcWHR0DnyGZyT6b1+thIDQeBfsRjeo+jQDRrzkcBkJIk4098E6Xy5+dnd1//MSxEyurV5QkJCSspMmWNN6DZKkw2ewejAv+Oj5eeIYPHmQ5MbBItDRAeR7qohsamrh3C46jg1gfBB1gBsbFxxmlfQA1BAhCWFYemePvE7
OEjxAdA9ECAW1YkSytmJSRzAzhWbshHkH72L1rFy3vCbfHy2zzhhtuENOmTed6YzeBqxfR3eGSm4dhg2OB4HDm8bDmtz9YYJYBTTfyJ3FDQA/u2zfcJo4dPyG2vLpFHDx4QMybN5/AcS4za74RQLkcgI7AixTYxXWGdBlcGah3JzZZ1t7eXnYF+MPAcB40geNETUCY4Psh1QXT/BrGhQQLFGksWLhQW7NmjZaQmKDNKp6thbmcdmJoMIdBCDmmT
KCE1+jcZvN4vHgfJrLNEGKwMzCqSegz2oQGpBMiwiN0AsKWI0eOQCyisKCwcNygGCknKIabcxW7WOoLzxEgMOpvjR4pYIXwI8bHxbKaNkxmBWZgmPAhKsDpl31dIO+PplNGy4I4Bln47pDug0DMnt27xZp16wIgweyQ+1D3sM7j3vffF63t7VxxkzN1iphZPEvMmDmTU31wHQDITq/XYF1O57jN6GB800dkkaH+wuDPBtN+wFhxPfAIVaDUNJRN
rhGJSSniEAFiU3OTeOfdd4gFHhFLly0X8+fPN2q48RvKqDuCVieOHzMCLEaEuo4YYzWk466gYIHK+XtIXFwJ3UMmk/rxaxAXrum+M46Fixbx3/y8uXO13NxcG5mZGjCQANBOPAOOJrBBu+7TbZqdk+6AfXhPsxnqCAH3H16qKhJ8rut6oJES2EZmVlbvqdOnj5I5XpqZmZlOoJE4kYPt50btPdyIvq21nQAujGtv4W/U9S6j7YDDzqk6kApzc98
RO5vOSLtR7U9xRJD0R7Mq+B4R3YZaNiLaCIygtSkAEiY09Azfeucdcf2qVQxqAEUAcl1NDQMi/HJI58kggFm9erWYS0ACM98vOxD2cndCX4BxaTKwMSwe6iOl6Oijg+GwvsjgWmVlwuM8kWuo/J9g3/PmzxNFRUViz/u7xf4DB8SZc+dEe0enqK2uFitWruTkeJXriZYNe/buNaXs+I+KiWkDfpjg+KiJNW6c4NfBlh4zgeM1oX4tbyTjYYqT1u
/ouPuuu4CBWm93L5yFtuioKCaGNJntXo+X5q+dzWSamDaf1w8wBPAF+Q3xnk+XuXnGZ8r3pOlStMGr+7SoqGi9qrKq4eTJk+8QAKXOmTv3+okodIP5IWqsy8hu9pRscfbMWS7hA7ABNMHMYLLC3EPvFgUIg13/+pl51tfVMYCjtSfArrGpiUVuk5OTRHb2FI4ow1eI9faQ+Qvl7WJigAg2AOzOnjkj3t25Q7S2tIrPff7zYtGiRQy8YKxKTQY10
fBzQvwWgAxQwnE6wKyZedlGBb9hTeKRmKHptR7kYzTWx7li/6jL1ugnAmMEOMIHy61j6XH1mjViAZ3H/pIS8dzzzxsiEnPmsAgtfmukMlWUl4ljx44q/2g/Af57tN0TV+ofuEww3kSTHaD42EWYhjDBt05mTUG0PJDnmT8Bf+yk1VYEZbFHRkTYIiPQoZLtZDjNEFBBIMVOgGIT0kAVmh/msYZ0G5iqUqlGM9e8KlBU0eeA6Uav8XzatGndJfv3
7XMP9M8gE3M+gUzseIUE4KsDqBhJ2272ay1cuJDL8eDngggDxBMgTosKFRah5fYCRk4hxGuPHj0qli5dysK1p06eZN3F5OQUVqBGz+ipBLTryFQ+S4CA5GRuBUD7fOaZP4q//9a3RSKBJvxt8xYsEJnZ2QEJrkGGprNZDwBBdUs6MUhleoKduWUEHSWDNvvo4DfseyP4D/WAfzF4dXkTY3MXxw0GbDS8d4ienl4WgkCtuBKCAEtee+utDJBQ/kZ
rBRUgOl9RQUzyoEzu5rdOtrS2Hm5pbr7iJcNkZPlZAoDHxMXJdSlNwcvib7wE0/5KWTWY3bRhPFiREeEe6iieEiAHARQ8RzJ00oB8pTBEebdcwMMqs5+owJrxmLJ2gBKGIx6mb9msEWfQGgxELmWN+Ax/t+6YkTb070gLlyhSY0VG1mzpzJStAAmIUL5os5xGrogDmyinXsXLZmZ0kxgCEr4XR3c3QZ24GpO3NGMfvMLlReMPyLUi7rk5/5jF
h9802GuUz7ePvtt8VRYkgwqYE8DMDTpnGE2tx/Gj43rAMzHLmVmhJL4A6FvQzq8FlysjR9V1fdA3U9qJe1IVOmC5Pqg9E4S0l6MdDrgcUsDhE4HgJD7E9FjZFbqdwbuD4Q54UfFAEl3DDUzc0vBWbhU1TJ2qgPh7uAWD5fE1xX+n0fpXWPXU1/8DSZH4S/UYyucziWv7H1KpAt+yDHpHYlgBE6yARyEhA66Q/cabNpDoAh/IhIvvbpOlyHbC7Te
qbFKxc2FbXAIoGPwZCZ5OBr9Z20tPR+YmXlz2/efKC3t6dyogcN8EFEGB37oI+YlZnFLEiTDerh20QeYnNTM7NGqFcDiDCRly1bziA54HGzuARy8BAQKSosFIVFhVJJx88Blpkzi8XK668PsKg/vfCCKJNyZEoUQpgaxXd2GqrgiHIjjciv8igJiNAzGszR6DdtY9B89ZVXxJ49ewhwmoLALxQA/SZtwwAIitCv+E1tA3TOr3zvvfcY/EH+VSMt
RNW9UgHHxhJtRo24Yer3Gs25UPYno/2KxR85dIgbZdm4DFBDusGR+oaGw/Q7XHWlfUi5oQXA+OBFbkL5G/dL0/NaG5M6n9PmcIIM2hQQ0uLn1BtOQqSBgIlPASGYoFx8ihHqBH5cLkaskBYZYBlcDNCQIGm8B5BxhYV1u73e0wf2H3jbJGkftIw00Mt54aKF3MkP5Xnwk2HiwzSFb7C8vJwZWL9sHg8EAzgksKRYM8vvD6AVKoELer9AtxGfu0z
lfAAutBaYQWxQBRrOEQifPH2KAyvK/4f9YntgW3AUwD+JZHGblDnDAtMeQKZyJhHtRprQn7ds4W253V4GK7DHQWHXwWUoGxzKCgOmsqxhRh8buBf2yBYGADjkIEKYd0C2SjWCYzY+LjBGn+xFowRy1W8AtwKuKcBb1kXjy7+i693g9Xqv2mbPMrKceAmTXPkbN0+gkdVVzxIne9DJRoDH5jGYIdJrEGzB+7qRe8iszBNghYOLjytJ9AAQ0nq8qF
Ixr0+yRq8viD2qhdiJl0Cx7s3XXy9pbmkpHa5100hgibSaKVOmsi9Q9VSBaYwmV+jxAnBD0KO3p5f7vWhSBxCRU6yHTnsGCBrCEqh91iXI4Pswf8H44MMsJhN97ty5DB4wr9/bvZvY3fscnVU9nZXP0CkFKQxANAANHQnh1wQoIQ0INUn6d//3uOikMhHEEdXE+YsFgfQrVglWhtAACD4g6wR0l5cVN6pCTJxSsbeKH6xCfbICDdBseB+nB1L
fmc6RgYGPsHpc0UMKJ+XAleKD8ijmk3MU5U+cCPS5+5aalsbW19lcCz+2qfAPCN0QKZr7WXEDyAv7FV+gkn82i7BHZ99YAiyvKkqYvyPDaTVbTSMHmZKRpsj6PMcpHmsdQfDHymmCGnfngNoDT7zHQDTGGS+unznvNVledo0m3z6/6LUihtbiWga2xixsXCsgRAMBUBnIiuIigD5gNQgg8PTd9TCRTDOUXHkAFDwrXSaMQjWB8ky2DqFpBZfeMN
N3DJIBguKlbe27VLHNi/jwEJg3MgaV1V+QGA1bltqqHOHQBL3pdH1NXWiXd37hSrV63i7SPya8iOAQANHyOOHYDkcXvZb4oe0O/t2MHtSfsZaH2yZ41nsCZbphYhKT03L09kZWdzytFZafKzYAVyJcGqZdOuwb8EGzNuxWYxsL19e/eII0eP8A2Fvu8lQKynjzZX03CD4k6SAfZDC+o5LyWIgpLBMhnpnmwDPtjF10JHPxsxDj9NDj8Bld8nZa+
ImWhYDMCk5zrYnW6An8kUZtDkxeRT9HrlZ4MmtPqOXwGmbnyPJqg3Lj6hedtbb+3u6enCXcjHrCtkGW10IZ+utoZ9YvDjIUeQVbnpXOArQzoOq+wQS0K1yqxZszhPEIxNdfMDa2poaCQW2czAjugwKmMAHIjGLly8mOufI6S69onSk2L7tu3iVGkpm6syeZ39gCpwAmaHbcCPCcEJQ6tRiIb6Rg7YAHBvvW0DS5cZZq+X8xTDibk6pOmK7QHg4O
s7ceKEeGbzZrGDwBSRdaN7oWFGK9AFE0SbBPxWALciMv2XLlkq3ty6ldOV1HG6wpysnQiTPXDDUjmU0n+Kv4UL5y+Irdu2ida2duVLbKVtvEvf++lknRBSHGLxJQQTVMngVqlROBnYIVKRFl8zLU57+/p0Yk0+mpBG4YKdxfECDj2AmjlZWDfYZNBnoe/7ZLTZWIcjpUM+47w5gz0OnD17tvrcubLt06fPWBcVHTXENzMaMM4kkKu8cJ6TscEQw
QSPHTsmfFOmMIgB1GJjo5nJwSSF35GDCbRNNLICCMCEzc7KZPYIExzpNJ1kQoOhQZEbgLpyxQpmogBPiKseOX6cgzTfeehhboKlzGgOugAoJXOOU/XV8jqhGuSVV14V3/72t/mY/FLLEH5GmwQkdX3QgwUgjKgxfKVHjhxh/ybOwUHnBaDymURfAYqIritV7mxiilj3iSf+W9x+++0iKyuLwRPReThakUOJm4kmAz+DWpGCWfX2t98iQGzj34nA
HRvdR+b8j06dOtU4mSfFZRCZwFAlg/BbPnsV+eHM0mjbrsU6cO2h7zwUQ5PTQYwEM4XzFuViCwVFmIR6EOAZn+lGtDTwPligGAZIjck/uJ7fMBNt3V1d9rKysvRHf/Bv/7bihlVzCKiGlZQZKcUboAj2B7BCcAHjljVrAvvwSZ+bR6aonDhRGugP3UnmJVjS1JwcXreCQFGxPUSskXqjzh8KOY/8+w/E6TOGIAySvRcsWCAe+s53go7HKxO3XSZ
TFOPwoUNiT0mJaCcAf/i73w28j3xBlukKDwso0SBa7XRwQErUVNeI7W9tF7/97W+5pvprf/d3XH0DHUd8D0CpRjuxOpfsuQKQhUvgtddeY1/qPR/5CAePAr8Lfbeb9o3kcnNgC+C/d2+J+POfX5L+SCHmz5t3bMDtefypp576hbCGNSaz+UzmmpfMRC/8Q7pP99Jk8RF46PRa7+vrl75CH/sH9aEBEyNJW4KjV5rTUsAU3+WgDLELXnwh5rSqjY
6KivIRAPXUNzTubm9rrR0RwUdauPKknvMPkVu34rrrBj8D0yKAulBZKY4Tu+PG9/Fx3EMZPrTEhMRAawIGvpZm7h8Nholm8eYEabRKRbJ4LpnhYFXISTx08KD45S/+L0uFqWHIb0UGAJJbqNKxHTywX1QR6N57773CxJx5UXXbg+97jXz5UaLwqhGXR/o2ed8wq4U/EF0GYG4gM/3cuXPEjOvZZFaAiP3GxEQHA2JzC/tLt2/fKmTFkrh17Trx6
U9/Zvo/fPe7N/3i5z+fbk0ba0zmYb/55ps1mGGYLD4JaASSsr5ZgZ5hWisfoVHV4jf8hvI5HnXDb6i5ZcqODM4of6KxGCYzm+jyNQOn3e7QT5Ye748Mj0gvnFaU5nC6wkeKPocuYU4XszoAGRK54U9To6a6itkSa//JFp5xcbGBwEN3VydHqMEmEZRAs6u42Dj2RzqcjqD9YH34EDkpmh472zvYh9fW0SEa6ur4O2izqvL7GGykuvXePXtEO5nl
EKxdvGRJQL/QLdNjkF8ZULXWjbajELaA7w/pNRUVFeIQMU1U4ixftoxdBSoiza1JVZBH+jPhi1T9ZOCnhE8RDNBF1woMV9Vgm+XMzpw+Ld57bydXsnR2GB0Op8+YIW5bf5tYuGiRPSUlxdXW2uqj9961po41Ji0oAuy4PWeghpkTjg3GR1OLAMumgFDIyLRPl2BoAkldJmebmKAwAajwm8FUrWMK1jjsdr25paU/JjoqLD09PTUlNTVjvCeBplJ
gZvAJsvo17e8CmdTIK24nkxcRYDBEKOAAIKGliNcwNXEzCOeaZU1Ki8Vzzp5S0QYIwseo1HXga4Pit4A4QmsLvwaAdRBrbKPXiOzCtA0CRmEIUAC0Z8yYybXaasCvx+0UkGQNbUKwRPSQ9hrmNMAT0fDhQFF1HwQnVoCKGxyA1kgRGky+TkhMFAN0nEnJyQYo2mxBgHj0yBFRUrJXnCJgBIsGYObl5Yk7N9wuFi5ZzMdOI1zX/VH//P3/dfr2O+
6osqaPNSZloOWdd97RwXpQ0cH1uHDSE0IiQZsFZf3EEP26jeBPY/1BU7KwNIEDvjtziocutRQN/5Xhw/IZkU5N9fswr4NBbK+vrLzieMmevQX5+QX5YRER8RM5GS9HkvuZxVVX17D520FMEIwLgIf9AsCc0ufGCtMRYQyoACcwTK/h9yQzuoWTvwEcAFfoKaJsEGAE1W3UU/f09bKCNUxTgO2Ro8c48otzK541iwFIRXQhJYaBfQy2TDDMWICXY
o5CpkOZgy4jm88G6PpMted22VsFgOlTwrayLDFiwUJmjWpfKshTeuKE2LN3DwHveTbzkbuZM3WqWH3zLWLNurXGTcAYkQWFBTP++d/+7TO333bbod179vTTeVvNnq0xuUDRbyjE8h92fm4uA4TDznXDMIMhIUbsDnmHfqmn6leSYWxm+031skGRaWkqy8+YLbqNqOxgoAX/dL9majDvu1BZ1XjgwIHjCxcvKpozb/51EzkZJD13Eggi4IJIM0xZ
BBgiwgwABNuKiopmFoQUmClTso3cQfpeVGQUJ0r7pZI2or2IQEOXEUAHyTL455DQjQTvrKxssWFDimhsaBBPP/MMszmwzhOlpbwuvrt8+XUEjIZspBJtVcnXyp8Yytj4PKSi9VgyGcxE6Z85xRM3KbBV3fADB9q2grlCYDewHlKPentFeVmZ2Lp1q6glcHd7BjjpPY/Y6H0bN4lb1g5bwRZHN5g7f//0H55YsngJ1Lb7rGlkjUkFihK0GBhRLwy
GgDQUYo0a/fHDJPbLSew0SqCF5jHJg0mfouoOpxogMxDKBPAgsAxlkbqPfYryfR29o3sbmpuPbXl5S9rsufOXgmiN+2TIhIQ/MD09g5ONwfBy83K50TsCIWBBc+cY1Sloa8B5mW4Pm59gVgAZo6eLi81FVekRHY3+0q1ceWKzORhYNZtdxBDwwEcI3+NLL74kaurquPvfmbNnGFTdXo+4dd06AiRXwJQ1Ov4ZEWaAKEoC/bLVqPILwnSOiHAGtT
kYCRQVuJrZMtBUpdco5hi4EcmKIwSJysvKxZ9efIGVgri3Nd0MUdYIKbQFCxfKG9fQy0xAnuYKC/+2K8z1NbrmNbRPvzWVrDGpQNEMjPCRwScFv1h6WhpXnjBDhJ9RmmzugQE7TWBNms2a3wSEPNmN4EGQaWwGRiOSrQexRQWWDofT39rW1nH06JHSc2fPvDVt+oy14z0ZpK9gAcAh8BIdFSlqauuY4UEjMS0tXUTL9BVu/o7ACQEXanrPnSsXx
cUzxLmzZ8X0GdO5BzS2gcTuurpaMWVqDkuUIUgDUItzDUaKwQjhK/zPXz8uSk+eYhMdJYF/+P0fxJtvvClWrbpBXL/y+kA6jBrmgJBikEYkejAIY1wvT9D3UG2D3ylgRtN5qNdG7bVrxGuE/tn795VwGwdEoxWwwnVy/YqV4o477xAzi4sHgXcEy52We/buLdn8V3fdhfy7FmsqWWOyDC1Urw/mclx8vK2woMCekZnhiIuNdTqdPMucBFqQE3N6
3G4Hay8aTazYt8gN4CUoKlM6WO/Pryl1FiPqHPyZ8T4HXuDnsqGHy5xZs5b8z+9//1vESuJGnp9DBwDm3Xfe5tQZ7u0sdQT9bKIPql53EPtDWSAAGlUbKJ+LjoxkXyICF0jOhsgsxG0hExZgZMP0VdHZD9kq3t+9i3MRkS8JvMH+0LM6nhgseqIsXrxYFE2bzuAaOsxJ8mZ/IhKw2xE0omOEPzMtPZOZp/l7av2R/JBIR0KeJI4LPbAVAGMkxCe
Ij23aKFZcfwMnoruGaWk6wnU+edcdt//tNiRRWsMak40pmlicn4BRL6+o8IMpwfR0Otk35eeUFFkTDa1E3TCbA+1M/SEmtCwHHMoWWf1hEECJCQXe140J7ifm03Xs+PEzJ0+efH/27NlrnE7nuLvKY8uIsLLYQnS0sEkgRO5fS3MjgZ3hBuvp7hIx0THMrlDzDB8jTG6o2iB/DwAEkdV0ycBUTqEuJcHMwAjQTUlNEdddt0Jk0HdOnTopdr2/R3
T3dLOPs9njFf1Hj7JfE9uGLBn0IPPy89mPZwa0UGAD+01JSeVFMcPhzGjzQGJ3bW0t50VCYg1tFVi5vM/o5wwfI+q5F8yfJ5YsWizmzl9AYJs2dg8ZM1202fK/9rWvr9mw/taaV19/45Q1nawxKUFRASOwpbCoCMnc/sSkRJjWfsUujIZVqNf1OXSifIxjYJ2GT8tc8cJCsyZGE8wkdaOXCPIdA+vI9Wg/ns7Ozsb/879/9taPfvTjpSmpqQmI8
Jh7K480IKqKBlIAREx+sNhuMmdh/uKYUB8NE5QlwgjMjPzCeDZTwRpjicUBqGCGRkfHBExZroqR0lwykj6khSjUaeJpW3A/xMfFizNl50RlZRU32+qSArQ4pg4CXoAVeslkEohmw5eblcV5koj+hoLeeNTJ4RNG6k89gSECTGCESBVCig38l2rAX1pYkC9mEzDPmT+f68FdUudxgiNszdq1d/b39Z2/c8OGypdffbXXmlLWmJSgqEbZuXN+RISR
9JzATeO1QKWELPkDGgwm5HFzJF0o41JFndXc9g2momiqNliZ8CZGqXIc8Xnf3pJ9pQf27z+w/LrlyxKSkuPgCxwPMEJWDAORYvjboBOICDEYHxrA19XXcU0z5LLgHYC/saW5idNP4F+zy5xBDAAhcgkR0cYjC+v6vByV5pJAnKMpCg8wRT4hFtQrHzlymIGRI9oEzji3xvp6jlzzsaakiCxikKkpydyEC+lCaRmZQbXIBjgO9fShmgVBE4BeY3M
Tlwc2NDbKdgjBvsgU2n4SAWJ+foFYumyZWLJ0CVkBLj7/iwBE5Zuddcddd62uqak+Qi/3WFPKGpMaFGXwBY8+VISoLnde1kn0yt4tRv8W+AOhx8jfCwROpF/Rb/gLvaYoaYjQhKbK4ULYpJ9ArHvLK69sSU9PnxIbnxhNoGTXJjCBW1pbWFfR4TQ6+gHU+qXKdJgrzJD4gg9RpqxERkaJsPAIBiBVCscpSnTedrubK0w8nFztMHQjJXv2qQ55so
pFDZQdEuMWJ44f5+RrmM+GEpGHTVxcvVY6RjA8I8XGQD9012M1n2HM6gBQ0iMqVcAEsa3Qz43ouQvtZfkGgEqapcuWcj9qqJEDDO0BIQib/C30iwHGJUlJyXfOKi4+faK0tM2aVtaYtKCogBEP0P0De4ojYCSwYEBkpgi7mlW3+T1pWhvAxgKzugkITak4eB+fm81qqZyjmc3RiPDwgTfefLNixfLl5dOmT0+LiYsPaouq66MLPyclJrJ8F7QJu
WsdmcoHDh4SublTGQi4nM7lZB9kTEjww0efw7wGcNhliSBMbJi/sg2DAXByCYCjSnCX54BUnmXEzNBovrauVpw5c1ZUV1cxg1SleqbrbbD0sjITO9SCAC+YPQaXISLBHjXRqtUA2CpEcpctX2YkcEdEcqkf0ohCAdHY3kUxxrz7PvqxddnZU/bR8xetaWWNSQ2KJlNazyCzc8b06X4wK2JwflSJoK+yNJeVmKxDyYOxmRyMsJoSRTWb15IlBr5j
CNEOgp3T6ezcs2/fG7l5eSk33HTzUvMmbbbR0xiTUtJYkBUgDLaEPioIdEBFOoVMbJTchUdEDguuAEJkn/hZOssASN3nDCjuMEASqIH1IYoNgFOtRLmFgB7cYwXVJPn5+byogWZQUO0BWCIXEhUyg0TQFgDFIYCoMVQGMUOY/ZALQ6Qc+Zi5uTkMfkplHAsCZwimGBUzAE9DFPdSB12fWTa77Rt0LC/5x9uz1hrWuJpBEaOutlaHr27OnNlelgE
j8GNRUmnOYmpKQDSqXmQDdZ9M9lYgGIjg+oODMMqEVqRFlqFhH9633367LDEubjcBY1xObu4QpZbRJnZjYyOnsyCggKTr6OhIBkSueZbHPiq40mdEi5kpww9qp+N0KkkyTiFyS0D0SMD0BUxvFYwJNJ7Sg1khhGCnTp0q5PXkR0SJG+rrjHVD4iuaCAZG+ATjYmNFOuqtCezgIogk8I2IjOLSRhagdRjAiM/tDkMB3Gbark279D8kOs/+WbNnt+
94951MelljTS1rXBOgiNHc3KRnZGWKwvwCBiyV3mEQFmOwZJjHbdcMnqNEIgIgGAjW6IM9o81BGYNxIs2HP2EMGHC7ew4fOVK69Y03ir744INDQHG0iQ0AxLYAjgSo/B7UsB324YFwOIDllp64YLJjIAIt3EOZjtUwpw0/I8DR6TUAzicVa/wyUm1uYyorgLglazgSzsVgj2yUICIiHXoYyr84COSGSY/vIxcT/k7D92n0dXbKvjAOEzu0j0PNf
IgLZZTAFv0N1NfX1e3asmXL9j8+88xBOqe29/dY8RZrXEOgyIyxplbPLygQyUlJPElVDxRDodlm0wfbD9g0gy2awS9IsTvgT/R5A3Yip+pwkGYwt9Gm2Xznq6rqS/aVHL9p9c0FhUXTZoz3eGEio0kUSvpUDfKopuBIZRwSRJnAIZ5id0jZL6PBPEekOTLtCiSFe6Sv0VC0kSa17GVjgGQwe8SAzy9S6jEOBWeNAVqBI4TSkYdpl+rZEK2QtetG
03u0fJXHJwab2E9ohIIofhe66XU3NNSf+M3jv955orR0d01d3bGSkpI6+njAmlbWuJqHdrHuHzDCwqIiLS4uzu409O1dNMnDoJqNR59OyOAX6CUNRQKbPmgqmxpZ6ZraPXrBqAlnjkCjV0yAlXg9rinZ2VNv37Dh5u88/PDHyDQM+0tePIVl8EeqFCOl2q1SdozgkWx475PA6Dead/H3GBz9Yjy/gmKKBuMzAivw6Rr+TkOk1m4bDAopMNQuMt1
muEHnM9DW0lL1q1/8Yk9nV9ehrdu2lZw8dQoyYj20IE9xANkK1tSyxjUHimoQMNoSExLRtcnp9rhdAEViMuE0+V0EerQIbnNAbMlupPcYzM+nyzzHQGWLUCWDQRFoGXAJHKfb44nMy8mZ8/TTTz+Ym5dXdCVdTO7ix8K8smezDBipXEZzNYzuN5nUul+M+TvIdB1DVccAxMHFAEYWlZUCEMpMZtar2S/53Ab6+3oaGxrazp09W33ixImdX/vGN5
6VrLBXLn1ycfv9fktOzBrXlvlsHjRJdG6nmZkpe6CgkbqUxZJDmcwol2aFHZPOojKZIRJhrOcLmJOmQIzhk9Q56dvX199f98orW7b+9Ve+km+z2e1XysXkptlgZyYz24cqGVkS6De3epWpO75hSgbVDWEYdi6DJFogGm4wR6M7oepvPZaPdSLRZrrcPvfAgLusrOzoW9u2bX3ltdd2bd26tUkColsCoXruE0JYkWdrXNtM0WxOw5+YkZnhctgdr
jAaNEHDidWQKa2TKe13eb0+J61mB+mRLVMDoCirLwIMkcUhvLrZcmR2JQHFRTvIOnjw4PeSkhJzHQ6n42q54F6Z5K0bCe3DAuKowCvzCm2aCIqcX44I8nCjrbXtzGuvvfrcb5/4r93bt21H/xyPBMB++aie432fxRKtcc0zRROzYemx+rp6T1paWoDtQEgClSnSJFaLTTWvEqakE9nWwCibU+b0YP5g4DPalm7zelt/+YtfvvA3f/OVL6empcdc
NRccLNJEbieaI/hBgV8IO/R0d/c079q163ff+tY3dza3tLR0Qzl3eCD0ykW3ANEaFiiOAIwtra2e+Ph4OYltDHuQGfN43IhGg+M4OAAjlbzNidoqNUc31UUHgNEfCNL4B9zuvmeff+7k2jW3nI6KipoZFR0TdTX+AB8GyI13EFvva21pqdi+bduen//857v73e4L5RXnmwYMscYBEyAqtshgKAHRMputYYHiCMCos4J1Z6fHHxPDVSQqwEImM+e
EyGAL93+R2oqaMo1FsECEYTZ7dSW5H/jM7/XqZefONT311JM7IyLCUxcvXRZl/ZwXP5qbms7s2rnj8I4dOw+X7N9/cu++fedDmKEZEL0mQBQWIFrDAsVxYGNvby/A0SOioxkYfT4/cvhsXJLhH2zLDF1FE1sM+A51k5yY1+c1i0cESgHpsf/d93YdW758+fxp06YnxMbHx1g/6QTMZLq/uAf62l/f8sqBA4cOHjh67Njh02fPnT996lSXBL8BE0
MMBUO/BYbWsEBxAma0kKUoBF4eLdaQ8CeTl6xotqCDetkTU1RCtUG9X1TXO3M1jAJGr1E66KcJ3Prujh37c3Pzkm+8+abiKykafcWCoe7zdHZ0dFVXVdft37//1HPPP//fBw4dQmVK7zDM0G0CQ58FhtaY7EP7oP++EZR2Op22xKQkZ19vbzgi0vQe+geH0/wKJ4h0oc0BfIxmYJQ5ikF9Xcxs0fQZQDDub77ylXt+8Mgj62NiYuKuhR/upRde4
McZ06eL6aaeKqONra+/7u/u6fG5nM42u8Pxzrbt21/+yU9/elKxwS988Qufp5sKgK/l148//hsR4jf8+te//hBU4uj54z/76U9HlQj7xje/uYgeVDvAMde3xsQHXeOHzK/pGj86yrpo3P2A6a1ttP6BSXhN8De3aKzrEfIdKLRsVH+rH3gqi/Qx+hvq6xnRYmNjodXNk4wAUedIsg3NsZCy47VLUNRMbFCxxZEAHJO4r6W19dTJ0tLcpdDo+gCB
6J2dO8e9/rSCAvHxT3xcxCckhv4IE9rvpz/+cbFwyZKg99Rx7Nm7V3z1K19h5e7Rxq4dO8Srb76J6+kgBn765z//+aNiMGDCoBgTE8uTzK/rb9PDL+S1DQRR6Lj/XW7uAXq+aYxJhT/MR+RzJHq3DTOhHxnH6ZfjDxXboP2VjwIOj0zgkgIQ1l7iNnBOD14K2Msbx2Za8sexOvaDa74t5Bi2qu8DEELPy3xPVGAhj/tAyLHg3B8a77nT9zeFfP9
i2FU5bafgcv6O5r873Ajo84fHsZ3NpmvzrE18CENOKgChp6urC30A+gkQ+wn0eBkY6HfTo4cWn8fj8Rsirt5AUreZJY4w+v/88ssnHvn3f9/b0txUe6Xctc6UlYl333n3A91Hv9stnnzqKdHe1jriOqdLS8VzLw7KHPb19TUKoyyvm5Yu+dhtYvf8W6FcbwRTGZNwfyhT+YBGvvwjL/uQ9jfesTGEeV3MeGicgIiRICevmRniJrFW3jgw1tA12j
wM+IYC4uPDgPNEru3GK+y3GPH60nE+MMaNyQyIH5xPcSRgxGSjBy+EXYUhLxZQ2AETlOAX5G8c7+Z7aFTX1p55bvNzex78yl/f80Gfz3DsTY2D+/aJJ59+esxtxMXEiO//0z9d8rE0NjeLp//wtPjrr3416P3urq6mgzRefOklHGiArtogIW6AYqjfUKHieP2Gj2BChbKGixhDJmoIe3hIggL2B3bx7MVsawJjbQgjCzXPtl6mP6OEURiPeZ+Y2
I+Fsm0FjPT5WhNjBGA9Ru8/aALENWNcm4TxnLvcXpncT1vIcWhjMPiJ/i6X43fEeIyO48BwVo08vo2h73+olSAqXUdFmk1SWqyoY5IPUxfYNgFg9FZVVzdsee2Vkx/9xMea4+MTkv9St6dUmbz+oewrOZlBEawU5v3dH/kIWrW2lZ09e3L79rf2nisv/4jd4Uj0ejylDqeTnY92mw0A2CuCgygTSbw+ICdGgpyE+6VpV365zw9+IfxRm4DoIWk2
/qVG+Ye9QwkOj4/yeSgwPiBdNPkmQHz4coCM2dy9wke5/Ptkho3rY/77pNcbTYB9wMwWP/TyOAWM8jmryEjgMwOgGQjHG03W6+vqek6cKD3/xG9+u+vr3/rW3X+pX0M1pBproNPecP7FDevWibXr149rG7euXSt27NwpLlRXs5+xu6fnTENd/e4TJ08cdoWFrw2PiMjVfb6q93bt+sRNN910mO80Bij2icGI8kQrUfBHtMnki1kkzelNo7GMS5i
I28AQ5SRfNA5m8NhovqvLYM5frtFmMnvHw8w3jcSShwNG08cPjxF0MLO+rePweW+Tx/JBBs8u9XfE38ujpuuBv9XFJneB2vbDJl9kgImJvwQwysmonPx9YlBtxVw9MdEJ621pbW186umnt7c0NbV4vZ4PTMIK5jH+eIZbxmM6X87L+YlPfbIvOjKS7y77Dx6c9vbOHbu9Pn8eAeLteK+qqurBw4cOBXytUDKX1xiBFg6kTDTNBpOQlsUm1pYgJ9
QH5Wsy3+XzP+BrivPwD7dcRtNZyEk7Eea5eazfJMTHOB5AFNK0fHQCx7FGXLo/9cNg2NvMoAf/oYzCb5Z/r88Od23+YkIKKiotQU8Bnz9kMR/neADc39nRMXDs2LH6ne+9t3vVqlU3JSYlRU/ABL+sA9HnG2+68QP1Kfb29XU1NTWfyczMPHb6zJl7NZstNj8v7x9tdvsUfN7X2/uvL7zwwh55k5EuQ+O6X458Q/gTQ3xHj0i/2+VmjItCJv8H7
Ysaazw7mkk7zmsHMBqT+Zh8inxD38JWPcZgKtZ8d5LA+bAESMw6f4Qd+YHrxM5v6j8ib6gPQfLpLHjmv44HDf+Yuqy5iSvD0SuEYCRW0CzFYnk7z7Jz/96TvZWVkzY2JjkRvpvNzHbg60mFN14OP79P9n71qD2irT8EfkIhTG1tFW3LaLW0ep0rEq7Q+dURnh587UGa2zo3ipOvijO/rDXXWY0Z3dHR3XUflRV6CO260dp1yKQy1tAwRaueSC
yUlCS2tItoGkkJRsA+Tkck4umA+/r3ycJgQKSQO+z8w3oec0J+ckOU/e572++GLCFJmVglqjOdV95syhtrY2597nnx+MkuPnlBBDweB/6+rqPiEyeW4I9OIDKUv1+9Ff4HLGl7VsEP/PBka6JRvXgg0SwteiFfadUqs3Gf7YGziX8mS4P9LAYqwikpkSonsh+S9Lh5Mmcloqpb1k+dH8jiyJbmbckSek0WhGlCpVj2Ns7Eqyzx8HN57ds+dXf6L
LhT6rqZnNC0wWnE6nhv59tufHHkWXAne+5hsbGpqxZUjeVO7AgQNV1B2R7G7Y5GYqJcSxUjfpoyR/jpWN9Sm+ofCPyl7GWrUQkl42ARE5bkmB1ZXoXDrIubyL1i72Mm6FqoVybNOmDyGR0yF2k8RixI/Z1C2WQBLPiKLIH/jyS01Bfv72Fyor70j26ILHn3hidq7yocOHZ3MHcV7gsNmMXtm3L+5z4gVapMCBl7LypwXP9PTIqZOnWr6qqzWW7t
r9Hd6XnZXNB/wBnvyoiFHL8F/79+8XBFFsRHMtvWZSRCD4S1dKcr+WQhyxnOoxv9gJ0nEWe6wlBV7wa0aPiQm/A81FM+tp2ssNYqnBjdnnJMmiXGrAB6HkR+FX9HMkbgVMjI8m+g7JUHohQm5kajUGyKLWIk0hWUyH57DJZHL29fWpB1SqlEgTXG6Hq0uwhMYwnDuHPv7oI1xet6zj8l6vS37q9ImXX3q59t91tZ16vWGI7svKzpaOBBCiFuLHB
+vrzegmtfQieYvvrdDhmsixbl8EISbzmrQSSxinvfyUKDk4wfGW+h5VJenyPlmihd+Zaot9pT7DxfgpM9Kttp80i5ARKxZbhrdGVy55pCub7E9kMWY99PDO3/3pub1//PNbb5XnxRuRl6a44nSYT/xw4jyn050ftdvOHf/hxBCai8zT6DH9oQiiuWg9NG0AAG4QadfGn1a+ILbCYgEORQsneAcNnN5VfO+9ZtPFi/ftfOSR4nT/QCKRcIT38M5B
g8Gp47iuow0NPf1KpRXNVZ5Im71Syxk62AAAa5EUKTHim5ykjqBE1mACYgxcGrVdOH36dOH9xcX35Obl5aTjNWMyFEVRcF+96tbpdG3fHjlysk+pGrOPjnqRpHED+TclROh8DQCsdVJkyDEiSddBaH7wJWMRxBjSqNWOnKwsrryiYmvprl2Pp+O1CoLoHjaZBlqPtzZ88MGHRhS7ryErl2FIFACQBGSsBgMD92QkxIcXtvSojzGXLLovno9Rtnn
rlg2P7d5d2tDU/E5akaEYnOzubG85dPiwvK3t5Eg4Egn4fT5/DJnMSmWQyQDAb9FSlFiMsXIUMyRWJIpDjJErziv8+QsXzV0KRftTZU9VyGS33MyRUZEoGU4Nm35WvPfXv5z9n9U67J6cdPE8z3auYaUy9RnCxDwAACzFa9YiDapg0ssmFqM0Mp3NWIzSdCPZbetvy3v4oZ3bW1tb388vyF8XJcaUpyT5fD73ucFB/T//8feOgCBYOIPR6pqY8M
SRyhBRBgCAFBMSYwaxcDNR/HQdKqel1iAmy/UateadB7YXP7iuoCA3hWToMuo5i1qpGlRpNNqjjY0qFN9nyFqHQIYAAMjnuDKarZWOMKQ3E0daSy1G/Bzf0aPf9b6277WN9xXff3dmZlZS3wNBEDy20ZHLA5oB49mzZ3oHdByn02onUfz0mhCSjAEAAABAignJMUqMYUIqcQ1LVjoz5On//IsadcmDOx7YuGlTwR133rkhGaeIpyv4eK+H4/TGf
mX/903Hmo2cjrsagwxZQoyAVAYAgBSXS4xirN0SUpSONvB0KDrPbN26pfDpiooVJ8VQOCy6r7ov9StVR555Zo88eqqx0mvYMQC0bBHIEAAAUlw2MYbQ/OYR8fozsj0ZhebmZvMfior0JSUlGzYVFm5eqXMaGbGqD9bVH/uspqY3enqB6PKhOV9hrCDKDESUAQAgxVRYjKxkZq93dnswGPR2dXdr79q48e79b7+9bFK02Ww9LU1NvR0KhdE4OGgL
+P1ThPgC6Pqh8pBvCAAAKaaMGKllKFvgmvG+4LDFMsYZ9BdHrNZtvy8qKroRmeyZmhyv+6r2WL9SOWQfG7s07nD83zE+7mMsQrYXJJTlAQBAiiknRgq2kzeS/I2vO8M1McFrOf3Qf7755q73q6sLc6JYzGtFImFx7PJl24Baw8nl8p+1BoP8wtDQlNfrDaDY6TUhkMoAAJDizSZGabqOtGktTtcRDQaDfV1ervGVfa/uLCq6Z1siMpyemp5Q9vd
b1WqVWm80drW2HrejX6PJwgJkCBFlAABIMS2kNELzfYpsIwnaxVvkvb7xdnn7wOtvvF4kk90Sa6TqjN/v5/U6ndVut/cd/Prr7o7OTpNEIrMBFLYSBaQyALCKkLFW71dS/UJLAmlZINtIIodsx/8nb8uWzduGhi78LW9d/npZxvxKmGAwKFgslu/LysrqHA7HFLo+rUZA1ydfg2UIAKxCyNbqhdGejGj+aAO2bT/tRINJLOAPCLbq6upafmryWh
Tb4/HY2+XybysrK58t2bHj0ygh4iFYHrKmySOPmHnV0ZcNYr8hECIAAPI5naU0Qky1iERKzz5Out38sZYW05tVVbboBhmn0yobGpt6OxUKs4fnXeFQyItiV6EEwTIEAEA+r0Ypzc59YWX0rURK45VX9uSTO7KyMkV/IGAbdzjtZrN5Gl3fyktaowwRZQAASHFVE2NODFJkm9QKKHZEmS3Lg4gyAADyedVLaToQiw2k0G201Rg7ZpVNr4F2XgAAk
OKaJUZMcDRnkfoFM9FcwjfrL5R2vgYyBACAFNesxUgj1CE0PxIfZrYDGQIAvyH8IsAAoH90TvkreAEAAAAASUVORK5CYII='  
  ,'Thanks & Regards',@LastProductKey  
  )  
    
    End  
 END
GO
/****** Object:  StoredProcedure [dbo].[sproc_wsLRS_NotificationSetting_Update]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_wsLRS_NotificationSetting_Update]
	@NotificationID int,
	@EmailSend nvarchar(50) = null,
	@EmailCancel int = null,
	@EmailReschedule int = null,
	@EmailWhen nvarchar(50) = null,
	@SMSSend nvarchar(50) = null,
	@SMSCancel int = null,
	@SMSReschedule int = null,
	@SMSWhen nvarchar(50) = null,
	@isactive int = null,
	@CreatedDatetime datetime = null,
	@ModifiedDatetime datetime = null,
	@ClientKey	nvarchar(1000)

AS

IF EXISTS(SELECT 1 FROM tbl_LRS_NotificationSetting WHERE ClientKey = @ClientKey)
	BEGIN
		UPDATE [dbo].[tbl_LRS_NotificationSetting]
		SET
			[EmailSend] = @EmailSend,
			[EmailCancel] = @EmailCancel,
			[EmailReschedule] = @EmailReschedule,
			[EmailWhen] = @EmailWhen,
			[SMSSend] = @SMSSend,
			[SMSCancel] = @SMSCancel,
			[SMSReschedule] = @SMSReschedule,
			[SMSWhen] = @SMSWhen,
			[isactive] = @isactive,
			[CreatedDatetime] = @CreatedDatetime,
			[ModifiedDatetime] = @ModifiedDatetime
		 WHERE 
			ClientKey = @ClientKey
	END
ELSE
	BEGIN
		INSERT [dbo].[tbl_LRS_NotificationSetting]
		(
			[EmailSend],
			[EmailCancel],
			[EmailReschedule],
			[EmailWhen],
			[SMSSend],
			[SMSCancel],
			[SMSReschedule],
			[SMSWhen],
			[isactive],
			[CreatedDatetime],
			[ModifiedDatetime],
			ClientKey

		)
		VALUES
		(
			@EmailSend,
			@EmailCancel,
			@EmailReschedule,
			@EmailWhen,
			@SMSSend,
			@SMSCancel,
			@SMSReschedule,
			@SMSWhen,
			@isactive,
			@CreatedDatetime,
			@ModifiedDatetime,
			@ClientKey

		)
	END
GO
/****** Object:  StoredProcedure [dbo].[sproc_wsEmail_Setting_Update]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_wsEmail_Setting_Update]    
 @EmailID int =null,    
 @EmailFrom nvarchar(50) = null,    
 @EmailPassword nvarchar(50) = null,    
 @EmailFromTitle nvarchar(500) = null,    
 @EmailPort nvarchar(50) = null,    
 @EmailSMTP nvarchar(50) = null,    
 @Twilio_CALL nvarchar(100) = null,    
 @Twilio_SMS nvarchar(100) = null,    
 @Twilio_AccountSid nvarchar(100) = null,    
 @Twilio_AuthToken nvarchar(100) = null,   
 @CompanyLogo nvarchar(MAX) = null,   
 @Signature nvarchar(MAX) = null,   
 @ClientKey nvarchar(1000)    
    
AS    
    
IF EXISTS(SELECT 1 FROM tbl_Email_Setting WHERE ClientKey = @ClientKey)    
 BEGIN    
  UPDATE [dbo].[tbl_Email_Setting]    
  SET    
   [EmailFrom] = @EmailFrom,    
   [EmailPassword] = @EmailPassword,    
   [EmailFromTitle] = @EmailFromTitle,    
   [EmailPort] = @EmailPort,    
   [EmailSMTP] = @EmailSMTP,    
   [Twilio_CALL] = @Twilio_CALL,    
   [Twilio_SMS] = @Twilio_SMS,    
   [Twilio_AccountSid] = @Twilio_AccountSid,    
   [Twilio_AuthToken] = @Twilio_AuthToken   
   
     
   WHERE     
   ClientKey = @ClientKey    
 END    
ELSE    
 BEGIN    
  INSERT [dbo].[tbl_Email_Setting]    
  (    
   [EmailFrom],    
   [EmailPassword],    
   [EmailFromTitle],    
   [EmailPort],    
   [EmailSMTP],    
   [Twilio_CALL],    
   [Twilio_SMS],    
   [Twilio_AccountSid],    
   [Twilio_AuthToken],    
   [CompanyLogo],  
   [Signature],  
   ClientKey    
    
  )    
  VALUES    
  (    
   @EmailFrom,    
   @EmailPassword,    
   @EmailFromTitle,    
   @EmailPort,    
   @EmailSMTP,    
   @Twilio_CALL,    
   @Twilio_SMS,    
   @Twilio_AccountSid,    
   @Twilio_AuthToken,    
   @CompanyLogo,  
   @Signature,  
   @ClientKey    
    
  )    
 END
GO
/****** Object:  StoredProcedure [dbo].[sproc_wsEmail_CompanyLogoSignature_Update]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_wsEmail_CompanyLogoSignature_Update]
	@CompanyLogo nvarchar(MAX) = null,
	@Signature nvarchar(MAX) = null,
	@ClientKey	nvarchar(1000)

AS

IF EXISTS(SELECT 1 FROM tbl_Email_Setting WHERE ClientKey = @ClientKey)
	BEGIN
		UPDATE [dbo].[tbl_Email_Setting]
		SET
			[CompanyLogo] = @CompanyLogo,
			[Signature] = @Signature
		 WHERE 
			ClientKey = @ClientKey
	END
ELSE
	BEGIN
		INSERT [dbo].[tbl_Email_Setting]
		(
			[CompanyLogo],
			[Signature],
			ClientKey
		)
		VALUES
		(
			@CompanyLogo,
			@Signature,
			@ClientKey
		)
	END
GO
/****** Object:  StoredProcedure [dbo].[sproc_UpdateTwilioIVRStatusinCalaudit_temp]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sproc_UpdateTwilioIVRStatusinCalaudit '2'              
      
--Update tbl_LRS_Reminder set LastUpdatedStatusFK=5 where eventno='2'        
--exec sproc_UpdateTwilioIVRStatusinCalaudit '1'      
              
CREATE Proc [dbo].[sproc_UpdateTwilioIVRStatusinCalaudit_temp]              
@EventID nvarchar(50)              
As              
Begin                 
          
If Exists (select Eventno from tbl_LRS_Calaudit where eventFK=@EventID and IsActive<> 1  )           
BEGIN          
  
  
          
 If Exists (select  eventno from tbl_LRS_Reminder where eventno=@EventID and LastUpdatedStatusFK <> 5  )           
 BEGIN          
              
           
 SELECT DISTINCT TL.Caseno as Caseno ,            
             
 ltrim(RTRIM( LL.location)) as EventT,1 as Category,1 as Color,LL.CreatedUserFK as Staff            
 ,TL.Eventno,            
             
 (ltrim(RTRIM( LL.location)) +' '+ (case when LL.LastUpdatedStatusFK=1 then ': Confirmed by client'             
 when LL.LastUpdatedStatusFK=2 then ': Declined by client'             
 when LL.LastUpdatedStatusFK=3 then ': Requested reschedule by client'             
 when LL.LastUpdatedStatusFK=4 then ': Requested operator callback by client'             
 ELSE ': No response from client' END)) as  FullEvent            
 from tbl_LRS_Calaudit TL              
 INNER join tbl_LRS_Reminder LL On LL.eventno=TL.eventFK               
 where LL.eventno=@EventID          
   
           
 UPDATE tbl_LRS_Calaudit set IsActive=1 Where eventFK=@EventID           
 END          
           
END          
          
               
END
GO
/****** Object:  StoredProcedure [dbo].[sproc_UpdateTwilioIVRStatusinCalaudit]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sproc_UpdateTwilioIVRStatusinCalaudit '1'  
                  
CREATE Proc [dbo].[sproc_UpdateTwilioIVRStatusinCalaudit]    
@EventID nvarchar(50) ,
@MACID Nvarchar(50) =Null                  
As                  
Begin       


    
--==================================   Decalre Variable  ===================================================    
Declare @LastStatus int,@LastStatusCompare int    

	Declare @TmpClientMac Nvarchar(50)             
	            
	set @TmpClientMac      =(select MacID from tbl_RS_Registration where  MacID=@MACID) 
	If(@TmpClientMac='62C25F068041' and @MACID <>'')                        
	Begin   
		  
		--==================================  Check Isactive or Not :START :01 ==========================  
		If Exists (select Eventno from tbl_LRS_Calaudit where eventFK=@EventID and IsActive <> 1  )               
		BEGIN    
		  

		--==================================  Remidner table Check Twilio Status :START :02 ==========================  
		If Exists (select  eventno from tbl_LRS_Reminder where eventno=@EventID)               
		BEGIN       
		  
		  
		--==================================  Comparison  :START :03 ==========================         
		If Exists (select EventFK from tbl_TwowaySyncCompar where EventFK=@EventID)               
		BEGIN       
		           
		   
		             
		 set @LastStatus = (select  LastUpdatedStatusFK from tbl_LRS_Reminder where eventno=@EventID )    
		 set @LastStatusCompare = (select top 1  LastSyncStatus from tbl_TwowaySyncCompar where EventFK=@EventID order by 1 desc)     
		    
		    
			 if(@LastStatus <> @LastStatusCompare)    
			 BEGIN    
			   
			 set @LastStatus = (select  LastUpdatedStatusFK from tbl_LRS_Reminder where eventno=@EventID)    
			               
			 SELECT DISTINCT TL.Caseno as Caseno ,ltrim(RTRIM( LL.location)) as EventT,1 as Category,1 as Color,LL.CreatedUserFK as Staff                
			 ,TL.Eventno,(ltrim(RTRIM( LL.location)) +' '+ (case when LL.LastUpdatedStatusFK=1 then ': Confirmed by client'                 
			 when LL.LastUpdatedStatusFK=2 then ': Declined by client'                 
			 when LL.LastUpdatedStatusFK=3 then ': Requested reschedule by client'                 
			 when LL.LastUpdatedStatusFK=4 then ': Requested operator callback by client'                 
			 ELSE ': No response from client' END)) as  FullEvent                
			 from tbl_LRS_Calaudit TL                  
			 INNER join tbl_LRS_Reminder LL On LL.eventno=TL.eventFK                   
			 where LL.eventno=@EventID              
			        
			       
			 INSERT INTO tbl_TwowaySyncCompar  (EventFK,LastSyncStatus,CreatedDttm,ModifiedDttm)    
			 VALUES(@EventID,@LastStatus,GETDATE(),GETDATE())    
			            
			              
			 END      
		    
		END    
		ELSE    
		BEGIN    
		    
		  
		  
		set @LastStatus = (select  LastUpdatedStatusFK from tbl_LRS_Reminder where eventno=@EventID )  
		  
		SELECT   @LastStatus  
		              
		SELECT DISTINCT TL.Caseno as Caseno ,ltrim(RTRIM( LL.location)) as EventT,1 as Category,1 as Color,LL.CreatedUserFK as Staff                
		,TL.Eventno,(ltrim(RTRIM( LL.location)) +' '+ (case when LL.LastUpdatedStatusFK=1 then ': Confirmed by client'                 
		when LL.LastUpdatedStatusFK=2 then ': Declined by client'                 
		when LL.LastUpdatedStatusFK=3 then ': Requested reschedule by client'                 
		when LL.LastUpdatedStatusFK=4 then ': Requested operator callback by client'                 
		ELSE ': No response from client' END)) as  FullEvent                
		from tbl_LRS_Calaudit TL                  
		INNER join tbl_LRS_Reminder LL On LL.eventno=TL.eventFK                   
		where LL.eventno=@EventID              
		       
		      
		 INSERT INTO tbl_TwowaySyncCompar  (EventFK,LastSyncStatus,CreatedDttm,ModifiedDttm)    
		 VALUES(@EventID,@LastStatus,GETDATE(),GETDATE())    
		               
		END            
		               
		END              
		   
		  
		  
		  
		--==================================  END :02 ===================================================    
		  
		END    
		  
		--==================================  END :01 ===================================================    
	END

END
GO
/****** Object:  StoredProcedure [dbo].[sproc_UpdateRegistrationDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec sproc_UpdateRegistrationDetails 'Get','','','',''

CREATE proc [dbo].[sproc_UpdateRegistrationDetails]
@Flag Nvarchar(10),
@CompanyName Nvarchar(Max) = Null,
@Mobile Nvarchar(50)= Null,
@Email Nvarchar(500)= Null,
@MACID Nvarchar(100)= Null
as
Begin

	IF(@Flag='Update')
	Begin


	Declare @RID Nvarchar(30)

	set @RID =(Select RID from tbl_RS_Registration where MacID=@MACID)

	UPDATE tbl_RS_Registration set 

	CompanyName=@CompanyName,
	Mobile=@Mobile,
	Email=@Email,
	ModifiedDtTm=GETDATE()
	 where RID=@RID
	 
	 SELECT 1 as Result

	End

	IF(@Flag='GET')
	Begin

	SELECT     RID, RSClientID, CompanyName, Mobile, Email, ProductKey, MacID, ISActivated, LogoImage, CreatedDtTm, ModifiedDtTm, InstalledVersion, LastUseDate
	FROM         tbl_RS_Registration where MacID=@MACID

	END


END
GO
/****** Object:  StoredProcedure [dbo].[Sproc_UpdateRecall_Details]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sproc_UpdateRecall_Details]       
@RecallEnable int,    
@HMT int,    
@Inteval int        
as        
Begin         
        
        
update tbl_Email_Setting set  RecallInterval=@Inteval,RecallHMT=@HMT,ReCallEnable=@RecallEnable    
      
SELECT 1 as Result ,RecallInterval,RecallHMT ,ReCallEnable   from  tbl_Email_Setting
        
        
END      
      
      
            
--update tbl_Email_Setting set  RecallInterval=NULL,RecallHMT=NULL,ReCallEnable=NULL
GO
/****** Object:  StoredProcedure [dbo].[sproc_Update_Template]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec sproc_Update_Template '17'  
  
CREATE Proc [dbo].[sproc_Update_Template]  
@ID Int  
as  
Begin  
  
select ROW_NUMBER() over(order by (select 1)) as No,Name as Title,CONVERT(VARCHAR(10), TA.CreatedDatetime, 101)  as [Created Date],body as [Event Body],ID,EmailSubject as [BODY],        
Purpose as [Appointment Type],LT.Language as [Language],isnull(TA.TwilioLang,6),
TA.isSMS,TA.isCALL,TA.isEMAIL,TA.whenSMS,TA.whenCALL,TA.whenEMAIL,TA.IsReschedule   
from tbl_Apptoto_AddSmsEmail   TA      
LEFT join  lst_TwilioLanguage LT On LT.LID=isnull(TA.TwilioLang,6)  
where   ID =@ID  
  
  
END
GO
/****** Object:  StoredProcedure [dbo].[Sproc_Update_AfterTwoWaysync]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sproc_Update_AfterTwoWaysync]    
@EventID nvarchar(50),   
@MACID Nvarchar(50) =Null  
as    
Begin    
  
	Declare @LastStatus int  ,@TmpClientMac Nvarchar(50)             
	set @LastStatus = (select  LastUpdatedStatusFK from tbl_LRS_Reminder where eventno=@EventID)  

	            
	set @TmpClientMac =(select MacID from tbl_RS_Registration where  MacID=@MACID)
 
	If(@TmpClientMac='62C25F068041' and @MACID <>'')                        
	Begin   
	  
		if(@LastStatus <> 5 OR @LastStatus <> 0)  
		BEGIN  
		  
		 UPDATE tbl_LRS_Calaudit set IsActive=1 Where eventFK=@EventID     
		  
		END  
		  
	End   
End
GO
/****** Object:  StoredProcedure [dbo].[sproc_tbl_LRS_NotificationSetting_Update]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_tbl_LRS_NotificationSetting_Update]
	@NotificationID int,
	@EmailSend nvarchar(50) = null,
	@EmailCancel int = null,
	@EmailReschedule int = null,
	@EmailWhen nvarchar(50) = null,
	@SMSSend nvarchar(50) = null,
	@SMSCancel int = null,
	@SMSReschedule int = null,
	@SMSWhen nvarchar(50) = null,
	@isactive int = null,
	@CreatedDatetime datetime = null,
	@ModifiedDatetime datetime = null,
	@ClientKey	nvarchar(1000)

AS

IF EXISTS(SELECT 1 FROM tbl_Email_Setting WHERE ClientKey = @ClientKey)
	BEGIN
		UPDATE [dbo].[tbl_LRS_NotificationSetting]
		SET
			[EmailSend] = @EmailSend,
			[EmailCancel] = @EmailCancel,
			[EmailReschedule] = @EmailReschedule,
			[EmailWhen] = @EmailWhen,
			[SMSSend] = @SMSSend,
			[SMSCancel] = @SMSCancel,
			[SMSReschedule] = @SMSReschedule,
			[SMSWhen] = @SMSWhen,
			[isactive] = @isactive,
			[CreatedDatetime] = @CreatedDatetime,
			[ModifiedDatetime] = @ModifiedDatetime
		 WHERE 
			ClientKey = @ClientKey
	END
ELSE
	BEGIN
		INSERT [dbo].[tbl_LRS_NotificationSetting]
		(
			[EmailSend],
			[EmailCancel],
			[EmailReschedule],
			[EmailWhen],
			[SMSSend],
			[SMSCancel],
			[SMSReschedule],
			[SMSWhen],
			[isactive],
			[CreatedDatetime],
			[ModifiedDatetime],
			ClientKey

		)
		VALUES
		(
			@EmailSend,
			@EmailCancel,
			@EmailReschedule,
			@EmailWhen,
			@SMSSend,
			@SMSCancel,
			@SMSReschedule,
			@SMSWhen,
			@isactive,
			@CreatedDatetime,
			@ModifiedDatetime,
			@ClientKey

		)
	END
GO
/****** Object:  StoredProcedure [dbo].[sproc_tbl_LRS_NotificationSetting_Insert]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_tbl_LRS_NotificationSetting_Insert]
	@NotificationID int output,
	@EmailSend nvarchar(50) = null ,
	@EmailCancel int = null ,
	@EmailReschedule int = null ,
	@EmailWhen nvarchar(50) = null ,
	@SMSSend nvarchar(50) = null ,
	@SMSCancel int = null ,
	@SMSReschedule int = null ,
	@SMSWhen nvarchar(50) = null ,
	@isactive int = null ,
	@CreatedDatetime datetime = null ,
	@ModifiedDatetime datetime = null ,
	@ClientKey	varchar(1000)

AS

INSERT [dbo].[tbl_LRS_NotificationSetting]
(
	[EmailSend],
	[EmailCancel],
	[EmailReschedule],
	[EmailWhen],
	[SMSSend],
	[SMSCancel],
	[SMSReschedule],
	[SMSWhen],
	[isactive],
	[CreatedDatetime],
	[ModifiedDatetime],
	ClientKey

)
VALUES
(
	@EmailSend,
	@EmailCancel,
	@EmailReschedule,
	@EmailWhen,
	@SMSSend,
	@SMSCancel,
	@SMSReschedule,
	@SMSWhen,
	@isactive,
	@CreatedDatetime,
	@ModifiedDatetime,
	@ClientKey

)
	SELECT @NotificationID=SCOPE_IDENTITY();
GO
/****** Object:  StoredProcedure [dbo].[sproc_tbl_Email_Setting_Update]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_tbl_Email_Setting_Update]
	@EmailID int,
	@EmailFrom nvarchar(50) = null,
	@EmailPassword nvarchar(50) = null,
	@EmailFromTitle nvarchar(500) = null,
	@EmailPort nvarchar(50) = null,
	@EmailSMTP nvarchar(50) = null,
	@Twilio_CALL nvarchar(100) = null,
	@Twilio_SMS nvarchar(100) = null,
	@Twilio_AccountSid nvarchar(100) = null,
	@Twilio_AuthToken nvarchar(100) = null,
	@CompanyLogo nvarchar(MAX) = null,
	@Signature nvarchar(MAX) = null,
	@ClientKey	nvarchar(1000)

AS

IF EXISTS(SELECT 1 FROM tbl_Email_Setting WHERE ClientKey = @ClientKey)
	BEGIN
		UPDATE [dbo].[tbl_Email_Setting]
		SET
			[EmailFrom] = @EmailFrom,
			[EmailPassword] = @EmailPassword,
			[EmailFromTitle] = @EmailFromTitle,
			[EmailPort] = @EmailPort,
			[EmailSMTP] = @EmailSMTP,
			[Twilio_CALL] = @Twilio_CALL,
			[Twilio_SMS] = @Twilio_SMS,
			[Twilio_AccountSid] = @Twilio_AccountSid,
			[Twilio_AuthToken] = @Twilio_AuthToken,
			[CompanyLogo] = @CompanyLogo,
			[Signature] = @Signature
		 WHERE 
			ClientKey = @ClientKey
	END
ELSE
	BEGIN
		INSERT [dbo].[tbl_Email_Setting]
		(
			[EmailFrom],
			[EmailPassword],
			[EmailFromTitle],
			[EmailPort],
			[EmailSMTP],
			[Twilio_CALL],
			[Twilio_SMS],
			[Twilio_AccountSid],
			[Twilio_AuthToken],
			[CompanyLogo],
			[Signature],
			ClientKey

		)
		VALUES
		(
			@EmailFrom,
			@EmailPassword,
			@EmailFromTitle,
			@EmailPort,
			@EmailSMTP,
			@Twilio_CALL,
			@Twilio_SMS,
			@Twilio_AccountSid,
			@Twilio_AuthToken,
			@CompanyLogo,
			@Signature,
			@ClientKey

		)
	END
GO
/****** Object:  StoredProcedure [dbo].[sproc_tbl_Email_Setting_Insert]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_tbl_Email_Setting_Insert]
	@EmailID int output,
	@EmailFrom nvarchar(50) = null ,
	@EmailPassword nvarchar(50) = null ,
	@EmailFromTitle nvarchar(500) = null ,
	@EmailPort nvarchar(50) = null ,
	@EmailSMTP nvarchar(50) = null ,
	@Twilio_CALL nvarchar(100) = null ,
	@Twilio_SMS nvarchar(100) = null ,
	@Twilio_AccountSid nvarchar(100) = null ,
	@Twilio_AuthToken nvarchar(100) = null ,
	@CompanyLogo nvarchar(MAX) = null ,
	@Signature nvarchar(MAX) = null ,
	@ClientKey	nvarchar(1000)

AS

INSERT [dbo].[tbl_Email_Setting]
(
	[EmailFrom],
	[EmailPassword],
	[EmailFromTitle],
	[EmailPort],
	[EmailSMTP],
	[Twilio_CALL],
	[Twilio_SMS],
	[Twilio_AccountSid],
	[Twilio_AuthToken],
	[CompanyLogo],
	[Signature],
	ClientKey

)
VALUES
(
	@EmailFrom,
	@EmailPassword,
	@EmailFromTitle,
	@EmailPort,
	@EmailSMTP,
	@Twilio_CALL,
	@Twilio_SMS,
	@Twilio_AccountSid,
	@Twilio_AuthToken,
	@CompanyLogo,
	@Signature,
	@ClientKey

)
	SELECT @EmailID=SCOPE_IDENTITY();
GO
/****** Object:  StoredProcedure [dbo].[Sproc_Sync_Calaudit]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sproc_Sync_Calaudit]
as
Begin

if( 0 >= (select Count(*) as Total  from tbl_Calaudit_Last_Sync))
BEGIN

	Insert INTO tbl_Calaudit_Last_Sync (LastSync, CreatedDatetime, ModifiedDatetime)
	VALUES(GETDATE(),GETDATE(),GETDATE())



END
ELSE
BEGIN

	UPDATE tbl_Calaudit_Last_Sync set LastSync =GETDATE(),ModifiedDatetime=GETDATE()


END

End
GO
/****** Object:  StoredProcedure [dbo].[sproc_RemoveWhenNoCloud]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sproc_RemoveWhenNoCloud]
as
BEGIN

/*====================== Remove from tblAPIAppointment_Final ====================================*/   

Delete from  tblAPIAppointment_Final  where 
(Account in (SELECT Account from tblAPIAppointment_New) OR 
(case when    ( case  when isnull(LTRIM(RTRIM(Appointment)),'')= '' or Appointment='0' THEN isnull(LTRIM(RTRIM(home)),'')                                                 
else isnull(LTRIM(RTRIM(Appointment)),'') end)='' then                                                 
(Case when isnull(LTRIM(RTRIM(home)),'')= ''  OR home='0'     then isnull(LTRIM(RTRIM(Work)),'')  else '' EnD)                                                
else    ( case  when isnull(LTRIM(RTRIM(Appointment)),'')= '' or Appointment='0'   THEN isnull(LTRIM(RTRIM(home)),'')                                                 
else isnull(LTRIM(RTRIM(Appointment)),'') end) end)  in (SELECT                               
(case when    ( case  when isnull(LTRIM(RTRIM(Appointment)),'')= '' or Appointment='0' THEN isnull(LTRIM(RTRIM(home)),'')                                                 
else isnull(LTRIM(RTRIM(Appointment)),'') end)='' then                                                 
(Case when isnull(LTRIM(RTRIM(home)),'')= ''  OR home='0'     then isnull(LTRIM(RTRIM(Work)),'')  else '' EnD)                                                
else    ( case  when isnull(LTRIM(RTRIM(Appointment)),'')= '' or Appointment='0'   THEN isnull(LTRIM(RTRIM(home)),'')                                                 
else isnull(LTRIM(RTRIM(Appointment)),'') end) end)                              
from tblAPIAppointment_New)                           
OR Convert(nvarchar,Apptdate)  in(SELECT Convert(nvarchar,Apptdate) from tblAPIAppointment_New)  )                          
OR Convert(nvarchar,ApptTime)  in(SELECT Convert(nvarchar,ApptTime) from tblAPIAppointment_New)             

/*====================== Remove from tbl_LRS_Reminder_Timing ====================================*/  

DELETE from tbl_LRS_Reminder_Timing where EventFK in (
SELECT eventno FROM tbl_LRS_Reminder Where eventno in 
(SELECT MainEventID FROM tbl_FinalReminderDetails where RID in (SELECT NextEventID from tbl_NextGenerateEventID)))

/*====================== Remove from tbl_LRS_Reminder (Not show in Dashboard) ====================================*/  


delete FROM tbl_LRS_Reminder Where eventno in 
(SELECT MainEventID FROM tbl_FinalReminderDetails where RID in (SELECT NextEventID from tbl_NextGenerateEventID))


/*====================== Remove from tbl_FinalReminderDetails(Cloud Final Sync Table) ====================================*/  


delete FROM tbl_FinalReminderDetails where RID in (SELECT NextEventID from tbl_NextGenerateEventID)

/*====================== Remove from  tbl_NextGenerateEventID ====================================*/  



Delete from tbl_NextGenerateEventID

/*====================== Return Result ====================================*/  

SELECT 1 as Result






END
GO
/****** Object:  StoredProcedure [dbo].[Sproc_Remove_RS_Contact]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Sproc_Remove_RS_Contact 'RS1'  
  
CREATE proc [dbo].[Sproc_Remove_RS_Contact]  
@CardCode Nvarchar(100)  
as  
Begin  
  
If(@CardCode like '%RS%')  
BEGIN  

update tbl_RS_Contactlist set Isactive=1 where  CardCode=@CardCode  

select 1 as Result
  
END  
ELSE  
BEGIN  
  
DELETE from	[card2] where firmcode in (select firmcode from [card] where cardcode=@CardCode)  
DELETE from [card] where cardcode=@CardCode  
DELETE from [case] where caseno in (select caseno from [casecard] where cardcode=@CardCode)  
DELETE from [casecard] where cardcode=@CardCode  
  
  select 1 as Result
  
END  
END
GO
/****** Object:  StoredProcedure [dbo].[sproc_Reminder_Delete]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_Reminder_Delete]
	@eventno int
AS

DELETE FROM [dbo].[tbl_LRS_Reminder]
 WHERE 
	[eventno] = @eventno
GO
/****** Object:  StoredProcedure [dbo].[Sproc_Recallupdate_Cloud]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sproc_Recallupdate_Cloud]
@ClientKey nvarchar(100),
@RecallEnable int,  
@HMT int,  
@Inteval int  
as
Begin

Update tbl_Email_Setting SET RecallInterval=@Inteval,RecallHMT=@HMT,ReCallEnable=@RecallEnable  Where ClientKey=@ClientKey

SELECT 1 as Result

END
GO
/****** Object:  StoredProcedure [dbo].[Sproc_InsertAllawCount]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sproc_InsertAllawCount]  
@ServerMACID nvarchar(100),  
@CardCount nvarchar(100),  
@Card2Count nvarchar(100),  
@CaseCount nvarchar(100),  
@CasecardCount nvarchar(100)  ,
@StatusLog nvarchar(100)  
as  
Begin  
  
if exists (select ServerMACID from tbl_A1lawVSDataloader where ServerMACID=@ServerMACID)  
BEGIN  
  
 update tbl_A1lawVSDataloader set Isstatus=1 where ServerMACID=@ServerMACID  
  
 insert Into tbl_A1lawVSDataloader (ServerMACID,[Card], Card2, [Case], Casecard, CreatedDatetime, ModifiedDatetime,Isstatus,StatusLog)  
 VALUES  
 (@ServerMACID,@CardCount,@Card2Count,@CaseCount,@CasecardCount,GETDATE(),GETDATE(),0,@StatusLog)  
  
  
 SELECT 1 as Result  
  
END  
ELSE  
BEGIN  
  
  
   
  
 insert Into tbl_A1lawVSDataloader (ServerMACID,[Card], Card2, [Case], Casecard, CreatedDatetime, ModifiedDatetime,Isstatus,StatusLog)  
 VALUES  
 (@ServerMACID,@CardCount,@Card2Count,@CaseCount,@CasecardCount,GETDATE(),GETDATE(),0,@StatusLog)  
  
  
 SELECT 1 as Result  
  
END  
  
End   
  
--ALTER TABLE tbl_A1lawVSDataloader ADD Isstatus int
GO
/****** Object:  StoredProcedure [dbo].[sproc_Insert_SyncGoogle]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC sproc_Insert_SyncGoogle '','','','','','','','','','','','','','','',''  
  
CREATE Proc [dbo].[sproc_Insert_SyncGoogle]                  
@Salutation Nvarchar(50) =Null,                  
@FName Nvarchar(500)=Null,                  
@MName Nvarchar(500)=Null,                  
@LName Nvarchar(500)=Null,                  
@PhoneNo Nvarchar(20)=Null,                  
@Email Nvarchar(500)=Null,                  
@Zipcode Nvarchar(10)=Null,                  
@Notes Nvarchar(MAX) =Null,              
@DOB datetime =Null,            
@CountryCode Nvarchar(10)=Null,            
@MobileType Nvarchar(100)=Null,            
@EmailType Nvarchar(100)=Null,          
@ContactType  Nvarchar(200) =Null ,  
@ClientKey  Nvarchar(200) =Null                 
         
As                  
Begin                  
           
Insert Into tbl_GoogleSync_Contact (  
Salutation,   
FName,   
MName,   
LName,   
PhoneNo,   
Email,   
Zipcode,   
Notes,   
DOB,   
CountryCode,   
MobileType,   
EmailType,   
ContactType,   
ClientKey  
)  
VALUES  
(  
@Salutation  
,@FName  
,@MName  
,@LName  
,@PhoneNo  
,@Email  
,@Zipcode  
,@Notes  
,@DOB  
,@CountryCode  
,@MobileType  
,@EmailType  
,@ContactType  
,@ClientKey  
)  
           
select 1 As Result                  
                  
           
END   
  
  
  
  
  
  
  
--truncate table tbl_GoogleSync_Contact
GO
/****** Object:  StoredProcedure [dbo].[Sproc_Insert_BadgeTime]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sproc_Insert_BadgeTime]
@MacID nvarchar(50)
as
Begin

if not Exists(select MACid from tbl_badge_Notification where MACid=@MacID)
BEGIN

	insert into tbl_badge_Notification 
	(MACid,OpenDatetime,Createddate,ModifiedDate)
	VALUES
	(@MacID,GETDATE(),GETDATE(),GETDATE())

END
ELSE
BEGIN

	UPDATE tbl_badge_Notification SET OpenDatetime=Getdate(),ModifiedDate=GETDATE() where MACid=@MacID

END



END

--TRUNCATE TABLE tbl_badge_Notification
GO
/****** Object:  StoredProcedure [dbo].[sproc_GooglesyncContect_Separate]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[sproc_GooglesyncContect_Separate]  
@Salutation Nvarchar(50) =Null,                      
@FName Nvarchar(500)=Null,                      
@MName Nvarchar(500)=Null,                      
@LName Nvarchar(500)=Null,                      
@PhoneNo Nvarchar(20)=Null,                      
@Email Nvarchar(500)=Null,                      
@Zipcode Nvarchar(10)=Null,                      
@Notes Nvarchar(MAX) =Null,                  
@DOB datetime =Null,                
@CountryCode Nvarchar(10)=Null,                
@MobileType Nvarchar(100)=Null,                
@EmailType Nvarchar(100)=Null,              
@ContactType  Nvarchar(200) =Null ,      
@ClientKey  Nvarchar(200) =Null       
  
As  
Begin  
  
If Not Exists(select @Email from [card] where  email=@Email)                      
Begin                               
                        
      
-----CARD-----------      
DECLARE @CardCodeNotAuto NVARCHAR(50)      
      
set @CardCodeNotAuto =(SELECT isnull(MAX(cardcode),0)+1 AS Highestfirmcode FROM [card])                               
Insert Into [card]                            
select @CardCodeNotAuto,0,'Dear'+' '+@Salutation+' '+@LName,@Salutation,@FName,@MName,@LName,'','','RS','','','','','','',@Email,@DOB,'','','','','',0,'RS','RS',Getdate(),Getdate(),@Notes,'','','','','','','','','','','','','','','','','','','','','','RS'
  
  
-----CARD2-----------             
DECLARE @FirmCodeNotAuto NVARCHAR(50)          
set @FirmCodeNotAuto =(SELECT isnull(MAX(firmcode),0)+1 AS Highestfirmcode FROM card2)    
                                              
Insert Into card2                            
select @FirmCodeNotAuto,'','','','','','','',@Zipcode,@PhoneNo,'','','','','RS','RS',Getdate(),Getdate(),'',0,0,'','','','','',@CountryCode,@MobileType,@EmailType                           
                            
      
Update [card] set firmcode=@FirmCodeNotAuto where cardcode=@CardCodeNotAuto    
  
-----CASE-----------       
DECLARE @CasenoNotAuto NVARCHAR(50)       
      
set @CasenoNotAuto =(SELECT isnull(MAX(caseno),0)+1 AS HighestCaseNo FROM [case])       
                           
insert Into [case]                          
select @CasenoNotAuto,'RS','OPEN','',0,'','','','','',Getdate(),Getdate(),Getdate(),Getdate(),0,Getdate(),'',Getdate(),0,'','',Getdate(),'This case Genrated with RS','',''                           
                            
-----CASECARD-----------                        
                            
Insert Into casecard                            
select @CasenoNotAuto,@CardCodeNotAuto,0,'','','RS',0,''     
  
  
End  
End
GO
/****** Object:  StoredProcedure [dbo].[Sproc_GetTableDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sproc_GetTableDetails]
AS
Begin
 Declare @cardErrorPer int
 Declare @card2ErrorPer int
 Declare @caseErrorPer int
 Declare @caseCardErrorPer int
 Declare @error Nvarchar(50) = null
 
 set @cardErrorPer = case WHEN (Select COUNT(cardcode) from dbo.[card])=0 then 0 else(((Select COUNT(cardcode) as Number from dbo.[card] where cardcode = 0) * 100) 
      / (Select COUNT(cardcode) from dbo.[card])) end
      
 set @card2ErrorPer = case WHEN (Select COUNT(firmcode) from dbo.[card2])=0 then 0 else (((Select COUNT(firmcode) as Number from dbo.[card2] where firmcode = 0) * 100)
      / (Select COUNT(firmcode) from dbo.[card2])) end
 
 set @caseErrorPer = 
 case WHEN (Select COUNT(caseno) From dbo.[case])=0 THEN 0 ELSE
 (((Select COUNT(caseno) as Number from dbo.[case] where caseno = 0) * 100)
      / (Select COUNT(caseno) From dbo.[case])) END
 
 set @caseCardErrorPer = case WHEN (Select COUNT(caseno) From dbo.casecard)=0 THEN 0 ELSE  (((Select COUNT(caseno) as Number from dbo.casecard where caseno = 0) * 100)
      / (Select COUNT(caseno) From dbo.casecard))END
 
 If @cardErrorPer > 10 And @card2ErrorPer > 10 And @caseErrorPer > 10 And @caseCardErrorPer > 10
  BEGIN
   Set @error = '1'
  END
 Else
  BEGIN
   Set @error = '0'
  END
  
 Select
  (Select Count(*) from [card]) As [card],
  (Select Count(*) from [card2])  As [card2], 
  (Select Count(*)  from [case])   As [case],  
  (select Count(*)  from [casecard]) As [casecard],
  case 
   when @error=1 then 'Error occur(s) during sync' 
  else 'Sync Completed' end     As StatusLog
  
END
GO
/****** Object:  StoredProcedure [dbo].[Sproc_GetRecallTime]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sproc_GetRecallTime]
As
Begin
select ID,HMTime from lst_Recall_Time where isnull(Isactive,0) <>1

END
GO
/****** Object:  StoredProcedure [dbo].[Sproc_GetRecallInterval]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sproc_GetRecallInterval]
As
Begin
select ID,	Intervaltime
 from lst_Recall_Interval where isnull(Isactive,0) <>1

END
GO
/****** Object:  StoredProcedure [dbo].[sproc_GetgoogleContactFormClientKey]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec sproc_GetgoogleContactFormClientKey 'C73F-DDDA-BB96-4ACF-BA6D'

CREATE PROC [dbo].[sproc_GetgoogleContactFormClientKey]
@ClientKey nvarchar(500)
As
Begin

SELECT    
ID, Salutation, FName, MName, LName, PhoneNo, Email, Zipcode, Notes, DOB, CountryCode, MobileType, EmailType, ContactType,
 ClientKey
FROM tbl_GoogleSync_Contact
WHERE ClientKey=@ClientKey


End
GO
/****** Object:  StoredProcedure [dbo].[sproc_Get_BadgeCount]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sproc_Get_BadgeCount '62C25F068041'  
CREATE Proc [dbo].[sproc_Get_BadgeCount]  
@MACID Nvarchar(50)  
as  
Begin  
  
Declare @OpenDate datetime  
  
set  @OpenDate =(SELECT OpenDatetime from  tbl_badge_Notification where MACid=@MACID)  
  
select Count(*) as Total  from tbl_LRS_Reminder   
where  

CONVERT(Date, @OpenDate , 103)  <= CONVERT(Date, ModifiedDate , 103)

and 

CONVERT(varchar(5),@OpenDate,108) <= CONVERT(varchar(5),ModifiedDate,108)


  
  
and LastUpdatedStatusFK is NOT NULL and  LastUpdatedStatusFK <> ''  
   
  
END
GO
/****** Object:  StoredProcedure [dbo].[Sproc_Get_APIserverDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sproc_Get_APIserverDetails]
@Flag nvarchar(50),
@IsCSVstatus Int =null,
@CSVfilePath nvarchar(500) =null,
@BatfilePath nvarchar(500) =null

as    
Begin    

if(@Flag = 'GET')
Begin 
    
select IsCSVstatus,	CSVfilePath,	BatfilePath  from  tbl_Email_Setting    
    
End

if(@Flag = 'UPDATE')
Begin 

update tbl_Email_Setting set IsCSVstatus=@IsCSVstatus,CSVfilePath=@CSVfilePath ,BatfilePath=@BatfilePath 

SELECT 1 as Result 



End  


END
GO
/****** Object:  StoredProcedure [dbo].[Sproc_Get_A1lawSyncStatus]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sproc_Get_A1lawSyncStatus]  
as  
Begin   
  
select A1Sync,RecallInterval,	RecallHMT,	ReCallEnable from  tbl_Email_Setting  
  
End
GO
/****** Object:  StoredProcedure [dbo].[Sproc_Generate_Report_Monthly_Useges]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sproc_Generate_Report_Monthly_Useges]            
@Fromdate datetime,            
@Todate datetime            
            
as            
Begin            
            
--TOtal            
select LR.CreatedUserFK,RS.CompanyName as [Created User] ,COUNT(*) as Total            
into #TempTotal from tbl_FinalReminderDetails TF            
left Join tbl_LRS_Reminder LR On LR.eventno=TF.MainEventID            
left Join tbl_RS_Registration RS on COnvert(varchar,RS.RID) =COnvert(varchar,LR.CreatedUserFK)            
where          
Convert(date,EventDatetime)            
 between Convert(date,@Fromdate) and Convert(date,@Todate)            
Group By LR.CreatedUserFK,RS.CompanyName            
            
            
--CALL            
            
select LR.CreatedUserFK,RS.CompanyName as [Created User] ,COUNT(*) as Total  into #TempCall from tbl_FinalReminderDetails TF            
left Join tbl_LRS_Reminder LR On LR.eventno=TF.MainEventID            
left Join tbl_RS_Registration RS on COnvert(varchar,RS.RID) =COnvert(varchar,LR.CreatedUserFK)            
where TF.CallStatus=1            
and Convert(date,EventDatetime) between Convert(date,@Fromdate) and Convert(date,@Todate)            
Group By LR.CreatedUserFK,RS.CompanyName            
            
--SMS            
select LR.CreatedUserFK,RS.CompanyName as [Created User] ,COUNT(*) as Total into #TempSMS from tbl_FinalReminderDetails TF            
left Join tbl_LRS_Reminder LR On LR.eventno=TF.MainEventID            
left Join tbl_RS_Registration RS on COnvert(varchar,RS.RID) =COnvert(varchar,LR.CreatedUserFK)            
where TF.SMSStatus=1            
and Convert(date,EventDatetime) between Convert(date,@Fromdate) and Convert(date,@Todate)            
Group By LR.CreatedUserFK,RS.CompanyName            
            
            
--EMAIL            
select LR.CreatedUserFK,RS.CompanyName as [Created User] ,COUNT(*) as Total into #TempEMAIL from tbl_FinalReminderDetails TF            
left Join tbl_LRS_Reminder LR On LR.eventno=TF.MainEventID            
left Join tbl_RS_Registration RS on COnvert(varchar,RS.RID) =COnvert(varchar,LR.CreatedUserFK)            
where TF.EmailStatus=1            
and Convert(date,EventDatetime) between Convert(date,@Fromdate) and Convert(date,@Todate)            
Group By LR.CreatedUserFK,RS.CompanyName            
            
--Isconfirm            
select LR.CreatedUserFK,RS.CompanyName as [Created User] ,COUNT(*) as Total into #TempIsconfom  from tbl_FinalReminderDetails TF            
left Join tbl_LRS_Reminder LR On LR.eventno=TF.MainEventID            
left Join tbl_RS_Registration RS on COnvert(varchar,RS.RID) =COnvert(varchar,LR.CreatedUserFK)            
where isnull(TF.IsConfirmable,0) =1            
and Convert(date,EventDatetime) between Convert(date,@Fromdate) and Convert(date,@Todate)            
Group By LR.CreatedUserFK,RS.CompanyName            
            
            
--IsPrivate            
select LR.CreatedUserFK,RS.CompanyName as [Created User] ,COUNT(*) as Total into #TempIsPrivate  from tbl_FinalReminderDetails TF            
left Join tbl_LRS_Reminder LR On LR.eventno=TF.MainEventID            
left Join tbl_RS_Registration RS on COnvert(varchar,RS.RID) =COnvert(varchar,LR.CreatedUserFK)            
where  isnull(LR.IsPrivate,0) =1            
and Convert(date,EventDatetime) between Convert(date,@Fromdate) and Convert(date,@Todate)            
Group By LR.CreatedUserFK,RS.CompanyName            
            
            
            
--IsCancel By  User            
select LR.CreatedUserFK,RS.CompanyName as [Created User] ,COUNT(*) as Total into #TempIsCancel  from tbl_FinalReminderDetails TF            
left Join tbl_LRS_Reminder LR On LR.eventno=TF.MainEventID            
left Join tbl_RS_Registration RS on COnvert(varchar,RS.RID) =COnvert(varchar,LR.CreatedUserFK)            
where    isnull(LR.LastUpdatedStatusFK,0)=4        
and Convert(date,EventDatetime) between Convert(date,@Fromdate) and Convert(date,@Todate)            
Group By LR.CreatedUserFK,RS.CompanyName            
            
            
            
--IsConform By  User            
select LR.CreatedUserFK,RS.CompanyName as [Created User] ,COUNT(*) as Total into #TempIsConfirmuser  from tbl_FinalReminderDetails TF            
inner Join tbl_LRS_Reminder LR On LR.eventno=TF.MainEventID            
inner Join tbl_RS_Registration RS on COnvert(varchar,RS.RID) =COnvert(varchar,LR.CreatedUserFK)            
where  isnull(LR.LastUpdatedStatusFK,0)=1            
and Convert(date,EventDatetime) between Convert(date,@Fromdate) and Convert(date,@Todate)            
Group By LR.CreatedUserFK,RS.CompanyName            
            
            
            
select  TS.[Created User],TS.Total as [Call],TE.Total as [SMS],TEM.Total As [EMAIL],        
isnull(TT.Total,0) as [Total_Reminder],TI.Total as [Isconfirm]            
,TP.Total as [Private],TCI.Total as [Cancel By User],TIC.Total as [Confirm By User]            
into #Temp from #TempCall TS            
inner JOin #TempSMS TE On TE.CreatedUserFK=TS.CreatedUserFK            
inner JOin #TempEMAIL TEM On TEM.CreatedUserFK=TS.CreatedUserFK            
left join #TempIsconfom TI on TI.CreatedUserFK =TS.CreatedUserFK            
left join #TempTotal TT On TT.CreatedUserFK=TS.CreatedUserFK            
left join #TempIsPrivate TP On TP.CreatedUserFK=TS.CreatedUserFK            
left join #TempIsCancel TCI On TCI.CreatedUserFK=TS.CreatedUserFK            
left join #TempIsConfirmuser TIC On TIC.CreatedUserFK=TT.CreatedUserFK            
            
            
            
            
            
            
select [Created User], Call, SMS, EMAIL,[Total_Reminder] as [Total Reminder],isnull([Isconfirm],0) as Cancellable,            
isnull([Private],0) as [Private Reminder],isnull([Cancel by User],0) as [Cancel by User],            
ISNULL([Confirm By User],0) as [Confirm By User],        
        
case when ISNULL([Total_Reminder],0)=0 then 0 else        
 convert(numeric(18,0),ISNULL(convert(numeric(18,2),[Confirm By User]),0)/ ((isnull(convert(numeric(18,2),[Total_Reminder]),0)) )* 100) END as percentage          
            
from #Temp   
where [Created User] is not null           
             
Union all            
            
select 'Total :', sum(Call), sum(SMS),SUM(EMAIL),SUM([Total_Reminder]),SUM([Isconfirm]),SUM([Private])            
,sum([Cancel By User]),SUM([Confirm By User]) ,        
        
case when ISNULL(SUM([Total_Reminder]),0)=0 then 0 else        
 convert(numeric(18,0),ISNULL(convert(numeric(18,2),SUM([Confirm By User])),0)/ ((isnull(convert(numeric(18,2),SUM([Total_Reminder])),0)) )* 100) END        
        
 from #Temp  
 where [Created User] is not null            
             
 END
GO
/****** Object:  StoredProcedure [dbo].[sproc_FinalReminderDetails_Update]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_FinalReminderDetails_Update]              
 @RID int,              
 @EventID nvarchar(50) = null,              
 @Event nvarchar(MAX) = null,              
 @EventTitle nvarchar(500) = null,              
 @Phone nvarchar(50) = null,              
 @Email nvarchar(50) = null,              
 @FromPhoneNo nvarchar(50) = null,              
 @AccountSID nvarchar(500) = null,              
 @AuthToken nvarchar(500) = null,              
 @EventDatetime datetime = null,              
 @SMSStatus int = null,              
 @EmailStatus int = null,              
 @CallStatus int = null,              
 @ClientKey nvarchar(500) = null,              
 @RedirectLink nvarchar(MAX) = null,              
 @EmailFrom nvarchar(500) = null,              
 @EmailPassword nvarchar(500) = null,              
 @EmailFromTitle nvarchar(500) = null,              
 @EmailPort nvarchar(50) = null,              
 @EmailSMTP nvarchar(100) = null,              
 @CompanyLO nvarchar(MAX) = null,              
 @IsStatus int = null,              
 @MainEventID int = null,              
 @IsTwilioStatus int = null,    
 @AppointmentDatetime datetime = null ,          
 @IsConfirmable int =null,    
 @TwilioLang nvarchar(50)=null          
          
AS              
              
UPDATE [dbo].[tbl_FinalReminderDetails]              
SET              
 [EventID] = @EventID,              
 [Event] = @Event,              
 [EventTitle] = @EventTitle,              
 [Phone] = @Phone,              
 [Email] = @Email,              
 [FromPhoneNo] = @FromPhoneNo,              
 [AccountSID] = @AccountSID,              
 [AuthToken] = @AuthToken,              
 [EventDatetime] = @EventDatetime,              
 [SMSStatus] = @SMSStatus,              
 [EmailStatus] = @EmailStatus,              
 [CallStatus] = @CallStatus,              
 [ClientKey] = @ClientKey,              
 [RedirectLink] = @RedirectLink,              
 [EmailFrom] = @EmailFrom,              
 [EmailPassword] = @EmailPassword,              
 [EmailFromTitle] = @EmailFromTitle,              
 [EmailPort] = @EmailPort,              
 [EmailSMTP] = @EmailSMTP,              
 [CompanyLO] = @CompanyLO,              
 [IsStatus] = @IsStatus,              
 [MainEventID] = @MainEventID,              
 [IsTwilioStatus] = @IsTwilioStatus       ,    
 [AppointmentDatetime]= @AppointmentDatetime,          
 [IsConfirmable]=@IsConfirmable      ,    
 [TwilioLang]=@TwilioLang    
 WHERE               
 [RID] = @RID    
   
   
insert into [tbl_FinalReminderDetails_Log]  
SELECT * from tbl_FinalReminderDetails where RID=@RID
GO
/****** Object:  StoredProcedure [dbo].[sproc_FinalReminderDetails_Status]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [sproc_FinalReminderDetails_Status] '83B7-2CE8-95DF-4F46-845C'          
          
CREATE PROCEDURE [dbo].[sproc_FinalReminderDetails_Status]                              
@ClientKey nvarchar(100)                              
AS                              
BEgin                              
 DECLARE @LASTDATE AS DATETIME                              
                               
 SELECT @LASTDATE = LastAccessDateTime FROM tbl_TwilioStatusAccess WHERE ClientKey = @ClientKey                              
                               
 IF (@LASTDATE IS NULL)                              
 BEGIN                              
  INSERT INTO tbl_TwilioStatusAccess VALUES (@ClientKey,GETDATE())                              
  SET @LASTDATE  =CAST('2016-01-01' AS DATETIME)                              
 END                              
                   
                   
                   
                   
SELECT ClientKey,MainEventID,MAX(ModifiedDttm) as ModifiedDttm      
Into #TEMPClient              
FROM tbl_FinalReminderDetails                             
WHERE ClientKey = @ClientKey      
AND ModifiedDttm>= @LASTDATE                            
AND ([SID] is NOT NULL  or IsTwilioStatus <> 0  )      
GROUP BY    MainEventID ,ClientKey       
                   
                   
                              
SELECT top 10 TF.RID,TF.ClientKey,EventID,TF.MainEventID,IsTwilioStatus,                    
isnull((Case WHEN SID ='' then NULL ELSE [SID] END),'') as SID,Convert(VARCHAR(50),tf.ModifiedDttm,101) as [ConfirmDatetime],            
 CONVERT(VARCHAR(8), TF.ModifiedDttm, 24) as     [ConfirmTime]            
                
FROM tbl_FinalReminderDetails  TF      
inner join #TEMPClient TC On TC.ModifiedDttm=TF.ModifiedDttm                         
 WHERE TF.ClientKey = @ClientKey                              
 AND TF.ModifiedDttm>= @LASTDATE                              
 AND ([SID] is NOT NULL  or IsTwilioStatus <> 0  )                      
                        
                         
 ORDER by 1 desc,  TF.ModifiedDttm ASC    
     
 END
GO
/****** Object:  StoredProcedure [dbo].[sproc_FinalReminderDetails_SelectByPrimaryKey]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_FinalReminderDetails_SelectByPrimaryKey]
	@RID int
AS

	SELECT 
		[RID], [EventID], [Event], [EventTitle], [Phone], [Email], [FromPhoneNo], [AccountSID], [AuthToken], [EventDatetime], [SMSStatus], [EmailStatus], [CallStatus], [ClientKey], [RedirectLink], [EmailFrom], [EmailPassword], [EmailFromTitle], [EmailPort], [EmailSMTP], [CompanyLO], [IsStatus], [MainEventID], [IsTwilioStatus]
	FROM [dbo].[tbl_FinalReminderDetails]
	WHERE 
			[RID] = @RID
GO
/****** Object:  StoredProcedure [dbo].[sproc_FinalReminderDetails_SelectByField]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_FinalReminderDetails_SelectByField]
	@ClientKey varchar(100),
	@MainEventID varchar(1000)
AS

	DECLARE @query varchar(2000);

	SELECT [RID], [EventID], [Event], [EventTitle], [Phone], [Email], [FromPhoneNo], [AccountSID], [AuthToken], [EventDatetime], [SMSStatus], [EmailStatus], [CallStatus], [ClientKey], [RedirectLink], [EmailFrom], [EmailPassword], [EmailFromTitle], [EmailPort], [EmailSMTP], [CompanyLO], [IsStatus], [MainEventID], [IsTwilioStatus] FROM [dbo].[tbl_FinalReminderDetails] WHERE ClientKey = @ClientKey AND MainEventID = @MainEventID
GO
/****** Object:  StoredProcedure [dbo].[sproc_FinalReminderDetails_SelectAll]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_FinalReminderDetails_SelectAll]
AS

	SELECT 
		[RID], [EventID], [Event], [EventTitle], [Phone], [Email], [FromPhoneNo], [AccountSID], [AuthToken], [EventDatetime], [SMSStatus], [EmailStatus], [CallStatus], [ClientKey], [RedirectLink], [EmailFrom], [EmailPassword], [EmailFromTitle], [EmailPort], [EmailSMTP], [CompanyLO], [IsStatus], [MainEventID], [IsTwilioStatus]
	FROM [dbo].[tbl_FinalReminderDetails]
GO
/****** Object:  StoredProcedure [dbo].[sproc_FinalReminderDetails_Insert]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_FinalReminderDetails_Insert]                  
 @RID int output,                  
 @EventID nvarchar(50) = null ,                  
 @Event nvarchar(MAX) = null ,                  
 @EventTitle nvarchar(500) = null ,                  
 @Phone nvarchar(50) = null ,                  
 @Email nvarchar(50) = null ,                  
 @FromPhoneNo nvarchar(50) = null ,                  
 @AccountSID nvarchar(500) = null ,                  
 @AuthToken nvarchar(500) = null ,                  
 @EventDatetime datetime = null ,                  
 @SMSStatus int = null ,                  
 @EmailStatus int = null ,                  
 @CallStatus int = null ,                  
 @ClientKey nvarchar(500) = null ,                  
 @RedirectLink nvarchar(MAX) = null ,                  
 @EmailFrom nvarchar(500) = null ,                  
 @EmailPassword nvarchar(500) = null ,                  
 @EmailFromTitle nvarchar(500) = null ,                  
 @EmailPort nvarchar(50) = null ,                  
 @EmailSMTP nvarchar(100) = null ,                  
 @CompanyLO nvarchar(MAX) = null ,                  
 @IsStatus int = null ,                  
 @MainEventID int = null ,                  
 @IsTwilioStatus int = null ,          
 @AppointmentDatetime datetime = null,                
 @IsConfirmable int =null     ,          
 @TwilioLang nvarchar(50)=null          
                  
AS       
Begin --//Begin Start    

If not Exists (select MainEventID from [tbl_FinalReminderDetails] where MainEventID=@MainEventID and ClientKey=@ClientKey)
BEGIN
    
    
declare  @ReHMT int ,@ReInterval int  ,@ReCallorNot Int             
    
      
set @ReCallorNot = (select ReCallEnable from tbl_Email_Setting where ClientKey=@ClientKey )    
set @ReHMT =(SELECT RecallHMT from tbl_Email_Setting where ClientKey=@ClientKey)    
set @ReInterval =(SELECT RecallInterval from tbl_Email_Setting where ClientKey=@ClientKey)         
      
    
    
If(@CallStatus = 1) --//Only Call Recall Not SMS /Email    
BEGIN    
    
    
IF(@ReCallorNot=1)  --//Check if Recall Enable    
BEGIN     
    
    
Declare @SP int,@EP int,@FinalRecallEventDate DATETIME,@ResultAdd int    
                      
set @SP =0                                                                                   
set @EP =@ReHMT                                              
    
While(@EP > @SP)                                                                                      
Begin     
    
    
    
set @ResultAdd = (@SP * @ReInterval)    
    
set @FinalRecallEventDate =DATEADD(mi,@ResultAdd,@EventDatetime)    
    
    
                  
INSERT [dbo].[tbl_FinalReminderDetails]                  
(                  
 [EventID],                  
 [Event],                  
 [EventTitle],                  
 [Phone],                  
 [Email],                  
 [FromPhoneNo],                  
 [AccountSID],                  
 [AuthToken],                  
 [EventDatetime],                  
 [SMSStatus],                  
 [EmailStatus],                  
 [CallStatus],                  
 [ClientKey],                  
 [RedirectLink],                  
 [EmailFrom],                  
 [EmailPassword],                  
 [EmailFromTitle],                  
 [EmailPort],                  
 [EmailSMTP],                  
 [CompanyLO],                  
 [IsStatus],                  
 [MainEventID],                  
 [IsTwilioStatus],                  
 CreatedDttm,                  
 ModifiedDttm   ,    
 AppointmentDatetime,                
 IsConfirmable ,          
 TwilioLang                
                  
)                  
VALUES                  
(                  
 @EventID,                  
 @Event,                  
 @EventTitle,                  
 @Phone,                  
 @Email,                  
 @FromPhoneNo,                  
 @AccountSID,                  
 @AuthToken,                  
 @FinalRecallEventDate,                  
 @SMSStatus,                  
 @EmailStatus,                  
 @CallStatus,                  
 @ClientKey,                  
 @RedirectLink,                  
 @EmailFrom,                  
 @EmailPassword,                  
 @EmailFromTitle,                  
 @EmailPort,                  
 @EmailSMTP,                  
 @CompanyLO,                  
 @IsStatus,                  
 @MainEventID,                  
 @IsTwilioStatus,                  
 GETDATE(),                  
 GETDATE(),    
 @AppointmentDatetime,                
 @IsConfirmable  ,          
 @TwilioLang              
                  
)           
        
        
        
insert into [tbl_FinalReminderDetails_Log]        
SELECT * from tbl_FinalReminderDetails where RID=SCOPE_IDENTITY()       
               
SELECT @RID=SCOPE_IDENTITY();       
       
set @SP =@SP +1               
END    
    
END    
ELSE--//Check if Recall Disable    
BEGIN    
    
INSERT [dbo].[tbl_FinalReminderDetails]                  
(                  
 [EventID],                  
 [Event],                  
 [EventTitle],                  
 [Phone],                  
 [Email],                  
 [FromPhoneNo],                  
 [AccountSID],                  
 [AuthToken],                  
 [EventDatetime],                  
 [SMSStatus],                  
 [EmailStatus],                  
 [CallStatus],                  
 [ClientKey],                  
 [RedirectLink],                  
 [EmailFrom],                  
 [EmailPassword],                  
 [EmailFromTitle],                  
 [EmailPort],                  
 [EmailSMTP],                  
 [CompanyLO],                  
 [IsStatus],                  
 [MainEventID],                  
 [IsTwilioStatus],                  
 CreatedDttm,                  
 ModifiedDttm   ,    
 AppointmentDatetime,                
 IsConfirmable ,          
 TwilioLang                
                  
)                  
VALUES                  
(                  
 @EventID,                  
 @Event,                  
 @EventTitle,                  
 @Phone,                  
 @Email,                  
 @FromPhoneNo,                  
 @AccountSID,                  
 @AuthToken,                  
 @EventDatetime,                  
 @SMSStatus,                  
 @EmailStatus,                  
 @CallStatus,                  
 @ClientKey,                  
 @RedirectLink,                  
 @EmailFrom,                  
 @EmailPassword,                  
 @EmailFromTitle,                  
 @EmailPort,                  
 @EmailSMTP,                  
 @CompanyLO,                  
 @IsStatus,                  
 @MainEventID,                  
 @IsTwilioStatus,                  
 GETDATE(),                  
 GETDATE(),    
 @AppointmentDatetime,                
 @IsConfirmable  ,          
 @TwilioLang              
                  
)           
        
        
        
insert into [tbl_FinalReminderDetails_Log]        
SELECT * from tbl_FinalReminderDetails where RID=SCOPE_IDENTITY()       
               
SELECT @RID=SCOPE_IDENTITY();       
       
END      
    
     
END    
ELSE              --//Email /SMS Event    
BEGIN    
INSERT [dbo].[tbl_FinalReminderDetails]                  
(                  
 [EventID],                  
 [Event],                  
 [EventTitle],                  
 [Phone],                  
 [Email],                  
 [FromPhoneNo],                  
 [AccountSID],                  
 [AuthToken],                  
 [EventDatetime],                  
 [SMSStatus],                  
 [EmailStatus],                  
 [CallStatus],                  
 [ClientKey],                  
 [RedirectLink],                  
 [EmailFrom],                  
 [EmailPassword],                  
 [EmailFromTitle],                  
 [EmailPort],                  
 [EmailSMTP],                  
 [CompanyLO],                  
 [IsStatus],                  
 [MainEventID],                  
 [IsTwilioStatus],                  
 CreatedDttm,                  
 ModifiedDttm   ,    
 AppointmentDatetime,                
 IsConfirmable ,          
 TwilioLang                
                  
)                  
VALUES                  
(                  
 @EventID,                  
 @Event,                  
 @EventTitle,                  
 @Phone,     
 @Email,                  
 @FromPhoneNo,                  
 @AccountSID,                  
 @AuthToken,                  
 @EventDatetime,                  
 @SMSStatus,                  
 @EmailStatus,                  
 @CallStatus,                  
 @ClientKey,                  
 @RedirectLink,                  
 @EmailFrom,                  
 @EmailPassword,                  
 @EmailFromTitle,                  
 @EmailPort,                  
 @EmailSMTP,                  
 @CompanyLO,                  
 @IsStatus,                  
 @MainEventID,                  
 @IsTwilioStatus,                  
 GETDATE(),                  
 GETDATE(),    
 @AppointmentDatetime,                
 @IsConfirmable  ,          
 @TwilioLang              
                  
)           
        
        
        
insert into [tbl_FinalReminderDetails_Log]        
SELECT * from tbl_FinalReminderDetails where RID=SCOPE_IDENTITY()       
               
SELECT @RID=SCOPE_IDENTITY();       
END      
      
END
      
    
    
    
      
END
GO
/****** Object:  StoredProcedure [dbo].[sproc_FinalReminderDetails_DeleteByPrimaryKey]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_FinalReminderDetails_DeleteByPrimaryKey]
	@RID int
AS

DELETE FROM [dbo].[tbl_FinalReminderDetails]
 WHERE 
	[RID] = @RID
GO
/****** Object:  StoredProcedure [dbo].[sproc_FinalReminderDetails_DeleteByField]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sproc_FinalReminderDetails_DeleteByField]
	@ClientKey varchar(100),
	@MainEventID varchar(1000)
AS


	DELETE FROM [dbo].[tbl_FinalReminderDetails] WHERE ClientKey = @ClientKey AND MainEventID = @MainEventID
GO
/****** Object:  StoredProcedure [dbo].[Sproc_A1synctoRS]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sproc_A1synctoRS]  
@Status int  
as  
Begin   
  
  
update tbl_Email_Setting set  A1Sync=@Status 


If(@Status=1)
BEGIN
	if( 0 >= (select Count(*) as Total  from tbl_Calaudit_Last_Sync))  
	BEGIN  
	  
	 Insert INTO tbl_Calaudit_Last_Sync (LastSync, CreatedDatetime, ModifiedDatetime)  
	 VALUES(GETDATE(),GETDATE(),GETDATE())  
	  
	  
	  
	END  
	ELSE  
	BEGIN  
	  
	 UPDATE tbl_Calaudit_Last_Sync set LastSync =GETDATE(),ModifiedDatetime=GETDATE()  
	  
	  
	END  

END 
  
  
END

--TRUNCATE table tbl_Calaudit_Last_Sync
GO
/****** Object:  UserDefinedFunction [dbo].[InsertReminderFinal_API]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from [InsertReminderFinal](1,'C73F-DDDA-BB96-4ACF-BA6D')                    
                    
CREATE FUNCTION [dbo].[InsertReminderFinal_API] (@LastId  int,@ClientKey Nvarchar(200))                    
RETURNS   @Result TABLE (                    
EventID int, Event nvarchar(MAX) , EventTitle nvarchar(500), Phone nvarchar(50), Email nvarchar(500),                     
FromPhoneNo nvarchar(50), AccountSID nvarchar(500), AuthToken nvarchar(500), EventDatetime datetime, SMSStatus Int,                    
EmailStatus int, CallStatus int, ClientKey nvarchar(100), RedirectLink nvarchar(MAX),                     
EmailFrom nvarchar(500), EmailPassword nvarchar(500), EmailFromTitle nvarchar(MAX),                    
EmailPort nvarchar(500), EmailSMTP nvarchar(500), CompanyLO nvarchar(MAX), IsStatus int,                    
MainEventID int,IsTwilioStatus int,[SID] nvarchar(500),CreatedDatetime Datetime,ModifiedDatetime Datetime,                    
AppointmentDatetime Datetime,IsConfirmable int,TwilioLang Nvarchar(20)                    
                    
                    
)                    
AS                    
BEGIN                    
                  
Declare @FINDNEWFlag Nvarchar(50)                  
                  
set @FINDNEWFlag=(select CardcodeFK from tbl_LRS_Reminder RR where RR.eventno=@LastId)                  
                    
if(@FINDNEWFlag not like '%RS%' )                   
Begin                  
                    
insert Into @Result                    
SELECT LRT.TimingID as EventID,                    
            
Case When LRT.ReminderType in ('SMS','CALL') THEN                      
replace(replace(replace(replace(replace(LR.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')) ,                              
'{EventDatetime}',                    
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                       
(CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,birth_date, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)+' '+RIGHT(Convert(VARCHAR(20),birth_date,100),2)
  
    
      
        
          
            
                                              
else                                 
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                                    
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.EventParties)            
                    
ELSE                     
                    
replace(replace(replace(replace(replace(LR.EventHTML,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')) ,                              
'{EventDatetime}',Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                       
(CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,birth_date, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)+' '+ RIGHT(Convert(VARCHAR(20),birth_date,100),2
  
    
      
        
          
)                    
else                                 
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                  
                              
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.EventParties)   END                  
        
                    
 AS [Event],                    
                    
                    
LR.location,       
case when    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')                     
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then                     
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''              then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)                    
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')                     
else isnull(LTRIM(RTRIM(C.car)),'') end) end as Phone,                 
                 
LTRIM(RTRIM(C.email)) AS Email,                     
(select ES.Twilio_CALL from tbl_Email_Setting as  ES) as FromPhoneNo,                    
(select ES.Twilio_AccountSid from tbl_Email_Setting as  ES) AS AccountSID,                                    
(select ES.Twilio_AuthToken from tbl_Email_Setting as  ES)   AS AuthToken,                    
                    
                    
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                         
CONVERT(VARCHAR, birth_date, 120) else  CONVERT(VARCHAR,LRT.ReminderDatetime, 120)  End as [EventDateTime],                    
                   
                    
Case When LRT.ReminderType='SMS' Then 1 ELSE 0 END As SMSStatus,                       
Case When LRT.ReminderType='EMAIL' Then 1 ELSE 0 END As EmailStatus,                    
Case When LRT.ReminderType='CALL' Then 1 ELSE 0 END As CallStatus,                   
@ClientKey as ClientKey,                    
'http://62.151.183.51:81/Index.aspx?UID='+Convert(Nvarchar,LRT.TimingID) as RedirectLink,                    
(select ES.EmailFrom from tbl_Email_Setting ES) as EmailFrom,                                    
                                    
(select ES.EmailPassword from tbl_Email_Setting ES) as EmailPassword,                                    
                                    
(select ES.EmailFromTitle from tbl_Email_Setting ES) as EmailFromTitle,                                    
                                     
(select ES.EmailPort from tbl_Email_Setting ES) as EmailPort,                                    
                                    
(select ES.EmailSMTP from tbl_Email_Setting ES) as EmailSMTP ,                    
                          
'' as EmailCompanyLogo  ,0 as [Status],LR.eventno as MainEventID                    
,0 as ISTwilioStatus,'' as [SID],GETDATE(),GETDATE(),LR.[date],LR.IsConfirmable  ,LR.TwilioLang                  
                    
from tbl_LRS_Reminder LR                    
INNER join tbl_LRS_Reminder_Timing LRT On LRT.EventFK=LR.eventno                    
inner join [Card] C on Convert(Nvarchar(50),C.CardCode) =Convert(Nvarchar(50),LR.CardcodeFK)                     
inner join Card2 C2 On Convert(Nvarchar(50),C2.firmcode)=Convert(Nvarchar(50),C.firmcode)                   
where LR.eventno=@LastId                    
and  isnull(LRT.Isactive,0) <> 1                      
                    
 END                  
 ELSE                  
 BEGIN                  
                   
insert Into @Result                    
SELECT LRT.TimingID as EventID,                    
Case When LRT.ReminderType in ('SMS','CALL') THEN               
                   
replace(replace(replace(replace(replace(LR.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.FName)),'')+' '+ISNULL(LTRIM(RTRIM(C.LName)),'')) ,                              
'{EventDatetime}',                    
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                       
(CONVERT(VARCHAR(5), C.DOB, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,C.DOB, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, C.DOB)), 2)+' '+RIGHT(Convert(VARCHAR(20),C.DOB,100),2)                  
                                 
else                                 
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                  
                              
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.AppointmentType)          
                    
ELSE                     
                    
replace(replace(replace(replace(replace(LR.EventHTML,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.FName)),'')+' '+ISNULL(LTRIM(RTRIM(c.LName)),'')) ,                              
'{EventDatetime}',Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                       
(CONVERT(VARCHAR(5), C.DOB, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,C.DOB, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, C.DOB)), 2)+' '+ RIGHT(Convert(VARCHAR(20),C.DOB,100),2                  
)                
else                                 
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                  
                              
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.AppointmentType)      END                    
                    
                    
 AS [Event],                    
                    
                    
LR.location,                    
    
Case when C.PhoneNo not LIKE '%+91%' and C.PhoneNo not LIKE '%+1%' then    
LTRIM(RTRIM(isnull(C.CountryCode,'')))+LTRIM(RTRIM(isnull(C.PhoneNo,''))) else LTRIM(RTRIM(isnull(C.PhoneNo,''))) end as phone,    
    
                                
LTRIM(RTRIM(C.email)) AS Email,                     
(select ES.Twilio_CALL from tbl_Email_Setting as  ES) as FromPhoneNo,                    
(select ES.Twilio_AccountSid from tbl_Email_Setting as  ES) AS AccountSID,                                    
(select ES.Twilio_AuthToken from tbl_Email_Setting as  ES)   AS AuthToken,                    
                    
                    
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                         
CONVERT(VARCHAR, C.DOB, 120) else  CONVERT(VARCHAR,LRT.ReminderDatetime, 120)  End as [EventDateTime],                    
                   
                    
Case When LRT.ReminderType='SMS' Then 1 ELSE 0 END As SMSStatus,                       
Case When LRT.ReminderType='EMAIL' Then 1 ELSE 0 END As EmailStatus,                    
Case When LRT.ReminderType='CALL' Then 1 ELSE 0 END As CallStatus,                     
@ClientKey as ClientKey,                    
'http://62.151.183.51:81/Index.aspx?UID='+Convert(Nvarchar,LRT.TimingID) as RedirectLink,                    
(select ES.EmailFrom from tbl_Email_Setting ES) as EmailFrom,                                    
                                    
(select ES.EmailPassword from tbl_Email_Setting ES) as EmailPassword,                                    
                                    
(select ES.EmailFromTitle from tbl_Email_Setting ES) as EmailFromTitle,                                    
                                     
(select ES.EmailPort from tbl_Email_Setting ES) as EmailPort,                                    
                                    
(select ES.EmailSMTP from tbl_Email_Setting ES) as EmailSMTP ,                    
                          
'' as EmailCompanyLogo  ,0 as [Status],LR.eventno as MainEventID                    
,0 as ISTwilioStatus,'' as [SID],GETDATE(),GETDATE(),LR.[date],LR.IsConfirmable  ,LR.TwilioLang                  
                    
from tbl_LRS_Reminder LR                    
INNER join tbl_LRS_Reminder_Timing LRT On LRT.EventFK=LR.eventno                    
inner join tbl_RS_Contactlist C on Convert(Nvarchar(50),C.CardCode) =Convert(Nvarchar(50),LR.CardcodeFK)
inner join tbl_Apptoto_AddSmsEmail TAA On TAA.Name=AppointmentType
                 
                  
where LR.eventno=@LastId                    
and  isnull(LRT.Isactive,0) <> 1                     
                  
 END                  
                    
RETURN                     
END
GO
/****** Object:  UserDefinedFunction [dbo].[InsertReminderFinal_A1LAW]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from [InsertReminderFinal](1,'C73F-DDDA-BB96-4ACF-BA6D')              
              
CREATE FUNCTION [dbo].[InsertReminderFinal_A1LAW] (@LastId  int,@ClientKey Nvarchar(200),@Phone Nvarchar(200),@Email Nvarchar(200),@first Nvarchar(200),@last Nvarchar(200))              
RETURNS   @Result TABLE (              
EventID int, Event nvarchar(MAX) , EventTitle nvarchar(500), Phone nvarchar(50), Email nvarchar(500),               
FromPhoneNo nvarchar(50), AccountSID nvarchar(500), AuthToken nvarchar(500), EventDatetime datetime, SMSStatus Int,              
EmailStatus int, CallStatus int, ClientKey nvarchar(100), RedirectLink nvarchar(MAX),               
EmailFrom nvarchar(500), EmailPassword nvarchar(500), EmailFromTitle nvarchar(MAX),              
EmailPort nvarchar(500), EmailSMTP nvarchar(500), CompanyLO nvarchar(MAX), IsStatus int,              
MainEventID int,IsTwilioStatus int,[SID] nvarchar(500),CreatedDatetime Datetime,ModifiedDatetime Datetime,              
AppointmentDatetime Datetime,IsConfirmable int,TwilioLang Nvarchar(20)              
              
              
)              
AS              
BEGIN              
            
           
              
insert Into @Result              
SELECT LRT.TimingID as EventID,              
      
Case When LRT.ReminderType in ('SMS','CALL') THEN                
replace(replace(replace(replace(replace(LR.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(@first)),'')+' '+ISNULL(LTRIM(RTRIM(@last)),'')) ,                        
'{EventDatetime}',        
    
          
                           
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) )                              
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.EventParties)      
              
ELSE               
              
replace(replace(replace(replace(replace(LR.EventHTML,'{EventUserName}',ISNULL(LTRIM(RTRIM(@first)),'')+' '+ISNULL(LTRIM(RTRIM(@last)),'')) ,                        
'{EventDatetime}',        
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) )            
                        
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.EventParties)   END            
              
              
 AS [Event],              
              
              
LR.location,              
@Phone as Phone,           
           
LTRIM(RTRIM(@Email)) AS Email,               
(select ES.Twilio_CALL from tbl_Email_Setting as  ES) as FromPhoneNo,              
(select ES.Twilio_AccountSid from tbl_Email_Setting as  ES) AS AccountSID,                              
(select ES.Twilio_AuthToken from tbl_Email_Setting as  ES)   AS AuthToken,              
              
              
CONVERT(VARCHAR,LRT.ReminderDatetime, 120) as [EventDateTime],              
             
              
Case When LRT.ReminderType='SMS' Then 1 ELSE 0 END As SMSStatus,                 
Case When LRT.ReminderType='EMAIL' Then 1 ELSE 0 END As EmailStatus,              
Case When LRT.ReminderType='CALL' Then 1 ELSE 0 END As CallStatus,             
@ClientKey as ClientKey,              
'http://62.151.183.51:81/Index.aspx?UID='+Convert(Nvarchar,LRT.TimingID) as RedirectLink,              
(select ES.EmailFrom from tbl_Email_Setting ES) as EmailFrom,                              
                              
(select ES.EmailPassword from tbl_Email_Setting ES) as EmailPassword,                              
                              
(select ES.EmailFromTitle from tbl_Email_Setting ES) as EmailFromTitle,                              
                               
(select ES.EmailPort from tbl_Email_Setting ES) as EmailPort,                        
                              
(select ES.EmailSMTP from tbl_Email_Setting ES) as EmailSMTP ,              
             
'' as EmailCompanyLogo  ,0 as [Status],LR.eventno as MainEventID              
,0 as ISTwilioStatus,'' as [SID],GETDATE(),GETDATE(),LR.[date],LR.IsConfirmable  ,LR.TwilioLang            
              
from tbl_LRS_Reminder LR              
INNER join tbl_LRS_Reminder_Timing LRT On LRT.EventFK=LR.eventno              
           
where LR.eventno=@LastId              
and  isnull(LRT.Isactive,0) <> 1                
              
           
           
              
RETURN               
END
GO
/****** Object:  UserDefinedFunction [dbo].[InsertReminderFinal]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from [InsertReminderFinal](1,'C73F-DDDA-BB96-4ACF-BA6D')                  
                  
CREATE FUNCTION [dbo].[InsertReminderFinal] (@LastId  int,@ClientKey Nvarchar(200))                  
RETURNS   @Result TABLE (                  
EventID int, Event nvarchar(MAX) , EventTitle nvarchar(500), Phone nvarchar(50), Email nvarchar(500),                   
FromPhoneNo nvarchar(50), AccountSID nvarchar(500), AuthToken nvarchar(500), EventDatetime datetime, SMSStatus Int,                  
EmailStatus int, CallStatus int, ClientKey nvarchar(100), RedirectLink nvarchar(MAX),                   
EmailFrom nvarchar(500), EmailPassword nvarchar(500), EmailFromTitle nvarchar(MAX),                  
EmailPort nvarchar(500), EmailSMTP nvarchar(500), CompanyLO nvarchar(MAX), IsStatus int,                  
MainEventID int,IsTwilioStatus int,[SID] nvarchar(500),CreatedDatetime Datetime,ModifiedDatetime Datetime,                  
AppointmentDatetime Datetime,IsConfirmable int,TwilioLang Nvarchar(20)                  
                  
                  
)                  
AS                  
BEGIN                  
                
Declare @FINDNEWFlag Nvarchar(50)                
                
set @FINDNEWFlag=(select CardcodeFK from tbl_LRS_Reminder RR where RR.eventno=@LastId)                
                  
if(@FINDNEWFlag not like '%RS%' )                 
Begin                
                  
insert Into @Result                  
SELECT LRT.TimingID as EventID,                  
          
Case When LRT.ReminderType in ('SMS','CALL') THEN                    
replace(replace(replace(replace(replace(LR.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')) ,                            
'{EventDatetime}',                  
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                     
(CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,birth_date, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)+' '+RIGHT(Convert(VARCHAR(20),birth_date,100),2)
  
    
      
        
          
                                            
else                               
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                                  
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.EventParties)          
                  
ELSE                   
                  
replace(replace(replace(replace(replace(LR.EventHTML,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')) ,                            
'{EventDatetime}',Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                     
(CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,birth_date, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)+' '+ RIGHT(Convert(VARCHAR(20),birth_date,100),2
  
    
      
        
)                  
else                               
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                
                            
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.EventParties)   END                
                  
                  
 AS [Event],                  
                  
                  
LR.location,     
case when    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')                   
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then                   
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''              then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)                  
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')                   
else isnull(LTRIM(RTRIM(C.car)),'') end) end as Phone,               
               
LTRIM(RTRIM(C.email)) AS Email,                   
(select ES.Twilio_CALL from tbl_Email_Setting as  ES) as FromPhoneNo,                  
(select ES.Twilio_AccountSid from tbl_Email_Setting as  ES) AS AccountSID,                                  
(select ES.Twilio_AuthToken from tbl_Email_Setting as  ES)   AS AuthToken,                  
                  
                  
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                       
CONVERT(VARCHAR, birth_date, 120) else  CONVERT(VARCHAR,LRT.ReminderDatetime, 120)  End as [EventDateTime],                  
                 
                  
Case When LRT.ReminderType='SMS' Then 1 ELSE 0 END As SMSStatus,                     
Case When LRT.ReminderType='EMAIL' Then 1 ELSE 0 END As EmailStatus,                  
Case When LRT.ReminderType='CALL' Then 1 ELSE 0 END As CallStatus,                 
@ClientKey as ClientKey,                  
'http://62.151.183.51:81/Index.aspx?UID='+Convert(Nvarchar,LRT.TimingID) as RedirectLink,                  
(select ES.EmailFrom from tbl_Email_Setting ES) as EmailFrom,                                  
                                  
(select ES.EmailPassword from tbl_Email_Setting ES) as EmailPassword,                                  
                                  
(select ES.EmailFromTitle from tbl_Email_Setting ES) as EmailFromTitle,                                  
                                   
(select ES.EmailPort from tbl_Email_Setting ES) as EmailPort,                                  
                                  
(select ES.EmailSMTP from tbl_Email_Setting ES) as EmailSMTP ,                  
                        
'' as EmailCompanyLogo  ,0 as [Status],LR.eventno as MainEventID                  
,0 as ISTwilioStatus,'' as [SID],GETDATE(),GETDATE(),LR.[date],LR.IsConfirmable  ,LR.TwilioLang                
                  
from tbl_LRS_Reminder LR                  
INNER join tbl_LRS_Reminder_Timing LRT On LRT.EventFK=LR.eventno                  
inner join [Card] C on Convert(Nvarchar(50),C.CardCode) =Convert(Nvarchar(50),LR.CardcodeFK)                   
inner join Card2 C2 On Convert(Nvarchar(50),C2.firmcode)=Convert(Nvarchar(50),C.firmcode)                 
where LR.eventno=@LastId                  
and  isnull(LRT.Isactive,0) <> 1                    
                  
 END                
 ELSE                
 BEGIN                
                 
 insert Into @Result                  
SELECT LRT.TimingID as EventID,                  
Case When LRT.ReminderType in ('SMS','CALL') THEN             
                 
replace(replace(replace(replace(replace(LR.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.FName)),'')+' '+ISNULL(LTRIM(RTRIM(C.LName)),'')) ,                            
'{EventDatetime}',                  
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                     
(CONVERT(VARCHAR(5), C.DOB, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,C.DOB, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, C.DOB)), 2)+' '+RIGHT(Convert(VARCHAR(20),C.DOB,100),2)                
                                                    
else                               
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                
                            
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.AppointmentType)        
                  
ELSE                   
                  
replace(replace(replace(replace(replace(LR.EventHTML,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.FName)),'')+' '+ISNULL(LTRIM(RTRIM(c.LName)),'')) ,                            
'{EventDatetime}',Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                     
(CONVERT(VARCHAR(5), C.DOB, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,C.DOB, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, C.DOB)), 2)+' '+ RIGHT(Convert(VARCHAR(20),C.DOB,100),2                
)              
else                               
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                
                            
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.AppointmentType)      END                  
                  
                  
 AS [Event],                  
                  
                  
LR.location,                  
  
Case when C.PhoneNo not LIKE '%+91%' and C.PhoneNo not LIKE '%+1%' then  
LTRIM(RTRIM(isnull(C.CountryCode,'')))+LTRIM(RTRIM(isnull(C.PhoneNo,''))) else LTRIM(RTRIM(isnull(C.PhoneNo,''))) end as phone,  
  
                              
LTRIM(RTRIM(C.email)) AS Email,                   
(select ES.Twilio_CALL from tbl_Email_Setting as  ES) as FromPhoneNo,                  
(select ES.Twilio_AccountSid from tbl_Email_Setting as  ES) AS AccountSID,                                  
(select ES.Twilio_AuthToken from tbl_Email_Setting as  ES)   AS AuthToken,                  
                  
                  
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                       
CONVERT(VARCHAR, C.DOB, 120) else  CONVERT(VARCHAR,LRT.ReminderDatetime, 120)  End as [EventDateTime],                  
                 
                  
Case When LRT.ReminderType='SMS' Then 1 ELSE 0 END As SMSStatus,                     
Case When LRT.ReminderType='EMAIL' Then 1 ELSE 0 END As EmailStatus,                  
Case When LRT.ReminderType='CALL' Then 1 ELSE 0 END As CallStatus,                   
@ClientKey as ClientKey,                  
'http://62.151.183.51:81/Index.aspx?UID='+Convert(Nvarchar,LRT.TimingID) as RedirectLink,                  
(select ES.EmailFrom from tbl_Email_Setting ES) as EmailFrom,                                  
                                  
(select ES.EmailPassword from tbl_Email_Setting ES) as EmailPassword,                                  
                                  
(select ES.EmailFromTitle from tbl_Email_Setting ES) as EmailFromTitle,                                  
                                   
(select ES.EmailPort from tbl_Email_Setting ES) as EmailPort,                                  
                                  
(select ES.EmailSMTP from tbl_Email_Setting ES) as EmailSMTP ,                  
                        
'' as EmailCompanyLogo  ,0 as [Status],LR.eventno as MainEventID                  
,0 as ISTwilioStatus,'' as [SID],GETDATE(),GETDATE(),LR.[date],LR.IsConfirmable  ,LR.TwilioLang                
                  
from tbl_LRS_Reminder LR                  
INNER join tbl_LRS_Reminder_Timing LRT On LRT.EventFK=LR.eventno                  
inner join tbl_RS_Contactlist C on Convert(Nvarchar(50),C.CardCode) =Convert(Nvarchar(50),LR.CardcodeFK)                   
                
where LR.eventno=@LastId                  
and  isnull(LRT.Isactive,0) <> 1                   
                
 END                
                  
RETURN                   
END
GO
/****** Object:  StoredProcedure [dbo].[InsertGoogleContact]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[InsertGoogleContact]  
@UserID NVARCHAR(50),
@EmailID NVARCHAR(MAX) =NULL  
AS  
BEGIN  
  
  
IF NOT EXISTS(SELECT ContactEmail FROM tbl_Apptoto_UserContact where ContactEmail=@EmailID)  
BEGIN  
  
 INSERT INTO tbl_Apptoto_UserContact  
 SELECT @UserID,@EmailID,0  
 
 SELECT 1 AS Result
  
END  
   
   
END
GO
/****** Object:  StoredProcedure [dbo].[InsertGoogle]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[InsertGoogle]
@EmailID NVARCHAR(MAX) =NULL  
AS  
BEGIN  
  
  
	IF NOT EXISTS(SELECT Email FROM tbl_Apptoto_User where Email=@EmailID AND ISNULL(Isdelete,0) <> 1)  
	BEGIN  
	  
		 INSERT INTO tbl_Apptoto_User  
		 SELECT '',@EmailID,0  
		 
		 SELECT 1 AS Result,SCOPE_IDENTITY() AS UserID
	  
	END
	ELSE
	BEGIN
	
		SELECT ID AS UserID FROM tbl_Apptoto_User where Email=@EmailID AND ISNULL(Isdelete,0) <> 1
	
	END  
	   
   
END
GO
/****** Object:  StoredProcedure [dbo].[InsertBulkTiming]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[InsertBulkTiming]
@SMS int,
@CALL int,
@EMAIL int,
@SMSTiming Nvarchar(500),
@CALLTiming Nvarchar(500) ,
@EMAILTiming Nvarchar(500) ,
@TID int

As
Begin

Update tbl_bulk_appointment set SMS=@SMS,SMSWhen=@SMSTiming,CALL=@CALL,CALLWhen=@CALLTiming,
EMAIL=@EMAIL,EMAILWhen=@EMAILTiming,ModifiedDatetime=GETDATE() where TID_FK=@TID


delete from tbl_bulk_appointmenttiming where TID_FK=@TID


--// SMS-Manipulation

select * into #TempSMS from dbo.SplitByComma(@SMSTiming)  

select ROW_NUMBER() over(order by (select 1)) as No,Id, smsTime, Isactive,'SMS'as RTYPE into #TempSMSFINAL                       
from #TempSMS T                      
inner Join lst_SMS_Timing LST on LST.ID =T.Column1       

Declare @SMSStart nvarchar(10),@SMSEnd nvarchar(10),@SMSWhen nvarchar(100),@SMSTYPE Nvarchar(100)                      
                
set @SMSStart =(select MIN(NO) from #TempSMSFINAL)                      
set @SMSEnd =(select MAX(NO) from #TempSMSFINAL)                      
                 
While(@SMSEnd >= @SMSStart)                      
Begin                      
set @SMSWhen =(select top 1 smsTime from #TempSMSFINAL where No=@SMSStart)                      
set @SMSTYPE =(select top 1 RTYPE from #TempSMSFINAL where No=@SMSStart)                      
                
	               
	Insert Into tbl_bulk_appointmenttiming                      
	Select @TID,@SMSWhen,@SMSTYPE,0,Getdate(),Getdate()                 
	               
	set @SMSStart =@SMSStart+1       
	               
END                       


--// CALL-Manipulation


select * into #TempCALL from dbo.SplitByComma(@CALLTiming) 

 select ROW_NUMBER() over(order by (select 1)) as No,Id, smsTime, Isactive,'CALL'as RTYPE into #TempCALLFINAL                       
  from #TempCALL T                      
  inner Join lst_SMS_Timing LST on LST.ID =T.Column1                        
                        
  Declare @CALLStart nvarchar(10),@CALLEnd nvarchar(10),@CALLWhen nvarchar(100),@CALLTYPE Nvarchar(100)                      
                        
  set @CALLStart =(select MIN(NO) from #TempCALLFINAL)                      
  set @CALLEnd =(select MAX(NO) from #TempCALLFINAL)                      
                         
 While(@CALLEnd >= @CALLStart)                      
 Begin                      
  set @CALLWhen =(select top 1 smsTime from #TempCALLFINAL where No=@CALLStart)                      
  set @CALLTYPE =(select top 1 RTYPE from #TempCALLFINAL where No=@CALLStart)                      
                        
                       
	Insert Into tbl_bulk_appointmenttiming                      
	Select @TID,@CALLWhen,@CALLTYPE,0,Getdate(),Getdate() 
                       
  set @CALLStart =@CALLStart+1   
                     
 END    

--// EMAIL-Manipulation
  
select * into #TempEMAIL from dbo.SplitByComma(@EMAILTiming)   
	select ROW_NUMBER() over(order by (select 1)) as No,Id, smsTime, Isactive,'EMAIL'as RTYPE into #TempEMAILFINAL                       
	from #TempEMAIL T                      
	inner Join lst_SMS_Timing LST on LST.ID =T.Column1                        
                        
  Declare @EMAILStart nvarchar(10),@EMAILEnd nvarchar(10),@EMAILWhen nvarchar(100),@EMAILTYPE Nvarchar(100)                                              
  set @EMAILStart =(select MIN(NO) from #TempEMAILFINAL)                      
  set @EMAILEnd =(select MAX(NO) from #TempEMAILFINAL)                      
                         
 While(@EMAILEnd >= @EMAILStart)                      
 Begin                      
  set @EMAILWhen =(select top 1 smsTime from #TempEMAILFINAL where No=@EMAILStart)                      
  set @EMAILTYPE =(select top 1 RTYPE from #TempEMAILFINAL where No=@EMAILStart)                      
                        
                       
  Insert Into tbl_bulk_appointmenttiming                      
  Select @TID,@EMAILWhen,@EMAILTYPE,0,Getdate(),Getdate() 
                       
  set @EMAILStart =@EMAILStart+1                      
 END         



select 1 as Result

End
GO
/****** Object:  StoredProcedure [dbo].[Insert_TimeZone]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Insert_TimeZone]

@TimeZone int
AS
Begin

Update tbl_Email_Setting set TimeZone=@TimeZone 

SELECT 1 as Result

END
GO
/****** Object:  StoredProcedure [dbo].[Insert_RegistrationDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from tbl_RS_Registration    
    
CREATE Proc [dbo].[Insert_RegistrationDetails]      
@CompanyName Nvarchar(500),      
@Mobile nvarchar(50),      
@Email Nvarchar(200),      
@Key Nvarchar(50),      
@MACID nvarchar(50)      
      
as      
Begin      
    
Declare  @ClientID nvarchar(100),@UPdatedClientID nvarchar(100)    
      
   
  
Insert Into tbl_RS_Registration         
(RSClientID, CompanyName, Mobile, Email, ProductKey, MacID, ISActivated, CreatedDtTm, ModifiedDtTm    
)        
VALUES        
(NULL,@CompanyName,@Mobile,@Email,@Key,@MACID,0,Getdate(),getdate())   
    
    
set @ClientID =SCOPE_IDENTITY()    
    
    
set @UPdatedClientID =(select dbo.[RS_GetClientID] (@ClientID))    
     
    
Update tbl_RS_Registration SET RSClientID=@UPdatedClientID where RID=@ClientID    
      
SELECT 1 As Result      
      
END
GO
/****** Object:  StoredProcedure [dbo].[ImportContactReport_RS]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[ImportContactReport_RS]
as
Begin
SELECT distinct C.salutation,C.first,'',C.last,

case when CC.phone1='' then C.business else CC.phone1 end ,C.email,CC.zip,'Import Contact From RS','Mobile' from Card C
inner join [card2] cc On cc.firmcode=C.firmcode
inner Join [casecard] CCC on CCC.cardcode=C.cardcode
Inner join [case] CCCC On CCCC.caseno=CCC.caseno
where C.type in ('RS','LRS') and phone1 is Not Null and phone1 <> '' and first <> ''

end
GO
/****** Object:  StoredProcedure [dbo].[GetTwilioLanguage]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetTwilioLanguage]  
As  
Begin  
  
 SELECT     LID, Language, LanguageValue FROM         lst_TwilioLanguage  where isnull(isActive,0)=0

  
End
GO
/****** Object:  StoredProcedure [dbo].[GetTempleteWithBOdy]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC GetTempleteWithBOdy 'Appointment New''s'          
          
CREATE Proc [dbo].[GetTempleteWithBOdy]            
@TempleteName Nvarchar(500)          
          
          
as                
Begin                
                
set @TempleteName = Replace(@TempleteName, '', '''')           
                
select Body,Purpose,IL.Language, Taa.isSMS,TAA.isCALL,TAA.isEMAIL,TAA.whenSMS,TAA.whenCALL,TAA.whenEMAIL,TAA.IsReschedule

from tbl_Apptoto_AddSmsEmail  TAA    
Left join lst_TwilioLanguage IL On IL.LID =isnull(TAA.TwilioLang, 6)   
where   Name =@TempleteName          
                
End
GO
/****** Object:  StoredProcedure [dbo].[GetTemplete]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetTemplete]    
as        
Begin        
        
select ID,Name  from tbl_Apptoto_AddSmsEmail 
--chirag       
      
End
GO
/****** Object:  StoredProcedure [dbo].[GetTemplate]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetTemplate]  
As  
Begin  
select TID as TID, '{'+Template+'}' as Templete from lst_eventtemplete  
  
End
GO
/****** Object:  StoredProcedure [dbo].[GetsystemUserName]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetsystemUserName]  
@MACID Nvarchar(50)  
as  
Begin  
select '' as Name from tbl_RS_Registration where MacID=@MACID  
  
End
GO
/****** Object:  StoredProcedure [dbo].[GetSMStiming]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetSMStiming]        
As        
Begin        
  
SELECT '0' as SMSID,'Immediate' as SMSTIME  
  
UNION ALL  
        
SELECT  Id as SMSID,smsTime as SMSTIME  FROM lst_SMS_Timing   where isnull(Isactive,0)=0      
  
  
        
End
GO
/****** Object:  StoredProcedure [dbo].[GetSMSStatusONOFF]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetSMSStatusONOFF]  
as  
Begin  
  
select ROW_NUMBER() over(order by (select 1)) as No,ID,Category,Status as [Status],Purpose,Body from tbl_Apptoto_AddSmsEmail  
  
End  
  
  
--Update   tbl_Apptoto_AddSmsEmail set Status=0,Purpose='Birthdays' where ID=4  
--Update   tbl_Apptoto_AddSmsEmail set Status=1,Purpose='Hearing / Court Date' where ID=6  
--Update   tbl_Apptoto_AddSmsEmail set Status=1,Purpose='Birthdays' where ID=7
GO
/****** Object:  StoredProcedure [dbo].[GETMaster]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GETMaster]
@CaseNO Int
as
Begin

--Declare @CardCode Nvarchar(50),@FirmCode Nvarchar(50),@Eamsref Nvarchar(50)

----set @CardCode =( select Card from cal1 where Caseno in (@CaseNO))
--set @CardCode =( select Cardcode from  casecard where cardcode in (@CaseNO))
--set @FirmCode =(select firmcode from [card] where firmcode in (@CardCode))
--set @Eamsref =(select Eamsref from card2 where firmcode in (@FirmCode))
--select * from card3 where Eamsref in (@Eamsref)


select cc.caseno,C.cardcode,C2.firmcode,C3.eamsref from casecard CC
Left Outer join [card] C On C.cardcode =CC.cardcode
Left Outer  join  card2 C2 on C2.firmcode =C.firmcode
Left Outer  join card3 C3 on C3.eamsref =C2.eamsref
Where cc.caseno =@CaseNO

End
GO
/****** Object:  StoredProcedure [dbo].[GetListBulktype]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetListBulktype]
As
Begin
select TID,TypeName  from tbl_bulk_appointmenttype

End
GO
/****** Object:  StoredProcedure [dbo].[GetGoogleContact]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetGoogleContact]  
@EmailID NVARCHAR(500)
AS  
BEGIN  
  
SELECT ContactEmail FROM tbl_Apptoto_UserContact TAC
INNER JOIN  tbl_Apptoto_User TAU ON TAU.ID =TAC.AppToto_UserFK
WHERE TAU.Email =@EmailID
  
END
GO
/****** Object:  StoredProcedure [dbo].[GetErrorDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[GetErrorDetails]
As
Begin	

SELECT     EventTitle, AppointmentDatetime, EventBody, Firstname, Lastname, Mobile, EmailID, AppointmentType, IsCorrect, ErrorMsg
FROM         tblUploadedExcelAppointment
where isnull(IsCorrect,0)=1 and ErrorMsg is Not NULL

END
GO
/****** Object:  StoredProcedure [dbo].[GetCountryCode]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetCountryCode]    
AS    
BEGIN    
    
 select CountryCode FROM [lst_CountryCode]
 order by   CountryCode
 SELECT 1 as result  
    
END
GO
/****** Object:  StoredProcedure [dbo].[GetCalanderIteamList]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[GetCalanderIteamList]
AS
BEGIN

SELECT     ID, StartTime, EndTime, Title, A, R, G, B FROM  tbl_TempData
END
GO
/****** Object:  StoredProcedure [dbo].[GetBulkTimingAsID]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec GetBulkTimingAsID 1
CREATE Proc [dbo].[GetBulkTimingAsID]  
@TidFK int   
AS  
BEGIN  
SELECT ROW_NUMBER() over(order by (select 1)) as No,TBT.Timing,TB.SMS,TB.CALL,TB.EMAIL, 
SMSWhen,	CALLWhen,	EMAILWhen,TBT.Type,TBTY.TID,TB.ID,TBT.PID,TBTY.TypeName

from tbl_bulk_appointment TB
inner join tbl_bulk_appointmenttype TBTY On TBTY.TID=TB.TID_FK 
inner join tbl_bulk_appointmenttiming TBT On TBT.TID_FK =TB.TID_FK
where TB.TID_FK=@TidFK
ORDER BY TBTY.TID ASC
  
End
GO
/****** Object:  StoredProcedure [dbo].[GetBuliUploadDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec GetBuliUploadDetails '1'

CREATE Proc [dbo].[GetBuliUploadDetails]
@TypeID nvarchar(50)
As
Begin

SELECT ROW_NUMBER() over(order by (select 1)) as No,TBTY.TID,TB.ID,TBT.PID,TBTY.TypeName as [Type],TBT.Timing,TB.SMS,TB.CALL,TB.EMAIL 

from tbl_bulk_appointmenttype TBTY 
inner join tbl_bulk_appointment TB  On TBTY.TID=TB.TID_FK 
inner join tbl_bulk_appointmenttiming TBT On TBT.TID_FK =TB.TID_FK
Where TBTY.TID = @TypeID

ORDER BY TBTY.TID ASC

END
GO
/****** Object:  StoredProcedure [dbo].[GetAPIPathDetails]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[GetAPIPathDetails]
as
BEGIN	
SELECT IsCSVstatus,	CSVfilePath,BatfilePath from tbl_Email_Setting

END
GO
/****** Object:  StoredProcedure [dbo].[Get_TimeZone]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC Get_TimeZone 'App'  
CREATE Proc [dbo].[Get_TimeZone]  
@Flag Nvarchar(10)  
AS  
Begin  
  
  
If(@Flag='App')  
Begin  
select case When isnull(TimeZone,12) =24 THEN 23 ELSE  isnull(TimeZone,12) End as TimeHours from  tbl_Email_Setting   
END  
ELSE  
BEGIN  
select isnull(TimeZone,12) as TimeHours from  tbl_Email_Setting   
END  
  
  
END
GO
/****** Object:  StoredProcedure [dbo].[GET_Log]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GET_Log]
As
Begin


select * from tbl_TwilioLog Order by LID asc


End
GO
/****** Object:  StoredProcedure [dbo].[Get_CloudSyncDate_promotional]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Get_CloudSyncDate_promotional        
CREATE proc [dbo].[Get_CloudSyncDate_promotional]                        
as                        
Begin                        
                        
SELECT    RID, EventID, Event,               
EventTitle, Phone, Email, FromPhoneNo, AccountSID, AuthToken, CONVERT(VARCHAR, EventDatetime, 120) as EventDatetime , SMSStatus, EmailStatus, CallStatus, ClientKey, RedirectLink,                         
                      EmailFrom, EmailPassword, EmailFromTitle, EmailPort, EmailSMTP, CompanyLO, IsStatus, MainEventID, IsTwilioStatus    ,            
                      CONVERT(VARCHAR, AppointmentDatetime, 120) as AppointmentDatetime,IsConfirmable , TwilioLang          
          
                    
FROM         tbl_FinalReminderDetails                               
WHERE    
EventID in (select distinct NextEventID from tbl_NextGenerateEventID)   AND  
isnull(IsStatus,0)!=1         
and EmailStatus = '999'    
and (SMSStatus =1 OR CallStatus=1) 
and Phone <> '0' 
order by MainEventID asc            
        
        
            
                
                        
                        
                        
                        
End
GO
/****** Object:  StoredProcedure [dbo].[Get_CloudSyncDate_ForLog]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Get_CloudSyncDate_ForLog]                  
as                  
Begin                  
                  
SELECT     RID, EventID, Event,         
EventTitle, Phone, Email, FromPhoneNo, AccountSID, AuthToken, CONVERT(VARCHAR,DATEADD(minute,2,CONVERT(VARCHAR,GETDATE(),120)),120)  as EventDatetime , SMSStatus, EmailStatus, CallStatus, ClientKey, RedirectLink,                   
                      EmailFrom, EmailPassword, EmailFromTitle, EmailPort, EmailSMTP, CompanyLO, IsStatus, MainEventID, IsTwilioStatus    ,      
                      CONVERT(VARCHAR, GETDATE(),120) as AppointmentDatetime,IsConfirmable , TwilioLang    
    
              
FROM         tbl_FinalReminderDetails             
where EventID in (select distinct NextEventID from tbl_NextGenerateEventID)              
And  isnull(IsStatus,0)!=1              
order by MainEventID asc          
          
                  
                  
                  
                  
End
GO
/****** Object:  StoredProcedure [dbo].[Get_CloudSyncDate_API]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Get_CloudSyncDate_API]                  
as                  
Begin                  
                  
SELECT    RID, EventID, Event,         
EventTitle, Phone, Email, FromPhoneNo, AccountSID, AuthToken, CONVERT(VARCHAR, EventDatetime, 120) as EventDatetime , SMSStatus, EmailStatus, CallStatus, ClientKey, RedirectLink,                   
                      EmailFrom, EmailPassword, EmailFromTitle, EmailPort, EmailSMTP, CompanyLO, IsStatus, MainEventID, IsTwilioStatus    ,      
                      CONVERT(VARCHAR, AppointmentDatetime, 120) as AppointmentDatetime,IsConfirmable , TwilioLang    
    
              
FROM         tbl_FinalReminderDetails             
where EventID in (select distinct NextEventID from tbl_NextGenerateEventID)              
And  isnull(IsStatus,0)!=1   
and EmailStatus = 0  

order by MainEventID asc 

End
GO
/****** Object:  StoredProcedure [dbo].[Get_CloudSyncDate]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Get_CloudSyncDate  
CREATE proc [dbo].[Get_CloudSyncDate]                 
as                  
Begin                  
                  
SELECT    RID, EventID, Event,         
EventTitle, Phone, Email, FromPhoneNo, AccountSID, AuthToken, CONVERT(VARCHAR, EventDatetime, 120) as EventDatetime , SMSStatus, EmailStatus, CallStatus, ClientKey, RedirectLink,                   
                      EmailFrom, EmailPassword, EmailFromTitle, EmailPort, EmailSMTP, CompanyLO, IsStatus, MainEventID, IsTwilioStatus    ,      
                      CONVERT(VARCHAR, AppointmentDatetime, 120) as AppointmentDatetime,IsConfirmable , TwilioLang    
    
              
FROM         tbl_FinalReminderDetails             
where EventID in (select distinct NextEventID from tbl_NextGenerateEventID)              
And  isnull(IsStatus,0)!=1 
order by MainEventID asc 
End
GO
/****** Object:  UserDefinedFunction [dbo].[DaysNametoDate]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[DaysNametoDate](@whenDays Nvarchar(500),@EventDate Datetime)            
RETURNS VARCHAR(20)            
AS            
BEGIN            
            
 if(@whenDays='Immediate')            
 Begin            
  set @whenDays = '-10 Minute'            
  set @EventDate = Getdate()            
 END             
             
 DECLARE @tempTable table (srNo int,WhenTiming Nvarchar(500))            
            
 DECLARE @Result Datetime,@BreakStringA int,@BreakStringB Nvarchar(500)            
             
 insert @tempTable            
 select ROW_NUMBER() over(order by (select 1)) as No,NewColumn  from dbo.SplitBySpace(@whenDays,'')            
             
 Select @BreakStringA =(select WhenTiming from @tempTable where srNo=1 )            
 Select @BreakStringB =(select WhenTiming from @tempTable where srNo=2 )            
             
             
 SELECT            
  @Result = (CASE WHEN @BreakStringB like '%Day%' then DATEADD(DD,Convert(int,-@BreakStringA),@EventDate)            
      WHEN @BreakStringB like '%Minute%' THEN DATEADD(MINUTE,Convert(int,-@BreakStringA),@EventDate)            
      WHEN @BreakStringB like '%Hour%' THEN DATEADD(HOUR,Convert(int,-@BreakStringA),@EventDate)            
      WHEN @BreakStringB like '%WEEK%' THEN DATEADD(WEEK,Convert(int,-@BreakStringA),@EventDate)            
      WHEN @BreakStringB like '%MONTH%' THEN DATEADD(MM,Convert(int,-@BreakStringA),@EventDate)            
      WHEN @BreakStringB like '%YEAR%' THEN DATEADD(YYYY,Convert(int,-@BreakStringA),@EventDate)            
                  
                  
                   
      ELSE ''            
     END )            
                 
 RETURN CONVERT(VARCHAR(19), @Result, 120)             
             
             
END
GO
/****** Object:  StoredProcedure [dbo].[Cloud_UpdateSync_New]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from tbl_FinalReminderDetails  
  
CREATE Proc [dbo].[Cloud_UpdateSync_New]    
@MainEventID Nvarchar(50),    
@ClientKey nvarchar(200)  
  
As    
Begin    
    
    
Update tbl_FinalReminderDetails set IsStatus=1 Where  MainEventID=@MainEventID AND ClientKey=@ClientKey    
SELECT 1 as Result    
    
End
GO
/****** Object:  StoredProcedure [dbo].[CheckRegistration]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CheckRegistration]  
@MACID NVARCHAR(50)
As  
Begin  
  
select ProductKey,MacID from  tbl_RS_Registration  where  MacID=@MACID

  
  
End  
  
  
--truncate TABLE tbl_RS_Registration
GO
/****** Object:  UserDefinedFunction [dbo].[12to24hour]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

select dbo.[12to24hour]('8:17')        

*/
        
        
CREATE FUNCTION [dbo].[12to24hour](@whenTime Nvarchar(20))        
RETURNS VARCHAR(20)        
AS        
BEGIN        
     
        
DECLARE @Result Nvarchar(50) ,@FirstHours int ,@Minute int    
      




set @FirstHours =(select  top 1 Column1 as EventTitle  from dbo.SplitByJEM(@whenTime))  
set @Minute =(select  top 1 Column1 as EventTitle  from dbo.SplitByJEM(@whenTime)order by 1 ASC )  
    
if(@FirstHours >= 0 and @FirstHours<=7)    
BEGIN  
set @Result= CONVERT(NVARCHAR,(SELECT  case WHEN @FirstHours=0 THEN 12     
WHEN @FirstHours=1 THEN 13    
WHEN @FirstHours=2 THEN 14     
WHEN @FirstHours=3 THEN 15     
WHEN @FirstHours=4 THEN 16     
WHEN @FirstHours=5 THEN 17     
WHEN @FirstHours=6 THEN 18    
WHEN @FirstHours=7 THEN 19  ELSE @FirstHours end ))+':'+CONVERT(NVARCHAR,(select  top 1 Column1 as EventTitle  from dbo.SplitByJEM(@whenTime)order by 1 ASC ))    
    
    
END   
ELSE if(@Minute < 30 and @FirstHours=8)
BEGIN

set @Result= CONVERT(NVARCHAR,(SELECT  case WHEN @FirstHours=8 THEN 20     
ELSE @FirstHours end ))+':'+CONVERT(NVARCHAR,(select  top 1 Column1 as EventTitle  from dbo.SplitByJEM(@whenTime)order by 1 ASC ))    

END
 
ELSE    
BEGIN   

	set @Result=@whenTime
	

    
End    
       
       
RETURN CONVERT(NVARCHAR, @Result)    
         
         
END
GO
/****** Object:  StoredProcedure [dbo].[AddressBooktoParties]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec AddressBooktoParties '15'  
  
CREATE Proc [dbo].[AddressBooktoParties]  
@ID Int  
As  
Begin  
  
select Convert(Nvarchar(50),CCa.caseno) as caseno,c.firmcode,c.type as [Type]  
,ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                                  
email as Email,C2.phone1 as Phone      ,ISNULL(LTRIM(RTRIM(c.first)),'')as FirstName,ISNULL(LTRIM(RTRIM(c.last)),'') as LastName ,        
Convert(Nvarchar(50), TLA.CardCode) as CardCode   
  
from tbl_LRS_AddressBookDetails TLA  
Inner Join casecard CCa On Convert(nvarchar(50),CCa.cardcode) = Convert(nvarchar(50),TLA.cardcode)      
Inner join [card] C on Convert(nvarchar(50),C.cardcode) =Convert(nvarchar(50),TLA.cardcode   )
Inner join [card2] C2 on Convert(nvarchar(50),C2.firmcode) =Convert(nvarchar(50),C.firmcode )
   
where TLA.AddId = @ID  
  
union ALL
  

  
select RC.[Case],RC.firmcode,'RS' as [Type]  
,ISNULL(LTRIM(RTRIM( RC .salutation)),'')+' '+ISNULL(LTRIM(RTRIM(RC .FName)),'')+' '+ISNULL(LTRIM(RTRIM(RC .LName)),'') AS [Name],                                  
RC.Email as Email,PhoneNo as Phone      ,ISNULL(LTRIM(RTRIM(FName)),'')as FirstName,ISNULL(LTRIM(RTRIM(LName)),'') as LastName ,        
Convert(Nvarchar(50),RC.CardCode    ) as CardCode

from tbl_RS_Contactlist RC   
inner join tbl_LRS_AddressBookDetails LA on Convert(nvarchar(50),LA.CardCode) = Convert(nvarchar(50),RC.CardCode)  

where AddId = @ID   
  
  
  
End
GO
/****** Object:  StoredProcedure [dbo].[Sproc_A1LawMail]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sproc_A1LawMail]      
as      
Begin      
      
SELECT RR.CompanyName as ServerName,AD.Card as [Card],AD.Card2 as Card2,Ad.[Case],AD.Casecard,      
Convert(VARCHAR,ad.CreatedDatetime,106)+' '+ CONVERT(VARCHAR(8), ad.CreatedDatetime, 24)  as SyncDate ,isnull(AD.StatusLog,'')   as StatusLog  
    
    
from tbl_A1lawVSDataloader  AD      
inner JOIN tbl_RS_Registration RR ON RR.MacID=AD.ServerMACID      
      
--where isnull(Isstatus,0) <>1      
    
    
SELECT    
'A1-LAW SYNC STATUS' as Report_Title ,                 
'inforemindersystem@gmail.com' as EmailFrom,                                                                
'blpetrriuwzpyalv' as EmailPassword,                                                                
'SNS Sync Report' as  [Subject],     
'Synergy Notification System' as  EmailFromTitle,     
'587' as EmailPort,     
'smtp.gmail.com'  as EmailSMTP,    
'keyur063solanki@gmail.com,chirag@solulab.com,5cmthakkar@gmail.com,Tony@tclarkconsulting.com,hitesh.solulab@gmail.com,raj.rr7@gmail.com,shashank@solulab.com' as EmailTo    
      
END
GO
/****** Object:  StoredProcedure [dbo].[RemoveEvent]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[RemoveEvent]        
@EventNo nvarchar(50)        
as        
Begin        
 
 delete from tbl_LRS_Reminder where eventno=@EventNo        
        
 Delete from tbl_FinalReminderDetails   Where MainEventID=@EventNo    
    
        
 select 1 as Result        
End
GO
/****** Object:  StoredProcedure [dbo].[UpdateClientVersion]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[UpdateClientVersion]
@ProductKey Nvarchar(100),
@Version Nvarchar(20)
As
Begin

Update tbl_RS_Registration SET InstalledVersion=@Version,	LastUseDate=GETDATE() where ProductKey=@ProductKey

select 1 AS Result
END
GO
/****** Object:  StoredProcedure [dbo].[Test]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Test]
as
Begin
select ROW_NUMBER() over(order by (select 1)) as No,initials,initials0  INTO #TEMP from Cal1

select ROW_NUMBER() over(order by (select 1)) as No, FIRST INTO #TEMPa from CARD


sELECT * FROM #TEMP tt
FULL oUTER JOIN #TEMPa ttt oN ttt.No=TT.No
End
GO
/****** Object:  UserDefinedFunction [dbo].[TempleteVariableSet_API]    Script Date: 01/24/2018 10:54:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[TempleteVariableSet_API](@LastId  int,@Event Nvarchar(MAX),@EventAppointmentTypeAPI  Nvarchar(1000))      
RETURNS NVARCHAR(MAX)      
AS      
BEGIN      
      
    
Declare @FINDNEWFlag Nvarchar(50) ,@Result Nvarchar(MAX)                       
                      
set @FINDNEWFlag=(select CardcodeFK from tbl_LRS_Reminder RR where RR.eventno=@LastId)          
    
if(@FINDNEWFlag not like '%RS%' )                       
Begin                      
           
    
       
set @Result = (    
SELECT     
                 
@EventAppointmentTypeAPI+':'+replace(replace(replace(replace(replace(@Event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')) ,                                  
'{EventDatetime}',                        
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                           
(CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,birth_date, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)+' '+RIGHT(Convert(VARCHAR(20),birth_date,100),2)
  
    
                                        
else                                     
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                                        
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.EventParties)                
                        
                     
            
                        
 AS [Event]    
from tbl_LRS_Reminder LR         
                    
inner join [Card] C on Convert(Nvarchar(50),C.CardCode) =Convert(Nvarchar(50),LR.CardcodeFK)                         
inner join Card2 C2 On Convert(Nvarchar(50),C2.firmcode)=Convert(Nvarchar(50),C.firmcode)                       
where LR.eventno=@LastId)                       
    
       
           
    
       
       
END     
ELSE     
BEGIN     
    
set @Result = (select             
                       
@EventAppointmentTypeAPI+':'+replace(replace(replace(replace(replace(@Event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.FName)),'')+' '+ISNULL(LTRIM(RTRIM(C.LName)),'')) ,                                  
'{EventDatetime}',                        
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then                                                           
(CONVERT(VARCHAR(5), C.DOB, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + SUBSTRING(CONVERT(VARCHAR,C.DOB, 100), 13, 2)   + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, C.DOB)), 2)+' '+RIGHT(Convert(VARCHAR(20),C.DOB,100),2)                    
  
                                     
else                                     
CONVERT(VARCHAR, LR.date, 101) + ' ' + SUBSTRING(CONVERT(VARCHAR,LR.date, 100), 13, 2) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LR.date)), 2) +' '+ RIGHT(Convert(VARCHAR(20),LR.date,100),2) End)                      
                                  
,'{Company Signature}',isnull((select [Signature] from tbl_Email_Setting),'')),'{Company Logo}',''),'{Location}',LR.AppointmentType)              
                        
                     
                        
                        
 AS [Event]    
from tbl_LRS_Reminder LR                        
                  
inner join tbl_RS_Contactlist C on Convert(Nvarchar(50),C.CardCode) =Convert(Nvarchar(50),LR.CardcodeFK)                         
                      
where LR.eventno=@LastId)    
    
    
    
END    
    
    
 RETURN @Result    
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Insesrt_AddressBook]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[SP_Insesrt_AddressBook]                    
@AddId int,                    
@AddName varchar(100),                    
@Description varchar(100),                    
@Status bit
              
As           
	begin
		INSERT INTO tbl_LRS_AddressBook 
		(
				AddName,
				Description,
				Status
		)
		VALUES
				(
				@AddName,
				@Description,
				@Status
				)
	end
GO
/****** Object:  StoredProcedure [dbo].[Sp_InsertTwilioSID]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_InsertTwilioSID]            
@EventNo Int,            
@SID Nvarchar(500)            
As            
Begin            
          
 if(@SID <> '')
 Begin	    
 Update tbl_FinalReminderDetails set SID=@SID,IsTwilioStatus=5 ,ModifiedDttm=getdate()   where RID=@EventNo            
 END
 ELSE
 BEGIN
  
  Update tbl_FinalReminderDetails set SID=@SID,IsTwilioStatus=0 ,ModifiedDttm=getdate()   where RID=@EventNo     
 
 END
              
 
            
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_InsertTwilioLog]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_InsertTwilioLog]              
@FromNumber Nvarchar(50),              
@ToNumber Nvarchar(50),              
@Direction Nvarchar(50),              
@Duration Nvarchar(50),              
@StartDate Datetime,              
@EndDate Datetime,              
@Status Nvarchar(50),              
@SID Nvarchar(100) ,            
@LogType nvarchar(100)             
              
AS              
Begin              
              
              
If Not Exists (select SID from tbl_TwilioLog where SID=@SID )              
Begin              
       
 insert Into tbl_TwilioLog              
 SELECT     @FromNumber, @ToNumber, @Direction, @Duration,@StartDate,@EndDate, @Status, Getdate(),Getdate(),@SID ,@LogType             
               
 select 1 As Result               
               
END              
    
              
        
              
              
select 1 As Result              
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_InsertTimeSlot]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Sp_InsertTimeSlot 'Insert','4 Days',''

CREATE Proc [dbo].[Sp_InsertTimeSlot]
@Flag nvarchar(50),
@TimeSlot Nvarchar(500)= Null,
@TimeID Int =Null
as
Begin

	IF(@Flag = 'Insert')
	Begin

		Declare @NoOfTime int
		set @NoOfTime =(select Count(*)  from lst_SMS_Timing where isnull(Isactive,0)=0)
		If(@NoOfTime < 8)
		Begin

			Insert Into lst_SMS_Timing
			select @TimeSlot,0,Getdate(),Getdate()

			select 1 as Result
		End
		Else
		Begin

			select 2 as Result

		End

	End

	IF(@Flag = 'Update')
	Begin
		 
		 Update lst_SMS_Timing set Isactive =1 Where ID =@TimeID
		select 1 as Result
	End
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_insertTemplete]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sp_insertTemplete 'Get'      
            
CREATE proc [dbo].[Sp_insertTemplete]                    
@Flag Nvarchar(50),                    
@Title Nvarchar(1000)=null,                    
@Body Nvarchar(MAX)=null,                
@BodyNotHTML nvarchar(MAx)=Null ,            
@ID Int =Null ,        
@AppointmentType nvarchar(MAX) =Null ,      
@TwilioLangID Int =null   ,  
@SMS Int =null   ,  
@CALL Int =null   ,  
@EMAIL Int =null   ,  
@whenSMS nvarchar(MAX)  =null   ,  
@whenCALL nvarchar(MAX)  =null   ,  
@whenEMAIL nvarchar(MAX)  =null    ,
@IsReschedule Int =null
  
as                    
Begin                    
                    
IF(@Flag='Insert')                    
Begin                    
 IF not Exists(select * from tbl_Apptoto_AddSmsEmail where Name=@Title)                    
 Begin                    
 Insert Into tbl_Apptoto_AddSmsEmail       
 (Name, Purpose, AppointMentWhen, Body, EmailSubject, Category, Status, CreatedDatetime, ModifiedDatetime, TwilioLang  
 ,isSMS ,isCALL, isEMAIL, whenSMS, whenCALL, whenEMAIL ,IsReschedule)      
 VALUES      
 (@Title,@AppointmentType,NULL,@body,@BodyNotHTML,NULL,NULL,Getdate(),Getdate(),@TwilioLangID,  
 @SMS,@CALL,@EMAIL,@whenSMS,@whenCALL,@whenEMAIL,@IsReschedule)      
                    
  select 1 As Result                    
 End                    
 Else                    
 Begin                    
  select 2 As Result                    
                
 End                    
End                    
IF(@Flag='Remove')                    
Begin                    
                    
delete From tbl_Apptoto_AddSmsEmail where id=@ID                  
select 1 As Result                    
                    
End                    
IF(@Flag='Update')                    
Begin                    
                    
Update tbl_Apptoto_AddSmsEmail set         
Name=@Title ,        
Purpose=@AppointmentType,        
Body=@Body,        
EmailSubject=@BodyNotHTML,        
ModifiedDatetime=getdate(),      
TwilioLang=   @TwilioLangID    ,  
isSMS=@SMS,  
isEMAIL=@EMAIL,  
isCALL=@CALL,  
whenSMS=@whenSMS,  
whenCALL=@whenCALL,  
whenEMAIL=@whenEMAIL ,
IsReschedule=@IsReschedule 
        
where ID=@ID            
select 1 As Result                    
End                    
                    
IF(@Flag='Get')                    
Begin                    
                    
select ROW_NUMBER() over(order by (select 1)) as No,Name as Title,CONVERT(VARCHAR(10), TA.CreatedDatetime, 101)  as [Created Date],body as [Event Body],ID,EmailSubject as [BODY],        
Purpose as [Appointment Type],LT.Language as [Language],isnull(TA.TwilioLang,6)
,TA.isSMS,TA.isCALL,TA.isEMAIL,TA.whenSMS,TA.whenCALL,TA.whenEMAIL ,TA.IsReschedule 
from tbl_Apptoto_AddSmsEmail   TA      
LEFT join  lst_TwilioLanguage LT On LT.LID=isnull(TA.TwilioLang,6)    
      
                
                    
End                    
                    
                    
                    
End
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertNotification]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[sp_InsertNotification]    
@EmailSend nvarchar(50),    
@EmailCancel nvarchar(50),    
@EmailReschedule nvarchar(50),    
@EmailWhen nvarchar(50),    
@SMSSend nvarchar(50),    
@SMSCancel nvarchar(50),    
@SMSReschedule nvarchar(50),    
@SMSWhen nvarchar(50)    
    
As    
Begin    
    
    
Update tbl_LRS_NotificationSetting set isactive=1     
    
Insert Into tbl_LRS_NotificationSetting    
select @EmailSend, @EmailCancel, @EmailReschedule, @EmailWhen, @SMSSend, @SMSCancel, @SMSReschedule, @SMSWhen, 0 , Getdate(), Getdate(),''    
                          
select 1 As Result    
    
delete From  tbl_LRS_NotificationSetting where isactive=1     
    
    
    
End
GO
/****** Object:  StoredProcedure [dbo].[SP_Insert_Contact_Outlook]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec SP_Insert_Contact_Outlook '','kaushik','','patel','','kaushik_patel827@yahoo.in','','','','','Mobile','Work','Google'                
                
CREATE Proc [dbo].[SP_Insert_Contact_Outlook]                        
@Salutation Nvarchar(50) =Null,                        
@FName Nvarchar(500)=Null,                        
@MName Nvarchar(500)=Null,                        
@LName Nvarchar(500)=Null,                        
@PhoneNo Nvarchar(20)=Null,                        
@Email Nvarchar(500)=Null,                        
@Zipcode Nvarchar(10)=Null,                        
@Notes Nvarchar(MAX) =Null,                    
@DOB datetime =Null,                  
@CountryCode Nvarchar(10)=Null,                  
@MobileType Nvarchar(100)=Null,                  
@EmailType Nvarchar(100)=Null,                
@ContactType  Nvarchar(200) =Null                
               
As                        
Begin     
  
Declare @CIDNew BIGINT   
  
Insert Into tbl_RS_Contactlist                      
select isnull(@Salutation,''),isnull(@FName,''),isnull(@MName,''),isnull(@LName,''), isnull(@PhoneNo,'') ,                 
isnull(@Email,''), isnull(@Zipcode,''),isnull(@Notes,'')+' - '+isnull(@ContactType,''),'','',@MobileType,@EmailType,0,'','','',GETDATE(),GETDATE()  
                 
   
set @CIDNew =SCOPE_IDENTITY()   
        
Update    tbl_RS_Contactlist set  [Case]='RS'+''+Convert(NVARCHAR(100),@CIDNew)    ,Firmcode='RS'+''+Convert(NVARCHAR(100),@CIDNew)    
,CardCode ='RS'+''+Convert(NVARCHAR(100),@CIDNew) where  CID=  @CIDNew                         
                        
update tbl_RS_Contactlist set  PhoneNo = ltrim(rtrim(Replace(PhoneNo,'tel:',''))) where PhoneNo Like '%tel:%'                        
                        
select 1 As Result                        
                        
End
GO
/****** Object:  StoredProcedure [dbo].[SP_Insert_Contact_API]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Insert_Contact_API]                                      
@Salutation Nvarchar(50),                                      
@FName Nvarchar(500),                                      
@MName Nvarchar(500),                                      
@LName Nvarchar(500),                                      
@PhoneNo Nvarchar(20),                                      
@Email Nvarchar(500),                                      
@Zipcode Nvarchar(10),                                      
@Notes Nvarchar(MAX) ,                                  
@DOB datetime ,                                
@CountryCode Nvarchar(10),                                
@MobileType Nvarchar(100),                                
@EmailType Nvarchar(100)                                    
                                      
                                      
                                      
As                                      
Begin                                      
If Not Exists(select PhoneNo from tbl_RS_Contactlist where ( PhoneNo=@PhoneNo or     
LTRIM(RTRIM(isnull(CountryCode,'')))+LTRIM(RTRIM(isnull(PhoneNo,''))) = @PhoneNo)    
 and isnull(Isactive,0)<> 1  )                                
Begin                            
                            
insert into tbl_RS_Contactlist                        
(            
Salutation,            
FName,            
MName,            
LName,            
PhoneNo,            
Email,            
Zipcode,            
Notes,            
DOB,            
CountryCode,            
MobileType,            
EmailType,            
Isactive,            
CreatedDatetime,            
ModifiedDatetime)            
            
VALUES            
(            
@Salutation,            
@FName,            
@MName,            
@LName,            
@PhoneNo,            
@Email,            
@Zipcode,            
@Notes,            
@DOB,            
@CountryCode,            
@MobileType,            
@EmailType,            
0,            
GETDATE(),            
GETDATE()            
)            
            
Declare @CIDNew BIGINT            
            
set @CIDNew =SCOPE_IDENTITY()            
            
             
                
Update    tbl_RS_Contactlist set  [Case]='RS'+''+Convert(NVARCHAR(100),@CIDNew)    ,Firmcode='RS'+''+Convert(NVARCHAR(100),@CIDNew)            
,CardCode ='RS'+''+Convert(NVARCHAR(100),@CIDNew) where  CID=  @CIDNew            
                                      
                              
                            
End                            
                                 
                                      
End
GO
/****** Object:  StoredProcedure [dbo].[SP_Insert_Contact]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Insert_Contact]                                    
@Salutation Nvarchar(50),                                    
@FName Nvarchar(500),                                    
@MName Nvarchar(500),                                    
@LName Nvarchar(500),                                    
@PhoneNo Nvarchar(20),                                    
@Email Nvarchar(500),                                    
@Zipcode Nvarchar(10),                                    
@Notes Nvarchar(MAX) ,                                
@DOB datetime ,                              
@CountryCode Nvarchar(10),                              
@MobileType Nvarchar(100),                              
@EmailType Nvarchar(100)                                  
                                    
                                    
                                    
As                                    
Begin                                    
If Not Exists(select PhoneNo from tbl_RS_Contactlist where ( PhoneNo=@PhoneNo or   
LTRIM(RTRIM(isnull(CountryCode,'')))+LTRIM(RTRIM(isnull(PhoneNo,''))) = @PhoneNo)  
 and isnull(Isactive,0)<> 1  )                              
Begin                          
                          
insert into tbl_RS_Contactlist                      
(          
Salutation,          
FName,          
MName,          
LName,          
PhoneNo,          
Email,          
Zipcode,          
Notes,          
DOB,          
CountryCode,          
MobileType,          
EmailType,          
Isactive,          
CreatedDatetime,          
ModifiedDatetime)          
          
VALUES          
(          
@Salutation,          
@FName,          
@MName,          
@LName,          
@PhoneNo,          
@Email,          
@Zipcode,          
@Notes,          
@DOB,          
@CountryCode,          
@MobileType,          
@EmailType,          
0,          
GETDATE(),          
GETDATE()          
)          
          
Declare @CIDNew BIGINT          
          
set @CIDNew =SCOPE_IDENTITY()          
          
           
              
Update    tbl_RS_Contactlist set  [Case]='RS'+''+Convert(NVARCHAR(100),@CIDNew)    ,Firmcode='RS'+''+Convert(NVARCHAR(100),@CIDNew)          
,CardCode ='RS'+''+Convert(NVARCHAR(100),@CIDNew) where  CID=  @CIDNew          
                                    
select 1 As Result                             
                          
End                          
Else                          
Begin                          
                          
 select 2 As Result                             
                          
END                                  
                                    
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetWebPageDetails]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from tbl_FinalReminderDetails where ClientKey='C73F-DDDA-BB96-4ACF-BA6D'      
      
--Exec Sp_GetWebPageDetails '25'                
                
CREATE Proc [dbo].[Sp_GetWebPageDetails]                
@EventID Nvarchar(50)                
AS                
Begin                
                
Declare @EventTitle Nvarchar(500)                
          
set @EventTitle =(select Event from tbl_FinalReminderDetails where RID=@EventID)          
                
--set @EventTitle =(select TR.event                 
--     from dbo.tbl_LRS_Reminder TR                
--     Inner Join tbl_LRS_Reminder_Timing TLR on TLR.EventFK =TR.eventno                
--     where TimingID=@EventID)                
                
                
select ROW_NUMBER() over(order by (select 1)) as [No],Column1 as EventTitle into #Temp from dbo.SplitByJEM(@EventTitle)                
                
                
                
--select TimingID as EventID,(select EventTitle from #Temp where [No]=1) as EventTitle,                   
              
                
--CONVERT(VARCHAR(20), TR.[Date], 100) as [EventDate],C1.Phone1 as Phone                
-- from dbo.tbl_LRS_Reminder TR                
--Inner Join tbl_LRS_Reminder_Timing TLR on TLR.EventFK =TR.eventno                
--INNER JOIN [card] c ON c.cardcode=TR.cardcodeFK                                           
--Inner JOIN card2 c1 ON c1.firmcode =c.firmcode                   
--Where TLR.TimingID=@EventID                
          
          
SELECT RID as EventID,(select EventTitle from #Temp where [No]=1) as EventTitle,          
--CONVERT(VARCHAR(20), EventDatetime, 100) as [EventDate]      
CONVERT(VARCHAR(20), AppointmentDatetime, 100) as [EventDate]     
,Phone as Phone  ,IsConfirmable         
 from tbl_FinalReminderDetails          
 where RID=@EventID          
       
      
                
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetTwilioLogMAXDate]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Sp_GetTwilioLogMAXDate 'SMS'  
CREATE Proc [dbo].[Sp_GetTwilioLogMAXDate]    
@Flag Nvarchar(100)  
as    
Begin    
    
select top 1 Enddate as MAXDT from tbl_TwilioLog where LogType=@Flag order By EndDate desc    
    
    
    
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetTwilio_Log]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sp_GetTwilio_Log]    
As    
Begin    
    
    
SELECT     FromNumber as [From], ToNumber as [To],Status as [Status], 
isnull(case When Duration ='' then NULL Else Duration End,0)  as [Duration (Second)],    
(CONVERT(VARCHAR(5), StartDate, 101)+'/'+CAST(YEAR(StartDate) AS VARCHAR(4)))+' '+    
isnull( CONVERT(VARCHAR, DATEPART(hh,StartDate)),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,StartDate)), 2),00)    
as [Start Date] ,    
    
(CONVERT(VARCHAR(5), EndDate, 101)+'/'+CAST(YEAR(EndDate) AS VARCHAR(4)))+' '+    
isnull( CONVERT(VARCHAR, DATEPART(hh,EndDate)),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,EndDate)), 2),00)    
as [End Date] ,SID,LogType  
  
Into #Temp    
FROM tbl_TwilioLog     
where EndDate <> '1900-01-01 00:00:00.000'    
Order by StartDate Desc    
    
    
select ROW_NUMBER() over(order by (select 1)) as No,[From],[To],[Status],[Duration (Second)] as [Duration]  
,[Start Date],[End Date],LogType as [Type]  
 from #Temp    
    
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetTimeSlot]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_GetTimeSlot]
as
Begin

select ROW_NUMBER() over(order by (select 1)) as No,ID,SMSTIME as [Time Slot] from lst_SMS_Timing where isnull(Isactive,0)=0

End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetSigAndLogo_Webpage]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_GetSigAndLogo_Webpage]       
@EventID nvarchar(50)       
as        
Begin        
      
Declare @ProductKey Nvarchar(500)      
      
set @ProductKey=(SELECT ClientKey from tbl_FinalReminderDetails where RID=@EventID)      
  
     
select 


isnull(CompanyLogo,'iVBORw0KGgoAAAANSUhEUgAAAUUAAACRCAYAAABUrKrQAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAADImlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoyNjlFMTA4QkU0MTMxMUU2QjUwMkY5ODA1NUI4M0Y5RSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoyNjlFMTA4Q0U0MTMxMUU2QjUwMkY5ODA1NUI4M0Y5RSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjI2OUUxMDg5RTQxMzExRTZCNTAyRjk4MDU1QjgzRjlFIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjI2OUUxMDhBRTQxMzExRTZCNTAyRjk4MDU1QjgzRjlFIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+NMFJfwAAJ/VJREFUeF7tnQf4HFW5hze0EAIkgBSpgYCC0nsLobcQOhh6UzAgGMGLF0HQS0eKCAgI0puBYLhEuIGgCU0RpBfpAaR3QigJIff3hjnxZJjZnbKzO7P75Xl+z+a/M2fmnO9833u+c6ZsrfdlN9VaoN46xyLSEhVXP9V/vqLtVbtoRG2b0ffWpk6dWleTJ0+uDRkypDZo0KDa4MGDnRbQ/9eXVpNm8r7397H//8deWW3RV7ZdUxogLebbeeDAgbXRo0fX7buP1XfzXTmq1uvSkUXF36zy012ky6SRHaCb1Ibh0kXScdKu0tJFxGIzO2QOVXAZaQvpKOly6Q7pCelZ6XnphQ7Qc2rD/dKJUv9COiUbFFdVYF4pPS+9LL0k3S8NlXobHKcPGlkh6MoBwsukZwIbY+sXpL9I+0izlQCKawex95o+P5QmdIg+Ujs+kN6RaNuL0r3SBdJu0rek2fLGZDOguJYq8UvpdmmiNLWL9Krauk7eTgiXz5ApDlQwvi9NjdEYfb+IgTE3GLeVDT+qY2fs/3tBsUcbM8Xl5U9vd1EM+rz5TO2+WRoqfTNrXGaFIqn5YGm09G6XdoDrDLLhObN2QFS5lFCcQ9Pn0Q0ClWC9U1rYwJgZjGTbTyew81RBccs2QvF3XR6PLi6Z0ZGsLZg2NrNAkenxGDP89Iz4c9li1bSGr7d/SiiuICh+nCRYtc84aX4DYyYwDkhoY6B4cZug2Fd+9TeLzRlmq/8OMkeW9xLxLtFOwcFIRy+UgEA3TZGTtHVQUoMn2S8lFDcRFOOmzVHfM5U2MKa/0HJACiiKiW250LKY/OsZi89IPt0qu6yYKP6S7KR9NpeeNGPHDgY7JrRjokEoJRR3SAlFQHmrxNXTvBcduqn8oSmgeEeboLioxWndhI211r0axWqSID1UB/nUgFjX2Bs2MnSa7SmhuHMGKDowzmNgTDwwVAGKvWxpq+Es9kvZiLXGmeJishEUf2UwbGhkrkBz/2IjWybe3iIoAsaR0hwGxkRgrAIU8bGfW8w2jFmWxM7IAsUTzLiJjPuLZgKRY7UQioBxhDSXgbEhGKsCxfnlQ+MsdhPF7smy08zh+I3LXg4xozY0KjeSYtSm3o7TBigCxj9KPQ2MdcFYFSgS0wtLJ0mPS0wXk1ws7MZ9Jss2ByaB4qbaiZsg8xrpPR2De4W4l5FHc04LIHKKPqsqIHiMNERaqtkZojteizNFd4X6ckFxNgNjLBirBEWX7Mwjn+LR2mWlFTpAq6sNW0tc5+DxxaekvA+MfKxjcNzpCWI4U8SIea4yT1H5P0s/kVaRYhcziwJKJxy3TVAEjlcYGDsKionXsSscN5uo7mdLPPaXNZG7S2Wnz/jCRsuzjshD5xtIPO3SDZ1RWBvbCEXAeIHUwzLGr8GxipliYT5awhgnG/6NxJQ4LRxZYpg+jfaN9m1teCvDAV9WGR7G7qYOKLStbYYiYLxQmsXAOAMYDYrViPHNxCJeQJMWjI+pzLxwzA/uLLff8IaK5QyIzR0QSgBFwHi2QdGgWNHY5u1V92UA404+FFlLfDTlQQBi5jdRVNTYhWaIziYlgSJgPMPAOB2MlilWI1N0Mcr7W9NeH/mTD0Xev5Ym3XxT+zPdbgkkuu08JYIiYDzN1hingdGgWL14X1/s+CAF23hPY18HtR+nKAg8D+42ULWyvSWDImA8wTJGg2IrY6BJ5+qh45yXgm2TtO/GDopXpCjINHvagqSpGBuUEIqA8bguB6NlitWM+TXFKd4+nnQmfKgD2z0pCp1pMCwGhi1YU3xSYHtQSvOqMX/fo7sYjAbFakJxFsXVIyn4drKD4tMpCu1jUKwsFG8X1L4tjc0BxiO6FIwGxWpCEcZdl4Jv51Ggj8TjeEnSS14hxpu3bepcoA0KnD7fLaDNLC0m/SMjGL9UuZ92IRgNigX6fMFM+W1CvsHAS4Db4hK/spcEirwEYWDBDeh64BYMRfdy2cUFtgcygvFzlTu4y8BoUKwuFE9PyDcYeBkA4oHxNFDkUb6uB1eRNigYin08mPXX/x/PCMYpXQZGg2J14553JyZJ+gyKRYItz7FbCEXuv2ONMSsYJ6rsfl2SMRoUDYqRZGX6bJliwc7RYigCxmWlZzNmjJ+p3N5dAEaDYsF+nyeRaFDWMsUCjduSZYM2QBEwrpQDjBNUdvcOB6NB0aBomWK74NomKALGFaXxGTPGD1Vupw4Go0HRoGhQ7EIoAsY1pFdygHHbDgWjQdGgaFDsUig6ML6aEYzvqNygDgSjQdGgaFDsYigCxvWkNzKC8X2V26zDwGhQNCgaFLscioBxA+mtjGAEqJt2EBgNigZFg6JBcdp7BDeWmBJneYnEayq3boeA0aDYRVDsZ0+0lOsJnTZefQaCUdpC33/c5WA0KFYXiqkf81ssBRQn2LPPxQO0hFAElFtJ72YE4wsqt1rFM0aDYnWheFaKx/wu5WZkfu806VtyPtO+W7VrWtkt5y0pFAHj9tKnGcH4UsXBaFCsLhQvTQHFC90TGk+lKPSDboFTu9pZYigCRm7Q5kbtLGuMT6vcChXNGA2K1YUiP7KX9IUQZzgojktR6Nx2waJbzltyKALGIRLPPGcB4zMq990KgtGgWE0o8haw11Pw7UgHxYtTFOKHpu2nTQt0kApAETDuJfFexSxgfELllq4YGA2KBfp8gQnPz3TsLxPybYr2295BcWjCQi4FPbrARrTkpQtlrn9FoAgY982RMT6isrzPMe6Kd9m+NyhWD4pp3hUL23gL2GIOQKumhCK/jrVWmcFS5bpVCIqA66CM2SIZ5sMSbwAvGwCj6mNQrBYUZxIDbkjJtb/BDQfFufRHmsVIqPqktFyV4VPWulcMigBkqDQpIxz5rZhFKwBGg2J1oDi7Yvv8lECEaYf4UASOR2Y4CL8CuH5Z4VLVelUQioBxmMSPWmVZY7xP5RYuORgNitWA4oKK+z9mYBkXY3iQZXqmCBS5ifulDAdjHn641LuqECpbvSsKRcDIr/xlgSJl+KXBBUoMRoNiuaHI7zsPltLcXujfpnOK40D4osYRGaDoDnynyu4kkbp2/cWSPDaoMBQB41HSlIxwHFdiMBoUyxnXPRVr20gjcrCLH+7jV02ncSsMrzn0Zdq1xfBNkX/XMX4lrR4AskceQHRj2YpDsYfAdlxGKJIx3iHNW8KMscpQJAarLi6czCzNKi0kbSmdKnFx5IscQIRfe/qcicroVtMO7+U8CSf6XHpNukPiMRuePzxZOl46ocKi/kdJh0k7SKypLiv1bRbAKw5Fd+X2+BxrjLeqrP9TrGW4Ol1mKAKLJaV1gwA/Vp8XSTdKt0pjpNsrLmai90svSxMl7ilM+pRKvf24IDMDB+OmuUO04+QmnbQZFS/7Md6Qre4KHPEAfXJ/VOYlhA6B4kwC26k5MsY/q+w8JcoYywhFZmM/l0ZKz1u8pobkaNls3qRQJKDJhJLeCV52aLW6fmTIrHFsLfHCjVSA7BAokt3NLJ2ZA4w3qmzPkoCxLFCcW/60uzRWasaMrtWxUZbz3S37fSMqNhsFK0+6fGojUOoRyO941mhZs0h8AaqDoAgYyRjPzgHGESo7dwnA2G4o4j/MQh62eMwVj8QmSwqsS0byrxEU2b6r9I51RO6OYE1nYJKsscOgCBh7SRfmAOPVKjtbm8HYTihuJL/5i8Vg7hgEiBdIddf/k0CRfVaR0rxJpywpctnqwcWnEyWeIIq1fQdC0WWMF+UA45UBXNt10aUdUOTe319KkwyIuYHIuj+ZdkPmNdzBO0gf/Z+rxrx9u2ywqVp9/iobcsU6On2/aERtm9H31qZOnVpXkydPrg0ZMmTnQYMGJb1hmhuk23lVd06dH7glrW94v0tUdtY2ZYythiIX67gQUDXfLmN9eQZ6pSRAZJ80UHT7csWLS/28hbuMBqhKnbhaOCCqozo0U3QZ3uyC2rU5wMg0nHXKVmeMrYTi8vKLRy2+cvGFi8T3STtKPO2SmHWJd4w46Cb67lqJtLQqICpbPXnectOwbTscisCMjPGGHGA8rw1rjK2C4oryh39ZTGVmClfkuUVpDynTgyN5oOjKkpYOk7iiw02VZQNP2evDoEL2Pb0vugCKgJFp/E05wHhWi7PFVkCRR82yPrtbdj8vsn4fyG43S9xGuGaarDByppb3AF55Hr9ZWOLxG+6o500V3I7ymMTbdJgu8sKJqos76gFZM9dWmSphu2lg7BIoAsa5pFE5wHhaC6fSRUOxl/qeOxSaAQ8uzHCvLBknt/A8KD1UcT0Q8ISnc66Tfi1xyyDvdZ1PSjVFrse9ZmSKjY4BLOeReKUPi8dVFiM5j1OtILF8cKD0G4nnvfM6M4PItI7tIigCxvmk0TnAeJLK8rx10WuMRUOR9wXk9SGgSkKyncSFPG70bhSftj3hY35mqHTOxKNCXDS5XPokh3Pv1oVQBGZ9pTtygJEXUFQZiivn8BserrgkyJjs9X3p4jbzzdsGyHSG5oUaf8oIRqY7c3dZpuhgNr/AxqvDst6uc0zBU+kiM8Ws/sJUkpdAWIw20QZmzCYa03NO3lrCom/aC0/cRnBwl0IROPL27XszgvELleMlt0VljEVBcW31eZZHaXlbE6/6sxhusg3MoE02aMhJt9XfXBlLs1Y0VlDs3aE3bycB1kIC298zgpGfQ+BnEZKcJ+0+RUHxvJT+wUWUab8lYirGBmbY4p2Lm0d5vC8pGD8RFNfpYigCqyWlBzOCcbLKHVIAGIuA4oK9Lh2Z9iZtHvuzuC3QBmbcAo3rOe9ZKaA4VVAc1uVQBIyLSQ9lBONnKndgk8FYBBQHCIpp3hp9j/yI1+9b3BZoAzNugcb1nJd7EHl6JVG2KCheLyj26MBnn9NOWZcR2J7MCEbWGPdvIhiLgOJQQTGRT8h3WG/eyoBY/IBgUGwNFHnc6NwUUHxCUOxpUJy2Nri09ERGME5Qub2aBMYioHhOCig+Iv+p+3YlA2ZzgGlQbA0UsTPPYiZ6k7kyxQ8Fxd4GxekXTL4jsD2bEYyTVG5IE8BYBBRvSAFFLshYvLbABmbkFhg5cOYN9MlvZDecLgmKnwmK8xoUZ7iKvLzA9lxGMH6kcjvnBzoTjTfFeOuj0FFH9mUGzNoGBQbB0UeVHvmwmh+Lmg2M+g+LVba1YR2MbnAKOKZr5dp9lQnEVQHJcCigcbFA2KnQZsHuVK9Jo1ZYpAsb9BMRJgqwlsr2YE44cqNygjGJsNxZlTQpGXH3RaTJSyPaWsVId2flooLmVQjM3q1hXYXskIxndUbrMMYDQodgmUDYqt62iDYvapa9StPOsJbG9lBON7KrdxSjAaFFsXK23lUltP3qEZYZxNDYrNhSKg3Eh6IyMY31S5ASnAaFA0KNr6RZOhbVBsPhQB46YSU+Isb9cBqGScSW4qNygaFA2KBsVEsEgClKL32Vpgez8jGF9TuTUSgNGgaFA0KBoUKwNFoMs/1gqzZIwvqtyqDcBoUDQoGhQNipWCImDkBu1PM4LxZZVbsQ4YDYoGRYOiQbFyUASMu0rcj5glY3xG5XikMGq6b1A0KBoUDYqVhCJA20fi9WFZwMgz1stGgNGgaFA0KBoUKwvFvGB8XFDsHwKjQdGgaFA0KFYaioDxhzkyRsDIa8vcVNqgaFA0KDYZiqumfCGEPeaX7P7BRrf7HJxxGs3Um59EWDwA42FJjzNw4MA7Ro8eXav3mObHkyfz7POdKV4IcWiT/dEe3IiBvBmmdaPfhnLqj1O8JWdBe/a5aZnqTwQ03quYZY3xAZXjx7S+n7R8Qijy6rAxKaB4nEGxNYmaQbF1UNw7CRDZx14y2zQY+lnkkUmhFrHf7fruJGlKkmMkhGJNULwuBRSvlm/MZGAsHowGxdZB8aIUUHxUrw6b1TLFpsPxGEGNn0HNkjEmvpqdAoonp4DiK/KfhQyKBsVOgfaScua3U0DxGvvhqqYD0WWNx+UAYyKYpoDiHimgyBvbf2RQNCh2ChQvTgrEYPr8I/uJ08KgCByZCicCXJb9UkBxNUFxQgrfIFtc3MBYLBg7BTplbseBcuIpKRx/otYUVzcoFgpFwHhyFuAlKZMCinMJiuNS+AbZ4i3SnAbG4sBYZph0Qt32lPN+kNLpbxMUexkUC4fizALc2Ukgl3afFFCsCYpHp/QPwDhcsp87Leh6QCeAp4xtwGGPl77I4PD7CYo1g2LhUCRb7FEEGFNCsZ985K0MfkKGyTs6y+j/la5TpStfUofYSvUam8HJyQAelHobFFsCRHfhZSaB8fdps8F6+6eEIjF4UkZ/eUfljpEWKGksVJIvlax0CR2AzHBn6Vbp04wODhS3oW0GxZZCETj2lC5qFhgzQHFB9fvTOfyGsidIy0g9SxgfleJMVGUJ8G8FqTmPpplmtMFqssn60iDpMOkq6bUcDg0M0YXOmQ2KLYciYCRjvLwZYMwAReJwO+mznH40SeXvlU6RWM/eXFpXWsPiOJJj/BY7rJvVH0h8KM6jDcdKd0ovSqxzvGv6mg3el00+z+m8DoTu8x863vwGxbbA0H/qpbegeE1eMGaEIrF4YpP9Cv/6RPrQ4jiSZSw/wLr/k/abHn/Bf/ro87YCOiQc/Pb3V1mhr+f1N9Oe6QOUZYptheOsguLwPGDMAUUe47vC4vBrMdIqbpxMHLpA/LF1RFs64lnZfUUfiNM6xa4+N3rzTdHb5xQUb8wKxhxQJB7nlq6xeGxLPHK3yKoOimlvIG0VuTv5PP9UBywbBqJBsa1Zog9bwHhzFjDmhCIx2Us6z8DYFjAeSwdwdzxTuE4GUNnadq3sHXsbRcpMcZdBgwYlfWTtbgV5H6noTKtTjj+3bDUqLRibAEWXrPAORdb1y+a/nVyfizH+vNILZviWOB4vhRgqzRyVIbrvUkJxcAoo3qkAJ9A7BVqtaMe8stcdacAoKOodsw1fMsurw3iipdHtKuvIL+6x+GxJfAL7S+mQHtKjZvRCjc69i9y6w+X/RkGQdk1xvRRQHKng5kJCK2DSSeeYTzYbkxSMguKVTYQi/sJtckdIb1icFhqnQPF0F6DnmLELMTa37twgbZEEhhkzxYUFRX6aM8kUmvcJdhKsWtmWBWS7e5LYWVDcr8lQdHG6lHyEexBtZlfcksJ2ztg8Q/mBgbEpYOSNOOMlLu+vlQaGGaFYExSTvFX6eQX0EgbFXIPCN2W/sQ3A+LCgOGdBUHTx2l++clAwreaG7U5e42tl27jg3LNWu/B6p0H6/wPS59JUUyIbTJKd3paelMZIp0kbSr09u/o2Tvb/c66tbTDqzro/fMRbuSdPngwQawMGDKgpEI+RnpOmhjRBf4+VVpfYz5TPBn1lw/Ok10N2nqi/x0jfXXnllWujRo2q238T1He1i2+s1c4fnswn/hOn/v6zyM+WlX4sjQji91V9TrT4TRS/jnPE8PXSotPi9twnnvfVW39vJf1QGmaKtcFQ2WZfaRtpTWmBkB3Ddk3191mPPVsbOf61hlCcMmVK7ZZbbqmNHDmydtNNN6F+0vekQ6Vh0kHSRtIswXa3n31+Za88Wlnl9w/sfLA+N3V2Hj58eG38+PF1+2+S+u78p16onfPEc6l8o4Gfza7ty0ubSrtJB0mHWRzXZdn+QQxP74eGgdfod0Js+1SzobJW8wOzQaf4gDmzBbT5gPmA+YDnA2YMCwjzAfMB8wGDok11OmWqY+0wX262D9gIYVmC+YD5gPmAZYo2ujZ7dLXjmU91ig/YCGFZgvmA+YD5gGWKNqp3yqhu7TBfbrYP2AhhWYL5gPmA+YBlija6Nnt0teOZT3WKD9gIYVmC+YD5gPmAZYo2qnfKqG7tMF9utg/YCGFZgvmA+YD5gGWKNro2e3S145lPdYoP2AhhWYL5gPmA+YBlijaqd8qobu0wX262D9gIYVmC+YD5gPmAZYo2ujZ7dLXjmU91ig/U9tt/f18b6O8jpFOkkyJ0sr47UVo6VC58HPt7RrtmsUd/2fgwqU9KWy+r/Q+X5k9ZLksd48r00rl3DHzlUH32bGNdmtmush6LeNwnJmZdHJ+q7TtZP8zAu+n9uc+++9aGDRs2LWOubbX11mhB6SppaqBP9PlxhCbqO7RjUM6Vt8+v7NhMDQ36YqQ++6Y49vFBuZ1TlGlmvRfSeUd4vvSk/j93m+rSzHaV9Vj4yduBvafoc0JM7H6u76+WelhffD1ON99ii9puu+/+FRQHb7st+r00VbpSWk/6rrSc9J0Y9QrKufL2+ZUdm6n/CvqEfrlBmjvh8U8Myn0v4f6N6txDx5lLmiXieEvru30Df+E47HtScP7f6nN1aSlpTekAqW+T6tSozvW2z6Y69I6ox0z6bpC0szRHCeqZpI3bBbZ+QZ87SStJcXG7grYt2OZ2zRz4MbZO0r6W7bO1fhVzr733ng7FJVTBd6Wx0uxlq2wX1+eIwOGfDT7/FDhUI0c5Idh/1ybZDrBNkH4ScbwzgnMND7YBk3HSFMkfOO8M9juwSXVqZIO47UAbWL8l9QnVZT79PSmo56ptrmeS9tEWkhgGzQ0qUF/a9H3pM2mNstU3DMUtVMGPpGvLVtEur4+DIhnWiMD5+QwHcziAmg3FLYNznxzRH2vru19Lg4Nts+rzDmlyaN899Pc5EpllkoAvch+ybkAStiNZzOES7Zy/BPVsZAOyrVukL6T+Fagv7flFYPsNy1bfMBQ3VQXJBPJCkZGrUcD21D4ug+D/7M+0DCdcSCK9R/x/zgaGY7tfxpV15QnQsGNxPr4nI6Ys8utMG74Rc1z2ZfpHfeeR2Jfj045G0y2mvmmnDA6K2wfn+F99EsxnNbBLPSiSDUXZjO/YFrYXttozOC8Z1gJB//hTab+fFtH2eySguFiwL/3BsZmCxwV6vXrVm73QH3HtoY/C55tX340K2sM0M9z/9BH9GVVP+riev8WV45zYCx8I+zi+1gh+cdup658loLhkjuPQx/X6hvOznUHDrwvl6tkjvNyC/U4PbM8ShfMLF0fu2CxvuBj2Y7oeFzg2fkgd8VFXLz6xv1/vMGv4e5YwFBfVl29IjwQdl7WTWIf8h7RDTAfhNEyzzg4qT/DeJ90ovSy9Lb0Z6B19/k06VAo7Gx30U+neUBlXlk+mR0CENRfXHhzwLolp6JigLOf5u4RhqP9V0osxx2XfR6XrJdrJ/hz7GInsKA7imwRt2dirSxIbOygOCcoBADIDwIhzhZ3JHTMKiqwnXSwxFfft7Nv7OW0DfP28ejLdZRbBOSdK+Ak6ONhnHX0+KLEOzeDysMTUmf39/ng1sIF/bOq7mnSZxJpYVL34Dn+JGlAAAf0RV+5xbSPrc+DBb5jaAxHqh49Q9gEJmFOf8ySmpASmsyeDOH7IEkDUuWgn39M34SUL1lKxCdnpP0PlKYOvnS99yztfEt9w+1wStGWbjOU5zkEScQFAos7NUgIDHSBz27fW/2+SsKHfz74/HeXtD6wukD4P6vu+PvEj2r+ut98u+j+D1usxxyYGifvDJMcF+oprImMl7PyaV5b9/yWxzr2ZRN++FDr2K/r7RkFxbX9NkYbieDjK7dL2Ep3J1ChKNIIgC48cywTH+ItXYd/IWwXbL9InTs65OOen0nVBw/6gT4QBnwi2c+HABwCOTjm2YwxXxn0S/FdLGP4ziQ6kHnSuC1gMRT0ISI7Pmoxbu6OzL4w4LufCsTk3wsgc97Tg772Cv/02YyOXmWwUsb1eAIShyL6Axx2Pjo4qH4bi4toPeFDnm2NsRtsAO/vgdC5rpM9wNr5n0PydxMDBBQnOvVuwDXAzWhPgBMaXEralT/jO2Q2IujrjL4CYYzNQRfUlTjxUioIidaSfAEPYBzjW/cGxqQf+A9yY6hMEnBMfoRwgdLMFBruHvL+p68+D/Z/RJz4TPhd/U08gB3AJbNfGfYOynA/w+G2k7vga2wB4eMBIAkd8Gx/n3Kz51otbbM/28IxgWFAHEo2oc9Jm6uigyODOAMl5sSHxFrbJpfrue97x6D8GFtrJsW6VaP/lkhsQdtT/sd+7wfdRx6Wvnw6OcWxwfDJw59/EPH3h+gmOYHfO+UnwyeDltvM5gvMKiu8Iiiv4V59xaCpJY13QN/okk1vDazgGxRiUGxj6nm1/DSrmMqzR+htIxY1yfbXtMenfEpkcxyAVpgznjpoe+Z2KExCcZHZ8v2JQlmAJT1sc2A6IqLd/TKYE1Js2kgGy7TsSDkLGEZ6yM6jQ0WStSZzc3ycKig6MLmOk3uGM0UHRBechQX3jnN6dE8cFerRtW6++bjCLgjCZEe1zAcDozaA4WfIHTdcWgtKd7+jgXGQqaW2TZH/6gsGW9nzbOwf+wHdR/kMAMXtxV/p76/8EE0HH9K3eeVcObMGU1u23R3Augjlu+YSZBvX5fkY7MDA50DeKWbZ/IAFC5zeL6f9k8sQZ7fXbuLT+/lAi3tz3fwzqm+XiDiCjDuEEgb5ixkaCxOyhnp0ZwJ6XGFDpJ5ZXGLDpJ3/Q9f2aDJLzMsBFzbB2EhSnCoqn+lDkAOxMEOOkjDr1dK62kwqPl4CCqwCGomFMif2Gra6/HaA4j8sU2Zfpe5wRGMVJgb8Z7LNW0Diyj0aBsaz2mSTdFuwLFDEMI0m4rAsUP3jiju/A4aDIfmRPnAuA+OWYjkY5QaO6sz0OimxjcKBdHJvM2T+eg6Ib2c8M9kvixEx5OOY+3jEdFMPn4ZxhKJKNOSj6A0QUFMkEOFejIEhiq7h9mAVwDpfV43eur8MZE8cIQ5EZEeUBQaN64KNMCQnuMBQZmOLKD9A2YokpYaNzxG3vp7Jk643iFii4GZh/PjLocL9zLjdw+Vkfsw18nfhKW99fBOcJLyXhzy9KT0nhxCLqHLQBW9OHDMRkoLCob0ydWO6hfdvHbO8nKE4UFM8MQzFtAxnZONH/eCcCeC6Tclkk37kR22WQNJzpM1BkbSju3Ew3WLdwUHRwZTpGB9NpcXKdv29wfAfFayLO5+pHEDSyAxm1nymyP1NzviMjdCNRf/3fQTlJR4fPWw+K7Eu2S1bjwOgykTAUncP7EI9ro8ta/KUApmhR8I2CIjMOB0V/XY4slWP4maKzIwNxI5uHt5Ox0x7qi6J8AMC7qZMLQrJXB8XwbCEKigz41JtMo1EdGdzflFh+CENxWJ3ytAMoMr1sdI5mbF9E52G6TXy4bJkYJLsH6PQh52F2RpbImrEPm70Dm1A+LvbICOHDwqE2uUwx7IvUAyC+Lx0v0XdRxz5S37tZEokRPs9aPlB8WYrqU9qC/elHlyiE7dhfUPxAUDwjLxRX0UkI+uGhhu8QVACn5+TLB8b1R9A8UOScTKEBKmLqGhb7YGR/NCwSioCQ9SGyYZf5nBHYYfeQfZI6diMochwyW9rppgZ8h1P5DtBpUCRo8TnaSD/H+QDfu4sqBsUZgftX2eY9yU9IXOa+ZeCv/xXYGD8M+yxxhd9h/6j4w/b0Dxc9/KlyPSgyBaYMx6sX1xzzHMmBmil0M6D4YTOgSPBHQRHgPSsxyjB6Hhc01l+nygpFMg0MB4DIzqhDlMhSXXbpOrQeFN06iVvvrAcuFm/DmSL7Dw6+J7tllCXD5YIOI1lSEPr7JYEi+zONYc3LLfK7bM+Nig7O4SlLVJ3cdKkVmSKL3NiRJZE09nFrpNiZfo7zAfzjiuAcWaHIgE4d3bp0vXpWJVOkDWTzYSi6pSlmOwCHTJClK7eeH2478RVnf2y/p8QUFxa4WUMcFJk+sx9roxvW6VPOt0zIX0oFxXWDQLwswqkPCJyJ9TfI/pjkwyErFN0aj8tC0wRTPSheHdTXXx+NO3bUmiL70jlMnXAEApZg+lmEbZLWOSkUOd5y0jNBfzBqAkgHRS6QUBe3dFHv/ExP2Hc/r95FTZ9ZL86ypsh6NVM9d9WyXntc27NCkSWQpNNnAptp6f2e7dyFFqZvcfVs9fSZejwsvS756/nEJMkGWZpbCz8rh/9ynksD+xF7/B0Hxbm0jQsn+HDS+HD7FQZFOnTxwEgYqp4YIaA5AMBhtohoSB99R5bEdvSj0D50wBiJVLnemiLZhL+myKIqkH1JYvGcNYtwXbmatkBEnVYK6nJtxDYHIGBGfVh3CR+X79aXyMqYvket0QET12acLm6NI0nHu3U4ri4m2Z+p9JPe+XcJyvFJna6U2CfKZgvpexz3ToklAHdhgvMOCsqfElEPFuABsFuIZ2rL1IzvotYU/azQrUkzwDH6R9XL9QG+6frUrZmylk1fR/kqxyKrYDD2s3rWFFkf5Luovrlb3/tXn1mzIlDpS2YCcXXk+4OD4/r+RbbEubgA0giKWS60YG9sEGcH3zbUkf53yykkM+G1bvqa9U3qPEHi6nO43lztbcQKGLGBRJwSr0CP47hZY1Ts0C8MdtiReodjkDbyHevJfp2AIlktWWajNUUXE+E2sabI9Hn6hRaM9ZDEJe2JCfSx9iFwAAPrV1GXuDmpC+rx+v8SoYbQMAIQ4/eLMLyr9OXaxsjlT4W5yuY6jrpE1fkDfe9GJ3cspsacj/uSwkbpq+/cAi52iDquazfHoE5RHcvVVwemU+u0KwnkHKjTrEmSMbrzOwfAid3yAMsdUW0jAHBI2kZ25TueyxRPj2gPMKQMGRFtov1usPShyMI5+/kXWnrr71HB9/RnXF/Sv/QJGfg6EkEAeDlenL9yLNrqBigGceoHFJkexkGRLA/7cQ7XR8CQ81Mmro58z3YGTMDjytJ3fB+1Luf22TTYpx444/zlzDo2CNurq1vrH6P4AJH9ePS6AZdV4GsUascLFCv/qzjl8FbaXN4WMTn7CiUb8yC/XLMgPlFh2WA0juoursWORmT+F9lg5uyTnHXWhhtKGy16QQo83qMRVwJzwoaGBUhsE++0isdxG0cZ2+o7YRpNTR34dzn1+nvpdEdDrnOVfaNeZ8ZKFcAQTEcbZggfcuCYBEQZGgY8GY9dQk07t6cGQq/wfJD7IkMGWJgWAJD0RkZlc1sNlOEbYheAiEqPaSSZwtreyV4zz0uT9YkjXQX/OFjo+9WCNk+aKR/1EHprPYgEHs8AZlmAKyD+d1V1kpC8ipc5TfUff/lnygU4ZZBr5Tr45kQWHQ9NN3l0r1YoWsi/qkXVulXgR5I7v52/EnZh7hDNH3q9u0HZixPBblb8RtknOy9h5uN4MasUmbo45Ngka81zs+tzCFyzKgYH93B0Z4O235nRSOCbdfH0HxNGWKW+e9+lwvQHE4RiNGJufISQK6Cvv8Rm1iNIsa7VhKYBujrJ9tVaFdVsdkyxSdbCeWHMiwASNJQie3dYa2hZ99LqLhWwVwYAQs4vjtPObwoG3+rQbUh6zHTcEbZdHtrL+du/N8shl9SmbvbsvZvAPjtq6NWgHF0TIqaxpZbsxtRgdnPQbrXNtLrFswTfDF2gjTZzLBB6VvhByHtrLWwgWkuHXWrPWycgayon2Ai13vSiz/hJcPij53249fNBQHBuDgalLcHL/tRogZCVns5YoZ4OMKqhNriFxc4vtxUjgTBIKs17Cd9pe1fVYv65s4H2AtD/+Nu0Lb0b5TNBR/IMO+IPm3dVTFoMCNdZVtJa66+uIqJFdPo7JAd4sRVza7ai3GBoCOGQBJYrgjxL8oVZW4zV1PH4r/DyD+m59EBJimAAAAAElFTkSuQmCC')as CompanyLogo, Signature from tbl_Email_Setting  where ClientKey=@ProductKey      
      
        
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetSigAndLogo_GoogleSync]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Sp_GetSigAndLogo '3'  
  
CREATE Proc [dbo].[Sp_GetSigAndLogo_GoogleSync]   
@ProductKey nvarchar(50)   
as    
Begin    
    
select CompanyLogo, Signature from tbl_Email_Setting  where ClientKey=@ProductKey  
  
    
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetSigAndLogo]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_GetSigAndLogo]         
        
as          
Begin          
    
       
select isnull(case when CompanyLogo='' then NULL ELSE CompanyLogo end,'iVBORw0KGgoAAAANSUhEUgAAAUUAAACRCAYAAABUrKrQAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAADImlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0
IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemt  
jOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS4zLWMwMTEgNjYuMTQ1NjYxLCAyMDEyLzAyLzA2LTE0OjU2OjI3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2
  
NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZ
  
SBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoyNjlFMTA4QkU0MTMxMUU2QjUwMkY5ODA1NUI4M0Y5RSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoyNjlFMTA4Q0U0MTMxMUU2QjUwMkY5ODA1NUI4M0Y5RSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlk
  
OjI2OUUxMDg5RTQxMzExRTZCNTAyRjk4MDU1QjgzRjlFIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjI2OUUxMDhBRTQxMzExRTZCNTAyRjk4MDU1QjgzRjlFIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+NMFJfwAAJ/VJREFUeF7tnQf4HFW5hze0EAIkgBSpgYC
  
C0nsLobcQOhh6UzAgGMGLF0HQS0eKCAgI0puBYLhEuIGgCU0RpBfpAaR3QigJIff3hjnxZJjZnbKzO7P75Xl+z+a/M2fmnO9833u+c6ZsrfdlN9VaoN46xyLSEhVXP9V/vqLtVbtoRG2b0ffWpk6dWleTJ0+uDRkypDZo0KDa4MGDnRbQ/9eXVpNm8r7397H//8deWW3RV7ZdUxogLebbeeDAgbXRo0fX7buP1XfzXTmq1uvSkUXF36zy012ky6
  
SRHaCb1Ibh0kXScdKu0tJFxGIzO2QOVXAZaQvpKOly6Q7pCelZ6XnphQ7Qc2rD/dKJUv9COiUbFFdVYF4pPS+9LL0k3S8NlXobHKcPGlkh6MoBwsukZwIbY+sXpL9I+0izlQCKawex95o+P5QmdIg+Ujs+kN6RaNuL0r3SBdJu0rek2fLGZDOguJYq8UvpdmmiNLWL9Krauk7eTgiXz5ApDlQwvi9NjdEYfb+IgTE3GLeVDT+qY2fs/3tBsUcbM
  
8Xl5U9vd1EM+rz5TO2+WRoqfTNrXGaFIqn5YGm09G6XdoDrDLLhObN2QFS5lFCcQ9Pn0Q0ClWC9U1rYwJgZjGTbTyew81RBccs2QvF3XR6PLi6Z0ZGsLZg2NrNAkenxGDP89Iz4c9li1bSGr7d/SiiuICh+nCRYtc84aX4DYyYwDkhoY6B4cZug2Fd+9TeLzRlmq/8OMkeW9xLxLtFOwcFIRy+UgEA3TZGTtHVQUoMn2S8lFDcRFOOmzVHfM5U2
  
MKa/0HJACiiKiW250LKY/OsZi89IPt0qu6yYKP6S7KR9NpeeNGPHDgY7JrRjokEoJRR3SAlFQHmrxNXTvBcduqn8oSmgeEeboLioxWndhI211r0axWqSID1UB/nUgFjX2Bs2MnSa7SmhuHMGKDowzmNgTDwwVAGKvWxpq+Es9kvZiLXGmeJishEUf2UwbGhkrkBz/2IjWybe3iIoAsaR0hwGxkRgrAIU8bGfW8w2jFmWxM7IAsUTzLiJjPuLZgK
  
RY7UQioBxhDSXgbEhGKsCxfnlQ+MsdhPF7smy08zh+I3LXg4xozY0KjeSYtSm3o7TBigCxj9KPQ2MdcFYFSgS0wtLJ0mPS0wXk1ws7MZ9Jss2ByaB4qbaiZsg8xrpPR2De4W4l5FHc04LIHKKPqsqIHiMNERaqtkZojteizNFd4X6ckFxNgNjLBirBEWX7Mwjn+LR2mWlFTpAq6sNW0tc5+DxxaekvA+MfKxjcNzpCWI4U8SIea4yT1H5P0s/kV
  
aRYhcziwJKJxy3TVAEjlcYGDsKionXsSscN5uo7mdLPPaXNZG7S2Wnz/jCRsuzjshD5xtIPO3SDZ1RWBvbCEXAeIHUwzLGr8GxipliYT5awhgnG/6NxJQ4LRxZYpg+jfaN9m1teCvDAV9WGR7G7qYOKLStbYYiYLxQmsXAOAMYDYrViPHNxCJeQJMWjI+pzLxwzA/uLLff8IaK5QyIzR0QSgBFwHi2QdGgWNHY5u1V92UA404+FFlLfDTlQQBi5
  
jdRVNTYhWaIziYlgSJgPMPAOB2MlilWI1N0Mcr7W9NeH/mTD0Xev5Ym3XxT+zPdbgkkuu08JYIiYDzN1hingdGgWL14X1/s+CAF23hPY18HtR+nKAg8D+42ULWyvSWDImA8wTJGg2IrY6BJ5+qh45yXgm2TtO/GDopXpCjINHvagqSpGBuUEIqA8bguB6NlitWM+TXFKd4+nnQmfKgD2z0pCp1pMCwGhi1YU3xSYHtQSvOqMX/fo7sYjAbFakJx
  
FsXVIyn4drKD4tMpCu1jUKwsFG8X1L4tjc0BxiO6FIwGxWpCEcZdl4Jv51Ggj8TjeEnSS14hxpu3bepcoA0KnD7fLaDNLC0m/SMjGL9UuZ92IRgNigX6fMFM+W1CvsHAS4Db4hK/spcEirwEYWDBDeh64BYMRfdy2cUFtgcygvFzlTu4y8BoUKwuFE9PyDcYeBkA4oHxNFDkUb6uB1eRNigYin08mPXX/x/PCMYpXQZGg2J14553JyZJ+gyKRYI
  
tz7FbCEXuv2ONMSsYJ6rsfl2SMRoUDYqRZGX6bJliwc7RYigCxmWlZzNmjJ+p3N5dAEaDYsF+nyeRaFDWMsUCjduSZYM2QBEwrpQDjBNUdvcOB6NB0aBomWK74NomKALGFaXxGTPGD1Vupw4Go0HRoGhQ7EIoAsY1pFdygHHbDgWjQdGgaFDsUig6ML6aEYzvqNygDgSjQdGgaFDsYigCxvWkNzKC8X2V26zDwGhQNCgaFLscioBxA+mtjGAEqJ
  
t2EBgNigZFg6JBcdp7BDeWmBJneYnEayq3boeA0aDYRVDsZ0+0lOsJnTZefQaCUdpC33/c5WA0KFYXiqkf81ssBRQn2LPPxQO0hFAElFtJ72YE4wsqt1rFM0aDYnWheFaKx/wu5WZkfu806VtyPtO+W7VrWtkt5y0pFAHj9tKnGcH4UsXBaFCsLhQvTQHFC90TGk+lKPSDboFTu9pZYigCRm7Q5kbtLGuMT6vcChXNGA2K1YUiP7KX9IUQZzgoj
  
ktR6Nx2waJbzltyKALGIRLPPGcB4zMq990KgtGgWE0o8haw11Pw7UgHxYtTFOKHpu2nTQt0kApAETDuJfFexSxgfELllq4YGA2KBfp8gQnPz3TsLxPybYr2295BcWjCQi4FPbrARrTkpQtlrn9FoAgY982RMT6isrzPMe6Kd9m+NyhWD4pp3hUL23gL2GIOQKumhCK/jrVWmcFS5bpVCIqA66CM2SIZ5sMSbwAvGwCj6mNQrBYUZxIDbkjJtb/B
  
DQfFufRHmsVIqPqktFyV4VPWulcMigBkqDQpIxz5rZhFKwBGg2J1oDi7Yvv8lECEaYf4UASOR2Y4CL8CuH5Z4VLVelUQioBxmMSPWmVZY7xP5RYuORgNitWA4oKK+z9mYBkXY3iQZXqmCBS5ifulDAdjHn641LuqECpbvSsKRcDIr/xlgSJl+KXBBUoMRoNiuaHI7zsPltLcXujfpnOK40D4osYRGaDoDnynyu4kkbp2/cWSPDaoMBQB41HSlIx
  
wHFdiMBoUyxnXPRVr20gjcrCLH+7jV02ncSsMrzn0Zdq1xfBNkX/XMX4lrR4AskceQHRj2YpDsYfAdlxGKJIx3iHNW8KMscpQJAarLi6czCzNKi0kbSmdKnFx5IscQIRfe/qcicroVtMO7+U8CSf6XHpNukPiMRuePzxZOl46ocKi/kdJh0k7SKypLiv1bRbAKw5Fd+X2+BxrjLeqrP9TrGW4Ol1mKAKLJaV1gwA/Vp8XSTdKt0pjpNsrLmai90
  
svSxMl7ilM+pRKvf24IDMDB+OmuUO04+QmnbQZFS/7Md6Qre4KHPEAfXJ/VOYlhA6B4kwC26k5MsY/q+w8JcoYywhFZmM/l0ZKz1u8pobkaNls3qRQJKDJhJLeCV52aLW6fmTIrHFsLfHCjVSA7BAokt3NLJ2ZA4w3qmzPkoCxLFCcW/60uzRWasaMrtWxUZbz3S37fSMqNhsFK0+6fGojUOoRyO941mhZs0h8AaqDoAgYyRjPzgHGESo7dwnA2
  
G4o4j/MQh62eMwVj8QmSwqsS0byrxEU2b6r9I51RO6OYE1nYJKsscOgCBh7SRfmAOPVKjtbm8HYTihuJL/5i8Vg7hgEiBdIddf/k0CRfVaR0rxJpywpctnqwcWnEyWeIIq1fQdC0WWMF+UA45UBXNt10aUdUOTe319KkwyIuYHIuj+ZdkPmNdzBO0gf/Z+rxrx9u2ywqVp9/iobcsU6On2/aERtm9H31qZOnVpXkydPrg0ZMmTnQYMGJb1hmhuk
  
23lVd06dH7glrW94v0tUdtY2ZYythiIX67gQUDXfLmN9eQZ6pSRAZJ80UHT7csWLS/28hbuMBqhKnbhaOCCqozo0U3QZ3uyC2rU5wMg0nHXKVmeMrYTi8vKLRy2+cvGFi8T3STtKPO2SmHWJd4w46Cb67lqJtLQqICpbPXnectOwbTscisCMjPGGHGA8rw1rjK2C4oryh39ZTGVmClfkuUVpDynTgyN5oOjKkpYOk7iiw02VZQNP2evDoEL2Pb0
  
vugCKgJFp/E05wHhWi7PFVkCRR82yPrtbdj8vsn4fyG43S9xGuGaarDByppb3AF55Hr9ZWOLxG+6o500V3I7ymMTbdJgu8sKJqos76gFZM9dWmSphu2lg7BIoAsa5pFE5wHhaC6fSRUOxl/qeOxSaAQ8uzHCvLBknt/A8KD1UcT0Q8ISnc66Tfi1xyyDvdZ1PSjVFrse9ZmSKjY4BLOeReKUPi8dVFiM5j1OtILF8cKD0G4nnvfM6M4PItI7tIi
  
gCxvmk0TnAeJLK8rx10WuMRUOR9wXk9SGgSkKyncSFPG70bhSftj3hY35mqHTOxKNCXDS5XPokh3Pv1oVQBGZ9pTtygJEXUFQZiivn8BserrgkyJjs9X3p4jbzzdsGyHSG5oUaf8oIRqY7c3dZpuhgNr/AxqvDst6uc0zBU+kiM8Ws/sJUkpdAWIw20QZmzCYa03NO3lrCom/aC0/cRnBwl0IROPL27XszgvELleMlt0VljEVBcW31eZZHaXlbE
  
6/6sxhusg3MoE02aMhJt9XfXBlLs1Y0VlDs3aE3bycB1kIC298zgpGfQ+BnEZKcJ+0+RUHxvJT+wUWUab8lYirGBmbY4p2Lm0d5vC8pGD8RFNfpYigCqyWlBzOCcbLKHVIAGIuA4oK9Lh2Z9iZtHvuzuC3QBmbcAo3rOe9ZKaA4VVAc1uVQBIyLSQ9lBONnKndgk8FYBBQHCIpp3hp9j/yI1+9b3BZoAzNugcb1nJd7EHl6JVG2KCheLyj26MBn
  
n9NOWZcR2J7MCEbWGPdvIhiLgOJQQTGRT8h3WG/eyoBY/IBgUGwNFHnc6NwUUHxCUOxpUJy2Nri09ERGME5Qub2aBMYioHhOCig+Iv+p+3YlA2ZzgGlQbA0UsTPPYiZ6k7kyxQ8Fxd4GxekXTL4jsD2bEYyTVG5IE8BYBBRvSAFFLshYvLbABmbkFhg5cOYN9MlvZDecLgmKnwmK8xoUZ7iKvLzA9lxGMH6kcjvnBzoTjTfFeOuj0FFH9mUGz
  
NoGBQbB0UeVHvmwmh+Lmg2M+g+LVba1YR2MbnAKOKZr5dp9lQnEVQHJcCigcbFA2KnQZsHuVK9Jo1ZYpAsb9BMRJgqwlsr2YE44cqNygjGJsNxZlTQpGXH3RaTJSyPaWsVId2flooLmVQjM3q1hXYXskIxndUbrMMYDQodgmUDYqt62iDYvapa9StPOsJbG9lBON7KrdxSjAaFFsXK23lUltP3qEZYZxNDYrNhSKg3Eh6IyMY31S5ASnAaFA0KN
  
r6RZOhbVBsPhQB46YSU+Isb9cBqGScSW4qNygaFA2KBsVEsEgClKL32Vpgez8jGF9TuTUSgNGgaFA0KBoUKwNFoMs/1gqzZIwvqtyqDcBoUDQoGhQNipWCImDkBu1PM4LxZZVbsQ4YDYoGRYOiQbFyUASMu0rcj5glY3xG5XikMGq6b1A0KBoUDYqVhCJA20fi9WFZwMgz1stGgNGgaFA0KBoUKwvFvGB8XFDsHwKjQdGgaFA0KFYaioDxhzkyR
  
sDIa8vcVNqgaFA0KDYZiqumfCGEPeaX7P7BRrf7HJxxGs3Um59EWDwA42FJjzNw4MA7Ro8eXav3mObHkyfz7POdKV4IcWiT/dEe3IiBvBmmdaPfhnLqj1O8JWdBe/a5aZnqTwQ03quYZY3xAZXjx7S+n7R8Qijy6rAxKaB4nEGxNYmaQbF1UNw7CRDZx14y2zQY+lnkkUmhFrHf7fruJGlKkmMkhGJNULwuBRSvlm/MZGAsHowGxdZB8aIUUHxU
  
rw6b1TLFpsPxGEGNn0HNkjEmvpqdAoonp4DiK/KfhQyKBsVOgfaScua3U0DxGvvhqqYD0WWNx+UAYyKYpoDiHimgyBvbf2RQNCh2ChQvTgrEYPr8I/uJ08KgCByZCicCXJb9UkBxNUFxQgrfIFtc3MBYLBg7BTplbseBcuIpKRx/otYUVzcoFgpFwHhyFuAlKZMCinMJiuNS+AbZ4i3SnAbG4sBYZph0Qt32lPN+kNLpbxMUexkUC4fizALc2Uk
  
gl3afFFCsCYpHp/QPwDhcsp87Leh6QCeAp4xtwGGPl77I4PD7CYo1g2LhUCRb7FEEGFNCsZ985K0MfkKGyTs6y+j/la5TpStfUofYSvUam8HJyQAelHobFFsCRHfhZSaB8fdps8F6+6eEIjF4UkZ/eUfljpEWKGksVJIvlax0CR2AzHBn6Vbp04wODhS3oW0GxZZCETj2lC5qFhgzQHFB9fvTOfyGsidIy0g9SxgfleJMVGUJ8G8FqTmPpplmtM
  
Fqssn60iDpMOkq6bUcDg0M0YXOmQ2KLYciYCRjvLwZYMwAReJwO+mznH40SeXvlU6RWM/eXFpXWsPiOJJj/BY7rJvVH0h8KM6jDcdKd0ovSqxzvGv6mg3el00+z+m8DoTu8x863vwGxbbA0H/qpbegeE1eMGaEIrF4YpP9Cv/6RPrQ4jiSZSw/wLr/k/abHn/Bf/ro87YCOiQc/Pb3V1mhr+f1N9Oe6QOUZYptheOsguLwPGDMAUUe47vC4vBrM
  
dIqbpxMHLpA/LF1RFs64lnZfUUfiNM6xa4+N3rzTdHb5xQUb8wKxhxQJB7nlq6xeGxLPHK3yKoOimlvIG0VuTv5PP9UBywbBqJBsa1Zog9bwHhzFjDmhCIx2Us6z8DYFjAeSwdwdzxTuE4GUNnadq3sHXsbRcpMcZdBgwYlfWTtbgV5H6noTKtTjj+3bDUqLRibAEWXrPAORdb1y+a/nVyfizH+vNILZviWOB4vhRgqzRyVIbrvUkJxcAoo3qkA
  
J9A7BVqtaMe8stcdacAoKOodsw1fMsurw3iipdHtKuvIL+6x+GxJfAL7S+mQHtKjZvRCjc69i9y6w+X/RkGQdk1xvRRQHKng5kJCK2DSSeeYTzYbkxSMguKVTYQi/sJtckdIb1icFhqnQPF0F6DnmLELMTa37twgbZEEhhkzxYUFRX6aM8kUmvcJdhKsWtmWBWS7e5LYWVDcr8lQdHG6lHyEexBtZlfcksJ2ztg8Q/mBgbEpYOSNOOMlLu+vlQa
  
GGaFYExSTvFX6eQX0EgbFXIPCN2W/sQ3A+LCgOGdBUHTx2l++clAwreaG7U5e42tl27jg3LNWu/B6p0H6/wPS59JUUyIbTJKd3paelMZIp0kbSr09u/o2Tvb/c66tbTDqzro/fMRbuSdPngwQawMGDKgpEI+RnpOmhjRBf4+VVpfYz5TPBn1lw/Ok10N2nqi/x0jfXXnllWujRo2q238T1He1i2+s1c4fnswn/hOn/v6zyM+WlX4sjQji91V9Tr
  
T4TRS/jnPE8PXSotPi9twnnvfVW39vJf1QGmaKtcFQ2WZfaRtpTWmBkB3Ddk3191mPPVsbOf61hlCcMmVK7ZZbbqmNHDmydtNNN6F+0vekQ6Vh0kHSRtIswXa3n31+Za88Wlnl9w/sfLA+N3V2Hj58eG38+PF1+2+S+u78p16onfPEc6l8o4Gfza7ty0ubSrtJB0mHWRzXZdn+QQxP74eGgdfod0Js+1SzobJW8wOzQaf4gDmzBbT5gPmA+YDnA
  
2YMCwjzAfMB8wGDok11OmWqY+0wX262D9gIYVmC+YD5gPmAZYo2ujZ7dLXjmU91ig/YCGFZgvmA+YD5gGWKNqp3yqhu7TBfbrYP2AhhWYL5gPmA+YBlija6Nnt0teOZT3WKD9gIYVmC+YD5gPmAZYo2qnfKqG7tMF9utg/YCGFZgvmA+YD5gGWKNro2e3S145lPdYoP2AhhWYL5gPmA+YBlijaqd8qobu0wX262D9gIYVmC+YD5gPmAZYo2ujZ7
  
dLXjmU91ig/U9tt/f18b6O8jpFOkkyJ0sr47UVo6VC58HPt7RrtmsUd/2fgwqU9KWy+r/Q+X5k9ZLksd48r00rl3DHzlUH32bGNdmtmush6LeNwnJmZdHJ+q7TtZP8zAu+n9uc+++9aGDRs2LWOubbX11mhB6SppaqBP9PlxhCbqO7RjUM6Vt8+v7NhMDQ36YqQ++6Y49vFBuZ1TlGlmvRfSeUd4vvSk/j93m+rSzHaV9Vj4yduBvafoc0JM7H6
  
u76+WelhffD1ON99ii9puu+/+FRQHb7st+r00VbpSWk/6rrSc9J0Y9QrKufL2+ZUdm6n/CvqEfrlBmjvh8U8Myn0v4f6N6txDx5lLmiXieEvru30Df+E47HtScP7f6nN1aSlpTekAqW+T6tSozvW2z6Y69I6ox0z6bpC0szRHCeqZpI3bBbZ+QZ87SStJcXG7grYt2OZ2zRz4MbZO0r6W7bO1fhVzr733ng7FJVTBd6Wx0uxlq2wX1+eIwOGfDT
  
7/FDhUI0c5Idh/1ybZDrBNkH4ScbwzgnMND7YBk3HSFMkfOO8M9juwSXVqZIO47UAbWL8l9QnVZT79PSmo56ptrmeS9tEWkhgGzQ0qUF/a9H3pM2mNstU3DMUtVMGPpGvLVtEur4+DIhnWiMD5+QwHcziAmg3FLYNznxzRH2vru19Lg4Nts+rzDmlyaN899Pc5EpllkoAvch+ybkAStiNZzOES7Zy/BPVsZAOyrVukL6T+Fagv7flFYPsNy1bfM
  
BQ3VQXJBPJCkZGrUcD21D4ug+D/7M+0DCdcSCK9R/x/zgaGY7tfxpV15QnQsGNxPr4nI6Ys8utMG74Rc1z2ZfpHfeeR2Jfj045G0y2mvmmnDA6K2wfn+F99EsxnNbBLPSiSDUXZjO/YFrYXttozOC8Z1gJB//hTab+fFtH2eySguFiwL/3BsZmCxwV6vXrVm73QH3HtoY/C55tX340K2sM0M9z/9BH9GVVP+riev8WV45zYCx8I+zi+1gh+cdup
  
658loLhkjuPQx/X6hvOznUHDrwvl6tkjvNyC/U4PbM8ShfMLF0fu2CxvuBj2Y7oeFzg2fkgd8VFXLz6xv1/vMGv4e5YwFBfVl29IjwQdl7WTWIf8h7RDTAfhNEyzzg4qT/DeJ90ovSy9Lb0Z6B19/k06VAo7Gx30U+neUBlXlk+mR0CENRfXHhzwLolp6JigLOf5u4RhqP9V0osxx2XfR6XrJdrJ/hz7GInsKA7imwRt2dirSxIbOygOCcoBADI
  
DwIhzhZ3JHTMKiqwnXSwxFfft7Nv7OW0DfP28ejLdZRbBOSdK+Ak6ONhnHX0+KLEOzeDysMTUmf39/ng1sIF/bOq7mnSZxJpYVL34Dn+JGlAAAf0RV+5xbSPrc+DBb5jaAxHqh49Q9gEJmFOf8ySmpASmsyeDOH7IEkDUuWgn39M34SUL1lKxCdnpP0PlKYOvnS99yztfEt9w+1wStGWbjOU5zkEScQFAos7NUgIDHSBz27fW/2+SsKHfz74/He
  
XtD6wukD4P6vu+PvEj2r+ut98u+j+D1usxxyYGifvDJMcF+oprImMl7PyaV5b9/yWxzr2ZRN++FDr2K/r7RkFxbX9NkYbieDjK7dL2Ep3J1ChKNIIgC48cywTH+ItXYd/IWwXbL9InTs65OOen0nVBw/6gT4QBnwi2c+HABwCOTjm2YwxXxn0S/FdLGP4ziQ6kHnSuC1gMRT0ISI7Pmoxbu6OzL4w4LufCsTk3wsgc97Tg772Cv/02YyOXmWwUs
  
b1eAIShyL6Axx2Pjo4qH4bi4toPeFDnm2NsRtsAO/vgdC5rpM9wNr5n0PydxMDBBQnOvVuwDXAzWhPgBMaXEralT/jO2Q2IujrjL4CYYzNQRfUlTjxUioIidaSfAEPYBzjW/cGxqQf+A9yY6hMEnBMfoRwgdLMFBruHvL+p68+D/Z/RJz4TPhd/U08gB3AJbNfGfYOynA/w+G2k7vga2wB4eMBIAkd8Gx/n3Kz51otbbM/28IxgWFAHEo2oc9Jm
  
6uigyODOAMl5sSHxFrbJpfrue97x6D8GFtrJsW6VaP/lkhsQdtT/sd+7wfdRx6Wvnw6OcWxwfDJw59/EPH3h+gmOYHfO+UnwyeDltvM5gvMKiu8Iiiv4V59xaCpJY13QN/okk1vDazgGxRiUGxj6nm1/DSrmMqzR+htIxY1yfbXtMenfEpkcxyAVpgznjpoe+Z2KExCcZHZ8v2JQlmAJT1sc2A6IqLd/TKYE1Js2kgGy7TsSDkLGEZ6yM6jQ0WS
  
tSZzc3ycKig6MLmOk3uGM0UHRBechQX3jnN6dE8cFerRtW6++bjCLgjCZEe1zAcDozaA4WfIHTdcWgtKd7+jgXGQqaW2TZH/6gsGW9nzbOwf+wHdR/kMAMXtxV/p76/8EE0HH9K3eeVcObMGU1u23R3Augjlu+YSZBvX5fkY7MDA50DeKWbZ/IAFC5zeL6f9k8sQZ7fXbuLT+/lAi3tz3fwzqm+XiDiCjDuEEgb5ixkaCxOyhnp0ZwJ6XGFDpJ5
  
ZXGLDpJ3/Q9f2aDJLzMsBFzbB2EhSnCoqn+lDkAOxMEOOkjDr1dK62kwqPl4CCqwCGomFMif2Gra6/HaA4j8sU2Zfpe5wRGMVJgb8Z7LNW0Diyj0aBsaz2mSTdFuwLFDEMI0m4rAsUP3jiju/A4aDIfmRPnAuA+OWYjkY5QaO6sz0OimxjcKBdHJvM2T+eg6Ib2c8M9kvixEx5OOY+3jEdFMPn4ZxhKJKNOSj6A0QUFMkEOFejIEhiq7h9mAVwD
  
pfV43eur8MZE8cIQ5EZEeUBQaN64KNMCQnuMBQZmOLKD9A2YokpYaNzxG3vp7Jk643iFii4GZh/PjLocL9zLjdw+Vkfsw18nfhKW99fBOcJLyXhzy9KT0nhxCLqHLQBW9OHDMRkoLCob0ydWO6hfdvHbO8nKE4UFM8MQzFtAxnZONH/eCcCeC6Tclkk37kR22WQNJzpM1BkbSju3Ew3WLdwUHRwZTpGB9NpcXKdv29wfAfFayLO5+pHEDSyAxm1
  
nymyP1NzviMjdCNRf/3fQTlJR4fPWw+K7Eu2S1bjwOgykTAUncP7EI9ro8ta/KUApmhR8I2CIjMOB0V/XY4slWP4maKzIwNxI5uHt5Ox0x7qi6J8AMC7qZMLQrJXB8XwbCEKigz41JtMo1EdGdzflFh+CENxWJ3ytAMoMr1sdI5mbF9E52G6TXy4bJkYJLsH6PQh52F2RpbImrEPm70Dm1A+LvbICOHDwqE2uUwx7IvUAyC+Lx0v0XdRxz5S37t
  
ZEokRPs9aPlB8WYrqU9qC/elHlyiE7dhfUPxAUDwjLxRX0UkI+uGhhu8QVACn5+TLB8b1R9A8UOScTKEBKmLqGhb7YGR/NCwSioCQ9SGyYZf5nBHYYfeQfZI6diMochwyW9rppgZ8h1P5DtBpUCRo8TnaSD/H+QDfu4sqBsUZgftX2eY9yU9IXOa+ZeCv/xXYGD8M+yxxhd9h/6j4w/b0Dxc9/KlyPSgyBaYMx6sX1xzzHMmBmil0M6D4YTOgSP
  
BHQRHgPSsxyjB6Hhc01l+nygpFMg0MB4DIzqhDlMhSXXbpOrQeFN06iVvvrAcuFm/DmSL7Dw6+J7tllCXD5YIOI1lSEPr7JYEi+zONYc3LLfK7bM+Nig7O4SlLVJ3cdKkVmSKL3NiRJZE09nFrpNiZfo7zAfzjiuAcWaHIgE4d3bp0vXpWJVOkDWTzYSi6pSlmOwCHTJClK7eeH2478RVnf2y/p8QUFxa4WUMcFJk+sx9roxvW6VPOt0zIX0oFx
  
XWDQLwswqkPCJyJ9TfI/pjkwyErFN0aj8tC0wRTPSheHdTXXx+NO3bUmiL70jlMnXAEApZg+lmEbZLWOSkUOd5y0jNBfzBqAkgHRS6QUBe3dFHv/ExP2Hc/r95FTZ9ZL86ypsh6NVM9d9WyXntc27NCkSWQpNNnAptp6f2e7dyFFqZvcfVs9fSZejwsvS756/nEJMkGWZpbCz8rh/9ynksD+xF7/B0Hxbm0jQsn+HDS+HD7FQZFOnTxwEgYqp4Y
  
IaA5AMBhtohoSB99R5bEdvSj0D50wBiJVLnemiLZhL+myKIqkH1JYvGcNYtwXbmatkBEnVYK6nJtxDYHIGBGfVh3CR+X79aXyMqYvket0QET12acLm6NI0nHu3U4ri4m2Z+p9JPe+XcJyvFJna6U2CfKZgvpexz3ToklAHdhgvMOCsqfElEPFuABsFuIZ2rL1IzvotYU/azQrUkzwDH6R9XL9QG+6frUrZmylk1fR/kqxyKrYDD2s3rWFFkf5Lu
  
ovrlb3/tXn1mzIlDpS2YCcXXk+4OD4/r+RbbEubgA0giKWS60YG9sEGcH3zbUkf53yykkM+G1bvqa9U3qPEHi6nO43lztbcQKGLGBRJwSr0CP47hZY1Ts0C8MdtiReodjkDbyHevJfp2AIlktWWajNUUXE+E2sabI9Hn6hRaM9ZDEJe2JCfSx9iFwAAPrV1GXuDmpC+rx+v8SoYbQMAIQ4/eLMLyr9OXaxsjlT4W5yuY6jrpE1fkDfe9GJ3csps
  
acj/uSwkbpq+/cAi52iDquazfHoE5RHcvVVwemU+u0KwnkHKjTrEmSMbrzOwfAid3yAMsdUW0jAHBI2kZ25TueyxRPj2gPMKQMGRFtov1usPShyMI5+/kXWnrr71HB9/RnXF/Sv/QJGfg6EkEAeDlenL9yLNrqBigGceoHFJkexkGRLA/7cQ7XR8CQ81Mmro58z3YGTMDjytJ3fB+1Luf22TTYpx444/zlzDo2CNurq1vrH6P4AJH9ePS6AZd
  
V4GsUascLFCv/qzjl8FbaXN4WMTn7CiUb8yC/XLMgPlFh2WA0juoursWORmT+F9lg5uyTnHXWhhtKGy16QQo83qMRVwJzwoaGBUhsE++0isdxG0cZ2+o7YRpNTR34dzn1+nvpdEdDrnOVfaNeZ8ZKFcAQTEcbZggfcuCYBEQZGgY8GY9dQk07t6cGQq/wfJD7IkMGWJgWAJD0RkZlc1sNlOEbYheAiEqPaSSZwtreyV4zz0uT9YkjXQX/OFjo+9
  
WCNk+aKR/1EHprPYgEHs8AZlmAKyD+d1V1kpC8ipc5TfUff/lnygU4ZZBr5Tr45kQWHQ9NN3l0r1YoWsi/qkXVulXgR5I7v52/EnZh7hDNH3q9u0HZixPBblb8RtknOy9h5uN4MasUmbo45Ngka81zs+tzCFyzKgYH93B0Z4O235nRSOCbdfH0HxNGWKW+e9+lwvQHE4RiNGJufISQK6Cvv8Rm1iNIsa7VhKYBujrJ9tVaFdVsdkyxSdbCeWHMi
  
wASNJQie3dYa2hZ99LqLhWwVwYAQs4vjtPObwoG3+rQbUh6zHTcEbZdHtrL+du/N8shl9SmbvbsvZvAPjtq6NWgHF0TIqaxpZbsxtRgdnPQbrXNtLrFswTfDF2gjTZzLBB6VvhByHtrLWwgWkuHXWrPWycgayon2Ai13vSiz/hJcPij53249fNBQHBuDgalLcHL/tRogZCVns5YoZ4OMKqhNriFxc4vtxUjgTBIKs17Cd9pe1fVYv65s4H2AtD/
  
+Nu0Lb0b5TNBR/IMO+IPm3dVTFoMCNdZVtJa66+uIqJFdPo7JAd4sRVza7ai3GBoCOGQBJYrgjxL8oVZW4zV1PH4r/DyD+m59EBJimAAAAAElFTkSuQmCC') as CompanyLogo, Signature from tbl_Email_Setting      
        
          
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetNotificationSettings]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_GetNotificationSettings]
As
Begin
SELECT     NotificationID, EmailSend, EmailCancel, EmailReschedule, EmailWhen, SMSSend, SMSCancel, SMSReschedule, SMSWhen, isactive, CreatedDatetime, 
                      ModifiedDatetime
FROM         tbl_LRS_NotificationSetting where isnull(isactive,1)=0 

End
GO
/****** Object:  StoredProcedure [dbo].[SP_GetEvent_Parties_Speed]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec SP_GetEvent_Parties_Speed 'Chirag'                      
                      
CREATE Proc [dbo].[SP_GetEvent_Parties_Speed]                      
--@SearchFilter Nvarchar(500)                      
As                      
Begin                      
--Drop table #TempA                      
                      
select CCa.caseno,CC.firmcode as firmcode,Upper(CCa.[Type]) as [Type]         
,ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                      
email as Email,CC.phone1 AS Phone,ISNULL(LTRIM(RTRIM(c.first)),'')as FirstName,ISNULL(LTRIM(RTRIM(c.last)),'') as LastName ,                  
 CCa.CardCode           
from [card] C                     
Inner Join casecard CCa On CCa.cardcode = C.cardcode
Inner join card2 CC On CC.firmcode =C.firmcode    
where 
  (c.first is not null or c.first <> '')
  and (c.last is not null or c.last <> '')

Order By C.first,CCa.caseno,C.firmcode asc                   
End
GO
/****** Object:  StoredProcedure [dbo].[SP_GetEvent_Parties]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec SP_GetEvent_Parties 'Bija'                    
CREATE Proc [dbo].[SP_GetEvent_Parties]                                                    
@SearchFilter Nvarchar(500)                                              
As                                                    
Begin                                                    
If(@SearchFilter <> '' or @SearchFilter is not null)                            
Begin                                 
select Convert(Nvarchar(50),CCa.caseno) as [Case No],Convert(Nvarchar(50),C.firmcode) as firmcode,CCa.[Type]                                        
,ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                                                    
email as Email,           
           
-- case when    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
--else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
--(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
--then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
--else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
--else isnull(LTRIM(RTRIM(C.car)),'') end) end as Phone,  

Case when( 
Case When(
case when( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end) end)='' THEN isnull(LTRIM(RTRIM(phone1)),'') else

case when( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end) end

END)='' THEN isnull(LTRIM(RTRIM(phone2)),'') ELSE Case When(
case when( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end) end)='' THEN isnull(LTRIM(RTRIM(phone1)),'') else

case when( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end) end

END
END as Phone,     
                   
ISNULL(LTRIM(RTRIM(c.first)),'')as FirstName,ISNULL(LTRIM(RTRIM(c.last)),'') as LastName ,                 
Convert(Nvarchar(50),CCa.cardcode)  as CardCode                                          
                                             
from [card] C                                                   
Inner Join casecard CCa On CCa.cardcode = C.cardcode                   
Inner join card2 CC On CC.firmcode =C.firmcode                           
where C.First <>''and C.last <>''                        
and                                                           
(                
(ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')    like     LTRIM(RTRIM(@SearchFilter)) + '%')                                                   
OR                                                 
(CCa.caseno like  LTRIM(RTRIM(@SearchFilter)) + '%')                      
OR                                                 
(C.First like  LTRIM(RTRIM(@SearchFilter)) + '%')                       
OR                                                 
(C.Last like  LTRIM(RTRIM(@SearchFilter)) + '%')                                              
             
 OR                   
(ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') like    LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                 
OR                 
(ISNULL(LTRIM(RTRIM(c.last)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'') like   LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                     
OR                 
(ISNULL(LTRIM(RTRIM(C.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') like    LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                
               
OR                 
(ISNULL(LTRIM(RTRIM(C.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                   
              
              
OR                 
(ISNULL(LTRIM(RTRIM(replace(C.salutation,'.',''))),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                       
OR                 
(ISNULL(LTRIM(RTRIM(replace(C.salutation,'.',''))),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                           
                            
                              
)                              
UNION ALL            
            
SELECT RC.[Case],RC.Firmcode,'RS',            
ISNULL(LTRIM(RTRIM(salutation)),'')+' '+ISNULL(LTRIM(RTRIM(FName)),'')+' '+ISNULL(LTRIM(RTRIM(LName)),'') AS [Name],            
Email,PhoneNo,FName,LName,RC.CardCode            
from tbl_RS_Contactlist RC                          
where  isnull(RC.Isactive,0)<>1  and RC.FName <>''and RC.LName <>''                        
and                                                           
(                
(ISNULL(LTRIM(RTRIM(RC.FName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'')    like     LTRIM(RTRIM(@SearchFilter)) + '%')                                                   
OR                                                 
(RC.[Case] like  LTRIM(RTRIM(@SearchFilter)) + '%')                      
OR                                                 
(RC.FName like  LTRIM(RTRIM(@SearchFilter)) + '%')                       
OR                                           
(RC.LName like  LTRIM(RTRIM(@SearchFilter)) + '%')                                              
             
 OR                   
(ISNULL(LTRIM(RTRIM(RC.FName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'') like    LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                 
OR                 
(ISNULL(LTRIM(RTRIM(RC.LName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'') like   LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                     
OR                 
(ISNULL(LTRIM(RTRIM(RC.Salutation)),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'') like    LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                
               
OR                 
(ISNULL(LTRIM(RTRIM(RC.Salutation)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                   
              
              
OR                 
(ISNULL(LTRIM(RTRIM(replace(RC.Salutation,'.',''))),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                       
OR                 
(ISNULL(LTRIM(RTRIM(replace(RC.Salutation,'.',''))),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                           
                            
                              
)                          
                                              
                              
                            
END                                       
End
GO
/****** Object:  StoredProcedure [dbo].[SP_GetEvent_Contact]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec SP_GetEvent_Contact ''                                                 
CREATE Proc [dbo].[SP_GetEvent_Contact]                                                 
@SearchFilter Nvarchar(500) = NUll                                                 
                                                 
As                                                        
                                                        
begin                                                  
                                              
select Convert(Nvarchar(50),CCa.caseno) as [Case No],Convert(Nvarchar(50),C.firmcode) as firmcode,CCa.[Type] as [Type],                                           
ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                                                        
email as Email,                          
--case when    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
--else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
--(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
--then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
--else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
--else isnull(LTRIM(RTRIM(C.car)),'') end) end as Phone,

Case when( 
Case When(
case when( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end) end)='' THEN isnull(LTRIM(RTRIM(phone1)),'') else

case when( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end) end

END)='' THEN isnull(LTRIM(RTRIM(phone2)),'') ELSE Case When(
case when( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end) end)='' THEN isnull(LTRIM(RTRIM(phone1)),'') else

case when( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then       
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''      
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)      
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')       
else isnull(LTRIM(RTRIM(C.car)),'') end) end

END
END as Phone,      
              
ISNULL(LTRIM(RTRIM(c.first)),'')as FirstName,ISNULL(LTRIM(RTRIM(c.last)),'') as LastName, C.salutation,                                           
ISNULL(LTRIM(RTRIM(c.middle)),'')  as MiddleName ,birth_date as DOB,C.comments as Note,                           
CC.Zip as Zipcode,CC.CountryCode,MobileType,EmailType,Convert(Nvarchar(50),CCa.cardcode) AS cardcode  ,                               
ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name_WITHOUT]                         
                                                 
from [card] C                                                       
Inner Join casecard CCa On CCa.cardcode = C.cardcode                             
Inner join card2 CC On CC.firmcode =C.firmcode                  
                  
where                                                         
(                           
                        
(CCa.caseno like  LTRIM(RTRIM(@SearchFilter)) + '%')                                                          
OR                                  
(C.first like  LTRIM(RTRIM(@SearchFilter)) + '%')                                                          
OR                                  
(C.last like  LTRIM(RTRIM(@SearchFilter)) + '%')                                                          
OR                                                     
(C.Email like  LTRIM(RTRIM(@SearchFilter)) + '%')                        
                        
                        
 OR                             
(ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') like    LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                           
OR                           
(ISNULL(LTRIM(RTRIM(c.last)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'') like   LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                               
OR                           
(ISNULL(LTRIM(RTRIM(C.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') like    LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                          
                         
OR                           
(ISNULL(LTRIM(RTRIM(C.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                             
                        
                        
OR                           
(ISNULL(LTRIM(RTRIM(replace(C.salutation,'.',''))),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                                 
OR                    
(ISNULL(LTRIM(RTRIM(replace(C.salutation,'.',''))),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                    
                        
                                                     
OR                                                 
(CC.phone1 like  LTRIM(RTRIM(@SearchFilter)) + '%')                        
)                                                     
                  
UNION ALL                  
                  
SELECT RC.[Case],RC.Firmcode,'RS',                  
ISNULL(LTRIM(RTRIM(salutation)),'')+' '+ISNULL(LTRIM(RTRIM(FName)),'')+' '+ISNULL(LTRIM(RTRIM(LName)),'') AS [Name],                  
Email,PhoneNo,FName,LName,Salutation,MName,DOB,Notes,Zipcode,CountryCode,MobileType,EmailType,RC.CardCode,                  
ISNULL(LTRIM(RTRIM(FName)),'')+' '+ISNULL(LTRIM(RTRIM(LName)),'') AS [Name_WITHOUT]                   
from tbl_RS_Contactlist RC                   
                    
 where                                   
isnull(RC.Isactive,0)<>1   and                      
                        
(                                
(RC.FName like  LTRIM(RTRIM(@SearchFilter)) + '%')                                         
OR                                  
(RC.LName like  LTRIM(RTRIM(@SearchFilter)) + '%')                                                          
OR                                                     
(RC.Email like  LTRIM(RTRIM(@SearchFilter)) + '%')                        
                        
                        
 OR                             
(ISNULL(LTRIM(RTRIM(RC.FName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'') like    LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                           
OR                           
(ISNULL(LTRIM(RTRIM(RC.LName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'') like   LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                               
OR                           
(ISNULL(LTRIM(RTRIM(RC.Salutation)),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'') like    LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                          
                         
OR                           
(ISNULL(LTRIM(RTRIM(RC.Salutation)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                             
                        
                        
OR                           
(ISNULL(LTRIM(RTRIM(replace(RC.Salutation,'.',''))),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                                 
OR                           
(ISNULL(LTRIM(RTRIM(replace(RC.Salutation,'.',''))),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'') like     LTRIM(RTRIM(REPLACE(@SearchFilter,',',''))) + '%')                                 
                        
                                                     
OR                                                 
(RC.PhoneNo like  LTRIM(RTRIM(@SearchFilter)) + '%')                        
)                                           
                                           
                           
                             
                         
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetEmailSMSDetails]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_GetEmailSMSDetails]  
As  
Begin  
SELECT     EmailID, EmailFrom, EmailPassword, EmailFromTitle, EmailPort, EmailSMTP, Twilio_CALL, Twilio_SMS, Twilio_AccountSid, Twilio_AuthToken  
FROM         tbl_Email_Setting  


SELECT TOP 1 LogAccessDateTime FROM tbl_Twilio_LogAccess ORDER BY 1 DESC

End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetColorcode_Reminder]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Sp_GetColorcode_Reminder 'Log'

CREATE Proc [dbo].[Sp_GetColorcode_Reminder]
@Flag nvarchar(100)
as
Begin

if(@Flag='Reminder')
Begin


	SELECT     ColorID, Status, R, G, B, StatusID FROM lst_Appointment_Responce_Color
	order by StatusID asc

End

if(@Flag='Log')
Begin


	SELECT     ColorID, Status, R, G, B, StatusID FROM lst_Log_Responce_Color
	order by StatusID asc

End



End




--update lst_Appointment_Responce_Color set R=57,G=224,B=16 where ColorID=1
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllEventForNested]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_GetAllEventForNested]  
AS  
BEGIN  
  
select caseno AS [Case No],date AS [EventStartDate],event AS [Title] from cal1  
WHERE event IS NOT NULL AND event <> ''  
ORDER BY caseno ASC
  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllEvent]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_GetAllEvent]                  
AS                  
BEGIN                  
                  
select Ca.eventno as EventNo,date AS [EventStartDate] ,dateadd(MINUTE, 1, date) as EventEndDate,           
        
isnull(CA.LastUpdatedStatusFK,0) as IVRStatus ,          
              
(replace(replace(replace(ca.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')) ,'{EventBody}',ca.[event])  ,'{EventDatetime}',Case When ((ca.location like '%birth%') or (

ca.location like '%b''day%')) then                         
(CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, birth_date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)                        
else                        
CONVERT(VARCHAR, LRT.ReminderDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh,LRT.ReminderDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LRT.ReminderDatetime)), 2)                               
End)) AS [Title],        
        
255  AS A,        
Case When CA.LastUpdatedStatusFK =1 then '57' When CA.LastUpdatedStatusFK =2 then '250' When CA.LastUpdatedStatusFK =3 then '247' When CA.LastUpdatedStatusFK =4 then '106' Else '255' END  AS R,        
Case When CA.LastUpdatedStatusFK =1 then '224' When CA.LastUpdatedStatusFK =2 then '50' When CA.LastUpdatedStatusFK =3 then '255' When CA.LastUpdatedStatusFK =4 then '111' Else '255' END  AS G,        
Case When CA.LastUpdatedStatusFK =1 then '16' When CA.LastUpdatedStatusFK =2 then '77' When CA.LastUpdatedStatusFK =3 then '3' When CA.LastUpdatedStatusFK =4 then '252' Else '255' END  AS B   ,                  
GETDATE() as dateStart,Getdate()+7 as dateEnd       
from tbl_LRS_Reminder AS ca             
INNER JOIN [card] c ON c.cardcode=ca.cardcodeFK     
inner join tbl_LRS_Reminder_Timing LRT on LRT.EventFK =ca.eventno                                   
Inner JOIN card2 c1 ON c1.firmcode =c.firmcode                  
WHERE event IS NOT NULL AND event <> ''        
        
        
UNION ALL

select Ca.eventno as EventNo,date AS [EventStartDate] ,dateadd(MINUTE, 1, date) as EventEndDate,           
        
isnull(CA.LastUpdatedStatusFK,0) as IVRStatus ,          
              
(replace(replace(replace(ca.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c1.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c1.FName)),'')+' '+ISNULL(LTRIM(RTRIM(c1.LName)),'')) ,'{EventBody}',ca.[event])  ,'{EventDatetime}',Case When ((ca.location like '%birth%') or (

ca.location like '%b''day%')) then                         
(CONVERT(VARCHAR(5), c1.DOB, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, c1.DOB)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, c1.DOB)), 2)                        
else                        
CONVERT(VARCHAR, LRT.ReminderDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh,LRT.ReminderDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LRT.ReminderDatetime)), 2)                               
End)) AS [Title],        
        
255  AS A,        
Case When CA.LastUpdatedStatusFK =1 then '57' When CA.LastUpdatedStatusFK =2 then '250' When CA.LastUpdatedStatusFK =3 then '247' When CA.LastUpdatedStatusFK =4 then '106' Else '255' END  AS R,        
Case When CA.LastUpdatedStatusFK =1 then '224' When CA.LastUpdatedStatusFK =2 then '50' When CA.LastUpdatedStatusFK =3 then '255' When CA.LastUpdatedStatusFK =4 then '111' Else '255' END  AS G,        
Case When CA.LastUpdatedStatusFK =1 then '16' When CA.LastUpdatedStatusFK =2 then '77' When CA.LastUpdatedStatusFK =3 then '3' When CA.LastUpdatedStatusFK =4 then '252' Else '255' END  AS B   ,                  
GETDATE() as dateStart,Getdate()+7 as dateEnd       
from tbl_LRS_Reminder ca             
Inner JOIN tbl_RS_Contactlist c1 ON c1.CardCode =ca.CardcodeFK       
inner join tbl_LRS_Reminder_Timing LRT on LRT.EventFK =ca.eventno                                   
               
WHERE event IS NOT NULL AND event <> ''        
                                 
                  
                
                  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAdded_Parties]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec SP_GetAdded_Parties 'Get','','62C25F068041'            
CREATE Proc [dbo].[SP_GetAdded_Parties]                    
@Flag Nvarchar(100),                    
@FrimCode Nvarchar(50) =null   ,  
@MACID Nvarchar(100)  =null                         
As                    
Begin                    
                    
If(@Flag ='Get')                    
Begin                    
                        
                    
Select Convert(Nvarchar(50),C.CardCode) as CardCode,          
ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],C.Email,             
case when    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')           
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then           
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''          
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)          
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')           
else isnull(LTRIM(RTRIM(C.car)),'') end) end as Phone         
from  [card] C                    
Inner join card2 CC On CC.firmcode =C.firmcode    and                   
C.first in (select Ap.Name from AddParties Ap where Ap.MACID=@MACID) and C.last in (select Ap.LName from AddParties Ap  
where Ap.MACID=@MACID)                        
where                     
Convert(Nvarchar(50),C.cardcode) In (select Ap.firmcode from AddParties Ap where Ap.MACID=@MACID)  and                   
C.first in (select Ap.Name from AddParties Ap where Ap.MACID=@MACID) and C.last in (select Ap.LName from AddParties Ap where Ap.MACID=@MACID)  
                  
UNION ALL          
          
Select Convert(Nvarchar(50),RC.CardCode) as CardCode,          
ISNULL(LTRIM(RTRIM( RC.Salutation)),'')+' '+ISNULL(LTRIM(RTRIM(RC.FName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.MName)),'')+' '+ISNULL(LTRIM(RTRIM(RC.LName)),'') AS [Name],RC.Email,             
RC.PhoneNo AS Phone          
from  tbl_RS_Contactlist RC                 
                  
where                     
Convert(Nvarchar(50),RC.CardCode) In (select Ap.firmcode from AddParties Ap where Ap.MACID=@MACID)  and                   
RC.FName in (select Ap.Name from AddParties Ap where Ap.MACID=@MACID) and RC.LName in (select Ap.LName from AddParties Ap where Ap.MACID=@MACID)                  
                              
                    
                    
End                    
If(@Flag ='Remove')                    
Begin                    
                    
 Delete from AddParties where firmcode = Convert(Nvarchar(50),@FrimCode) and  MACID=@MACID    
                
 select 1 As Result                    
                    
End                    
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetA1LawDetailsCalendar]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sp_GetA1LawDetailsCalendar '1'                
                      
CREATE PROC [dbo].[Sp_GetA1LawDetailsCalendar]                                           
@EventNo NVARCHAR(50)= null                    
                      
AS                                            
BEGIN             
        
Declare @TimeZoneFormat Int               
set @TimeZoneFormat =(select isnull(TimeZone,12) as TimeHours from  tbl_Email_Setting)                                     
                            
                            
SELECT   ca.eventno, ca.CardcodeFK,(CONVERT(VARCHAR(5), ca.date, 101)+'/'+CAST(YEAR(  ca.date) AS VARCHAR(4)))as [Appt Date],                      
Case When @TimeZoneFormat =12 then CONVERT(varchar(15),  CAST(ca.date AS TIME), 100) Else        
isnull( CONVERT(VARCHAR, DATEPART(hh,ca.date)),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,ca.date)), 2),00)        
End as [Appt Time]          
,ca.event as [Event Details], ca.first as [First Name],  ca.last as [Last Name], ca.location as Location,              
 ca.SMS as SMS_Status, ca.CALL  as CALL_Status, ca.EMAIL as Email_Status,                      
C.email as Email,(CONVERT(VARCHAR(5), c.birth_date, 101)+'/'+CAST(YEAR(c.birth_date) AS VARCHAR(4)))as [Birth Date],                      
Case When @TimeZoneFormat =12 then SUBSTRING(CONVERT(VARCHAR, ca.date, 100), 13, 2) ELSE        
isnull( CONVERT(VARCHAR, DATEPART(hh,  ca.date)),00) end  as [Hours],          
                                         
isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  ca.date)), 2),00)  as [Minute],              
EventParties,C1.phone1 as Phone ,Ca.caseno,ca.whenSMS,Ca.whenCALL,Ca.whenEMAIL ,isnull(CA.LastUpdatedStatusFK,0) as IVRStatus,                
ca.date as  BindDate,Ca.EventHTML            
,RIGHT(Convert(VARCHAR(20), ca.date,100),2) as [AMPM],Ca.IsPrivate,Ca.IsConfirmable   ,lt.Language as TwilioLang  
               
            
                  
                     
FROM  tbl_LRS_Reminder AS ca                                           
INNER JOIN [card] c ON c.cardcode=ca.cardcodeFK                                           
Inner JOIN card2 c1 ON c1.firmcode =c.firmcode   
left Join lst_TwilioLanguage lt on lt.LID=    CA.TwilioLang                   
              
where ca.eventno=@EventNo              
                                  
                      
                                  
              
                       
 END
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetA1LawBasicDetails_Filter]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sp_GetA1LawBasicDetails_Filter 'Today','','','','62C25F068041','0'                                         
                                                                  
CREATE PROC [dbo].[Sp_GetA1LawBasicDetails_Filter]                                                                                       
@Flag NVARCHAR(50)= null ,                                                                                            
@Fromdt DATETIME= null ,                                                                                            
@Todt DATETIME= null,                                                                        
@Filter Nvarchar(50) =null ,
@UserFK Nvarchar(500) =null,
@IsPrivate int = null
                                                                  
AS                                                                                        
BEGIN                                                                                      

Declare @MainEventIdTable table
    (    
		MainEventId int
    )
                           
Declare @TimeZoneFormat Int,@FilterScript nvarchar(MAX) ,@PrivateFlag Nvarchar(100),@MACID Nvarchar(100)
                                                  
set @TimeZoneFormat =(select isnull(TimeZone,12) as TimeHours from  tbl_Email_Setting)
                                              
if Exists(select SuparAdminMAC from tbl_RS_Suparadmin where SuparAdminMAC=@UserFK)
BEGIN                                              
    
 set @UserFK = NULL 
 set @PrivateFlag=NULL
 
 SELECT   @UserFK = COALESCE(isnull(Convert(Varchar,@UserFK),0) + ',', '') + Convert(Varchar,RID) FROM tbl_RS_Registration
 SET @PrivateFlag = '0,1'
                                     
END
ELSE
BEGIN
 set @UserFK =(select RID from tbl_RS_Registration Where MacID =@UserFK)
 set @PrivateFlag = '0'
END

Insert into @MainEventIdTable (MainEventId) (
Select Distinct 
	tbl_FinalReminderDetails.MainEventID
		from tbl_FinalReminderDetails 
			Inner Join
				tbl_LRS_Reminder on tbl_FinalReminderDetails.MainEventID = tbl_LRS_Reminder.eventno
			Where        
				CONVERT(date, dbo.tbl_FinalReminderDetails.EventDatetime) >  CONVERT(date,GETDATE()) And        
				dbo.tbl_FinalReminderDetails.IsStatus=0		
				)

SELECT   ca.eventno, Convert(NVARCHAR(50), ca.CardcodeFK) as CardcodeFK ,
(CONVERT(VARCHAR(5), ca.date, 101) + '/' + CAST(YEAR(  ca.date) AS VARCHAR(4)))as [Appt Date],
Case When @TimeZoneFormat =12 then
CONVERT(varchar(15),  CAST(ca.date AS TIME), 100)
Else
Isnull( CONVERT(VARCHAR, DATEPART(hh,ca.date)),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,ca.date)), 2),00)
End  as [Appt Time],
 ca.event as [Event Details],
 LTRIM(RTRIM(ca.first)) as [First Name],
 LTRIM(RTRIM(ca.last)) as [Last Name],
 ca.location as Location,
 ca.SMS as SMS_Status,
 ca.CALL  as CALL_Status,
 ca.EMAIL as Email_Status,
                
 Case When Convert(NVARCHAR(50), ca.CardcodeFK) NOT like '%RS%' then C.email else RSC.Email end as Email,                
 Case When Convert(NVARCHAR(50), ca.CardcodeFK) NOT like '%RS%' then (CONVERT(VARCHAR(5), c.birth_date, 101)+'/'+CAST(YEAR(c.birth_date) AS VARCHAR(4))) else RSC.DOB end as [Birth Date],                                                                  
                   
 Case When @TimeZoneFormat =12 then SUBSTRING(CONVERT(VARCHAR, ca.date, 100), 13, 2) ELSE                                                  
 isnull( CONVERT(VARCHAR, DATEPART(hh,  ca.date)),00) end  as [Hours],                                 
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  ca.date)), 2),00)  as [Minute],EventParties,                       
                                        
 case When isnull(C.Home,'')='' THEN LTRIM(RTRIM(isnull(C1.CountryCode,'')))+LTRIM(RTRIM(isnull(C1.phone1,'')))                                         
 else LTRIM(RTRIM(isnull(C1.CountryCode,'')))+LTRIM(RTRIM(isnull(C.Home,''))) End  AS Phone,                                         
    
 Ca.caseno,ca.whenSMS,Ca.whenCALL,Ca.whenEMAIL,
 isnull(CA.LastUpdatedStatusFK,0) as IVRStatus,ca.date as  BindDate,
 LA.Status as [ReminderStatus],LA.R,LA.G,LA.B,                                                        
 ca.EventHTML as [Event HTML Details],
 RIGHT(Convert(VARCHAR(20), ca.date,100),2) as [AMPM]  ,ca.IsPrivate,Ca.CreatedUserFK ,Ca.IsConfirmable,
 case When isnull(RS.CompanyName,'')='' then Convert(NVARCHAR(50),
 ca.CreatedUserFK) ELSE RS.CompanyName end as [Created by],            
 ca.AppointmentType as [Appointment Type],
 lt.Language as TwilioLang,
 case When ca.LastUpdatedStatusFK is not Null and ca.LastUpdatedStatusFK <> 0 and ca.LastUpdatedStatusFK <> 5 THEN ModifiedDate                  
 ELSE CASE WHEN CONVERT(DATE, ModifiedDate) = '1900-01-01' -- to account for accidental time                  
  THEN ''                  

END  END   as [Confirm Datetime],
case when ca.eventno in (Select MainEventId from @MainEventIdTable) THEN 0 Else 1 END as Isstatus

into #temp
FROM  tbl_LRS_Reminder ca
inner join lst_Appointment_Responce_Color LA on LA.StatusID=isnull(CA.LastUpdatedStatusFK,0)                                        
LEFT join tbl_RS_Registration RS On Convert(NVARCHAR(50),RS.RID) =Convert(NVARCHAR(50),ca.CreatedUserFK)                                
left Join lst_TwilioLanguage lt on lt.LID=    CA.TwilioLang                    
LEFT outer JOIN [card] c ON Convert(NVARCHAR(50),c.cardcode)=Convert(NVARCHAR(50),ca.cardcodeFK)                                                                                       
LEFT outer JOIN card2 c1 ON Convert(NVARCHAR(50),c1.firmcode) =Convert(NVARCHAR(50),c.firmcode)                             
LEFT JOin tbl_RS_Contactlist RSC On RSC.CardCode =Convert(NVARCHAR(50),Ca.CardcodeFK)                                         
                 
WHERE                                                
(Convert(nvarchar(100),IsPrivate) in (select * from dbo.SplitByComma(@PrivateFlag)) OR                                               
Convert(nvarchar(100),CreatedUserFK) in (select * from dbo.SplitByComma(@UserFK))                                            
OR(C.email in (select RSR.Email  from tbl_RS_Registration RSR ))      
)
and  ('' = ISNULL(@IsPrivate,'') or(convert(varchar,Ca.IsPrivate) = LTRIM(RTRIM(@IsPrivate))) or @IsPrivate='')                  
IF(@Flag='Upcoming')                  
BEGIN

 Select ROW_NUMBER() over(order by (select 1)) as [Sr.No],* from #temp c
 WHERE --ca.type='CLIENT'  AND 
 c.[First Name] <> '' and c.[Last Name] <>'' and
 CONVERT(DATE,c.[Appt Date])  BETWEEN CONVERT(date,GETDATE())  and CONVERT(date,GETDATE()+7)
 and
 ((c.[First Name] like '%' + LTRIM(RTRIM(@Filter)) + '%') or                                                                
 (c.[Last Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                                  
 or                                                                                    
 (C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                  
 or                                                                    
 (C.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                                    
 or                                                             
 (c.[Created by] like '%' + LTRIM(RTRIM(@Filter)) + '%')
 or                                  
 (c.Phone like '%' + LTRIM(RTRIM(@Filter)) + '%'))
           
 END

ELSE IF(@Flag='Today')
BEGIN
 select ROW_NUMBER() over(order by (select 1)) as [Sr.No],* from #temp c
 WHERE --ca.type='CLIENT'  AND
 c.[First Name] <> '' and c.[Last Name] <>'' and                                                                       
 CONVERT(DATE,c.[Appt Date]) = CONVERT(date,GETDATE()) and
 ((c.[First Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')
 or                             
 (c.[Last Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')
 or                                
 (C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')
 or                                                                    
 (C.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                                 
  or                                                             
 (c.[Created by] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                
 or                                    
 (c.Phone like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                                                  

END

ELSE IF(@Flag='Tomorrow')                                 
BEGIN                                                          
select ROW_NUMBER() over(order by (select 1)) as [Sr.No],* from #temp c                                                                            
 WHERE --ca.type='CLIENT' AND                                                      
 c.[First Name] <> '' and c.[Last Name] <>'' and                                                                       
  CONVERT(DATE,c.[Appt Date]) = CONVERT(date,GETDATE()+1) and                                                                
 ((c.[First Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                  
  or                                                                                   
 (c.[Last Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                                  
 or                                                                    
 (C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                   
 or                                                                    
 (C.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                                 
  or                                                      
 (c.[Created by] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                           
 or                                                                             
 (c.Phone like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                                                    
END                                                                  
ELSE IF(@Flag='Yesterday')                                                                                               
BEGIN                                                                     
select ROW_NUMBER() over(order by (select 1)) as [Sr.No],* from #temp c                                                                
 WHERE --ca.type='CLIENT'  AND                                                                               
 c.[First Name] <> '' and c.[Last Name] <>'' and                                                                       
  CONVERT(DATE,c.[Appt Date]) = CONVERT(date,GETDATE()-1) and                                                                        
 ((c.[First Name] like '%' + LTRIM(RTRIM(@Filter)) + '%') or                                                                                   
 (c.[Last Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                     
 or                                                                    
 (C.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')
 or                                                                                    
 (C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')
  or                                                             
 (c.[Created by] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                     
 or                                                                                 
 (c.Phone like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                                                  
END                                                                  
ELSE IF(@Flag='Past Week')                                                                                               
BEGIN                                           
select ROW_NUMBER() over(order by (select 1)) as [Sr.No],* from #temp c                                                                                         
 WHERE --ca.type='CLIENT'  AND                                                                         
 c.[First Name] <> '' and c.[Last Name] <>'' and                                                                       
  CONVERT(DATE,c.[Appt Date])  BETWEEN CONVERT(date,GETDATE()-7) AND CONVERT(date,GETDATE()) and                                                           
 ((c.[First Name] like '%' + LTRIM(RTRIM(@Filter)) + '%') or                                                                                   
 (c.[Last Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                  
 or                                                      
 (C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                   
 or                                                                    
 (C.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                                     
  or                                   
 (c.[Created by] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                            
 or                                              
 (c.Phone like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                                                  
END                                                                  
ELSE IF(@Flag='Specific Date')                                                                                             
BEGIN                                                           
select ROW_NUMBER() over(order by (select 1)) as [Sr.No],* from #temp c                                                         
 WHERE --ca.type='CLIENT'  AND                                                                               
 c.[First Name] <> '' and c.[Last Name] <>'' and                                                                       
  CONVERT(DATE,c.[Appt Date]) = CONVERT(DATE,@Fromdt)         and                                                       
 ((c.[First Name] like '%' + LTRIM(RTRIM(@Filter)) + '%') or                                                                                   
 (c.[Last Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                               
 or                                                                                    
 (C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')                                     
 or                                                                    
 (C.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                                
  or                                                             
 (c.[Created by] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                 
 or                                                                                 
 (c.Phone like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                                                     
END                                                                  
ELSE IF(@Flag='Custom Range')                                                                                               
BEGIN                                                                 
select ROW_NUMBER() over(order by (select 1)) as [Sr.No],* from #temp c                                                                                         
 WHERE --ca.type='CLIENT'  AND                                         
 c.[First Name] <> '' and c.[Last Name] <>'' and                                                                       
  CONVERT(DATE,c.[Appt Date]) BETWEEN CONVERT(date,@Fromdt)AND CONVERT(date,@Todt)        and                                                                 
 ((c.[First Name] like '%' + LTRIM(RTRIM(@Filter)) + '%') or                                                                            
 (c.[Last Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                                  
 or                                                                                    
 (C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')                                                                   
 or                                                                    
 (C.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                                  
  or                                                             
 (c.[Created by] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                               
 or                                                                                 
 (c.Phone like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                                                  
END                                                                  
                                                               
 END
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetA1LawBasicDetails]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sp_GetA1LawBasicDetails 'Upcoming','','',''
CREATE PROC [dbo].[Sp_GetA1LawBasicDetails]                           
@Flag NVARCHAR(50)= null ,                          
@Fromdt DATETIME= null ,                          
@Todt DATETIME= null,      
@Filter Nvarchar(50) =null     
     
                            
AS                            
BEGIN                          
                  
IF(@Flag='Upcoming')                           
BEGIN                          
    
 SELECT ROW_NUMBER() over(order by (select 1)) as No,c2.PartiesFirmcode as    PartiesFirmcode                   
into #TempDateAssignmentWithRow
FROM casecard ca                   
Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno   
inner join [card] c ON c.cardcode=ca.cardcode -- and C.firmcode in (c2.PartiesFirmcode)
LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode                           
WHERE --ca.type='CLIENT'  AND                   
c.first <> '' and c.last <>'' and                      
CONVERT(DATE,c2.date)  BETWEEN CONVERT(date,GETDATE())  and CONVERT(date,GETDATE()+7)     


Declare @Start int,@End int,@FirmCode Nvarchar(50)  
  
set @Start =(select Min(No) from #TempDateAssignmentWithRow)  
set @End =(select Max(No) from #TempDateAssignmentWithRow) 

Drop table tbl_TempFirmCode

Create Table tbl_TempFirmCode (FirmCode Nvarchar(50))

While(@End >= @Start)  
Begin

	set @FirmCode = (select PartiesFirmcode from #TempDateAssignmentWithRow where No=@Start) 

	Insert Into tbl_TempFirmCode  
	SELECT * FROM dbo.SplitByComma(@FirmCode)

	set @Start =@Start+1  

END      
--drop table #Temp

select FirmCode into #Temp from tbl_TempFirmCode 
Group  By FirmCode                      
                            
 SELECT ca.caseno AS [Case No],                
 (CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR( date) AS VARCHAR(4)))+' '+isnull( CONVERT(VARCHAR, DATEPART(hh,  date)),00)+':'+           
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2),00) as [Appt time]          
 ,           
 event AS [Title],                          
 ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                               
 c1.phone1  as Phone,c1.phone2,c.email AS Email ,Enddttm as [Event Enddate],location as   [Event Template]                   
 ,(CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR(date) AS VARCHAR(4)))  as E_Date,CONVERT(VARCHAR, DATEPART(hh,  date))  as E_Hours,RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2)  as E_minute                                
 ,isnull((CONVERT(VARCHAR(5), Enddttm, 101)+'/'+CAST(YEAR(Enddttm) AS VARCHAR(4))), (CONVERT(VARCHAR(5), getdate(), 101)+'/'+CAST(YEAR( getdate()) AS VARCHAR(4)))) as END_Date,                    
isnull( CONVERT(VARCHAR, DATEPART(hh,  Enddttm)),00)  as END_Hours,                    
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  Enddttm)), 2),00)  as END_minute                       
 ,eventno  , C.firmcode                     
 ,c2.first,c2.last ,c2.SMS,c2.CALL,C2.EMAIL ,c2.PartiesFirmcode as    PartiesFirmcode                   
 FROM casecard ca                            
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno   
 inner join [card] c ON c.cardcode=ca.cardcode or C.firmcode in (c2.PartiesFirmcode)
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode                                     
 WHERE --ca.type='CLIENT'  AND                   
 c.first <> '' and c.last <>'' and                      
 CONVERT(DATE,c2.date)  BETWEEN CONVERT(date,GETDATE())  and CONVERT(date,GETDATE()+7)      
	and      
	((c.[first] like '%' + LTRIM(RTRIM(@Filter)) + '%')                        
	or                 
	(c.[last] like '%' + LTRIM(RTRIM(@Filter)) + '%')                
	or                  
	(C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')               
	or                
	(C2.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
	or               
	(C1.phone1 like '%' + LTRIM(RTRIM(@Filter)) + '%'))     
	
	and  c.firmcode in (select Firmcode from #Temp)                           
	ORDER BY c2.date ASC                            
                            
END                          
ELSE IF(@Flag='Today')                           
BEGIN                          
                            
                            
 SELECT ca.caseno AS [Case No],           
  (CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR( date) AS VARCHAR(4)))+' '+isnull( CONVERT(VARCHAR, DATEPART(hh,  date)),00)+':'+           
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2),00) as [Appt time]          
            
  ,event AS [Title],                          
 ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                               
 c1.phone1 as Phone,c1.phone2,c.email AS Email ,Enddttm as [Event Enddate],location as   [Event Template]                             
 ,(CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR(date) AS VARCHAR(4)))  as E_Date,CONVERT(VARCHAR, DATEPART(hh,  date))  as E_Hours,RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2)  as E_minute    
 ,isnull((CONVERT(VARCHAR(5), Enddttm, 101)+'/'+CAST(YEAR(Enddttm) AS VARCHAR(4))),          
  (CONVERT(VARCHAR(5), getdate(), 101)+'/'+CAST(YEAR( getdate()) AS VARCHAR(4)))) as END_Date,                    
isnull( CONVERT(VARCHAR, DATEPART(hh,  Enddttm)),00)  as END_Hours,                    
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  Enddttm)), 2),00)  as END_minute                       
 ,eventno  , C.firmcode                     
 ,c2.first,c2.last,c2.SMS,c2.CALL,C2.EMAIL ,c2.PartiesFirmcode as    PartiesFirmcode                         
 FROM casecard ca                         
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno   
 inner join [card] c ON c.cardcode=ca.cardcode and C.firmcode in (c2.PartiesFirmcode)
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode                                     
 WHERE --ca.type='CLIENT'  AND                  
 c.first <> '' and c.last <>'' and                       
 CONVERT(DATE,c2.date) = CONVERT(date,GETDATE())       
 and      
 ((c.[first] like '%' + LTRIM(RTRIM(@Filter)) + '%')                        
or                 
(c.[last] like '%' + LTRIM(RTRIM(@Filter)) + '%')                
or                  
(C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')               
or                
(C2.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
or               
(C1.phone1 like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                
 ORDER BY c2.date ASC                         
                           
                     
END                          
ELSE IF(@Flag='Tomorrow')                           
BEGIN                          
                            
                            
 SELECT ca.caseno AS [Case No],            
  (CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR( date) AS VARCHAR(4)))+' '+isnull( CONVERT(VARCHAR, DATEPART(hh,  date)),00)+':'+           
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2),00) as [Appt time]          
 ,event AS [Title],                          
 ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                               
 c1.phone1 as Phone,c1.phone2,c.email AS Email ,Enddttm as [Event Enddate],location as [Event Template]                        
 ,(CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR(date) AS VARCHAR(4)))  as E_Date,CONVERT(VARCHAR, DATEPART(hh,  date))  as E_Hours,RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2)  as E_minute                                
 ,isnull((CONVERT(VARCHAR(5), Enddttm, 101)+'/'+CAST(YEAR(Enddttm) AS VARCHAR(4))), (CONVERT(VARCHAR(5), getdate(), 101)+'/'+CAST(YEAR( getdate()) AS VARCHAR(4)))) as END_Date,                    
isnull( CONVERT(VARCHAR, DATEPART(hh,  Enddttm)),00)  as END_Hours,                    
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  Enddttm)), 2),00)  as END_minute                       
 ,eventno  , C.firmcode                     
 ,c2.first,c2.last  ,c2.SMS,c2.CALL,C2.EMAIL    ,c2.PartiesFirmcode as    PartiesFirmcode                                       
 FROM casecard ca                            
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno   
 inner join [card] c ON c.cardcode=ca.cardcode and C.firmcode in (c2.PartiesFirmcode)
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode                                        
 WHERE --ca.type='CLIENT'  AND                    
 c.first <> '' and c.last <>'' and                     
 CONVERT(DATE,c2.date) = CONVERT(date,GETDATE()+1)        
 and      
 ((c.[first] like '%' + LTRIM(RTRIM(@Filter)) + '%')                        
or                 
(c.[last] like '%' + LTRIM(RTRIM(@Filter)) + '%')                
or                  
(C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')               
or                
(C2.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
or               
(C1.phone1 like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                
 ORDER BY c2.date ASC        
                   
                            
END             
ELSE IF(@Flag='Yesterday')                           
BEGIN                          
                            
                            
 SELECT ca.caseno AS [Case No],           
  (CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR( date) AS VARCHAR(4)))+' '+isnull( CONVERT(VARCHAR, DATEPART(hh,  date)),00)+':'+           
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2),00) as [Appt time]          
 ,event AS [Title],                          
 ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                               
 c1.phone1 as Phone,c1.phone2,c.email AS Email ,Enddttm as [Event Enddate],location as  [Event Template]                           
 ,(CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR(date) AS VARCHAR(4)))  as E_Date,CONVERT(VARCHAR, DATEPART(hh,  date))  as E_Hours,RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2)  as E_minute                                
 ,isnull((CONVERT(VARCHAR(5), Enddttm, 101)+'/'+CAST(YEAR(Enddttm) AS VARCHAR(4))), (CONVERT(VARCHAR(5), getdate(), 101)+'/'+CAST(YEAR( getdate()) AS VARCHAR(4)))) as END_Date,                    
isnull( CONVERT(VARCHAR, DATEPART(hh,  Enddttm)),00)  as END_Hours,                    
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  Enddttm)), 2),00)  as END_minute                       
 ,eventno  , C.firmcode                     
 ,c2.first,c2.last,c2.SMS,c2.CALL,C2.EMAIL ,c2.PartiesFirmcode as    PartiesFirmcode                                           
 FROM casecard ca                            
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno   
 inner join [card] c ON c.cardcode=ca.cardcode and C.firmcode in (c2.PartiesFirmcode)
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode                                              
 WHERE --ca.type='CLIENT'  AND                    
 c.first <> '' and c.last <>'' and                     
 CONVERT(DATE,c2.date) = CONVERT(date,GETDATE()-1)        
 and      
 ((c.[first] like '%' + LTRIM(RTRIM(@Filter)) + '%')                        
or                 
(c.[last] like '%' + LTRIM(RTRIM(@Filter)) + '%')                
or                  
(C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')               
or                
(C2.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
or               
(C1.phone1 like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                
 ORDER BY c2.date ASC             
                            
END                          
ELSE IF(@Flag='Past Week')                           
BEGIN                          
                            
 SELECT ca.caseno AS [Case No],            
  (CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR( date) AS VARCHAR(4)))+' '+isnull( CONVERT(VARCHAR, DATEPART(hh,  date)),00)+':'+           
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2),00) as [Appt time]          
 ,event AS [Title],                          
 ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                               
 c1.phone1 as Phone,c1.phone2,c.email AS Email ,Enddttm as [Event Enddate],location as   [Event Template]                             
 ,(CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR(date) AS VARCHAR(4)))  as E_Date,CONVERT(VARCHAR, DATEPART(hh,  date))  as E_Hours,RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2)  as E_minute                                
 ,isnull((CONVERT(VARCHAR(5), Enddttm, 101)+'/'+CAST(YEAR(Enddttm) AS VARCHAR(4))), (CONVERT(VARCHAR(5), getdate(), 101)+'/'+CAST(YEAR( getdate()) AS VARCHAR(4)))) as END_Date,                    
isnull( CONVERT(VARCHAR, DATEPART(hh,  Enddttm)),00)  as END_Hours,                    
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  Enddttm)), 2),00)  as END_minute                       
 ,eventno  , C.firmcode                     
 ,c2.first,c2.last  ,c2.SMS,c2.CALL,C2.EMAIL,c2.PartiesFirmcode as    PartiesFirmcode                                        
 FROM casecard ca                            
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno   
 inner join [card] c ON c.cardcode=ca.cardcode and C.firmcode in (c2.PartiesFirmcode)
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode                                       
 WHERE --ca.type='CLIENT'  AND                
 c.first <> '' and c.last <>'' and                         
 CONVERT(DATE,c2.date)  BETWEEN CONVERT(date,GETDATE()-7) AND CONVERT(date,GETDATE())             
 and      
 ((c.[first] like '%' + LTRIM(RTRIM(@Filter)) + '%')                        
or                 
(c.[last] like '%' + LTRIM(RTRIM(@Filter)) + '%')                
or                  
(C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')               
or                
(C2.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
or               
(C1.phone1 like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                
 ORDER BY c2.date ASC                       
                            
END                          
                          
ELSE IF(@Flag='Specific Date')                         
BEGIN                          
                            
                            
 SELECT ca.caseno AS [Case No],           
  (CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR( date) AS VARCHAR(4)))+' '+isnull( CONVERT(VARCHAR, DATEPART(hh,  date)),00)+':'+           
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2),00) as [Appt time]          
 ,event AS [Title],                          
 ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                               
 c1.phone1 as Phone,c1.phone2,c.email AS Email ,Enddttm as [Event Enddate],location as  [Event Template]                           
 ,(CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR(date) AS VARCHAR(4)))  as E_Date,CONVERT(VARCHAR, DATEPART(hh,  date))  as E_Hours,RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2)  as E_minute                                
 ,isnull((CONVERT(VARCHAR(5), Enddttm, 101)+'/'+CAST(YEAR(Enddttm) AS VARCHAR(4))), (CONVERT(VARCHAR(5), getdate(), 101)+'/'+CAST(YEAR( getdate()) AS VARCHAR(4)))) as END_Date,                    
isnull( CONVERT(VARCHAR, DATEPART(hh,  Enddttm)),00)  as END_Hours,                    
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  Enddttm)), 2),00)  as END_minute                       
 ,eventno  , C.firmcode                     
 ,c2.first,c2.last ,c2.SMS,c2.CALL,C2.EMAIL   ,c2.PartiesFirmcode as    PartiesFirmcode                                        
 FROM casecard ca                            
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno   
 inner join [card] c ON c.cardcode=ca.cardcode and C.firmcode in (c2.PartiesFirmcode)
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode                                     
 WHERE --ca.type='CLIENT'  AND             
 c.first <> '' and c.last <>'' and                      
 CONVERT(DATE,c2.date) = CONVERT(DATE,@Fromdt)            
 and      
 ((c.[first] like '%' + LTRIM(RTRIM(@Filter)) + '%')                        
or                 
(c.[last] like '%' + LTRIM(RTRIM(@Filter)) + '%')                
or                  
(C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')               
or                
(C2.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
or               
(C1.phone1 like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                
 ORDER BY c2.date ASC         
       
                      
END                          
ELSE IF(@Flag='Custom Range')                           
BEGIN                          
                            
 SELECT ca.caseno AS [Case No],   (CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR( date) AS VARCHAR(4)))+' '+isnull( CONVERT(VARCHAR, DATEPART(hh,  date)),00)+':'+           
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2),00) as [Appt time],event AS [Title],                          
 ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                               
 c1.phone1 as Phone,c1.phone2,c.email AS Email ,Enddttm as [Event Enddate],location as  [Event Template]                             
 ,(CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR(date) AS VARCHAR(4)))  as E_Date,CONVERT(VARCHAR, DATEPART(hh,  date))  as E_Hours,RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2)  as E_minute                                
 ,isnull((CONVERT(VARCHAR(5), Enddttm, 101)+'/'+CAST(YEAR(Enddttm) AS VARCHAR(4))), (CONVERT(VARCHAR(5), getdate(), 101)+'/'+CAST(YEAR( getdate()) AS VARCHAR(4)))) as END_Date,                    
isnull( CONVERT(VARCHAR, DATEPART(hh,  Enddttm)),00)  as END_Hours,                    
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  Enddttm)), 2),00)  as END_minute                       
 ,eventno  , C.firmcode                     
 ,c2.first,c2.last ,c2.SMS,c2.CALL,C2.EMAIL  ,c2.PartiesFirmcode as    PartiesFirmcode                                         
 FROM casecard ca                            
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno   
 inner join [card] c ON c.cardcode=ca.cardcode and C.firmcode in (c2.PartiesFirmcode)
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode                                    
 WHERE --ca.type='CLIENT'  AND                    
 c.first <> '' and c.last <>'' and                     
 CONVERT(DATE,c2.date)  BETWEEN CONVERT(date,@Fromdt)AND CONVERT(date,@Todt)        
 and      
 ((c.[first] like '%' + LTRIM(RTRIM(@Filter)) + '%')                        
or                 
(c.[last] like '%' + LTRIM(RTRIM(@Filter)) + '%')                
or                  
(C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')               
or                
(C2.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
or               
(C1.phone1 like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                
 ORDER BY c2.date ASC                    
                            
END                          
                          
                          
ELSE                          
BEgin                          
 SELECT ca.caseno AS [Case No],            
  (CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR( date) AS VARCHAR(4)))+' '+isnull( CONVERT(VARCHAR, DATEPART(hh,  date)),00)+':'+           
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2),00) as [Appt time]          
 ,event AS [Title],                          
 ISNULL(LTRIM(RTRIM( c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],                           
 c1.phone1 as Phone,c1.phone2,c.email AS Email ,Enddttm as [Event Enddate],location  as [Event Template]                 
 ,(CONVERT(VARCHAR(5), date, 101)+'/'+CAST(YEAR(date) AS VARCHAR(4)))  as E_Date,CONVERT(VARCHAR, DATEPART(hh,  date))  as E_Hours,RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  date)), 2)  as E_minute                                
 ,isnull((CONVERT(VARCHAR(5), Enddttm, 101)+'/'+CAST(YEAR(Enddttm) AS VARCHAR(4))), (CONVERT(VARCHAR(5), getdate(), 101)+'/'+CAST(YEAR( getdate()) AS VARCHAR(4)))) as END_Date,                    
isnull( CONVERT(VARCHAR, DATEPART(hh,  Enddttm)),00)  as END_Hours,                    
 isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,  Enddttm)), 2),00)  as END_minute                       
 ,eventno  , C.firmcode                     
 ,c2.first,c2.last ,c2.SMS,c2.CALL,C2.EMAIL   ,c2.PartiesFirmcode as    PartiesFirmcode                                      
 FROM casecard ca                            
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno   
 inner join [card] c ON c.cardcode=ca.cardcode and C.firmcode in (c2.PartiesFirmcode)
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode                                 
--WHERE ca.type='CLIENT'                      
 where c.first <> '' and c.last <>''        
 and      
 ((c.[first] like '%' + LTRIM(RTRIM(@Filter)) + '%')                        
or                 
(c.[last] like '%' + LTRIM(RTRIM(@Filter)) + '%')                
or                  
(C.Email like '%' + LTRIM(RTRIM(@Filter)) + '%')               
or                
(C2.caseno like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
or               
(C1.phone1 like '%' + LTRIM(RTRIM(@Filter)) + '%'))                                
 ORDER BY c2.date ASC          
                            
END                          
                         
                            
                            
                          
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_TwilioLogForReport]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_Get_TwilioLogForReport]
as 
begin

select FromNumber as [From], ToNumber as [To],TL.Status as [Status],               
isnull(case When Duration ='' then NULL Else Duration End,0)  as [Duration (Second)],                  
(CONVERT(VARCHAR(5), StartDate, 101)+'/'+CAST(YEAR(StartDate) AS VARCHAR(4)))+' '+                  
isnull( CONVERT(VARCHAR, DATEPART(hh,StartDate)),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,StartDate)), 2),00)                  
as [Start Date] ,                  
                  
(CONVERT(VARCHAR(5), EndDate, 101)+'/'+CAST(YEAR(EndDate) AS VARCHAR(4)))+' '+                  
isnull( CONVERT(VARCHAR, DATEPART(hh,EndDate)),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,EndDate)), 2),00)                  
as [End Date] ,TL.SID,LogType,StartDate,TR.[first] as [First Name],TR.[last] as [Last Name],TR.eventno as eventno      
,TimingID as TimingID ,Caseno as  [Case No] ,LRC.R as R,LRC.G as G,LRC.B as B , Ca.email       
--into #Temp             
FROM tbl_TwilioLog  TL          
Inner Join  tbl_Twilio_SID TS On TS.SID =TL.SID          
Inner Join  tbl_LRS_Reminder_Timing TLRT on  TLRT.TimingID = TS.EventFK          
Inner join  tbl_LRS_Reminder TR on TR.eventno=TLRT.EventFK       
Inner join lst_Log_Responce_Color LRC on LRC.Status = TL.Status     
inner Join [Card] Ca On Ca.cardcode = TR.CardcodeFK
where CONVERT(DATE, StartDate) = CONVERT(DATE, GETDATE())
--select * from tbl_TwilioLog where CONVERT(DATE, StartDate) = CONVERT(DATE, GETDATE())
end

--exec sp_Get_TwilioLogForReport
GO
/****** Object:  StoredProcedure [dbo].[sp_get_future_reminder]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_get_future_reminder]        
AS            
Begin             
 Select         
  RID,          
  EventID,          
  [Event],          
  EventTitle,          
  Phone,          
  Email,          
  FromPhoneNo,          
  AccountSID,          
  AuthToken,          
  CONVERT(VARCHAR, EventDatetime, 120) as EventDatetime,          
  SMSStatus,          
  EmailStatus,    
  CallStatus,     
  ClientKey,    
  RedirectLink,                             
  EmailFrom,           
  EmailPassword,           
  EmailFromTitle,    
  EmailPort,    
  EmailSMTP,           
  CompanyLO,           
  IsStatus,           
  MainEventID,           
  IsTwilioStatus,          
  CONVERT(VARCHAR, AppointmentDatetime, 120) as AppointmentDatetime,          
  IsConfirmable,          
  TwilioLang          
 From         
  dbo.tbl_FinalReminderDetails        
 Where        
  CONVERT(date, dbo.tbl_FinalReminderDetails.EventDatetime) >  CONVERT(date,GETDATE()) And        
  dbo.tbl_FinalReminderDetails.IsStatus=0  
 Order by MainEventID asc          
End   
  
--select * from tbl_FinalReminderDetails order by 1 desc  
/*  
  
update tbl_FinalReminderDetails set IsStatus=0,MainEventID=85 where ClientKey='743A-F81C-5830-4852-8D1D'  
and RID in (851,  
852,  
853)  
  
*/
GO
/****** Object:  StoredProcedure [dbo].[Sp_ClearAddparties]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_ClearAddparties]   
@MACID Nvarchar(100)  =null      
as    
Begin    
    
 delete from  AddParties where MACID=@MACID  
    
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_CheckEmailLogin]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Sp_CheckEmailLogin]
@Username Nvarchar(100),
@Password nvarchar(100)

as
Begin

if exists (select * from tbl_Email_login EL where EL.Username=@Username and EL.Password=@Password)
Begin

select 1 as Result

END
Else	
Begin

select 2 as Result

End

--insert Into tbl_Email_login
--select 'tony','tony@123',0,Getdate(),Getdate()



end
GO
/****** Object:  StoredProcedure [dbo].[Sp_Change_Password]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_Change_Password]
@oldPassword Nvarchar(50),
@NewPassword Nvarchar(50)
as
Begin

	If Exists (select Password from tbl_Email_Login where Password =@oldPassword)
	Begin

		Update tbl_Email_Login set Password =@NewPassword
		
		select 1 as Result

	End
	Else
	Begin
		
		select 2 as Result
	End

End

select GEtdate()
GO
/****** Object:  StoredProcedure [dbo].[Sp_CaseWiseParties]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC Sp_CaseWiseParties '266','05/18/2016'
  
CREATE Proc [dbo].[Sp_CaseWiseParties]  
@CaseNo Nvarchar(50),  
@Date Datetime   
As  
Begin             
 --SELECT C.firmcode,ISNULL(LTRIM(RTRIM(c.first)),'') as Name,ISNULL(LTRIM(RTRIM(c.last)),'') as LName,ca.caseno AS [CaseNo],c.cardcode,c.Email   
 --FROM casecard ca              
 --INNER JOIN [card] c ON c.cardcode=ca.cardcode             
 --Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno             
 --LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode              
 --WHERE   Ca.caseno=@CaseNo and   c.first <> '' and c.last <>''    and   
 --CONVERT(DATE,c2.date) = CONVERT(date,@Date)            
 --ORDER BY c2.date ASC 
 
 
  select C.firmcode,ISNULL(LTRIM(RTRIM(TL.first)),'') as Name,ISNULL(LTRIM(RTRIM(TL.last)),'') as LName,CC.caseno AS [CaseNo],CC.cardcode,c.Email  
 from tbl_LRS_Reminder  TL
 inner join [card] C On C.cardcode =TL.CardcodeFK
 inner Join card2 C2 on C2.firmcode =C.firmcode
 inner join casecard CC on CC.cardcode =TL.CardcodeFK
 WHERE   CC.caseno=@CaseNo and   c.first <> '' and c.last <>''    and   
 CONVERT(DATE,TL.date) = CONVERT(date,@Date)            
 ORDER BY TL.date ASC 
 
   
   
   


   
End  
   
 --select getdate()  
 --select * from  AddParties  
 --select * from
GO
/****** Object:  StoredProcedure [dbo].[SP_A1DataJobschedule]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_A1DataJobschedule]
AS
BEGIN

select eventno,caseno, CONVERT(VARCHAR(10), date, 101)  AS [EventStartDate],event AS [Title],date ,GETDATE(),DATEPART(HOUR, GETDATE()),DATEPART(MINUTE, GETDATE())
from 
cal1  
WHERE event IS NOT NULL AND event <> '' AND   
--date = GETDATE()
CONVERT(DATE,date) = CONVERT(DATE,GETDATE())
ORDER BY caseno ASC




END
GO
/****** Object:  StoredProcedure [dbo].[SP_Update_Contact]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Update_Contact]                    
@Salutation Nvarchar(50),                    
@FName Nvarchar(500),                    
@MName Nvarchar(500),                    
@LName Nvarchar(500),                    
@PhoneNo Nvarchar(20),                    
@Email Nvarchar(500),                    
@Zipcode Nvarchar(10),                    
@Notes Nvarchar(MAX) ,                
@DOB datetime ,              
@CountryCode Nvarchar(10),              
@MobileType Nvarchar(100),              
@EmailType Nvarchar(100)  ,              
@CardCode Nvarchar(10),              
@firmcode Nvarchar(10)              
                    
                    
              
                    
                    
As                    
Begin                    
                   
if(@CardCode not like '%RS%' and @firmcode not like '%RS%' )         
Begin         
  
  
insert into tbl_RS_Contactlist                      
(          
Salutation,          
FName,          
MName,          
LName,          
PhoneNo,          
Email,          
Zipcode,          
Notes,          
DOB,          
CountryCode,          
MobileType,          
EmailType,          
Isactive,          
CreatedDatetime,          
ModifiedDatetime)          
          
VALUES          
(          
@Salutation,          
@FName,          
@MName,          
@LName,          
@PhoneNo,          
@Email,          
@Zipcode,          
@Notes,          
@DOB,          
@CountryCode,          
@MobileType,          
@EmailType,          
0,          
GETDATE(),          
GETDATE()          
)          
          
Declare @CIDNew BIGINT          
          
set @CIDNew =SCOPE_IDENTITY()          
          
           
              
Update    tbl_RS_Contactlist set  [Case]='RS'+''+Convert(NVARCHAR(100),@CIDNew)    ,Firmcode='RS'+''+Convert(NVARCHAR(100),@CIDNew)          
,CardCode ='RS'+''+Convert(NVARCHAR(100),@CIDNew) where  CID=  @CIDNew          
                                    
select 1 As Result            
                                                                                                                                                                                                                 
  --If Not Exists(select phone1 from card2 C2 inner join Card C on C.firmcode=C2.firmcode  where   C2.phone1=@PhoneNo and C.cardcode!=@CardCode )                
  --Begin            
                
  --  Update [Card] set   letsal=  'Dear'+' '+@Salutation,              
  --  [first]=@FName, middle=@MName ,[last]=@LName,[type]='LRS',email=@Email,              
  --  salutation=@Salutation,birth_date=@DOB,lastdt=Getdate(),comments= @Notes  ,home=@PhoneNo,car=@PhoneNo,business=@PhoneNo          
  --  where cardcode=@CardCode              
                     
                     
  --  UPDate card2 set               
  --  zip=@Zipcode,phone1=@PhoneNo,lastdt=Getdate(),CountryCode=@CountryCode, MobileType=@MobileType, EmailType=@EmailType              
  --  where firmcode=@firmcode                   
                     
                      
  -- select 1 As Result            
            
  --END                  
  --Else          
  --Begin          
  -- select 2 As Result          
  --END          
                    
End        
ELSE        
BEGIN        
         
 update  tbl_RS_Contactlist set Salutation=@Salutation,FName=@FName,MName=@MName,        
 LName=@LName,PhoneNo =@PhoneNo,Email=@Email,Zipcode=@Zipcode,Notes=@Notes,        
 DOB=@DOB,CountryCode=@CountryCode,MobileType=@MobileType,EmailType=@EmailType,ModifiedDatetime=Getdate()        
 where CardCode=@CardCode        
        
select 1 As Result            
END          
END
GO
/****** Object:  StoredProcedure [dbo].[sp_update_confirm_status]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[sp_update_confirm_status]                                                                        
                                                                   
@EventNo int,    
@ModifiedDatetime Datetime,                                                                
@Status int                                                               
                                                                   
As                                                                        
Begin            

	Update dbo.tbl_LRS_Reminder    
	Set     
	dbo.tbl_LRS_Reminder.ModifiedDate = @ModifiedDatetime ,     
	dbo.tbl_LRS_Reminder.LastUpdatedStatusFK = @Status    
	Where    
	dbo.tbl_LRS_Reminder.eventno = @EventNo    
       
	Insert     
	Into dbo.tbl_LRS_ManuallyUpadatedReminder    
	(    
	EventNo,    
	ModifiedDate,    
	CreatedDate,    
	TwillioStatus    
	)    
	Values     
	(    
	@EventNo,    
	@ModifiedDatetime,    
	GETDATE(),    
	@Status    
	)    


	Select 1 AS Result;    
	
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_Update_Color]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_Update_Color]  
@Flag nvarchar(100),  
@ColorID nvarchar(30) ,  
@R nvarchar(30),  
@G nvarchar(30),  
@B nvarchar(30)  
as  
Begin  
  
if(@Flag='Reminder')  
Begin  
  
 update lst_Appointment_Responce_Color set R=@R, G=@G, B=@B where ColorID=@ColorID  
   
 select 1 as Result  
  
End  
  
    
if(@Flag='Log')  
Begin  
  
 update lst_Log_Responce_Color set R=@R, G=@G, B=@B where ColorID=@ColorID  
   
 select 1 as Result  
  
End  
  
  
End
GO
/****** Object:  StoredProcedure [dbo].[SP_TwilioStatus_New]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_TwilioStatus_New]                        
@EventID Nvarchar(50),                        
@CallValue Nvarchar(50)                        
                        
As                        
Begin                        
                        
Update tbl_FinalReminderDetails set IsTwilioStatus=@CallValue ,ModifiedDttm=getdate()   
  where RID=@EventID                          
                        
select 1 as Result                        
        
End
GO
/****** Object:  StoredProcedure [dbo].[SP_TwilioStatus]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec SP_TwilioStatus '497','2'                
                
CREATE Proc [dbo].[SP_TwilioStatus]                
@EventID Nvarchar(50),                
@CallValue Nvarchar(50)                
                
As                
Begin                
                
Update tbl_LRS_Reminder_Timing set IVRStatus=@CallValue where TimingID=@EventID                
                
Declare @Event nvarchar(50),@IVRS Int                 
                
                
set @Event =(select EventFK from tbl_LRS_Reminder_Timing where TimingID=@EventID )                
                
set @IVRS =(select IVRStatus from tbl_LRS_Reminder_Timing where TimingID=@EventID )                
                
                
update tbl_LRS_Reminder set LastUpdatedStatusFK=@IVRS  where eventno=@Event                
                
                
If(@CallValue = '2' or @CallValue = '3')                
Begin                 
                
 update tbl_LRS_Reminder_Timing set Isactive=1 where EventFK=@Event                
               
               
Declare @EmailCancelStatus Nvarchar(50),@EmailRescheduleStatus Nvarchar(50),@SMSCancelStatus Nvarchar(50)              
,@SMSRescheduleStatus Nvarchar(50),@FullName Nvarchar(100),@EventName Nvarchar(500),@EventDt Nvarchar(50)              
,@Phone Nvarchar(50),@Email Nvarchar(50)  ,@EventTitle Nvarchar(500),@MINPHONE Nvarchar(50),@MAXPHONE Nvarchar(50)  
,@PhoneChange Nvarchar(50)  
  
            
               
Set @SMSCancelStatus =(select isnull(SMSCancel,0) from tbl_LRS_NotificationSetting where isnull(Isactive,1)=0)             
Set @SMSRescheduleStatus=(select isnull(SMSReschedule,0) from tbl_LRS_NotificationSetting where isnull(Isactive,1)=0)               
            
Set  @EmailCancelStatus =(select isnull(EmailCancel,0) from tbl_LRS_NotificationSetting where isnull(Isactive,1)=0)             
Set  @EmailRescheduleStatus =(select isnull(EmailReschedule,0) from tbl_LRS_NotificationSetting where isnull(Isactive,1)=0)               
            
            
set @FullName = (select LR.first+' '+LR.last as FullName from tbl_LRS_Reminder LR where LR.Eventno=@Event)               
            
            
set @EventName = (select Event as [EventTitle] from tbl_LRS_Reminder LR where LR.Eventno=@Event)             
            
             
               
select ROW_NUMBER() over(order by (select 1)) as [No],Column1 as EventTitle into #Temp from dbo.SplitByJEM(@EventName)               
set @EventDt =(select LR.date as [Eventdt] from tbl_LRS_Reminder LR where LR.Eventno=@Event)   
  
  
set @Phone =(select SMSSend from tbl_LRS_NotificationSetting)  
  
select ROW_NUMBER() over(order by (select 1))as [No],Column1 as Phone into #TempPhone from dbo.SplitByComma(@Phone)  
  
  
  
set @MINPHONE =(select Min(No) from #TempPhone)                          
set @MAXPHONE =(select Max(No) from #TempPhone)   
            
set @Email = (select EmailSend from tbl_LRS_NotificationSetting)                          
set @EventTitle  =(select EventTitle from #Temp where [No]=1)            
            
            
             
             
if(@EmailCancelStatus = '1' and @Email <> '')              
Begin              
              
insert into tbl_Notification_TimingSetting              
Select DATEADD(MINUTE,5,Getdate()),@FullName+' '+'has requested to Cancel this'+''+@EventTitle+' at '+@EventDt,              
@Phone,@Email,'Cancel Requested:'+@EventTitle+' @ '+@EventDt +''+' by '+@FullName ,0,Getdate(), Getdate(),'EMAIL'            
End              
              
if(@EmailRescheduleStatus = '1' and @Email <> '')              
Begin              
              
insert into tbl_Notification_TimingSetting              
Select DATEADD(MINUTE,5,Getdate()),@FullName+' '+'has requested to Reschedule this'+''+@EventTitle+' at '+@EventDt,              
@Phone,@Email,'Reschedule Requested:'+@EventTitle+' @ '+@EventDt +''+' by '+@FullName  ,0,Getdate(), Getdate(),'EMAIL'            
              
End           
  
 While(@MAXPHONE >= @MINPHONE)                          
 Begin    
   
 set @PhoneChange = (select Phone from #TempPhone where No=@MINPHONE)          
     
 if(@SMSCancelStatus = '1' and @Phone <> '')              
 Begin              
               
 insert into tbl_Notification_TimingSetting              
 Select DATEADD(MINUTE,5,Getdate()),@FullName+' '+'has requested to Cancel this'+''+@EventTitle+' at '+@EventDt,              
 @PhoneChange,@Email,'Cancel Requested:'+@EventTitle+' @ '+@EventDt +''+' by '+@FullName ,0,Getdate(), Getdate() ,'SMS'            
 End              
 if(@SMSRescheduleStatus = '1' and @Phone <> '')              
 Begin              
               
 insert into tbl_Notification_TimingSetting              
 Select DATEADD(MINUTE,5,Getdate()),@FullName+' '+'has requested to Reschedule this'+''+@EventTitle+' at '+@EventDt,              
 @PhoneChange,@Email,'Reschedule Requested:'+@EventTitle+' @ '+@EventDt +''+' by '+@FullName  ,0,Getdate(), Getdate(),'SMS'            
 End      
  
 set @MINPHONE =@MINPHONE+1          
  
END          
               
               
               
                
End                
                
select 1 as Result                
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_TwilioLog_SendMail]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sp_TwilioLog_SendMail 'Today','','',''      
                          
CREATE PROC [dbo].[Sp_TwilioLog_SendMail]                                               
@Flag NVARCHAR(50)= null ,                                                    
@Fromdt DATETIME= null ,                                                    
@Todt DATETIME= null,                                
@Filter Nvarchar(50) =null                          
                          
AS                                                
BEGIN                                              
                                
      
                
SELECT     FromNumber as [From], ToNumber as [To],TL.Status as [Status],                   
isnull(case When Duration ='' then NULL Else Duration End,0)  as [Duration (Second)],                      
(CONVERT(VARCHAR(5), StartDate, 101)+'/'+CAST(YEAR(StartDate) AS VARCHAR(4)))+' '+                      
isnull( CONVERT(VARCHAR, DATEPART(hh,StartDate)),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,StartDate)), 2),00)                      
as [Start Date] ,                      
                      
(CONVERT(VARCHAR(5), EndDate, 101)+'/'+CAST(YEAR(EndDate) AS VARCHAR(4)))+' '+                      
isnull( CONVERT(VARCHAR, DATEPART(hh,EndDate)),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,EndDate)), 2),00)                      
as [End Date] ,TL.SID,LogType,StartDate,TR.[first] as [First Name],TR.[last] as [Last Name],TR.eventno as eventno          
,TimingID as TimingID ,Caseno as  [Case No] ,LRC.R as R,LRC.G as G,LRC.B as B , Ca.email ,
(select ES.EmailFrom from tbl_Email_Setting ES) as EmailFrom,      
      
(select ES.EmailPassword from tbl_Email_Setting ES) as EmailPassword,      
      
(select ES.EmailFromTitle from tbl_Email_Setting ES) as EmailFromTitle,      
       
(select ES.EmailPort from tbl_Email_Setting ES) as EmailPort,      
      
(select ES.EmailSMTP from tbl_Email_Setting ES) as EmailSMTP ,
(select EmailSend from tbl_LRS_NotificationSetting) as EmailTO


          
into #Temp                 
FROM tbl_TwilioLog  TL              
Inner Join  tbl_Twilio_SID TS On TS.SID =TL.SID              
Inner Join  tbl_LRS_Reminder_Timing TLRT on  TLRT.TimingID = TS.EventFK              
Inner join  tbl_LRS_Reminder TR on TR.eventno=TLRT.EventFK           
Inner join lst_Log_Responce_Color LRC on LRC.Status = TL.Status         
inner Join [Card] Ca On Ca.cardcode = TR.CardcodeFK      
         
where EndDate <> '1900-01-01 00:00:00.000'                      
Order by StartDate Desc                   
                         
                           
              
                          
                        
                           
IF(@Flag='Today')                                                       
BEGIN                           
select ROW_NUMBER() over(order by (select 1)) as No,[From],[To],[Status],[Duration (Second)] as [Duration]                    
,[Start Date],[End Date],LogType as [Type],[First Name],[Last Name],eventno,TimingID ,[Case No],R,G,B,EmailFrom ,EmailPassword,EmailFromTitle  ,
EmailPort ,EmailSMTP ,EmailTO                 
 from #Temp                                                
 WHERE   CONVERT(DATE,StartDate) = CONVERT(date,GETDATE())           
 and       
  [First Name] <> '' and [Last Name] <>'' and              
 (([First Name] like '%' + LTRIM(RTRIM(@Filter)) + '%') or                                   
 ([Last Name] like '%' + LTRIM(RTRIM(@Filter)) + '%')                                  
 or                                    
 (email like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
 or                    
 ([Case No] like '%' + LTRIM(RTRIM(@Filter)) + '%')                   
 or                                 
 ([to] like '%' + LTRIM(RTRIM(@Filter)) + '%')      
 or       
 ([Status] like '%' + LTRIM(RTRIM(@Filter)) + '%'))        
       
        
 Order by StartDate Desc                 
                          
                          
END        




SELECT     EmailID, EmailFrom, EmailPassword, EmailFromTitle, EmailPort, EmailSMTP, Twilio_CALL, Twilio_SMS, Twilio_AccountSid, Twilio_AuthToken, CompanyLogo, 
                      Signature,(select EmailSend from tbl_LRS_NotificationSetting) as EmailTO
FROM         tbl_Email_Setting
end
GO
/****** Object:  StoredProcedure [dbo].[Sp_TwilioLog_Filter]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sp_TwilioLog_Filter 'Today'
CREATE PROC [dbo].[Sp_TwilioLog_Filter]                                                                               
@Flag NVARCHAR(50)= null ,                                                                                    
@Fromdt DATETIME= null ,                                                                                    
@Todt DATETIME= null,                                                                
@Filter Nvarchar(50) =null                                                          
                                                          
AS                                                                                
BEGIN                                                                              
                                                                
                                                                
                                                
                                                
SELECT  DISTINCT    FromNumber as [From], ToNumber as [To],TL.Status as [Status],                                                   
isnull(case When Duration ='' then NULL Else Duration End,0)  as [Duration (Second)],                               
                          
(CONVERT(VARCHAR(5), DATEADD(MINUTE,-840,StartDate), 101)+'/'+CAST(YEAR(DATEADD(MINUTE,-840,StartDate)) AS VARCHAR(4)))+' '+                                                      
isnull( CONVERT(VARCHAR, DATEPART(hh,DATEADD(MINUTE,-840,StartDate))),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,DATEADD(MINUTE,-840,StartDate))), 2),00)                                                      
as [Start Date]                                                    
        ,                  
                          
(CONVERT(VARCHAR(5), DATEADD(MINUTE,-840,EndDate), 101)+'/'+CAST(YEAR(DATEADD(MINUTE,-840,EndDate)) AS VARCHAR(4)))+' '+                                                      
isnull( CONVERT(VARCHAR, DATEPART(hh,DATEADD(MINUTE,-840,EndDate))),00)+':'+isnull(RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,DATEADD(MINUTE,-840,EndDate))), 2),00)                                                      
as [End Date] ,                          
                          
                  
                          
                          
TL.SID,LogType,StartDate,TR.[first] as [First Name],TR.[last] as [Last Name],TR.eventno as eventno                                          
,TimingID as TimingID ,Caseno as  [Case No] ,LRC.R as R,LRC.G as G,LRC.B as B , Ca.email,TR.EventHTML                         
,RIGHT(Convert(VARCHAR(20), TR.date,100),2) as [AMPM],TR.IsPrivate,TR.IsConfirmable                                   
                                    
                                          
into #Temp                                                 
FROM tbl_TwilioLog  TL                                              
LEFT outer Join  tbl_FinalReminderDetails TS On TS.SID =TL.SID                                              
LEFT outer Join tbl_LRS_Reminder_Timing TLRT on  TLRT.TimingID = TS.EventID                                              
LEFT outer Join tbl_LRS_Reminder TR on TR.eventno=TLRT.EventFK                                           
Left outer join lst_Log_Responce_Color LRC on LRC.Status = TL.Status                                         
LEFT outer Join [Card] Ca On Convert(nvarchar(50),Ca.cardcode)= COnvert(nvarchar(50),TR.CardcodeFK)                                  
                                         
where EndDate <> '1900-01-01 00:00:00.000'                                                      
Order by StartDate Desc                                                   
                                                         
                                                           
                                              
                                                      
                                                 
                                                
IF(@Flag='Today')                           BEGIN                                                           
select ROW_NUMBER() over(order by (select 1)) as No,[From],[To],[Status],[Duration (Second)] as [Duration]                                                    
,[Start Date],[End Date],LogType as [Type],[First Name],[Last Name],eventno,TimingID ,[Case No],R,G,B                
 from #Temp                                                           
 WHERE   CONVERT(DATE,StartDate)    BETWEEN CONVERT(date,GETDATE()) AND CONVERT(date,GETDATE()+1)                                          
                         
                                        
 and                                       
  --[First Name] <> '' and [Last Name] <>'' and                                              
 (([First Name] like LTRIM(RTRIM(@Filter)) + '%') or                                                                   
 ([Last Name] like  LTRIM(RTRIM(@Filter)) + '%')                                                                  
 or                                                                    
 (email like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                    
 ([Case No] like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                                 
 ([to] like '%' +  LTRIM(RTRIM(@Filter)) + '%')                                      
 or                                       
 ([Status] like  LTRIM(RTRIM(@Filter)) + '%'))                                        
                                       
                                        
 Order by StartDate Desc                                                 
                                                          
                                                          
END                                                          
                                                          
ELSE IF(@Flag='Tomorrow')                                                                                       
BEGIN                                                             
select ROW_NUMBER() over(order by (select 1)) as No,[From],[To],[Status],[Duration (Second)] as [Duration]                                                    
,[Start Date],[End Date],LogType as [Type],[First Name],[Last Name],eventno,TimingID ,[Case No] ,R,G,B                                                     
 from #Temp                                                                                  
 WHERE                                                              
  CONVERT(DATE,StartDate) = CONVERT(date,GETDATE()+1)                                         
                                         
 and                                       
  --[First Name] <> '' and [Last Name] <>'' and                                           
 (([First Name] like LTRIM(RTRIM(@Filter)) + '%') or                                                                   
 ([Last Name] like  LTRIM(RTRIM(@Filter)) + '%')                                                                  
 or                                                                    
 (email like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                    
 ([Case No] like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                                 
 ([to] like  '%' + LTRIM(RTRIM(@Filter)) + '%')                                      
 or                                       
 ([Status] like  LTRIM(RTRIM(@Filter)) + '%'))                                    
  Order by StartDate Desc                                                           
                                                         
END       
ELSE IF(@Flag='Yesterday')                                                                                       
BEGIN                                                   
                                        
select ROW_NUMBER() over(order by (select 1)) as No,[From],[To],[Status],[Duration (Second)] as [Duration]                                                    
,[Start Date] ,[End Date],LogType as [Type],[First Name],[Last Name],eventno,TimingID ,[Case No] ,R,G,B                                                     
 from #Temp                                                                                
 WHERE CONVERT(DATE,StartDate) = CONVERT(date,GETDATE()-1)                          
                                        
 and                                       
  --[First Name] <> '' and [Last Name] <>'' and                             
 (([First Name] like LTRIM(RTRIM(@Filter)) + '%') or                                             
 ([Last Name] like  LTRIM(RTRIM(@Filter)) + '%')                                                                  
 or                                                               
 (email like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                    
 ([Case No] like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                                 
 ([to] like '%' +  LTRIM(RTRIM(@Filter)) + '%')                                      
 or                                       
 ([Status] like  LTRIM(RTRIM(@Filter)) + '%'))                                                                 
  Order by StartDate Desc                                                
END                                                          
ELSE IF(@Flag='Past Week')                                                                           
BEGIN                                                           
                                                
select ROW_NUMBER() over(order by (select 1)) as No,[From],[To],[Status],[Duration (Second)] as [Duration]                                                    
,[Start Date],[End Date],LogType as [Type],[First Name],[Last Name],eventno,TimingID ,[Case No] ,R,G,B                                                     
 from #Temp                                                                                  
 WHERE   CONVERT(DATE,StartDate)  BETWEEN CONVERT(date,GETDATE()-7) AND CONVERT(date,GETDATE())                                          
                                       
                                       
 and                                       
  --[First Name] <> '' and [Last Name] <>'' and                                              
 (([First Name] like LTRIM(RTRIM(@Filter)) + '%') or                                                                   
 ([Last Name] like  LTRIM(RTRIM(@Filter)) + '%')                                                                  
 or                                                                    
 (email like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                    
 ([Case No] like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                                 
 ([to] like '%' +  LTRIM(RTRIM(@Filter)) + '%')                                      
 or                                       
 ([Status] like  LTRIM(RTRIM(@Filter)) + '%'))                                          
                                       
                                       
                                       
                                             
    Order by StartDate Desc                                              
END                                                          
ELSE IF(@Flag='Specific Date')                                       
BEGIN                                                   
                                                         
select ROW_NUMBER() over(order by (select 1)) as No,[From],[To],[Status],[Duration (Second)] as [Duration]                               
,[Start Date],[End Date],LogType as [Type],[First Name],[Last Name],eventno,TimingID ,[Case No] ,R,G,B                                                     
 from #Temp                                                                                 
 WHERE                 
  CONVERT(DATE,StartDate) = CONVERT(DATE,@Fromdt)                                                          
 and                                       
  --[First Name] <> '' and [Last Name] <>'' and                                              
 (([First Name] like LTRIM(RTRIM(@Filter)) + '%') or                                                                   
 ([Last Name] like  LTRIM(RTRIM(@Filter)) + '%')                                                                  
 or                                                                    
 (email like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                
 ([Case No] like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                                 
 ([to] like  '%' + LTRIM(RTRIM(@Filter)) + '%')                                      
 or                                       
 ([Status] like  LTRIM(RTRIM(@Filter)) + '%'))                                       
                                                     
     Order by StartDate Desc                                             
END                                                          
ELSE IF(@Flag='Custom Range')                                                                             
BEGIN                         
select ROW_NUMBER() over(order by (select 1)) as No,[From],[To],[Status],[Duration (Second)] as [Duration]                                                    
,[Start Date],[End Date],LogType as [Type] ,[First Name],[Last Name],eventno,TimingID ,[Case No],R,G,B                                                     
 from #Temp                                                                                  
 WHERE                                                       
  CONVERT(DATE,StartDate) BETWEEN CONVERT(date,@Fromdt )AND CONVERT(date,@Todt)                                         
                                         
 and                                       
  --[First Name] <> '' and [Last Name] <>'' and                                            
 (([First Name] like LTRIM(RTRIM(@Filter)) + '%') or                                                                   
 ([Last Name] like  LTRIM(RTRIM(@Filter)) + '%')                                                                  
 or                                                                    
 (email like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                    
 ([Case No] like  LTRIM(RTRIM(@Filter)) + '%')                                                   
 or                                                                 
 ([to] like '%' +  LTRIM(RTRIM(@Filter)) + '%')                                      
 or                                       
 ([Status] like  LTRIM(RTRIM(@Filter)) + '%'))                                                     
     Order by StartDate Desc                                                   
END                                                          
                                                           
 END
GO
/****** Object:  StoredProcedure [dbo].[Sp_TimeinsertDelete]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[Sp_TimeinsertDelete]

as
Begin

select * from lst_SMS_Timing where isnull(Isactive,0)=0

End
GO
/****** Object:  StoredProcedure [dbo].[SP_TempPartiesADD_A1LAW]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec SP_TempPartiesADD 'RS1','Bijal','patel','RS1'      
                
CREATE Proc [dbo].[SP_TempPartiesADD_A1LAW]                
@firmcode Nvarchar(100),              
@Name Nvarchar(100) =null,              
@LName Nvarchar(100) =null,          
@Caseno Nvarchar(100)  =null      
         
As                
Begin                
                
If Not exists (select * from  AddParties where Firmcode=@firmcode and CaseNo=@Caseno )                
Begin                
 Insert Into AddParties       
 
(Firmcode,	Name,	LName,	CaseNo,	isstatus) VALUES (@firmcode,@Name,@LName,@Caseno,1)
         
          
                
 select 1 as Result                
End                
Else                
Begin                
 select 1 as Result                
END                
End
GO
/****** Object:  StoredProcedure [dbo].[SP_TempPartiesADD]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec SP_TempPartiesADD 'RS1','Bijal','patel','RS1'      
                
CREATE Proc [dbo].[SP_TempPartiesADD]                
@firmcode Nvarchar(100),              
@Name Nvarchar(100) =null,              
@LName Nvarchar(100) =null,          
@Caseno Nvarchar(100)  =null,      
@MACID Nvarchar(100)  =null          
As                
Begin                
                
If Not exists (select * from  AddParties where Firmcode=@firmcode and CaseNo=@Caseno and  MACID=@MACID)                
Begin                
 Insert Into AddParties                
 select @firmcode,@Name,@LName,@Caseno,1 ,@MACID            
                
 select 1 as Result                
End                
Else                
Begin                
 select 1 as Result                
END                
End                
--truncate table AddParties              
--alter Table AddParties add Name Nvarchar(500)              
--alter Table AddParties add LName Nvarchar(500)          
--alter Table AddParties add CaseNo Nvarchar(500)     
--alter Table AddParties add MACID Nvarchar(100)
GO
/****** Object:  StoredProcedure [dbo].[Sp_removeContact]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[Sp_removeContact]
@CardCode nvarchar(100)
as
Begin


Declare @FrimCode Nvarchar(100)

set @FrimCode =(select Firmcode from [Card] where Cardcode=@CardCode)


Delete From [Card] where Cardcode=@CardCode
delete From Card2 Where FirmCode =@FrimCode

END
GO
/****** Object:  StoredProcedure [dbo].[Sp_ReminderSMS_promotional]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_ReminderSMS_promotional]                                  
                               
As                                  
Begin               
          
          
-----------------------------------------------------------------// New Code : 07/18/2016 //            
              
SELECT CONVERT(VARCHAR,EventDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh,EventDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,EventDatetime)), 2)as [Event DateTime] ,            
            
[Event] AS EventTitle,Phone,Email,TES.Twilio_SMS as FormCall,        
 TES.Twilio_AccountSid as accountSID,        
 TES.Twilio_AuthToken as authToken,         
         
SMSStatus as SMSStatus, EmailStatus as EmailStatus, CallStatus as CALLStatus,         
            
EventTitle as location,RID as EventID            
            
            
FROM  tbl_FinalReminderDetails TFD        
INNER join tbl_Email_Setting TES On TES.ClientKey=TFD.ClientKey                      
WHERE                                            
--(CONVERT(VARCHAR(5), EventDatetime, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, EventDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, EventDatetime)), 2)                                            
--= CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)                                           
--and  

isnull(SMSStatus,0)=1      
and isnull(EmailStatus,0)=999          
            
  
                               
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_ReminderSMS_New]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_ReminderSMS_New]                                
                             
As                                
Begin             
        
        
-----------------------------------------------------------------// New Code : 07/18/2016 //          
            
SELECT CONVERT(VARCHAR,EventDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh,EventDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,EventDatetime)), 2)as [Event DateTime] ,          
          
[Event]+' '+'Please click below link to confirm : http://50.21.182.203:81/index.aspx?UID='+Convert(Nvarchar,RID) AS       
EventTitle,Phone,Email,TES.Twilio_SMS as FormCall,      
 TES.Twilio_AccountSid as accountSID,      
 TES.Twilio_AuthToken as authToken,       
       
SMSStatus as SMSStatus, EmailStatus as EmailStatus, CallStatus as CALLStatus,       
          
EventTitle as location,RID as EventID          
          
          
FROM  tbl_FinalReminderDetails TFD      
INNER join tbl_Email_Setting TES On TES.ClientKey=TFD.ClientKey                    
WHERE                                          
(CONVERT(VARCHAR(5), EventDatetime, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, EventDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, EventDatetime)), 2)                                          
= CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)                                         
and         
isnull(SMSStatus,0)=1            
          

                             
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_ReminderEmail_New]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_ReminderEmail_New]                                      
                                   
As                                      
Begin                  
              
              
-----------------------------------------------------------------// New Code : 07/18/2016 //                
              
SELECT top 1 CONVERT(VARCHAR,EventDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh,EventDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,EventDatetime)), 2)as [Event DateTime] ,                
                
[Event] AS EventTitle,Phone,Email,TES.Twilio_CALL as FormCall,            
 TES.Twilio_AccountSid as accountSID,            
 TES.Twilio_AuthToken as authToken,             
             
 SMSStatus as SMSStatus, EmailStatus as EmailStatus, CallStatus as CALLStatus,                 
isnull(EventTitle,'Reminder System') as [Subject],            
           
            
'http://50.21.182.203:81/index.aspx?UID='+Convert(Nvarchar,RID) AS EventRedirect,            
             
'synergynotificationsystem@gmail.com'  as EmailFrom,                              
                              
'kpagulifkpwecetp' as EmailPassword,                              
                              
TES.EmailFromTitle as EmailFromTitle,                              
                               
TES.EmailPort as EmailPort,                              
                              
TES.EmailSMTP  as EmailSMTP,              
TES.CompanyLogo as EmailCompanyLogo ,              
'0' as LogoSendStatus, --Not in Use              
              
RID as EventID                
                
                
FROM  tbl_FinalReminderDetails  TFD            
INNER join tbl_Email_Setting TES On TES.ClientKey=TFD.ClientKey            
            
                 
WHERE
Email='chirag@solulab.com'
                                                
--(CONVERT(VARCHAR(5), EventDatetime, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, EventDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, EventDatetime)), 2)                                                
--= CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)  and                                             
-- isnull(EmailStatus,0)=1             
           
  
                         
                                   
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_ReminderCallMessage_New]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_ReminderCallMessage_New]      
   
As      
Begin        
 
Select    
Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then       
(CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, birth_date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)      
else      
CONVERT(VARCHAR, LRT.ReminderDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, LRT.ReminderDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LRT.ReminderDatetime)), 2)             
End as [Event DateTime]  ,  

'http://twimlets.com/message?Message%5B0%5D='+dbo.UrlEncode(replace(replace(replace(LR.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')) ,'{EventBody}',LR.[event])  ,'{EventDatetime}',Case When ((LR.location like '%birth%') or (LR.location like '%b''day%')) then     
(CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, birth_date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)    
else    
CONVERT(VARCHAR, LRT.ReminderDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh,LRT.ReminderDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, LRT.ReminderDatetime)), 2)           
End)) AS EventTitle  ,LTRIM(RTRIM(C2.phone1)) as phone,LTRIM(RTRIM(C.email)) AS Email,
LTRIM(RTRIM('+917405372509')) as FormCall ,'ACaab6cd4373c0287b3b81cf957ba4f398' as accountSID,'537fce18a5579c7df939fdc51761e39e'   as authToken
,LR.SMS as SMSStatus,LR.Email as EmailStatus,LR.CALL as CALLStatus,LR.location 
into #TempreminderFinal
from tbl_LRS_Reminder LR
inner join [Card] C on C.CardCode =LR.CardcodeFK
inner join tbl_LRS_Reminder_Timing LRT on LRT.EventFK =LR.eventno
inner join Card2 C2 On C2.firmcode=C.firmcode
Where
isnull(LR.[CALL],0)=1
and isnull(LRT.Isactive,0) <> 1
and LTRIM(RTRIM(C2.phone1)) in ('+18054698585','+15627060200','+18184397535','+919099876111','+917405372509','+919825257926','+18183901964','+919898112147')            
and  LRT.ReminderType='CALL'

select 
[Event DateTime],	EventTitle,	phone,	Email,	FormCall,	accountSID	,authToken	,SMSStatus,	EmailStatus,	CALLStatus,	location
 from #TempreminderFinal
 WHERE    
(CONVERT(VARCHAR(5), [Event DateTime], 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, [Event DateTime])) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, [Event DateTime])), 2)  
= CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2) 

   
END

--select Getdate()
GO
/****** Object:  StoredProcedure [dbo].[Sp_ReminderCallMessage]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [Sp_ReminderCallMessage] 'AAA'  
CREATE Proc [dbo].[Sp_ReminderCallMessage]    
 
As    
Begin      
 SELECT ca.caseno AS [CaseNo],    
 CONVERT(VARCHAR, date, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, date)), 2)       
 as [Event DateTime]  
 ,'http://twimlets.com/message?Message%5B0%5D='+dbo.UrlEncode(replace(replace(replace(c2.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')) ,'{EventBody}',C2.[event])  ,'{Ev
entDatetime}',CONVERT(VARCHAR, date, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, date)), 2))) AS EventTitle,   
 ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],           
 c1.phone1 as phone1,c1.phone2,c.email AS Email,'+917405372509' as FormCall ,'ACaab6cd4373c0287b3b81cf957ba4f398' as accountSID,'537fce18a5579c7df939fdc51761e39e'   as authToken  
 ,c2.SMS as SMSStatus,c2.Email as EmailStatus,c2.CALL as CALLStatus,c2.PartiesFirmCode,c2.location,
 Case When ((c2.location like '%birth%') or (c2.location like '%b''day%')) then 
 (CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, birth_date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)
 else
 CONVERT(VARCHAR, date, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, date)), 2)       
 End as FinalEventdate into #TempreminderFinal
 FROM casecard ca        
 INNER JOIN [card] c ON c.cardcode=ca.cardcode       
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno       
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode 
 where c.firmcode in (c2.PartiesFirmCode) and C2.[CALL]=1
 
 
Select  CaseNo,	[Event DateTime],	EventTitle,	Name	,phone1,	phone2,	Email,	FormCall,	accountSID,	authToken,	SMSStatus,	EmailStatus,	CALLStatus	,PartiesFirmCode,	location,	FinalEventdate
from #TempreminderFinal
WHERE    
(CONVERT(VARCHAR(5), FinalEventdate, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, FinalEventdate)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, FinalEventdate)), 2)  
= CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)    
 
End  
     
 --select Getdate()
GO
/****** Object:  StoredProcedure [dbo].[Sp_Reminder_SMS]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec Sp_Reminder_SMS 'Medical Appointment','Mr.{EventUserName} and Medical Appointment Is Date On {EventBody}.  {EventDatetime} For Location {EventLocation}  Invite For {EventTitle}'    
    
CREATE Proc [dbo].[Sp_Reminder_SMS]    
As        
Begin        
        
 SELECT ca.caseno AS [CaseNo],      
Case When ((c2.location like '%birth%') or (c2.location like '%b''day%')) then     
 (CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, birth_date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)    
 else    
 CONVERT(VARCHAR, c2.[date], 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, c2.[date])) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, c2.[date])), 2)           
 End

 as [Event DateTime],    
 ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],               
 LTRIM(RTRIM(c1.phone1)) as phone1,c1.phone2,LTRIM(RTRIM(c.email)) AS Email,LTRIM(RTRIM('+18182103230')) as FormCall     
 ,'ACaab6cd4373c0287b3b81cf957ba4f398' as accountSID,'537fce18a5579c7df939fdc51761e39e'   as authToken  ,  
 replace(replace(replace(c2.[event],    
 '{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),''))    
 ,'{EventBody}',c2.[event])  
     
 ,'{EventDatetime}',Case When ((c2.location like '%birth%') or (c2.location like '%b''day%')) then     
 (CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, birth_date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)    
 else    
 CONVERT(VARCHAR, c2.[date], 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, c2.[date])) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, c2.[date])), 2)           
 End)     
 AS EventTitle    
 ,c2.SMS as SMSStatus,c2.Email as EmailStatus,c2.CALL as CALLStatus,c2.PartiesFirmCode,c2.location, birth_date,   
 Case When ((c2.location like '%birth%') or (c2.location like '%b''day%')) then     
 (CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, birth_date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)    
 else    
 CONVERT(VARCHAR, c2.[date], 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, c2.[date])) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, c2.[date])), 2)           
 End as FinalEventdate into #TempreminderFinal    
  
 FROM casecard ca            
 INNER JOIN [card] c ON c.cardcode=ca.cardcode           
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno           
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode              
 where c.firmcode in (c2.PartiesFirmCode) and isnull(C2.[SMS],0)=1    
       
       
 Select  CaseNo, [Event DateTime], EventTitle, Name ,phone1, phone2, Email, FormCall, accountSID, authToken, SMSStatus, EmailStatus, CALLStatus ,PartiesFirmCode, location, FinalEventdate    
 from #TempreminderFinal    
 WHERE        
 (CONVERT(VARCHAR(5), FinalEventdate, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, FinalEventdate)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, FinalEventdate)), 2)      
 = CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)        
     
  
End
--Drop table #TempreminderFinal
GO
/****** Object:  StoredProcedure [dbo].[Sp_Reminder_Email]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_Reminder_Email]     
   
As      
Begin        
 SELECT ca.caseno AS [CaseNo],      
 CONVERT(VARCHAR, date, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, date)), 2)         
 as [Event DateTime]    
 ,replace(replace(replace(c2.event,'{EventUserName}',ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'')) ,'{EventBody}',C2.[event])  ,
 
'{EventDatetime}',CONVERT(VARCHAR, date, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, date)), 2)) AS EventTitle,     
 ISNULL(LTRIM(RTRIM(c.salutation)),'')+' '+ISNULL(LTRIM(RTRIM(c.first)),'')+' '+ISNULL(LTRIM(RTRIM(c.middle)),'')+' '+ISNULL(LTRIM(RTRIM(c.last)),'') AS [Name],             
 c1.phone1 as phone1,c1.phone2,c.email AS Email,'+917405372509' as FormCall ,'ACaab6cd4373c0287b3b81cf957ba4f398' as accountSID,'537fce18a5579c7df939fdc51761e39e'   as authToken    
 ,c2.SMS as SMSStatus,c2.Email as EmailStatus,c2.CALL as CALLStatus,c2.PartiesFirmCode,c2.location,  
 Case When ((c2.location like '%birth%') or (c2.location like '%b''day%')) then   
 (CONVERT(VARCHAR(5), birth_date, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, birth_date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, birth_date)), 2)  
 else  
 CONVERT(VARCHAR, date, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, date)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, date)), 2)         
 End as FinalEventdate,c2.location as [Subject] into #TempreminderFinal  
 FROM casecard ca          
 INNER JOIN [card] c ON c.cardcode=ca.cardcode         
 Right Outer JOIN cal1 c2 ON c2.caseno=ca.caseno         
 LEFT JOIN card2 c1 ON c1.firmcode =c.firmcode   
 where c.firmcode in (c2.PartiesFirmCode) and C2.[EMAIL]=1  
   
   
Select  CaseNo, [Event DateTime], EventTitle, Name ,phone1, phone2, Email, FormCall, accountSID, authToken, SMSStatus, EmailStatus, CALLStatus ,PartiesFirmCode, [Subject], FinalEventdate  
from #TempreminderFinal  
WHERE      
(CONVERT(VARCHAR(5), FinalEventdate, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, FinalEventdate)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, FinalEventdate)), 2)    
= CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)      
   
End    
       
 --select Getdate()
GO
/****** Object:  StoredProcedure [dbo].[SP_LRS_AddressBookDetails]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_LRS_AddressBookDetails]    
@AddDetailId int,    
@AddId int,                        
@CardCode Nvarchar(50)    
                  
As               
 begin    
     
     
 select * from tbl_LRS_AddressBookDetails    
     
  DELETE FROM tbl_LRS_AddressBookDetails WHERE AddId= @AddId AND CardCode=@CardCode    
         
  INSERT INTO tbl_LRS_AddressBookDetails    
  (    
    AddId,    
    CardCode    
  )    
  VALUES    
    (    
    @AddId,    
    @CardCode    
    )    
      
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LRS_AddressBook]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_LRS_AddressBook]
@AddId int,                    
@AddName varchar(100),                    
@Description varchar(100),                    
@Status bit
              
As           
	begin
		INSERT INTO tbl_LRS_AddressBook 
		(
				AddName,
				Description,
				Status
		)
		VALUES
				(
				@AddName,
				@Description,
				@Status
				)
	end
GO
/****** Object:  StoredProcedure [dbo].[Sp_IVRS_Reminder_promotional]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_IVRS_Reminder_promotional]                                                                                                   
As                                                              
Begin                                                                
                          
-----------------------------------------------------------------// New Code : 07/18/2016 //                          
                       
SELECT TOP 1 ROW_NUMBER() over(order by (select 1)) as [No],CONVERT(VARCHAR,AppointmentDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh,AppointmentDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,AppointmentDatetime)), 2)as [Event DateTime],
                
[Event] AS EventTitle,Phone,Email,TES.Twilio_CALL as FormCall,                      
TES.Twilio_AccountSid as accountSID,                      
TES.Twilio_AuthToken as authToken,                       
SMSStatus as SMSStatus, EmailStatus as EmailStatus, CallStatus as CALLStatus,                           
EventTitle as location,RID as EventID  ,TFD.MainEventID ,TFD.ClientKey ,    
RTRIM(ltrim(LTL.LanguageValue)) as LanguageValue           
                    
                         
FROM  tbl_FinalReminderDetails  TFD                       
INNER join tbl_Email_Setting TES On TES.ClientKey=TFD.ClientKey             
LEFT OUTER JOIN lst_TwilioLanguage LTL On LTL.LID=isnull(TFD.TwilioLang,6)                                   
WHERE                 
                                                     
(CONVERT(VARCHAR(5), EventDatetime, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, EventDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, EventDatetime)), 2)                             
= CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)                                                         
and isnull(CallStatus,0)=1 and isnull(EmailStatus,0)=999                       
                  
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_IVRS_Reminder_New_Test]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_IVRS_Reminder_New_Test]                                                                                               
As                                                          
Begin                                                            
                      
-----------------------------------------------------------------// New Code : 07/18/2016 //                      
                   
SELECT TOP 1 ROW_NUMBER() over(order by (select 1)) as [No],CONVERT(VARCHAR,AppointmentDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh,AppointmentDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,AppointmentDatetime)), 2)as [Event DateTime] 
,       
    
      
                 
[Event] AS EventTitle,Phone,Email,TES.Twilio_CALL as FormCall,                  
TES.Twilio_AccountSid as accountSID,                  
TES.Twilio_AuthToken as authToken,                   
SMSStatus as SMSStatus, EmailStatus as EmailStatus, CallStatus as CALLStatus,                       
EventTitle as location,RID as EventID  ,TFD.MainEventID ,TFD.ClientKey ,  
'es-MX' as LanguageValue  
--RTRIM(ltrim(LTL.LanguageValue)) as LanguageValue       
                
into #TempRecall                      
FROM  tbl_FinalReminderDetails  TFD                   
INNER join tbl_Email_Setting TES On TES.ClientKey=TFD.ClientKey         
LEFT OUTER JOIN lst_TwilioLanguage LTL On LTL.LID=isnull(TFD.TwilioLang,6)                               
WHERE             
                                                 
(CONVERT(VARCHAR(5), EventDatetime, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, EventDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, EventDatetime)), 2)                                                
    
    
= CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)                                                     
and   
isnull(CallStatus,0)=1  
                      
        
Declare @SP int,@EP int,@CountMainEventID int,@CountRID int,@ActualCount int  ,@ClientKey NVARCHAR(100)      
                          
set @SP =(select Min(No) from #TempRecall)                                                                                          
set @EP =(select Max(No) from #TempRecall)          
        
        
CREATE table #TempEventRecall (EventIDTemp int)         
        
While(@EP >= @SP)                                                                                          
Begin           
         
 set @CountMainEventID= (SELECT MainEventID from #TempRecall where [No]=@SP)        
 set @CountRID= (SELECT EventID from #TempRecall where [No]=@SP)        
 set @ClientKey =(SELECT ClientKey from #TempRecall where [No]=@SP)        
      
 set @ActualCount = (SELECT Count(*) as Count from tbl_FinalReminderDetails         
 Where MainEventID=@CountMainEventID and isnull(IsTwilioStatus,0) not in (5,0)  and ClientKey= @ClientKey)      
         
 if(@ActualCount > 0)        
 BEGIN        
 insert into  #TempEventRecall (EventIDTemp)  VALUES (@CountMainEventID)                     
    END             
                 
                 
     set @SP =@SP +1             
END           
        
DELETE From tbl_FinalReminderDetails where MainEventID in (select EventIDTemp FROM #TempEventRecall) and RID in (select EventID from  #TempRecall)        
        
select * from  #TempRecall  WHERE MainEventID  NOT in (select EventIDTemp FROM #TempEventRecall)        
        
        
        
        
                      
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_IVRS_Reminder_New]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_IVRS_Reminder_New]                                                                                                 
As                                                            
Begin                                                              
                        
-----------------------------------------------------------------// New Code : 07/18/2016 //                        
                     
SELECT TOP 1 ROW_NUMBER() over(order by (select 1)) as [No],
CONVERT(VARCHAR,AppointmentDatetime, 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh,AppointmentDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi,AppointmentDatetime)), 2)as [Event DateTime] 
,              
[Event] AS EventTitle,Phone,Email,TES.Twilio_CALL as FormCall,                    
TES.Twilio_AccountSid as accountSID,                    
TES.Twilio_AuthToken as authToken,                     
SMSStatus as SMSStatus, EmailStatus as EmailStatus, CallStatus as CALLStatus,                         
EventTitle as location,RID as EventID  ,TFD.MainEventID ,TFD.ClientKey ,  
RTRIM(ltrim(LTL.LanguageValue)) as LanguageValue         
                  
into #TempRecall                        
FROM  tbl_FinalReminderDetails  TFD                     
INNER join tbl_Email_Setting TES On TES.ClientKey=TFD.ClientKey           
LEFT OUTER JOIN lst_TwilioLanguage LTL On LTL.LID=isnull(TFD.TwilioLang,6)                                 
WHERE               
                                                   
(CONVERT(VARCHAR(5), EventDatetime, 101)+'/'+CAST(YEAR(Getdate()) AS VARCHAR(4))) + ' ' + CONVERT(VARCHAR, DATEPART(hh, EventDatetime)) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, EventDatetime)), 2)                                                
= CONVERT(VARCHAR, GetDate(), 101) + ' ' + CONVERT(VARCHAR, DATEPART(hh, GetDate())) + ':' + RIGHT('0' + CONVERT(VARCHAR, DATEPART(mi, GetDate())), 2)                                                       
and     
isnull(CallStatus,0)=1  
and ISNULL(TFD.EmailStatus,0)<>999                        
                        
          
Declare @SP int,@EP int,@CountMainEventID int,@CountRID int,@ActualCount int  ,@ClientKey NVARCHAR(100)        
                            
set @SP =(select Min(No) from #TempRecall)                                                                                            
set @EP =(select Max(No) from #TempRecall)            
          
          
CREATE table #TempEventRecall (EventIDTemp int)           
          
While(@EP >= @SP)                                                                                            
Begin             
           
	set @CountMainEventID= (SELECT MainEventID from #TempRecall where [No]=@SP)          
	set @CountRID= (SELECT EventID from #TempRecall where [No]=@SP)          
	set @ClientKey =(SELECT ClientKey from #TempRecall where [No]=@SP)          

	set @ActualCount = (SELECT Count(*) as Count from tbl_FinalReminderDetails           
	Where MainEventID=@CountMainEventID and isnull(IsTwilioStatus,0) not in (5,0)  and ClientKey= @ClientKey)        

	if(@ActualCount > 0)          
	BEGIN          
	insert into  #TempEventRecall (EventIDTemp)  VALUES (@CountMainEventID)                       
	END               
           
    set @SP =@SP +1               
END             
          
DELETE From tbl_FinalReminderDetails where MainEventID in (select EventIDTemp FROM #TempEventRecall) and RID in (select EventID from  #TempRecall)          
          
select * from  #TempRecall  WHERE MainEventID  NOT in (select EventIDTemp FROM #TempEventRecall)          
          
          
          
          
                        
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_IVRS_Reminder_EventSpeechGet]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sp_IVRS_Reminder_EventSpeechGet '26'                 
                                     
CREATE Proc [dbo].[Sp_IVRS_Reminder_EventSpeechGet]                                        
@EventID Nvarchar(100)                                 
As                                        
Begin                                          
                  
-----------------------------------------------------------------// New Code : 07/18/2016 , For Cloud //            
               
SELECT [Event] AS EventTitle,RID as EventID ,IsConfirmable  , RTRIM(ltrim(LTL.LanguageValue)) as LanguageValue  
FROM tbl_FinalReminderDetails TF  
LEFT JOIN lst_TwilioLanguage LTL On LTL.LID=isnull(TF.TwilioLang,6)                                   
WHERE RID = @EventID               
              
                              
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_updateSignature]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[Sp_updateSignature]
@CompanyLogo nvarchar(MAX),
@Signature nvarchar(MAX)
as
Begin

Update tbl_Email_Setting set CompanyLogo=@CompanyLogo,	Signature=@Signature

select 1 as Result

End
GO
/****** Object:  StoredProcedure [dbo].[Sp_UpdateIsstatus_Parties]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sp_UpdateIsstatus_Parties ''

CREATE Proc [dbo].[Sp_UpdateIsstatus_Parties]
@FirmCode Nvarchar(50),
@Status Int
as
begin

Update AddParties set isstatus=@Status where Firmcode= @FirmCode

select 1 as Result

End
GO
/****** Object:  StoredProcedure [dbo].[Sp_UpdateEmailSMSDetails]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sp_UpdateEmailSMSDetails 'reminder@solulab.com','lionking7','Lawyers Reminder System','25','mail.solulab.com','+917405372508','+18182103230','ACaab6cd4373c0287b3b81cf957ba4f398','537fce18a5579c7df939fdc51761e39e'

Create Proc [dbo].[Sp_UpdateEmailSMSDetails]
@EmailFrom nvarchar(50),
@EmailPassword nvarchar(50),
@EmailFromTitle nvarchar(50),
@EmailPort nvarchar(50),
@EmailSMTP nvarchar(50),
@Twilio_CALL nvarchar(50),
@Twilio_SMS nvarchar(50),
@Twilio_AccountSid nvarchar(500),
@Twilio_AuthToken nvarchar(500)
As
Begin
 
Update

tbl_Email_Setting

set

 EmailFrom=@EmailFrom, 
EmailPassword=@EmailPassword, 
EmailFromTitle=@EmailFromTitle, 
EmailPort=@EmailPort, 
EmailSMTP=@EmailSMTP, 
Twilio_CALL=@Twilio_CALL, 
Twilio_SMS=@Twilio_SMS, 
Twilio_AccountSid=@Twilio_AccountSid, 
Twilio_AuthToken=@Twilio_AuthToken
        
        
select 1 as Result        
End
GO
/****** Object:  StoredProcedure [dbo].[Sp_Update_reschedule]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec [Sp_Update_reschedule] '4'  
CREATE Proc [dbo].[Sp_Update_reschedule]    
@TimingID nvarchar(50)    
as    
Begin   
  
	TRUNCATE Table tbl_NextGenerateEventID     

	Declare @TimingSCOPEIdentity Nvarchar(100),@ClientKey Nvarchar(100)  

	set @TimingSCOPEIdentity =(SELECT EventFK from tbl_LRS_Reminder_Timing where TimingID=@TimingID  )  
	set @ClientKey =(select ProductKey from tbl_RS_Registration where RID =(SELECT CreatedUserFK from tbl_LRS_Reminder   
	where eventno=(SELECT EventFK from tbl_LRS_Reminder_Timing where TimingID=@TimingID)))  





	INSERT Into tbl_NextGenerateEventID                                
	select EventID from InsertReminderFinal(@TimingSCOPEIdentity,@ClientKey)     


	INSERT into tbl_FinalReminderDetails                                  
	select * from InsertReminderFinal(@TimingSCOPEIdentity,@ClientKey)   
    
   
    
select 1 As Result    
End
GO
/****** Object:  StoredProcedure [dbo].[SP_Update_Event]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Update_Event]                                                                    
@EventStartDate Datetime,                                                                    
@EventEndDate Datetime,                                                                    
@Event Nvarchar(MAX),                                                                    
@Location Nvarchar(MAX),                                                                    
@EventTitle nvarchar(MAX),                                                                 
@EventNo Nvarchar(50),                                                                
@CaseNoChange Nvarchar(50) ,                                                              
@SMS int,                                                                
@CALL int,                                                                
@EMAIL int,                                                  
@WhenSMS Nvarchar(500)=null ,                                                      
@WhenCALL Nvarchar(500)=null ,                                                      
@WhenEMAIL Nvarchar(500)=null,                                          
@EventHTML Nvarchar(MAX)=null,                                    
@ClientKey Nvarchar(200)  ,                          
@IsPrivate int,                          
@CreatedUserFK Nvarchar(50)  ,                        
@IsConfirmable int      ,                  
@AppointmentType Nvarchar(MAX)  ,                
@TwilioLang Nvarchar(MAX)                   
                                                            
                                                            
                                                               
As                                                                    
Begin                                                                    
                                                             
                                                                    
Declare @EventFull Nvarchar(Max)                                          
Declare @EventHTMLFull Nvarchar(Max),@CreatedUserFKNEW Nvarchar(500)                                                                  
set @EventFull =(Case When @EventTitle is null or @EventTitle ='' then @Location ELSE @EventTitle END)+':'+@Event                                         
set @EventHTMLFull =(Case When @EventTitle is null or @EventTitle ='' then @Location ELSE @EventTitle END) +':'+@EventHTML           
                                                                
set @CreatedUserFKNEW =(select RID from tbl_RS_Registration Where MacID =@CreatedUserFK)                        
                              
if(@CreatedUserFKNEW='' or @CreatedUserFKNEW is NULL)      
BEGIN      
set @CreatedUserFKNEW =@CreatedUserFK      
END      
      
                        
                                                                  
select ROW_NUMBER() over(order by (select 1)) as No,firmcode,Name,LName,Caseno into #TempPaties from  AddParties   where  isnull(isstatus,0)=1   and MACID=@CreatedUserFK                                                                  
                                                                    
                                                  
Declare @FName Nvarchar(500),@LName Nvarchar(500),@firmcode nvarchar(50),@SP int,@EP int,@SPF int,@EPF int,@Caseno Nvarchar(50),@EventParties Nvarchar(50),@EventPartiesFInal Nvarchar(50)                                                            
,@TimingSCOPEIdentity nvarchar(50),@SMSTIME Nvarchar(100),@CALLTIME Nvarchar(100),@EMAILTIME Nvarchar(100)                                       
                                                          
set @SP =(select Min(No) from #TempPaties)                                                                      
set @EP =(select Max(No) from #TempPaties)                                                             
                                 
TRUNCATE Table tbl_NextGenerateEventID                                                           
                                                         
 While(@EP >= @SP)                                                                      
 Begin                                  
                                                                        
  set @FName = (select top 1 Name from #TempPaties where No=@SP)                                                     
  set @LName = (select top 1  LName from #TempPaties where No=@SP)                                                                     
  set @firmcode = (select top 1  firmcode from #TempPaties where No=@SP)                                                    
  set @Caseno = (select top 1  CaseNo from #TempPaties where No=@SP)                                                       
                                                                      
                                                               
                                                                      
 if Not Exists (select * from tbl_LRS_Reminder c2 where c2.first=@FName and c2.last=@LName and c2.CardcodeFK=@firmcode and EventParties=@CaseNoChange)                                                                    
 Begin                                                                    
                                      Insert Into tbl_LRS_Reminder                           
   (                          
  CardcodeFK, date, event, first, last, location, SMS, CALL, EMAIL, EventParties, CreatedDate, ModifiedDate, Caseno, whenSMS, whenCALL, whenEMAIL,                           
                      LastUpdatedStatusFK, EventHTML, IsPrivate,CreatedUserFK,IsConfirmable,AppointmentType,TwilioLang  )                          
 VALUES                          
 (                          
  @firmcode,@EventStartDate,@EventFull,@FName,@LName,@Location,@SMS,@CALL,@EMAIL,isnull(@EventPartiesFInal,'RS'),Getdate(),Getdate(),@Caseno,@WhenSMS,@WhenCALL,@WhenEMAIL ,NULL,@EventHTMLFull,                              
  @IsPrivate,  @CreatedUserFKNEW,@IsConfirmable,@AppointmentType,@TwilioLang)                                              
                                                    
  set @TimingSCOPEIdentity = SCOPE_IDENTITY()                                     
                                  
                                  
                                      
                                                      
                                                       
  Update tbl_LRS_Reminder set  EventParties = (select  top 1  EventParties from tbl_LRS_Reminder where  eventno=@EventNo )                                                      
  where EventParties ='RS'                                             
                                      
                                      
                                                   
 --//*************** Other Table Entry --SMS*******************************************************************//                                                  
                                                    
  set  @SMSTIME =(select top 1 case when whenSMS='' THEN '-1' ELSE whenSMS end as  whenSMS from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                                     
                                                    
  select * into #TempSMS from dbo.SplitByComma(@SMSTIME)                                                    
                                                    
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'SMS'as RTYPE into #TempSMSFINAL                                             
  from #TempSMS T                                            
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1         
    where T.Column1 <> -1                                                     
                                                    
  Declare @SMSStart nvarchar(10),@SMSEnd nvarchar(10),@SMSWhen nvarchar(100),@SMSTYPE Nvarchar(100)                                                 
                                               
  set @SMSStart =(select MIN(NO) from #TempSMSFINAL)                                                  
  set @SMSEnd =(select MAX(NO) from #TempSMSFINAL)                                                  
                                
 While(@SMSEnd >= @SMSStart)                                                  
 Begin                                                  
  set @SMSWhen =(select top 1  smsTime from #TempSMSFINAL where No=@SMSStart)                                                  
  set @SMSTYPE =(select top 1  RTYPE from #TempSMSFINAL where No=@SMSStart)                                                  
                                                    
                                                   
  Insert Into tbl_LRS_Reminder_Timing                                                
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@SMSWhen,@EventStartDate)),@SMSWhen,@SMSTYPE,0,Getdate(),Getdate(),0                                           
                                                   
  set @SMSStart =@SMSStart+1                                
                                                    
 END                                                   
                                                   
--//*************** Other Table Entry --CALL*******************************************************************//                                                  
                                                    
  set  @CALLTIME =(select top 1  case when whenCALL='' THEN '-1' ELSE whenCALL end as whenCALL from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                                     
                                                    
  select * into #TempCALL from dbo.SplitByComma(@CALLTIME)                                               
                                                    
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'CALL'as RTYPE into #TempCALLFINAL                                                   
  from #TempCALL T                                                  
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1          
    where T.Column1 <> -1                                                    
                                                    
  Declare @CALLStart nvarchar(10),@CALLEnd nvarchar(10),@CALLWhen nvarchar(100),@CALLTYPE Nvarchar(100)                                                  
                                                    
  set @CALLStart =(select MIN(NO) from #TempCALLFINAL)                                                  
  set @CALLEnd =(select MAX(NO) from #TempCALLFINAL)                                                  
                                                     
 While(@CALLEnd >= @CALLStart)                                      
 Begin                                                  
  set @CALLWhen =(select top 1  smsTime from #TempCALLFINAL where No=@CALLStart)                                                  
  set @CALLTYPE =(select top 1  RTYPE from #TempCALLFINAL where No=@CALLStart)                                              
                                                    
                                                   
  Insert Into tbl_LRS_Reminder_Timing                                                  
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@CALLWhen,@EventStartDate)),@CALLWhen,@CALLTYPE,0,Getdate(),Getdate(),0                                            
                                                   
  set @CALLStart =@CALLStart+1                                                  
 END                                                   
--//*************** Other Table Entry --EMAIL*******************************************************************//                                                  
                                                  
  set  @EMAILTIME =(select top 1  case when whenEMAIL='' THEN '-1' ELSE whenEMAIL end as whenEMAIL  from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                               
                                                    
  select * into #TempEMAIL from dbo.SplitByComma(@EMAILTIME)                                                    
        
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'EMAIL'as RTYPE into #TempEMAILFINAL                                                   
  from #TempEMAIL T                                                  
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1         
    where T.Column1 <> -1                                                     
                                                    
  Declare @EMAILStart nvarchar(10),@EMAILEnd nvarchar(10),@EMAILWhen nvarchar(100),@EMAILTYPE Nvarchar(100)                                                  
                                                    
  set @EMAILStart =(select MIN(NO) from #TempEMAILFINAL)                                                  
  set @EMAILEnd =(select MAX(NO) from #TempEMAILFINAL)                                         
                                                     
 While(@EMAILEnd >= @EMAILStart)                                                  
 Begin                                                  
  set @EMAILWhen =(select top 1  smsTime from #TempEMAILFINAL where No=@EMAILStart)                                                  
  set @EMAILTYPE =(select top 1  RTYPE from #TempEMAILFINAL where No=@EMAILStart)                                                  
                                                    
                                                   
  Insert Into tbl_LRS_Reminder_Timing                                                  
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@EMAILWhen,@EventStartDate)),@EMAILWhen,@EMAILTYPE,0,Getdate(),Getdate(),0                                                  
                                                   
  set @EMAILStart =@EMAILStart+1                                                  
 END                                                      
                                                  
                                                  
 --//Insert Event In Cloud Mapping Table                                    
                              
                                 
                               
                                 
 INSERT Into tbl_NextGenerateEventID                                  
 select EventID from InsertReminderFinal(@TimingSCOPEIdentity,@ClientKey)                                   
                                    
                                    
 INSERT into tbl_FinalReminderDetails                                    
 select * from InsertReminderFinal(@TimingSCOPEIdentity,@ClientKey)                                                                      
                                                                  
 End                                            
                                             
 --//--------------------------UPDate Start-------------------                              
 Else                                                                
 Begin                                                                
                                                          
   update tbl_LRS_Reminder set date=@EventStartDate,event= @EventFull,location=@Location,ModifiedDate=Getdate(),                                                               
   SMS=@SMS,CALL=@CALL,Email=@EMAIL,whenSMS=@WhenSMS,whenCALL=@WhenCALL, whenEMAIL=@WhenEMAIL,   LastUpdatedStatusFK=0,                                          
   EventHTML = @EventHTMLFull ,IsPrivate=@IsPrivate,CreatedUserFK=@CreatedUserFKNEW,IsConfirmable=@IsConfirmable,                  
   AppointmentType=@AppointmentType ,TwilioLang=@TwilioLang                                                     
   where eventno=@EventNo                                                               
                                                        
      --//*************** Delete Exists Reminder*******************************************************************//                                                         
                                                  
   update tbl_LRS_Reminder_Timing set Isactive=1  where EventFK=@EventNo       
                                                     
--//*************** Other Table Update --SMS*******************************************************************//                                                  
                                                    
  set  @SMSTIME =(select top 1  case when whenSMS='' THEN '-1' ELSE whenSMS end as  whenSMS from tbl_LRS_Reminder  where eventno=@EventNo)                                                     
                                                    
  select * into #TempSMSU from dbo.SplitByComma(@SMSTIME)                                                    
                                                    
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'SMS'as RTYPE into #TempSMSFINALU                                                   
  from #TempSMSU T                                                  
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1         
    where T.Column1 <> -1          
                                                       
                                                    
  Declare @SMSStartU nvarchar(10),@SMSEndU nvarchar(10),@SMSWhenU nvarchar(100),@SMSTYPEU Nvarchar(100)                                                  
                                                    
  set @SMSStartU =(select MIN(NO) from #TempSMSFINALU)                                                  
  set @SMSEndU =(select MAX(NO) from #TempSMSFINALU)                                                  
                                                     
 While(@SMSEndU >= @SMSStartU)                                                  
 Begin                                                  
  set @SMSWhenU =(select top 1  smsTime from #TempSMSFINALU where No=@SMSStartU)                                                  
set @SMSTYPEU =(select top 1  RTYPE from #TempSMSFINALU where No=@SMSStartU)                                                  
                                                    
                           
  Insert Into tbl_LRS_Reminder_Timing                        
  Select @EventNo,(select dbo.DaysNametoDate(@SMSWhenU,@EventStartDate)),@SMSWhenU,@SMSTYPEU,0,Getdate(),Getdate(),0                                                  
                                                   
  set @SMSStartU =@SMSStartU+1                                                  
 END                                                   
                                                   
--//*************** Other Table Entry --CALL*******************************************************************//                                                  
                                                    
  set @CALLTIME =(select top 1  case when whenCALL='' THEN '-1' ELSE whenCALL end as whenCALL from tbl_LRS_Reminder  where eventno=@EventNo)                                                     
  select * into #TempCALLU from dbo.SplitByComma(@CALLTIME)                                                    
                                                    
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'CALL'as RTYPE into #TempCALLFINALU                                                   
  from #TempCALLU T                                                  
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1          
    where T.Column1 <> -1          
                                                      
  Declare @CALLStartU nvarchar(10),@CALLEndU nvarchar(10),@CALLWhenU nvarchar(100),@CALLTYPEU Nvarchar(100)                                                    
  set @CALLStartU =(select MIN(NO) from #TempCALLFINALU)                                 
  set @CALLEndU =(select MAX(NO) from #TempCALLFINALU)            
                                                   
 While(@CALLEndU >= @CALLStartU)                                                  
 Begin                                                  
  set @CALLWhenU =(select top 1  smsTime from #TempCALLFINALU where No=@CALLStartU)                                                  
  set @CALLTYPEU =(select top 1  RTYPE from #TempCALLFINALU where No=@CALLStartU)                                                    
                
  Insert Into tbl_LRS_Reminder_Timing                                                  
Select @EventNo,(select dbo.DaysNametoDate(@CALLWhenU,@EventStartDate)),@CALLWhenU,@CALLTYPEU,0,Getdate(),Getdate() ,0                                                 
                                                  
  set @CALLStartU =@CALLStartU+1                                                  
 END                                                   
--//*************** Other Table Entry --EMAIL*******************************************************************//                                                  
                                                  
  set  @EMAILTIME =(select  top 1 case when whenEMAIL='' THEN '-1' ELSE whenEMAIL end as whenEMAIL  from tbl_LRS_Reminder  where eventno=@EventNo)                                                       
  select * into #TempEMAILU from dbo.SplitByComma(@EMAILTIME)                                                      
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'EMAIL'as RTYPE into #TempEMAILFINALU                                                   
  from #TempEMAILU T                                                  
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1             
    where T.Column1 <> -1                                        
                                                      
  Declare @EMAILStartU nvarchar(10),@EMAILEndU nvarchar(10),@EMAILWhenU nvarchar(100),@EMAILTYPEU Nvarchar(100)                                                  
                                                    
  set @EMAILStartU =(select MIN(NO) from #TempEMAILFINALU)                                                  
  set @EMAILEndU =(select MAX(NO) from #TempEMAILFINALU)                                                  
  While(@EMAILEndU >= @EMAILStartU)                                            
 Begin                                                  
  set @EMAILWhenU =(select top 1  smsTime from #TempEMAILFINALU where No=@EMAILStartU)                                                  
  set @EMAILTYPEU =(select top 1  RTYPE from #TempEMAILFINALU where No=@EMAILStartU)                                                  
                                                    
                                                   
  Insert Into tbl_LRS_Reminder_Timing                                                  
  Select @EventNo,(select dbo.DaysNametoDate(@EMAILWhenU,@EventStartDate)),@EMAILWhenU,@EMAILTYPEU,0,Getdate(),Getdate(),0                                                  
                                                   
  set @EMAILStartU =@EMAILStartU+1                                   
  END                                                     
                                       
                                      
           
                                              
                                              
Drop Table #TempSMSU                                             
Drop Table #TempCALLU                                             
Drop Table #TempEMAILU                                                
Drop Table #TempSMSFINALU                                              
Drop Table #TempCALLFINALU                                             
Drop Table #TempEMAILFINALU                                             
                                                           
                                    
--//*************** END *******************************************************************//                                                    
                                     
                                     
                                   
                                     
                                               
 End                                   
                                 
                                 
--//Insert Event In Cloud Mapping Table                                    
                                    
 If exists (select * from tbl_FinalReminderDetails where MainEventID in (@EventNo))                                    
 BEGIN                                     
                                  
  Delete From tbl_FinalReminderDetails where MainEventID in (@EventNo)    ---//First Delete if Exists                                 
                                  
                                 
  INSERT Into tbl_NextGenerateEventID                                  
  select EventID from InsertReminderFinal(@EventNo,@ClientKey)                                   
                                
  INSERT into tbl_FinalReminderDetails                                    
  select * from InsertReminderFinal(@EventNo,@ClientKey)                                     
                                                                     
    END                                    
    ELSE                                    
    BEGIN                                    
                                   
                               
  INSERT Into tbl_NextGenerateEventID                                  
  select EventID from InsertReminderFinal(@EventNo,@ClientKey)                                   
                                         
  INSERT into tbl_FinalReminderDetails                                    
  select * from InsertReminderFinal(@EventNo,@ClientKey)                                                
                         
    END                                     
                                    
                                      
                                
                                    
                                                             
  set @SP =@SP +1                                                                    
                                                                    
 END                                                                    
                                        
                                                                     
                                                               
Select 1 as Result                                                                  
                                                          
                                                                  
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Insesrt_Event_API]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Insesrt_Event_API]                                                                            
@EventStartDate Datetime,                                                                            
@EventEndDate Datetime,                                                                            
@Event Nvarchar(MAX),                                                                            
@Location Nvarchar(MAX),                                                                            
@EventTitle Nvarchar(MAX) ,                                                                      
@SMS int,                                                                      
@CALL int,                                                                      
@EMAIL int  ,                                                                    
@WhenSMS Nvarchar(500)=null ,                                                                  
@WhenCALL Nvarchar(500)=null ,                                                                  
@WhenEMAIL Nvarchar(500)=null,                                                        
@EventHTML Nvarchar(MAX)=null,                                                    
@ClientKey Nvarchar(200),                                          
@IsPrivate int,                                          
@CreatedUserFK Nvarchar(50)  ,                                        
@IsConfirmable int,                                    
@AppointmentType Nvarchar(MAX)    ,                                  
@TwilioLang Nvarchar(MAX)                   
                
                                                    
                                                              
                                                                   
                                                                      
As                                                                            
Begin                                                                            
                                                                            
Declare @EventFull Nvarchar(Max)                                                                         
Declare @EventHTMLFull Nvarchar(Max),@CreatedUserFKNEW Nvarchar(500)                                                                                
set @EventFull = LTRIM(RTRIM (Case When @EventTitle is null or @EventTitle ='' then @Location ELSE @EventTitle END))+':'+LTRIM(RTRIM (@Event))                                                                  
set @EventHTMLFull =LTRIM(RTRIM (Case When @EventTitle is null or @EventTitle ='' then @Location ELSE @EventTitle END)) +':'+LTRIM(RTRIM (@EventHTML))                                                   
                                          
                                          
set @CreatedUserFKNEW =(select RID from tbl_RS_Registration Where MacID =@CreatedUserFK)                                          
                                                
if(@CreatedUserFKNEW='' or @CreatedUserFKNEW is NULL)                        
BEGIN                        
set @CreatedUserFKNEW =@CreatedUserFK                        
END                        
                        
                        
                                               
                                                                            
select ROW_NUMBER() over(order by (select 1)) as No,firmcode,Name,LName,Caseno into #TempPaties from  AddParties where  isnull(isstatus,0)=1   and MACID=@CreatedUserFK                                                                           
                                                                       
Declare @FName Nvarchar(500),@LName Nvarchar(500),@firmcode nvarchar(50),@SP int,@EP int ,@EventParties Nvarchar(50),@Caseno Nvarchar(50)                  
,@EventPartiesFInal Nvarchar(50),                                                                  
@TimingSCOPEIdentity nvarchar(50),@SMSTIME Nvarchar(100),@CALLTIME Nvarchar(100),@EMAILTIME Nvarchar(100)        
                                                                  
set @SP =(select Min(No) from #TempPaties)                                                                              
set @EP =(select Max(No) from #TempPaties)                                                                     
set @EventParties = ''                                                
                                            
 --TRUNCATE Table tbl_NextGenerateEventID                                                                          
                                     
 While(@EP >= @SP)                                                                              
 Begin                                                                            
                                       
  set @FName = (select Name from #TempPaties where No=@SP)                                                                             
  set @LName = (select LName from #TempPaties where No=@SP)                                                                             
  set @firmcode = (select firmcode from #TempPaties where No=@SP)                                                                             
  set @Caseno = (select CaseNo from #TempPaties where No=@SP)                                                                             
                                                                              
                                                                                
  Insert Into tbl_LRS_Reminder                                          
(                                          
  CardcodeFK, date, event, first, last, location, SMS, CALL, EMAIL, EventParties, CreatedDate, ModifiedDate, Caseno, whenSMS, whenCALL, whenEMAIL,                                           
                      LastUpdatedStatusFK, EventHTML, IsPrivate,CreatedUserFK,IsConfirmable,AppointmentType,TwilioLang  )                                          
  VALUES                                                                              
(@firmcode,@EventStartDate,@EventFull,@FName,@LName,@Location,@SMS,@CALL,@EMAIL,isnull(@EventPartiesFInal,'RS'),Getdate(),Getdate(),@Caseno ,@WhenSMS,                                    
 @WhenCALL,@WhenEMAIL,NULL,@EventHTMLFull,@IsPrivate,@CreatedUserFKNEW,@IsConfirmable,@AppointmentType,@TwilioLang)                                          
                                                                    
  set @TimingSCOPEIdentity = SCOPE_IDENTITY()                                                              
                                                                       
  IF(@EventParties = '')                                                                  
  Begin                                                                       
  set @EventParties = SCOPE_IDENTITY()                                                                 
  set @EventPartiesFInal = @EventParties                                                           
  END                                                                 
                                                                
 Update tbl_LRS_Reminder set  EventParties =@EventPartiesFInal  where EventParties = 'RS'                                                                 
                                                                
  --//*************** Other Table Entry --SMS*******************************************************************//                                                              
  --declare @SMSTIME Nvarchar(100)                                                                     
  set  @SMSTIME =(select top 1 case when whenSMS='' THEN '-1' ELSE whenSMS end as  whenSMS from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                                                 
                                                            
  select * into #TempSMS from dbo.SplitByComma(@SMSTIME)                                                                
                 
--drop table      #TempSMS                          
--drop table      #TempSMSFINAL                          
                                                           
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'SMS'as RTYPE into #TempSMSFINAL                                                               
  from #TempSMS T                                                         
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1                                     
  where T.Column1 <> -1                          
                                                                 
  Declare @SMSStart nvarchar(10),@SMSEnd nvarchar(10),@SMSWhen nvarchar(100),@SMSTYPE Nvarchar(100)                                                              
                                                                
  set @SMSStart =(select MIN(NO) from #TempSMSFINAL)                                                              
  set @SMSEnd =(select MAX(NO) from #TempSMSFINAL)                                                              
                            
 While(@SMSEnd >= @SMSStart)                                                              
 Begin                                                              
 set @SMSWhen =(select top 1 smsTime from #TempSMSFINAL where No=@SMSStart)                                                              
  set @SMSTYPE =(select top 1 RTYPE from #TempSMSFINAL where No=@SMSStart)                                                              
                                                       
                                                          
  Insert Into tbl_LRS_Reminder_Timing                                                              
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@SMSWhen,@EventStartDate)),@SMSWhen,@SMSTYPE,0,Getdate(),Getdate(),0                                                              
                                                               
  set @SMSStart =@SMSStart+1                                                              
 END                                                               
                                                               
--//*************** Other Table Entry --CALL*******************************************************************//                                                              
                                                                
  set  @CALLTIME =(select top 1 case when whenCALL='' THEN '-1' ELSE whenCALL end as whenCALL from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                                                 
                                                                
  select * into #TempCALL from dbo.SplitByComma(@CALLTIME)                                                                
                                                                
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'CALL'as RTYPE into #TempCALLFINAL                                                               
  from #TempCALL T                                                              
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1                            
  where T.Column1 <> -1                                                              
                                                                
  Declare @CALLStart nvarchar(10),@CALLEnd nvarchar(10),@CALLWhen nvarchar(100),@CALLTYPE Nvarchar(100)                                                              
                                                                
  set @CALLStart =(select MIN(NO) from #TempCALLFINAL)                                                              
  set @CALLEnd =(select MAX(NO) from #TempCALLFINAL)                                                              
                                                                 
 While(@CALLEnd >= @CALLStart)                                                              
 Begin                                                              
  set @CALLWhen =(select top 1 smsTime from #TempCALLFINAL where No=@CALLStart)                 
  set @CALLTYPE =(select top 1 RTYPE from #TempCALLFINAL where No=@CALLStart)                                                              
                                                                
                                                               
  Insert Into tbl_LRS_Reminder_Timing                                                              
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@CALLWhen,@EventStartDate)),@CALLWhen,@CALLTYPE,0,Getdate(),Getdate(),0                                   
                                                               
  set @CALLStart =@CALLStart+1                                                              
 END                                                               
--//*************** Other Table Entry --EMAIL*******************************************************************//                                                              
                                                              
  set  @EMAILTIME =(select top 1 case when whenEMAIL='' THEN '-1' ELSE whenEMAIL end as whenEMAIL  from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                                         
                                                                
  select * into #TempEMAIL from dbo.SplitByComma(@EMAILTIME)                        
                                                                
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'EMAIL'as RTYPE into #TempEMAILFINAL                                                               
  from #TempEMAIL T                                                              
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1                           
  where T.Column1 <> -1                                                               
                                                                
  Declare @EMAILStart nvarchar(10),@EMAILEnd nvarchar(10),@EMAILWhen nvarchar(100),@EMAILTYPE Nvarchar(100)                                                                                      
  set @EMAILStart =(select MIN(NO) from #TempEMAILFINAL)                                          
  set @EMAILEnd =(select MAX(NO) from #TempEMAILFINAL)                                                              
                        
 While(@EMAILEnd >= @EMAILStart)                                                              
 Begin                                                              
  set @EMAILWhen =(select top 1 smsTime from #TempEMAILFINAL where No=@EMAILStart)                                                              
  set @EMAILTYPE =(select top 1 RTYPE from #TempEMAILFINAL where No=@EMAILStart)                                                              
                                                                
                                                               
  Insert Into tbl_LRS_Reminder_Timing                                                              
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@EMAILWhen,@EventStartDate)),@EMAILWhen,@EMAILTYPE,0,Getdate(),Getdate(),0                                                              
                                                           
  set @EMAILStart =@EMAILStart+1                                                              
 END                                                                
                                                                    
      Drop Table #TempSMS                                                 
      Drop Table #TempCALL                                                         
      Drop Table #TempEMAIL                                                        
      Drop Table #TempSMSFINAL                                                      
      Drop Table #TempCALLFINAL                                                          
      Drop Table #TempEMAILFINAL                                                         
                                                    
                
                                                    
      --//Insert Event In Cloud Mapping Table                                                    
                                              
                                              
                                              
 INSERT Into tbl_NextGenerateEventID                                                  
 select EventID from InsertReminderFinal(@TimingSCOPEIdentity,@ClientKey)                                                    
                                                   
                                                   
 INSERT into tbl_FinalReminderDetails                                                    
 select * from InsertReminderFinal(@TimingSCOPEIdentity,@ClientKey)           
         
--//UPdate Event In Cloud Mapping Table For API server                   
        
Declare @TemplateIDAPI NVARCHAR(50),@SMSTemplateAPIUPdate NVARCHAR(MAX),@SMSAppointmentType NVARCHAR(MAX),@FinalSMSBodyReady NVARCHAR(MAX)        
        
set @TemplateIDAPI=(select ID from tbl_Apptoto_AddSmsEmail where    
 ltrim(RTRIM(Name))=(select ltrim(RTRIM(location)) from tbl_LRS_Reminder where eventno=@TimingSCOPEIdentity)       
 and     
 ltrim(RTRIM(Purpose))=(select ltrim(RTRIM(AppointmentType)) from tbl_LRS_Reminder where eventno=@TimingSCOPEIdentity))      
     
set @SMSTemplateAPIUPdate =(SELECT EmailSubject from tbl_Apptoto_AddSmsEmail where ID in (select TID from tbl_Medical_RoomLink where RoomNo=(select RoomNo from tbl_Medical_RoomLink where TID=@TemplateIDAPI)) and TwilioLang=6)        
set @SMSAppointmentType=(SELECT Purpose from tbl_Apptoto_AddSmsEmail where ID in (select TID from tbl_Medical_RoomLink where RoomNo=(select RoomNo from tbl_Medical_RoomLink where TID=@TemplateIDAPI)) and TwilioLang=6)        
        
set @FinalSMSBodyReady= (select [dbo].[TempleteVariableSet_API](@TimingSCOPEIdentity,@SMSTemplateAPIUPdate,@SMSAppointmentType))        
        
        
update tbl_FinalReminderDetails set Event=@FinalSMSBodyReady  where MainEventID=@TimingSCOPEIdentity and SMSStatus=1         
                                    
                                                                            
set @SP =@SP +1                                                                
                                                           
End                                                                            
                                    
                                            
                                                  
--------// Notification Set Before Appointment                                                       
                                                      
Declare                                                       
@EmailWhenNotification Nvarchar(50),        
@SMSWhenNotification Nvarchar(50),                                                      
@FullName Nvarchar(100),                                                      
@EventName Nvarchar(500),                                                      
@EventDt Nvarchar(50),                                                      
@Phone Nvarchar(50),                    
@EmailNoti Nvarchar(50),                                                      
@EventTitleNoti Nvarchar(500),                                                      
@MINPHONE Nvarchar(50),                                                      
@MAXPHONE Nvarchar(50),                  
@PhoneChange Nvarchar(50)                                                        
                                   
                                                       
                                                                     
Set @EmailWhenNotification =(select top 1 case when EmailWhen='' then NULL Else EmailWhen end  from tbl_LRS_NotificationSetting where isnull(Isactive,1)=0)        
Set @SMSWhenNotification=(select top 1 case when SMSWhen='' then NULL Else SMSWhen end from tbl_LRS_NotificationSetting where isnull(Isactive,1)=0)                                                    
set @FullName = (select top 1 LR.first+' '+LR.last as FullName from tbl_LRS_Reminder LR where LR.Eventno=@TimingSCOPEIdentity)                                                                 
set @EventName = (select top 1 Event as [EventTitle] from tbl_LRS_Reminder LR where LR.Eventno=@TimingSCOPEIdentity)                                                            
                                                      
select ROW_NUMBER() over(order by (select 1)) as [No],Column1 as EventTitle into #Temp from dbo.SplitByJEM(@EventName)                                                        
                                              
set @EventTitleNoti  =(select top 1 EventTitle from #Temp where [No]=1)                                                       
set @Phone =(select top 1 SMSSend  from tbl_LRS_NotificationSetting)                                                        
                                                        
select ROW_NUMBER() over(order by (select 1))as [No],Column1 as Phone into #TempPhone from dbo.SplitByComma(@Phone)                                                        
                                                        
                                                        
                                                        
set @MINPHONE =(select Min(No) from #TempPhone)                                                                                
set @MAXPHONE =(select Max(No) from #TempPhone)                                                      
set @EmailNoti = (select top 1 EmailSend from tbl_LRS_NotificationSetting)                                                         
set @EventDt= (select dbo.DaysNametoDate(@EmailWhenNotification,@EventStartDate))                                                      
                                                      
                                                      
                                                      
                                                       
                                                      
if(@EmailWhenNotification <> '' and @EmailWhenNotification is not Null and @Email <> '')                                                                    
Begin                                                
                                                                    
insert into tbl_Notification_TimingSetting                                                        
select @EventDt,'You have an upcoming appointment at @'+' '+Convert(varchar,@EventStartDate),@Phone,@EmailNoti,                                                      
'Upcoming appointment: '+' '+@EventTitleNoti+' @ '+Convert(varchar,@EventStartDate),0,getdate(),getdate(),'EMAIL'                                                      
                                                              
End                                                        
While(@MAXPHONE >= @MINPHONE)                                                                                
Begin                                                         
                                                      
 set @PhoneChange = (select Phone from #TempPhone where No=@MINPHONE)                          
                                                                  
 if(@SMSWhenNotification <> '' and @SMSWhenNotification is not Null and @Phone <> '')                                      
 Begin                                                                    
                                                         
 insert into tbl_Notification_TimingSetting                                    
 Select @EventDt                                                      
 ,'You have an upcoming appointment at @'+' '+Convert(varchar,@EventStartDate),                                            
 @PhoneChange,@EmailNoti,'Upcoming appointment: '+@EventTitleNoti+' @ '+Convert(varchar,@EventStartDate),0,Getdate(), Getdate() ,'SMS'                                                                  
                                                       
                       
                                                         
 END                                                      
                                                       
 set @MINPHONE =@MINPHONE+1                                                                  
End                                                            
                                                                          
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Insesrt_Event_A1LAW]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Insesrt_Event_A1LAW]                                                                  
@EventStartDate Datetime,                                                                  
@EventEndDate Datetime,                                                                  
@Event Nvarchar(MAX),                                                                  
@Location Nvarchar(MAX),                                                                  
@EventTitle Nvarchar(MAX) ,                                                            
@SMS int,                                                            
@CALL int,                                                            
@EMAIL int  ,                                                          
@WhenSMS Nvarchar(500)=null ,                                                        
@WhenCALL Nvarchar(500)=null ,                                                        
@WhenEMAIL Nvarchar(500)=null,                                              
@EventHTML Nvarchar(MAX)=null,                                          
@ClientKey Nvarchar(200),                                
@IsPrivate int,                                
@CreatedUserFK Nvarchar(50)  ,                              
@IsConfirmable int,                          
@AppointmentType Nvarchar(MAX)    ,                        
@TwilioLang Nvarchar(MAX) ,        
@Phone Nvarchar(200),        
@EmailID Nvarchar(200),        
@first Nvarchar(200),        
@last Nvarchar(200),        
@EventPartiesLocation Nvarchar(MAX)=null                               
                                          
                                                    
                                                         
                                                            
As                                                                  
Begin                                                                  
                                                                  
Declare @EventFull Nvarchar(Max)                                                               
Declare @EventHTMLFull Nvarchar(Max),@CreatedUserFKNEW Nvarchar(500)                                                                      
set @EventFull = LTRIM(RTRIM (Case When @EventTitle is null or @EventTitle ='' then @Location ELSE @EventTitle END))+':'+LTRIM(RTRIM (@Event))                                                        
set @EventHTMLFull =LTRIM(RTRIM (Case When @EventTitle is null or @EventTitle ='' then @Location ELSE @EventTitle END)) +':'+LTRIM(RTRIM (@EventHTML))                                         
                                
                                
set @CreatedUserFKNEW =(select RID from tbl_RS_Registration Where MacID =@CreatedUserFK)                                
                                      
if(@CreatedUserFKNEW='' or @CreatedUserFKNEW is NULL)              
BEGIN              
set @CreatedUserFKNEW =@CreatedUserFK              
END              
              
              
                                     
                                                                  
select ROW_NUMBER() over(order by (select 1)) as No,firmcode,Name,LName,Caseno into #TempPaties from  AddParties where  isnull(isstatus,0)=1                                                          
                                                  
Declare @FName Nvarchar(500),@LName Nvarchar(500),@firmcode nvarchar(50),@SP int,@EP int ,@EventParties Nvarchar(50),@Caseno Nvarchar(50),@EventPartiesFInal Nvarchar(50),                                                        
@TimingSCOPEIdentity nvarchar(50),@SMSTIME Nvarchar(100),@CALLTIME Nvarchar(100),@EMAILTIME Nvarchar(100)                                                    
                                                        
set @SP =(select Min(No) from #TempPaties)                                                                    
set @EP =(select Max(No) from #TempPaties)                                                           
set @EventParties = ''                  
                                  
 TRUNCATE Table tbl_NextGenerateEventID                  
                                                               
 While(@EP >= @SP)                                         
 Begin                                                                  
                                     
  set @FName = (select Name from #TempPaties where No=@SP)                                                                   
  set @LName = (select LName from #TempPaties where No=@SP)                                                                   
  set @firmcode = (select firmcode from #TempPaties where No=@SP)                                                                   
  set @Caseno = (select CaseNo from #TempPaties where No=@SP)                                                                   
                                                                    
                                                                      
  Insert Into tbl_LRS_Reminder                                
(                                
  CardcodeFK, date, event, first, last, location, SMS, CALL, EMAIL, EventParties, CreatedDate, ModifiedDate, Caseno, whenSMS, whenCALL, whenEMAIL,                                 
                      LastUpdatedStatusFK, EventHTML, IsPrivate,CreatedUserFK,IsConfirmable,AppointmentType,TwilioLang  )                                
  VALUES                                                                    
(@firmcode,@EventStartDate,@EventFull,@FName,@LName,@Location,@SMS,@CALL,@EMAIL,isnull(@EventPartiesFInal,'RS'),Getdate(),Getdate(),@Caseno ,@WhenSMS,                          
 @WhenCALL,@WhenEMAIL,NULL,@EventHTMLFull,@IsPrivate,@CreatedUserFKNEW,@IsConfirmable,@AppointmentType,@TwilioLang)                                
                                                          
  set @TimingSCOPEIdentity = SCOPE_IDENTITY()                                                    
                                                             
  IF(@EventParties = '')                                                        
  Begin                                                             
  set @EventParties = SCOPE_IDENTITY()                                                       
  set @EventPartiesFInal = @EventPartiesLocation                                        
  END                                                       
                                                      
 Update tbl_LRS_Reminder set  EventParties =@EventPartiesFInal  where EventParties = 'RS'                                                       
                                                      
  --//*************** Other Table Entry --SMS*******************************************************************//                                                    
  --declare @SMSTIME Nvarchar(100)                                                           
  set  @SMSTIME =(select top 1 case when whenSMS='' THEN '-1' ELSE whenSMS end as  whenSMS from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                                       
                                                  
  select * into #TempSMS from dbo.SplitByComma(@SMSTIME)                  
                                                      
--drop table      #TempSMS                
--drop table      #TempSMSFINAL                
                                                 
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'SMS'as RTYPE into #TempSMSFINAL                                                     
  from #TempSMS T                                               
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1                                               
  where T.Column1 <> -1                
                                                       
  Declare @SMSStart nvarchar(10),@SMSEnd nvarchar(10),@SMSWhen nvarchar(100),@SMSTYPE Nvarchar(100)                                  
                                                      
  set @SMSStart =(select MIN(NO) from #TempSMSFINAL)           
  set @SMSEnd =(select MAX(NO) from #TempSMSFINAL)                                                    
                                                       
 While(@SMSEnd >= @SMSStart)                                                    
 Begin                                                    
  set @SMSWhen =(select top 1 smsTime from #TempSMSFINAL where No=@SMSStart)                                                    
  set @SMSTYPE =(select top 1 RTYPE from #TempSMSFINAL where No=@SMSStart)                                                    
                                             
                                                
  Insert Into tbl_LRS_Reminder_Timing                                                    
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@SMSWhen,@EventStartDate)),@SMSWhen,@SMSTYPE,0,Getdate(),Getdate(),0                                                    
                                                     
  set @SMSStart =@SMSStart+1                                                    
 END                                                     
                                                     
--//*************** Other Table Entry --CALL*******************************************************************//                                                    
                                                      
  set  @CALLTIME =(select top 1 case when whenCALL='' THEN '-1' ELSE whenCALL end as whenCALL from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                                       
                                                      
  select * into #TempCALL from dbo.SplitByComma(@CALLTIME)                                                      
                                                      
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'CALL'as RTYPE into #TempCALLFINAL                                                     
  from #TempCALL T                                                    
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1                  
  where T.Column1 <> -1                                                    
                                                      
  Declare @CALLStart nvarchar(10),@CALLEnd nvarchar(10),@CALLWhen nvarchar(100),@CALLTYPE Nvarchar(100)                                                    
                                                      
  set @CALLStart =(select MIN(NO) from #TempCALLFINAL)                                                    
  set @CALLEnd =(select MAX(NO) from #TempCALLFINAL)                                                    
                                                       
 While(@CALLEnd >= @CALLStart)                                                    
 Begin                                                    
  set @CALLWhen =(select top 1 smsTime from #TempCALLFINAL where No=@CALLStart)                                                    
  set @CALLTYPE =(select top 1 RTYPE from #TempCALLFINAL where No=@CALLStart)                                                    
                                                      
                                                     
  Insert Into tbl_LRS_Reminder_Timing                                                    
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@CALLWhen,@EventStartDate)),@CALLWhen,@CALLTYPE,0,Getdate(),Getdate(),0                                                    
                                                     
  set @CALLStart =@CALLStart+1                                                    
 END                                                     
--//*************** Other Table Entry --EMAIL*******************************************************************//                                                    
                 
  set  @EMAILTIME =(select top 1 case when whenEMAIL='' THEN '-1' ELSE whenEMAIL end as whenEMAIL  from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                               
                                             
  select * into #TempEMAIL from dbo.SplitByComma(@EMAILTIME)                                                      
                                                      
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'EMAIL'as RTYPE into #TempEMAILFINAL                                                     
  from #TempEMAIL T                                                    
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1                 
  where T.Column1 <> -1                                                     
                                                      
  Declare @EMAILStart nvarchar(10),@EMAILEnd nvarchar(10),@EMAILWhen nvarchar(100),@EMAILTYPE Nvarchar(100)                                                                            
  set @EMAILStart =(select MIN(NO) from #TempEMAILFINAL)                                
  set @EMAILEnd =(select MAX(NO) from #TempEMAILFINAL)                                                    
              
 While(@EMAILEnd >= @EMAILStart)                                                    
 Begin                                                    
  set @EMAILWhen =(select top 1 smsTime from #TempEMAILFINAL where No=@EMAILStart)                                                    
  set @EMAILTYPE =(select top 1 RTYPE from #TempEMAILFINAL where No=@EMAILStart)                                                    
                                                      
                                                     
  Insert Into tbl_LRS_Reminder_Timing                                                    
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@EMAILWhen,@EventStartDate)),@EMAILWhen,@EMAILTYPE,0,Getdate(),Getdate(),0                                                    
                                                 
  set @EMAILStart =@EMAILStart+1                                                    
 END                                                      
                                                          
      Drop Table #TempSMS                                               
      Drop Table #TempCALL                                               
      Drop Table #TempEMAIL                                              
      Drop Table #TempSMSFINAL                                                
      Drop Table #TempCALLFINAL                                                
      Drop Table #TempEMAILFINAL                                               
                                          
                                     
 INSERT Into tbl_NextGenerateEventID                                          
 select EventID from InsertReminderFinal_A1LAW(@TimingSCOPEIdentity,@ClientKey,@Phone,@EmailID,@first,@last)                                          
               
                                         
                                         
 INSERT into tbl_FinalReminderDetails                                          
 select * from InsertReminderFinal_A1LAW(@TimingSCOPEIdentity,@ClientKey,@Phone,@EmailID,@first,@last)                                         
                                                                  
  set @SP =@SP +1                                                      
                                                 
End                                                                  
                                                                
                                  
                                      
                                                         
                                      
                                                                  
select 1 as Result                                        
               
                                        
                                        
                        
                                        
                                                                
                                                                
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Insesrt_Event]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_Insesrt_Event]                                                                  
@EventStartDate Datetime,                                                                  
@EventEndDate Datetime,                                                                  
@Event Nvarchar(MAX),                                                                  
@Location Nvarchar(MAX),                                                                  
@EventTitle Nvarchar(MAX) ,                                                            
@SMS int,                                                            
@CALL int,                                                            
@EMAIL int  ,                                                          
@WhenSMS Nvarchar(500)=null ,                                                        
@WhenCALL Nvarchar(500)=null ,                                                        
@WhenEMAIL Nvarchar(500)=null,                                              
@EventHTML Nvarchar(MAX)=null,                                          
@ClientKey Nvarchar(200),                                
@IsPrivate int,                                
@CreatedUserFK Nvarchar(50)  ,                              
@IsConfirmable int,                          
@AppointmentType Nvarchar(MAX)    ,                        
@TwilioLang Nvarchar(MAX)         
      
                                          
                                                    
                                                         
                                                            
As                                                                  
Begin                                                                  
                                                                  
Declare @EventFull Nvarchar(Max)                                                               
Declare @EventHTMLFull Nvarchar(Max),@CreatedUserFKNEW Nvarchar(500)                                                                      
set @EventFull = LTRIM(RTRIM (Case When @EventTitle is null or @EventTitle ='' then @Location ELSE @EventTitle END))+':'+LTRIM(RTRIM (@Event))                                                        
set @EventHTMLFull =LTRIM(RTRIM (Case When @EventTitle is null or @EventTitle ='' then @Location ELSE @EventTitle END)) +':'+LTRIM(RTRIM (@EventHTML))                                         
                                
                                
set @CreatedUserFKNEW =(select RID from tbl_RS_Registration Where MacID =@CreatedUserFK)                                
                                      
if(@CreatedUserFKNEW='' or @CreatedUserFKNEW is NULL)              
BEGIN              
set @CreatedUserFKNEW =@CreatedUserFK              
END              
              
              
                                     
                                                                  
select ROW_NUMBER() over(order by (select 1)) as No,firmcode,Name,LName,Caseno into #TempPaties from  AddParties where  isnull(isstatus,0)=1 and MACID=@CreatedUserFK                                                              
                                                                  
Declare @FName Nvarchar(500),@LName Nvarchar(500),@firmcode nvarchar(50),@SP int,@EP int ,@EventParties Nvarchar(50),@Caseno Nvarchar(50)        
,@EventPartiesFInal Nvarchar(50),                                                        
@TimingSCOPEIdentity nvarchar(50),@SMSTIME Nvarchar(100),@CALLTIME Nvarchar(100),@EMAILTIME Nvarchar(100)                                                    
                                                        
set @SP =(select Min(No) from #TempPaties)                                                                    
set @EP =(select Max(No) from #TempPaties)                                                           
set @EventParties = ''                                      
                                  
 TRUNCATE Table tbl_NextGenerateEventID                                                                
                           
 While(@EP >= @SP)                                                                    
 Begin                                                                  
                             
  set @FName = (select Name from #TempPaties where No=@SP)                                                                   
  set @LName = (select LName from #TempPaties where No=@SP)                                                                   
  set @firmcode = (select firmcode from #TempPaties where No=@SP)                                                                   
  set @Caseno = (select CaseNo from #TempPaties where No=@SP)                                                                   
                                                                    
                                                                      
  Insert Into tbl_LRS_Reminder                                
(                                
  CardcodeFK, date, event, first, last, location, SMS, CALL, EMAIL, EventParties, CreatedDate, ModifiedDate, Caseno, whenSMS, whenCALL, whenEMAIL,                                 
                      LastUpdatedStatusFK, EventHTML, IsPrivate,CreatedUserFK,IsConfirmable,AppointmentType,TwilioLang  )                                
  VALUES                                                                    
(@firmcode,@EventStartDate,@EventFull,@FName,@LName,@Location,@SMS,@CALL,@EMAIL,isnull(@EventPartiesFInal,'RS'),Getdate(),Getdate(),@Caseno ,@WhenSMS,                          
 @WhenCALL,@WhenEMAIL,NULL,@EventHTMLFull,@IsPrivate,@CreatedUserFKNEW,@IsConfirmable,@AppointmentType,@TwilioLang)                                
                                                          
  set @TimingSCOPEIdentity = SCOPE_IDENTITY()                                                    
                                                             
  IF(@EventParties = '')                                                        
  Begin                                                             
  set @EventParties = SCOPE_IDENTITY()                                                       
  set @EventPartiesFInal = @EventParties                                                 
  END                                                       
                                                      
 Update tbl_LRS_Reminder set  EventParties =@EventPartiesFInal  where EventParties = 'RS'                                                       
                                                      
  --//*************** Other Table Entry --SMS*******************************************************************//                                                    
  --declare @SMSTIME Nvarchar(100)                                                           
  set  @SMSTIME =(select top 1 case when whenSMS='' THEN '-1' ELSE whenSMS end as  whenSMS from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                                       
                                                  
  select * into #TempSMS from dbo.SplitByComma(@SMSTIME)                                                      
                                                      
--drop table      #TempSMS                
--drop table      #TempSMSFINAL                
                                                 
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'SMS'as RTYPE into #TempSMSFINAL                                                     
  from #TempSMS T                                               
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1                                               
  where T.Column1 <> -1                
                                                       
  Declare @SMSStart nvarchar(10),@SMSEnd nvarchar(10),@SMSWhen nvarchar(100),@SMSTYPE Nvarchar(100)                                                    
                                                      
  set @SMSStart =(select MIN(NO) from #TempSMSFINAL)                                                    
  set @SMSEnd =(select MAX(NO) from #TempSMSFINAL)                                                    
                  
 While(@SMSEnd >= @SMSStart)                                                    
 Begin                                                    
 set @SMSWhen =(select top 1 smsTime from #TempSMSFINAL where No=@SMSStart)                                                    
  set @SMSTYPE =(select top 1 RTYPE from #TempSMSFINAL where No=@SMSStart)                                                    
                                             
                                                
  Insert Into tbl_LRS_Reminder_Timing                                                    
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@SMSWhen,@EventStartDate)),@SMSWhen,@SMSTYPE,0,Getdate(),Getdate(),0                                                    
                                                     
  set @SMSStart =@SMSStart+1                                                    
 END                                                     
                                                     
--//*************** Other Table Entry --CALL*******************************************************************//                                                    
                                                      
  set  @CALLTIME =(select top 1 case when whenCALL='' THEN '-1' ELSE whenCALL end as whenCALL from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                                       
                                                      
  select * into #TempCALL from dbo.SplitByComma(@CALLTIME)                                                      
                                                      
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'CALL'as RTYPE into #TempCALLFINAL                                                     
  from #TempCALL T                                                    
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1                  
  where T.Column1 <> -1                                                    
                                                      
  Declare @CALLStart nvarchar(10),@CALLEnd nvarchar(10),@CALLWhen nvarchar(100),@CALLTYPE Nvarchar(100)                                                    
                                                      
  set @CALLStart =(select MIN(NO) from #TempCALLFINAL)                                                    
  set @CALLEnd =(select MAX(NO) from #TempCALLFINAL)                                                    
                                                       
 While(@CALLEnd >= @CALLStart)                                                    
 Begin                                                    
  set @CALLWhen =(select top 1 smsTime from #TempCALLFINAL where No=@CALLStart)                                                    
  set @CALLTYPE =(select top 1 RTYPE from #TempCALLFINAL where No=@CALLStart)                                                    
                                                      
                                                     
  Insert Into tbl_LRS_Reminder_Timing                                                    
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@CALLWhen,@EventStartDate)),@CALLWhen,@CALLTYPE,0,Getdate(),Getdate(),0                                                    
                                                     
  set @CALLStart =@CALLStart+1                                                    
 END                                                     
--//*************** Other Table Entry --EMAIL*******************************************************************//                                                    
                                                    
  set  @EMAILTIME =(select top 1 case when whenEMAIL='' THEN '-1' ELSE whenEMAIL end as whenEMAIL  from tbl_LRS_Reminder  where eventno=@TimingSCOPEIdentity)                                               
                                                      
  select * into #TempEMAIL from dbo.SplitByComma(@EMAILTIME)              
                                                      
  select ROW_NUMBER() over(order by (select 1)) as No,isnull(Id,0) as Id, isnull(smsTime,'Immediate') as smsTime, isnull(Isactive,0) as Isactive,'EMAIL'as RTYPE into #TempEMAILFINAL                                                     
  from #TempEMAIL T                                                    
  LEFT Join lst_SMS_Timing LST on LST.ID =T.Column1                 
  where T.Column1 <> -1                                                     
                                                      
  Declare @EMAILStart nvarchar(10),@EMAILEnd nvarchar(10),@EMAILWhen nvarchar(100),@EMAILTYPE Nvarchar(100)                                                                            
  set @EMAILStart =(select MIN(NO) from #TempEMAILFINAL)                                
  set @EMAILEnd =(select MAX(NO) from #TempEMAILFINAL)                                                    
              
 While(@EMAILEnd >= @EMAILStart)                                                    
 Begin                                                    
  set @EMAILWhen =(select top 1 smsTime from #TempEMAILFINAL where No=@EMAILStart)                                                    
  set @EMAILTYPE =(select top 1 RTYPE from #TempEMAILFINAL where No=@EMAILStart)                                                    
                                                      
                                                     
  Insert Into tbl_LRS_Reminder_Timing                                                    
  Select @TimingSCOPEIdentity,(select dbo.DaysNametoDate(@EMAILWhen,@EventStartDate)),@EMAILWhen,@EMAILTYPE,0,Getdate(),Getdate(),0                                                    
                                                 
  set @EMAILStart =@EMAILStart+1                                                    
 END                                                      
                                                          
      Drop Table #TempSMS                                               
      Drop Table #TempCALL                                               
      Drop Table #TempEMAIL                                              
      Drop Table #TempSMSFINAL                                                
      Drop Table #TempCALLFINAL                                                
      Drop Table #TempEMAILFINAL                                               
                                          
                                          
                                          
      --//Insert Event In Cloud Mapping Table                                          
                                    
                                    
                                    
 INSERT Into tbl_NextGenerateEventID                                        
 select EventID from InsertReminderFinal(@TimingSCOPEIdentity,@ClientKey)                                          
                                         
                                         
 INSERT into tbl_FinalReminderDetails                                          
 select * from InsertReminderFinal(@TimingSCOPEIdentity,@ClientKey)                                         
                                                                  
  set @SP =@SP +1                                                      
                                                 
End                                                                  
                                              
                                  
                                        
--------// Notification Set Before Appointment                                             
                                            
Declare                                             
@EmailWhenNotification Nvarchar(50),                                          @SMSWhenNotification Nvarchar(50),                                            
@FullName Nvarchar(100),                                            
@EventName Nvarchar(500),                                            
@EventDt Nvarchar(50),                                            
@Phone Nvarchar(50),          
@EmailNoti Nvarchar(50),                                            
@EventTitleNoti Nvarchar(500),                                            
@MINPHONE Nvarchar(50),                                            
@MAXPHONE Nvarchar(50),                                            
@PhoneChange Nvarchar(50)                                              
                         
                                             
                                                           
Set @EmailWhenNotification =(select top 1 case when EmailWhen='' then NULL Else EmailWhen end  from tbl_LRS_NotificationSetting where isnull(Isactive,1)=0)                                                         
Set @SMSWhenNotification=(select top 1 case when SMSWhen='' then NULL Else SMSWhen end from tbl_LRS_NotificationSetting where isnull(Isactive,1)=0)                                          
set @FullName = (select top 1 LR.first+' '+LR.last as FullName from tbl_LRS_Reminder LR where LR.Eventno=@TimingSCOPEIdentity)                                                       
set @EventName = (select top 1 Event as [EventTitle] from tbl_LRS_Reminder LR where LR.Eventno=@TimingSCOPEIdentity)                                                  
                                            
select ROW_NUMBER() over(order by (select 1)) as [No],Column1 as EventTitle into #Temp from dbo.SplitByJEM(@EventName)                                              
                                    
set @EventTitleNoti  =(select top 1 EventTitle from #Temp where [No]=1)                                             
set @Phone =(select top 1 SMSSend  from tbl_LRS_NotificationSetting)                                              
                                              
select ROW_NUMBER() over(order by (select 1))as [No],Column1 as Phone into #TempPhone from dbo.SplitByComma(@Phone)                                              
                                              
                                              
                                              
set @MINPHONE =(select Min(No) from #TempPhone)                                                                      
set @MAXPHONE =(select Max(No) from #TempPhone)                                            
set @EmailNoti = (select top 1 EmailSend from tbl_LRS_NotificationSetting)                                               
set @EventDt= (select dbo.DaysNametoDate(@EmailWhenNotification,@EventStartDate))                                            
                                            
                                            
                                            
                                             
                                            
if(@EmailWhenNotification <> '' and @EmailWhenNotification is not Null and @Email <> '')                                                          
Begin                                                          
                                                          
insert into tbl_Notification_TimingSetting                                              
select @EventDt,'You have an upcoming appointment at @'+' '+Convert(varchar,@EventStartDate),@Phone,@EmailNoti,                                            
'Upcoming appointment: '+' '+@EventTitleNoti+' @ '+Convert(varchar,@EventStartDate),0,getdate(),getdate(),'EMAIL'                                            
                                                    
End                                              
While(@MAXPHONE >= @MINPHONE)                                                                      
Begin                                               
                                            
 set @PhoneChange = (select Phone from #TempPhone where No=@MINPHONE)                
                                                        
 if(@SMSWhenNotification <> '' and @SMSWhenNotification is not Null and @Phone <> '')                                                          
 Begin                                                          
                                               
 insert into tbl_Notification_TimingSetting                          
 Select @EventDt                                            
 ,'You have an upcoming appointment at @'+' '+Convert(varchar,@EventStartDate),                                  
 @PhoneChange,@EmailNoti,'Upcoming appointment: '+@EventTitleNoti+' @ '+Convert(varchar,@EventStartDate),0,Getdate(), Getdate() ,'SMS'                                                        
                                             
             
                                               
 END                                            
                                             
 set @MINPHONE =@MINPHONE+1                                                        
End                                              
                                                         
                                      
                                                                  
select 1 as Result                                        
                                        
                                        
                                        
                        
                                        
                                                                
                                                                
END
GO
/****** Object:  StoredProcedure [dbo].[Sproc_A1lawPullRS_A1Law]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Sproc_A1lawPullRS_A1Law 'C73F-DDDA-BB96-4ACF-BA6D','62C25F068041'                            
                              
CREATE Proc [dbo].[Sproc_A1lawPullRS_A1Law]                                   
@ClientKey  nvarchar(100),          
@MACID Nvarchar(50) =Null                               
as                                      
Begin                          
                  
 /*========================================================== First Check Status ON/OFF  ==========================================================*/                          
                            
Declare @A1LAWSyncStatus int      ,@TmpClientMac Nvarchar(50)           
          
set @TmpClientMac      =(select MacID from tbl_RS_Registration where ProductKey=@ClientKey and MacID=@MACID)          
                      
set @A1LAWSyncStatus = (select A1Sync from  tbl_Email_Setting)                      
                      
If(@A1LAWSyncStatus = 1 and @TmpClientMac='62C25F068041' and @MACID <>'')                      
Begin                      
/*========================================================== IF "ON" THEN SYNC Date Update   ==========================================================*/                     
                  
--drop table #TempCalaudit                            
                            
                               
   Declare @LastSyncDate Datetime   
    set @LastSyncDate =(select LastSync  as LastSync from  tbl_Calaudit_Last_Sync )                            
                           
                           
   if(isnull(@LastSyncDate,'')='')                            
   BEGIN                            
    set @LastSyncDate = GETDATE()                        
   END                          
                            
/*========================================================== Mapping With Template Mapping ==========================================================*/                               
                                
   select ROW_NUMBER() over(order by (select 1)) as No,Convert(Nvarchar(50),'A1law')as CardcodeFK,C1.date,AA.EmailSubject,                                  
   C1.first,C1.last,C1.event,AA.isSMS,AA.isCALL,AA.isEMAIL,1 as EventParties                                      
   ,GETDATE() as CreatedDate,GETDATE() as ModifiedDate,C1.caseno,                                      
   AA.whenSMS,AA.whenCALL,AA.whenEMAIL,0 as LastUpdatedStatusFK,AA.Body,0 as IsPrivate,C1.initials0 as CreatedUserFK,                                      
   AA.IsReschedule,AA.Purpose,AA.TwilioLang   , 
                          
   --Case when C1.phonehome = '' or C1.phonehome is NULL then C1.phonebus ELSE C1.phonehome End  as Phone,
   
case when    ( case  when isnull(LTRIM(RTRIM(C1.phonehome)),'')= '' THEN isnull(LTRIM(RTRIM(C1.phonebus)),'')                     
	else isnull(LTRIM(RTRIM(C1.phonehome)),'') end)='' then                     
	(Case when isnull(LTRIM(RTRIM(C1.phonebus)),'')= ''              then isnull(LTRIM(RTRIM(C1.phonecar)),'') else '' EnD)                    
	else    ( case  when isnull(LTRIM(RTRIM(C1.phonehome)),'')= '' THEN isnull(LTRIM(RTRIM(C1.phonebus)),'')                     
	else isnull(LTRIM(RTRIM(C1.phonehome)),'') end) end as Phone,

    C1.location   ,                                   
   C1.email as email  ,C1.eventno   ,C1.chglast                                
   Into #TempCalaudit                            
   FROM [calaudit1] C1                                      
   inner  Join tbl_Apptoto_AddSmsEmail AA on AA.Name=C1.event                       
   WHERE   
   
   
            
    C1.reminder1=1        and            
   (CONVERT(Date,C1.chglast , 103)  >= CONVERT(Date, @LastSyncDate , 103))  and                       
   --(CONVERT(varchar(5),C1.chglast,108)  > CONVERT(varchar(5),@LastSyncDate,108))                  
   (CAST(C1.chglast as Time)  > CAST(@LastSyncDate as Time))                 
                        
                            
/*========================================================== Final Create process ==========================================================*/                                       
   Declare @SP int,@EP int                   
                     
   --Declare @ClientKey   nvarchar(100)                     
   --set  @ClientKey='C73F-DDDA-BB96-4ACF-BA6D'                   
                     
                                      
   set @SP =(select Min(No) from #TempCalaudit)                                                                                                
   set @EP =(select Max(No) from #TempCalaudit)                           
        
                                         
   DECLARE  @CardcodeFK nvarchar(100),@date datetime,@EmailSubject nvarchar(MAX),                      
   @first nvarchar(100),@last nvarchar(100),@event nvarchar(MAX),@isSMS int,                                      
   @isCALL int,@isEMAIL int,@EventParties nvarchar(MAX),                                      
   @CreatedDate datetime,@ModifiedDate datetime,@caseno nvarchar(100),@whenSMS nvarchar(100),@whenCALL nvarchar(100) ,@whenEMAIL nvarchar(100) ,                                      
   @LastUpdatedStatusFK nvarchar(100),@Body nvarchar(MAX)                                     
   ,@IsPrivate INT,@CreatedUserFK nvarchar(100),@IsReschedule int,@Purpose nvarchar(100),@TwilioLang nvarchar(100),@eventno int ,                              
   @LRSLastID NVARCHAR(50)  ,@CalauditScope NVARCHAR(50) ,@Phone Nvarchar(50),@Email Nvarchar(100),  @chglast DATETIME                              
                                         
    While(@EP >= @SP)                                                                                                
    Begin                                 
                                  
 set  @CardcodeFK =(select CardcodeFK from #TempCalaudit  WHERE No=@SP)                                      
    set  @date =(select date from #TempCalaudit  WHERE No=@SP)                                      
    set  @EmailSubject =(select EmailSubject from #TempCalaudit  WHERE No=@SP)                                      
    set  @first =(select first from #TempCalaudit  WHERE No=@SP)                                      
    set  @last =(select last from #TempCalaudit  WHERE No=@SP)                                      
    set  @event =(select event from #TempCalaudit  WHERE No=@SP)                                      
    set  @isSMS =(select isSMS from #TempCalaudit  WHERE No=@SP)                                      
    set  @isCALL =(select isCALL from #TempCalaudit  WHERE No=@SP)                                      
    set  @isEMAIL =(select isEMAIL from #TempCalaudit  WHERE No=@SP)                                      
    set  @EventParties =(select location from #TempCalaudit  WHERE No=@SP)                                      
    set  @CreatedDate =(select CreatedDate from #TempCalaudit  WHERE No=@SP)                                      
    set  @ModifiedDate =(select ModifiedDate from #TempCalaudit  WHERE No=@SP)                                      
    set  @caseno =(select caseno from #TempCalaudit  WHERE No=@SP)                                      
    set  @whenSMS =(select whenSMS from #TempCalaudit  WHERE No=@SP)                                      
    set  @whenCALL =(select whenCALL from #TempCalaudit  WHERE No=@SP)                                      
    set  @whenEMAIL =(select whenEMAIL from #TempCalaudit  WHERE No=@SP)                                      
    set  @LastUpdatedStatusFK =(select LastUpdatedStatusFK from #TempCalaudit  WHERE No=@SP)                                      
    set  @Body =(select Body from #TempCalaudit WHERE No=@SP)                                      
    set  @IsPrivate =(select IsPrivate from #TempCalaudit  WHERE No=@SP)                                      
    set  @CreatedUserFK =(select Convert(nvarchar,CreatedUserFK) from #TempCalaudit  WHERE No=@SP)                                      
    set  @IsReschedule =(select IsReschedule from #TempCalaudit  WHERE No=@SP)                                       
    set  @Purpose =(select Purpose from #TempCalaudit  WHERE No=@SP)                                       
    set  @TwilioLang =(select TwilioLang from #TempCalaudit  WHERE No=@SP)                                       
    set  @eventno = (select eventno from #TempCalaudit  WHERE No=@SP)                               
    SET  @Phone=  (select Phone from #TempCalaudit  WHERE No=@SP)                                   
    SET  @Email=  (select email from #TempCalaudit  WHERE No=@SP)                           
    SET  @chglast = (select chglast from #TempCalaudit  WHERE No=@SP)                  
                                      
    -- // tbl_LRS_Calaudit Table EventNo Insert                                  
                                      
    IF not Exists (select Eventno from tbl_LRS_Calaudit where Eventno=@eventno)                                  
    BEGIN                          
                                    
 Insert into tbl_LRS_Calaudit                                   
 (Eventno, Caseno, Cardcode, IsActive, CreatedDatetime, ModifiedDatetime,chglast)                                  
 VALUES                                  
 (@eventno,@caseno,@CardcodeFK,0,Getdate(),Getdate(),@chglast)                                  
              
 set @CalauditScope =SCOPE_IDENTITY()                           
              
 -- // Parties Add particular Calander Event                                  
              
 Truncate table AddParties                   
              
 Exec [SP_TempPartiesADD_A1LAW] @CardcodeFK,@first,@last,@caseno                                 
              
 -- // LRS Other Table Insert                           
              
 Exec [SP_Insesrt_Event_A1LAW]  @date,@date,@EmailSubject,@event,'',@isSMS,@isCALL,@isEMAIL,@whenSMS,@whenCALL,@whenEMAIL,@Body,@ClientKey,@IsPrivate,@CreatedUserFK,@IsReschedule,@Purpose,@TwilioLang,@Phone,@Email,@first,@last,@EventParties               
 
    
      
         
         
             
             
               
--select   @date,@date,@EmailSubject,@event,'',@isSMS,@isCALL,@isEMAIL,@whenSMS,@whenCALL,@whenEMAIL,@Body,@ClientKey,@IsPrivate,@CreatedUserFK,@IsReschedule,@Purpose,@TwilioLang,@Phone,@Email,@first,@last,@EventParties                             
                    
                         
                           
                             
   set @LRSLastID= IDENT_CURRENT('dbo.tbl_LRS_Reminder')                                  
                             
     Update  tbl_LRS_Calaudit SET eventFK=@LRSLastID where ID=@CalauditScope                           
                                   
                                        
    END                           
    ELSE                      
    BEGIN                        
                           
    IF Exists (select Eventno from tbl_LRS_Calaudit where Eventno=@eventno and chglast <> @chglast)                                  
    BEGIN                          
                           
     update tbl_LRS_Calaudit set chglast=@chglast,IsActive=0  where Eventno=@eventno                        
                           
                             
                           
     Truncate table AddParties                                  
     Exec [SP_TempPartiesADD_A1LAW] @CardcodeFK,@first,@last,@caseno                   
                           
     -- // LRS Other Table Insert                           
                           
     Exec [SP_Insesrt_Event_A1LAW]  @date,@date,@EmailSubject,@event,'',@isSMS,@isCALL,@isEMAIL,@whenSMS,@whenCALL,@whenEMAIL,@Body,@ClientKey,@IsPrivate,@CreatedUserFK,@IsReschedule,@Purpose,@TwilioLang,@Phone,@Email,@first,@last,@EventParties           
  
          
                             
     set @LRSLastID= IDENT_CURRENT('dbo.tbl_LRS_Reminder')                                  
                             
     Delete from tbl_LRS_Reminder Where eventno in (select eventFK from tbl_LRS_Calaudit where Eventno=@eventno )                        
                             
     Update  tbl_LRS_Calaudit SET eventFK=@LRSLastID where  Eventno=@eventno                        
                             
                             
                             
                             
                                   
                           
    END                        
                           
                            
    END                        
     set @SP =@SP +1                                           
                   
                                      
    END             
              
                        
    exec Sproc_Sync_Calaudit                            
                  
 SELECT 1 as Result                              
 END                      
                       
                             
End
GO
/****** Object:  StoredProcedure [dbo].[Sproc_A1lawPullRS]    Script Date: 01/24/2018 10:54:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC Sproc_A1lawPullRS 'C73F-DDDA-BB96-4ACF-BA6D'

CREATE Proc [dbo].[Sproc_A1lawPullRS]     
@ClientKey  nvarchar(100)      
as        
Begin      
    
select ROW_NUMBER() over(order by (select 1)) as No,Convert(Nvarchar(50),c.cardcode)as CardcodeFK,C1.date,AA.EmailSubject,    
C1.first,C1.last,C1.event,AA.isSMS,AA.isCALL,AA.isEMAIL,1 as EventParties        
,GETDATE() as CreatedDate,GETDATE() as ModifiedDate,C1.caseno,        
AA.whenSMS,AA.whenCALL,AA.whenEMAIL,0 as LastUpdatedStatusFK,AA.Body,0 as IsPrivate,C1.initials0 as CreatedUserFK,        
AA.IsReschedule,AA.Purpose,AA.TwilioLang   ,      
case when    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')           
else isnull(LTRIM(RTRIM(C.car)),'') end)='' then           
(Case when isnull(LTRIM(RTRIM(C.home)),'')= ''          
then isnull(LTRIM(RTRIM(C.business)),'') else '' EnD)          
else    ( case  when isnull(LTRIM(RTRIM(C.car)),'')= '' THEN isnull(LTRIM(RTRIM(C.home)),'')           
else isnull(LTRIM(RTRIM(C.car)),'') end) end as Phone,  C1.location   ,     
C.email ,C1.eventno     
Into #TempCalaudit        
FROM [calaudit1] C1        
inner  Join tbl_Apptoto_AddSmsEmail AA on AA.Name=C1.event        
inner join casecard CC On CC.caseno=C1.caseno    
inner join card c on c.cardcode=CC.cardcode and c.first=C1.first and c.last=C1.last    
Inner Join card2 C2 On C2.firmcode =c.firmcode    
where Convert(date,C1.chglast) >= Convert(date,getdate())     
and C1.eventno not in (select LC.Eventno from tbl_LRS_Calaudit LC)    
    
    
    
declare @SP int,@EP int         
set @SP =(select Min(No) from #TempCalaudit)                                                                  
set @EP =(select Max(No) from #TempCalaudit)          
        
DECLARE  @CardcodeFK nvarchar(100),@date datetime,@EmailSubject nvarchar(MAX),        
@first nvarchar(100),@last nvarchar(100),@event nvarchar(MAX),@isSMS int,        
@isCALL int,@isEMAIL int,@EventParties nvarchar(MAX),        
@CreatedDate datetime,@ModifiedDate datetime,@caseno nvarchar(100),@whenSMS nvarchar(100),@whenCALL nvarchar(100) ,@whenEMAIL nvarchar(100) ,        
@LastUpdatedStatusFK nvarchar(100),@Body nvarchar(MAX)       
,@IsPrivate INT,@CreatedUserFK nvarchar(100),@IsReschedule int,@Purpose nvarchar(100),@TwilioLang nvarchar(100),@eventno int ,    
@LRSLastID NVARCHAR(50)  ,@CalauditScope NVARCHAR(50)    
        
 While(@EP >= @SP)                                                                  
 Begin   
 
 set  @CardcodeFK =(select CardcodeFK from #TempCalaudit  WHERE No=@SP)        
 set  @date =(select date from #TempCalaudit  WHERE No=@SP)        
 set  @EmailSubject =(select EmailSubject from #TempCalaudit  WHERE No=@SP)        
 set  @first =(select first from #TempCalaudit  WHERE No=@SP)        
 set  @last =(select last from #TempCalaudit  WHERE No=@SP)        
 set  @event =(select event from #TempCalaudit  WHERE No=@SP)        
 set  @isSMS =(select isSMS from #TempCalaudit  WHERE No=@SP)        
 set  @isCALL =(select isCALL from #TempCalaudit  WHERE No=@SP)        
 set  @isEMAIL =(select isEMAIL from #TempCalaudit  WHERE No=@SP)        
 set  @EventParties =(select location from #TempCalaudit  WHERE No=@SP)        
 set  @CreatedDate =(select CreatedDate from #TempCalaudit  WHERE No=@SP)        
 set  @ModifiedDate =(select ModifiedDate from #TempCalaudit  WHERE No=@SP)        
 set  @caseno =(select caseno from #TempCalaudit  WHERE No=@SP)        
 set  @whenSMS =(select whenSMS from #TempCalaudit  WHERE No=@SP)        
 set  @whenCALL =(select whenCALL from #TempCalaudit  WHERE No=@SP)        
 set  @whenEMAIL =(select whenEMAIL from #TempCalaudit  WHERE No=@SP)        
 set  @LastUpdatedStatusFK =(select LastUpdatedStatusFK from #TempCalaudit  WHERE No=@SP)        
 set  @Body =(select Body from #TempCalaudit  WHERE No=@SP)        
 set  @IsPrivate =(select IsPrivate from #TempCalaudit  WHERE No=@SP)        
 set  @CreatedUserFK =(select Convert(nvarchar,CreatedUserFK) from #TempCalaudit  WHERE No=@SP)        
 set  @IsReschedule =(select IsReschedule from #TempCalaudit  WHERE No=@SP)         
 set  @Purpose =(select Purpose from #TempCalaudit  WHERE No=@SP)         
 set  @TwilioLang =(select TwilioLang from #TempCalaudit  WHERE No=@SP)         
 set @eventno = (select eventno from #TempCalaudit  WHERE No=@SP)         
     
 -- // tbl_LRS_Calaudit Table EventNo Insert    
     
 IF not Exists (select Eventno from tbl_LRS_Calaudit where Eventno=@eventno)    
 BEGIN    
  Insert into tbl_LRS_Calaudit     
  (Eventno, Caseno, Cardcode, IsActive, CreatedDatetime, ModifiedDatetime)    
  VALUES    
  (@eventno,@caseno,@CardcodeFK,0,Getdate(),Getdate())    
  set @CalauditScope =SCOPE_IDENTITY()    
       
 END    
    
  -- // Parties Add particular Calander Event    
      
  Truncate table AddParties    
  Exec SP_TempPartiesADD @CardcodeFK,@first,@last,@caseno       
         
    
              
 -- // LRS Other Table Insert       


               
Exec [SP_Insesrt_Event]  @date,@date,@EmailSubject,@event,'',@isSMS,@isCALL,@isEMAIL,@whenSMS,@whenCALL,@whenEMAIL,@Body,@ClientKey,@IsPrivate,@CreatedUserFK,@IsReschedule,@Purpose,@TwilioLang,@EventParties        
        
        
set @LRSLastID= IDENT_CURRENT('dbo.tbl_LRS_Reminder')    

    
Update  tbl_LRS_Calaudit SET eventFK=@LRSLastID where ID=@CalauditScope    
 
          
     
     
  set @SP =@SP +1        
     
  END 
End
GO
/****** Object:  Default [DF_tbl_RS_Registration_ISActivated]    Script Date: 01/24/2018 10:54:49 ******/
ALTER TABLE [dbo].[tbl_RS_Registration] ADD  CONSTRAINT [DF_tbl_RS_Registration_ISActivated]  DEFAULT ((0)) FOR [ISActivated]
GO
/****** Object:  Default [DF_tbl_LRS_AddressBookDetails_CreatedDate]    Script Date: 01/24/2018 10:54:49 ******/
ALTER TABLE [dbo].[tbl_LRS_AddressBookDetails] ADD  CONSTRAINT [DF_tbl_LRS_AddressBookDetails_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
/****** Object:  Default [DF_tbl_LRS_AddressBook_status]    Script Date: 01/24/2018 10:54:49 ******/
ALTER TABLE [dbo].[tbl_LRS_AddressBook] ADD  CONSTRAINT [DF_tbl_LRS_AddressBook_status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_tbl_LRS_AddressBook_CreatedDate]    Script Date: 01/24/2018 10:54:49 ******/
ALTER TABLE [dbo].[tbl_LRS_AddressBook] ADD  CONSTRAINT [DF_tbl_LRS_AddressBook_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
