﻿using RS_UpgradeApplication.Datalayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace RS_UpgradeApplication
{

    [ServiceContract]
    public interface Irs_UpgradeApplication
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetVersion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<getVersion> GetVersion(getVersion GetVersion);


        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "setClientVersion", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string SetUserInstalledVersion(UpdateLatestVesrion setClientVersion);


        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateRegistrationDetails", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        string UpdateRegistration(UpdateRegistrationDetails UpdateRegistrationDetails);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetA1LawCount", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<getCountA1> getCountA1table(getCountA1 getCountA1table);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateRecall", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        List<UpdateRecallSetting> UpdateRecallDetails(UpdateRecallSetting getRecall);

       


    }
}
