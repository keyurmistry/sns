﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Web.Hosting;
using System.Web.Script.Serialization;
using System.Linq;
using RS_UpgradeApplication.Datalayer;
using System.Text;
using System.Web;


namespace RS_UpgradeApplication
{
    public class getApplicationVersionSql
    {
        public static DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["rsApplicationVersion"].ConnectionString);

        public static List<getVersion> Get_ApplicationVersionSQL(getVersion GetVersion)
        {


            string qry = string.Empty;

            List<getVersion> ListDetails = new List<getVersion>();
            DataSet dsInsert = new DataSet();
            qry = "Exec sproc_wsRS_Upgrade_Details '" + GetVersion.ClientKey + "'";
            dsInsert = dal.GetData(qry);



            if (dsInsert != null)
            {
                ListDetails.Add(new getVersion()
                {
                    ApplicationVersion = dsInsert.Tables[0].Rows[0]["ApplicationVersion"].ToString(),
                    ApplicationPath = dsInsert.Tables[0].Rows[0]["ApplicationPath"].ToString()
                });


            }
            return ListDetails;

        }

        public static string UpdateClientVersionDetails(UpdateLatestVesrion setClientVersion)
        {

            string qry, Result = string.Empty;

            DataSet dsInsert = new DataSet();
            SqlParameter[] p = new SqlParameter[2];

            p[0] = new SqlParameter("@ProductKey", setClientVersion.UserProductKey);
            p[1] = new SqlParameter("@Version", setClientVersion.UserInstalledVersion);

            qry = "Exec UpdateClientVersion '" + p[0].Value + "','" + p[1].Value + "'";
            dsInsert = dal.GetData(qry);

            if (dsInsert.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                Result = "success";
            }
            else
            {
                Result = "failed";
            }


            return Result;

        }

        public static string UpdateReg(UpdateRegistrationDetails setRegDetails)
        {
            string Result = string.Empty;

            DataSet dsUpdateReg = new DataSet();
            SqlParameter[] p = new SqlParameter[5];



            p[0] = new SqlParameter("@Flag", "Update");
            p[1] = new SqlParameter("@CompanyName", setRegDetails.CompanyName);
            p[2] = new SqlParameter("@Mobile", setRegDetails.Mobile);
            p[3] = new SqlParameter("@Email", setRegDetails.Email);
            p[4] = new SqlParameter("@MACID", setRegDetails.MacID);


            string s = "Exec sproc_UpdateRegistrationDetails '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "'";
            dsUpdateReg = dal.GetData(s);
            if (dsUpdateReg.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                Result = "success";
            }


            else
            {
                Result = "failed";
            }


            return Result;

        }

        public static List<getCountA1> SendalltablewithCloud(getCountA1 getCountA1table)
        {


            string qry = string.Empty;

            List<getCountA1> ListDetails = new List<getCountA1>();



            DataSet dstableCount = new DataSet();
            SqlParameter[] p = new SqlParameter[6];

            p[0] = new SqlParameter("@ServerMACID", getCountA1table.ServerMAC);
            p[1] = new SqlParameter("@CardCount", getCountA1table.card);
            p[2] = new SqlParameter("@Card2Count", getCountA1table.card2);
            p[3] = new SqlParameter("@CaseCount", getCountA1table.casetabte);
            p[4] = new SqlParameter("@CasecardCount", getCountA1table.casecard);
            p[5] = new SqlParameter("@StatusLog", getCountA1table.StatusLog);



            qry = "Exec Sproc_InsertAllawCount '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "'";
            dstableCount = dal.GetData(qry);

            List<getCountA1> _AddtoFavResponce = new List<getCountA1>();

            if (dstableCount.Tables[0].Rows[0]["Result"].ToString() == "1")
            {

                _AddtoFavResponce.Add(new getCountA1
                {
                    Success = true


                });
            }
            else
            {
                _AddtoFavResponce.Add(new getCountA1
                {
                    Success = false


                });

            }

            return _AddtoFavResponce;

        }

        public static List<UpdateRecallSetting> UpdateRecall_Details(UpdateRecallSetting getRecall)
        {


            string qry = string.Empty;

            DataSet dstableCount = new DataSet();
            SqlParameter[] p = new SqlParameter[4];

            p[0] = new SqlParameter("@ClientKey", getRecall.ClientKey);
            p[1] = new SqlParameter("@RecallEnable", getRecall.ReCallEnable);
            p[2] = new SqlParameter("@HMT", getRecall.RecallHMT);
            p[3] = new SqlParameter("@Inteval", getRecall.RecallInterval);




            qry = "Exec Sproc_Recallupdate_Cloud '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "'";
            dstableCount = dal.GetData(qry);

            List<UpdateRecallSetting> _RecallResponce = new List<UpdateRecallSetting>();

            if (dstableCount.Tables[0].Rows[0]["Result"].ToString() == "1")
            {

                _RecallResponce.Add(new UpdateRecallSetting
                {
                    Success = true


                });
            }
            else
            {
                _RecallResponce.Add(new UpdateRecallSetting
                {
                    Success = false


                });

            }

            return _RecallResponce;

        }

        


     






    }
}