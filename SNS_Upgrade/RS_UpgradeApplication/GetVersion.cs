﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RS_UpgradeApplication
{
    [Serializable, DataContract]
    public class getVersion
    {
        [DataMember]
        public string ApplicationVersion { get; set; }

        [DataMember]
        public string ApplicationPath { get; set; }

        [DataMember]
        public string ClientKey { get; set; }
    }

    [Serializable, DataContract]
    public class UpdateLatestVesrion
    {
        [DataMember]
        public string UserInstalledVersion { get; set; }

        [DataMember]
        public string UserProductKey { get; set; }
    }

    [Serializable, DataContract]
    public class UpdateRegistrationDetails
    {
        [DataMember]
        public string CompanyName { get; set; }

        [DataMember]
        public string Mobile { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string MacID { get; set; }

        [DataMember]
        public Content Content { get; set; }
    }

    [Serializable, DataContract]
    public class Content
    {
        [DataMember]
        public string RegistrationResponse { get; set; }

    }

    [Serializable, DataContract]
    public class getCountA1
    {
        [DataMember]
        public string ServerMAC { get; set; }

        [DataMember]
        public string card { get; set; }

        [DataMember]
        public string card2 { get; set; }

        [DataMember]
        public string casetabte { get; set; }

        [DataMember]
        public string casecard { get; set; }

        [DataMember]
        public string StatusLog { get; set; }


        [DataMember]
        public bool Success { get; set; }
    }


    [Serializable, DataContract]
    public class UpdateRecallSetting
    {
        [DataMember]
        public string ClientKey { get; set; }

        [DataMember]
        public string RecallInterval { get; set; }

        [DataMember]
        public string RecallHMT { get; set; }

        [DataMember]
        public string ReCallEnable { get; set; }

        [DataMember]
        public bool Success { get; set; }
        
    }








}