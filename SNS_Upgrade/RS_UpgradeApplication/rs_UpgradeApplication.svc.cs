﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace RS_UpgradeApplication
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class rs_UpgradeApplication : Irs_UpgradeApplication
    {


        public List<getVersion> GetVersion(getVersion GetVersion)
        {
            return getApplicationVersionSql.Get_ApplicationVersionSQL(GetVersion);
        }


        public string SetUserInstalledVersion(UpdateLatestVesrion setClientVersion)
        {
            return getApplicationVersionSql.UpdateClientVersionDetails(setClientVersion);
        }

        public string UpdateRegistration(UpdateRegistrationDetails UpdateRegistrationDetails)
        {
            return getApplicationVersionSql.UpdateReg(UpdateRegistrationDetails);
        }


        public List<getCountA1> getCountA1table(getCountA1 getCountA1table)
        {
            return getApplicationVersionSql.SendalltablewithCloud(getCountA1table);
        }


        public List<UpdateRecallSetting> UpdateRecallDetails(UpdateRecallSetting getRecall)
        {
            return getApplicationVersionSql.UpdateRecall_Details(getRecall);
        }


       

    }
}
