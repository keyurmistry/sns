﻿using DataLayer;
using Newtonsoft.Json;
using ReminderSystem.Business.Base;
using ReminderSystem.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppToto
{
    public partial class EmailPassword : Form
    {

        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        public EmailPassword()
        {
            InitializeComponent();
        }

        private void EmailPassword_Load(object sender, EventArgs e)
        {
            try
            {
                GetEmailDetails();
            }
            catch
            { }
        }



        public void GetEmailDetails()
        {


            DataSet dsGetList = new DataSet();
            string strEmailDetails = "exec Sp_GetEmailSMSDetails";
            dsGetList = dal.GetData(strEmailDetails, Application.StartupPath);
            if (dsGetList != null && dsGetList.Tables != null && dsGetList.Tables[0].Rows.Count > 0)
            {

                txtEmailID.Text = dsGetList.Tables[0].Rows[0]["EmailFrom"].ToString().Trim();
                txtPassword.Text = dsGetList.Tables[0].Rows[0]["EmailPassword"].ToString().Trim();
                txtEmailSubject.Text = dsGetList.Tables[0].Rows[0]["EmailFromTitle"].ToString().Trim();
                txtSMTPhost.Text = dsGetList.Tables[0].Rows[0]["EmailSMTP"].ToString().Trim();
                txtSMTPPort.Text = dsGetList.Tables[0].Rows[0]["EmailPort"].ToString().Trim();
                txtSMSSenderNo.Text = dsGetList.Tables[0].Rows[0]["Twilio_SMS"].ToString().Trim();
                txtCallNo.Text = dsGetList.Tables[0].Rows[0]["Twilio_CALL"].ToString().Trim();
                txtAccountSID.Text = dsGetList.Tables[0].Rows[0]["Twilio_AccountSid"].ToString().Trim();
                txtAuthoToken.Text = dsGetList.Tables[0].Rows[0]["Twilio_AuthToken"].ToString().Trim();

                txtClientKey.Text = new DalBase().GetClientKey();
            }
            else
            {


            }
        }

        private void btnEmailSettings_Click(object sender, EventArgs e)
        {


            if (txtEmailID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please enter email id", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmailID.Focus();
                return;
            }

            try
            {
                var test = new MailAddress(txtEmailID.Text);
            }
            catch
            {
                MessageBox.Show("Please enter valid email id", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmailID.Focus();
                return;
            }

            if (txtEmailSubject.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please enter email subject", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmailSubject.Focus();
                return;
            }


            try
            {
                UPdateEmailDetails();
            }
            catch
            {

            }
        }
        public void UPdateEmailDetails()
        {
            //DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Are you sure Update Details ?", "Update Confirmation", MessageBoxButtons.YesNo);

            //if (dialogResult == DialogResult.Yes || true)  // error is here
            if (true)  // error is here
            {
                DataSet dsGetList = new DataSet();

                SqlParameter[] p = new SqlParameter[9];

                p[0] = new SqlParameter("@EmailFrom", txtEmailID.Text);
                p[1] = new SqlParameter("@EmailPassword", txtPassword.Text);
                p[2] = new SqlParameter("@EmailFromTitle", txtEmailSubject.Text);
                p[3] = new SqlParameter("@EmailPort", txtSMTPPort.Text);
                p[4] = new SqlParameter("@EmailSMTP", txtSMTPhost.Text);
                p[5] = new SqlParameter("@Twilio_CALL", txtCallNo.Text);
                p[6] = new SqlParameter("@Twilio_SMS", txtSMSSenderNo.Text);
                p[7] = new SqlParameter("@Twilio_AccountSid", txtAccountSID.Text);
                p[8] = new SqlParameter("@Twilio_AuthToken", txtAuthoToken.Text);


                #region Push Data to Cloud
                try
                {
                    rsWebRequest objRequest = new rsWebRequest();
                    wsEmail_SettingResponse objResponse = new wsEmail_SettingResponse();
                    wsEmail_Setting businessObject = new wsEmail_Setting()
                    {
                        EmailFrom = txtEmailID.Text,
                        EmailPassword = txtPassword.Text,
                        EmailFromTitle = txtEmailSubject.Text,
                        EmailPort = txtSMTPPort.Text,
                        EmailSMTP = txtSMTPhost.Text,
                        Twilio_CALL = txtCallNo.Text,
                        Twilio_SMS = txtSMSSenderNo.Text,
                        Twilio_AccountSid = txtAccountSID.Text,
                        Twilio_AuthToken = txtAuthoToken.Text,
                        CompanyLogo = "",
                        Signature = "",
                        ClientKey = new DalBase().GetClientKey()
                    };

                    objRequest.postData = JsonConvert.SerializeObject(businessObject);
                    objRequest.webURIServiceName = "EmailSetting/wsSyncEmailSetting";
                    objResponse = JsonConvert.DeserializeObject<wsEmail_SettingResponse>(objRequest.getWebResponse());

                    if (!objResponse.Success)
                    {
                        MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }

                }
                catch (Exception Ex)
                {

                    throw;
                }
                #endregion Push Data to Cloud


                string s = "exec Sp_UpdateEmailSMSDetails '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";

                dsGetList = dal.GetData(s, Application.StartupPath);
                if (dsGetList != null && dsGetList.Tables != null && dsGetList.Tables[0].Rows.Count > 0)
                {
                    System.Windows.MessageBox.Show("Record Update Successfully .");
                    picEmailSend.Visible = false;
                }
                else
                {

                    picEmailSend.Visible = false;
                }
            }
            else
            {
                MessageBox.Show("Please Enter Details First.");
            }


        }

        private void btnTestMail_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (txtEmailID.Text != "" && txtPassword.Text != "" && txtSMTPhost.Text != "" && txtSMTPPort.Text != "")
            {


                try
                {

                    MailMessage ms = new MailMessage();
                    ms.To.Add(txtEmailID.Text);
                    ms.From = new MailAddress(txtEmailID.Text, "LRS - Test Mail.");
                    ms.Subject = "LRS Test Mail";
                    ms.Body = "Test LRS Body:";
                    ms.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient(txtSMTPhost.Text, Convert.ToInt32(txtSMTPPort.Text));
                    smtp.Host = txtSMTPhost.Text;
                    smtp.Credentials = new System.Net.NetworkCredential(txtEmailID.Text, txtPassword.Text);
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = false;
                    smtp.EnableSsl = false;
                    smtp.Send(ms);
                    picEmailSend.Visible = true;
                    picEmailSend.Image = SynergyNotificationSystem.Properties.Resources.Emailsend;
                }
                catch
                {
                    picEmailSend.Visible = true;
                    picEmailSend.Image = SynergyNotificationSystem.Properties.Resources.EmailCancel;
                }





            }
            else
            {

                MessageBox.Show("Please Fill Details First..!");

            }

            Cursor.Current = Cursors.Default;
        }

    }
}
