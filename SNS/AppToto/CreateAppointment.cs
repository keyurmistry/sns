﻿using DataLayer;
using ReminderSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppToto
{
    public partial class CreateAppointment : Form
    {
        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        string SMSTiming, CALLTiming, EMailTiming = string.Empty;

        private void CreateAppointment_Load(object sender, EventArgs e)
        {

            try { getTwilioLang(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try { getTemplateField(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

           


            try { GetTimingCheckbox(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try { GetEdit(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }

        public CreateAppointment()
        {
            InitializeComponent();
        }

        private void Ccancelbtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        public static string SQLFix(string str)
        {
            if (str == "NA")
                str = "";
            return str.Replace("&nbsp;", "").Replace("'", "''");
        }

        private void Submiteventbtn_Click(object sender, EventArgs e)
        {
            try
            {

                if (txttitle_Template.Text == "")
                {
                    MessageBox.Show("Please Enter Template Title.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txttitle_Template.Focus();
                    return;
                }
                if (this.txtBody_Template.InnerText == null || txtBody_Template.InnerText == string.Empty)
                {
                    MessageBox.Show("Please Enter Template Body.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtBody_Template.Focus();
                    return;
                }






                DataSet dsTemp = new DataSet();
                SqlParameter[] p = new SqlParameter[14];


                System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                dateInfo.ShortDatePattern = Helper.UniversalDateFormat;
                string Strflag = string.Empty;
                if (Submiteventbtn.Text == "Save")
                {
                    Strflag = "Insert";
                    Main.str_ID = "";
                }
                else
                {
                    Strflag = "Update";
                }


                int SMS, CALL, EMAIL, IsConfirmable = 0;

                if (chkSMS.Checked == true)
                {
                    SMS = 1;
                }
                else
                {
                    SMS = 0;
                }
                if (chkCALL.Checked == true)
                {
                    CALL = 1;
                }
                else
                {
                    CALL = 0;
                }
                if (chkEMAIL.Checked == true)
                {
                    EMAIL = 1;
                }
                else
                {
                    EMAIL = 0;
                }

                if (chkIsConfirmable.Checked == true)
                {
                    IsConfirmable = 1;
                }
                else
                {
                    IsConfirmable = 0;
                }

                try
                {
                    GetAllTimingWithComma();
                }
                catch
                { }

                p[0] = new SqlParameter("@Flag", Strflag);
                p[1] = new SqlParameter("@Title", SQLFix(txttitle_Template.Text));
                p[2] = new SqlParameter("@Body", SQLFix(txtBody_Template.InnerHtml));
                p[3] = new SqlParameter("@BodyNotHTML", SQLFix(txtBody_Template.InnerText));
                p[4] = new SqlParameter("@ID", SQLFix(Main.str_ID));
                p[5] = new SqlParameter("@AppointmentType", SQLFix(txtappointmentType.Text));
                p[6] = new SqlParameter("@TwilioLangID", (drpTwilioLang.SelectedItem as drpTwilioLangBind).Value.ToString());
                p[7] = new SqlParameter("@SMS", SMS);
                p[8] = new SqlParameter("@CALL", CALL);
                p[9] = new SqlParameter("@EMAIL", EMAIL);
                p[10] = new SqlParameter("@whenSMS", SMSTiming);
                p[11] = new SqlParameter("@whenCALL", CALLTiming);
                p[12] = new SqlParameter("@whenEMAIL", EMailTiming);

                p[13] = new SqlParameter("@IsReschedule", IsConfirmable);





                string s = "Exec Sp_insertTemplete '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "','" + p[9].Value + "','" + p[10].Value + "','" + p[11].Value + "','" + p[12].Value + "','" + p[13].Value + "'";
                dsTemp = dal.GetData(s, Application.StartupPath);
                if (dsTemp.Tables[0].Rows[0]["Result"].ToString() == "1")
                {

                    MessageBox.Show("Template " + Strflag + " successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();


                    try
                    {
                        Main master = (Main)Application.OpenForms["Main"];
                        master.btnappointmentRefresh.PerformClick();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                }
                else if (dsTemp.Tables[0].Rows[0]["Result"].ToString() == "2")
                {

                    MessageBox.Show("Template Title Already Exists.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Question);
                }
                else
                {

                    MessageBox.Show("Error occur(s) during save Event.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }



        private void getTemplateField()
        {
            DataSet dsTemplateField = new DataSet();
            string s = "EXEC GetTemplate";
            dsTemplateField = dal.GetData(s, Application.StartupPath);
            if (dsTemplateField != null && dsTemplateField.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsTemplateField.Tables[0].Rows.Count; i++)
                {
                    drptemplete.Items.Add(dsTemplateField.Tables[0].Rows[i][1].ToString());

                }
            }

            else
            {

            }

        }

        public void GetEdit()
        {

            txtBody_Template.InnerHtml = Main.str_BodyAppoint;
            txttitle_Template.Text = Main.str_TitleAppoint;
            Submiteventbtn.Text = Main.str_SubmitAppoint;
            txtappointmentType.Text = Main.str_AppointmentType;


            if (Main.str_TwilioSay !="")
                drpTwilioLang.SelectedIndex = drpTwilioLang.FindStringExact(Main.str_TwilioSay);

            lblID.Text = Main.str_ID;

            try
            {
                _UpdateTemplateDetails(lblID.Text);
            }
            catch
            { 
            
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void drptemplete_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtBody_Template.InnerHtml += drptemplete.Text;
        }

        private void txtBody_Template_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == '\'');
        }


        private void getTwilioLang()
        {

            DataSet dsbulktype = new DataSet();
            string s = "EXEC GetTwilioLanguage";
            string DefaultLangID = "";

            dsbulktype = dal.GetData(s, Application.StartupPath);
            if (dsbulktype != null && dsbulktype.Tables[0].Rows.Count > 0)
            {
                drpTwilioLang.Items.Clear();
                for (int i = 0; i < dsbulktype.Tables[0].Rows.Count; i++)
                {
                    drpTwilioLangBind itembind = new drpTwilioLangBind();
                    itembind.Text = dsbulktype.Tables[0].Rows[i][1].ToString();
                    itembind.Value = dsbulktype.Tables[0].Rows[i][0].ToString();
                    drpTwilioLang.Items.Add(itembind);

                    if (dsbulktype.Tables[0].Rows[i][3].ToString() == "True")
                    {
                        DefaultLangID = dsbulktype.Tables[0].Rows[i][1].ToString();
                    }

                }

                drpTwilioLang.SelectedIndex = drpTwilioLang.FindStringExact(DefaultLangID);
            }
            else
            {

            }

        }

        public class drpTwilioLangBind
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }


        public void GetTimingCheckbox()
        {

            DataSet dsgetA1DetailsTiming = new DataSet();
            string s = "exec GetSMStiming";
            dsgetA1DetailsTiming = dal.GetData(s, Application.StartupPath);
            if (dsgetA1DetailsTiming != null && dsgetA1DetailsTiming.Tables != null && dsgetA1DetailsTiming.Tables[0].Rows.Count > 0)
            {
                //SMS Grid Bind
                try
                {
                    grdapplyAll.Columns.Clear();
                    grdSMS.Columns.Clear();
                    grdCALL.Columns.Clear();
                    grdEMAIL.Columns.Clear();
                }
                catch
                { }

                try
                {
                    grdapplyAll.DataSource = dsgetA1DetailsTiming.Tables[0];

                    grdapplyAll.Columns[0].Visible = false;
                    grdapplyAll.Columns[1].Visible = false;
                    grdapplyAll.Columns[1].Width = 70;
                    grdapplyAll.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                    checkBoxColumn.HeaderText = "";
                    checkBoxColumn.Width = 30;
                    checkBoxColumn.Name = "";
                    checkBoxColumn.ValueType = typeof(bool);
                    grdapplyAll.Columns.Insert(0, checkBoxColumn);

                }
                catch
                { }


                try
                {
                    grdSMS.DataSource = dsgetA1DetailsTiming.Tables[0];
                    grdSMS.CurrentCell.Selected = false;
                    grdSMS.Columns[0].Visible = false;
                    grdSMS.Columns[1].Width = 70;
                    grdSMS.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                    checkBoxColumn.HeaderText = "";
                    checkBoxColumn.Width = 30;
                    checkBoxColumn.Name = "";
                    checkBoxColumn.ValueType = typeof(bool);
                    grdSMS.Columns.Insert(0, checkBoxColumn);

                }
                catch
                { }
                //CALL Grid Bind
                try
                {
                    grdCALL.DataSource = dsgetA1DetailsTiming.Tables[0];
                    grdCALL.CurrentCell.Selected = false;
                    grdCALL.Columns[0].Visible = false;
                    grdCALL.Columns[1].Width = 70;
                    grdCALL.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn1 = new DataGridViewCheckBoxColumn();
                    checkBoxColumn1.HeaderText = "";
                    checkBoxColumn1.Width = 30;
                    checkBoxColumn1.Name = "";
                    checkBoxColumn1.ValueType = typeof(bool);
                    grdCALL.Columns.Insert(0, checkBoxColumn1);
                }
                catch
                { }
                //EMAIL Grid Bind

                try
                {
                    grdEMAIL.DataSource = dsgetA1DetailsTiming.Tables[0];
                    grdEMAIL.CurrentCell.Selected = false;
                    grdEMAIL.Columns[0].Visible = false;
                    grdEMAIL.Columns[1].Width = 70;
                    grdEMAIL.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn2 = new DataGridViewCheckBoxColumn();
                    checkBoxColumn2.HeaderText = "";
                    checkBoxColumn2.Width = 30;
                    checkBoxColumn2.Name = "";
                    checkBoxColumn2.ValueType = typeof(bool);
                    grdEMAIL.Columns.Insert(0, checkBoxColumn2);
                }
                catch
                { }


            }
            else
            {

            }



        }

        private void grdapplyAll_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {

                DataGridViewCheckBoxCell checkbox = (DataGridViewCheckBoxCell)grdapplyAll.CurrentCell;
                try
                {
                    bool isChecked = (bool)checkbox.EditedFormattedValue;

                    if (isChecked == true)
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = true;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = true;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = true;
                    }
                    else
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = false;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = false;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = false;
                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            }
        }

        private void grdapplyAll_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                Cursor.Current = Cursors.WaitCursor;

                DataGridViewCheckBoxCell checkbox = (DataGridViewCheckBoxCell)grdapplyAll.CurrentCell;


                try
                {
                    bool isChecked = (bool)checkbox.EditedFormattedValue;

                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    if (isChecked == true)
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = true;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = true;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = true;
                    }
                    else
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = false;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = false;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = false;
                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                Cursor.Current = Cursors.Default;

            }

        }

        private void grdSMS_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {

                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;

                    if (isSMS == true)
                    {
                        if (isCALL == true && isEMAIL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }
                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                try //SMS Values GET
                {

                    List<DataGridViewRow> selectedSMS = (from row in grdSMS.Rows.Cast<DataGridViewRow>()
                                                         where Convert.ToBoolean(row.Cells[0].Value) == true
                                                         select row).ToList();


                    if (selectedSMS.Count > 0)
                    {
                        chkSMS.Checked = true;
                    }
                    else
                    {
                        chkSMS.Checked = false;
                    }


                }
                catch
                {

                }

            }

        }

        private void grdSMS_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in this.grdSMS.Rows)
                {
                    if (((bool)grdSMS.Rows[e.RowIndex].Cells[0].Value))
                    {
                        chkSMS.Checked = true;
                    }

                }
            }
            catch
            { }

        }

        private void grdSMS_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {

                if (grdSMS.IsCurrentCellDirty)
                {
                    grdSMS.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            { }
        }

        private void grdCALL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {


                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;


                    if (isCALL == true)
                    {
                        if (isSMS == true && isEMAIL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }


                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                try
                {

                    List<DataGridViewRow> selectedCALL = (from row in grdCALL.Rows.Cast<DataGridViewRow>()
                                                          where Convert.ToBoolean(row.Cells[0].Value) == true
                                                          select row).ToList();


                    if (selectedCALL.Count > 0)
                    {
                        chkCALL.Checked = true;
                    }
                    else
                    {
                        chkCALL.Checked = false;
                    }


                }
                catch
                {

                }
            }

        }

        private void grdCALL_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in this.grdCALL.Rows)
                {
                    if (((bool)grdCALL.Rows[e.RowIndex].Cells[0].Value))
                    {
                        chkCALL.Checked = true;
                    }

                }
            }
            catch
            { }
        }

        private void grdCALL_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {

                if (grdCALL.IsCurrentCellDirty)
                {
                    grdCALL.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            { }
        }

        private void grdEMAIL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {


                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;


                    if (isEMAIL == true)
                    {
                        if (isSMS == true && isCALL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }
                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                try
                {

                    List<DataGridViewRow> selectedEMAIL = (from row in grdEMAIL.Rows.Cast<DataGridViewRow>()
                                                           where Convert.ToBoolean(row.Cells[0].Value) == true
                                                           select row).ToList();


                    if (selectedEMAIL.Count > 0)
                    {
                        chkEMAIL.Checked = true;
                    }
                    else
                    {
                        chkEMAIL.Checked = false;
                    }


                }
                catch
                {

                }
            }
        }

        private void grdEMAIL_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in this.grdEMAIL.Rows)
                {
                    if (((bool)grdEMAIL.Rows[e.RowIndex].Cells[0].Value))
                    {
                        chkEMAIL.Checked = true;
                    }

                }
            }
            catch
            { }
        }

        private void grdEMAIL_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {

                if (grdEMAIL.IsCurrentCellDirty)
                {
                    grdEMAIL.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            { }
        }

        private void chkSMS_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSMS.Checked == false)
            {
                for (int i = 0; i < grdSMS.Rows.Count - 1; i++)
                {
                    grdSMS.Rows[i].Cells[0].Value = false;
                }
            }
        }

        private void chkCALL_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCALL.Checked == false)
            {
                for (int i = 0; i < grdCALL.Rows.Count - 1; i++)
                {
                    grdCALL.Rows[i].Cells[0].Value = false;
                }
            }

        }

        private void chkEMAIL_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEMAIL.Checked == false)
            {
                for (int i = 0; i < grdEMAIL.Rows.Count - 1; i++)
                {
                    grdEMAIL.Rows[i].Cells[0].Value = false;
                }
            }
        }

        public void GetAllTimingWithComma()
        {
            try //SMS Values GET
            {

                List<DataGridViewRow> selectedRows = (from row in grdSMS.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[0].Value) == true
                                                      select row).ToList();
                string StrSMS = string.Empty;

                foreach (DataGridViewRow row in selectedRows)
                {
                    StrSMS += row.Cells[1].Value.ToString() + ",";
                    SMSTiming = StrSMS;
                    SMSTiming = SMSTiming.Remove(SMSTiming.Length - 1);

                }
            }
            catch
            {

            }
            try //CALL Values GET
            {

                List<DataGridViewRow> selectedRows = (from row in grdCALL.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[0].Value) == true
                                                      select row).ToList();
                string StrCALL = string.Empty;

                foreach (DataGridViewRow row in selectedRows)
                {
                    StrCALL += row.Cells[1].Value.ToString() + ",";
                    CALLTiming = StrCALL;
                    CALLTiming = CALLTiming.Remove(CALLTiming.Length - 1);

                }
            }
            catch
            {

            }
            try //EMAIL Values GET
            {

                List<DataGridViewRow> selectedRows = (from row in grdEMAIL.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[0].Value) == true
                                                      select row).ToList();
                string StrEMAIL = string.Empty;

                foreach (DataGridViewRow row in selectedRows)
                {
                    StrEMAIL += row.Cells[1].Value.ToString() + ",";
                    EMailTiming = StrEMAIL;
                    EMailTiming = EMailTiming.Remove(EMailTiming.Length - 1);

                }
            }
            catch
            {

            }

        }

        public void GetUpdatetime(string SMSComma, string CALLComma, string EMAILComma)
        {

            try
            {

                if (SMSComma != "")
                {
                    string[] SMS = SMSComma.Split(',');

                    foreach (string word in SMS)
                    {

                        foreach (DataGridViewRow row in grdSMS.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (CALLComma != "")
                {
                    string[] CALL = CALLComma.Split(',');

                    foreach (string word1 in CALL)
                    {

                        foreach (DataGridViewRow row in grdCALL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word1))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (EMAILComma != "")
                {
                    string[] CALL = EMAILComma.Split(',');

                    foreach (string word2 in CALL)
                    {

                        foreach (DataGridViewRow row in grdEMAIL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word2))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }

        public void _UpdateTemplateDetails(string TemplateID)
        {
            DataSet dsget_update = new DataSet();
            string s = "exec sproc_Update_Template '" + TemplateID + "'";
            dsget_update = dal.GetData(s, Application.StartupPath);
            if (dsget_update != null && dsget_update.Tables != null && dsget_update.Tables[0].Rows.Count > 0)
            {
                if (dsget_update.Tables[0].Rows[0]["isSMS"].ToString() == "1")
                {
                    chkSMS.Checked = true;
                }
                else
                {
                    chkSMS.Checked = false;
                }

                if (dsget_update.Tables[0].Rows[0]["isCALL"].ToString() == "1")
                {
                    chkCALL.Checked = true;
                }
                else
                {
                    chkCALL.Checked = false;
                }
                if (dsget_update.Tables[0].Rows[0]["isEMAIL"].ToString() == "1")
                {
                    chkEMAIL.Checked = true;
                }
                else
                {
                    chkEMAIL.Checked = false;
                }

                if (dsget_update.Tables[0].Rows[0]["IsReschedule"].ToString() == "1")
                {
                    chkIsConfirmable.Checked = true;
                }
                else
                {
                    chkIsConfirmable.Checked = false;
                }


                try
                {
                    GetUpdatetime(dsget_update.Tables[0].Rows[0]["whenSMS"].ToString(), dsget_update.Tables[0].Rows[0]["whenCALL"].ToString(), dsget_update.Tables[0].Rows[0]["whenEMAIL"].ToString());
                }

                catch
                { }
            }
            else
            {

            }
        }

    }
}
