﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReminderSystem
{
    public partial class Recurring : Form
    {
        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        public Recurring()
        {
            InitializeComponent();
        }

        private void btnRecurringCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }

        private void chkdayslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get selected index, and then make sure it is valid.
            int selected = chkdayslist.SelectedIndex;
            if (selected != -1)
            {
                this.Text = chkdayslist.Items[selected].ToString();
            }

        }

        private void btnRecurringAdd_Click(object sender, EventArgs e)
        {

        }

        private void Recurring_Load(object sender, EventArgs e)
        {
            try { AddHour(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            try { AddMinute(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }
        public void AddHour()
        {
            int HourBind = 0;
            string TimeTT = string.Empty;

            TimeTT = GethourBind();

            if (TimeTT != "")
            {
                HourBind = Convert.ToInt32(TimeTT);

                if (HourBind == 23)
                {

                    //lblAMPM.Visible = false;
                    drpAMPM.SelectedValue = "";
                    drpAMPM.Visible = false;
                }
                else
                {
                    //lblAMPM.Visible = true;
                    drpAMPM.Visible = true;
                }

            }
            else
            {

            }

            for (int i = 0; i <= HourBind; i++)
            {
                string Hour = string.Empty;
                if (i < 10)
                {
                    Hour = Convert.ToString("0" + i);
                }
                else
                {
                    Hour = Convert.ToString(i);
                }

                drphours.Items.Add(Hour);
                drpToHour.Items.Add(Hour);


            }
        }

        public void AddMinute()
        {
            for (int i = 0; i <= 59; i++)
            {
                string Minute = string.Empty;
                if (i < 10)
                {
                    Minute = Convert.ToString("0" + i);
                }
                else
                {
                    Minute = Convert.ToString(i);
                }

                drpMinute.Items.Add(Minute);
                drpToMinute.Items.Add(Minute);


            }
        }
        public string GethourBind()
        {
            string TT = string.Empty;
            DataSet dsTimeZone = new DataSet();
            dsTimeZone.Clear();
            string s = "exec Get_TimeZone 'APP'";
            dsTimeZone = dal.GetData(s, Application.StartupPath);
            if (dsTimeZone != null)
            {
                TT = dsTimeZone.Tables[0].Rows[0]["TimeHours"].ToString();
            }

            return TT;
        }

     
    }
}
