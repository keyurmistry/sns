﻿using DataLayer;
using ReminderSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AppToto
{
    public partial class AddContact : Form
    {


        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        public AddContact()
        {
            InitializeComponent();

            txtFirst.ForeColor = SystemColors.GrayText;
            txtFirst.Text = "First";
            this.txtFirst.Leave += new System.EventHandler(this.Txt_first_Leave);
            this.txtFirst.Enter += new System.EventHandler(this.Txt_first_Enter);


            txtLast.ForeColor = SystemColors.GrayText;
            txtLast.Text = "Last";
            this.txtLast.Leave += new System.EventHandler(this.Txt_Last_Leave);
            this.txtLast.Enter += new System.EventHandler(this.Txt_Last_Enter);


            txtmiddle.ForeColor = SystemColors.GrayText;
            txtmiddle.Text = "Middle";
            this.txtmiddle.Leave += new System.EventHandler(this.Txt_middle_Leave);
            this.txtmiddle.Enter += new System.EventHandler(this.Txt_middle_Enter);

        }

        private void NewContactPanel_MouseClick(object sender, MouseEventArgs e)
        {
            NewContactPanel.Focus();
        }

        private void Txt_first_Enter(object sender, EventArgs e)
        {
            if (txtFirst.Text == "First")
            {
                txtFirst.Text = "";
                txtFirst.ForeColor = SystemColors.WindowText;
            }
        }

        private void Txt_first_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtFirst.Text == "First")
            {
                txtFirst.Text = "";
                txtFirst.ForeColor = SystemColors.WindowText;
            }
        }

        private void Txt_first_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtFirst.Text.Length == 0)
            {

                txtFirst.Text = "First";
                txtFirst.ForeColor = SystemColors.GrayText;
            }
        }

        private void Txt_first_Leave(object sender, EventArgs e)
        {
            if (txtFirst.Text.Length == 0)
            {

                txtFirst.Text = "First";
                txtFirst.ForeColor = SystemColors.GrayText;
            }
        }

        private void Txt_Last_Enter(object sender, EventArgs e)
        {
            if (txtLast.Text == "Last")
            {
                txtLast.Text = "";
                txtLast.ForeColor = SystemColors.WindowText;
            }
        }

        private void Txt_Last_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtLast.Text == "Last")
            {
                txtLast.Text = "";
                txtLast.ForeColor = SystemColors.WindowText;
            }
        }

        private void Txt_Last_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtLast.Text.Length == 0)
            {

                txtLast.Text = "Last";
                txtLast.ForeColor = SystemColors.GrayText;
            }
        }

        private void Txt_Last_Leave(object sender, EventArgs e)
        {
            if (txtLast.Text.Length == 0)
            {

                txtLast.Text = "Last";
                txtLast.ForeColor = SystemColors.GrayText;
            }
        }

        private void Txt_middle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtmiddle.Text == "Middle")
            {
                txtmiddle.Text = "";
                txtmiddle.ForeColor = SystemColors.WindowText;
            }
        }

        private void Txt_middle_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtmiddle.Text.Length == 0)
            {

                txtmiddle.Text = "Middle";
                txtmiddle.ForeColor = SystemColors.GrayText;
            }
        }

        private void Txt_middle_Leave(object sender, EventArgs e)
        {
            if (txtmiddle.Text.Length == 0)
            {

                txtmiddle.Text = "Middle";
                txtmiddle.ForeColor = SystemColors.GrayText;
            }
        }

        private void Txt_middle_Enter(object sender, EventArgs e)
        {
            if (txtmiddle.Text == "Middle")
            {
                txtmiddle.Text = "";
                txtmiddle.ForeColor = SystemColors.WindowText;
            }
        }

        public void ClearContact()
        {
            try
            {
                txtFirst.Text = "";
                txtmiddle.Text = "";
                txtLast.Text = "";
                txtZipcode.Text = "";
                txtNote.Text = "";
                txtdob.Text = "";
                Createbtn.Text = "Create Contact";
                drpCountryCode.SelectedIndex = -1;
                drpphone.SelectedIndex = -1;
                drpEmail.SelectedIndex = -1;
                drpprefix.SelectedIndex = -1;
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void Createbtn_Click(object sender, EventArgs e)
        {

            if (Createbtn.Text != "Update Contact")
            {


                try
                {
                    if (txtPhone.Text.Trim() == "" || txtPhone.Text == string.Empty)
                    {

                        MessageBox.Show("Please Enter Mobile Number", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtPhone.Focus();
                        return;


                    }


                    if ((txtFirst.Text != "" && txtFirst.Text.Trim().ToUpper() != "FIRST") || (txtLast.Text != "" && txtLast.Text.Trim().ToUpper() != "LAST"))
                    {


                        DataSet dsInsertAppointment = new DataSet();
                        SqlParameter[] p = new SqlParameter[12];

                        System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                        dateInfo.ShortDatePattern = Helper.UniversalDateFormat;

                        if (txtmiddle.Text == "Middle")
                        {
                            txtmiddle.Text = "";

                        }
                        else if (txtLast.Text == "Last")
                        {
                            txtLast.Text = "";
                        }


                        p[0] = new SqlParameter("@Salutation", drpprefix.Text);
                        p[1] = new SqlParameter("@FName", txtFirst.Text);
                        p[2] = new SqlParameter("@MName", txtmiddle.Text);
                        p[3] = new SqlParameter("@LName", txtLast.Text);
                        p[4] = new SqlParameter("@PhoneNo", txtPhone.Text);
                        p[5] = new SqlParameter("@Email", txtEmail.Text);
                        p[6] = new SqlParameter("@Zipcode", txtZipcode.Text);
                        p[7] = new SqlParameter("@Notes", SQLFix(txtNote.Text));
                        p[8] = new SqlParameter("@DOB", SQLFix(txtdob.Text == "" ? Convert.ToString(txtdob.Text) : Convert.ToDateTime(txtdob.Text, dateInfo).ToString(Helper.UniversalDateFormat)));
                        p[9] = new SqlParameter("@CountryCode", drpCountryCode.SelectedItem);
                        p[10] = new SqlParameter("@MobileType", drpphone.SelectedItem);
                        p[11] = new SqlParameter("@EmailType", drpEmail.Text);

                        string s = "Exec SP_Insert_Contact '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "','" + p[9].Value + "','" + p[10].Value + "','" + p[11].Value + "'";
                        dsInsertAppointment = dal.GetData(s, Application.StartupPath);
                        if (dsInsertAppointment.Tables[0].Rows[0]["Result"].ToString() == "1")
                        {
                            MessageBox.Show("Contact Added Successfully.");

                            this.Close();

                            try
                            {
                                Main master = (Main)Application.OpenForms["Main"];
                                master.btnrefreshContact.PerformClick();
                            }
                            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                        }

                        else if (dsInsertAppointment.Tables[0].Rows[0]["Result"].ToString() == "2")
                        {
                            MessageBox.Show("Mobile Number Already Exists.");


                        }
                        else
                        {
                            MessageBox.Show("Error occur(s) during save Contact.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please Fill the Details.");
                        txtFirst.Focus();
                        return;
                    }
                }
                catch
                {

                }
            }
            else
            {
                // Update Start
                try
                {
                    if ((txtFirst.Text != "" && txtFirst.Text.Trim().ToUpper() != "FIRST") || (txtLast.Text != "" && txtLast.Text.Trim().ToUpper() != "LAST"))
                    {


                        DataSet dsInsertAppointment = new DataSet();
                        SqlParameter[] p = new SqlParameter[14];

                        System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                        dateInfo.ShortDatePattern = Helper.UniversalDateFormat;

                        if (txtmiddle.Text == "Middle")
                        {
                            txtmiddle.Text = "";

                        }
                        else if (txtLast.Text == "Last")
                        {
                            txtLast.Text = "";
                        }


                        p[0] = new SqlParameter("@Salutation", drpprefix.Text);
                        p[1] = new SqlParameter("@FName", txtFirst.Text);
                        p[2] = new SqlParameter("@MName", txtmiddle.Text);
                        p[3] = new SqlParameter("@LName", txtLast.Text);
                        p[4] = new SqlParameter("@PhoneNo", txtPhone.Text);
                        p[5] = new SqlParameter("@Email", txtEmail.Text);
                        p[6] = new SqlParameter("@Zipcode", txtZipcode.Text);
                        p[7] = new SqlParameter("@Notes", SQLFix(txtNote.Text));
                        p[8] = new SqlParameter("@DOB", SQLFix(txtdob.Text == "" ? Convert.ToString(txtdob.Text) : Convert.ToDateTime(txtdob.Text, dateInfo).ToString(Helper.UniversalDateFormat)));
                        p[9] = new SqlParameter("@CountryCode", drpCountryCode.SelectedItem);
                        p[10] = new SqlParameter("@MobileType", drpphone.SelectedItem);
                        p[11] = new SqlParameter("@EmailType", drpEmail.Text);
                        p[12] = new SqlParameter("@CardCode", Main.Con_SetValueForCardCode);
                        p[13] = new SqlParameter("@firmcode", Main.Con_SetValueForFrimCode);

                        string s = "Exec SP_Update_Contact '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "','" + p[9].Value + "','" + p[10].Value + "','" + p[11].Value + "','" + p[12].Value + "','" + p[13].Value + "'";
                        dsInsertAppointment = dal.GetData(s, Application.StartupPath);
                        if (dsInsertAppointment.Tables[0].Rows[0]["Result"].ToString() == "1")
                        {
                            MessageBox.Show("Mobile Number Update Successfully.");
                            this.Close();

                            try
                            {
                                Main master = (Main)Application.OpenForms["Main"];
                                master.btnrefreshContact.PerformClick();
                            }
                            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                        }
                        else if (dsInsertAppointment.Tables[0].Rows[0]["Result"].ToString() == "2")
                        {
                            MessageBox.Show("Mobile Number Already Exists.");


                        }
                        else
                        {
                            MessageBox.Show("Error occur(s) during save Contact.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please Fill the Details.");
                        txtFirst.Focus();
                        return;
                    }
                }
                catch
                {

                }





            }
        }
        public static string SQLFix(string str)
        {
            if (str == "NA")
                str = "";
            return str.Replace("&nbsp;", "").Replace("'", "''");
        }
        private void bind()
        {
            DataSet dsgetA1 = new DataSet();
            string s = "EXEC GetCountryCode";
            dsgetA1 = dal.GetData(s, Application.StartupPath);
            if (dsgetA1 != null && dsgetA1.Tables != null && dsgetA1.Tables[1].Rows.Count > 0)
            {
                for (int i = 0; i < dsgetA1.Tables[0].Rows.Count; i++)
                {
                    drpCountryCode.Items.Add(dsgetA1.Tables[0].Rows[i][0].ToString());
                }
            }

            else
            {

            }

        }

        private void AddContact_Load(object sender, EventArgs e)
        {
            try
            {
                bind();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            try
            {
                GetEdit();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        public void GetEdit()
        {
            try
            {

                drpprefix.SelectedText = Main.Con_SetValueForSuffex.Trim(); ;
                txtFirst.Text = Main.Con_SetValueForFName.Trim();

                txtLast.Text = Main.Con_SetValueForLName.Trim();

                txtmiddle.Text = Main.Con_SetValueForMName.Trim();


                drpCountryCode.SelectedItem = Main.Con_SetValueForCountryCode.Trim();
                txtPhone.Text = Main.Con_SetValueForPhoneNo.Trim();
                drpphone.SelectedItem = Main.Con_SetValueForMobileType.Trim();
                drpEmail.SelectedItem = Main.Con_SetValueForEmailType.Trim();


                txtEmail.Text = Main.Con_SetValueForEmail.Trim();

                txtdob.Text = Main.Con_SetValueForDOB.Trim();
                txtZipcode.Text = Main.Con_SetValueForZipCode.Trim();
                txtNote.Text = Main.Con_SetValueForNote.Trim();
                Createbtn.Text = Main.Con_SetValueForButton.Trim();

            }
            catch
            { }
        }

        private void drpphone_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drpphone.Items.Contains(drpphone.Text))
                {
                    drpphone.SelectedIndex = 0;
                    drpphone.SelectedItem = null;
                    drpphone.SelectedText = "";
                }
            }
            catch
            {

            }
        }

        private void drpprefix_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drpprefix.Items.Contains(drpprefix.Text))
                {
                    drpprefix.SelectedIndex = 0;
                    drpprefix.SelectedItem = null;
                    drpprefix.SelectedText = "";
                }
            }
            catch
            {

            }
        }

        private void drpCountryCode_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drpCountryCode.Items.Contains(drpCountryCode.Text))
                {
                    drpCountryCode.SelectedIndex = 0;
                    drpCountryCode.SelectedItem = null;
                    drpCountryCode.SelectedText = "";
                }
            }
            catch
            {

            }
        }

        private void drpEmail_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drpEmail.Items.Contains(drpEmail.Text))
                {
                    drpEmail.SelectedIndex = 0;
                    drpEmail.SelectedItem = null;
                    drpEmail.SelectedText = "";
                }
            }
            catch
            {

            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '(') && (e.KeyChar != ')') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
        }

        private void txtZipcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }




    }
}
