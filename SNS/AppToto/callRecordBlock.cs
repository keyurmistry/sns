﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppToto
{
    class callRecordBlock
    {
        public string sid { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_updated { get; set; }
        public string parent_call_sid { get; set; }
        public string account_sid { get; set; }
        public string to { get; set; }
        public string to_formatted { get; set; }
        public string from { get; set; }
        public string from_formatted { get; set; }
        public string phone_number_sid { get; set; }
        public string status { get; set; }
        public DateTime start_time { get; set; }
        public DateTime end_time { get; set; }
        public int duration { get; set; }
        public string price { get; set; }
        public string price_unit { get; set; }
        public string direction { get; set; }
        public string answered_by { get; set; }
        public string annotation { get; set; }
        public string api_version { get; set; }
        public string forwarded_from { get; set; }
        public string group_sid { get; set; }
        public string caller_name { get; set; }
        public string uri { get; set; }
        public List<subResourceURI> subresource_uris { get; set; }
    }
}
