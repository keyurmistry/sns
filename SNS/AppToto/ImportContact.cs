﻿using DataLayer;
using ReminderSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppToto
{
    public partial class ImportContact : Form
    {
        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        string UploadedFileName, UploadedFilePath = string.Empty;
        public ImportContact()
        {
            InitializeComponent();
        }

        private void Browsebtn_Click(object sender, EventArgs e)
        {
            try
            {


                Cursor.Current = Cursors.Default;

                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = @"C:\";
                openFileDialog1.Title = "Browse Text Files";
                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;
                openFileDialog1.Multiselect = false;
                openFileDialog1.DefaultExt = "Excel sheet (*.Excel)|*.xls;*.xlsx";
                openFileDialog1.Filter = "All files (*.*)|*.*|Excel sheet (*.Excel)|*.xls;*.xlsx";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                openFileDialog1.ReadOnlyChecked = true;
                openFileDialog1.ShowReadOnly = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    txtBrowse.Text = openFileDialog1.FileName;
                }


                int count = 0;
                string[] FName;

                foreach (string s in openFileDialog1.FileNames)
                {
                    FName = s.Split('\\');
                    UploadedFileName = FName[FName.Length - 1].ToString();
                    UploadedFilePath = Path.GetDirectoryName(txtBrowse.Text);
                    File.Copy(s, UploadedFilePath + FName[FName.Length - 1]);
                    count++;
                }

            }
            catch
            { }

        }


        private void ExcelImportData()
        {
            try
            {
                ExcelImport EI = new ExcelImport();
                if (UploadedFileName != "" && UploadedFileName != string.Empty)
                {
                    DataTable dtExcel = EI.ExcelImportSQL(UploadedFileName, UploadedFilePath);
                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = Helper.UniversalDateFormat;
                    if (dtExcel != null)
                    {
                        SqlParameter[] p = new SqlParameter[1];

                        p[0] = new SqlParameter("@UploadContact", dtExcel);


                        DataSet dtResult = dal.GetData("LRS_UploadContact", p,Application.StartupPath);
                        if (dtResult.Tables[0].Rows[0]["Result"].ToString() == "Inserted")
                        {
                            MessageBox.Show("Data Imported Successfully.");
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Error Occured During Import Data");

                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select File.");

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());

            }
        }

        private void lnkdownload_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {

                var fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == DialogResult.OK)
                {

                    var localPath = Path.Combine(fbd.SelectedPath, Application.StartupPath + @"\Download\Synergy Notification System.xlsx");
                    File.Copy(Application.StartupPath + @"\Download\Synergy Notification System.xlsx", fbd.SelectedPath + "\\" + "Synergy Notification System.xlsx", true);

                    MessageBox.Show("Download Complete.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    
                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (txtBrowse.Text != "")
            {
                try
                {
                    ExcelImportData();
                }
                catch
                { }
            }
            else
            {
                MessageBox.Show("Please Select *.xlsx File.");
            }
        }
    }
}
