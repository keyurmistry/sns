﻿namespace AppToto
{
    partial class CreateAppointment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.createappointmentpanel = new System.Windows.Forms.Panel();
            this.chkIsConfirmable = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.chkEMAIL = new System.Windows.Forms.CheckBox();
            this.chkCALL = new System.Windows.Forms.CheckBox();
            this.chkSMS = new System.Windows.Forms.CheckBox();
            this.grdapplyAll = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.grdEMAIL = new System.Windows.Forms.DataGridView();
            this.grdCALL = new System.Windows.Forms.DataGridView();
            this.grdSMS = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.drpTwilioLang = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtappointmentType = new System.Windows.Forms.TextBox();
            this.lblHead = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.txtBody_Template = new MSDN.Html.Editor.HtmlEditorControl();
            this.label3 = new System.Windows.Forms.Label();
            this.drptemplete = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Submiteventbtn = new System.Windows.Forms.Button();
            this.txttitle_Template = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.createappointmentpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdapplyAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdEMAIL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCALL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSMS)).BeginInit();
            this.SuspendLayout();
            // 
            // createappointmentpanel
            // 
            this.createappointmentpanel.AutoScroll = true;
            this.createappointmentpanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.createappointmentpanel.Controls.Add(this.chkIsConfirmable);
            this.createappointmentpanel.Controls.Add(this.label16);
            this.createappointmentpanel.Controls.Add(this.chkEMAIL);
            this.createappointmentpanel.Controls.Add(this.chkCALL);
            this.createappointmentpanel.Controls.Add(this.chkSMS);
            this.createappointmentpanel.Controls.Add(this.grdapplyAll);
            this.createappointmentpanel.Controls.Add(this.label4);
            this.createappointmentpanel.Controls.Add(this.grdEMAIL);
            this.createappointmentpanel.Controls.Add(this.grdCALL);
            this.createappointmentpanel.Controls.Add(this.grdSMS);
            this.createappointmentpanel.Controls.Add(this.label2);
            this.createappointmentpanel.Controls.Add(this.drpTwilioLang);
            this.createappointmentpanel.Controls.Add(this.label1);
            this.createappointmentpanel.Controls.Add(this.txtappointmentType);
            this.createappointmentpanel.Controls.Add(this.lblHead);
            this.createappointmentpanel.Controls.Add(this.lblID);
            this.createappointmentpanel.Controls.Add(this.txtBody_Template);
            this.createappointmentpanel.Controls.Add(this.label3);
            this.createappointmentpanel.Controls.Add(this.drptemplete);
            this.createappointmentpanel.Controls.Add(this.button1);
            this.createappointmentpanel.Controls.Add(this.Submiteventbtn);
            this.createappointmentpanel.Controls.Add(this.txttitle_Template);
            this.createappointmentpanel.Controls.Add(this.label20);
            this.createappointmentpanel.Controls.Add(this.label18);
            this.createappointmentpanel.Location = new System.Drawing.Point(0, 0);
            this.createappointmentpanel.Name = "createappointmentpanel";
            this.createappointmentpanel.Size = new System.Drawing.Size(723, 722);
            this.createappointmentpanel.TabIndex = 23;
            // 
            // chkIsConfirmable
            // 
            this.chkIsConfirmable.AutoSize = true;
            this.chkIsConfirmable.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.chkIsConfirmable.ForeColor = System.Drawing.Color.SteelBlue;
            this.chkIsConfirmable.Location = new System.Drawing.Point(433, 401);
            this.chkIsConfirmable.Name = "chkIsConfirmable";
            this.chkIsConfirmable.Size = new System.Drawing.Size(234, 19);
            this.chkIsConfirmable.TabIndex = 122;
            this.chkIsConfirmable.Text = "Disable Cancel/Reschedule Options.";
            this.chkIsConfirmable.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(22, 429);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(122, 19);
            this.label16.TabIndex = 121;
            this.label16.Text = "Reminder Types :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkEMAIL
            // 
            this.chkEMAIL.AutoSize = true;
            this.chkEMAIL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEMAIL.Location = new System.Drawing.Point(435, 434);
            this.chkEMAIL.Name = "chkEMAIL";
            this.chkEMAIL.Size = new System.Drawing.Size(65, 18);
            this.chkEMAIL.TabIndex = 120;
            this.chkEMAIL.Text = "EMAIL";
            this.chkEMAIL.UseVisualStyleBackColor = true;
            this.chkEMAIL.CheckedChanged += new System.EventHandler(this.chkEMAIL_CheckedChanged);
            // 
            // chkCALL
            // 
            this.chkCALL.AutoSize = true;
            this.chkCALL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCALL.Location = new System.Drawing.Point(319, 434);
            this.chkCALL.Name = "chkCALL";
            this.chkCALL.Size = new System.Drawing.Size(57, 18);
            this.chkCALL.TabIndex = 119;
            this.chkCALL.Text = "CALL";
            this.chkCALL.UseVisualStyleBackColor = true;
            this.chkCALL.CheckedChanged += new System.EventHandler(this.chkCALL_CheckedChanged);
            // 
            // chkSMS
            // 
            this.chkSMS.AutoSize = true;
            this.chkSMS.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSMS.Location = new System.Drawing.Point(204, 434);
            this.chkSMS.Name = "chkSMS";
            this.chkSMS.Size = new System.Drawing.Size(53, 18);
            this.chkSMS.TabIndex = 118;
            this.chkSMS.Text = "SMS";
            this.chkSMS.UseVisualStyleBackColor = true;
            this.chkSMS.CheckedChanged += new System.EventHandler(this.chkSMS_CheckedChanged);
            // 
            // grdapplyAll
            // 
            this.grdapplyAll.AllowUserToAddRows = false;
            this.grdapplyAll.AllowUserToResizeColumns = false;
            this.grdapplyAll.AllowUserToResizeRows = false;
            this.grdapplyAll.BackgroundColor = System.Drawing.Color.White;
            this.grdapplyAll.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdapplyAll.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdapplyAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdapplyAll.ColumnHeadersVisible = false;
            this.grdapplyAll.Location = new System.Drawing.Point(150, 460);
            this.grdapplyAll.Name = "grdapplyAll";
            this.grdapplyAll.RowHeadersVisible = false;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            this.grdapplyAll.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.grdapplyAll.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdapplyAll.ShowCellToolTips = false;
            this.grdapplyAll.Size = new System.Drawing.Size(30, 199);
            this.grdapplyAll.TabIndex = 117;
            this.grdapplyAll.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdapplyAll_CellContentClick);
            this.grdapplyAll.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdapplyAll_CellContentDoubleClick);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(64, 460);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 19);
            this.label4.TabIndex = 116;
            this.label4.Text = "When :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grdEMAIL
            // 
            this.grdEMAIL.AllowUserToAddRows = false;
            this.grdEMAIL.AllowUserToResizeColumns = false;
            this.grdEMAIL.AllowUserToResizeRows = false;
            this.grdEMAIL.BackgroundColor = System.Drawing.Color.White;
            this.grdEMAIL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdEMAIL.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdEMAIL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEMAIL.ColumnHeadersVisible = false;
            this.grdEMAIL.Location = new System.Drawing.Point(428, 460);
            this.grdEMAIL.Name = "grdEMAIL";
            this.grdEMAIL.RowHeadersVisible = false;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdEMAIL.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.grdEMAIL.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grdEMAIL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdEMAIL.Size = new System.Drawing.Size(100, 199);
            this.grdEMAIL.TabIndex = 115;
            this.grdEMAIL.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdEMAIL_CellContentClick);
            this.grdEMAIL.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdEMAIL_CellValueChanged);
            this.grdEMAIL.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdEMAIL_CurrentCellDirtyStateChanged);
            // 
            // grdCALL
            // 
            this.grdCALL.AllowUserToAddRows = false;
            this.grdCALL.AllowUserToResizeColumns = false;
            this.grdCALL.AllowUserToResizeRows = false;
            this.grdCALL.BackgroundColor = System.Drawing.Color.White;
            this.grdCALL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCALL.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdCALL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCALL.ColumnHeadersVisible = false;
            this.grdCALL.Location = new System.Drawing.Point(311, 460);
            this.grdCALL.Name = "grdCALL";
            this.grdCALL.RowHeadersVisible = false;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdCALL.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.grdCALL.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grdCALL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdCALL.Size = new System.Drawing.Size(100, 199);
            this.grdCALL.TabIndex = 114;
            this.grdCALL.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCALL_CellContentClick);
            this.grdCALL.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCALL_CellValueChanged);
            this.grdCALL.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdCALL_CurrentCellDirtyStateChanged);
            // 
            // grdSMS
            // 
            this.grdSMS.AllowUserToAddRows = false;
            this.grdSMS.AllowUserToResizeColumns = false;
            this.grdSMS.AllowUserToResizeRows = false;
            this.grdSMS.BackgroundColor = System.Drawing.Color.White;
            this.grdSMS.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdSMS.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdSMS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSMS.ColumnHeadersVisible = false;
            this.grdSMS.Location = new System.Drawing.Point(197, 460);
            this.grdSMS.Name = "grdSMS";
            this.grdSMS.RowHeadersVisible = false;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdSMS.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.grdSMS.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdSMS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdSMS.ShowCellToolTips = false;
            this.grdSMS.Size = new System.Drawing.Size(100, 199);
            this.grdSMS.TabIndex = 113;
            this.grdSMS.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSMS_CellContentClick);
            this.grdSMS.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSMS_CellValueChanged);
            this.grdSMS.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdSMS_CurrentCellDirtyStateChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(60, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 19);
            this.label2.TabIndex = 112;
            this.label2.Text = "Language :";
            // 
            // drpTwilioLang
            // 
            this.drpTwilioLang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpTwilioLang.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drpTwilioLang.FormattingEnabled = true;
            this.drpTwilioLang.Location = new System.Drawing.Point(148, 147);
            this.drpTwilioLang.Name = "drpTwilioLang";
            this.drpTwilioLang.Size = new System.Drawing.Size(241, 23);
            this.drpTwilioLang.TabIndex = 111;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(-1, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 19);
            this.label1.TabIndex = 110;
            this.label1.Text = "Appointment Type :";
            // 
            // txtappointmentType
            // 
            this.txtappointmentType.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtappointmentType.Location = new System.Drawing.Point(149, 108);
            this.txtappointmentType.Name = "txtappointmentType";
            this.txtappointmentType.Size = new System.Drawing.Size(518, 23);
            this.txtappointmentType.TabIndex = 109;
            // 
            // lblHead
            // 
            this.lblHead.BackColor = System.Drawing.Color.SteelBlue;
            this.lblHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHead.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead.ForeColor = System.Drawing.Color.White;
            this.lblHead.Location = new System.Drawing.Point(0, 0);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(721, 39);
            this.lblHead.TabIndex = 108;
            this.lblHead.Text = "Create Appointment Template ";
            this.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.ForeColor = System.Drawing.Color.White;
            this.lblID.Location = new System.Drawing.Point(484, 384);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(62, 13);
            this.lblID.TabIndex = 107;
            this.lblID.Text = "TemplateID";
            // 
            // txtBody_Template
            // 
            this.txtBody_Template.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBody_Template.InnerText = null;
            this.txtBody_Template.Location = new System.Drawing.Point(146, 186);
            this.txtBody_Template.Name = "txtBody_Template";
            this.txtBody_Template.Size = new System.Drawing.Size(518, 195);
            this.txtBody_Template.TabIndex = 106;
            this.txtBody_Template.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBody_Template_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(52, 398);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 19);
            this.label3.TabIndex = 99;
            this.label3.Text = "Insert Field :";
            // 
            // drptemplete
            // 
            this.drptemplete.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drptemplete.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drptemplete.FormattingEnabled = true;
            this.drptemplete.Location = new System.Drawing.Point(148, 397);
            this.drptemplete.Name = "drptemplete";
            this.drptemplete.Size = new System.Drawing.Size(241, 23);
            this.drptemplete.TabIndex = 98;
            this.drptemplete.SelectedIndexChanged += new System.EventHandler(this.drptemplete_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(251, 675);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 33);
            this.button1.TabIndex = 3;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Submiteventbtn
            // 
            this.Submiteventbtn.BackColor = System.Drawing.Color.SteelBlue;
            this.Submiteventbtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Submiteventbtn.ForeColor = System.Drawing.Color.White;
            this.Submiteventbtn.Location = new System.Drawing.Point(143, 675);
            this.Submiteventbtn.Name = "Submiteventbtn";
            this.Submiteventbtn.Size = new System.Drawing.Size(102, 33);
            this.Submiteventbtn.TabIndex = 2;
            this.Submiteventbtn.Text = "Save";
            this.Submiteventbtn.UseVisualStyleBackColor = false;
            this.Submiteventbtn.Click += new System.EventHandler(this.Submiteventbtn_Click);
            // 
            // txttitle_Template
            // 
            this.txttitle_Template.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txttitle_Template.Location = new System.Drawing.Point(148, 69);
            this.txttitle_Template.Name = "txttitle_Template";
            this.txttitle_Template.Size = new System.Drawing.Size(518, 23);
            this.txttitle_Template.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label20.Location = new System.Drawing.Point(92, 188);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 19);
            this.label20.TabIndex = 63;
            this.label20.Text = "Body :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(98, 71);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 19);
            this.label18.TabIndex = 62;
            this.label18.Text = "Title :";
            // 
            // CreateAppointment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(735, 724);
            this.Controls.Add(this.createappointmentpanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(222, 243);
            this.MaximizeBox = false;
            this.Name = "CreateAppointment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Appointment";
            this.Load += new System.EventHandler(this.CreateAppointment_Load);
            this.createappointmentpanel.ResumeLayout(false);
            this.createappointmentpanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdapplyAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdEMAIL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCALL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSMS)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel createappointmentpanel;
        private System.Windows.Forms.TextBox txttitle_Template;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox drptemplete;
        private MSDN.Html.Editor.HtmlEditorControl txtBody_Template;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblHead;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtappointmentType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox drpTwilioLang;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Submiteventbtn;
        private System.Windows.Forms.DataGridView grdapplyAll;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView grdEMAIL;
        private System.Windows.Forms.DataGridView grdCALL;
        private System.Windows.Forms.DataGridView grdSMS;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkEMAIL;
        private System.Windows.Forms.CheckBox chkCALL;
        private System.Windows.Forms.CheckBox chkSMS;
        private System.Windows.Forms.CheckBox chkIsConfirmable;
    }
}