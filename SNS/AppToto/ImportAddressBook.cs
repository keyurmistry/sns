﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetOffice;
using Outlook = NetOffice.OutlookApi;
using NetOffice.OutlookApi.Enums;
using System.Data.SqlClient;
using DataLayer;
using System.Configuration;
using System.Net;
using System.Diagnostics;
using System.IO;
using System.Threading;
using ReminderSystem.Classes;
using Newtonsoft.Json;
using ReminderSystem;

namespace AppToto
{
    public partial class ImportAddressBook : Form
    {

        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        public ImportAddressBook()
        {
            InitializeComponent();
        }

        private void googlelink_Paint_1(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, googlelink.DisplayRectangle, Color.LightGray, ButtonBorderStyle.Solid);
        }

        private void Outlooklink_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, googlelink.DisplayRectangle, Color.LightGray, ButtonBorderStyle.Solid);
        }

        private void Importlink_Paint(object sender, PaintEventArgs e)
        {
            ControlPaint.DrawBorder(e.Graphics, googlelink.DisplayRectangle, Color.LightGray, ButtonBorderStyle.Solid);
        }



        private void pbxlsimport_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            ImportContact importcont = new ImportContact();
            importcont.ShowDialog();

            Cursor.Current = Cursors.Default;
        }

        private void pbOutlook_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            DialogResult dialogResult = MessageBox.Show("Are you sure you want to sync contacts from Outlook?", "Synergy Notification System", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    ImportOUtlook();
                }
                catch
                { }

                Cursor.Current = Cursors.Default;
            }

        }

        private void pbgoogle_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            DialogResult dialogResult = MessageBox.Show("Are you sure you want to sync contacts from Google?", "Synergy Notification System", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {

                bool Internet = CheckForInternetConnection();
                if (Internet == true)
                {
                    try
                    {
                        CallingURL();
                    }
                    catch
                    { }


                }
                else
                {
                    MessageBox.Show("Please Connect System to Internet.");
                }
            }

            Cursor.Current = Cursors.Default;
        }








        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public void CallingURL()
        {
            Cursor.Current = Cursors.WaitCursor;



            string rs_ClientKey = dal.GetClientKey();

            //string clientId = "181001756825-s196pv8fu3kcav5d91lf02n1shqbk05o.apps.googleusercontent.com"; //Cloud Server
            string clientId = "856096110264-jl7stmvgue2rsu1t4vnli7q2ga2nr3d6.apps.googleusercontent.com";

            //string redirectUrl = "http://googlesync.solulab.in/google-contact-api.aspx";//CloudServer

            string redirectUrl = "http://localhost:5244/google-contact-api.aspx"; //Local

            //string redirectUrl = "http://googlesync.solulab.in/tonygooglesync/google-contact-api.aspx";//Mirror Server

            string FinalURL = "https://accounts.google.com/o/oauth2/auth?redirect_uri=" + redirectUrl + "&response_type=code&client_id=" + clientId + "&scope=https://www.google.com/m8/feeds/&approval_prompt=force&access_type=offline&state=" + rs_ClientKey.Trim();


            try
            {
                OpenURLInBrowser(FinalURL);
            }
            catch
            {
            }

            Cursor.Current = Cursors.Default;



        }

        private void OpenURLInBrowser(string url)
        {
            if (!url.StartsWith("http://") && !url.StartsWith("https://"))
            {
                url = "http://" + url;
            }
            try
            {
                browser.Navigate(new Uri(url));
                Application.DoEvents();


            }
            catch (System.UriFormatException)
            {
                return;
            }
        }

        public static string SQLFix(string str)
        {
            if (str == "NA")
                str = "";
            return str.Replace("&nbsp;", "").Replace("'", "''");
        }

        public void ImportOUtlook()
        {        // start outlook
            Outlook.Application outlookApplication = new Outlook.Application();

            // enum contacts 
            int i = 0;
            Outlook.MAPIFolder contactFolder = outlookApplication.Session.GetDefaultFolder(OlDefaultFolders.olFolderContacts);
            foreach (COMObject item in contactFolder.Items)
            {
                Outlook.ContactItem contact = item as Outlook.ContactItem;
                if (null != contact)
                {

                    var FirstName = contact.FirstName;
                    var LastName = contact.LastName;
                    var Email = contact.Email1Address;
                    var Phone = contact.BusinessTelephoneNumber;
                    var Address = contact.BusinessAddress;
                    var DOB = contact.Birthday;
                    var Title = contact.Title;
                    var MiddleName = contact.MiddleName;
                    var Zipcode = contact.MailingAddressPostalCode;


                    insertIntoOutlookUser(FirstName, MiddleName, LastName, Email, Phone, Address, DOB, Title, Zipcode);
                }
            }

            // close outlook and dispose
            outlookApplication.Quit();
            outlookApplication.Dispose();

            MessageBox.Show("Outlook Contacts synchronized Successfully.");
        }


        private void insertIntoOutlookUser(string FirstName, string MiddleName, string LastName, string Email, string Phone, string Address, DateTime DOB, string Title, string Zipcode)
        {
            DataSet dsGetSal1 = new DataSet();
            SqlParameter[] p = new SqlParameter[13];

            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = Helper.UniversalDateFormat;

            p[0] = new SqlParameter("@Salutation", Title);
            p[1] = new SqlParameter("@FName", FirstName);
            p[2] = new SqlParameter("@MName", MiddleName);
            p[3] = new SqlParameter("@LName", LastName);
            p[4] = new SqlParameter("@PhoneNo", Phone);
            p[5] = new SqlParameter("@Email", Email);
            p[6] = new SqlParameter("@Zipcode", Zipcode);
            p[7] = new SqlParameter("@Notes", Address);
            p[8] = new SqlParameter("@DOB", SQLFix(DOB == null ? Convert.ToString(DOB) : Convert.ToDateTime(DOB, dateInfo).ToString(Helper.UniversalDateFormat)));
            p[9] = new SqlParameter("@CountryCode", "");
            p[10] = new SqlParameter("@MobileType", "Mobile");
            p[11] = new SqlParameter("@EmailType", "Work");
            p[12] = new SqlParameter("@ContactType", "Outlook");


            string s = "exec SP_Insert_Contact_Outlook '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "','" + p[9].Value + "','" + p[10].Value + "','" + p[11].Value + "','" + p[12].Value + "'";
            dsGetSal1 = dal.GetData(s, Application.StartupPath);
            string ResultID = string.Empty;
        }




        public void googleContactCloud()
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {

                string ClientproductKey = string.Empty;

                ClientproductKey = dal.GetClientKey();


                var postData = string.Empty;




                var request = (HttpWebRequest)WebRequest.Create("http://www.snsapp.in/Upgrade/rs_UpgradeApplication.svc/GetGoogleContact");



                request.ContentType = "application/json";
                request.Method = "POST";



                postData = "{\"ClientKey\":\"" + ClientproductKey + "\"}";



                var data = Encoding.ASCII.GetBytes(postData);
                request.ContentLength = data.Length;


                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


                List<GoogleSync> listContact = JsonConvert.DeserializeObject<List<GoogleSync>>(responseString);

                for (int j = 0; j <= listContact.Count - 1; j++)
                {
                    try
                    {

                        DataSet dsgoogleStaus = new DataSet();
                        dsgoogleStaus.Clear();
                        string s = "exec sproc_GooglesyncContect_Separate '" + listContact[j].Salutation.ToString() + "','" + listContact[j].FName.ToString() + "','" + listContact[j].MName.ToString() + "','" + listContact[j].LName.ToString() + "','" + listContact[j].PhoneNo.ToString() + "','" + listContact[j].Email.ToString() + "','" + listContact[j].Zipcode.ToString() + "','" + listContact[j].Notes.ToString() + "','" + listContact[j].DOB.ToString() + "','" + listContact[j].CountryCode.ToString() + "','" + listContact[j].MobileType.ToString() + "','" + listContact[j].EmailType.ToString() + "','" + listContact[j].ContactType.ToString() + "','" + listContact[j].ClientKey.ToString() + "'";
                        dsgoogleStaus = dal.GetData(s, Application.StartupPath);
                    }
                    catch
                    { }

                }

            }

            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            Cursor.Current = Cursors.Default;
        }


        string body = string.Empty;
        private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {

                if (body == browser.Document.Body.InnerHtml) return;

                body = browser.Document.Body.InnerHtml;

                if (body.Contains("Your google contacts have been synced successfully with Synergy Notification System."))
                {
                    googleContactCloud();

                    MessageBox.Show("Google Contact Sync Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();
                }
                else
                {

                }
            }
            catch
            { }
            Cursor.Current = Cursors.Default;

        }
    }
}
