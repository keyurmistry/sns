﻿namespace AppToto
{
    partial class Calendaraettingform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label51 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.CalendarTitletxt = new System.Windows.Forms.TextBox();
            this.CalendarNametxt = new System.Windows.Forms.TextBox();
            this.Ownerphonetxt = new System.Windows.Forms.TextBox();
            this.Owneremailtxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.AdminEmailtxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Defaultcombo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.addresscombo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.timecombo = new System.Windows.Forms.ComboBox();
            this.CalendarSavebtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Malgun Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label51.ForeColor = System.Drawing.Color.Gray;
            this.label51.Location = new System.Drawing.Point(4, 8);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(92, 25);
            this.label51.TabIndex = 61;
            this.label51.Text = "Calendar :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Gainsboro;
            this.label17.Location = new System.Drawing.Point(-1, 37);
            this.label17.MaximumSize = new System.Drawing.Size(1000, 2);
            this.label17.MinimumSize = new System.Drawing.Size(1000, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(1000, 2);
            this.label17.TabIndex = 62;
            this.label17.Text = "label17";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label3.Location = new System.Drawing.Point(79, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 15);
            this.label3.TabIndex = 63;
            this.label3.Text = "Calendar Title :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(83, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 15);
            this.label1.TabIndex = 64;
            this.label1.Text = "Owner Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label2.Location = new System.Drawing.Point(83, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 15);
            this.label2.TabIndex = 65;
            this.label2.Text = "Owner Phone :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label4.Location = new System.Drawing.Point(88, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 15);
            this.label4.TabIndex = 66;
            this.label4.Text = "Owner Email :";
            // 
            // CalendarTitletxt
            // 
            this.CalendarTitletxt.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.CalendarTitletxt.Location = new System.Drawing.Point(222, 52);
            this.CalendarTitletxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CalendarTitletxt.Name = "CalendarTitletxt";
            this.CalendarTitletxt.Size = new System.Drawing.Size(289, 23);
            this.CalendarTitletxt.TabIndex = 67;
            // 
            // CalendarNametxt
            // 
            this.CalendarNametxt.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.CalendarNametxt.Location = new System.Drawing.Point(222, 83);
            this.CalendarNametxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CalendarNametxt.Name = "CalendarNametxt";
            this.CalendarNametxt.Size = new System.Drawing.Size(289, 23);
            this.CalendarNametxt.TabIndex = 68;
            // 
            // Ownerphonetxt
            // 
            this.Ownerphonetxt.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Ownerphonetxt.Location = new System.Drawing.Point(222, 114);
            this.Ownerphonetxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Ownerphonetxt.Name = "Ownerphonetxt";
            this.Ownerphonetxt.Size = new System.Drawing.Size(289, 23);
            this.Ownerphonetxt.TabIndex = 69;
            // 
            // Owneremailtxt
            // 
            this.Owneremailtxt.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Owneremailtxt.Location = new System.Drawing.Point(222, 145);
            this.Owneremailtxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Owneremailtxt.Name = "Owneremailtxt";
            this.Owneremailtxt.Size = new System.Drawing.Size(289, 23);
            this.Owneremailtxt.TabIndex = 70;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label5.Location = new System.Drawing.Point(88, 184);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 15);
            this.label5.TabIndex = 71;
            this.label5.Text = "Admin Email :";
            // 
            // AdminEmailtxt
            // 
            this.AdminEmailtxt.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.AdminEmailtxt.Location = new System.Drawing.Point(222, 181);
            this.AdminEmailtxt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AdminEmailtxt.Name = "AdminEmailtxt";
            this.AdminEmailtxt.Size = new System.Drawing.Size(289, 23);
            this.AdminEmailtxt.TabIndex = 72;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label6.Location = new System.Drawing.Point(69, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 15);
            this.label6.TabIndex = 73;
            this.label6.Text = "Default location :";
            // 
            // Defaultcombo
            // 
            this.Defaultcombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Defaultcombo.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Defaultcombo.FormattingEnabled = true;
            this.Defaultcombo.Items.AddRange(new object[] {
            "(Default)"});
            this.Defaultcombo.Location = new System.Drawing.Point(222, 217);
            this.Defaultcombo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Defaultcombo.Name = "Defaultcombo";
            this.Defaultcombo.Size = new System.Drawing.Size(289, 23);
            this.Defaultcombo.TabIndex = 74;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label7.Location = new System.Drawing.Point(78, 256);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 15);
            this.label7.TabIndex = 75;
            this.label7.Text = "Address book :";
            // 
            // addresscombo
            // 
            this.addresscombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.addresscombo.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.addresscombo.FormattingEnabled = true;
            this.addresscombo.Items.AddRange(new object[] {
            "(All address books)",
            "A1 Law Book",
            "Google"});
            this.addresscombo.Location = new System.Drawing.Point(222, 253);
            this.addresscombo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.addresscombo.Name = "addresscombo";
            this.addresscombo.Size = new System.Drawing.Size(289, 23);
            this.addresscombo.TabIndex = 76;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label8.Location = new System.Drawing.Point(100, 289);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 15);
            this.label8.TabIndex = 77;
            this.label8.Text = "Time zone :";
            // 
            // timecombo
            // 
            this.timecombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.timecombo.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.timecombo.FormattingEnabled = true;
           
            this.timecombo.Location = new System.Drawing.Point(222, 286);
            this.timecombo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.timecombo.Name = "timecombo";
            this.timecombo.Size = new System.Drawing.Size(289, 23);
            this.timecombo.TabIndex = 78;
           
            // 
            // CalendarSavebtn
            // 
            this.CalendarSavebtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CalendarSavebtn.BackColor = System.Drawing.Color.WhiteSmoke;
            this.CalendarSavebtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CalendarSavebtn.FlatAppearance.BorderSize = 5;
            this.CalendarSavebtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.CalendarSavebtn.Image = global::ReminderSystem.Properties.Resources._17382;
            this.CalendarSavebtn.Location = new System.Drawing.Point(634, 301);
            this.CalendarSavebtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CalendarSavebtn.Name = "CalendarSavebtn";
            this.CalendarSavebtn.Size = new System.Drawing.Size(60, 34);
            this.CalendarSavebtn.TabIndex = 79;
            this.CalendarSavebtn.UseVisualStyleBackColor = false;
            // 
            // Calendaraettingform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(784, 348);
            this.Controls.Add(this.CalendarSavebtn);
            this.Controls.Add(this.timecombo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.addresscombo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Defaultcombo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.AdminEmailtxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Owneremailtxt);
            this.Controls.Add(this.Ownerphonetxt);
            this.Controls.Add(this.CalendarNametxt);
            this.Controls.Add(this.CalendarTitletxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label51);
            this.MaximizeBox = false;
            this.Name = "Calendaraettingform";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calendar setting form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CalendarTitletxt;
        private System.Windows.Forms.TextBox CalendarNametxt;
        private System.Windows.Forms.TextBox Ownerphonetxt;
        private System.Windows.Forms.TextBox Owneremailtxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox AdminEmailtxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox Defaultcombo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox addresscombo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox timecombo;
        private System.Windows.Forms.Button CalendarSavebtn;
    }
}