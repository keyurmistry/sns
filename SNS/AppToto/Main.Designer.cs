﻿using ReminderSystem;
namespace AppToto
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.InboxOutbox = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.Settingtb = new System.Windows.Forms.TabPage();
            this.pnlSetting = new System.Windows.Forms.Panel();
            this.SettingSubtab = new System.Windows.Forms.TabControl();
            this.Appointmenttb = new System.Windows.Forms.TabPage();
            this.btnappointmentRefresh = new System.Windows.Forms.Button();
            this.crtnewtempbtn = new System.Windows.Forms.Button();
            this.grdTemplateList = new System.Windows.Forms.DataGridView();
            this.Notificationtb = new System.Windows.Forms.TabPage();
            this.Notificationtabpanel = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEmailSendTo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Savebtn = new System.Windows.Forms.Button();
            this.drpSMSNotication = new System.Windows.Forms.ComboBox();
            this.chkSMSWhen = new System.Windows.Forms.CheckBox();
            this.chkSMSReschedule = new System.Windows.Forms.CheckBox();
            this.ChkSMSCancel = new System.Windows.Forms.CheckBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtSMSSendTo = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.drpEmailNotication = new System.Windows.Forms.ComboBox();
            this.chkEmailWhen = new System.Windows.Forms.CheckBox();
            this.chkEmailReschedule = new System.Windows.Forms.CheckBox();
            this.chkEmailCancel = new System.Windows.Forms.CheckBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Emailsetting = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtClientKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnEmailSettings = new System.Windows.Forms.Button();
            this.grbCall = new System.Windows.Forms.GroupBox();
            this.txtAuthoToken = new System.Windows.Forms.TextBox();
            this.txtCallNo = new System.Windows.Forms.TextBox();
            this.txtAccountSID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.grbSMS = new System.Windows.Forms.GroupBox();
            this.txtSMSSenderNo = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.grbEmail = new System.Windows.Forms.GroupBox();
            this.picEmailSend = new System.Windows.Forms.PictureBox();
            this.btnTestMail = new System.Windows.Forms.Button();
            this.txtSMTPPort = new System.Windows.Forms.TextBox();
            this.txtSMTPhost = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtEmailSubject = new System.Windows.Forms.TextBox();
            this.txtEmailID = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.OtherSettings = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label58 = new System.Windows.Forms.Label();
            this.btnTriggerPramotionlas = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnbrowsepramotionals = new System.Windows.Forms.Button();
            this.txtCSVpathpramotionals = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btnCSVupload = new System.Windows.Forms.Button();
            this.chkAPI = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnuploadBatfile = new System.Windows.Forms.Button();
            this.txtAPIbatfile = new System.Windows.Forms.TextBox();
            this.Browsebtn = new System.Windows.Forms.Button();
            this.txtbrowseFile = new System.Windows.Forms.TextBox();
            this.lblbatFilePath = new System.Windows.Forms.Label();
            this.lblcsvFilePath = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btnrecallUpdate = new System.Windows.Forms.Button();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.drpReinterval = new System.Windows.Forms.ComboBox();
            this.drpHMT = new System.Windows.Forms.ComboBox();
            this.pnlrecallSetting = new System.Windows.Forms.Panel();
            this.chkrecall = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btnA1sync = new System.Windows.Forms.Button();
            this.chkA1toRS = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label48 = new System.Windows.Forms.Label();
            this.btnUpdateBulk = new System.Windows.Forms.Button();
            this.pnlbulkTiming = new System.Windows.Forms.Panel();
            this.grdEMAIL = new System.Windows.Forms.DataGridView();
            this.grdapplyAll = new System.Windows.Forms.DataGridView();
            this.chkSMS = new System.Windows.Forms.CheckBox();
            this.chkCALL = new System.Windows.Forms.CheckBox();
            this.grdCALL = new System.Windows.Forms.DataGridView();
            this.chkEMAIL = new System.Windows.Forms.CheckBox();
            this.grdSMS = new System.Windows.Forms.DataGridView();
            this.drpBulkType = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnChangeTimeZone = new System.Windows.Forms.Button();
            this.drpTimeZone = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnpasswordChange = new System.Windows.Forms.Button();
            this.txtConfirmPassword = new System.Windows.Forms.TextBox();
            this.txtNewpassword = new System.Windows.Forms.TextBox();
            this.txtOldpassword = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.grdtimeSlot = new System.Windows.Forms.DataGridView();
            this.btnAddtime = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.drptimeSlot = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnRemoveLogo = new System.Windows.Forms.Button();
            this.lblPriviewLogo = new System.Windows.Forms.Label();
            this.pblogo = new System.Windows.Forms.PictureBox();
            this.lblImagename = new System.Windows.Forms.Label();
            this.txtsignature = new System.Windows.Forms.TextBox();
            this.btnUpload = new System.Windows.Forms.Button();
            this.btnCompanyLogo = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCompanyLogo = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.Appoint = new System.Windows.Forms.Label();
            this.grdLogColor = new System.Windows.Forms.DataGridView();
            this.grdReminderColor = new System.Windows.Forms.DataGridView();
            this.RegistrationDetailstb = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblRegistrationMac = new System.Windows.Forms.Label();
            this.btnRegiCancel = new System.Windows.Forms.Button();
            this.btnregistrationUpdate = new System.Windows.Forms.Button();
            this.label50 = new System.Windows.Forms.Label();
            this.txtproductkey = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmailID = new System.Windows.Forms.Label();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.Reportstab = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label53 = new System.Windows.Forms.Label();
            this.btnReportExport = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.enddateTimePickerReport = new System.Windows.Forms.DateTimePicker();
            this.label52 = new System.Windows.Forms.Label();
            this.strtdateTimePickerReport = new System.Windows.Forms.DateTimePicker();
            this.pnlPassword = new System.Windows.Forms.Panel();
            this.txtpasswordSetting = new System.Windows.Forms.TextBox();
            this.txtusername = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.btncancel = new System.Windows.Forms.Button();
            this.btnsignin = new System.Windows.Forms.Button();
            this.lblHead = new System.Windows.Forms.Label();
            this.Logtb = new System.Windows.Forms.TabPage();
            this.btnDateSearchLog = new System.Windows.Forms.Button();
            this.txtTwilioLogsearch = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlLogDeliverd = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pnlLogNoans = new System.Windows.Forms.Panel();
            this.pnlLogUndeliverd = new System.Windows.Forms.Panel();
            this.pnlLogComplete = new System.Windows.Forms.Panel();
            this.txtTwilioFilterTo = new System.Windows.Forms.DateTimePicker();
            this.txtTwilioFilterFrom = new System.Windows.Forms.DateTimePicker();
            this.lblTwilioTO = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.drpogFilter = new System.Windows.Forms.ComboBox();
            this.grdTwilioLog = new System.Windows.Forms.DataGridView();
            this.btnRefeshTwilioLog = new System.Windows.Forms.Button();
            this.Contact = new System.Windows.Forms.TabPage();
            this.searchtxt = new System.Windows.Forms.TextBox();
            this.Cntimportbtn = new System.Windows.Forms.Button();
            this.Contactgridview = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.addcontbtn = new System.Windows.Forms.Button();
            this.btnrefreshContact = new System.Windows.Forms.Button();
            this.Calendartab = new System.Windows.Forms.TabPage();
            this.btnPendingEvent = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtgridSearch = new System.Windows.Forms.TextBox();
            this.chkprivateFilter = new System.Windows.Forms.CheckBox();
            this.btnsync = new System.Windows.Forms.Button();
            this.btnserchfirst = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.grdAgendadata = new System.Windows.Forms.DataGridView();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pnlreschedule = new System.Windows.Forms.Panel();
            this.pnlnorepsonce = new System.Windows.Forms.Panel();
            this.pnlcancel = new System.Windows.Forms.Panel();
            this.pnlcallback = new System.Windows.Forms.Panel();
            this.pnlconfirm = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.txtsearchValue = new System.Windows.Forms.TextBox();
            this.lbldate = new System.Windows.Forms.Label();
            this.enddateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.strtdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.tolbl = new System.Windows.Forms.Label();
            this.Monthbtn = new System.Windows.Forms.Button();
            this.line = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.AddAppointment = new System.Windows.Forms.Button();
            this.Datecombo = new System.Windows.Forms.ComboBox();
            this.Calendars = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnDeleteAddBook = new System.Windows.Forms.Button();
            this.btnSaveAddBook = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.txtSearchAddBook = new System.Windows.Forms.TextBox();
            this.lblAddBookTypeId = new System.Windows.Forms.Label();
            this.chkAllAddBook = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btnAddNewAddBook = new System.Windows.Forms.Button();
            this.dgvAddressBook = new System.Windows.Forms.DataGridView();
            this.txtAddBookType = new System.Windows.Forms.TextBox();
            this.dgvAddressBookList = new System.Windows.Forms.DataGridView();
            this.lblApplicationVersion = new System.Windows.Forms.Label();
            this.tmrAppointment = new System.Windows.Forms.Timer(this.components);
            this.tmrTwiliolog = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrbadgenotification = new System.Windows.Forms.Timer(this.components);
            this.tmrAPI = new System.Windows.Forms.Timer(this.components);
            this.tmrTwoSync = new System.Windows.Forms.Timer(this.components);
            this.tooltipPenddingreminder = new System.Windows.Forms.ToolTip(this.components);
            this.pbhelp = new System.Windows.Forms.PictureBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label59 = new System.Windows.Forms.Label();
            this.cbLanguage = new System.Windows.Forms.ComboBox();
            this.btnSetLanguage = new System.Windows.Forms.Button();
            this.Settingtb.SuspendLayout();
            this.pnlSetting.SuspendLayout();
            this.SettingSubtab.SuspendLayout();
            this.Appointmenttb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTemplateList)).BeginInit();
            this.Notificationtb.SuspendLayout();
            this.Notificationtabpanel.SuspendLayout();
            this.Emailsetting.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.grbCall.SuspendLayout();
            this.grbSMS.SuspendLayout();
            this.grbEmail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmailSend)).BeginInit();
            this.OtherSettings.SuspendLayout();
            this.panel10.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.pnlbulkTiming.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEMAIL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdapplyAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCALL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSMS)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdtimeSlot)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pblogo)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLogColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdReminderColor)).BeginInit();
            this.RegistrationDetailstb.SuspendLayout();
            this.panel1.SuspendLayout();
            this.Reportstab.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlPassword.SuspendLayout();
            this.Logtb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTwilioLog)).BeginInit();
            this.Contact.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Contactgridview)).BeginInit();
            this.Calendartab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAgendadata)).BeginInit();
            this.Calendars.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddressBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddressBookList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbhelp)).BeginInit();
            this.groupBox13.SuspendLayout();
            this.SuspendLayout();
            // 
            // InboxOutbox
            // 
            this.InboxOutbox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.InboxOutbox.Location = new System.Drawing.Point(0, 0);
            this.InboxOutbox.Name = "InboxOutbox";
            this.InboxOutbox.Size = new System.Drawing.Size(200, 100);
            this.InboxOutbox.TabIndex = 0;
            this.InboxOutbox.Text = "Inbox/Outbox";
            // 
            // tabPage5
            // 
            this.tabPage5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage5.Location = new System.Drawing.Point(0, 0);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(200, 100);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Log";
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(0, 0);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(200, 100);
            this.tabPage6.TabIndex = 0;
            // 
            // Settingtb
            // 
            this.Settingtb.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Settingtb.Controls.Add(this.pnlSetting);
            this.Settingtb.Controls.Add(this.pnlPassword);
            this.Settingtb.Location = new System.Drawing.Point(4, 24);
            this.Settingtb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Settingtb.Name = "Settingtb";
            this.Settingtb.Size = new System.Drawing.Size(976, 479);
            this.Settingtb.TabIndex = 3;
            this.Settingtb.Text = "Settings";
            this.Settingtb.UseVisualStyleBackColor = true;
            // 
            // pnlSetting
            // 
            this.pnlSetting.Controls.Add(this.SettingSubtab);
            this.pnlSetting.Location = new System.Drawing.Point(6, 3);
            this.pnlSetting.Name = "pnlSetting";
            this.pnlSetting.Size = new System.Drawing.Size(960, 466);
            this.pnlSetting.TabIndex = 3;
            this.pnlSetting.Visible = false;
            // 
            // SettingSubtab
            // 
            this.SettingSubtab.Controls.Add(this.Appointmenttb);
            this.SettingSubtab.Controls.Add(this.Notificationtb);
            this.SettingSubtab.Controls.Add(this.Emailsetting);
            this.SettingSubtab.Controls.Add(this.OtherSettings);
            this.SettingSubtab.Controls.Add(this.RegistrationDetailstb);
            this.SettingSubtab.Controls.Add(this.Reportstab);
            this.SettingSubtab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SettingSubtab.Location = new System.Drawing.Point(0, 0);
            this.SettingSubtab.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SettingSubtab.Name = "SettingSubtab";
            this.SettingSubtab.SelectedIndex = 0;
            this.SettingSubtab.Size = new System.Drawing.Size(960, 466);
            this.SettingSubtab.TabIndex = 1;
            this.SettingSubtab.SelectedIndexChanged += new System.EventHandler(this.SettingSubtab_SelectedIndexChanged);
            // 
            // Appointmenttb
            // 
            this.Appointmenttb.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Appointmenttb.Controls.Add(this.btnappointmentRefresh);
            this.Appointmenttb.Controls.Add(this.crtnewtempbtn);
            this.Appointmenttb.Controls.Add(this.grdTemplateList);
            this.Appointmenttb.Location = new System.Drawing.Point(4, 24);
            this.Appointmenttb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Appointmenttb.Name = "Appointmenttb";
            this.Appointmenttb.Size = new System.Drawing.Size(952, 438);
            this.Appointmenttb.TabIndex = 0;
            this.Appointmenttb.Text = "Appointment Page";
            // 
            // btnappointmentRefresh
            // 
            this.btnappointmentRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnappointmentRefresh.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnappointmentRefresh.FlatAppearance.BorderSize = 5;
            this.btnappointmentRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnappointmentRefresh.Image = global::SynergyNotificationSystem.Properties.Resources.ajax_refresh_icon;
            this.btnappointmentRefresh.Location = new System.Drawing.Point(925, 10);
            this.btnappointmentRefresh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnappointmentRefresh.MaximumSize = new System.Drawing.Size(35, 28);
            this.btnappointmentRefresh.MinimumSize = new System.Drawing.Size(35, 28);
            this.btnappointmentRefresh.Name = "btnappointmentRefresh";
            this.btnappointmentRefresh.Size = new System.Drawing.Size(35, 28);
            this.btnappointmentRefresh.TabIndex = 63;
            this.btnappointmentRefresh.UseVisualStyleBackColor = false;
            this.btnappointmentRefresh.Click += new System.EventHandler(this.btnappointmentRefresh_Click);
            // 
            // crtnewtempbtn
            // 
            this.crtnewtempbtn.BackColor = System.Drawing.Color.SteelBlue;
            this.crtnewtempbtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.crtnewtempbtn.ForeColor = System.Drawing.Color.White;
            this.crtnewtempbtn.Location = new System.Drawing.Point(759, 10);
            this.crtnewtempbtn.Name = "crtnewtempbtn";
            this.crtnewtempbtn.Size = new System.Drawing.Size(158, 29);
            this.crtnewtempbtn.TabIndex = 62;
            this.crtnewtempbtn.Text = "+ Add New Template";
            this.crtnewtempbtn.UseVisualStyleBackColor = false;
            this.crtnewtempbtn.Click += new System.EventHandler(this.crtnewtempbtn_Click);
            // 
            // grdTemplateList
            // 
            this.grdTemplateList.AllowUserToAddRows = false;
            this.grdTemplateList.AllowUserToDeleteRows = false;
            this.grdTemplateList.AllowUserToResizeRows = false;
            this.grdTemplateList.BackgroundColor = System.Drawing.Color.White;
            this.grdTemplateList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdTemplateList.DefaultCellStyle = dataGridViewCellStyle1;
            this.grdTemplateList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdTemplateList.Location = new System.Drawing.Point(0, 47);
            this.grdTemplateList.Name = "grdTemplateList";
            this.grdTemplateList.RowHeadersVisible = false;
            this.grdTemplateList.RowTemplate.Height = 25;
            this.grdTemplateList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdTemplateList.Size = new System.Drawing.Size(962, 400);
            this.grdTemplateList.TabIndex = 61;
            this.grdTemplateList.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdTemplateList_CellContentClick);
            this.grdTemplateList.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdTemplateList_CellDoubleClick);
            this.grdTemplateList.Paint += new System.Windows.Forms.PaintEventHandler(this.grdTemplateList_Paint);
            // 
            // Notificationtb
            // 
            this.Notificationtb.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Notificationtb.Controls.Add(this.Notificationtabpanel);
            this.Notificationtb.Controls.Add(this.label14);
            this.Notificationtb.Location = new System.Drawing.Point(4, 24);
            this.Notificationtb.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Notificationtb.Name = "Notificationtb";
            this.Notificationtb.Size = new System.Drawing.Size(952, 438);
            this.Notificationtb.TabIndex = 0;
            this.Notificationtb.Text = "Notifications";
            // 
            // Notificationtabpanel
            // 
            this.Notificationtabpanel.AutoScroll = true;
            this.Notificationtabpanel.Controls.Add(this.label15);
            this.Notificationtabpanel.Controls.Add(this.label12);
            this.Notificationtabpanel.Controls.Add(this.txtEmailSendTo);
            this.Notificationtabpanel.Controls.Add(this.label13);
            this.Notificationtabpanel.Controls.Add(this.Savebtn);
            this.Notificationtabpanel.Controls.Add(this.drpSMSNotication);
            this.Notificationtabpanel.Controls.Add(this.chkSMSWhen);
            this.Notificationtabpanel.Controls.Add(this.chkSMSReschedule);
            this.Notificationtabpanel.Controls.Add(this.ChkSMSCancel);
            this.Notificationtabpanel.Controls.Add(this.label44);
            this.Notificationtabpanel.Controls.Add(this.txtSMSSendTo);
            this.Notificationtabpanel.Controls.Add(this.label39);
            this.Notificationtabpanel.Controls.Add(this.drpEmailNotication);
            this.Notificationtabpanel.Controls.Add(this.chkEmailWhen);
            this.Notificationtabpanel.Controls.Add(this.chkEmailReschedule);
            this.Notificationtabpanel.Controls.Add(this.chkEmailCancel);
            this.Notificationtabpanel.Controls.Add(this.label40);
            this.Notificationtabpanel.Controls.Add(this.label41);
            this.Notificationtabpanel.Controls.Add(this.label35);
            this.Notificationtabpanel.Controls.Add(this.label34);
            this.Notificationtabpanel.Controls.Add(this.label33);
            this.Notificationtabpanel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Notificationtabpanel.Location = new System.Drawing.Point(2, 0);
            this.Notificationtabpanel.Name = "Notificationtabpanel";
            this.Notificationtabpanel.Size = new System.Drawing.Size(968, 444);
            this.Notificationtabpanel.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.DarkRed;
            this.label15.Location = new System.Drawing.Point(221, 261);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(367, 15);
            this.label15.TabIndex = 113;
            this.label15.Text = "* you can add multiple mobile number separated by commas ( , ) ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.DarkRed;
            this.label12.Location = new System.Drawing.Point(221, 75);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(357, 15);
            this.label12.TabIndex = 111;
            this.label12.Text = "* you can add multiple email address separated by commas ( , ) ";
            // 
            // txtEmailSendTo
            // 
            this.txtEmailSendTo.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.txtEmailSendTo.Location = new System.Drawing.Point(223, 49);
            this.txtEmailSendTo.Name = "txtEmailSendTo";
            this.txtEmailSendTo.Size = new System.Drawing.Size(332, 23);
            this.txtEmailSendTo.TabIndex = 110;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Gainsboro;
            this.label13.Location = new System.Drawing.Point(9, 38);
            this.label13.MaximumSize = new System.Drawing.Size(1000, 2);
            this.label13.MinimumSize = new System.Drawing.Size(950, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(950, 2);
            this.label13.TabIndex = 108;
            this.label13.Text = "label13";
            // 
            // Savebtn
            // 
            this.Savebtn.BackColor = System.Drawing.Color.SteelBlue;
            this.Savebtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Savebtn.ForeColor = System.Drawing.Color.White;
            this.Savebtn.Location = new System.Drawing.Point(375, 403);
            this.Savebtn.Name = "Savebtn";
            this.Savebtn.Size = new System.Drawing.Size(178, 33);
            this.Savebtn.TabIndex = 107;
            this.Savebtn.Text = "Save Changes";
            this.Savebtn.UseVisualStyleBackColor = false;
            this.Savebtn.Click += new System.EventHandler(this.Savebtn_Click);
            // 
            // drpSMSNotication
            // 
            this.drpSMSNotication.Cursor = System.Windows.Forms.Cursors.Hand;
            this.drpSMSNotication.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpSMSNotication.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drpSMSNotication.FormattingEnabled = true;
            this.drpSMSNotication.Location = new System.Drawing.Point(378, 349);
            this.drpSMSNotication.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.drpSMSNotication.Name = "drpSMSNotication";
            this.drpSMSNotication.Size = new System.Drawing.Size(108, 23);
            this.drpSMSNotication.TabIndex = 106;
            // 
            // chkSMSWhen
            // 
            this.chkSMSWhen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSMSWhen.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkSMSWhen.Location = new System.Drawing.Point(223, 343);
            this.chkSMSWhen.Name = "chkSMSWhen";
            this.chkSMSWhen.Size = new System.Drawing.Size(154, 24);
            this.chkSMSWhen.TabIndex = 105;
            this.chkSMSWhen.Text = "before appointment ";
            this.chkSMSWhen.UseVisualStyleBackColor = true;
            // 
            // chkSMSReschedule
            // 
            this.chkSMSReschedule.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSMSReschedule.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkSMSReschedule.Location = new System.Drawing.Point(223, 317);
            this.chkSMSReschedule.Name = "chkSMSReschedule";
            this.chkSMSReschedule.Size = new System.Drawing.Size(275, 24);
            this.chkSMSReschedule.TabIndex = 98;
            this.chkSMSReschedule.Text = "Participant requests to be rescheduled ";
            this.chkSMSReschedule.UseVisualStyleBackColor = true;
            // 
            // ChkSMSCancel
            // 
            this.ChkSMSCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ChkSMSCancel.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.ChkSMSCancel.Location = new System.Drawing.Point(223, 296);
            this.ChkSMSCancel.Name = "ChkSMSCancel";
            this.ChkSMSCancel.Size = new System.Drawing.Size(154, 24);
            this.ChkSMSCancel.TabIndex = 97;
            this.ChkSMSCancel.Text = "Participant cancels ";
            this.ChkSMSCancel.UseVisualStyleBackColor = true;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label44.Location = new System.Drawing.Point(134, 296);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(62, 19);
            this.label44.TabIndex = 94;
            this.label44.Text = "When : ";
            // 
            // txtSMSSendTo
            // 
            this.txtSMSSendTo.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtSMSSendTo.Location = new System.Drawing.Point(223, 235);
            this.txtSMSSendTo.Name = "txtSMSSendTo";
            this.txtSMSSendTo.Size = new System.Drawing.Size(332, 23);
            this.txtSMSSendTo.TabIndex = 92;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label39.Location = new System.Drawing.Point(118, 235);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(79, 19);
            this.label39.TabIndex = 91;
            this.label39.Text = "Send To : ";
            // 
            // drpEmailNotication
            // 
            this.drpEmailNotication.Cursor = System.Windows.Forms.Cursors.Default;
            this.drpEmailNotication.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpEmailNotication.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drpEmailNotication.FormattingEnabled = true;
            this.drpEmailNotication.Location = new System.Drawing.Point(380, 153);
            this.drpEmailNotication.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.drpEmailNotication.Name = "drpEmailNotication";
            this.drpEmailNotication.Size = new System.Drawing.Size(108, 23);
            this.drpEmailNotication.TabIndex = 86;
            // 
            // chkEmailWhen
            // 
            this.chkEmailWhen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkEmailWhen.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkEmailWhen.Location = new System.Drawing.Point(225, 152);
            this.chkEmailWhen.Name = "chkEmailWhen";
            this.chkEmailWhen.Size = new System.Drawing.Size(154, 24);
            this.chkEmailWhen.TabIndex = 85;
            this.chkEmailWhen.Text = "before appointment ";
            this.chkEmailWhen.UseVisualStyleBackColor = true;
            // 
            // chkEmailReschedule
            // 
            this.chkEmailReschedule.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkEmailReschedule.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkEmailReschedule.Location = new System.Drawing.Point(225, 127);
            this.chkEmailReschedule.Name = "chkEmailReschedule";
            this.chkEmailReschedule.Size = new System.Drawing.Size(285, 24);
            this.chkEmailReschedule.TabIndex = 78;
            this.chkEmailReschedule.Text = "Participant requests to be rescheduled ";
            this.chkEmailReschedule.UseVisualStyleBackColor = true;
            // 
            // chkEmailCancel
            // 
            this.chkEmailCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkEmailCancel.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.chkEmailCancel.Location = new System.Drawing.Point(225, 107);
            this.chkEmailCancel.Name = "chkEmailCancel";
            this.chkEmailCancel.Size = new System.Drawing.Size(154, 24);
            this.chkEmailCancel.TabIndex = 77;
            this.chkEmailCancel.Text = "Participant cancels ";
            this.chkEmailCancel.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Gainsboro;
            this.label40.Location = new System.Drawing.Point(12, 216);
            this.label40.MaximumSize = new System.Drawing.Size(1000, 2);
            this.label40.MinimumSize = new System.Drawing.Size(950, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(950, 2);
            this.label40.TabIndex = 67;
            this.label40.Text = "label40";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label41.ForeColor = System.Drawing.Color.Gray;
            this.label41.Location = new System.Drawing.Point(5, 193);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(138, 16);
            this.label41.TabIndex = 66;
            this.label41.Text = "SMS Notifications: ";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label35.Location = new System.Drawing.Point(134, 107);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(62, 19);
            this.label35.TabIndex = 35;
            this.label35.Text = "When : ";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label34.Location = new System.Drawing.Point(118, 53);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(79, 19);
            this.label34.TabIndex = 33;
            this.label34.Text = "Send To : ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold);
            this.label33.ForeColor = System.Drawing.Color.Gray;
            this.label33.Location = new System.Drawing.Point(5, 14);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(145, 16);
            this.label33.TabIndex = 26;
            this.label33.Text = "Email Notifications: ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Gainsboro;
            this.label14.Location = new System.Drawing.Point(-24, 2);
            this.label14.MaximumSize = new System.Drawing.Size(1000, 2);
            this.label14.MinimumSize = new System.Drawing.Size(1000, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(1000, 2);
            this.label14.TabIndex = 19;
            this.label14.Text = "label14";
            // 
            // Emailsetting
            // 
            this.Emailsetting.BackColor = System.Drawing.Color.White;
            this.Emailsetting.Controls.Add(this.groupBox6);
            this.Emailsetting.Controls.Add(this.btnEmailSettings);
            this.Emailsetting.Controls.Add(this.grbCall);
            this.Emailsetting.Controls.Add(this.grbSMS);
            this.Emailsetting.Controls.Add(this.grbEmail);
            this.Emailsetting.ForeColor = System.Drawing.Color.White;
            this.Emailsetting.Location = new System.Drawing.Point(4, 24);
            this.Emailsetting.Name = "Emailsetting";
            this.Emailsetting.Size = new System.Drawing.Size(952, 438);
            this.Emailsetting.TabIndex = 1;
            this.Emailsetting.Text = "Email / Call / SMS Setting";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtClientKey);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Location = new System.Drawing.Point(485, 311);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(473, 85);
            this.groupBox6.TabIndex = 19;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Product Key";
            // 
            // txtClientKey
            // 
            this.txtClientKey.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClientKey.Location = new System.Drawing.Point(15, 40);
            this.txtClientKey.Name = "txtClientKey";
            this.txtClientKey.Size = new System.Drawing.Size(375, 23);
            this.txtClientKey.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(11, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "Key";
            // 
            // btnEmailSettings
            // 
            this.btnEmailSettings.BackColor = System.Drawing.Color.SteelBlue;
            this.btnEmailSettings.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnEmailSettings.Location = new System.Drawing.Point(440, 402);
            this.btnEmailSettings.Name = "btnEmailSettings";
            this.btnEmailSettings.Size = new System.Drawing.Size(75, 29);
            this.btnEmailSettings.TabIndex = 20;
            this.btnEmailSettings.Text = "Update";
            this.btnEmailSettings.UseVisualStyleBackColor = false;
            this.btnEmailSettings.Click += new System.EventHandler(this.btnEmailSettings_Click);
            // 
            // grbCall
            // 
            this.grbCall.Controls.Add(this.txtAuthoToken);
            this.grbCall.Controls.Add(this.txtCallNo);
            this.grbCall.Controls.Add(this.txtAccountSID);
            this.grbCall.Controls.Add(this.label4);
            this.grbCall.Controls.Add(this.label17);
            this.grbCall.Controls.Add(this.label32);
            this.grbCall.Location = new System.Drawing.Point(485, 87);
            this.grbCall.Name = "grbCall";
            this.grbCall.Size = new System.Drawing.Size(476, 218);
            this.grbCall.TabIndex = 18;
            this.grbCall.TabStop = false;
            this.grbCall.Text = "Call";
            // 
            // txtAuthoToken
            // 
            this.txtAuthoToken.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuthoToken.Location = new System.Drawing.Point(15, 172);
            this.txtAuthoToken.Name = "txtAuthoToken";
            this.txtAuthoToken.Size = new System.Drawing.Size(375, 23);
            this.txtAuthoToken.TabIndex = 14;
            // 
            // txtCallNo
            // 
            this.txtCallNo.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCallNo.Location = new System.Drawing.Point(15, 41);
            this.txtCallNo.Name = "txtCallNo";
            this.txtCallNo.Size = new System.Drawing.Size(375, 23);
            this.txtCallNo.TabIndex = 12;
            // 
            // txtAccountSID
            // 
            this.txtAccountSID.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccountSID.Location = new System.Drawing.Point(15, 107);
            this.txtAccountSID.Name = "txtAccountSID";
            this.txtAccountSID.Size = new System.Drawing.Size(375, 23);
            this.txtAccountSID.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(11, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "CALL Number";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(11, 148);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 15);
            this.label17.TabIndex = 11;
            this.label17.Text = "Twilio AuthToken";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(11, 85);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(108, 15);
            this.label32.TabIndex = 10;
            this.label32.Text = "Twilio AccountSid";
            // 
            // grbSMS
            // 
            this.grbSMS.Controls.Add(this.txtSMSSenderNo);
            this.grbSMS.Controls.Add(this.label36);
            this.grbSMS.Location = new System.Drawing.Point(485, 8);
            this.grbSMS.Name = "grbSMS";
            this.grbSMS.Size = new System.Drawing.Size(473, 73);
            this.grbSMS.TabIndex = 17;
            this.grbSMS.TabStop = false;
            this.grbSMS.Text = "SMS";
            // 
            // txtSMSSenderNo
            // 
            this.txtSMSSenderNo.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSMSSenderNo.Location = new System.Drawing.Point(15, 40);
            this.txtSMSSenderNo.Name = "txtSMSSenderNo";
            this.txtSMSSenderNo.Size = new System.Drawing.Size(375, 23);
            this.txtSMSSenderNo.TabIndex = 11;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(11, 20);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(130, 15);
            this.label36.TabIndex = 10;
            this.label36.Text = "SMS Sender Number";
            // 
            // grbEmail
            // 
            this.grbEmail.Controls.Add(this.picEmailSend);
            this.grbEmail.Controls.Add(this.btnTestMail);
            this.grbEmail.Controls.Add(this.txtSMTPPort);
            this.grbEmail.Controls.Add(this.txtSMTPhost);
            this.grbEmail.Controls.Add(this.txtPassword);
            this.grbEmail.Controls.Add(this.label37);
            this.grbEmail.Controls.Add(this.txtEmailSubject);
            this.grbEmail.Controls.Add(this.txtEmailID);
            this.grbEmail.Controls.Add(this.label38);
            this.grbEmail.Controls.Add(this.label42);
            this.grbEmail.Controls.Add(this.label43);
            this.grbEmail.Controls.Add(this.label45);
            this.grbEmail.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.grbEmail.Location = new System.Drawing.Point(8, 8);
            this.grbEmail.Name = "grbEmail";
            this.grbEmail.Size = new System.Drawing.Size(461, 388);
            this.grbEmail.TabIndex = 16;
            this.grbEmail.TabStop = false;
            this.grbEmail.Text = "Email";
            // 
            // picEmailSend
            // 
            this.picEmailSend.Image = global::SynergyNotificationSystem.Properties.Resources.Emailsend;
            this.picEmailSend.Location = new System.Drawing.Point(363, 301);
            this.picEmailSend.Name = "picEmailSend";
            this.picEmailSend.Size = new System.Drawing.Size(32, 32);
            this.picEmailSend.TabIndex = 10;
            this.picEmailSend.TabStop = false;
            this.picEmailSend.Visible = false;
            // 
            // btnTestMail
            // 
            this.btnTestMail.BackColor = System.Drawing.Color.SteelBlue;
            this.btnTestMail.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnTestMail.Location = new System.Drawing.Point(259, 302);
            this.btnTestMail.Name = "btnTestMail";
            this.btnTestMail.Size = new System.Drawing.Size(75, 29);
            this.btnTestMail.TabIndex = 4;
            this.btnTestMail.Text = "Test Mail";
            this.btnTestMail.UseVisualStyleBackColor = false;
            this.btnTestMail.Click += new System.EventHandler(this.btnTestMail_Click);
            // 
            // txtSMTPPort
            // 
            this.txtSMTPPort.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSMTPPort.Location = new System.Drawing.Point(22, 306);
            this.txtSMTPPort.Name = "txtSMTPPort";
            this.txtSMTPPort.Size = new System.Drawing.Size(214, 23);
            this.txtSMTPPort.TabIndex = 9;
            // 
            // txtSMTPhost
            // 
            this.txtSMTPhost.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSMTPhost.Location = new System.Drawing.Point(22, 241);
            this.txtSMTPhost.Name = "txtSMTPhost";
            this.txtSMTPhost.Size = new System.Drawing.Size(431, 23);
            this.txtSMTPhost.TabIndex = 8;
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(20, 175);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(431, 23);
            this.txtPassword.TabIndex = 6;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(16, 151);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(63, 15);
            this.label37.TabIndex = 1;
            this.label37.Text = "Password";
            // 
            // txtEmailSubject
            // 
            this.txtEmailSubject.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailSubject.Location = new System.Drawing.Point(21, 116);
            this.txtEmailSubject.Name = "txtEmailSubject";
            this.txtEmailSubject.Size = new System.Drawing.Size(431, 23);
            this.txtEmailSubject.TabIndex = 7;
            // 
            // txtEmailID
            // 
            this.txtEmailID.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailID.Location = new System.Drawing.Point(22, 50);
            this.txtEmailID.Name = "txtEmailID";
            this.txtEmailID.Size = new System.Drawing.Size(431, 23);
            this.txtEmailID.TabIndex = 5;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(18, 282);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(68, 15);
            this.label38.TabIndex = 4;
            this.label38.Text = "SMTP Port";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(18, 219);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(71, 15);
            this.label42.TabIndex = 3;
            this.label42.Text = "SMTP Host";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(17, 92);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(84, 15);
            this.label43.TabIndex = 2;
            this.label43.Text = "Email Subject";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(18, 30);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 15);
            this.label45.TabIndex = 0;
            this.label45.Text = "Email ID";
            // 
            // OtherSettings
            // 
            this.OtherSettings.Controls.Add(this.panel10);
            this.OtherSettings.Location = new System.Drawing.Point(4, 24);
            this.OtherSettings.Name = "OtherSettings";
            this.OtherSettings.Size = new System.Drawing.Size(952, 438);
            this.OtherSettings.TabIndex = 2;
            this.OtherSettings.Text = "Other Setting";
            this.OtherSettings.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.AutoScroll = true;
            this.panel10.Controls.Add(this.groupBox13);
            this.panel10.Controls.Add(this.groupBox12);
            this.panel10.Controls.Add(this.groupBox10);
            this.panel10.Controls.Add(this.groupBox11);
            this.panel10.Controls.Add(this.groupBox9);
            this.panel10.Controls.Add(this.groupBox7);
            this.panel10.Controls.Add(this.groupBox5);
            this.panel10.Controls.Add(this.groupBox4);
            this.panel10.Controls.Add(this.groupBox3);
            this.panel10.Controls.Add(this.groupBox2);
            this.panel10.Controls.Add(this.groupBox1);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(952, 438);
            this.panel10.TabIndex = 0;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label58);
            this.groupBox12.Controls.Add(this.btnTriggerPramotionlas);
            this.groupBox12.Controls.Add(this.panel4);
            this.groupBox12.Location = new System.Drawing.Point(493, 1237);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(424, 208);
            this.groupBox12.TabIndex = 133;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Promotional *.CSV Upload";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label58.ForeColor = System.Drawing.Color.SteelBlue;
            this.label58.Location = new System.Drawing.Point(25, 32);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(116, 16);
            this.label58.TabIndex = 132;
            this.label58.Text = "Upload *.CSV File";
            // 
            // btnTriggerPramotionlas
            // 
            this.btnTriggerPramotionlas.BackColor = System.Drawing.Color.SteelBlue;
            this.btnTriggerPramotionlas.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnTriggerPramotionlas.ForeColor = System.Drawing.Color.White;
            this.btnTriggerPramotionlas.Location = new System.Drawing.Point(27, 130);
            this.btnTriggerPramotionlas.Name = "btnTriggerPramotionlas";
            this.btnTriggerPramotionlas.Size = new System.Drawing.Size(84, 29);
            this.btnTriggerPramotionlas.TabIndex = 131;
            this.btnTriggerPramotionlas.Text = "Trigger";
            this.btnTriggerPramotionlas.UseVisualStyleBackColor = false;
            this.btnTriggerPramotionlas.Click += new System.EventHandler(this.btnTriggerPramotionlas_Click);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnbrowsepramotionals);
            this.panel4.Controls.Add(this.txtCSVpathpramotionals);
            this.panel4.Controls.Add(this.label54);
            this.panel4.Controls.Add(this.label55);
            this.panel4.Location = new System.Drawing.Point(8, 51);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(361, 73);
            this.panel4.TabIndex = 126;
            // 
            // btnbrowsepramotionals
            // 
            this.btnbrowsepramotionals.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnbrowsepramotionals.BackColor = System.Drawing.Color.SteelBlue;
            this.btnbrowsepramotionals.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnbrowsepramotionals.ForeColor = System.Drawing.Color.White;
            this.btnbrowsepramotionals.Location = new System.Drawing.Point(252, 22);
            this.btnbrowsepramotionals.Name = "btnbrowsepramotionals";
            this.btnbrowsepramotionals.Size = new System.Drawing.Size(89, 30);
            this.btnbrowsepramotionals.TabIndex = 142;
            this.btnbrowsepramotionals.Text = "browse...";
            this.btnbrowsepramotionals.UseVisualStyleBackColor = false;
            this.btnbrowsepramotionals.Click += new System.EventHandler(this.btnbrowsepramotionals_Click);
            // 
            // txtCSVpathpramotionals
            // 
            this.txtCSVpathpramotionals.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCSVpathpramotionals.Location = new System.Drawing.Point(20, 25);
            this.txtCSVpathpramotionals.Name = "txtCSVpathpramotionals";
            this.txtCSVpathpramotionals.Size = new System.Drawing.Size(230, 21);
            this.txtCSVpathpramotionals.TabIndex = 141;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.SteelBlue;
            this.label54.Location = new System.Drawing.Point(16, 86);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(0, 13);
            this.label54.TabIndex = 137;
            this.label54.Visible = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label55.ForeColor = System.Drawing.Color.SteelBlue;
            this.label55.Location = new System.Drawing.Point(16, 39);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(0, 13);
            this.label55.TabIndex = 135;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.btnCSVupload);
            this.groupBox10.Controls.Add(this.chkAPI);
            this.groupBox10.Controls.Add(this.panel3);
            this.groupBox10.Location = new System.Drawing.Point(8, 1236);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(479, 208);
            this.groupBox10.TabIndex = 132;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "*.CSV  Auto Upload Settings";
            // 
            // btnCSVupload
            // 
            this.btnCSVupload.BackColor = System.Drawing.Color.SteelBlue;
            this.btnCSVupload.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnCSVupload.ForeColor = System.Drawing.Color.White;
            this.btnCSVupload.Location = new System.Drawing.Point(19, 176);
            this.btnCSVupload.Name = "btnCSVupload";
            this.btnCSVupload.Size = new System.Drawing.Size(84, 29);
            this.btnCSVupload.TabIndex = 131;
            this.btnCSVupload.Text = "Update";
            this.btnCSVupload.UseVisualStyleBackColor = false;
            this.btnCSVupload.Click += new System.EventHandler(this.btnCSVupload_Click);
            // 
            // chkAPI
            // 
            this.chkAPI.AutoSize = true;
            this.chkAPI.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAPI.Location = new System.Drawing.Point(23, 27);
            this.chkAPI.Name = "chkAPI";
            this.chkAPI.Size = new System.Drawing.Size(227, 18);
            this.chkAPI.TabIndex = 16;
            this.chkAPI.Text = "*.CSV file Autoupload Enable/Disable";
            this.chkAPI.UseVisualStyleBackColor = true;
            this.chkAPI.CheckedChanged += new System.EventHandler(this.chkAPI_CheckedChanged);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnuploadBatfile);
            this.panel3.Controls.Add(this.txtAPIbatfile);
            this.panel3.Controls.Add(this.Browsebtn);
            this.panel3.Controls.Add(this.txtbrowseFile);
            this.panel3.Controls.Add(this.lblbatFilePath);
            this.panel3.Controls.Add(this.lblcsvFilePath);
            this.panel3.Location = new System.Drawing.Point(8, 51);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(361, 119);
            this.panel3.TabIndex = 126;
            // 
            // btnuploadBatfile
            // 
            this.btnuploadBatfile.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnuploadBatfile.BackColor = System.Drawing.Color.SteelBlue;
            this.btnuploadBatfile.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnuploadBatfile.ForeColor = System.Drawing.Color.White;
            this.btnuploadBatfile.Location = new System.Drawing.Point(253, 64);
            this.btnuploadBatfile.Name = "btnuploadBatfile";
            this.btnuploadBatfile.Size = new System.Drawing.Size(89, 30);
            this.btnuploadBatfile.TabIndex = 142;
            this.btnuploadBatfile.Text = "browse...";
            this.btnuploadBatfile.UseVisualStyleBackColor = false;
            this.btnuploadBatfile.Visible = false;
            this.btnuploadBatfile.Click += new System.EventHandler(this.btnuploadBatfile_Click);
            // 
            // txtAPIbatfile
            // 
            this.txtAPIbatfile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAPIbatfile.Location = new System.Drawing.Point(18, 67);
            this.txtAPIbatfile.Name = "txtAPIbatfile";
            this.txtAPIbatfile.Size = new System.Drawing.Size(230, 21);
            this.txtAPIbatfile.TabIndex = 141;
            this.txtAPIbatfile.Visible = false;
            // 
            // Browsebtn
            // 
            this.Browsebtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Browsebtn.BackColor = System.Drawing.Color.SteelBlue;
            this.Browsebtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Browsebtn.ForeColor = System.Drawing.Color.White;
            this.Browsebtn.Location = new System.Drawing.Point(253, 17);
            this.Browsebtn.Name = "Browsebtn";
            this.Browsebtn.Size = new System.Drawing.Size(89, 30);
            this.Browsebtn.TabIndex = 140;
            this.Browsebtn.Text = "browse...";
            this.Browsebtn.UseVisualStyleBackColor = false;
            this.Browsebtn.Visible = false;
            this.Browsebtn.Click += new System.EventHandler(this.Browsebtn_Click);
            // 
            // txtbrowseFile
            // 
            this.txtbrowseFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbrowseFile.Location = new System.Drawing.Point(18, 20);
            this.txtbrowseFile.Name = "txtbrowseFile";
            this.txtbrowseFile.Size = new System.Drawing.Size(230, 21);
            this.txtbrowseFile.TabIndex = 139;
            this.txtbrowseFile.Visible = false;
            // 
            // lblbatFilePath
            // 
            this.lblbatFilePath.AutoSize = true;
            this.lblbatFilePath.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold);
            this.lblbatFilePath.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblbatFilePath.Location = new System.Drawing.Point(22, 98);
            this.lblbatFilePath.Name = "lblbatFilePath";
            this.lblbatFilePath.Size = new System.Drawing.Size(0, 11);
            this.lblbatFilePath.TabIndex = 137;
            this.lblbatFilePath.Visible = false;
            // 
            // lblcsvFilePath
            // 
            this.lblcsvFilePath.AutoSize = true;
            this.lblcsvFilePath.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcsvFilePath.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblcsvFilePath.Location = new System.Drawing.Point(21, 51);
            this.lblcsvFilePath.Name = "lblcsvFilePath";
            this.lblcsvFilePath.Size = new System.Drawing.Size(0, 11);
            this.lblcsvFilePath.TabIndex = 135;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.btnrecallUpdate);
            this.groupBox11.Controls.Add(this.label56);
            this.groupBox11.Controls.Add(this.label57);
            this.groupBox11.Controls.Add(this.drpReinterval);
            this.groupBox11.Controls.Add(this.drpHMT);
            this.groupBox11.Controls.Add(this.pnlrecallSetting);
            this.groupBox11.Controls.Add(this.chkrecall);
            this.groupBox11.Location = new System.Drawing.Point(641, 1070);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(276, 160);
            this.groupBox11.TabIndex = 126;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Recall Settings";
            // 
            // btnrecallUpdate
            // 
            this.btnrecallUpdate.BackColor = System.Drawing.Color.SteelBlue;
            this.btnrecallUpdate.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnrecallUpdate.ForeColor = System.Drawing.Color.White;
            this.btnrecallUpdate.Location = new System.Drawing.Point(9, 125);
            this.btnrecallUpdate.Name = "btnrecallUpdate";
            this.btnrecallUpdate.Size = new System.Drawing.Size(84, 29);
            this.btnrecallUpdate.TabIndex = 131;
            this.btnrecallUpdate.Text = "Update";
            this.btnrecallUpdate.UseVisualStyleBackColor = false;
            this.btnrecallUpdate.Click += new System.EventHandler(this.btnrecallUpdate_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label56.ForeColor = System.Drawing.Color.SteelBlue;
            this.label56.Location = new System.Drawing.Point(14, 88);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(164, 16);
            this.label56.TabIndex = 130;
            this.label56.Text = "Timing interval for recall";
            this.label56.Visible = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label57.ForeColor = System.Drawing.Color.SteelBlue;
            this.label57.Location = new System.Drawing.Point(14, 56);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(157, 16);
            this.label57.TabIndex = 128;
            this.label57.Text = "How many times to call";
            this.label57.Visible = false;
            // 
            // drpReinterval
            // 
            this.drpReinterval.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.drpReinterval.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpReinterval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpReinterval.FormattingEnabled = true;
            this.drpReinterval.Location = new System.Drawing.Point(191, 85);
            this.drpReinterval.Name = "drpReinterval";
            this.drpReinterval.Size = new System.Drawing.Size(62, 23);
            this.drpReinterval.TabIndex = 129;
            this.drpReinterval.Visible = false;
            // 
            // drpHMT
            // 
            this.drpHMT.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.drpHMT.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpHMT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpHMT.FormattingEnabled = true;
            this.drpHMT.Location = new System.Drawing.Point(191, 53);
            this.drpHMT.Name = "drpHMT";
            this.drpHMT.Size = new System.Drawing.Size(62, 23);
            this.drpHMT.TabIndex = 127;
            this.drpHMT.Visible = false;
            // 
            // pnlrecallSetting
            // 
            this.pnlrecallSetting.Location = new System.Drawing.Point(9, 45);
            this.pnlrecallSetting.Name = "pnlrecallSetting";
            this.pnlrecallSetting.Size = new System.Drawing.Size(258, 74);
            this.pnlrecallSetting.TabIndex = 125;
            // 
            // chkrecall
            // 
            this.chkrecall.AutoSize = true;
            this.chkrecall.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkrecall.Location = new System.Drawing.Point(14, 22);
            this.chkrecall.Name = "chkrecall";
            this.chkrecall.Size = new System.Drawing.Size(137, 18);
            this.chkrecall.TabIndex = 16;
            this.chkrecall.Text = "Recall enable/disable";
            this.chkrecall.UseVisualStyleBackColor = true;
            this.chkrecall.CheckedChanged += new System.EventHandler(this.chkrecall_CheckedChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btnA1sync);
            this.groupBox9.Controls.Add(this.chkA1toRS);
            this.groupBox9.Location = new System.Drawing.Point(638, 954);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(279, 110);
            this.groupBox9.TabIndex = 124;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "A1-LAW Sync ON/OFF";
            // 
            // btnA1sync
            // 
            this.btnA1sync.BackColor = System.Drawing.Color.SteelBlue;
            this.btnA1sync.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnA1sync.ForeColor = System.Drawing.Color.White;
            this.btnA1sync.Location = new System.Drawing.Point(15, 68);
            this.btnA1sync.Name = "btnA1sync";
            this.btnA1sync.Size = new System.Drawing.Size(84, 29);
            this.btnA1sync.TabIndex = 120;
            this.btnA1sync.Text = "Update";
            this.btnA1sync.UseVisualStyleBackColor = false;
            this.btnA1sync.Click += new System.EventHandler(this.btnA1sync_Click);
            // 
            // chkA1toRS
            // 
            this.chkA1toRS.AutoSize = true;
            this.chkA1toRS.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkA1toRS.Location = new System.Drawing.Point(17, 23);
            this.chkA1toRS.Name = "chkA1toRS";
            this.chkA1toRS.Size = new System.Drawing.Size(202, 32);
            this.chkA1toRS.TabIndex = 16;
            this.chkA1toRS.Text = "Sync A1Law events to Synergy \r\nNotification System";
            this.chkA1toRS.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label48);
            this.groupBox7.Controls.Add(this.btnUpdateBulk);
            this.groupBox7.Controls.Add(this.pnlbulkTiming);
            this.groupBox7.Controls.Add(this.drpBulkType);
            this.groupBox7.Location = new System.Drawing.Point(8, 954);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(624, 276);
            this.groupBox7.TabIndex = 123;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Upload Bulk Reminder Setting";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label48.ForeColor = System.Drawing.Color.SteelBlue;
            this.label48.Location = new System.Drawing.Point(20, 42);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(127, 16);
            this.label48.TabIndex = 119;
            this.label48.Text = "Appointment Type";
            // 
            // btnUpdateBulk
            // 
            this.btnUpdateBulk.BackColor = System.Drawing.Color.SteelBlue;
            this.btnUpdateBulk.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnUpdateBulk.ForeColor = System.Drawing.Color.White;
            this.btnUpdateBulk.Location = new System.Drawing.Point(19, 111);
            this.btnUpdateBulk.Name = "btnUpdateBulk";
            this.btnUpdateBulk.Size = new System.Drawing.Size(141, 29);
            this.btnUpdateBulk.TabIndex = 118;
            this.btnUpdateBulk.Text = "Update";
            this.btnUpdateBulk.UseVisualStyleBackColor = false;
            this.btnUpdateBulk.Click += new System.EventHandler(this.btnUpdateBulk_Click);
            // 
            // pnlbulkTiming
            // 
            this.pnlbulkTiming.Controls.Add(this.grdEMAIL);
            this.pnlbulkTiming.Controls.Add(this.grdapplyAll);
            this.pnlbulkTiming.Controls.Add(this.chkSMS);
            this.pnlbulkTiming.Controls.Add(this.chkCALL);
            this.pnlbulkTiming.Controls.Add(this.grdCALL);
            this.pnlbulkTiming.Controls.Add(this.chkEMAIL);
            this.pnlbulkTiming.Controls.Add(this.grdSMS);
            this.pnlbulkTiming.Location = new System.Drawing.Point(180, 24);
            this.pnlbulkTiming.Name = "pnlbulkTiming";
            this.pnlbulkTiming.Size = new System.Drawing.Size(434, 246);
            this.pnlbulkTiming.TabIndex = 105;
            this.pnlbulkTiming.Visible = false;
            // 
            // grdEMAIL
            // 
            this.grdEMAIL.AllowUserToAddRows = false;
            this.grdEMAIL.AllowUserToResizeColumns = false;
            this.grdEMAIL.AllowUserToResizeRows = false;
            this.grdEMAIL.BackgroundColor = System.Drawing.Color.White;
            this.grdEMAIL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdEMAIL.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdEMAIL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEMAIL.ColumnHeadersVisible = false;
            this.grdEMAIL.Location = new System.Drawing.Point(317, 43);
            this.grdEMAIL.Name = "grdEMAIL";
            this.grdEMAIL.RowHeadersVisible = false;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdEMAIL.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grdEMAIL.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grdEMAIL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdEMAIL.Size = new System.Drawing.Size(100, 201);
            this.grdEMAIL.TabIndex = 18;
            this.grdEMAIL.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdEMAIL_CellContentClick);
            this.grdEMAIL.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdEMAIL_CellContentDoubleClick);
            this.grdEMAIL.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdEMAIL_CellValueChanged);
            this.grdEMAIL.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdEMAIL_CurrentCellDirtyStateChanged);
            // 
            // grdapplyAll
            // 
            this.grdapplyAll.AllowUserToAddRows = false;
            this.grdapplyAll.AllowUserToResizeColumns = false;
            this.grdapplyAll.AllowUserToResizeRows = false;
            this.grdapplyAll.BackgroundColor = System.Drawing.Color.White;
            this.grdapplyAll.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdapplyAll.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdapplyAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdapplyAll.ColumnHeadersVisible = false;
            this.grdapplyAll.Location = new System.Drawing.Point(28, 42);
            this.grdapplyAll.Name = "grdapplyAll";
            this.grdapplyAll.RowHeadersVisible = false;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            this.grdapplyAll.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.grdapplyAll.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdapplyAll.ShowCellToolTips = false;
            this.grdapplyAll.Size = new System.Drawing.Size(30, 201);
            this.grdapplyAll.TabIndex = 104;
            this.grdapplyAll.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdapplyAll_CellContentClick);
            this.grdapplyAll.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdapplyAll_CellContentDoubleClick);
            // 
            // chkSMS
            // 
            this.chkSMS.AutoSize = true;
            this.chkSMS.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSMS.Location = new System.Drawing.Point(96, 16);
            this.chkSMS.Name = "chkSMS";
            this.chkSMS.Size = new System.Drawing.Size(53, 18);
            this.chkSMS.TabIndex = 13;
            this.chkSMS.Text = "SMS";
            this.chkSMS.UseVisualStyleBackColor = true;
            // 
            // chkCALL
            // 
            this.chkCALL.AutoSize = true;
            this.chkCALL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCALL.Location = new System.Drawing.Point(211, 16);
            this.chkCALL.Name = "chkCALL";
            this.chkCALL.Size = new System.Drawing.Size(57, 18);
            this.chkCALL.TabIndex = 14;
            this.chkCALL.Text = "CALL";
            this.chkCALL.UseVisualStyleBackColor = true;
            // 
            // grdCALL
            // 
            this.grdCALL.AllowUserToAddRows = false;
            this.grdCALL.AllowUserToResizeColumns = false;
            this.grdCALL.AllowUserToResizeRows = false;
            this.grdCALL.BackgroundColor = System.Drawing.Color.White;
            this.grdCALL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCALL.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdCALL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCALL.ColumnHeadersVisible = false;
            this.grdCALL.Location = new System.Drawing.Point(202, 43);
            this.grdCALL.Name = "grdCALL";
            this.grdCALL.RowHeadersVisible = false;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdCALL.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.grdCALL.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grdCALL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdCALL.Size = new System.Drawing.Size(100, 201);
            this.grdCALL.TabIndex = 17;
            this.grdCALL.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCALL_CellContentClick);
            this.grdCALL.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCALL_CellContentDoubleClick);
            this.grdCALL.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCALL_CellValueChanged);
            this.grdCALL.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdCALL_CurrentCellDirtyStateChanged);
            // 
            // chkEMAIL
            // 
            this.chkEMAIL.AutoSize = true;
            this.chkEMAIL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEMAIL.Location = new System.Drawing.Point(324, 16);
            this.chkEMAIL.Name = "chkEMAIL";
            this.chkEMAIL.Size = new System.Drawing.Size(65, 18);
            this.chkEMAIL.TabIndex = 15;
            this.chkEMAIL.Text = "EMAIL";
            this.chkEMAIL.UseVisualStyleBackColor = true;
            // 
            // grdSMS
            // 
            this.grdSMS.AllowUserToAddRows = false;
            this.grdSMS.AllowUserToResizeColumns = false;
            this.grdSMS.AllowUserToResizeRows = false;
            this.grdSMS.BackgroundColor = System.Drawing.Color.White;
            this.grdSMS.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdSMS.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdSMS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSMS.ColumnHeadersVisible = false;
            this.grdSMS.Location = new System.Drawing.Point(88, 43);
            this.grdSMS.Name = "grdSMS";
            this.grdSMS.RowHeadersVisible = false;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdSMS.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.grdSMS.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdSMS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdSMS.ShowCellToolTips = false;
            this.grdSMS.Size = new System.Drawing.Size(110, 201);
            this.grdSMS.TabIndex = 16;
            this.grdSMS.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSMS_CellContentClick);
            this.grdSMS.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSMS_CellContentDoubleClick);
            this.grdSMS.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSMS_CellValueChanged);
            this.grdSMS.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdSMS_CurrentCellDirtyStateChanged);
            // 
            // drpBulkType
            // 
            this.drpBulkType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.drpBulkType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpBulkType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpBulkType.FormattingEnabled = true;
            this.drpBulkType.Location = new System.Drawing.Point(19, 63);
            this.drpBulkType.Name = "drpBulkType";
            this.drpBulkType.Size = new System.Drawing.Size(141, 23);
            this.drpBulkType.TabIndex = 1;
            this.drpBulkType.SelectedIndexChanged += new System.EventHandler(this.drpBulkType_SelectedIndexChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.btnChangeTimeZone);
            this.groupBox5.Controls.Add(this.drpTimeZone);
            this.groupBox5.Location = new System.Drawing.Point(8, 516);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(497, 104);
            this.groupBox5.TabIndex = 122;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Time Zone";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label10.Location = new System.Drawing.Point(131, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 16);
            this.label10.TabIndex = 119;
            this.label10.Text = "hours";
            // 
            // btnChangeTimeZone
            // 
            this.btnChangeTimeZone.BackColor = System.Drawing.Color.SteelBlue;
            this.btnChangeTimeZone.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnChangeTimeZone.ForeColor = System.Drawing.Color.White;
            this.btnChangeTimeZone.Location = new System.Drawing.Point(206, 39);
            this.btnChangeTimeZone.Name = "btnChangeTimeZone";
            this.btnChangeTimeZone.Size = new System.Drawing.Size(134, 29);
            this.btnChangeTimeZone.TabIndex = 119;
            this.btnChangeTimeZone.Text = "Change Time Zone";
            this.btnChangeTimeZone.UseVisualStyleBackColor = false;
            this.btnChangeTimeZone.Click += new System.EventHandler(this.btnChangeTimeZone_Click);
            // 
            // drpTimeZone
            // 
            this.drpTimeZone.FormattingEnabled = true;
            this.drpTimeZone.Items.AddRange(new object[] {
            "12",
            "24"});
            this.drpTimeZone.Location = new System.Drawing.Point(73, 43);
            this.drpTimeZone.Name = "drpTimeZone";
            this.drpTimeZone.Size = new System.Drawing.Size(55, 23);
            this.drpTimeZone.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.btnpasswordChange);
            this.groupBox4.Controls.Add(this.txtConfirmPassword);
            this.groupBox4.Controls.Add(this.txtNewpassword);
            this.groupBox4.Controls.Add(this.txtOldpassword);
            this.groupBox4.Location = new System.Drawing.Point(511, 516);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(406, 212);
            this.groupBox4.TabIndex = 108;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Change Password";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label31.ForeColor = System.Drawing.Color.SteelBlue;
            this.label31.Location = new System.Drawing.Point(43, 127);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(124, 16);
            this.label31.TabIndex = 121;
            this.label31.Text = "Confirm Password";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.SteelBlue;
            this.label7.Location = new System.Drawing.Point(44, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 16);
            this.label7.TabIndex = 120;
            this.label7.Text = "New Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.SteelBlue;
            this.label2.Location = new System.Drawing.Point(43, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 16);
            this.label2.TabIndex = 119;
            this.label2.Text = "Old Password";
            // 
            // btnpasswordChange
            // 
            this.btnpasswordChange.BackColor = System.Drawing.Color.SteelBlue;
            this.btnpasswordChange.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnpasswordChange.ForeColor = System.Drawing.Color.White;
            this.btnpasswordChange.Location = new System.Drawing.Point(175, 165);
            this.btnpasswordChange.Name = "btnpasswordChange";
            this.btnpasswordChange.Size = new System.Drawing.Size(84, 29);
            this.btnpasswordChange.TabIndex = 119;
            this.btnpasswordChange.Text = "Change";
            this.btnpasswordChange.UseVisualStyleBackColor = false;
            this.btnpasswordChange.Click += new System.EventHandler(this.btnpasswordChange_Click);
            // 
            // txtConfirmPassword
            // 
            this.txtConfirmPassword.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirmPassword.Location = new System.Drawing.Point(175, 125);
            this.txtConfirmPassword.MaxLength = 100;
            this.txtConfirmPassword.Name = "txtConfirmPassword";
            this.txtConfirmPassword.PasswordChar = '*';
            this.txtConfirmPassword.Size = new System.Drawing.Size(180, 23);
            this.txtConfirmPassword.TabIndex = 4;
            // 
            // txtNewpassword
            // 
            this.txtNewpassword.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNewpassword.Location = new System.Drawing.Point(175, 87);
            this.txtNewpassword.MaxLength = 100;
            this.txtNewpassword.Name = "txtNewpassword";
            this.txtNewpassword.PasswordChar = '*';
            this.txtNewpassword.Size = new System.Drawing.Size(180, 23);
            this.txtNewpassword.TabIndex = 3;
            // 
            // txtOldpassword
            // 
            this.txtOldpassword.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOldpassword.Location = new System.Drawing.Point(175, 49);
            this.txtOldpassword.MaxLength = 100;
            this.txtOldpassword.Name = "txtOldpassword";
            this.txtOldpassword.PasswordChar = '*';
            this.txtOldpassword.Size = new System.Drawing.Size(180, 23);
            this.txtOldpassword.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.grdtimeSlot);
            this.groupBox3.Controls.Add(this.btnAddtime);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.txtTime);
            this.groupBox3.Controls.Add(this.drptimeSlot);
            this.groupBox3.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.groupBox3.Location = new System.Drawing.Point(7, 260);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(910, 250);
            this.groupBox3.TabIndex = 107;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Time Slots";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label27.ForeColor = System.Drawing.Color.SteelBlue;
            this.label27.Location = new System.Drawing.Point(507, 18);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(129, 16);
            this.label27.TabIndex = 118;
            this.label27.Text = "Available Time Slot";
            // 
            // grdtimeSlot
            // 
            this.grdtimeSlot.AllowUserToAddRows = false;
            this.grdtimeSlot.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.grdtimeSlot.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.grdtimeSlot.BackgroundColor = System.Drawing.Color.White;
            this.grdtimeSlot.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdtimeSlot.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.grdtimeSlot.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdtimeSlot.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdtimeSlot.Location = new System.Drawing.Point(511, 41);
            this.grdtimeSlot.Name = "grdtimeSlot";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdtimeSlot.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.grdtimeSlot.RowHeadersVisible = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdtimeSlot.RowsDefaultCellStyle = dataGridViewCellStyle9;
            this.grdtimeSlot.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdtimeSlot.Size = new System.Drawing.Size(327, 202);
            this.grdtimeSlot.TabIndex = 117;
            this.grdtimeSlot.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdtimeSlot_CellContentClick);
            // 
            // btnAddtime
            // 
            this.btnAddtime.BackColor = System.Drawing.Color.SteelBlue;
            this.btnAddtime.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnAddtime.ForeColor = System.Drawing.Color.White;
            this.btnAddtime.Location = new System.Drawing.Point(310, 94);
            this.btnAddtime.Name = "btnAddtime";
            this.btnAddtime.Size = new System.Drawing.Size(84, 29);
            this.btnAddtime.TabIndex = 115;
            this.btnAddtime.Text = "Add Time";
            this.btnAddtime.UseVisualStyleBackColor = false;
            this.btnAddtime.Click += new System.EventHandler(this.btnAddtime_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label26.ForeColor = System.Drawing.Color.SteelBlue;
            this.label26.Location = new System.Drawing.Point(70, 75);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(96, 16);
            this.label26.TabIndex = 116;
            this.label26.Text = "Add Time Slot";
            // 
            // txtTime
            // 
            this.txtTime.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTime.Location = new System.Drawing.Point(74, 98);
            this.txtTime.MaxLength = 5;
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(100, 23);
            this.txtTime.TabIndex = 1;
            // 
            // drptimeSlot
            // 
            this.drptimeSlot.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drptimeSlot.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drptimeSlot.FormattingEnabled = true;
            this.drptimeSlot.Items.AddRange(new object[] {
            "Day",
            "Minute",
            "Hour"});
            this.drptimeSlot.Location = new System.Drawing.Point(180, 98);
            this.drptimeSlot.Name = "drptimeSlot";
            this.drptimeSlot.Size = new System.Drawing.Size(121, 23);
            this.drptimeSlot.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnRemoveLogo);
            this.groupBox2.Controls.Add(this.lblPriviewLogo);
            this.groupBox2.Controls.Add(this.pblogo);
            this.groupBox2.Controls.Add(this.lblImagename);
            this.groupBox2.Controls.Add(this.txtsignature);
            this.groupBox2.Controls.Add(this.btnUpload);
            this.groupBox2.Controls.Add(this.btnCompanyLogo);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.txtCompanyLogo);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.groupBox2.Location = new System.Drawing.Point(9, 738);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(908, 211);
            this.groupBox2.TabIndex = 106;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Upload";
            // 
            // btnRemoveLogo
            // 
            this.btnRemoveLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveLogo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRemoveLogo.FlatAppearance.BorderSize = 5;
            this.btnRemoveLogo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRemoveLogo.ForeColor = System.Drawing.Color.Black;
            this.btnRemoveLogo.Image = global::SynergyNotificationSystem.Properties.Resources.Remove;
            this.btnRemoveLogo.Location = new System.Drawing.Point(317, 139);
            this.btnRemoveLogo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRemoveLogo.Name = "btnRemoveLogo";
            this.btnRemoveLogo.Size = new System.Drawing.Size(22, 17);
            this.btnRemoveLogo.TabIndex = 117;
            this.btnRemoveLogo.UseVisualStyleBackColor = false;
            this.btnRemoveLogo.Visible = false;
            this.btnRemoveLogo.Click += new System.EventHandler(this.btnRemoveLogo_Click);
            // 
            // lblPriviewLogo
            // 
            this.lblPriviewLogo.AutoSize = true;
            this.lblPriviewLogo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblPriviewLogo.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblPriviewLogo.Location = new System.Drawing.Point(71, 105);
            this.lblPriviewLogo.Name = "lblPriviewLogo";
            this.lblPriviewLogo.Size = new System.Drawing.Size(87, 11);
            this.lblPriviewLogo.TabIndex = 116;
            this.lblPriviewLogo.Text = "Preview Image :";
            this.lblPriviewLogo.Visible = false;
            // 
            // pblogo
            // 
            this.pblogo.Location = new System.Drawing.Point(173, 78);
            this.pblogo.Name = "pblogo";
            this.pblogo.Size = new System.Drawing.Size(138, 78);
            this.pblogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pblogo.TabIndex = 115;
            this.pblogo.TabStop = false;
            this.pblogo.Visible = false;
            // 
            // lblImagename
            // 
            this.lblImagename.AutoSize = true;
            this.lblImagename.Location = new System.Drawing.Point(35, 40);
            this.lblImagename.Name = "lblImagename";
            this.lblImagename.Size = new System.Drawing.Size(0, 15);
            this.lblImagename.TabIndex = 114;
            this.lblImagename.Visible = false;
            // 
            // txtsignature
            // 
            this.txtsignature.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsignature.Location = new System.Drawing.Point(505, 44);
            this.txtsignature.Multiline = true;
            this.txtsignature.Name = "txtsignature";
            this.txtsignature.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtsignature.Size = new System.Drawing.Size(332, 112);
            this.txtsignature.TabIndex = 113;
            // 
            // btnUpload
            // 
            this.btnUpload.BackColor = System.Drawing.Color.SteelBlue;
            this.btnUpload.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnUpload.ForeColor = System.Drawing.Color.White;
            this.btnUpload.Location = new System.Drawing.Point(505, 162);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(129, 29);
            this.btnUpload.TabIndex = 112;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = false;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnCompanyLogo
            // 
            this.btnCompanyLogo.BackColor = System.Drawing.Color.SteelBlue;
            this.btnCompanyLogo.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnCompanyLogo.ForeColor = System.Drawing.Color.White;
            this.btnCompanyLogo.Location = new System.Drawing.Point(412, 40);
            this.btnCompanyLogo.Name = "btnCompanyLogo";
            this.btnCompanyLogo.Size = new System.Drawing.Size(66, 29);
            this.btnCompanyLogo.TabIndex = 110;
            this.btnCompanyLogo.Text = "Browse";
            this.btnCompanyLogo.UseVisualStyleBackColor = false;
            this.btnCompanyLogo.Click += new System.EventHandler(this.btnCompanyLogo_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label25.ForeColor = System.Drawing.Color.SteelBlue;
            this.label25.Location = new System.Drawing.Point(501, 20);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(122, 16);
            this.label25.TabIndex = 108;
            this.label25.Text = "Upload  Signature";
            // 
            // txtCompanyLogo
            // 
            this.txtCompanyLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyLogo.Location = new System.Drawing.Point(74, 44);
            this.txtCompanyLogo.Name = "txtCompanyLogo";
            this.txtCompanyLogo.Size = new System.Drawing.Size(332, 21);
            this.txtCompanyLogo.TabIndex = 107;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.SteelBlue;
            this.label24.Location = new System.Drawing.Point(71, 20);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(149, 16);
            this.label24.TabIndex = 106;
            this.label24.Text = "Upload Company Logo";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.Appoint);
            this.groupBox1.Controls.Add(this.grdLogColor);
            this.groupBox1.Controls.Add(this.grdReminderColor);
            this.groupBox1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.groupBox1.Location = new System.Drawing.Point(6, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(911, 230);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Color Setting";
            // 
            // label23
            // 
            this.label23.BackColor = System.Drawing.Color.Gainsboro;
            this.label23.Location = new System.Drawing.Point(482, 19);
            this.label23.MaximumSize = new System.Drawing.Size(20, 1000);
            this.label23.MinimumSize = new System.Drawing.Size(1, 50);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(1, 200);
            this.label23.TabIndex = 105;
            this.label23.Text = "label23";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.SteelBlue;
            this.label22.Location = new System.Drawing.Point(508, 21);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(115, 16);
            this.label22.TabIndex = 58;
            this.label22.Text = "Log Status Color";
            // 
            // Appoint
            // 
            this.Appoint.AutoSize = true;
            this.Appoint.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.Appoint.ForeColor = System.Drawing.Color.SteelBlue;
            this.Appoint.Location = new System.Drawing.Point(119, 21);
            this.Appoint.Name = "Appoint";
            this.Appoint.Size = new System.Drawing.Size(176, 16);
            this.Appoint.TabIndex = 56;
            this.Appoint.Text = "Appointment Status Color";
            // 
            // grdLogColor
            // 
            this.grdLogColor.AllowUserToAddRows = false;
            this.grdLogColor.AllowUserToResizeRows = false;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.grdLogColor.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.grdLogColor.BackgroundColor = System.Drawing.Color.White;
            this.grdLogColor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdLogColor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.grdLogColor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdLogColor.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdLogColor.Location = new System.Drawing.Point(512, 48);
            this.grdLogColor.Name = "grdLogColor";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdLogColor.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.grdLogColor.RowHeadersVisible = false;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdLogColor.RowsDefaultCellStyle = dataGridViewCellStyle13;
            this.grdLogColor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdLogColor.Size = new System.Drawing.Size(327, 158);
            this.grdLogColor.TabIndex = 55;
            this.grdLogColor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdLogColor_CellContentClick);
            this.grdLogColor.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdLogColor_CellFormatting);
            // 
            // grdReminderColor
            // 
            this.grdReminderColor.AllowUserToAddRows = false;
            this.grdReminderColor.AllowUserToResizeRows = false;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.grdReminderColor.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle14;
            this.grdReminderColor.BackgroundColor = System.Drawing.Color.White;
            this.grdReminderColor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdReminderColor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.grdReminderColor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdReminderColor.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdReminderColor.Location = new System.Drawing.Point(123, 48);
            this.grdReminderColor.Name = "grdReminderColor";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdReminderColor.RowHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.grdReminderColor.RowHeadersVisible = false;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdReminderColor.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.grdReminderColor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdReminderColor.Size = new System.Drawing.Size(327, 158);
            this.grdReminderColor.TabIndex = 54;
            this.grdReminderColor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdReminderColor_CellContentClick);
            this.grdReminderColor.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdReminderColor_CellFormatting);
            // 
            // RegistrationDetailstb
            // 
            this.RegistrationDetailstb.Controls.Add(this.panel1);
            this.RegistrationDetailstb.Location = new System.Drawing.Point(4, 24);
            this.RegistrationDetailstb.Name = "RegistrationDetailstb";
            this.RegistrationDetailstb.Size = new System.Drawing.Size(952, 438);
            this.RegistrationDetailstb.TabIndex = 3;
            this.RegistrationDetailstb.Text = "Registration Details";
            this.RegistrationDetailstb.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblRegistrationMac);
            this.panel1.Controls.Add(this.btnRegiCancel);
            this.panel1.Controls.Add(this.btnregistrationUpdate);
            this.panel1.Controls.Add(this.label50);
            this.panel1.Controls.Add(this.txtproductkey);
            this.panel1.Controls.Add(this.txtEmail);
            this.panel1.Controls.Add(this.lblEmailID);
            this.panel1.Controls.Add(this.txtMobileNo);
            this.panel1.Controls.Add(this.lblMobileNumber);
            this.panel1.Controls.Add(this.txtCompanyName);
            this.panel1.Controls.Add(this.lblCompanyName);
            this.panel1.Controls.Add(this.label49);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(952, 438);
            this.panel1.TabIndex = 0;
            // 
            // lblRegistrationMac
            // 
            this.lblRegistrationMac.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistrationMac.ForeColor = System.Drawing.Color.White;
            this.lblRegistrationMac.Location = new System.Drawing.Point(271, 268);
            this.lblRegistrationMac.Name = "lblRegistrationMac";
            this.lblRegistrationMac.Size = new System.Drawing.Size(385, 18);
            this.lblRegistrationMac.TabIndex = 41;
            this.lblRegistrationMac.Text = "Mobile Number";
            this.lblRegistrationMac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnRegiCancel
            // 
            this.btnRegiCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRegiCancel.BackColor = System.Drawing.Color.SteelBlue;
            this.btnRegiCancel.FlatAppearance.BorderSize = 5;
            this.btnRegiCancel.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnRegiCancel.ForeColor = System.Drawing.Color.White;
            this.btnRegiCancel.Location = new System.Drawing.Point(482, 290);
            this.btnRegiCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRegiCancel.Name = "btnRegiCancel";
            this.btnRegiCancel.Size = new System.Drawing.Size(112, 31);
            this.btnRegiCancel.TabIndex = 40;
            this.btnRegiCancel.Text = "Cancel";
            this.btnRegiCancel.UseVisualStyleBackColor = false;
            // 
            // btnregistrationUpdate
            // 
            this.btnregistrationUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnregistrationUpdate.BackColor = System.Drawing.Color.SteelBlue;
            this.btnregistrationUpdate.FlatAppearance.BorderSize = 5;
            this.btnregistrationUpdate.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnregistrationUpdate.ForeColor = System.Drawing.Color.White;
            this.btnregistrationUpdate.Location = new System.Drawing.Point(364, 290);
            this.btnregistrationUpdate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnregistrationUpdate.Name = "btnregistrationUpdate";
            this.btnregistrationUpdate.Size = new System.Drawing.Size(112, 31);
            this.btnregistrationUpdate.TabIndex = 39;
            this.btnregistrationUpdate.Text = "Update";
            this.btnregistrationUpdate.UseVisualStyleBackColor = false;
            this.btnregistrationUpdate.Click += new System.EventHandler(this.btnregistrationUpdate_Click);
            // 
            // label50
            // 
            this.label50.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.SteelBlue;
            this.label50.Location = new System.Drawing.Point(271, 213);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(385, 18);
            this.label50.TabIndex = 21;
            this.label50.Text = "Product Key";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtproductkey
            // 
            this.txtproductkey.Enabled = false;
            this.txtproductkey.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtproductkey.ForeColor = System.Drawing.Color.Black;
            this.txtproductkey.Location = new System.Drawing.Point(271, 237);
            this.txtproductkey.MaxLength = 50;
            this.txtproductkey.Name = "txtproductkey";
            this.txtproductkey.Size = new System.Drawing.Size(385, 23);
            this.txtproductkey.TabIndex = 20;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.Black;
            this.txtEmail.Location = new System.Drawing.Point(271, 184);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(385, 23);
            this.txtEmail.TabIndex = 19;
            // 
            // lblEmailID
            // 
            this.lblEmailID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailID.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblEmailID.Location = new System.Drawing.Point(271, 160);
            this.lblEmailID.Name = "lblEmailID";
            this.lblEmailID.Size = new System.Drawing.Size(385, 18);
            this.lblEmailID.TabIndex = 18;
            this.lblEmailID.Text = "Email Address";
            this.lblEmailID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMobileNo.ForeColor = System.Drawing.Color.Black;
            this.txtMobileNo.Location = new System.Drawing.Point(271, 131);
            this.txtMobileNo.MaxLength = 50;
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(385, 23);
            this.txtMobileNo.TabIndex = 17;
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNumber.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblMobileNumber.Location = new System.Drawing.Point(271, 107);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(385, 18);
            this.lblMobileNumber.TabIndex = 16;
            this.lblMobileNumber.Text = "Mobile Number";
            this.lblMobileNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyName.ForeColor = System.Drawing.Color.Black;
            this.txtCompanyName.Location = new System.Drawing.Point(271, 78);
            this.txtCompanyName.MaxLength = 50;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(385, 23);
            this.txtCompanyName.TabIndex = 15;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyName.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblCompanyName.Location = new System.Drawing.Point(271, 54);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(385, 18);
            this.lblCompanyName.TabIndex = 14;
            this.lblCompanyName.Text = "Company Name";
            this.lblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label49
            // 
            this.label49.BackColor = System.Drawing.Color.SteelBlue;
            this.label49.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label49.Dock = System.Windows.Forms.DockStyle.Top;
            this.label49.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(0, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(952, 39);
            this.label49.TabIndex = 1;
            this.label49.Text = "Registration Details";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Reportstab
            // 
            this.Reportstab.Controls.Add(this.groupBox8);
            this.Reportstab.Location = new System.Drawing.Point(4, 24);
            this.Reportstab.Name = "Reportstab";
            this.Reportstab.Size = new System.Drawing.Size(952, 438);
            this.Reportstab.TabIndex = 4;
            this.Reportstab.Text = "Reports";
            this.Reportstab.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.panel2);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Location = new System.Drawing.Point(0, 0);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(952, 438);
            this.groupBox8.TabIndex = 121;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Report generate";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label53);
            this.panel2.Controls.Add(this.btnReportExport);
            this.panel2.Controls.Add(this.label51);
            this.panel2.Controls.Add(this.enddateTimePickerReport);
            this.panel2.Controls.Add(this.label52);
            this.panel2.Controls.Add(this.strtdateTimePickerReport);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 19);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(946, 156);
            this.panel2.TabIndex = 114;
            // 
            // label53
            // 
            this.label53.BackColor = System.Drawing.Color.SteelBlue;
            this.label53.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label53.Dock = System.Windows.Forms.DockStyle.Top;
            this.label53.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.White;
            this.label53.Location = new System.Drawing.Point(0, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(946, 39);
            this.label53.TabIndex = 36;
            this.label53.Text = "Monthly Usage Report ";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnReportExport
            // 
            this.btnReportExport.BackColor = System.Drawing.Color.SteelBlue;
            this.btnReportExport.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnReportExport.ForeColor = System.Drawing.Color.White;
            this.btnReportExport.Location = new System.Drawing.Point(418, 97);
            this.btnReportExport.Name = "btnReportExport";
            this.btnReportExport.Size = new System.Drawing.Size(119, 38);
            this.btnReportExport.TabIndex = 35;
            this.btnReportExport.Text = "Export Report";
            this.btnReportExport.UseVisualStyleBackColor = false;
            this.btnReportExport.Click += new System.EventHandler(this.btnReportExport_Click);
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.SteelBlue;
            this.label51.Location = new System.Drawing.Point(242, 56);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(87, 18);
            this.label51.TabIndex = 3;
            this.label51.Text = "From Date :";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // enddateTimePickerReport
            // 
            this.enddateTimePickerReport.CustomFormat = Helper.UniversalDateFormat;
            this.enddateTimePickerReport.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.enddateTimePickerReport.Location = new System.Drawing.Point(561, 55);
            this.enddateTimePickerReport.Name = "enddateTimePickerReport";
            this.enddateTimePickerReport.Size = new System.Drawing.Size(108, 23);
            this.enddateTimePickerReport.TabIndex = 34;
            this.enddateTimePickerReport.Value = new System.DateTime(2016, 4, 27, 0, 0, 0, 0);
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.SteelBlue;
            this.label52.Location = new System.Drawing.Point(488, 56);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(70, 18);
            this.label52.TabIndex = 5;
            this.label52.Text = "To Date :";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // strtdateTimePickerReport
            // 
            this.strtdateTimePickerReport.CustomFormat = Helper.UniversalDateFormat;
            this.strtdateTimePickerReport.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.strtdateTimePickerReport.Location = new System.Drawing.Point(333, 55);
            this.strtdateTimePickerReport.Name = "strtdateTimePickerReport";
            this.strtdateTimePickerReport.Size = new System.Drawing.Size(108, 23);
            this.strtdateTimePickerReport.TabIndex = 32;
            this.strtdateTimePickerReport.Value = new System.DateTime(2016, 4, 27, 0, 0, 0, 0);
            // 
            // pnlPassword
            // 
            this.pnlPassword.Controls.Add(this.txtpasswordSetting);
            this.pnlPassword.Controls.Add(this.txtusername);
            this.pnlPassword.Controls.Add(this.label46);
            this.pnlPassword.Controls.Add(this.label47);
            this.pnlPassword.Controls.Add(this.btncancel);
            this.pnlPassword.Controls.Add(this.btnsignin);
            this.pnlPassword.Controls.Add(this.lblHead);
            this.pnlPassword.Location = new System.Drawing.Point(889, 1);
            this.pnlPassword.Name = "pnlPassword";
            this.pnlPassword.Size = new System.Drawing.Size(38, 32);
            this.pnlPassword.TabIndex = 1;
            this.pnlPassword.Visible = false;
            // 
            // txtpasswordSetting
            // 
            this.txtpasswordSetting.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.txtpasswordSetting.Location = new System.Drawing.Point(356, 224);
            this.txtpasswordSetting.Name = "txtpasswordSetting";
            this.txtpasswordSetting.PasswordChar = '*';
            this.txtpasswordSetting.Size = new System.Drawing.Size(332, 23);
            this.txtpasswordSetting.TabIndex = 19;
            this.txtpasswordSetting.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtpasswordSetting_KeyDown);
            // 
            // txtusername
            // 
            this.txtusername.Font = new System.Drawing.Font("Malgun Gothic", 9F);
            this.txtusername.Location = new System.Drawing.Point(356, 188);
            this.txtusername.Name = "txtusername";
            this.txtusername.Size = new System.Drawing.Size(332, 23);
            this.txtusername.TabIndex = 18;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label46.ForeColor = System.Drawing.Color.SteelBlue;
            this.label46.Location = new System.Drawing.Point(280, 227);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(70, 15);
            this.label46.TabIndex = 23;
            this.label46.Text = "Password :";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label47.ForeColor = System.Drawing.Color.SteelBlue;
            this.label47.Location = new System.Drawing.Point(277, 191);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(73, 15);
            this.label47.TabIndex = 22;
            this.label47.Text = "Username :";
            // 
            // btncancel
            // 
            this.btncancel.BackColor = System.Drawing.Color.SteelBlue;
            this.btncancel.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btncancel.ForeColor = System.Drawing.Color.White;
            this.btncancel.Location = new System.Drawing.Point(498, 282);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(129, 29);
            this.btncancel.TabIndex = 21;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = false;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // btnsignin
            // 
            this.btnsignin.BackColor = System.Drawing.Color.SteelBlue;
            this.btnsignin.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnsignin.ForeColor = System.Drawing.Color.White;
            this.btnsignin.Location = new System.Drawing.Point(358, 282);
            this.btnsignin.Name = "btnsignin";
            this.btnsignin.Size = new System.Drawing.Size(129, 29);
            this.btnsignin.TabIndex = 20;
            this.btnsignin.Text = "Sign In";
            this.btnsignin.UseVisualStyleBackColor = false;
            this.btnsignin.Click += new System.EventHandler(this.btnsignin_Click);
            // 
            // lblHead
            // 
            this.lblHead.BackColor = System.Drawing.Color.SteelBlue;
            this.lblHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHead.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead.ForeColor = System.Drawing.Color.White;
            this.lblHead.Location = new System.Drawing.Point(0, 0);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(38, 39);
            this.lblHead.TabIndex = 17;
            this.lblHead.Text = "Customer settings";
            this.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Logtb
            // 
            this.Logtb.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Logtb.Controls.Add(this.btnDateSearchLog);
            this.Logtb.Controls.Add(this.txtTwilioLogsearch);
            this.Logtb.Controls.Add(this.label30);
            this.Logtb.Controls.Add(this.label6);
            this.Logtb.Controls.Add(this.pnlLogDeliverd);
            this.Logtb.Controls.Add(this.label8);
            this.Logtb.Controls.Add(this.label9);
            this.Logtb.Controls.Add(this.pnlLogNoans);
            this.Logtb.Controls.Add(this.pnlLogUndeliverd);
            this.Logtb.Controls.Add(this.pnlLogComplete);
            this.Logtb.Controls.Add(this.txtTwilioFilterTo);
            this.Logtb.Controls.Add(this.txtTwilioFilterFrom);
            this.Logtb.Controls.Add(this.lblTwilioTO);
            this.Logtb.Controls.Add(this.label29);
            this.Logtb.Controls.Add(this.drpogFilter);
            this.Logtb.Controls.Add(this.grdTwilioLog);
            this.Logtb.Controls.Add(this.btnRefeshTwilioLog);
            this.Logtb.Location = new System.Drawing.Point(4, 24);
            this.Logtb.Name = "Logtb";
            this.Logtb.Size = new System.Drawing.Size(976, 479);
            this.Logtb.TabIndex = 2;
            this.Logtb.Text = "Log";
            this.Logtb.UseVisualStyleBackColor = true;
            // 
            // btnDateSearchLog
            // 
            this.btnDateSearchLog.BackColor = System.Drawing.Color.SteelBlue;
            this.btnDateSearchLog.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnDateSearchLog.ForeColor = System.Drawing.Color.White;
            this.btnDateSearchLog.Location = new System.Drawing.Point(406, 24);
            this.btnDateSearchLog.Name = "btnDateSearchLog";
            this.btnDateSearchLog.Size = new System.Drawing.Size(58, 25);
            this.btnDateSearchLog.TabIndex = 58;
            this.btnDateSearchLog.Text = "Search";
            this.btnDateSearchLog.UseVisualStyleBackColor = false;
            this.btnDateSearchLog.Click += new System.EventHandler(this.btnDateSearchLog_Click);
            // 
            // txtTwilioLogsearch
            // 
            this.txtTwilioLogsearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTwilioLogsearch.ForeColor = System.Drawing.Color.Black;
            this.txtTwilioLogsearch.Location = new System.Drawing.Point(512, 28);
            this.txtTwilioLogsearch.Name = "txtTwilioLogsearch";
            this.txtTwilioLogsearch.Size = new System.Drawing.Size(355, 23);
            this.txtTwilioLogsearch.TabIndex = 57;
            this.txtTwilioLogsearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTwilioLogsearch_KeyUp);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(808, 9);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 13);
            this.label30.TabIndex = 56;
            this.label30.Text = "Delivered";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(717, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 56;
            this.label6.Text = "No-answer";
            // 
            // pnlLogDeliverd
            // 
            this.pnlLogDeliverd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(224)))), ((int)(((byte)(16)))));
            this.pnlLogDeliverd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLogDeliverd.Location = new System.Drawing.Point(792, 9);
            this.pnlLogDeliverd.Name = "pnlLogDeliverd";
            this.pnlLogDeliverd.Size = new System.Drawing.Size(13, 13);
            this.pnlLogDeliverd.TabIndex = 55;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(621, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 13);
            this.label8.TabIndex = 55;
            this.label8.Text = "Undelivered";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(529, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 54;
            this.label9.Text = "Completed";
            // 
            // pnlLogNoans
            // 
            this.pnlLogNoans.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(255)))), ((int)(((byte)(3)))));
            this.pnlLogNoans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLogNoans.Location = new System.Drawing.Point(701, 9);
            this.pnlLogNoans.Name = "pnlLogNoans";
            this.pnlLogNoans.Size = new System.Drawing.Size(13, 13);
            this.pnlLogNoans.TabIndex = 52;
            // 
            // pnlLogUndeliverd
            // 
            this.pnlLogUndeliverd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(50)))), ((int)(((byte)(77)))));
            this.pnlLogUndeliverd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLogUndeliverd.Location = new System.Drawing.Point(605, 9);
            this.pnlLogUndeliverd.Name = "pnlLogUndeliverd";
            this.pnlLogUndeliverd.Size = new System.Drawing.Size(13, 13);
            this.pnlLogUndeliverd.TabIndex = 53;
            // 
            // pnlLogComplete
            // 
            this.pnlLogComplete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(224)))), ((int)(((byte)(16)))));
            this.pnlLogComplete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlLogComplete.Location = new System.Drawing.Point(513, 9);
            this.pnlLogComplete.Name = "pnlLogComplete";
            this.pnlLogComplete.Size = new System.Drawing.Size(13, 13);
            this.pnlLogComplete.TabIndex = 51;
            // 
            // txtTwilioFilterTo
            // 
            this.txtTwilioFilterTo.CustomFormat = Helper.UniversalDateFormat;
            this.txtTwilioFilterTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtTwilioFilterTo.Location = new System.Drawing.Point(291, 26);
            this.txtTwilioFilterTo.Name = "txtTwilioFilterTo";
            this.txtTwilioFilterTo.Size = new System.Drawing.Size(108, 23);
            this.txtTwilioFilterTo.TabIndex = 43;
            this.txtTwilioFilterTo.Value = new System.DateTime(2016, 4, 27, 0, 0, 0, 0);
            this.txtTwilioFilterTo.ValueChanged += new System.EventHandler(this.txtTwilioFilterTo_ValueChanged);
            // 
            // txtTwilioFilterFrom
            // 
            this.txtTwilioFilterFrom.CustomFormat = Helper.UniversalDateFormat;
            this.txtTwilioFilterFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtTwilioFilterFrom.Location = new System.Drawing.Point(146, 25);
            this.txtTwilioFilterFrom.Name = "txtTwilioFilterFrom";
            this.txtTwilioFilterFrom.Size = new System.Drawing.Size(108, 23);
            this.txtTwilioFilterFrom.TabIndex = 41;
            this.txtTwilioFilterFrom.Value = new System.DateTime(2016, 4, 27, 0, 0, 0, 0);
            this.txtTwilioFilterFrom.ValueChanged += new System.EventHandler(this.txtTwilioFilterFrom_ValueChanged);
            // 
            // lblTwilioTO
            // 
            this.lblTwilioTO.AutoSize = true;
            this.lblTwilioTO.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblTwilioTO.Location = new System.Drawing.Point(263, 29);
            this.lblTwilioTO.Name = "lblTwilioTO";
            this.lblTwilioTO.Size = new System.Drawing.Size(21, 15);
            this.lblTwilioTO.TabIndex = 42;
            this.lblTwilioTO.Text = "To";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label29.Location = new System.Drawing.Point(6, 7);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 15);
            this.label29.TabIndex = 40;
            this.label29.Text = "Filter :";
            // 
            // drpogFilter
            // 
            this.drpogFilter.AllowDrop = true;
            this.drpogFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpogFilter.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drpogFilter.FormattingEnabled = true;
            this.drpogFilter.Items.AddRange(new object[] {
            "Today",
            "Yesterday",
            "Past Week",
            "Specific Date",
            "Custom Range"});
            this.drpogFilter.Location = new System.Drawing.Point(6, 25);
            this.drpogFilter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.drpogFilter.Name = "drpogFilter";
            this.drpogFilter.Size = new System.Drawing.Size(129, 23);
            this.drpogFilter.TabIndex = 39;
            this.drpogFilter.SelectedIndexChanged += new System.EventHandler(this.drpogFilter_SelectedIndexChanged);
            // 
            // grdTwilioLog
            // 
            this.grdTwilioLog.AllowUserToAddRows = false;
            this.grdTwilioLog.AllowUserToResizeRows = false;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.grdTwilioLog.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle18;
            this.grdTwilioLog.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdTwilioLog.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.grdTwilioLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdTwilioLog.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdTwilioLog.Location = new System.Drawing.Point(5, 57);
            this.grdTwilioLog.Name = "grdTwilioLog";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdTwilioLog.RowHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.grdTwilioLog.RowHeadersVisible = false;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdTwilioLog.RowsDefaultCellStyle = dataGridViewCellStyle21;
            this.grdTwilioLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdTwilioLog.Size = new System.Drawing.Size(965, 410);
            this.grdTwilioLog.TabIndex = 36;
            this.grdTwilioLog.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdTwilioLog_CellContentClick);
            this.grdTwilioLog.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdTwilioLog_CellFormatting);
            this.grdTwilioLog.Paint += new System.Windows.Forms.PaintEventHandler(this.grdTwilioLog_Paint);
            // 
            // btnRefeshTwilioLog
            // 
            this.btnRefeshTwilioLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefeshTwilioLog.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRefeshTwilioLog.FlatAppearance.BorderSize = 5;
            this.btnRefeshTwilioLog.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRefeshTwilioLog.ForeColor = System.Drawing.Color.Black;
            this.btnRefeshTwilioLog.Image = global::SynergyNotificationSystem.Properties.Resources.ajax_refresh_icon;
            this.btnRefeshTwilioLog.Location = new System.Drawing.Point(935, 26);
            this.btnRefeshTwilioLog.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRefeshTwilioLog.MaximumSize = new System.Drawing.Size(31, 25);
            this.btnRefeshTwilioLog.Name = "btnRefeshTwilioLog";
            this.btnRefeshTwilioLog.Size = new System.Drawing.Size(31, 25);
            this.btnRefeshTwilioLog.TabIndex = 38;
            this.btnRefeshTwilioLog.UseVisualStyleBackColor = false;
            this.btnRefeshTwilioLog.Click += new System.EventHandler(this.btnRefeshTwilioLog_Click);
            // 
            // Contact
            // 
            this.Contact.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Contact.Controls.Add(this.searchtxt);
            this.Contact.Controls.Add(this.Cntimportbtn);
            this.Contact.Controls.Add(this.Contactgridview);
            this.Contact.Controls.Add(this.label5);
            this.Contact.Controls.Add(this.addcontbtn);
            this.Contact.Controls.Add(this.btnrefreshContact);
            this.Contact.Location = new System.Drawing.Point(4, 24);
            this.Contact.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Contact.Name = "Contact";
            this.Contact.Size = new System.Drawing.Size(976, 479);
            this.Contact.TabIndex = 0;
            this.Contact.Text = "Contacts";
            this.Contact.UseVisualStyleBackColor = true;
            // 
            // searchtxt
            // 
            this.searchtxt.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.searchtxt.Location = new System.Drawing.Point(6, 25);
            this.searchtxt.Name = "searchtxt";
            this.searchtxt.Size = new System.Drawing.Size(541, 23);
            this.searchtxt.TabIndex = 50;
            this.searchtxt.KeyUp += new System.Windows.Forms.KeyEventHandler(this.searchtxt_KeyUp);
            // 
            // Cntimportbtn
            // 
            this.Cntimportbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Cntimportbtn.BackColor = System.Drawing.Color.SteelBlue;
            this.Cntimportbtn.FlatAppearance.BorderSize = 5;
            this.Cntimportbtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Cntimportbtn.ForeColor = System.Drawing.Color.White;
            this.Cntimportbtn.Location = new System.Drawing.Point(682, 15);
            this.Cntimportbtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Cntimportbtn.Name = "Cntimportbtn";
            this.Cntimportbtn.Size = new System.Drawing.Size(122, 33);
            this.Cntimportbtn.TabIndex = 38;
            this.Cntimportbtn.Text = "Import Contacts";
            this.Cntimportbtn.UseVisualStyleBackColor = false;
            this.Cntimportbtn.Click += new System.EventHandler(this.Cntimportbtn_Click);
            // 
            // Contactgridview
            // 
            this.Contactgridview.AllowUserToAddRows = false;
            this.Contactgridview.AllowUserToResizeRows = false;
            this.Contactgridview.BackgroundColor = System.Drawing.Color.White;
            this.Contactgridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Contactgridview.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.Contactgridview.Location = new System.Drawing.Point(6, 54);
            this.Contactgridview.Name = "Contactgridview";
            this.Contactgridview.RowHeadersVisible = false;
            this.Contactgridview.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.Contactgridview.Size = new System.Drawing.Size(965, 415);
            this.Contactgridview.TabIndex = 36;
            this.Contactgridview.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Contactgridview_CellContentClick);
            this.Contactgridview.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Contactgridview_CellDoubleClick);
            this.Contactgridview.Paint += new System.Windows.Forms.PaintEventHandler(this.Contactgridview_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 18;
            this.label5.Text = "Search :";
            // 
            // addcontbtn
            // 
            this.addcontbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addcontbtn.BackColor = System.Drawing.Color.SteelBlue;
            this.addcontbtn.FlatAppearance.BorderSize = 5;
            this.addcontbtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.addcontbtn.ForeColor = System.Drawing.Color.White;
            this.addcontbtn.Location = new System.Drawing.Point(806, 15);
            this.addcontbtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.addcontbtn.Name = "addcontbtn";
            this.addcontbtn.Size = new System.Drawing.Size(122, 33);
            this.addcontbtn.TabIndex = 14;
            this.addcontbtn.Text = "+Add Contact";
            this.addcontbtn.UseVisualStyleBackColor = false;
            this.addcontbtn.Click += new System.EventHandler(this.addcontbtn_Click);
            // 
            // btnrefreshContact
            // 
            this.btnrefreshContact.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnrefreshContact.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnrefreshContact.FlatAppearance.BorderSize = 5;
            this.btnrefreshContact.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnrefreshContact.ForeColor = System.Drawing.Color.Black;
            this.btnrefreshContact.Image = global::SynergyNotificationSystem.Properties.Resources.ajax_refresh_icon;
            this.btnrefreshContact.Location = new System.Drawing.Point(938, 17);
            this.btnrefreshContact.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnrefreshContact.Name = "btnrefreshContact";
            this.btnrefreshContact.Size = new System.Drawing.Size(31, 30);
            this.btnrefreshContact.TabIndex = 37;
            this.btnrefreshContact.UseVisualStyleBackColor = false;
            this.btnrefreshContact.Click += new System.EventHandler(this.btnrefreshContact_Click);
            // 
            // Calendartab
            // 
            this.Calendartab.AllowDrop = true;
            this.Calendartab.BackColor = System.Drawing.Color.White;
            this.Calendartab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Calendartab.Controls.Add(this.btnPendingEvent);
            this.Calendartab.Controls.Add(this.pictureBox1);
            this.Calendartab.Controls.Add(this.txtgridSearch);
            this.Calendartab.Controls.Add(this.chkprivateFilter);
            this.Calendartab.Controls.Add(this.btnsync);
            this.Calendartab.Controls.Add(this.btnserchfirst);
            this.Calendartab.Controls.Add(this.label1);
            this.Calendartab.Controls.Add(this.grdAgendadata);
            this.Calendartab.Controls.Add(this.label21);
            this.Calendartab.Controls.Add(this.label20);
            this.Calendartab.Controls.Add(this.label19);
            this.Calendartab.Controls.Add(this.label18);
            this.Calendartab.Controls.Add(this.label11);
            this.Calendartab.Controls.Add(this.pnlreschedule);
            this.Calendartab.Controls.Add(this.pnlnorepsonce);
            this.Calendartab.Controls.Add(this.pnlcancel);
            this.Calendartab.Controls.Add(this.pnlcallback);
            this.Calendartab.Controls.Add(this.pnlconfirm);
            this.Calendartab.Controls.Add(this.btnDelete);
            this.Calendartab.Controls.Add(this.txtsearchValue);
            this.Calendartab.Controls.Add(this.lbldate);
            this.Calendartab.Controls.Add(this.enddateTimePicker);
            this.Calendartab.Controls.Add(this.strtdateTimePicker);
            this.Calendartab.Controls.Add(this.tolbl);
            this.Calendartab.Controls.Add(this.Monthbtn);
            this.Calendartab.Controls.Add(this.line);
            this.Calendartab.Controls.Add(this.btnRefresh);
            this.Calendartab.Controls.Add(this.AddAppointment);
            this.Calendartab.Controls.Add(this.Datecombo);
            this.Calendartab.Cursor = System.Windows.Forms.Cursors.Default;
            this.Calendartab.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Calendartab.ForeColor = System.Drawing.Color.Black;
            this.Calendartab.Location = new System.Drawing.Point(4, 24);
            this.Calendartab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 10);
            this.Calendartab.Name = "Calendartab";
            this.Calendartab.Size = new System.Drawing.Size(976, 479);
            this.Calendartab.TabIndex = 0;
            this.Calendartab.Text = "Reminders";
            this.Calendartab.Leave += new System.EventHandler(this.Calendartab_Leave);
            // 
            // btnPendingEvent
            // 
            this.btnPendingEvent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPendingEvent.BackColor = System.Drawing.Color.Transparent;
            this.btnPendingEvent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPendingEvent.FlatAppearance.BorderSize = 0;
            this.btnPendingEvent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPendingEvent.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPendingEvent.ForeColor = System.Drawing.Color.Black;
            this.btnPendingEvent.Image = global::SynergyNotificationSystem.Properties.Resources.pending;
            this.btnPendingEvent.Location = new System.Drawing.Point(746, 36);
            this.btnPendingEvent.Margin = new System.Windows.Forms.Padding(0);
            this.btnPendingEvent.Name = "btnPendingEvent";
            this.btnPendingEvent.Size = new System.Drawing.Size(26, 27);
            this.btnPendingEvent.TabIndex = 117;
            this.btnPendingEvent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPendingEvent.UseVisualStyleBackColor = false;
            this.btnPendingEvent.Click += new System.EventHandler(this.btnPendingEvent_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SynergyNotificationSystem.Properties.Resources.View;
            this.pictureBox1.Location = new System.Drawing.Point(780, 38);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(19, 21);
            this.pictureBox1.TabIndex = 116;
            this.pictureBox1.TabStop = false;
            // 
            // txtgridSearch
            // 
            this.txtgridSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtgridSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgridSearch.ForeColor = System.Drawing.Color.Blue;
            this.txtgridSearch.Location = new System.Drawing.Point(801, 36);
            this.txtgridSearch.Name = "txtgridSearch";
            this.txtgridSearch.Size = new System.Drawing.Size(167, 22);
            this.txtgridSearch.TabIndex = 115;
            this.txtgridSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtgridSearch_KeyUp);
            // 
            // chkprivateFilter
            // 
            this.chkprivateFilter.AutoSize = true;
            this.chkprivateFilter.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.chkprivateFilter.ForeColor = System.Drawing.Color.SteelBlue;
            this.chkprivateFilter.Location = new System.Drawing.Point(388, 8);
            this.chkprivateFilter.Name = "chkprivateFilter";
            this.chkprivateFilter.Size = new System.Drawing.Size(192, 19);
            this.chkprivateFilter.TabIndex = 114;
            this.chkprivateFilter.Text = "Show only private reminders";
            this.chkprivateFilter.UseVisualStyleBackColor = true;
            this.chkprivateFilter.CheckedChanged += new System.EventHandler(this.chkprivateFilter_CheckedChanged);
            // 
            // btnsync
            // 
            this.btnsync.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnsync.BackColor = System.Drawing.Color.Transparent;
            this.btnsync.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnsync.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsync.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsync.ForeColor = System.Drawing.Color.Black;
            this.btnsync.Image = global::SynergyNotificationSystem.Properties.Resources.cloud;
            this.btnsync.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsync.Location = new System.Drawing.Point(487, 71);
            this.btnsync.Margin = new System.Windows.Forms.Padding(0);
            this.btnsync.Name = "btnsync";
            this.btnsync.Size = new System.Drawing.Size(64, 27);
            this.btnsync.TabIndex = 62;
            this.btnsync.Text = "Sync";
            this.btnsync.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsync.UseVisualStyleBackColor = false;
            this.btnsync.Click += new System.EventHandler(this.btnsync_Click);
            // 
            // btnserchfirst
            // 
            this.btnserchfirst.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnserchfirst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnserchfirst.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnserchfirst.ForeColor = System.Drawing.Color.Black;
            this.btnserchfirst.Image = global::SynergyNotificationSystem.Properties.Resources.Find;
            this.btnserchfirst.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnserchfirst.Location = new System.Drawing.Point(378, 71);
            this.btnserchfirst.Name = "btnserchfirst";
            this.btnserchfirst.Size = new System.Drawing.Size(78, 27);
            this.btnserchfirst.TabIndex = 61;
            this.btnserchfirst.Text = "Search";
            this.btnserchfirst.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnserchfirst.UseVisualStyleBackColor = true;
            this.btnserchfirst.Click += new System.EventHandler(this.btnserchfirst_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 15);
            this.label1.TabIndex = 60;
            this.label1.Text = "Search :";
            // 
            // grdAgendadata
            // 
            this.grdAgendadata.AllowUserToAddRows = false;
            this.grdAgendadata.AllowUserToResizeRows = false;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.grdAgendadata.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle22;
            this.grdAgendadata.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdAgendadata.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.grdAgendadata.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdAgendadata.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdAgendadata.Location = new System.Drawing.Point(1, 106);
            this.grdAgendadata.Name = "grdAgendadata";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Malgun Gothic", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdAgendadata.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.grdAgendadata.RowHeadersVisible = false;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdAgendadata.RowsDefaultCellStyle = dataGridViewCellStyle25;
            this.grdAgendadata.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdAgendadata.Size = new System.Drawing.Size(970, 367);
            this.grdAgendadata.TabIndex = 53;
            this.grdAgendadata.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdAgendadata_CellClick);
            this.grdAgendadata.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdAgendadata_CellContentClick);
            this.grdAgendadata.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdAgendadata_CellContentDoubleClick);
            this.grdAgendadata.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdAgendadata_CellDoubleClick);
            this.grdAgendadata.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdAgendadata_CellFormatting);
            this.grdAgendadata.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.grdAgendadata_CellPainting);
            this.grdAgendadata.Paint += new System.Windows.Forms.PaintEventHandler(this.grdAgendadata_Paint);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(893, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 13);
            this.label21.TabIndex = 52;
            this.label21.Text = "No-response";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(670, 10);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 13);
            this.label20.TabIndex = 51;
            this.label20.Text = "Callback";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(743, 10);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 13);
            this.label19.TabIndex = 50;
            this.label19.Text = "Reschedule";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(830, 10);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 13);
            this.label18.TabIndex = 49;
            this.label18.Text = "Cancel";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Malgun Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(600, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 48;
            this.label11.Text = "Confirm";
            // 
            // pnlreschedule
            // 
            this.pnlreschedule.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(255)))), ((int)(((byte)(3)))));
            this.pnlreschedule.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlreschedule.Location = new System.Drawing.Point(727, 10);
            this.pnlreschedule.Name = "pnlreschedule";
            this.pnlreschedule.Size = new System.Drawing.Size(13, 13);
            this.pnlreschedule.TabIndex = 43;
            // 
            // pnlnorepsonce
            // 
            this.pnlnorepsonce.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlnorepsonce.Location = new System.Drawing.Point(877, 10);
            this.pnlnorepsonce.Name = "pnlnorepsonce";
            this.pnlnorepsonce.Size = new System.Drawing.Size(13, 13);
            this.pnlnorepsonce.TabIndex = 47;
            // 
            // pnlcancel
            // 
            this.pnlcancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(50)))), ((int)(((byte)(77)))));
            this.pnlcancel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlcancel.Location = new System.Drawing.Point(814, 10);
            this.pnlcancel.Name = "pnlcancel";
            this.pnlcancel.Size = new System.Drawing.Size(13, 13);
            this.pnlcancel.TabIndex = 44;
            // 
            // pnlcallback
            // 
            this.pnlcallback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(111)))), ((int)(((byte)(252)))));
            this.pnlcallback.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlcallback.Location = new System.Drawing.Point(654, 10);
            this.pnlcallback.Name = "pnlcallback";
            this.pnlcallback.Size = new System.Drawing.Size(13, 13);
            this.pnlcallback.TabIndex = 45;
            // 
            // pnlconfirm
            // 
            this.pnlconfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(224)))), ((int)(((byte)(16)))));
            this.pnlconfirm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlconfirm.Location = new System.Drawing.Point(584, 10);
            this.pnlconfirm.Name = "pnlconfirm";
            this.pnlconfirm.Size = new System.Drawing.Size(13, 13);
            this.pnlconfirm.TabIndex = 42;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.Color.Black;
            this.btnDelete.Image = global::SynergyNotificationSystem.Properties.Resources.delete2;
            this.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDelete.Location = new System.Drawing.Point(882, 71);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(0);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(88, 27);
            this.btnDelete.TabIndex = 41;
            this.btnDelete.Text = "Remove";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // txtsearchValue
            // 
            this.txtsearchValue.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtsearchValue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsearchValue.ForeColor = System.Drawing.Color.Blue;
            this.txtsearchValue.Location = new System.Drawing.Point(65, 7);
            this.txtsearchValue.Name = "txtsearchValue";
            this.txtsearchValue.Size = new System.Drawing.Size(307, 22);
            this.txtsearchValue.TabIndex = 40;
            this.txtsearchValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsearchValue_KeyDown);
            // 
            // lbldate
            // 
            this.lbldate.AutoSize = true;
            this.lbldate.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lbldate.Location = new System.Drawing.Point(6, 52);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(43, 15);
            this.lbldate.TabIndex = 37;
            this.lbldate.Text = "Filter :";
            // 
            // enddateTimePicker
            // 
            this.enddateTimePicker.CustomFormat = Helper.UniversalDateFormat;
            this.enddateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.enddateTimePicker.Location = new System.Drawing.Point(264, 71);
            this.enddateTimePicker.Name = "enddateTimePicker";
            this.enddateTimePicker.Size = new System.Drawing.Size(108, 25);
            this.enddateTimePicker.TabIndex = 33;
            this.enddateTimePicker.Value = new System.DateTime(2016, 4, 27, 0, 0, 0, 0);
            // 
            // strtdateTimePicker
            // 
            this.strtdateTimePicker.CustomFormat = Helper.UniversalDateFormat;
            this.strtdateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.strtdateTimePicker.Location = new System.Drawing.Point(126, 71);
            this.strtdateTimePicker.Name = "strtdateTimePicker";
            this.strtdateTimePicker.Size = new System.Drawing.Size(108, 25);
            this.strtdateTimePicker.TabIndex = 31;
            this.strtdateTimePicker.Value = new System.DateTime(2016, 4, 27, 0, 0, 0, 0);
            // 
            // tolbl
            // 
            this.tolbl.AutoSize = true;
            this.tolbl.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.tolbl.Location = new System.Drawing.Point(238, 75);
            this.tolbl.Name = "tolbl";
            this.tolbl.Size = new System.Drawing.Size(21, 15);
            this.tolbl.TabIndex = 32;
            this.tolbl.Text = "To";
            // 
            // Monthbtn
            // 
            this.Monthbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Monthbtn.BackColor = System.Drawing.Color.Transparent;
            this.Monthbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Monthbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Monthbtn.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Monthbtn.ForeColor = System.Drawing.Color.Black;
            this.Monthbtn.Image = global::SynergyNotificationSystem.Properties.Resources.calendar_select_days;
            this.Monthbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Monthbtn.Location = new System.Drawing.Point(553, 71);
            this.Monthbtn.Margin = new System.Windows.Forms.Padding(0);
            this.Monthbtn.Name = "Monthbtn";
            this.Monthbtn.Size = new System.Drawing.Size(122, 27);
            this.Monthbtn.TabIndex = 23;
            this.Monthbtn.Text = "View Calendar";
            this.Monthbtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Monthbtn.UseVisualStyleBackColor = false;
            this.Monthbtn.Click += new System.EventHandler(this.Monthbtn_Click);
            // 
            // line
            // 
            this.line.AllowDrop = true;
            this.line.AutoEllipsis = true;
            this.line.AutoSize = true;
            this.line.BackColor = System.Drawing.Color.DimGray;
            this.line.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.line.Location = new System.Drawing.Point(-6, 103);
            this.line.MaximumSize = new System.Drawing.Size(500, 2);
            this.line.MinimumSize = new System.Drawing.Size(1000, 2);
            this.line.Name = "line";
            this.line.Size = new System.Drawing.Size(1000, 2);
            this.line.TabIndex = 14;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.BackColor = System.Drawing.Color.Transparent;
            this.btnRefresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.Color.Black;
            this.btnRefresh.Image = global::SynergyNotificationSystem.Properties.Resources.reconnect;
            this.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRefresh.Location = new System.Drawing.Point(800, 71);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(0);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(80, 27);
            this.btnRefresh.TabIndex = 12;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.Refresh_Click);
            // 
            // AddAppointment
            // 
            this.AddAppointment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AddAppointment.BackColor = System.Drawing.Color.Transparent;
            this.AddAppointment.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AddAppointment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddAppointment.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddAppointment.ForeColor = System.Drawing.Color.Black;
            this.AddAppointment.Image = global::SynergyNotificationSystem.Properties.Resources.Schedule;
            this.AddAppointment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AddAppointment.Location = new System.Drawing.Point(677, 71);
            this.AddAppointment.Margin = new System.Windows.Forms.Padding(0);
            this.AddAppointment.Name = "AddAppointment";
            this.AddAppointment.Size = new System.Drawing.Size(121, 27);
            this.AddAppointment.TabIndex = 11;
            this.AddAppointment.Text = "Add Reminder";
            this.AddAppointment.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.AddAppointment.UseVisualStyleBackColor = false;
            this.AddAppointment.Click += new System.EventHandler(this.AddAppointment_Click);
            // 
            // Datecombo
            // 
            this.Datecombo.AllowDrop = true;
            this.Datecombo.BackColor = System.Drawing.Color.White;
            this.Datecombo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Datecombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Datecombo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Datecombo.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Datecombo.ForeColor = System.Drawing.Color.Black;
            this.Datecombo.FormattingEnabled = true;
            this.Datecombo.Items.AddRange(new object[] {
            "Upcoming",
            "Today",
            "Tomorrow",
            "Yesterday",
            "Past Week",
            "Specific Date",
            "Custom Range"});
            this.Datecombo.Location = new System.Drawing.Point(6, 71);
            this.Datecombo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Datecombo.Name = "Datecombo";
            this.Datecombo.Size = new System.Drawing.Size(108, 23);
            this.Datecombo.TabIndex = 4;
            this.Datecombo.SelectedIndexChanged += new System.EventHandler(this.Datecombo_SelectedIndexChanged);
            // 
            // Calendars
            // 
            this.Calendars.Controls.Add(this.Calendartab);
            this.Calendars.Controls.Add(this.Contact);
            this.Calendars.Controls.Add(this.Logtb);
            this.Calendars.Controls.Add(this.Settingtb);
            this.Calendars.Controls.Add(this.tabPage1);
            this.Calendars.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Calendars.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Calendars.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Calendars.Location = new System.Drawing.Point(0, 0);
            this.Calendars.Margin = new System.Windows.Forms.Padding(10);
            this.Calendars.Name = "Calendars";
            this.Calendars.Padding = new System.Drawing.Point(10, 3);
            this.Calendars.SelectedIndex = 0;
            this.Calendars.Size = new System.Drawing.Size(984, 507);
            this.Calendars.TabIndex = 0;
            this.Calendars.SelectedIndexChanged += new System.EventHandler(this.Calendars_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnDeleteAddBook);
            this.tabPage1.Controls.Add(this.btnSaveAddBook);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.txtSearchAddBook);
            this.tabPage1.Controls.Add(this.lblAddBookTypeId);
            this.tabPage1.Controls.Add(this.chkAllAddBook);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.btnAddNewAddBook);
            this.tabPage1.Controls.Add(this.dgvAddressBook);
            this.tabPage1.Controls.Add(this.txtAddBookType);
            this.tabPage1.Controls.Add(this.dgvAddressBookList);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(976, 479);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Address Book";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // btnDeleteAddBook
            // 
            this.btnDeleteAddBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteAddBook.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDeleteAddBook.FlatAppearance.BorderSize = 5;
            this.btnDeleteAddBook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDeleteAddBook.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnDeleteAddBook.Location = new System.Drawing.Point(899, 28);
            this.btnDeleteAddBook.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteAddBook.Name = "btnDeleteAddBook";
            this.btnDeleteAddBook.Size = new System.Drawing.Size(69, 23);
            this.btnDeleteAddBook.TabIndex = 44;
            this.btnDeleteAddBook.Text = "&Delete";
            this.btnDeleteAddBook.UseVisualStyleBackColor = false;
            this.btnDeleteAddBook.Click += new System.EventHandler(this.btnDeleteAddBook_Click);
            // 
            // btnSaveAddBook
            // 
            this.btnSaveAddBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAddBook.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnSaveAddBook.FlatAppearance.BorderSize = 5;
            this.btnSaveAddBook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaveAddBook.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnSaveAddBook.Location = new System.Drawing.Point(832, 28);
            this.btnSaveAddBook.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveAddBook.Name = "btnSaveAddBook";
            this.btnSaveAddBook.Size = new System.Drawing.Size(61, 23);
            this.btnSaveAddBook.TabIndex = 45;
            this.btnSaveAddBook.Text = "&Save";
            this.btnSaveAddBook.UseVisualStyleBackColor = false;
            this.btnSaveAddBook.Click += new System.EventHandler(this.btnSaveAddBook_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(354, 8);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 17);
            this.label16.TabIndex = 21;
            this.label16.Text = "Search :";
            // 
            // txtSearchAddBook
            // 
            this.txtSearchAddBook.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtSearchAddBook.Location = new System.Drawing.Point(354, 28);
            this.txtSearchAddBook.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSearchAddBook.Name = "txtSearchAddBook";
            this.txtSearchAddBook.Size = new System.Drawing.Size(472, 23);
            this.txtSearchAddBook.TabIndex = 22;
            this.txtSearchAddBook.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchAddBook_KeyDown);
            // 
            // lblAddBookTypeId
            // 
            this.lblAddBookTypeId.AutoSize = true;
            this.lblAddBookTypeId.BackColor = System.Drawing.Color.Transparent;
            this.lblAddBookTypeId.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddBookTypeId.Location = new System.Drawing.Point(285, 6);
            this.lblAddBookTypeId.Name = "lblAddBookTypeId";
            this.lblAddBookTypeId.Size = new System.Drawing.Size(0, 17);
            this.lblAddBookTypeId.TabIndex = 41;
            this.lblAddBookTypeId.Visible = false;
            // 
            // chkAllAddBook
            // 
            this.chkAllAddBook.AutoSize = true;
            this.chkAllAddBook.Location = new System.Drawing.Point(376, 64);
            this.chkAllAddBook.Name = "chkAllAddBook";
            this.chkAllAddBook.Size = new System.Drawing.Size(15, 14);
            this.chkAllAddBook.TabIndex = 43;
            this.chkAllAddBook.UseVisualStyleBackColor = true;
            this.chkAllAddBook.Visible = false;
            this.chkAllAddBook.CheckedChanged += new System.EventHandler(this.chkAllAddBook_CheckedChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Malgun Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(8, 6);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(101, 17);
            this.label28.TabIndex = 23;
            this.label28.Text = "Address Book :";
            // 
            // btnAddNewAddBook
            // 
            this.btnAddNewAddBook.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNewAddBook.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAddNewAddBook.FlatAppearance.BorderSize = 5;
            this.btnAddNewAddBook.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddNewAddBook.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.btnAddNewAddBook.Location = new System.Drawing.Point(248, 26);
            this.btnAddNewAddBook.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddNewAddBook.Name = "btnAddNewAddBook";
            this.btnAddNewAddBook.Size = new System.Drawing.Size(100, 24);
            this.btnAddNewAddBook.TabIndex = 40;
            this.btnAddNewAddBook.Text = "+&Add";
            this.btnAddNewAddBook.UseVisualStyleBackColor = false;
            this.btnAddNewAddBook.Click += new System.EventHandler(this.btnAddNewAddBook_Click);
            // 
            // dgvAddressBook
            // 
            this.dgvAddressBook.AllowUserToAddRows = false;
            this.dgvAddressBook.AllowUserToResizeRows = false;
            this.dgvAddressBook.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvAddressBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAddressBook.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvAddressBook.Location = new System.Drawing.Point(354, 57);
            this.dgvAddressBook.Name = "dgvAddressBook";
            this.dgvAddressBook.RowHeadersVisible = false;
            this.dgvAddressBook.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAddressBook.Size = new System.Drawing.Size(614, 413);
            this.dgvAddressBook.TabIndex = 37;
            this.dgvAddressBook.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAddressBook_CellContentClick);
            // 
            // txtAddBookType
            // 
            this.txtAddBookType.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtAddBookType.Location = new System.Drawing.Point(11, 27);
            this.txtAddBookType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAddBookType.Name = "txtAddBookType";
            this.txtAddBookType.Size = new System.Drawing.Size(231, 23);
            this.txtAddBookType.TabIndex = 39;
            // 
            // dgvAddressBookList
            // 
            this.dgvAddressBookList.AllowUserToAddRows = false;
            this.dgvAddressBookList.AllowUserToResizeRows = false;
            this.dgvAddressBookList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvAddressBookList.BackgroundColor = System.Drawing.Color.White;
            this.dgvAddressBookList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAddressBookList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvAddressBookList.Location = new System.Drawing.Point(11, 57);
            this.dgvAddressBookList.MultiSelect = false;
            this.dgvAddressBookList.Name = "dgvAddressBookList";
            this.dgvAddressBookList.RowHeadersVisible = false;
            this.dgvAddressBookList.RowTemplate.Height = 50;
            this.dgvAddressBookList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAddressBookList.Size = new System.Drawing.Size(337, 413);
            this.dgvAddressBookList.TabIndex = 38;
            this.dgvAddressBookList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAddressBookList_CellClick);
            this.dgvAddressBookList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAddressBookList_CellDoubleClick);
            // 
            // lblApplicationVersion
            // 
            this.lblApplicationVersion.AutoSize = true;
            this.lblApplicationVersion.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.lblApplicationVersion.ForeColor = System.Drawing.Color.Gray;
            this.lblApplicationVersion.Location = new System.Drawing.Point(876, 4);
            this.lblApplicationVersion.Name = "lblApplicationVersion";
            this.lblApplicationVersion.Size = new System.Drawing.Size(23, 13);
            this.lblApplicationVersion.TabIndex = 64;
            this.lblApplicationVersion.Text = "RS";
            // 
            // tmrAppointment
            // 
            this.tmrAppointment.Enabled = true;
            this.tmrAppointment.Interval = 60000;
            this.tmrAppointment.Tick += new System.EventHandler(this.tmrAppointment_Tick);
            // 
            // tmrTwiliolog
            // 
            this.tmrTwiliolog.Enabled = true;
            this.tmrTwiliolog.Interval = 300000;
            this.tmrTwiliolog.Tick += new System.EventHandler(this.tmrTwiliolog_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(93, 26);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // tmrbadgenotification
            // 
            this.tmrbadgenotification.Enabled = true;
            this.tmrbadgenotification.Interval = 60000;
            this.tmrbadgenotification.Tick += new System.EventHandler(this.tmrbadgenotification_Tick);
            // 
            // tmrAPI
            // 
            this.tmrAPI.Interval = 3600000;
            this.tmrAPI.Tick += new System.EventHandler(this.tmrAPI_Tick);
            // 
            // tmrTwoSync
            // 
            this.tmrTwoSync.Enabled = true;
            this.tmrTwoSync.Interval = 300000;
            this.tmrTwoSync.Tick += new System.EventHandler(this.tmrTwoSync_Tick);
            // 
            // tooltipPenddingreminder
            // 
            this.tooltipPenddingreminder.BackColor = System.Drawing.Color.SteelBlue;
            this.tooltipPenddingreminder.ForeColor = System.Drawing.Color.White;
            this.tooltipPenddingreminder.IsBalloon = true;
            this.tooltipPenddingreminder.Popup += new System.Windows.Forms.PopupEventHandler(this.tooltipPenddingreminder_Popup);
            // 
            // pbhelp
            // 
            this.pbhelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbhelp.Image = global::SynergyNotificationSystem.Properties.Resources.Help1;
            this.pbhelp.Location = new System.Drawing.Point(963, 3);
            this.pbhelp.Name = "pbhelp";
            this.pbhelp.Size = new System.Drawing.Size(16, 16);
            this.pbhelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbhelp.TabIndex = 65;
            this.pbhelp.TabStop = false;
            this.pbhelp.Click += new System.EventHandler(this.pbhelp_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label59);
            this.groupBox13.Controls.Add(this.cbLanguage);
            this.groupBox13.Controls.Add(this.btnSetLanguage);
            this.groupBox13.Location = new System.Drawing.Point(7, 626);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(498, 100);
            this.groupBox13.TabIndex = 135;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Default Language";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label59.ForeColor = System.Drawing.Color.SteelBlue;
            this.label59.Location = new System.Drawing.Point(11, 45);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(115, 16);
            this.label59.TabIndex = 121;
            this.label59.Text = "Select Language";
            // 
            // cbLanguage
            // 
            this.cbLanguage.FormattingEnabled = true;
            this.cbLanguage.Items.AddRange(new object[] {
            "12",
            "24"});
            this.cbLanguage.Location = new System.Drawing.Point(135, 45);
            this.cbLanguage.Name = "cbLanguage";
            this.cbLanguage.Size = new System.Drawing.Size(159, 23);
            this.cbLanguage.TabIndex = 120;
            // 
            // btnSetLanguage
            // 
            this.btnSetLanguage.BackColor = System.Drawing.Color.SteelBlue;
            this.btnSetLanguage.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnSetLanguage.ForeColor = System.Drawing.Color.White;
            this.btnSetLanguage.Location = new System.Drawing.Point(300, 41);
            this.btnSetLanguage.Name = "btnSetLanguage";
            this.btnSetLanguage.Size = new System.Drawing.Size(180, 29);
            this.btnSetLanguage.TabIndex = 119;
            this.btnSetLanguage.Text = "Set Default Language";
            this.btnSetLanguage.UseVisualStyleBackColor = false;
            this.btnSetLanguage.Click += new System.EventHandler(this.btnSetLanguage_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(984, 507);
            this.Controls.Add(this.pbhelp);
            this.Controls.Add(this.lblApplicationVersion);
            this.Controls.Add(this.Calendars);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Synergy Notification System";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.Resize += new System.EventHandler(this.Main_Resize);
            this.Settingtb.ResumeLayout(false);
            this.pnlSetting.ResumeLayout(false);
            this.SettingSubtab.ResumeLayout(false);
            this.Appointmenttb.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdTemplateList)).EndInit();
            this.Notificationtb.ResumeLayout(false);
            this.Notificationtb.PerformLayout();
            this.Notificationtabpanel.ResumeLayout(false);
            this.Notificationtabpanel.PerformLayout();
            this.Emailsetting.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.grbCall.ResumeLayout(false);
            this.grbCall.PerformLayout();
            this.grbSMS.ResumeLayout(false);
            this.grbSMS.PerformLayout();
            this.grbEmail.ResumeLayout(false);
            this.grbEmail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmailSend)).EndInit();
            this.OtherSettings.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.pnlbulkTiming.ResumeLayout(false);
            this.pnlbulkTiming.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdEMAIL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdapplyAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCALL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSMS)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdtimeSlot)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pblogo)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdLogColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdReminderColor)).EndInit();
            this.RegistrationDetailstb.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Reportstab.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlPassword.ResumeLayout(false);
            this.pnlPassword.PerformLayout();
            this.Logtb.ResumeLayout(false);
            this.Logtb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdTwilioLog)).EndInit();
            this.Contact.ResumeLayout(false);
            this.Contact.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Contactgridview)).EndInit();
            this.Calendartab.ResumeLayout(false);
            this.Calendartab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdAgendadata)).EndInit();
            this.Calendars.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddressBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAddressBookList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbhelp)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabPage InboxOutbox;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage Settingtb;
        private System.Windows.Forms.TabPage Logtb;
        private System.Windows.Forms.TabPage Contact;
        private System.Windows.Forms.Button AddAppointment;
        private System.Windows.Forms.Label line;
        private System.Windows.Forms.Button addcontbtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Monthbtn;
        private System.Windows.Forms.ComboBox Datecombo;
        private System.Windows.Forms.Label tolbl;
        private System.Windows.Forms.DateTimePicker enddateTimePicker;
        private System.Windows.Forms.DateTimePicker strtdateTimePicker;
        public System.Windows.Forms.TabControl Calendars;
        private System.Windows.Forms.DataGridView Contactgridview;
        private System.Windows.Forms.Label lbldate;
        private System.Windows.Forms.Label lblApplicationVersion;
        private System.Windows.Forms.TextBox txtsearchValue;
        private System.Windows.Forms.Button btnDelete;
        public System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Panel pnlconfirm;
        private System.Windows.Forms.Panel pnlreschedule;
        private System.Windows.Forms.Panel pnlnorepsonce;
        private System.Windows.Forms.Panel pnlcancel;
        private System.Windows.Forms.Panel pnlcallback;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Button btnrefreshContact;
        private System.Windows.Forms.DataGridView grdTwilioLog;
        public System.Windows.Forms.Button btnRefeshTwilioLog;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox drpogFilter;
        private System.Windows.Forms.DateTimePicker txtTwilioFilterTo;
        private System.Windows.Forms.DateTimePicker txtTwilioFilterFrom;
        private System.Windows.Forms.Label lblTwilioTO;
        private System.Windows.Forms.DataGridView grdAgendadata;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pnlLogNoans;
        private System.Windows.Forms.Panel pnlLogUndeliverd;
        private System.Windows.Forms.Panel pnlLogComplete;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel pnlLogDeliverd;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvAddressBook;
        private System.Windows.Forms.DataGridView dgvAddressBookList;
        private System.Windows.Forms.TextBox txtSearchAddBook;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnAddNewAddBook;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtAddBookType;
        private System.Windows.Forms.Label lblAddBookTypeId;
        private System.Windows.Forms.TextBox txtTwilioLogsearch;
        private System.Windows.Forms.Button btnDateSearchLog;
        private System.Windows.Forms.CheckBox chkAllAddBook;
        private System.Windows.Forms.PictureBox pbhelp;
        private System.Windows.Forms.Button Cntimportbtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDeleteAddBook;
        private System.Windows.Forms.Button btnSaveAddBook;
        public System.Windows.Forms.TextBox searchtxt;
        private System.Windows.Forms.Button btnserchfirst;
        private System.Windows.Forms.Button btnsync;
        public System.Windows.Forms.TabPage Calendartab;
        private System.Windows.Forms.Panel pnlPassword;
        private System.Windows.Forms.Panel pnlSetting;
        private System.Windows.Forms.Label lblHead;
        private System.Windows.Forms.TextBox txtpasswordSetting;
        private System.Windows.Forms.TextBox txtusername;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Button btnsignin;
        private System.Windows.Forms.Timer tmrAppointment;
        private System.Windows.Forms.Timer tmrTwiliolog;
        private System.Windows.Forms.CheckBox chkprivateFilter;
        private System.Windows.Forms.TextBox txtgridSearch;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Timer tmrbadgenotification;
        private System.Windows.Forms.Timer tmrTwoSync;
        public System.Windows.Forms.TabControl SettingSubtab;
        private System.Windows.Forms.TabPage Appointmenttb;
        public System.Windows.Forms.Button btnappointmentRefresh;
        private System.Windows.Forms.Button crtnewtempbtn;
        private System.Windows.Forms.DataGridView grdTemplateList;
        private System.Windows.Forms.TabPage Notificationtb;
        private System.Windows.Forms.Panel Notificationtabpanel;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtEmailSendTo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button Savebtn;
        private System.Windows.Forms.ComboBox drpSMSNotication;
        private System.Windows.Forms.CheckBox chkSMSWhen;
        private System.Windows.Forms.CheckBox chkSMSReschedule;
        private System.Windows.Forms.CheckBox ChkSMSCancel;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtSMSSendTo;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox drpEmailNotication;
        private System.Windows.Forms.CheckBox chkEmailWhen;
        private System.Windows.Forms.CheckBox chkEmailReschedule;
        private System.Windows.Forms.CheckBox chkEmailCancel;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage Emailsetting;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtClientKey;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnEmailSettings;
        private System.Windows.Forms.GroupBox grbCall;
        private System.Windows.Forms.TextBox txtAuthoToken;
        private System.Windows.Forms.TextBox txtCallNo;
        private System.Windows.Forms.TextBox txtAccountSID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox grbSMS;
        private System.Windows.Forms.TextBox txtSMSSenderNo;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox grbEmail;
        private System.Windows.Forms.PictureBox picEmailSend;
        private System.Windows.Forms.Button btnTestMail;
        private System.Windows.Forms.TextBox txtSMTPPort;
        private System.Windows.Forms.TextBox txtSMTPhost;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtEmailSubject;
        private System.Windows.Forms.TextBox txtEmailID;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TabPage OtherSettings;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btnA1sync;
        private System.Windows.Forms.CheckBox chkA1toRS;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button btnUpdateBulk;
        private System.Windows.Forms.Panel pnlbulkTiming;
        private System.Windows.Forms.DataGridView grdEMAIL;
        private System.Windows.Forms.DataGridView grdapplyAll;
        private System.Windows.Forms.CheckBox chkSMS;
        private System.Windows.Forms.CheckBox chkCALL;
        private System.Windows.Forms.DataGridView grdCALL;
        private System.Windows.Forms.CheckBox chkEMAIL;
        private System.Windows.Forms.DataGridView grdSMS;
        private System.Windows.Forms.ComboBox drpBulkType;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnChangeTimeZone;
        private System.Windows.Forms.ComboBox drpTimeZone;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnpasswordChange;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.TextBox txtNewpassword;
        private System.Windows.Forms.TextBox txtOldpassword;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DataGridView grdtimeSlot;
        private System.Windows.Forms.Button btnAddtime;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.ComboBox drptimeSlot;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.Button btnRemoveLogo;
        private System.Windows.Forms.Label lblPriviewLogo;
        private System.Windows.Forms.PictureBox pblogo;
        private System.Windows.Forms.Label lblImagename;
        private System.Windows.Forms.TextBox txtsignature;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnCompanyLogo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCompanyLogo;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label Appoint;
        private System.Windows.Forms.DataGridView grdLogColor;
        private System.Windows.Forms.DataGridView grdReminderColor;
        private System.Windows.Forms.TabPage RegistrationDetailstb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblRegistrationMac;
        private System.Windows.Forms.Button btnRegiCancel;
        private System.Windows.Forms.Button btnregistrationUpdate;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtproductkey;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmailID;
        private System.Windows.Forms.TextBox txtMobileNo;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TabPage Reportstab;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Button btnReportExport;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.DateTimePicker enddateTimePickerReport;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.DateTimePicker strtdateTimePickerReport;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.CheckBox chkrecall;
        private System.Windows.Forms.Button btnrecallUpdate;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.ComboBox drpReinterval;
        private System.Windows.Forms.ComboBox drpHMT;
        private System.Windows.Forms.Panel pnlrecallSetting;
        private System.Windows.Forms.Timer tmrAPI;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnCSVupload;
        private System.Windows.Forms.CheckBox chkAPI;
        private System.Windows.Forms.Label lblcsvFilePath;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblbatFilePath;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btnTriggerPramotionlas;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Button btnuploadBatfile;
        private System.Windows.Forms.TextBox txtAPIbatfile;
        private System.Windows.Forms.Button Browsebtn;
        private System.Windows.Forms.TextBox txtbrowseFile;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Button btnbrowsepramotionals;
        private System.Windows.Forms.TextBox txtCSVpathpramotionals;
        //private System.Windows.Forms.Timer tmrTwoSync;
        private System.Windows.Forms.Button btnPendingEvent;
        private System.Windows.Forms.ToolTip tooltipPenddingreminder;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.ComboBox cbLanguage;
        private System.Windows.Forms.Button btnSetLanguage;
    }
}

