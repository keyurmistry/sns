﻿namespace AppToto
{
    partial class ConfirmEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfirmEvent));
            this.drpAMPM = new System.Windows.Forms.ComboBox();
            this.drpMinute = new System.Windows.Forms.ComboBox();
            this.fromdatepicker = new System.Windows.Forms.DateTimePicker();
            this.drphours = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAMPM = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.lblHead = new System.Windows.Forms.Label();
            this.Submiteventbtn = new System.Windows.Forms.Button();
            this.btnConfirmCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // drpAMPM
            // 
            this.drpAMPM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpAMPM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpAMPM.DropDownHeight = 95;
            this.drpAMPM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpAMPM.FormattingEnabled = true;
            this.drpAMPM.IntegralHeight = false;
            this.drpAMPM.ItemHeight = 14;
            this.drpAMPM.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.drpAMPM.Location = new System.Drawing.Point(376, 74);
            this.drpAMPM.Name = "drpAMPM";
            this.drpAMPM.Size = new System.Drawing.Size(89, 22);
            this.drpAMPM.TabIndex = 112;
            this.drpAMPM.Leave += new System.EventHandler(this.drpAMPM_Leave);
            // 
            // drpMinute
            // 
            this.drpMinute.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpMinute.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpMinute.DropDownHeight = 95;
            this.drpMinute.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpMinute.FormattingEnabled = true;
            this.drpMinute.IntegralHeight = false;
            this.drpMinute.ItemHeight = 14;
            this.drpMinute.Location = new System.Drawing.Point(279, 74);
            this.drpMinute.Name = "drpMinute";
            this.drpMinute.Size = new System.Drawing.Size(89, 22);
            this.drpMinute.TabIndex = 111;
            this.drpMinute.Leave += new System.EventHandler(this.drpMinute_Leave);
            // 
            // fromdatepicker
            // 
            this.fromdatepicker.CustomFormat = "dd-MMM-yyyy";
            this.fromdatepicker.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromdatepicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fromdatepicker.Location = new System.Drawing.Point(89, 74);
            this.fromdatepicker.Name = "fromdatepicker";
            this.fromdatepicker.Size = new System.Drawing.Size(89, 22);
            this.fromdatepicker.TabIndex = 109;
            this.fromdatepicker.Value = new System.DateTime(2016, 4, 15, 19, 41, 0, 0);
            // 
            // drphours
            // 
            this.drphours.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drphours.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drphours.DropDownHeight = 95;
            this.drphours.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drphours.FormattingEnabled = true;
            this.drphours.IntegralHeight = false;
            this.drphours.ItemHeight = 14;
            this.drphours.Location = new System.Drawing.Point(183, 74);
            this.drphours.Name = "drphours";
            this.drphours.Size = new System.Drawing.Size(89, 22);
            this.drphours.TabIndex = 110;
            this.drphours.Leave += new System.EventHandler(this.drphours_Leave);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(24, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 19);
            this.label5.TabIndex = 113;
            this.label5.Text = "Time :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(283, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 12);
            this.label1.TabIndex = 117;
            this.label1.Text = "MM";
            // 
            // lblAMPM
            // 
            this.lblAMPM.AutoSize = true;
            this.lblAMPM.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblAMPM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAMPM.Location = new System.Drawing.Point(379, 60);
            this.lblAMPM.Name = "lblAMPM";
            this.lblAMPM.Size = new System.Drawing.Size(36, 12);
            this.lblAMPM.TabIndex = 116;
            this.lblAMPM.Text = "AM/PM";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label12.Location = new System.Drawing.Point(184, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 12);
            this.label12.TabIndex = 115;
            this.label12.Text = "HH";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(12, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 19);
            this.label2.TabIndex = 118;
            this.label2.Text = "Status :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbStatus
            // 
            this.cmbStatus.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.cmbStatus.Location = new System.Drawing.Point(89, 113);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(183, 23);
            this.cmbStatus.TabIndex = 119;
            // 
            // lblHead
            // 
            this.lblHead.BackColor = System.Drawing.Color.SteelBlue;
            this.lblHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblHead.Font = new System.Drawing.Font("Arial", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.lblHead.ForeColor = System.Drawing.Color.White;
            this.lblHead.Location = new System.Drawing.Point(0, 0);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(479, 40);
            this.lblHead.TabIndex = 120;
            this.lblHead.Text = "Manually Update Reminder Details";
            this.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHead.UseWaitCursor = true;
            // 
            // Submiteventbtn
            // 
            this.Submiteventbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Submiteventbtn.BackColor = System.Drawing.Color.SteelBlue;
            this.Submiteventbtn.FlatAppearance.BorderSize = 5;
            this.Submiteventbtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Submiteventbtn.ForeColor = System.Drawing.Color.White;
            this.Submiteventbtn.Location = new System.Drawing.Point(90, 169);
            this.Submiteventbtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Submiteventbtn.Name = "Submiteventbtn";
            this.Submiteventbtn.Size = new System.Drawing.Size(71, 33);
            this.Submiteventbtn.TabIndex = 121;
            this.Submiteventbtn.Text = "Update";
            this.Submiteventbtn.UseVisualStyleBackColor = false;
            this.Submiteventbtn.Click += new System.EventHandler(this.Submiteventbtn_Click);
            // 
            // btnConfirmCancel
            // 
            this.btnConfirmCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline;
            this.btnConfirmCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfirmCancel.BackColor = System.Drawing.Color.SteelBlue;
            this.btnConfirmCancel.FlatAppearance.BorderSize = 5;
            this.btnConfirmCancel.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnConfirmCancel.ForeColor = System.Drawing.Color.White;
            this.btnConfirmCancel.Location = new System.Drawing.Point(172, 169);
            this.btnConfirmCancel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnConfirmCancel.Name = "btnConfirmCancel";
            this.btnConfirmCancel.Size = new System.Drawing.Size(71, 33);
            this.btnConfirmCancel.TabIndex = 122;
            this.btnConfirmCancel.Text = "Cancel";
            this.btnConfirmCancel.UseVisualStyleBackColor = false;
            this.btnConfirmCancel.Click += new System.EventHandler(this.btnConfirmCancel_Click);
            // 
            // ConfirmEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 228);
            this.Controls.Add(this.btnConfirmCancel);
            this.Controls.Add(this.Submiteventbtn);
            this.Controls.Add(this.lblHead);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAMPM);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.drpAMPM);
            this.Controls.Add(this.drpMinute);
            this.Controls.Add(this.fromdatepicker);
            this.Controls.Add(this.drphours);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConfirmEvent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConfirmDatetime";
            this.Load += new System.EventHandler(this.ConfirmEvent_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox drpAMPM;
        private System.Windows.Forms.ComboBox drpMinute;
        private System.Windows.Forms.DateTimePicker fromdatepicker;
        private System.Windows.Forms.ComboBox drphours;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAMPM;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label lblHead;
        private System.Windows.Forms.Button Submiteventbtn;
        private System.Windows.Forms.Button btnConfirmCancel;
    }
}