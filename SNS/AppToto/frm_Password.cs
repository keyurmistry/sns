﻿using AppToto;
using DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReminderSystem
{
    public partial class frm_Password : Form
    {
        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        public frm_Password()
        {
            InitializeComponent();
        }

        private void btnsignin_Click(object sender, EventArgs e)
        {
            if (txtpassword.Text != "" && txtusername.Text != "")
            {
                DataSet dsEmailsign = new DataSet();

                SqlParameter[] p = new SqlParameter[2];

                p[0] = new SqlParameter("@EmailFrom", txtusername.Text);
                p[1] = new SqlParameter("@EmailPassword", txtpassword.Text);



                string s = "exec Sp_CheckEmailLogin '" + p[0].Value + "','" + p[1].Value + "'";

                dsEmailsign = dal.GetData(s, Application.StartupPath);

                if (dsEmailsign.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    try
                    {
                        this.Close();
                    }
                    catch { }


                    txtusername.Text = "";
                    txtpassword.Text = "";
                }

                else
                {

                    MessageBox.Show("Please Enter Valid Username & password.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {


                MessageBox.Show("Please Enter Username & Password.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            txtusername.Text = "";
            txtpassword.Text = "";
            
        }
    }
}
