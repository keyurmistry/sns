﻿namespace ReminderSystem
{
    partial class Registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registration));
            this.btnregister = new System.Windows.Forms.Button();
            this.lblKey = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmailID = new System.Windows.Forms.Label();
            this.btnCancelRegis = new System.Windows.Forms.Button();
            this.lblMacID = new System.Windows.Forms.Label();
            this.txtproductkey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblIp = new System.Windows.Forms.Label();
            this.chkworkstation = new System.Windows.Forms.CheckBox();
            this.lblHead = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnregister
            // 
            this.btnregister.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnregister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnregister.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnregister.ForeColor = System.Drawing.Color.Black;
            this.btnregister.Image = global::SynergyNotificationSystem.Properties.Resources._1345385847_tick_circle;
            this.btnregister.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnregister.Location = new System.Drawing.Point(219, 322);
            this.btnregister.Name = "btnregister";
            this.btnregister.Size = new System.Drawing.Size(85, 32);
            this.btnregister.TabIndex = 7;
            this.btnregister.Text = "Register";
            this.btnregister.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnregister.UseVisualStyleBackColor = true;
            this.btnregister.Click += new System.EventHandler(this.btnregister_Click);
            // 
            // lblKey
            // 
            this.lblKey.AutoSize = true;
            this.lblKey.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblKey.ForeColor = System.Drawing.Color.White;
            this.lblKey.Location = new System.Drawing.Point(16, 216);
            this.lblKey.Name = "lblKey";
            this.lblKey.Size = new System.Drawing.Size(0, 15);
            this.lblKey.TabIndex = 3;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCompanyName.ForeColor = System.Drawing.Color.Black;
            this.txtCompanyName.Location = new System.Drawing.Point(16, 74);
            this.txtCompanyName.MaxLength = 50;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(385, 23);
            this.txtCompanyName.TabIndex = 2;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyName.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblCompanyName.Location = new System.Drawing.Point(16, 51);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(385, 18);
            this.lblCompanyName.TabIndex = 1;
            this.lblCompanyName.Text = "Company Name";
            this.lblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMobileNo.ForeColor = System.Drawing.Color.Black;
            this.txtMobileNo.Location = new System.Drawing.Point(16, 129);
            this.txtMobileNo.MaxLength = 50;
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(385, 23);
            this.txtMobileNo.TabIndex = 4;
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNumber.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblMobileNumber.Location = new System.Drawing.Point(16, 106);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(385, 18);
            this.lblMobileNumber.TabIndex = 3;
            this.lblMobileNumber.Text = "Mobile Number";
            this.lblMobileNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.ForeColor = System.Drawing.Color.Black;
            this.txtEmail.Location = new System.Drawing.Point(16, 184);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(385, 23);
            this.txtEmail.TabIndex = 6;
            // 
            // lblEmailID
            // 
            this.lblEmailID.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailID.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblEmailID.Location = new System.Drawing.Point(16, 161);
            this.lblEmailID.Name = "lblEmailID";
            this.lblEmailID.Size = new System.Drawing.Size(385, 18);
            this.lblEmailID.TabIndex = 5;
            this.lblEmailID.Text = "Email Address";
            this.lblEmailID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCancelRegis
            // 
            this.btnCancelRegis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelRegis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelRegis.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.btnCancelRegis.ForeColor = System.Drawing.Color.Black;
            this.btnCancelRegis.Image = global::SynergyNotificationSystem.Properties.Resources._1345385963_Log_Out;
            this.btnCancelRegis.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelRegis.Location = new System.Drawing.Point(310, 322);
            this.btnCancelRegis.Name = "btnCancelRegis";
            this.btnCancelRegis.Size = new System.Drawing.Size(93, 32);
            this.btnCancelRegis.TabIndex = 8;
            this.btnCancelRegis.Text = "Exit";
            this.btnCancelRegis.UseVisualStyleBackColor = true;
            this.btnCancelRegis.Click += new System.EventHandler(this.btnCancelRegis_Click);
            // 
            // lblMacID
            // 
            this.lblMacID.AutoSize = true;
            this.lblMacID.Location = new System.Drawing.Point(76, 217);
            this.lblMacID.Name = "lblMacID";
            this.lblMacID.Size = new System.Drawing.Size(0, 13);
            this.lblMacID.TabIndex = 9;
            // 
            // txtproductkey
            // 
            this.txtproductkey.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtproductkey.ForeColor = System.Drawing.Color.Black;
            this.txtproductkey.Location = new System.Drawing.Point(16, 274);
            this.txtproductkey.MaxLength = 50;
            this.txtproductkey.Name = "txtproductkey";
            this.txtproductkey.Size = new System.Drawing.Size(385, 23);
            this.txtproductkey.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SteelBlue;
            this.label1.Location = new System.Drawing.Point(16, 219);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 18);
            this.label1.TabIndex = 13;
            this.label1.Text = "Product Key";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblIp
            // 
            this.lblIp.AutoSize = true;
            this.lblIp.Location = new System.Drawing.Point(79, 310);
            this.lblIp.Name = "lblIp";
            this.lblIp.Size = new System.Drawing.Size(35, 13);
            this.lblIp.TabIndex = 14;
            this.lblIp.Text = "label2";
            // 
            // chkworkstation
            // 
            this.chkworkstation.AutoSize = true;
            this.chkworkstation.Checked = true;
            this.chkworkstation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkworkstation.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.chkworkstation.ForeColor = System.Drawing.Color.SteelBlue;
            this.chkworkstation.Location = new System.Drawing.Point(16, 251);
            this.chkworkstation.Name = "chkworkstation";
            this.chkworkstation.Size = new System.Drawing.Size(193, 17);
            this.chkworkstation.TabIndex = 16;
            this.chkworkstation.Text = "Registration as a workstation";
            this.chkworkstation.UseVisualStyleBackColor = true;
            this.chkworkstation.CheckedChanged += new System.EventHandler(this.chkworkstation_CheckedChanged);
            // 
            // lblHead
            // 
            this.lblHead.BackColor = System.Drawing.Color.SteelBlue;
            this.lblHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHead.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead.ForeColor = System.Drawing.Color.White;
            this.lblHead.Location = new System.Drawing.Point(0, 0);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(413, 39);
            this.lblHead.TabIndex = 0;
            this.lblHead.Text = "Customer Registration";
            this.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Registration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(413, 367);
            this.Controls.Add(this.chkworkstation);
            this.Controls.Add(this.lblIp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtproductkey);
            this.Controls.Add(this.lblMacID);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.lblEmailID);
            this.Controls.Add(this.txtMobileNo);
            this.Controls.Add(this.lblMobileNumber);
            this.Controls.Add(this.txtCompanyName);
            this.Controls.Add(this.lblCompanyName);
            this.Controls.Add(this.lblHead);
            this.Controls.Add(this.lblKey);
            this.Controls.Add(this.btnCancelRegis);
            this.Controls.Add(this.btnregister);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Registration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Registration";
            this.Load += new System.EventHandler(this.Registration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnregister;
        private System.Windows.Forms.Button btnCancelRegis;
        private System.Windows.Forms.Label lblKey;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.TextBox txtMobileNo;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmailID;
        private System.Windows.Forms.Label lblMacID;
        private System.Windows.Forms.TextBox txtproductkey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblIp;
        private System.Windows.Forms.CheckBox chkworkstation;
        private System.Windows.Forms.Label lblHead;

    }
}