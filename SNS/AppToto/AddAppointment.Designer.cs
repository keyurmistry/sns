﻿using ReminderSystem;
namespace AppToto
{
    partial class AddAppointment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddAppointment));
            this.label46 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Titletxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.TimeZone = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bodytxt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.grdaddedparties = new System.Windows.Forms.DataGridView();
            this.drphours = new System.Windows.Forms.ComboBox();
            this.fromdatepicker = new System.Windows.Forms.DateTimePicker();
            this.grdParties = new System.Windows.Forms.DataGridView();
            this.lblselectionNo = new System.Windows.Forms.Label();
            this.lblpartiesFName = new System.Windows.Forms.Label();
            this.lblpartiesLName = new System.Windows.Forms.Label();
            this.lblCaseNo = new System.Windows.Forms.Label();
            this.drpMinute = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chkSMS = new System.Windows.Forms.CheckBox();
            this.chkCALL = new System.Windows.Forms.CheckBox();
            this.chkEMAIL = new System.Windows.Forms.CheckBox();
            this.locationtxt = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.drpToMinute = new System.Windows.Forms.ComboBox();
            this.enddatepicker = new System.Windows.Forms.DateTimePicker();
            this.drpToHour = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.drptemplete = new System.Windows.Forms.ComboBox();
            this.grdSMS = new System.Windows.Forms.DataGridView();
            this.grdCALL = new System.Windows.Forms.DataGridView();
            this.grdEMAIL = new System.Windows.Forms.DataGridView();
            this.label18 = new System.Windows.Forms.Label();
            this.grdapplyAll = new System.Windows.Forms.DataGridView();
            this.txtEventBody = new MSDN.Html.Editor.HtmlEditorControl();
            this.lnkAddContact = new System.Windows.Forms.LinkLabel();
            this.lblHead = new System.Windows.Forms.Label();
            this.drpAddressBookList = new System.Windows.Forms.ComboBox();
            this.drpAMPM = new System.Windows.Forms.ComboBox();
            this.lblAMPM = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lnkImortAppointment = new System.Windows.Forms.LinkLabel();
            this.chkprivate = new System.Windows.Forms.CheckBox();
            this.chkIsConfirmable = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtappointmentType = new System.Windows.Forms.TextBox();
            this.drpTwilioLang = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.Submiteventbtn = new System.Windows.Forms.Button();
            this.btnaddparties = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdaddedparties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdParties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCALL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdEMAIL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdapplyAll)).BeginInit();
            this.SuspendLayout();
            // 
            // label46
            // 
            this.label46.BackColor = System.Drawing.Color.Gainsboro;
            this.label46.Location = new System.Drawing.Point(568, 60);
            this.label46.MaximumSize = new System.Drawing.Size(20, 120);
            this.label46.MinimumSize = new System.Drawing.Size(1, 600);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(1, 600);
            this.label46.TabIndex = 28;
            this.label46.Text = "label46";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SteelBlue;
            this.label2.Location = new System.Drawing.Point(8, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 23);
            this.label2.TabIndex = 29;
            this.label2.Text = "Reminder Details";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.SteelBlue;
            this.label3.Location = new System.Drawing.Point(592, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 23);
            this.label3.TabIndex = 30;
            this.label3.Text = "Participant Details";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(84, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 19);
            this.label4.TabIndex = 31;
            this.label4.Text = "Title :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(25, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 19);
            this.label5.TabIndex = 32;
            this.label5.Text = "Time :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Titletxt
            // 
            this.Titletxt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Titletxt.Location = new System.Drawing.Point(149, 91);
            this.Titletxt.Name = "Titletxt";
            this.Titletxt.Size = new System.Drawing.Size(389, 22);
            this.Titletxt.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(25, 164);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 19);
            this.label8.TabIndex = 36;
            this.label8.Text = "Event Template :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TimeZone
            // 
            this.TimeZone.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.TimeZone.FormattingEnabled = true;
            this.TimeZone.IntegralHeight = false;
            this.TimeZone.Location = new System.Drawing.Point(646, 609);
            this.TimeZone.Name = "TimeZone";
            this.TimeZone.Size = new System.Drawing.Size(14, 23);
            this.TimeZone.TabIndex = 42;
            this.TimeZone.Visible = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(53, 235);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 19);
            this.label6.TabIndex = 44;
            this.label6.Text = "Event Body :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bodytxt
            // 
            this.bodytxt.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.bodytxt.Location = new System.Drawing.Point(150, 259);
            this.bodytxt.Multiline = true;
            this.bodytxt.Name = "bodytxt";
            this.bodytxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.bodytxt.Size = new System.Drawing.Size(371, 116);
            this.bodytxt.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.Location = new System.Drawing.Point(592, 92);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 16);
            this.label10.TabIndex = 46;
            this.label10.Text = "Search :";
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.ForeColor = System.Drawing.Color.Blue;
            this.txtSearch.Location = new System.Drawing.Point(656, 91);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(145, 22);
            this.txtSearch.TabIndex = 13;
            this.txtSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyUp);
            // 
            // grdaddedparties
            // 
            this.grdaddedparties.AllowUserToAddRows = false;
            this.grdaddedparties.AllowUserToResizeRows = false;
            this.grdaddedparties.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdaddedparties.BackgroundColor = System.Drawing.Color.White;
            this.grdaddedparties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdaddedparties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.grdaddedparties.Location = new System.Drawing.Point(598, 385);
            this.grdaddedparties.Name = "grdaddedparties";
            this.grdaddedparties.RowHeadersVisible = false;
            this.grdaddedparties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdaddedparties.Size = new System.Drawing.Size(332, 216);
            this.grdaddedparties.TabIndex = 17;
            this.grdaddedparties.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdaddedparties_CellContentClick);
            this.grdaddedparties.Paint += new System.Windows.Forms.PaintEventHandler(this.grdaddedparties_Paint);
            // 
            // drphours
            // 
            this.drphours.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drphours.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drphours.DropDownHeight = 95;
            this.drphours.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drphours.FormattingEnabled = true;
            this.drphours.IntegralHeight = false;
            this.drphours.ItemHeight = 14;
            this.drphours.Location = new System.Drawing.Point(266, 131);
            this.drphours.Name = "drphours";
            this.drphours.Size = new System.Drawing.Size(98, 22);
            this.drphours.TabIndex = 2;
            this.drphours.Leave += new System.EventHandler(this.drphours_Leave);
            // 
            // fromdatepicker
            // 
            this.fromdatepicker.CustomFormat = "dd-MMM-yyyy";
            this.fromdatepicker.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromdatepicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fromdatepicker.Location = new System.Drawing.Point(149, 131);
            this.fromdatepicker.Name = "fromdatepicker";
            this.fromdatepicker.Size = new System.Drawing.Size(111, 22);
            this.fromdatepicker.TabIndex = 1;
            this.fromdatepicker.Value = new System.DateTime(2016, 4, 15, 19, 41, 0, 0);
            // 
            // grdParties
            // 
            this.grdParties.AllowUserToAddRows = false;
            this.grdParties.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdParties.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.grdParties.BackgroundColor = System.Drawing.Color.White;
            this.grdParties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdParties.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.grdParties.Location = new System.Drawing.Point(598, 120);
            this.grdParties.Name = "grdParties";
            this.grdParties.RowHeadersVisible = false;
            this.grdParties.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdParties.Size = new System.Drawing.Size(332, 225);
            this.grdParties.TabIndex = 15;
            this.grdParties.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdParties_CellClick);
            this.grdParties.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdParties_CellContentClick);
            this.grdParties.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdParties_CellContentDoubleClick);
            this.grdParties.Paint += new System.Windows.Forms.PaintEventHandler(this.grdParties_Paint);
            // 
            // lblselectionNo
            // 
            this.lblselectionNo.AutoSize = true;
            this.lblselectionNo.Location = new System.Drawing.Point(736, 9);
            this.lblselectionNo.Name = "lblselectionNo";
            this.lblselectionNo.Size = new System.Drawing.Size(0, 13);
            this.lblselectionNo.TabIndex = 62;
            this.lblselectionNo.Visible = false;
            // 
            // lblpartiesFName
            // 
            this.lblpartiesFName.AutoSize = true;
            this.lblpartiesFName.ForeColor = System.Drawing.Color.White;
            this.lblpartiesFName.Location = new System.Drawing.Point(653, 364);
            this.lblpartiesFName.Name = "lblpartiesFName";
            this.lblpartiesFName.Size = new System.Drawing.Size(39, 13);
            this.lblpartiesFName.TabIndex = 63;
            this.lblpartiesFName.Text = "Fname";
            this.lblpartiesFName.Visible = false;
            // 
            // lblpartiesLName
            // 
            this.lblpartiesLName.AutoSize = true;
            this.lblpartiesLName.ForeColor = System.Drawing.Color.White;
            this.lblpartiesLName.Location = new System.Drawing.Point(593, 359);
            this.lblpartiesLName.Name = "lblpartiesLName";
            this.lblpartiesLName.Size = new System.Drawing.Size(35, 13);
            this.lblpartiesLName.TabIndex = 64;
            this.lblpartiesLName.Text = "lname";
            this.lblpartiesLName.Visible = false;
            // 
            // lblCaseNo
            // 
            this.lblCaseNo.AutoSize = true;
            this.lblCaseNo.ForeColor = System.Drawing.Color.White;
            this.lblCaseNo.Location = new System.Drawing.Point(593, 364);
            this.lblCaseNo.Name = "lblCaseNo";
            this.lblCaseNo.Size = new System.Drawing.Size(42, 13);
            this.lblCaseNo.TabIndex = 65;
            this.lblCaseNo.Text = "caseno";
            this.lblCaseNo.Visible = false;
            // 
            // drpMinute
            // 
            this.drpMinute.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpMinute.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpMinute.DropDownHeight = 95;
            this.drpMinute.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpMinute.FormattingEnabled = true;
            this.drpMinute.IntegralHeight = false;
            this.drpMinute.ItemHeight = 14;
            this.drpMinute.Location = new System.Drawing.Point(370, 131);
            this.drpMinute.Name = "drpMinute";
            this.drpMinute.Size = new System.Drawing.Size(98, 22);
            this.drpMinute.TabIndex = 3;
            this.drpMinute.Leave += new System.EventHandler(this.drpMinute_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label12.Location = new System.Drawing.Point(267, 117);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 12);
            this.label12.TabIndex = 69;
            this.label12.Text = "HH";
            // 
            // chkSMS
            // 
            this.chkSMS.AutoSize = true;
            this.chkSMS.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSMS.Location = new System.Drawing.Point(219, 446);
            this.chkSMS.Name = "chkSMS";
            this.chkSMS.Size = new System.Drawing.Size(53, 18);
            this.chkSMS.TabIndex = 7;
            this.chkSMS.Text = "SMS";
            this.chkSMS.UseVisualStyleBackColor = true;
            this.chkSMS.CheckedChanged += new System.EventHandler(this.chkSMS_CheckedChanged);
            // 
            // chkCALL
            // 
            this.chkCALL.AutoSize = true;
            this.chkCALL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCALL.Location = new System.Drawing.Point(334, 446);
            this.chkCALL.Name = "chkCALL";
            this.chkCALL.Size = new System.Drawing.Size(57, 18);
            this.chkCALL.TabIndex = 8;
            this.chkCALL.Text = "CALL";
            this.chkCALL.UseVisualStyleBackColor = true;
            this.chkCALL.CheckedChanged += new System.EventHandler(this.chkCALL_CheckedChanged);
            // 
            // chkEMAIL
            // 
            this.chkEMAIL.AutoSize = true;
            this.chkEMAIL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEMAIL.Location = new System.Drawing.Point(447, 446);
            this.chkEMAIL.Name = "chkEMAIL";
            this.chkEMAIL.Size = new System.Drawing.Size(65, 18);
            this.chkEMAIL.TabIndex = 9;
            this.chkEMAIL.Text = "EMAIL";
            this.chkEMAIL.UseVisualStyleBackColor = true;
            this.chkEMAIL.CheckedChanged += new System.EventHandler(this.chkEMAIL_CheckedChanged);
            // 
            // locationtxt
            // 
            this.locationtxt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.locationtxt.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.locationtxt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.locationtxt.FormattingEnabled = true;
            this.locationtxt.IntegralHeight = false;
            this.locationtxt.Location = new System.Drawing.Point(149, 164);
            this.locationtxt.Name = "locationtxt";
            this.locationtxt.Size = new System.Drawing.Size(389, 22);
            this.locationtxt.TabIndex = 4;
            this.locationtxt.SelectedIndexChanged += new System.EventHandler(this.locationtxt_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label13.Location = new System.Drawing.Point(638, 614);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 12);
            this.label13.TabIndex = 93;
            this.label13.Text = "MM";
            this.label13.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label11.Location = new System.Drawing.Point(624, 614);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 12);
            this.label11.TabIndex = 92;
            this.label11.Text = "HH";
            this.label11.Visible = false;
            // 
            // drpToMinute
            // 
            this.drpToMinute.DropDownHeight = 95;
            this.drpToMinute.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpToMinute.FormattingEnabled = true;
            this.drpToMinute.IntegralHeight = false;
            this.drpToMinute.ItemHeight = 13;
            this.drpToMinute.Location = new System.Drawing.Point(621, 609);
            this.drpToMinute.Name = "drpToMinute";
            this.drpToMinute.Size = new System.Drawing.Size(58, 21);
            this.drpToMinute.TabIndex = 91;
            this.drpToMinute.Visible = false;
            // 
            // enddatepicker
            // 
            this.enddatepicker.CustomFormat = Helper.UniversalDateFormat;
            this.enddatepicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.enddatepicker.Location = new System.Drawing.Point(604, 610);
            this.enddatepicker.Name = "enddatepicker";
            this.enddatepicker.Size = new System.Drawing.Size(28, 20);
            this.enddatepicker.TabIndex = 90;
            this.enddatepicker.Visible = false;
            // 
            // drpToHour
            // 
            this.drpToHour.DropDownHeight = 95;
            this.drpToHour.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpToHour.FormattingEnabled = true;
            this.drpToHour.IntegralHeight = false;
            this.drpToHour.Location = new System.Drawing.Point(624, 609);
            this.drpToHour.Name = "drpToHour";
            this.drpToHour.Size = new System.Drawing.Size(58, 21);
            this.drpToHour.TabIndex = 89;
            this.drpToHour.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(600, 607);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 19);
            this.label7.TabIndex = 35;
            this.label7.Text = "Time Zone :";
            this.label7.Visible = false;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(25, 441);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(122, 42);
            this.label16.TabIndex = 94;
            this.label16.Text = "Reminder Types :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(50, 394);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(97, 19);
            this.label17.TabIndex = 99;
            this.label17.Text = "Insert Field :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // drptemplete
            // 
            this.drptemplete.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drptemplete.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drptemplete.FormattingEnabled = true;
            this.drptemplete.Location = new System.Drawing.Point(150, 394);
            this.drptemplete.Name = "drptemplete";
            this.drptemplete.Size = new System.Drawing.Size(131, 22);
            this.drptemplete.TabIndex = 6;
            this.drptemplete.SelectedIndexChanged += new System.EventHandler(this.drptemplete_SelectedIndexChanged);
            // 
            // grdSMS
            // 
            this.grdSMS.AllowUserToAddRows = false;
            this.grdSMS.AllowUserToResizeColumns = false;
            this.grdSMS.AllowUserToResizeRows = false;
            this.grdSMS.BackgroundColor = System.Drawing.Color.White;
            this.grdSMS.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdSMS.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdSMS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSMS.ColumnHeadersVisible = false;
            this.grdSMS.Location = new System.Drawing.Point(211, 473);
            this.grdSMS.Name = "grdSMS";
            this.grdSMS.RowHeadersVisible = false;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdSMS.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grdSMS.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdSMS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdSMS.ShowCellToolTips = false;
            this.grdSMS.Size = new System.Drawing.Size(100, 199);
            this.grdSMS.TabIndex = 10;
            this.grdSMS.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSMS_CellContentClick);
            this.grdSMS.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSMS_CellValueChanged);
            this.grdSMS.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdSMS_CurrentCellDirtyStateChanged);
            // 
            // grdCALL
            // 
            this.grdCALL.AllowUserToAddRows = false;
            this.grdCALL.AllowUserToResizeColumns = false;
            this.grdCALL.AllowUserToResizeRows = false;
            this.grdCALL.BackgroundColor = System.Drawing.Color.White;
            this.grdCALL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCALL.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdCALL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCALL.ColumnHeadersVisible = false;
            this.grdCALL.Location = new System.Drawing.Point(325, 473);
            this.grdCALL.Name = "grdCALL";
            this.grdCALL.RowHeadersVisible = false;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdCALL.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grdCALL.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grdCALL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdCALL.Size = new System.Drawing.Size(100, 199);
            this.grdCALL.TabIndex = 11;
            this.grdCALL.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCALL_CellContentClick);
            this.grdCALL.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCALL_CellValueChanged);
            this.grdCALL.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdCALL_CurrentCellDirtyStateChanged);
            // 
            // grdEMAIL
            // 
            this.grdEMAIL.AllowUserToAddRows = false;
            this.grdEMAIL.AllowUserToResizeColumns = false;
            this.grdEMAIL.AllowUserToResizeRows = false;
            this.grdEMAIL.BackgroundColor = System.Drawing.Color.White;
            this.grdEMAIL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdEMAIL.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdEMAIL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEMAIL.ColumnHeadersVisible = false;
            this.grdEMAIL.Location = new System.Drawing.Point(440, 473);
            this.grdEMAIL.Name = "grdEMAIL";
            this.grdEMAIL.RowHeadersVisible = false;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.grdEMAIL.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.grdEMAIL.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9F);
            this.grdEMAIL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdEMAIL.Size = new System.Drawing.Size(100, 199);
            this.grdEMAIL.TabIndex = 12;
            this.grdEMAIL.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdEMAIL_CellContentClick);
            this.grdEMAIL.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdEMAIL_CellValueChanged);
            this.grdEMAIL.CurrentCellDirtyStateChanged += new System.EventHandler(this.grdEMAIL_CurrentCellDirtyStateChanged);
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(25, 473);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(122, 42);
            this.label18.TabIndex = 102;
            this.label18.Text = "When :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grdapplyAll
            // 
            this.grdapplyAll.AllowUserToAddRows = false;
            this.grdapplyAll.AllowUserToResizeColumns = false;
            this.grdapplyAll.AllowUserToResizeRows = false;
            this.grdapplyAll.BackgroundColor = System.Drawing.Color.White;
            this.grdapplyAll.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdapplyAll.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdapplyAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdapplyAll.ColumnHeadersVisible = false;
            this.grdapplyAll.Location = new System.Drawing.Point(164, 473);
            this.grdapplyAll.Name = "grdapplyAll";
            this.grdapplyAll.RowHeadersVisible = false;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            this.grdapplyAll.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.grdapplyAll.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdapplyAll.ShowCellToolTips = false;
            this.grdapplyAll.Size = new System.Drawing.Size(30, 199);
            this.grdapplyAll.TabIndex = 103;
            this.grdapplyAll.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdapplyAll_CellContentClick);
            this.grdapplyAll.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdapplyAll_CellContentDoubleClick);
            // 
            // txtEventBody
            // 
            this.txtEventBody.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEventBody.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEventBody.InnerText = null;
            this.txtEventBody.Location = new System.Drawing.Point(151, 235);
            this.txtEventBody.Name = "txtEventBody";
            this.txtEventBody.Size = new System.Drawing.Size(388, 148);
            this.txtEventBody.TabIndex = 5;
            this.txtEventBody.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEventBody_KeyPress);
            // 
            // lnkAddContact
            // 
            this.lnkAddContact.AutoSize = true;
            this.lnkAddContact.Location = new System.Drawing.Point(861, 73);
            this.lnkAddContact.Name = "lnkAddContact";
            this.lnkAddContact.Size = new System.Drawing.Size(66, 13);
            this.lnkAddContact.TabIndex = 106;
            this.lnkAddContact.TabStop = true;
            this.lnkAddContact.Text = "Add Contact";
            this.lnkAddContact.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkAddContact_LinkClicked);
            // 
            // lblHead
            // 
            this.lblHead.BackColor = System.Drawing.Color.SteelBlue;
            this.lblHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHead.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblHead.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead.ForeColor = System.Drawing.Color.White;
            this.lblHead.Location = new System.Drawing.Point(0, 0);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(951, 40);
            this.lblHead.TabIndex = 107;
            this.lblHead.Text = "Add New Reminder";
            this.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // drpAddressBookList
            // 
            this.drpAddressBookList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpAddressBookList.FormattingEnabled = true;
            this.drpAddressBookList.Location = new System.Drawing.Point(807, 91);
            this.drpAddressBookList.Name = "drpAddressBookList";
            this.drpAddressBookList.Size = new System.Drawing.Size(123, 21);
            this.drpAddressBookList.TabIndex = 14;
            this.drpAddressBookList.SelectedIndexChanged += new System.EventHandler(this.drpAddressBookList_SelectedIndexChanged);
            // 
            // drpAMPM
            // 
            this.drpAMPM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpAMPM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpAMPM.DropDownHeight = 95;
            this.drpAMPM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpAMPM.FormattingEnabled = true;
            this.drpAMPM.IntegralHeight = false;
            this.drpAMPM.ItemHeight = 14;
            this.drpAMPM.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.drpAMPM.Location = new System.Drawing.Point(474, 130);
            this.drpAMPM.Name = "drpAMPM";
            this.drpAMPM.Size = new System.Drawing.Size(64, 22);
            this.drpAMPM.TabIndex = 108;
            this.drpAMPM.Leave += new System.EventHandler(this.drpAMPM_Leave);
            // 
            // lblAMPM
            // 
            this.lblAMPM.AutoSize = true;
            this.lblAMPM.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblAMPM.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAMPM.Location = new System.Drawing.Point(477, 117);
            this.lblAMPM.Name = "lblAMPM";
            this.lblAMPM.Size = new System.Drawing.Size(36, 12);
            this.lblAMPM.TabIndex = 110;
            this.lblAMPM.Text = "AM/PM";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(374, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 12);
            this.label1.TabIndex = 111;
            this.label1.Text = "MM";
            // 
            // lnkImortAppointment
            // 
            this.lnkImortAppointment.AutoSize = true;
            this.lnkImortAppointment.Location = new System.Drawing.Point(829, 52);
            this.lnkImortAppointment.Name = "lnkImortAppointment";
            this.lnkImortAppointment.Size = new System.Drawing.Size(98, 13);
            this.lnkImortAppointment.TabIndex = 112;
            this.lnkImortAppointment.TabStop = true;
            this.lnkImortAppointment.Text = "Import Appointment";
            this.lnkImortAppointment.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkImortAppointment_LinkClicked);
            // 
            // chkprivate
            // 
            this.chkprivate.AutoSize = true;
            this.chkprivate.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.chkprivate.ForeColor = System.Drawing.Color.SteelBlue;
            this.chkprivate.Location = new System.Drawing.Point(306, 396);
            this.chkprivate.Name = "chkprivate";
            this.chkprivate.Size = new System.Drawing.Size(189, 19);
            this.chkprivate.TabIndex = 113;
            this.chkprivate.Text = "Make this reminder private.";
            this.chkprivate.UseVisualStyleBackColor = true;
            // 
            // chkIsConfirmable
            // 
            this.chkIsConfirmable.AutoSize = true;
            this.chkIsConfirmable.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.chkIsConfirmable.ForeColor = System.Drawing.Color.SteelBlue;
            this.chkIsConfirmable.Location = new System.Drawing.Point(306, 421);
            this.chkIsConfirmable.Name = "chkIsConfirmable";
            this.chkIsConfirmable.Size = new System.Drawing.Size(234, 19);
            this.chkIsConfirmable.TabIndex = 114;
            this.chkIsConfirmable.Text = "Disable Cancel/Reschedule Options.";
            this.chkIsConfirmable.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(11, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 16);
            this.label9.TabIndex = 116;
            this.label9.Text = "Appointment Type :";
            // 
            // txtappointmentType
            // 
            this.txtappointmentType.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtappointmentType.Location = new System.Drawing.Point(149, 201);
            this.txtappointmentType.Name = "txtappointmentType";
            this.txtappointmentType.Size = new System.Drawing.Size(215, 23);
            this.txtappointmentType.TabIndex = 5;
            // 
            // drpTwilioLang
            // 
            this.drpTwilioLang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drpTwilioLang.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drpTwilioLang.FormattingEnabled = true;
            this.drpTwilioLang.Location = new System.Drawing.Point(370, 201);
            this.drpTwilioLang.Name = "drpTwilioLang";
            this.drpTwilioLang.Size = new System.Drawing.Size(170, 23);
            this.drpTwilioLang.TabIndex = 117;
            // 
            // btnCancel
            // 
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnCancel.Image = global::SynergyNotificationSystem.Properties.Resources._1345385963_Log_Out;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(857, 629);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(73, 33);
            this.btnCancel.TabIndex = 20;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // Submiteventbtn
            // 
            this.Submiteventbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Submiteventbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Submiteventbtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Submiteventbtn.Image = global::SynergyNotificationSystem.Properties.Resources._1345385847_tick_circle;
            this.Submiteventbtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Submiteventbtn.Location = new System.Drawing.Point(740, 629);
            this.Submiteventbtn.Name = "Submiteventbtn";
            this.Submiteventbtn.Size = new System.Drawing.Size(111, 33);
            this.Submiteventbtn.TabIndex = 19;
            this.Submiteventbtn.Text = "Submit Event";
            this.Submiteventbtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Submiteventbtn.UseVisualStyleBackColor = true;
            this.Submiteventbtn.Click += new System.EventHandler(this.Submiteventbtn_Click);
            // 
            // btnaddparties
            // 
            this.btnaddparties.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnaddparties.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnaddparties.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnaddparties.Image = global::SynergyNotificationSystem.Properties.Resources._1345401628_add1_;
            this.btnaddparties.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnaddparties.Location = new System.Drawing.Point(802, 351);
            this.btnaddparties.Name = "btnaddparties";
            this.btnaddparties.Size = new System.Drawing.Size(128, 28);
            this.btnaddparties.TabIndex = 16;
            this.btnaddparties.Text = "   Add Selected";
            this.btnaddparties.UseVisualStyleBackColor = true;
            this.btnaddparties.Click += new System.EventHandler(this.AddParticipantbtn_Click);
            // 
            // AddAppointment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(951, 678);
            this.Controls.Add(this.drpTwilioLang);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtappointmentType);
            this.Controls.Add(this.chkIsConfirmable);
            this.Controls.Add(this.chkprivate);
            this.Controls.Add(this.lnkImortAppointment);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblAMPM);
            this.Controls.Add(this.drpAMPM);
            this.Controls.Add(this.drpAddressBookList);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblHead);
            this.Controls.Add(this.lnkAddContact);
            this.Controls.Add(this.txtEventBody);
            this.Controls.Add(this.grdapplyAll);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.grdEMAIL);
            this.Controls.Add(this.grdCALL);
            this.Controls.Add(this.grdSMS);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.drptemplete);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.drpToMinute);
            this.Controls.Add(this.enddatepicker);
            this.Controls.Add(this.drpToHour);
            this.Controls.Add(this.locationtxt);
            this.Controls.Add(this.chkEMAIL);
            this.Controls.Add(this.chkCALL);
            this.Controls.Add(this.chkSMS);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.drpMinute);
            this.Controls.Add(this.lblCaseNo);
            this.Controls.Add(this.lblpartiesLName);
            this.Controls.Add(this.lblpartiesFName);
            this.Controls.Add(this.lblselectionNo);
            this.Controls.Add(this.grdParties);
            this.Controls.Add(this.fromdatepicker);
            this.Controls.Add(this.drphours);
            this.Controls.Add(this.Submiteventbtn);
            this.Controls.Add(this.grdaddedparties);
            this.Controls.Add(this.btnaddparties);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.bodytxt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TimeZone);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Titletxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label46);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddAppointment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Reminder";
            this.Load += new System.EventHandler(this.AddAppointment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdaddedparties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdParties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdSMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCALL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdEMAIL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdapplyAll)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox Titletxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox TimeZone;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox bodytxt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnaddparties;
        private System.Windows.Forms.DataGridView grdaddedparties;
        private System.Windows.Forms.Button Submiteventbtn;
        private System.Windows.Forms.ComboBox drphours;
        private System.Windows.Forms.DateTimePicker fromdatepicker;
        private System.Windows.Forms.DataGridView grdParties;
        private System.Windows.Forms.Label lblselectionNo;
        private System.Windows.Forms.Label lblpartiesFName;
        private System.Windows.Forms.Label lblpartiesLName;
        private System.Windows.Forms.Label lblCaseNo;
        private System.Windows.Forms.ComboBox drpMinute;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chkSMS;
        private System.Windows.Forms.CheckBox chkCALL;
        private System.Windows.Forms.CheckBox chkEMAIL;
        private System.Windows.Forms.ComboBox locationtxt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox drpToMinute;
        private System.Windows.Forms.DateTimePicker enddatepicker;
        private System.Windows.Forms.ComboBox drpToHour;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox drptemplete;
        private System.Windows.Forms.DataGridView grdSMS;
        private System.Windows.Forms.DataGridView grdCALL;
        private System.Windows.Forms.DataGridView grdEMAIL;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridView grdapplyAll;
        public System.Windows.Forms.TextBox txtSearch;
        private MSDN.Html.Editor.HtmlEditorControl txtEventBody;
        private System.Windows.Forms.LinkLabel lnkAddContact;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblHead;
        private System.Windows.Forms.ComboBox drpAddressBookList;
        private System.Windows.Forms.ComboBox drpAMPM;
        private System.Windows.Forms.Label lblAMPM;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel lnkImortAppointment;
        private System.Windows.Forms.CheckBox chkprivate;
        private System.Windows.Forms.CheckBox chkIsConfirmable;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtappointmentType;
        private System.Windows.Forms.ComboBox drpTwilioLang;
    }
}