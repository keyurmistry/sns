﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ReminderSystem;
using ReminderSystem.Classes;
using CalendarDemo;
using DataLayer;
using System.Configuration;
using System.Data.SqlClient;

namespace AppToto
{
    public partial class ConfirmEvent : Form
    {
        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());
        public string TT = string.Empty;
        public ConfirmEvent()
        {
            InitializeComponent();
        }

        public void AddHour()
        {
            int HourBind = 0;
            string TimeTT = string.Empty;

            TimeTT = GethourBind();

            if (TimeTT != "")
            {
                HourBind = Convert.ToInt32(TimeTT);

                if (HourBind == 23)
                {
                    lblAMPM.Visible = false;
                    drpAMPM.SelectedValue = "";
                    drpAMPM.Visible = false;
                }
                else
                {
                    lblAMPM.Visible = true;
                    drpAMPM.Visible = true;
                }
            }
            else
            {

            }

            for (int i = 0; i <= HourBind; i++)
            {
                string Hour = string.Empty;
                if (i < 10)
                {
                    Hour = Convert.ToString("0" + i);
                }
                else
                {
                    Hour = Convert.ToString(i);
                }

                drphours.Items.Add(Hour.Trim());
                //drpToHour.Items.Add(Hour);
            }
        }

        public void AddMinute()
        {
            for (int i = 0; i <= 59; i++)
            {
                string Minute = string.Empty;
                if (i < 10)
                {
                    Minute = Convert.ToString("0" + i);
                }
                else
                {
                    Minute = Convert.ToString(i);
                }

                drpMinute.Items.Add(Minute.Trim());
                //drpToMinute.Items.Add(Minute);
            }
        }

        public void GetColorCode()
        {
            DataSet dsStatus = new DataSet();

            string s = "exec Sp_GetColorcode_Reminder 'Reminder'";

            dsStatus = dal.GetData(s, Application.StartupPath);

            DataRow dr = dsStatus.Tables[0].NewRow();
            dr["Status"] = "--Select--";
            dr["StatusID"] = 0;
            dsStatus.Tables[0].Rows.InsertAt(dr, 0);

            cmbStatus.DataSource = dsStatus.Tables[0];
            cmbStatus.DisplayMember = "Status";
            cmbStatus.ValueMember = "StatusID";
        }

        public string GethourBind()
        {
            DataSet dsTimeZone = new DataSet();
            dsTimeZone.Clear();
            string s = "exec Get_TimeZone 'APP'";
            dsTimeZone = dal.GetData(s, Application.StartupPath);
            if (dsTimeZone != null)
            {
                TT = dsTimeZone.Tables[0].Rows[0]["TimeHours"].ToString();
            }
            return TT;
        }

        private void ConfirmEvent_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try { AddHour(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try { AddMinute(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try { GetColorCode(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {
                DateTime FromDate = System.DateTime.Now;
                DateTime ToDate = System.DateTime.Now;

                int Findhour = FromDate.Hour;

                int FindMinute = FromDate.Minute;

                fromdatepicker.Text = FromDate.ToString();
                try
                {

                    if (FromDate.Hour > 12)
                    {
                        drphours.SelectedIndex = FromDate.Hour - 12;
                        drpAMPM.SelectedIndex = 1;
                    }
                    else
                    {
                        drphours.SelectedIndex = FromDate.Hour;
                        drpAMPM.SelectedIndex = 0;
                    }
                }
                catch
                { }
                try
                {
                    drpMinute.SelectedIndex = FindMinute;
                }
                catch
                { }


            }
            catch
            {
            }
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void drphours_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drphours.Items.Contains(drphours.Text))
                {
                    drphours.SelectedIndex = 0;
                    drphours.SelectedItem = null;
                    drphours.SelectedText = "";
                }
            }
            catch
            {

            }
        }

        private void drpMinute_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drpMinute.Items.Contains(drpMinute.Text))
                {
                    drpMinute.SelectedIndex = 0;
                    drpMinute.SelectedItem = null;
                    drpMinute.SelectedText = "";
                }
            }
            catch
            {

            }
        }

        private void drpAMPM_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drpAMPM.Items.Contains(drpAMPM.Text))
                {
                    drpAMPM.SelectedIndex = 0;
                    drpAMPM.SelectedItem = null;
                    drpAMPM.SelectedText = "";
                }
            }
            catch
            {

            }
        }

        public static string SQLFix(string str)
        {
            if (str == "NA")
                str = "";
            return str.Replace("&nbsp;", "").Replace("'", "''");
        }

        private void Submiteventbtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (drphours.SelectedIndex < 0)
            {
                MessageBox.Show("Please Select Hours.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (drpMinute.SelectedIndex < 0)
            {
                MessageBox.Show("Please Select Minutes.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string TimeTT = string.Empty;
            TimeTT = GethourBind();

            if (TimeTT != "23")
            {
                if (drpAMPM.SelectedIndex < 0)
                {
                    MessageBox.Show("Please Select AM/PM Format.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (cmbStatus.SelectedIndex == 0)
            {
                MessageBox.Show("Please Select Status", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataSet dsUpdateStatus = new DataSet();
            SqlParameter[] p = new SqlParameter[3];

            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = Helper.UniversalDateFormat;

            string TimeZoneTT, strModifiedDatetime = string.Empty;
            TimeZoneTT = GethourBind();

            if (TimeZoneTT == "12")
                strModifiedDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString() + " " + drpAMPM.SelectedItem.ToString();

            else
                strModifiedDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString();

            p[0] = new SqlParameter("@EventNo", Main.App_SetValueForEventNo);
            p[1] = new SqlParameter("@ModifiedDatetime", strModifiedDatetime);
            p[2] = new SqlParameter("@Status", cmbStatus.SelectedValue);

            string s = "Exec sp_update_confirm_status '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "'";
            dsUpdateStatus = dal.GetData(s, Application.StartupPath);

            if (dsUpdateStatus.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                MessageBox.Show("Event Status Updated Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();

                try
                {
                    Main master = (Main)Application.OpenForms["Main"];
                    master.btnRefresh.PerformClick();
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }
        }

        private void btnConfirmCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
