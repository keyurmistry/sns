﻿using AppToto;
using DataLayer;
using Microsoft.VisualBasic.Devices;
using Microsoft.Win32;
using Newtonsoft.Json;
using ReminderSystem.Business.Base;
using ReminderSystem.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReminderSystem
{
    public partial class Registration : Form
    {
        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        public Registration()
        {
            InitializeComponent();
        }


        public string GetSerialNumber()
        {
            Guid serialGuid = Guid.NewGuid();
            string uniqueSerial = serialGuid.ToString("N");

            string uniqueSerialLength = uniqueSerial.Substring(0, 20).ToUpper();

            char[] serialArray = uniqueSerialLength.ToCharArray();
            string finalSerialNumber = "";

            int j = 0;
            for (int i = 0; i < 20; i++)
            {
                for (j = i; j < 4 + i; j++)
                {
                    finalSerialNumber += serialArray[j];
                }
                if (j == 20)
                {
                    break;
                }
                else
                {
                    i = (j) - 1;
                    finalSerialNumber += "-";
                }
            }

            return finalSerialNumber;
        }

        private void btnregister_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;


            if (txtCompanyName.Text == "")
            {
                MessageBox.Show("Please enter Company Name", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCompanyName.Focus();
                return;
            }
            if (txtMobileNo.Text == "")
            {
                MessageBox.Show("Please enter Mobile Number", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtMobileNo.Focus();
                return;
            }
            if (txtEmail.Text == "")
            {
                MessageBox.Show("Please enter Email", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmail.Focus();
                return;
            }
            try
            {
                InsertRegistration();
            }
            catch
            {

            }

            Application.Restart();

            Cursor.Current = Cursors.Default;
        }

        public void CloudInsertRegistrationDetails(string strCompanyName, string strMobileNo, string strEmail, string strProductKey, string strMacID)
        {
            #region Push Data to Cloud
            try
            {
                rsWebRequest objRequest = new rsWebRequest();
                wsRegistrationResponse objResponse = new wsRegistrationResponse();
                wsRegistration businessObject = new wsRegistration()
                {
                    RSClientID = "RS2016",
                    CompanyName = strCompanyName,
                    Mobile = strMobileNo,
                    Email = strEmail,
                    ProductKey = new DalBase().GetClientKey(),
                    MacID = strMacID,
                    ISActivated = "0",
                    LogoImage = "",
                    CreatedDtTm = System.DateTime.Now,
                    ModifiedDtTm = System.DateTime.Now

                };

                objRequest.postData = JsonConvert.SerializeObject(businessObject);
                objRequest.webURIServiceName = "Registration/wsSyncRegistration";
                objResponse = JsonConvert.DeserializeObject<wsRegistrationResponse>(objRequest.getWebResponse());

                if (!objResponse.Success)
                {
                    MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }

            }
            catch (Exception Ex)
            {

                throw;
            }
            #endregion Push Data to Cloud


        }

        private void btnCancelRegis_Click(object sender, EventArgs e)
        {
            Application.Exit();
            Application.ExitThread();
        }


        


        public void InsertRegistration()
        {
            try
            {

                lblMacID.Text = dal.GetClientMAC();
                lblIp.Text = GetIP();
            }
            catch
            { }

            clsGlobalDeclaration.ClientKey = lblKey.Text;
            clsGlobalDeclaration.ClientMac = lblMacID.Text;


            DataSet dsInsertRegi = new DataSet();
            SqlParameter[] p = new SqlParameter[5];

            //txtproductkey.Text = lblKey.Text;

            p[0] = new SqlParameter("@CompanyName", txtCompanyName.Text);
            p[1] = new SqlParameter("@Mobile", txtMobileNo.Text);
            p[2] = new SqlParameter("@Email", txtEmail.Text);
            p[3] = new SqlParameter("@Key", txtproductkey.Text);
            p[4] = new SqlParameter("@MACID", lblMacID.Text);


            if (chkworkstation.Checked == false)
            {

                try
                {
                    CloudInsertRegistrationDetails(txtCompanyName.Text, txtMobileNo.Text, txtEmail.Text, txtproductkey.Text, lblMacID.Text);
                }
                catch
                {

                }
            }
            else
            {

            }


            string s = "Exec Insert_RegistrationDetails '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "'";
            dsInsertRegi = dal.GetData(s, Application.StartupPath);
            if (dsInsertRegi.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                MessageBox.Show("Thanks For Registration");
            }
            else
            {
                MessageBox.Show("Error occur(s) during Registration.");
            }
        }

        public string GetIP()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;


        }

        private void Registration_Load(object sender, EventArgs e)
        {

        }

        private void chkworkstation_CheckedChanged(object sender, EventArgs e)
        {
            if (chkworkstation.Checked == false)
            {
                try
                {
                    lblKey.Text = GetSerialNumber();
                    txtproductkey.Text = lblKey.Text;
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }
            else
            {


            }
        }

        //ServerComputer serverComputer = new ServerComputer();

        //RegistryKey regKey = serverComputer.Registry.CurrentUser.CreateSubKey(@"ReminderSystem");
        //try
        //{
        //    lblKey.Text = GetSerialNumber();
        //    lblMacID.Text = GetMacAddress();

        //    regKey.SetValue("SecurityKey", lblKey.Text);
        //    regKey.SetValue("CompanyName", txtCompanyName.Text);
        //    regKey.SetValue("Email", txtEmail.Text);
        //    regKey.SetValue("MobileNumber", txtMobileNo.Text);
        //    regKey.SetValue("MAC", lblMacID.Text);

        //    serverComputer = null;
        //    regKey = null;

        //    try
        //    {
        //        CloudInsertRegistrationDetails(txtCompanyName.Text, txtMobileNo.Text, txtEmail.Text, lblKey.Text, lblMacID.Text);
        //    }
        //    catch
        //    {
        //    }
        //}
        //catch
        //{ }


    }

}