﻿namespace ReminderSystem
{
    partial class Recurring
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHead = new System.Windows.Forms.Label();
            this.lblMobileNumber = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.chkdayslist = new System.Windows.Forms.CheckedListBox();
            this.fromdatepicker = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnRecurringAdd = new System.Windows.Forms.Button();
            this.btnRecurringCancel = new System.Windows.Forms.Button();
            this.drpAMPM = new System.Windows.Forms.ComboBox();
            this.drpMinute = new System.Windows.Forms.ComboBox();
            this.drphours = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.drpToMinute = new System.Windows.Forms.ComboBox();
            this.drpToHour = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblHead
            // 
            this.lblHead.BackColor = System.Drawing.Color.SteelBlue;
            this.lblHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHead.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead.ForeColor = System.Drawing.Color.White;
            this.lblHead.Location = new System.Drawing.Point(0, 0);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(507, 39);
            this.lblHead.TabIndex = 1;
            this.lblHead.Text = "Recurring Reminder Details";
            this.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMobileNumber
            // 
            this.lblMobileNumber.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMobileNumber.ForeColor = System.Drawing.Color.SteelBlue;
            this.lblMobileNumber.Location = new System.Drawing.Point(45, 56);
            this.lblMobileNumber.Name = "lblMobileNumber";
            this.lblMobileNumber.Size = new System.Drawing.Size(47, 18);
            this.lblMobileNumber.TabIndex = 4;
            this.lblMobileNumber.Text = "Type";
            this.lblMobileNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SteelBlue;
            this.label1.Location = new System.Drawing.Point(45, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 18);
            this.label1.TabIndex = 5;
            this.label1.Text = "Days";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.SteelBlue;
            this.label2.Location = new System.Drawing.Point(45, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Start";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.SteelBlue;
            this.label3.Location = new System.Drawing.Point(45, 268);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 18);
            this.label3.TabIndex = 7;
            this.label3.Text = "End";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(98, 57);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(71, 17);
            this.radioButton1.TabIndex = 10;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Recurring";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(175, 57);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(71, 17);
            this.radioButton2.TabIndex = 11;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "One Time";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // chkdayslist
            // 
            this.chkdayslist.BackColor = System.Drawing.SystemColors.Control;
            this.chkdayslist.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chkdayslist.CheckOnClick = true;
            this.chkdayslist.FormattingEnabled = true;
            this.chkdayslist.Items.AddRange(new object[] {
            "SUN",
            "MON",
            "TUE",
            "WED",
            "THU",
            "FRI",
            "SAT"});
            this.chkdayslist.Location = new System.Drawing.Point(98, 94);
            this.chkdayslist.Name = "chkdayslist";
            this.chkdayslist.Size = new System.Drawing.Size(60, 105);
            this.chkdayslist.TabIndex = 12;
            this.chkdayslist.SelectedIndexChanged += new System.EventHandler(this.chkdayslist_SelectedIndexChanged);
            // 
            // fromdatepicker
            // 
            this.fromdatepicker.CustomFormat = "dd-MMM-yyyy";
            this.fromdatepicker.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromdatepicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fromdatepicker.Location = new System.Drawing.Point(98, 221);
            this.fromdatepicker.Name = "fromdatepicker";
            this.fromdatepicker.Size = new System.Drawing.Size(148, 22);
            this.fromdatepicker.TabIndex = 13;
            this.fromdatepicker.Value = new System.DateTime(2016, 4, 15, 19, 41, 0, 0);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd-MMM-yyyy";
            this.dateTimePicker1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(98, 265);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(150, 22);
            this.dateTimePicker1.TabIndex = 14;
            this.dateTimePicker1.Value = new System.DateTime(2016, 4, 15, 19, 41, 0, 0);
            // 
            // btnRecurringAdd
            // 
            this.btnRecurringAdd.BackColor = System.Drawing.Color.SteelBlue;
            this.btnRecurringAdd.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnRecurringAdd.ForeColor = System.Drawing.Color.White;
            this.btnRecurringAdd.Location = new System.Drawing.Point(167, 337);
            this.btnRecurringAdd.Name = "btnRecurringAdd";
            this.btnRecurringAdd.Size = new System.Drawing.Size(84, 29);
            this.btnRecurringAdd.TabIndex = 116;
            this.btnRecurringAdd.Text = "Add";
            this.btnRecurringAdd.UseVisualStyleBackColor = false;
            this.btnRecurringAdd.Click += new System.EventHandler(this.btnRecurringAdd_Click);
            // 
            // btnRecurringCancel
            // 
            this.btnRecurringCancel.BackColor = System.Drawing.Color.SteelBlue;
            this.btnRecurringCancel.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnRecurringCancel.ForeColor = System.Drawing.Color.White;
            this.btnRecurringCancel.Location = new System.Drawing.Point(254, 337);
            this.btnRecurringCancel.Name = "btnRecurringCancel";
            this.btnRecurringCancel.Size = new System.Drawing.Size(84, 29);
            this.btnRecurringCancel.TabIndex = 117;
            this.btnRecurringCancel.Text = "Cancel";
            this.btnRecurringCancel.UseVisualStyleBackColor = false;
            this.btnRecurringCancel.Click += new System.EventHandler(this.btnRecurringCancel_Click);
            // 
            // drpAMPM
            // 
            this.drpAMPM.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpAMPM.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpAMPM.DropDownHeight = 95;
            this.drpAMPM.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpAMPM.FormattingEnabled = true;
            this.drpAMPM.IntegralHeight = false;
            this.drpAMPM.ItemHeight = 14;
            this.drpAMPM.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.drpAMPM.Location = new System.Drawing.Point(357, 220);
            this.drpAMPM.Name = "drpAMPM";
            this.drpAMPM.Size = new System.Drawing.Size(41, 22);
            this.drpAMPM.TabIndex = 120;
            // 
            // drpMinute
            // 
            this.drpMinute.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpMinute.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpMinute.DropDownHeight = 95;
            this.drpMinute.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpMinute.FormattingEnabled = true;
            this.drpMinute.IntegralHeight = false;
            this.drpMinute.ItemHeight = 14;
            this.drpMinute.Location = new System.Drawing.Point(302, 220);
            this.drpMinute.Name = "drpMinute";
            this.drpMinute.Size = new System.Drawing.Size(51, 22);
            this.drpMinute.TabIndex = 119;
            // 
            // drphours
            // 
            this.drphours.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drphours.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drphours.DropDownHeight = 95;
            this.drphours.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drphours.FormattingEnabled = true;
            this.drphours.IntegralHeight = false;
            this.drphours.ItemHeight = 14;
            this.drphours.Location = new System.Drawing.Point(254, 220);
            this.drphours.Name = "drphours";
            this.drphours.Size = new System.Drawing.Size(43, 22);
            this.drphours.TabIndex = 118;
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.DropDownHeight = 95;
            this.comboBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.IntegralHeight = false;
            this.comboBox1.ItemHeight = 14;
            this.comboBox1.Items.AddRange(new object[] {
            "AM",
            "PM"});
            this.comboBox1.Location = new System.Drawing.Point(357, 264);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(41, 22);
            this.comboBox1.TabIndex = 123;
            // 
            // drpToMinute
            // 
            this.drpToMinute.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpToMinute.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpToMinute.DropDownHeight = 95;
            this.drpToMinute.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpToMinute.FormattingEnabled = true;
            this.drpToMinute.IntegralHeight = false;
            this.drpToMinute.ItemHeight = 14;
            this.drpToMinute.Location = new System.Drawing.Point(302, 264);
            this.drpToMinute.Name = "drpToMinute";
            this.drpToMinute.Size = new System.Drawing.Size(51, 22);
            this.drpToMinute.TabIndex = 122;
            // 
            // drpToHour
            // 
            this.drpToHour.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpToHour.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpToHour.DropDownHeight = 95;
            this.drpToHour.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drpToHour.FormattingEnabled = true;
            this.drpToHour.IntegralHeight = false;
            this.drpToHour.ItemHeight = 14;
            this.drpToHour.Location = new System.Drawing.Point(254, 264);
            this.drpToHour.Name = "drpToHour";
            this.drpToHour.Size = new System.Drawing.Size(43, 22);
            this.drpToHour.TabIndex = 121;
            // 
            // Recurring
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 393);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.drpToMinute);
            this.Controls.Add(this.drpToHour);
            this.Controls.Add(this.drpAMPM);
            this.Controls.Add(this.drpMinute);
            this.Controls.Add(this.drphours);
            this.Controls.Add(this.btnRecurringCancel);
            this.Controls.Add(this.btnRecurringAdd);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.fromdatepicker);
            this.Controls.Add(this.chkdayslist);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblMobileNumber);
            this.Controls.Add(this.lblHead);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Recurring";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Recurring";
            this.Load += new System.EventHandler(this.Recurring_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHead;
        private System.Windows.Forms.Label lblMobileNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.CheckedListBox chkdayslist;
        private System.Windows.Forms.DateTimePicker fromdatepicker;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btnRecurringAdd;
        private System.Windows.Forms.Button btnRecurringCancel;
        private System.Windows.Forms.ComboBox drpAMPM;
        private System.Windows.Forms.ComboBox drpMinute;
        private System.Windows.Forms.ComboBox drphours;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox drpToMinute;
        private System.Windows.Forms.ComboBox drpToHour;
    }
}