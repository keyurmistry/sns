﻿using DataLayer;
using Newtonsoft.Json;
using ReminderSystem;
using ReminderSystem.Classes;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

namespace AppToto
{
    public partial class Import : Form
    {

        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());
        string UploadedFileName, UploadedFilePath = string.Empty;

        public Import()
        {
            InitializeComponent();
        }

        private void Browsebtn_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.Default;
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = @"C:\";
                openFileDialog1.Title = "Browse Text Files";
                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;
                openFileDialog1.Multiselect = false;
                openFileDialog1.DefaultExt = "Excel sheet (*.Excel)|*.xls;*.xlsx";
                openFileDialog1.Filter = "All files (*.*)|*.*|Excel sheet (*.Excel)|*.xls;*.xlsx";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                openFileDialog1.ReadOnlyChecked = true;
                openFileDialog1.ShowReadOnly = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    txtbrowseFile.Text = openFileDialog1.FileName;
                }


                int count = 0;
                string[] FName;

                foreach (string s in openFileDialog1.FileNames)
                {
                    FName = s.Split('\\');
                    UploadedFileName = FName[FName.Length - 1].ToString();
                    UploadedFilePath = Path.GetDirectoryName(txtbrowseFile.Text);
                    File.Copy(s, UploadedFilePath + FName[FName.Length - 1]);
                    count++;
                }

            }
            catch
            { }
        }



        private void btnUpload_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (txtbrowseFile.Text != "")
            {
                try
                {
                    ExcelImportData();
                }
                catch
                { }
            }
            else
            {
                MessageBox.Show("Please Select *.xlsx File.");
            }
        }
        private void ExcelImportData()
        {
            try
            {

                ExcelImport EI = new ExcelImport();
                if (UploadedFileName != "" && UploadedFileName != string.Empty)
                {
                    DataTable dtExcel = EI.ExcelImportSQL(UploadedFileName, UploadedFilePath);
                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = Helper.UniversalDateFormat;
                    if (dtExcel != null)
                    {
                        SqlParameter[] p = new SqlParameter[2];

                        p[0] = new SqlParameter("@UploadAppointment", dtExcel);
                        p[1] = new SqlParameter("@CreatedUserFK", clsGlobalDeclaration.ClientMac);


                        DataSet dtResult = dal.GetData("RS_ImportReminder", p, Application.StartupPath);
                        if (dtResult.Tables[0].Rows[0]["Result"].ToString() == "Inserted")
                        {

                            try
                            {
                                CloudSyncReminderAdd();
                            }
                            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                            MessageBox.Show("Data Imported Successfully, Please export your error uploded data.");
                            //this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Error Occured During Import Data");

                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please Select File.");

                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message.ToString());

            }
        }

        private void lnkdownload_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {

                var fbd = new FolderBrowserDialog();
                if (fbd.ShowDialog() == DialogResult.OK)
                {

                    var localPath = Path.Combine(fbd.SelectedPath, Application.StartupPath + @"\Download\SNS_Appointment.xlsx");
                    File.Copy(Application.StartupPath + @"\Download\SNS_Appointment.xlsx", fbd.SelectedPath + "\\" + "SNS_Appointment.xlsx", true);



                    MessageBox.Show("Download Complete.");
                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }


        public void CloudSyncReminderAdd()
        {
            try
            {
                DataSet dsCloudSync = new DataSet();
                string strCloud = "Exec Get_CloudSyncDate";
                dsCloudSync = dal.GetData(strCloud, Application.StartupPath);

                if (dsCloudSync.Tables[0].Rows.Count > 0)
                {


                    var postData = string.Empty;


                    var request = (HttpWebRequest)WebRequest.Create("http://62.151.183.51:83/Reminder/wsAddReminder");
                    request.ContentType = "application/json";
                    request.Method = "POST";





                    postData = "[";
                    for (int i = 0; i <= dsCloudSync.Tables[0].Rows.Count - 1; i++)
                    {

                        postData += "{";
                        postData += "'RID':'" + dsCloudSync.Tables[0].Rows[i]["RID"] + "',";
                        postData += "'EventID':'" + dsCloudSync.Tables[0].Rows[i]["EventID"] + "',";
                        postData += "'Event':'" + dsCloudSync.Tables[0].Rows[i]["Event"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'EventTitle':'" + dsCloudSync.Tables[0].Rows[i]["EventTitle"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'Phone':'" + dsCloudSync.Tables[0].Rows[i]["Phone"] + "',";
                        postData += "'Email':'" + dsCloudSync.Tables[0].Rows[i]["Email"] + "',";
                        postData += "'FromPhoneNo':'" + dsCloudSync.Tables[0].Rows[i]["FromPhoneNo"] + "',";
                        postData += "'AccountSID':'" + dsCloudSync.Tables[0].Rows[i]["AccountSID"] + "',";
                        postData += "'AuthToken':'" + dsCloudSync.Tables[0].Rows[i]["AuthToken"] + "',";
                        postData += "'EventDatetime':'" + dsCloudSync.Tables[0].Rows[i]["EventDatetime"] + "',";
                        postData += "'SMSStatus':'" + dsCloudSync.Tables[0].Rows[i]["SMSStatus"] + "',";
                        postData += "'EmailStatus':'" + dsCloudSync.Tables[0].Rows[i]["EmailStatus"] + "',";
                        postData += "'CallStatus':'" + dsCloudSync.Tables[0].Rows[i]["CallStatus"] + "',";
                        postData += "'ClientKey':'" + dsCloudSync.Tables[0].Rows[i]["ClientKey"] + "',";
                        postData += "'RedirectLink':'" + dsCloudSync.Tables[0].Rows[i]["RedirectLink"] + "',";
                        postData += "'EmailFrom':'" + dsCloudSync.Tables[0].Rows[i]["EmailFrom"] + "',";
                        postData += "'EmailPassword':'" + dsCloudSync.Tables[0].Rows[i]["EmailPassword"] + "',";
                        postData += "'EmailFromTitle':'" + dsCloudSync.Tables[0].Rows[i]["EmailFromTitle"] + "',";
                        postData += "'EmailPort':'" + dsCloudSync.Tables[0].Rows[i]["EmailPort"] + "',";
                        postData += "'EmailSMTP':'" + dsCloudSync.Tables[0].Rows[i]["EmailSMTP"] + "',";
                        postData += "'CompanyLOGO':'" + dsCloudSync.Tables[0].Rows[i]["CompanyLOGO"] + "',";
                        postData += "'IsStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsStatus"] + "',";
                        postData += "'MainEventID':'" + dsCloudSync.Tables[0].Rows[i]["MainEventID"] + "',";
                        postData += "'IsTwilioStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsTwilioStatus"] + "',";
                        postData += "'AppointmentDatetime':'" + dsCloudSync.Tables[0].Rows[i]["AppointmentDatetime"] + "',";
                        postData += "'IsConfirmable':'" + dsCloudSync.Tables[0].Rows[i]["IsConfirmable"] + "'";

                        postData += "},";

                    }
                    postData = postData.Remove(postData.Length - 1);
                    postData += "]";

                    var data = Encoding.ASCII.GetBytes(postData);



                    request.ContentLength = data.Length;


                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    wsReminderResponse WsCloud = JsonConvert.DeserializeObject<wsReminderResponse>(responseString);

                    if (WsCloud.Success == true)
                    {

              

                        DataTable dtBulkConfirm = new DataTable();
                        dtBulkConfirm.Columns.AddRange(new DataColumn[2] { new DataColumn("MainEventID", typeof(string)),
                        new DataColumn("ClientKey", typeof(string))});

                        for (int i = 0; i <= WsCloud.ResponseDetail.Count - 1; i++)
                        {
                            dtBulkConfirm.Rows.Add(WsCloud.ResponseDetail[i].EventID, WsCloud.ResponseDetail[i].ClientKey);
                        }


                        if (dtBulkConfirm != null)
                        {
                            DataSet dsCloudSyncupdate = new DataSet();
                            SqlParameter[] p = new SqlParameter[1];
                            p[0] = new SqlParameter("@SyncCloudStatus", dtBulkConfirm);
                            dsCloudSyncupdate = dal.GetData("[Cloud_UpdateSync]", p, Application.StartupPath);
                        }


                    }
                }

            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }

        private void btnExporttoexcelError_Click(object sender, EventArgs e)
        {


            DataSet dsgetErrorRecord = new DataSet();
            string s = "EXEC GetErrorDetails";
            dsgetErrorRecord = dal.GetData(s, Application.StartupPath);
            if (dsgetErrorRecord != null)
            {

                StreamWriter wr = new StreamWriter(Application.StartupPath + "\\Upload\\BulkAppointmentReminder.xls");
                // Write Columns to excel file
                for (int i = 0; i < dsgetErrorRecord.Tables[0].Columns.Count; i++)
                {
                    wr.Write(dsgetErrorRecord.Tables[0].Columns[i].ToString().ToUpper() + "\t");
                }
                wr.WriteLine();
                //write rows to excel file
                for (int i = 0; i < (dsgetErrorRecord.Tables[0].Rows.Count); i++)
                {
                    for (int j = 0; j < dsgetErrorRecord.Tables[0].Columns.Count; j++)
                    {
                        if (dsgetErrorRecord.Tables[0].Rows[i][j] != null)
                        {
                            wr.Write(Convert.ToString(dsgetErrorRecord.Tables[0].Rows[i][j]) + "\t");
                        }
                        else
                        {
                            wr.Write("\t");
                        }
                    }
                    wr.WriteLine();
                }
                wr.Close();

                try
                {

                    var fbd = new FolderBrowserDialog();
                    if (fbd.ShowDialog() == DialogResult.OK)
                    {

                        var localPath = Path.Combine(fbd.SelectedPath, Application.StartupPath + "\\Upload\\BulkAppointmentReminder.xls");
                        File.Copy(Application.StartupPath + @"\Upload\BulkAppointmentReminder.xls", fbd.SelectedPath + "\\" + "BulkAppointmentReminder.xls", true);



                        MessageBox.Show("Download Complete.");
                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

              //MessageBox.Show("Data Exported Successfully");


            }
            else
            {
                //grdErrorDetails.DataSource = null;
                
            }
        }

       
    
    }
}
