﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ReminderSystem.Classes
{
    class rsWebRequest
    {
        public string webURIBase { get; set; }
        public string postData { get; set; }
        public string contentType { get; set; }
        public string requestMethod { get; set; }
        public string webResponse { get; set; }
        public string webURIServiceName { get; set; }

        public rsWebRequest()
        {
            postData = "application/json";
            requestMethod = "POST";



            webURIBase = ConfigReader.GetCloudServiceHost(); 

            
        }

        public string getWebResponse()
        {

            var request = (HttpWebRequest)WebRequest.Create(webURIBase + webURIServiceName);
            request.ContentType = "application/json";
            request.Method = "POST";
            var data = Encoding.ASCII.GetBytes(postData);
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }
    }

    public static class clsGlobalDeclaration
    {
        private static string _clientKey;
        private static string _clientMacId;
        private static string _windowsusername;

        public static string ClientKey
        {
            get { return _clientKey; }
            set { _clientKey = value; }
        }
        public static string ClientMac
        {
            get { return _clientMacId; }
            set { _clientMacId = value; }
        }
        public static string WindowsUserName
        {
            get { return _windowsusername; }
            set { _windowsusername = value; }
        }

    
    }
}
