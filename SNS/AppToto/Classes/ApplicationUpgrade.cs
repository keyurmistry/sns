﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ReminderSystem.Classes
{

    public class ApplicationUpgrade
    {
        public string ApplicationVersion { get; set; }
        public string ApplicationPath { get; set; }
        public string webURIBase { get; set; }
        public string postData { get; set; }
        public string contentType { get; set; }
        public string requestMethod { get; set; }
        public string webResponse { get; set; }
        public string webURIServiceName { get; set; }

        public ApplicationUpgrade()
        {
            postData = "application/json";
            requestMethod = "POST";

            webURIBase = "http://www.snsapp.in/Upgrade/rs_UpgradeApplication.svc/";

            
        }

        public string getWebResponse()
        {

            var request = (HttpWebRequest)WebRequest.Create(webURIBase + webURIServiceName);
            request.ContentType = "application/json";
            request.Method = "POST";
            var data = Encoding.ASCII.GetBytes(postData);
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseString;
        }
    }


}
