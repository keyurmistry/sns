﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReminderSystem
{
    public static class Helper
    {
        public static readonly string UniversalDateFormat = "dd-MMM-yyyy";
        public static readonly log4net.ILog AppLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    }
}
