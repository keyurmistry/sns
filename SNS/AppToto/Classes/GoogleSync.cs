﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReminderSystem.Classes
{

    public class GoogleSync
    {

        public string Salutation { get; set; }
        public string FName { get; set; }
        public string MName { get; set; }
        public string LName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Zipcode { get; set; }
        public string Notes { get; set; }
        public DateTime DOB { get; set; }
        public string CountryCode { get; set; }
        public string MobileType { get; set; }
        public string EmailType { get; set; }
        public string ContactType { get; set; }
        public string ClientKey { get; set; }
        public string Result { get; set; }

    }
}

