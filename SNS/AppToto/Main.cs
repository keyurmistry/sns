﻿using CalendarDemo;
using DataLayer;
using Newtonsoft.Json;
using ReminderSystem;
using ReminderSystem.Business.Base;
using ReminderSystem.Classes;
using RS_UpgradeApplication;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace AppToto
{
    public partial class Main : Form
    {
        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());


        public Main()
        {
            InitializeComponent();
        }

        public void Showhide()
        {
            strtdateTimePicker.Visible = false;
            tolbl.Visible = false;
            enddateTimePicker.Visible = false;

            lbldate.Visible = false;
            Datecombo.Visible = false;
        }

        private void LoadLanguages()
        {
            DataSet dsLanguages = new DataSet();
            string sqlQuery = "EXEC GetTwilioLanguage";

            dsLanguages = dal.GetData(sqlQuery, Application.StartupPath);

            cbLanguage.ValueMember = "LID";
            cbLanguage.DisplayMember = "Language";
            cbLanguage.DataSource = dsLanguages.Tables[0];

            var results = from myRow in dsLanguages.Tables[0].AsEnumerable()
                          where myRow.Field<bool>("ISDEFAULTLANG") == true
                          select myRow;

            if (results.Count() > 0)
                cbLanguage.SelectedValue = results.First().Field<int>("LID");
            else
                cbLanguage.SelectedIndex = -1;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            notifyIcon1.Visible = false;
            this.tooltipPenddingreminder.SetToolTip(btnPendingEvent, "Pending Reminder");

            try
            {
                dal.GetWindowsUser();
            }
            catch
            {

            }

            try
            {
                lblApplicationVersion.Text = "RS.S.v" + GeyApplicationVersion();
            }
            catch
            {

            }
            try
            {
                string UserMac = string.Empty;
                UserMac = dal.GetClientMAC();

                GetSystemUserName(UserMac);
            }
            catch
            { }

            grdAgendadata.AutoGenerateColumns = true;

            try
            {
                Showhide();

                lbldate.Visible = true;
                Datecombo.Visible = true;

                Datecombo.SelectedIndex = 0;
                //GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", "");

            }
            catch
            {
            }

            try
            {
                GetPanelColorCode();
                LoadLanguages();
            }
            catch (Exception ex) {
            
            }

            try
            {
                GetAPITimerOnOff();
            }
            catch
            { }

            try
            {
                TwowayTimerOnOff();
            }
            catch
            { }


            Cursor.Current = Cursors.Default;
        }

        private void monthCalendar1_DateChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void MonthWeekDay()
        {
            Showhide();

            lbldate.Visible = false;
            Datecombo.Visible = false;
        }

        private void Monthbtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            DemoForm CalanderView1 = new DemoForm();
            CalanderView1.Show();

            Cursor.Current = Cursors.Default;

        }
        private bool CheckOpened(string name)
        {
            FormCollection fc = Application.OpenForms;
            foreach (Form frm in fc)
            {
                if (frm.Text == name)
                {
                    return true;
                }
            }
            return false;
        }

        private void Weekbtn_Click(object sender, EventArgs e)
        {
            MonthWeekDay();
        }

        private void Daybtn_Click(object sender, EventArgs e)
        {

            MonthWeekDay();
        }

        private void Addcal_Click(object sender, EventArgs e)
        {
            ImportAddressBook addcalendar = new ImportAddressBook();
            addcalendar.ShowDialog();
        }

        private void import_Click(object sender, EventArgs e)
        {
            Import Csvimport = new Import();
            Csvimport.ShowDialog();
        }

        private void AddAppointment_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ClearParties = new DataSet();
                ClearParties = dal.GetData("Sp_ClearAddparties '" + clsGlobalDeclaration.ClientMac + "'", Application.StartupPath);
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            try
            {
                Clean();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            try
            {
                CleanCalender();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            App_SetValueForBtnsubmit = "Submit Event";
            AddAppointment App = new AddAppointment();
            App.ShowDialog();

            try
            {
                DataSet ClearParties = new DataSet();
                ClearParties = dal.GetData("Sp_ClearAddparties '" + clsGlobalDeclaration.ClientMac + "'", Application.StartupPath);
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void addcontbtn_Click(object sender, EventArgs e)
        {

            try
            {
                ClearContact();
            }
            catch
            { }

            AddContact addcontact = new AddContact();
            addcontact.ShowDialog();

        }

        private void Cntimportbtn_Click(object sender, EventArgs e)
        {
            ImportAddressBook adcal = new ImportAddressBook();
            adcal.ShowDialog();
        }

        DataSet dsgetA1Details = new DataSet();

        public void GetA1LawBasicDetails(string Flag, string Fromdt, string todate, string Filter)
        {
            int isprivate = 0;
            if (chkprivateFilter.Checked == true)
            {
                isprivate = 1;
            }
            else
            {
                isprivate = 0;
            }

            string s = "exec Sp_GetA1LawBasicDetails_Filter '" + Flag + "','" + Fromdt + "','" + todate + "','" + Filter + "','" + clsGlobalDeclaration.ClientMac + "','" + isprivate + "'";

            dsgetA1Details = dal.GetData(s, Application.StartupPath);

            //lblEmptyGridText.Visible = false;

            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();

            if (dsgetA1Details != null && dsgetA1Details.Tables != null && dsgetA1Details.Tables[0].Rows.Count > 0)
            {

                grdAgendadata.Columns.Clear();

                grdAgendadata.DataSource = dsgetA1Details.Tables[0];

                grdAgendadata.Columns[18].HeaderText = "Case No";

                grdAgendadata.Columns[0].Visible = false;
                grdAgendadata.Columns[1].Visible = false;
                grdAgendadata.Columns[2].Visible = false;
                grdAgendadata.Columns[5].Visible = false;
                grdAgendadata.Columns[8].Visible = false;
                grdAgendadata.Columns[9].Visible = false;
                grdAgendadata.Columns[10].Visible = false;

                grdAgendadata.Columns[11].Visible = false;
                grdAgendadata.Columns[12].Visible = false;
                grdAgendadata.Columns[13].Visible = false;
                grdAgendadata.Columns[14].Visible = false;
                grdAgendadata.Columns[15].Visible = false;
                grdAgendadata.Columns[16].Visible = false;
                grdAgendadata.Columns[17].Visible = false;
                grdAgendadata.Columns[19].Visible = false;
                grdAgendadata.Columns[20].Visible = false;
                grdAgendadata.Columns[21].Visible = false;
                grdAgendadata.Columns[22].Visible = false;
                grdAgendadata.Columns[23].Visible = false;

                grdAgendadata.Columns[24].Visible = false;
                grdAgendadata.Columns[25].Visible = false;
                grdAgendadata.Columns[26].Visible = false;
                grdAgendadata.Columns[27].Visible = false;
                grdAgendadata.Columns[28].Visible = false;
                grdAgendadata.Columns[29].Visible = false;
                grdAgendadata.Columns[30].Visible = false;
                grdAgendadata.Columns[31].Visible = false;
                grdAgendadata.Columns[32].Visible = false;
                grdAgendadata.Columns[35].Visible = false;
                grdAgendadata.Columns[37].Visible = false;

                grdAgendadata.Columns[0].Width = 50;
                grdAgendadata.Columns[3].Width = 90;
                grdAgendadata.Columns[4].Width = 80;
                grdAgendadata.Columns[6].Width = 100;
                grdAgendadata.Columns[7].Width = 100;

                grdAgendadata.Columns[17].Width = 150;
                grdAgendadata.Columns[33].Width = 120;
                grdAgendadata.Columns[34].Width = 130;

                if (dsgetA1Details.Tables[0].Rows.Count >= 14)
                {
                    grdAgendadata.Columns[36].Width = 155;
                }
                else
                {
                    grdAgendadata.Columns[36].Width = 170;
                }

                grdAgendadata.Columns[34].DisplayIndex = 6;

                grdAgendadata.Columns[0].ReadOnly = true;
                grdAgendadata.Columns[3].ReadOnly = true;
                grdAgendadata.Columns[4].ReadOnly = true;
                grdAgendadata.Columns[6].ReadOnly = true;
                grdAgendadata.Columns[7].ReadOnly = true;
                grdAgendadata.Columns[12].ReadOnly = true;
                grdAgendadata.Columns[17].ReadOnly = true;
                grdAgendadata.Columns[18].ReadOnly = true;
                grdAgendadata.Columns[33].ReadOnly = true;
                grdAgendadata.Columns[34].ReadOnly = true;
                grdAgendadata.Columns[36].ReadOnly = true;

                checkBoxColumn.HeaderText = "Remove";
                checkBoxColumn.Width = 70;
                checkBoxColumn.Name = "Remove";
                checkBoxColumn.ValueType = typeof(bool);
                grdAgendadata.Columns.Insert(37, checkBoxColumn);


            }
            else
            {
                grdAgendadata.DataSource = null;
                grdAgendadata.Columns.Clear();

            }
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                Cloud_TwilioStatus();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {
                RefreshGrid();
            }
            catch
            { }
            try { GetPanelColorCode(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            Cursor.Current = Cursors.Default;
        }

        public void RefreshGrid()
        {
            try
            {
                try
                {
                    if (Datecombo.SelectedItem.ToString() == "Upcoming")
                    {
                        strtdateTimePicker.Visible = false;
                        tolbl.Visible = false;
                        enddateTimePicker.Visible = false;
                        try
                        {
                            GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", txtgridSearch.Text);
                        }
                        catch
                        {
                        }
                    }
                    else if (Datecombo.SelectedItem.ToString() == "Today")
                    {
                        strtdateTimePicker.Visible = false;
                        tolbl.Visible = false;
                        enddateTimePicker.Visible = false;
                        try
                        {
                            GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", txtgridSearch.Text);
                        }
                        catch
                        {
                        }
                    }
                    else if (Datecombo.SelectedItem.ToString() == "Tomorrow")
                    {
                        strtdateTimePicker.Visible = false;
                        tolbl.Visible = false;
                        enddateTimePicker.Visible = false;
                        try
                        {
                            GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", txtgridSearch.Text);
                        }
                        catch
                        {
                        }
                    }
                    else if (Datecombo.SelectedItem.ToString() == "Yesterday")
                    {
                        strtdateTimePicker.Visible = false;
                        tolbl.Visible = false;
                        enddateTimePicker.Visible = false;

                        try
                        {
                            GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", txtgridSearch.Text);
                        }
                        catch
                        {
                        }
                    }
                    else if (Datecombo.SelectedItem.ToString() == "Past Week")
                    {
                        strtdateTimePicker.Visible = false;
                        tolbl.Visible = false;
                        enddateTimePicker.Visible = false;

                        try
                        {
                            GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", txtgridSearch.Text);
                        }
                        catch
                        {
                        }
                    }

                    else if (Datecombo.SelectedItem.ToString() == "Specific Date")
                    {


                        strtdateTimePicker.Visible = true;
                        tolbl.Visible = false;
                        enddateTimePicker.Visible = false;

                        try
                        {
                            GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), strtdateTimePicker.Text, "", txtgridSearch.Text);
                        }
                        catch
                        {
                        }
                    }
                    else if (Datecombo.SelectedItem.ToString() == "Custom Range")
                    {
                        strtdateTimePicker.Visible = true;
                        tolbl.Visible = true;
                        enddateTimePicker.Visible = true;

                        try
                        {
                            GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), strtdateTimePicker.Text, enddateTimePicker.Text, txtgridSearch.Text);
                        }
                        catch
                        {
                        }
                    }
                    else
                    {
                        strtdateTimePicker.Visible = false;
                        tolbl.Visible = false;
                        enddateTimePicker.Visible = false;

                        try
                        {
                            GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", txtgridSearch.Text);
                        }
                        catch
                        {
                        }
                    }

                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }
            catch
            {
            }
        }

        private void Datecombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (Datecombo.SelectedItem.ToString() == "Upcoming")
                {
                    strtdateTimePicker.Visible = false;
                    tolbl.Visible = false;
                    enddateTimePicker.Visible = false;
                    btnserchfirst.Visible = false;

                    try
                    {
                        GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", "");
                    }
                    catch
                    {
                    }
                }
                else if (Datecombo.SelectedItem.ToString() == "Today")
                {
                    strtdateTimePicker.Visible = false;
                    tolbl.Visible = false;
                    enddateTimePicker.Visible = false;
                    btnserchfirst.Visible = false;

                    try
                    {
                        GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", "");
                    }
                    catch
                    {
                    }
                }
                else if (Datecombo.SelectedItem.ToString() == "Tomorrow")
                {
                    strtdateTimePicker.Visible = false;
                    tolbl.Visible = false;
                    enddateTimePicker.Visible = false;
                    btnserchfirst.Visible = false;
                    try
                    {
                        GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", "");
                    }
                    catch
                    {
                    }
                }
                else if (Datecombo.SelectedItem.ToString() == "Yesterday")
                {
                    strtdateTimePicker.Visible = false;
                    tolbl.Visible = false;
                    enddateTimePicker.Visible = false;
                    btnserchfirst.Visible = false;

                    try
                    {
                        GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", "");
                    }
                    catch
                    {
                    }
                }
                else if (Datecombo.SelectedItem.ToString() == "Past Week")
                {
                    strtdateTimePicker.Visible = false;
                    tolbl.Visible = false;
                    enddateTimePicker.Visible = false;
                    btnserchfirst.Visible = false;

                    try
                    {
                        GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", "");
                    }
                    catch
                    {
                    }
                }

                else if (Datecombo.SelectedItem.ToString() == "Specific Date")
                {
                    strtdateTimePicker.Visible = false;
                    tolbl.Visible = false;
                    enddateTimePicker.Visible = false;
                    btnserchfirst.Visible = false;

                    try
                    {
                        System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                        dateInfo.ShortDatePattern = Helper.UniversalDateFormat;

                        DateTime FromDate = DateTime.Parse(System.DateTime.Now.ToString());

                        strtdateTimePicker.Text = FromDate.ToString();
                    }
                    catch
                    {
                    }

                    strtdateTimePicker.Visible = true;
                    btnserchfirst.Visible = true;
                    btnserchfirst.Location = new System.Drawing.Point(242, 69);

                    try
                    {
                        GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), strtdateTimePicker.Text, "", "");
                    }
                    catch
                    {
                    }
                }
                else if (Datecombo.SelectedItem.ToString() == "Custom Range")
                {
                    strtdateTimePicker.Visible = false;
                    tolbl.Visible = false;
                    enddateTimePicker.Visible = false;
                    btnserchfirst.Visible = false;
                    try
                    {
                        DateTime FromDate = DateTime.Parse(DateTime.Today.AddDays(-7).ToString());
                        DateTime ToDate = DateTime.Parse(System.DateTime.Now.ToString());

                        strtdateTimePicker.Text = FromDate.ToString();
                        enddateTimePicker.Text = ToDate.ToString();
                    }
                    catch
                    {
                    }

                    strtdateTimePicker.Visible = true;
                    tolbl.Visible = true;
                    enddateTimePicker.Visible = true;
                    btnserchfirst.Visible = true;
                    btnserchfirst.Location = new System.Drawing.Point(383, 71);

                    try
                    {
                        GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), strtdateTimePicker.Text, enddateTimePicker.Text, "");
                    }
                    catch
                    {
                    }
                }
                else
                {
                    strtdateTimePicker.Visible = false;
                    tolbl.Visible = false;
                    enddateTimePicker.Visible = false;

                    try
                    {
                        GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), "", "", "");
                    }
                    catch
                    {
                    }
                }
                grdAgendadata.Focus();

            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            Cursor.Current = Cursors.Default;
        }

        private void Notificationtabpanel_MouseEnter(object sender, EventArgs e)
        {
            Notificationtabpanel.Focus();
        }

        private void crtnewtempbtn_Click(object sender, EventArgs e)
        {
            try
            {
                ClearAppointment();
            }
            catch
            { }

            try
            {
                CreateAppointment crtapp = new CreateAppointment();
                crtapp.ShowDialog();
            }
            catch
            { }
        }

        private void Caladdbtn_Click(object sender, EventArgs e)
        {
            ImportAddressBook adcal = new ImportAddressBook();
            adcal.ShowDialog();
        }

        private void AddressAddbtn_Click(object sender, EventArgs e)
        {
            ImportAddressBook ImpAddBook = new ImportAddressBook();
            ImpAddBook.ShowDialog();
        }

        bool IsTheSameCellValue(int column, int row)
        {
            DataGridViewCell cell1 = grdAgendadata[column, row];
            DataGridViewCell cell2 = grdAgendadata[column, row - 1];

            if (cell1.Value == null || cell2.Value == null)
            {
                return false;
            }
            return cell1.Value.ToString() == cell2.Value.ToString();
        }

        private void grdAgendadata_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            try
            {
                e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
                if (e.RowIndex < 1 || e.ColumnIndex < 0)
                    return;

                DataGridViewCell cell1 = grdAgendadata[0, e.RowIndex];
                DataGridViewCell cell2 = grdAgendadata[0, e.RowIndex - 1];

                if (cell1.Value.ToString() != cell2.Value.ToString())
                {
                    e.AdvancedBorderStyle.Top = grdAgendadata.AdvancedCellBorderStyle.Bottom;
                }
                else
                {
                    if (IsTheSameCellValue(e.ColumnIndex, e.RowIndex))
                    {
                        e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
                    }
                    else
                    {
                        e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.OutsetDouble;
                    }
                }
            }
            catch
            { }
        }

        private void grdAgendadata_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow Myrow in grdAgendadata.Rows)
            {
                if (Convert.ToInt32(Myrow.Cells[22].Value) == 1)
                {
                    Myrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Myrow.Cells[25].Value), Convert.ToInt32(Myrow.Cells[26].Value), Convert.ToInt32(Myrow.Cells[27].Value));
                }
                else if (Convert.ToInt32(Myrow.Cells[22].Value) == 2)
                {
                    Myrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Myrow.Cells[25].Value), Convert.ToInt32(Myrow.Cells[26].Value), Convert.ToInt32(Myrow.Cells[27].Value));
                }
                else if (Convert.ToInt32(Myrow.Cells[22].Value) == 3)
                {
                    Myrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Myrow.Cells[25].Value), Convert.ToInt32(Myrow.Cells[26].Value), Convert.ToInt32(Myrow.Cells[27].Value));
                }
                else if (Convert.ToInt32(Myrow.Cells[22].Value) == 4)
                {
                    Myrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Myrow.Cells[25].Value), Convert.ToInt32(Myrow.Cells[26].Value), Convert.ToInt32(Myrow.Cells[27].Value));
                }
                else if (Convert.ToInt32(Myrow.Cells[22].Value) == 5)
                {
                    Myrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Myrow.Cells[25].Value), Convert.ToInt32(Myrow.Cells[26].Value), Convert.ToInt32(Myrow.Cells[27].Value));
                }
                else if (Convert.ToInt16(Myrow.Cells[37].Value) == 0)
                {
                    if (Myrow.Cells[37].Value != null)
                    {
                        Myrow.DefaultCellStyle.BackColor = Color.FromArgb(255, 0, 0);
                    }
                }
                else
                {
                    Myrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Myrow.Cells[25].Value), Convert.ToInt32(Myrow.Cells[26].Value), Convert.ToInt32(Myrow.Cells[27].Value));
                }
            }
        }

        public string GeyApplicationVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);

            string version = fvi.FileVersion;

            return version;
        }

        private void Calendars_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                // For LOG TAB

                if (Calendars.SelectedIndex == 2)
                {
                    txtTwilioFilterFrom.Visible = false;
                    txtTwilioFilterTo.Visible = false;
                    btnDateSearchLog.Visible = false;
                    lblTwilioTO.Visible = false;
                    drpogFilter.SelectedIndex = 0;

                    try
                    {
                        GetPanelColorLogCode();
                    }
                    catch
                    { }
                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            // For Setting TAB

            try
            {
                if (Calendars.SelectedIndex == 3)
                {
                    GetTempleteList();

                    pnlPassword.Visible = false;
                    pnlSetting.Visible = false;

                    pnlPassword.Visible = true;
                    pnlPassword.Size = new Size(970, 550);
                    pnlPassword.Dock = DockStyle.Fill;
                    pnlPassword.Location = new Point(0, 0);
                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            // For ADDRESS BOOK

            try
            {
                if (Calendars.SelectedIndex == 4)
                {
                    txtAddBookType.Text = "";
                    btnAddNewAddBook.Text = "+ Add";
                    lblAddBookTypeId.Text = "";
                    GetAddressBook();
                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            Cursor.Current = Cursors.Default;
        }

        public void GetPanelColorCode()
        {
            DataSet dsColorReminder = new DataSet();
            string s = "exec Sp_GetColorcode_Reminder 'Reminder'";

            dsColorReminder = dal.GetData(s, Application.StartupPath);

            if (dsColorReminder != null && dsColorReminder.Tables != null && dsColorReminder.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= dsColorReminder.Tables[0].Rows.Count - 1; i++)

                    if (dsColorReminder.Tables[0].Rows[i]["StatusID"].ToString() == "1")
                    {
                        pnlconfirm.BackColor = Color.FromArgb(Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["R"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["G"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["B"]));
                    }
                    else if (dsColorReminder.Tables[0].Rows[i]["StatusID"].ToString() == "2")
                    {
                        pnlcancel.BackColor = Color.FromArgb(Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["R"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["G"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["B"]));
                    }
                    else if (dsColorReminder.Tables[0].Rows[i]["StatusID"].ToString() == "3")
                    {
                        pnlreschedule.BackColor = Color.FromArgb(Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["R"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["G"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["B"]));
                    }
                    else if (dsColorReminder.Tables[0].Rows[i]["StatusID"].ToString() == "4")
                    {
                        pnlcallback.BackColor = Color.FromArgb(Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["R"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["G"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["B"]));
                    }
                    else if (dsColorReminder.Tables[0].Rows[i]["StatusID"].ToString() == "5")
                    {
                        pnlnorepsonce.BackColor = Color.FromArgb(Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["R"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["G"]), Convert.ToInt32(dsColorReminder.Tables[0].Rows[i]["B"]));
                    }
            }
        }

        public void GetPanelColorLogCode()
        {
            DataSet dsColLog = new DataSet();
            string s = "exec Sp_GetColorcode_Reminder 'Log'";

            dsColLog = dal.GetData(s, Application.StartupPath);

            if (dsColLog != null && dsColLog.Tables != null && dsColLog.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i <= dsColLog.Tables[0].Rows.Count - 1; i++)

                    if (dsColLog.Tables[0].Rows[i]["Status"].ToString() == "Completed")
                    {
                        pnlLogComplete.BackColor = Color.FromArgb(Convert.ToInt32(dsColLog.Tables[0].Rows[i]["R"]), Convert.ToInt32(dsColLog.Tables[0].Rows[i]["G"]), Convert.ToInt32(dsColLog.Tables[0].Rows[i]["B"]));
                    }
                    else if (dsColLog.Tables[0].Rows[i]["Status"].ToString() == "Delivered")
                    {
                        pnlLogDeliverd.BackColor = Color.FromArgb(Convert.ToInt32(dsColLog.Tables[0].Rows[i]["R"]), Convert.ToInt32(dsColLog.Tables[0].Rows[i]["G"]), Convert.ToInt32(dsColLog.Tables[0].Rows[i]["B"]));
                    }
                    else if (dsColLog.Tables[0].Rows[i]["Status"].ToString() == "Undelivered")
                    {
                        pnlLogUndeliverd.BackColor = Color.FromArgb(Convert.ToInt32(dsColLog.Tables[0].Rows[i]["R"]), Convert.ToInt32(dsColLog.Tables[0].Rows[i]["G"]), Convert.ToInt32(dsColLog.Tables[0].Rows[i]["B"]));
                    }
                    else if (dsColLog.Tables[0].Rows[i]["Status"].ToString() == "No-answer")
                    {
                        pnlLogNoans.BackColor = Color.FromArgb(Convert.ToInt32(dsColLog.Tables[0].Rows[i]["R"]), Convert.ToInt32(dsColLog.Tables[0].Rows[i]["G"]), Convert.ToInt32(dsColLog.Tables[0].Rows[i]["B"]));
                    }
                    else
                    {

                    }
            }
        }

        public void GetReminderDetails()
        {
            DataSet dsReminder = new DataSet();
            string s = "exec SP_InsertAddCall 'Get','','','','','','','',''";

            dsReminder = dal.GetData(s, Application.StartupPath);

            if (dsReminder != null && dsReminder.Tables != null && dsReminder.Tables[0].Rows.Count > 0)
            {
                DataGridViewImageColumn dgvremove = new DataGridViewImageColumn();
                dgvremove.Image = SynergyNotificationSystem.Properties.Resources.Remove;
                dgvremove.HeaderText = "Remove";

                DataGridViewImageColumn dgvEdit = new DataGridViewImageColumn();
                dgvEdit.Image = SynergyNotificationSystem.Properties.Resources.Edit;
                dgvEdit.HeaderText = "Edit";
            }
            else
            {
                MessageBox.Show("No records were(s) found.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static string SetValueForID, SetValueForName, SetValueForPurpose, SetValueForWhen, SetValueForStatus, SetValueForBody, SetValueForCategory, SetValueForSubject, SetButton = string.Empty;

        public void RemoveReminder(string selectedID)
        {
            DataSet dsgetA1Details = new DataSet();
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete ?", "Synergy Notification System", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string s = "exec SP_InsertAddCall 'Remove','','','','','','','" + selectedID + "',''";
                dsgetA1Details = dal.GetData(s, Application.StartupPath);

                if (dsgetA1Details.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    GetReminderDetails();
                }
                else { }
            }
            else
            { }
        }

        public void Clear()
        {
            SetValueForID = string.Empty;
            SetValueForName = string.Empty;
            SetValueForPurpose = string.Empty;
            SetValueForWhen = string.Empty;
            SetValueForStatus = string.Empty;
            SetValueForBody = string.Empty;
            SetValueForSubject = string.Empty;
            SetValueForCategory = string.Empty;
            SetButton = "Save";
        }

        private void btnreloadhidden_Click(object sender, EventArgs e)
        {
            try
            {
                GetReminderDetails();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        public void GetContact()
        {
            DataSet dsgetContact = new DataSet();

            string s = "exec SP_GetEvent_Contact '" + searchtxt.Text + "'";
            dsgetContact = dal.GetData(s, Application.StartupPath);
            if (dsgetContact != null && dsgetContact.Tables != null && dsgetContact.Tables[0].Rows.Count > 0)
            {
                Contactgridview.Columns.Clear();
                Contactgridview.DataSource = dsgetContact.Tables[0];
                Contactgridview.CurrentCell.Selected = false;

                Contactgridview.Columns[0].Width = 100;
                Contactgridview.Columns[2].Width = 100;
                Contactgridview.Columns[3].Width = 140;
                Contactgridview.Columns[4].Width = 280;
                Contactgridview.Columns[5].Width = 200;

                Contactgridview.Columns[1].Visible = false;
                Contactgridview.Columns[6].Visible = false;
                Contactgridview.Columns[7].Visible = false;

                Contactgridview.Columns[8].Visible = false;
                Contactgridview.Columns[9].Visible = false;
                Contactgridview.Columns[10].Visible = false;
                Contactgridview.Columns[11].Visible = false;
                Contactgridview.Columns[12].Visible = false;
                Contactgridview.Columns[13].Visible = false;
                Contactgridview.Columns[14].Visible = false;
                Contactgridview.Columns[15].Visible = false;
                Contactgridview.Columns[16].Visible = false;
                Contactgridview.Columns[17].Visible = false;

                DataGridViewImageColumn dgvEdit = new DataGridViewImageColumn();
                dgvEdit.Image = SynergyNotificationSystem.Properties.Resources.Edit;
                dgvEdit.Width = 60;
                dgvEdit.HeaderText = "Edit";
                Contactgridview.Columns.Insert(0, dgvEdit);

                DataGridViewImageColumn dgvRemove = new DataGridViewImageColumn();
                dgvRemove.Image = SynergyNotificationSystem.Properties.Resources.Remove;
                dgvRemove.Width = 60;
                dgvRemove.HeaderText = "Remove";
                Contactgridview.Columns.Insert(18, dgvRemove);

                Contactgridview.Columns[18].DisplayIndex = 1;
            }
            else
            {
                Contactgridview.Columns.Clear();

                Contactgridview.DataSource = null;
            }
        }

        public void GetAddressBook()
        {
            DataSet dsgetAddressBook = new DataSet();
            string SearchContact = string.Empty;

            SearchContact = searchtxt.Text;

            string s = "exec SP_LRS_AddressBook;3";
            dsgetAddressBook = dal.GetData(s, Application.StartupPath);
            if (dsgetAddressBook != null && dsgetAddressBook.Tables != null && dsgetAddressBook.Tables[0].Rows.Count > 0)
            {
                dgvAddressBookList.Columns.Clear();
                dgvAddressBookList.DataSource = dsgetAddressBook.Tables[0];

                dgvAddressBookList.CurrentCell.Selected = false;

                dgvAddressBookList.Columns[1].HeaderText = "Address Book Type";

                dgvAddressBookList.Columns[0].Visible = false;
                dgvAddressBookList.Columns[2].Visible = false;
                dgvAddressBookList.Columns[3].Visible = false;
                dgvAddressBookList.Columns[4].Visible = false;

                DataGridViewImageColumn dgvEdit = new DataGridViewImageColumn();
                dgvEdit.Image = SynergyNotificationSystem.Properties.Resources.Remove;
                dgvEdit.Width = 25;
                dgvEdit.HeaderText = "";
                dgvAddressBookList.Columns.Insert(5, dgvEdit);

                dgvAddressBookList.Columns[1].ReadOnly = true;
                dgvAddressBookList.Columns[1].Width = 280;
                dgvAddressBookList.Columns[5].Width = 53;
            }
            else
            {
                dgvAddressBookList.Columns.Clear();
                dgvAddressBookList.DataSource = null;
            }
        }

        public void GetAddressBookDetails(string addId)
        {
            try
            {
                DataSet dsgetAddbook = new DataSet();
                string SearchContact = string.Empty;

                SearchContact = txtSearchAddBook.Text;

                string s = "exec SP_GetEvent_AddressBookContact '" + SearchContact + "'," + addId;
                dsgetAddbook = dal.GetData(s, Application.StartupPath);
                if (dsgetAddbook != null && dsgetAddbook.Tables != null && dsgetAddbook.Tables[0].Rows.Count > 0)
                {
                    Application.DoEvents();
                    dgvAddressBook.Columns.Clear();
                    dgvAddressBook.DataSource = dsgetAddbook.Tables[0];
                    Application.DoEvents();
                    dgvAddressBook.CurrentCell.Selected = false;

                    dgvAddressBook.Columns[0].Visible = false;
                    dgvAddressBook.Columns[2].Visible = false;
                    dgvAddressBook.Columns[1].Visible = false;
                    dgvAddressBook.Columns[6].Visible = false;
                    dgvAddressBook.Columns[7].Visible = false;

                    dgvAddressBook.Columns[8].Visible = false;
                    dgvAddressBook.Columns[9].Visible = false;
                    dgvAddressBook.Columns[10].Visible = false;
                    dgvAddressBook.Columns[11].Visible = false;
                    dgvAddressBook.Columns[12].Visible = false;
                    dgvAddressBook.Columns[13].Visible = false;
                    dgvAddressBook.Columns[14].Visible = false;
                    dgvAddressBook.Columns[15].Visible = false;
                    dgvAddressBook.Columns[16].Visible = false;

                    for (int i = 0; i <= dgvAddressBook.Columns.Count - 1; i++)
                    {
                        Application.DoEvents();
                    }

                    DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                    checkBoxColumn.HeaderText = "";
                    checkBoxColumn.Width = 30;
                    checkBoxColumn.Name = "";
                    checkBoxColumn.ValueType = typeof(bool);
                    dgvAddressBook.Columns.Insert(0, checkBoxColumn);

                    dgvAddressBook.Columns[0].Width = 50;
                    dgvAddressBook.Columns[4].Width = 145;
                    dgvAddressBook.Columns[5].Width = 270;
                    dgvAddressBook.Columns[6].Width = 144;

                    dgvAddressBook.Columns[0].ReadOnly = false;
                    dgvAddressBook.Columns[4].ReadOnly = true;
                    dgvAddressBook.Columns[5].ReadOnly = true;
                    dgvAddressBook.Columns[6].ReadOnly = true;

                    if (dsgetAddbook.Tables[0].Rows.Count >= 16)
                    {
                        dgvAddressBook.Columns[5].Width = 252;
                    }
                    else
                    {
                        dgvAddressBook.Columns[5].Width = 270;
                    }

                    chkAllAddBook.Location = new System.Drawing.Point(375, 64);

                    if (SearchContact.Trim().Length > 0 || addId != "0")
                    {
                        bool isCheck = false;
                        if (addId != "0")
                            isCheck = (addId != "0");

                        for (int j = 0; j <= dgvAddressBook.Rows.Count - 1; j++)
                        {
                            (dgvAddressBook.Rows[j].Cells[0] as DataGridViewCheckBoxCell).Value = isCheck;// (addId != "0");
                        }
                    }
                }
                else
                {
                    dgvAddressBook.Columns.Clear();
                    dgvAddressBook.DataSource = null;
                    chkAllAddBook.Visible = false;
                }
                chkAllAddBook.Visible = dgvAddressBook.Rows.Count > 0;
            }
            catch
            {

            }
        }

        public void Reload()
        {
            Contactgridview.Update();
        }

        private void Rclickbtn_Click(object sender, EventArgs e)
        {

        }

        public static string App_SetValueForBtnsubmit, App_SetValueForFname, App_SetValueForEventNo,
                             App_SetValueForLname, App_SetValueForCaseNo, App_SetValueForFirmCode, App_SetValueForTitle, App_SetValueForTimestart, App_SetValueForTimestartHour, App_SetValueForTimestartMinute, App_SetValueForTimeEnd, App_SetValueForTimeEndHour, App_SetValueForTimeEndMinute, App_SetValueForTimeZone, App_SetValueForLocation, App_SetValueForEventBody,
                             App_SetValueForEventHTMLBody,
                             App_SMS, APP_Call, APP_Email, App_PartiesFirmcode, App_PartiesCode, App_WhenSMS, App_WhenCALL, App_WhenEMAIL,
                             App_MainWhere, App_AMPM, App_Private, App_IsConfirmable, App_TwilioLanguage, App_AppointMentTempType = string.Empty;

        public void GetTempleteList()
        {
            DataSet dsgetAppointmentTemplate = new DataSet();
            string s = "exec Sp_insertTemplete 'Get'";
            dsgetAppointmentTemplate = dal.GetData(s, Application.StartupPath);
            if (dsgetAppointmentTemplate != null && dsgetAppointmentTemplate.Tables != null && dsgetAppointmentTemplate.Tables[0].Rows.Count > 0)
            {
                grdTemplateList.Columns.Clear();

                DataGridViewImageColumn dgvremove = new DataGridViewImageColumn();
                dgvremove.Image = SynergyNotificationSystem.Properties.Resources.Remove;
                dgvremove.HeaderText = "";
                grdTemplateList.Columns.Add(dgvremove);

                grdTemplateList.DataSource = dsgetAppointmentTemplate.Tables[0];


                grdTemplateList.Columns[0].Width = 50;
                grdTemplateList.Columns[1].Width = 40;
                grdTemplateList.Columns[2].Width = 200;
                grdTemplateList.Columns[3].Width = 105;

                grdTemplateList.Columns[7].Width = 130;
                grdTemplateList.Columns[8].Width = 130;

                grdTemplateList.Columns[4].Visible = false;
                grdTemplateList.Columns[5].Visible = false;

                grdTemplateList.Columns[9].Visible = false;
                grdTemplateList.Columns[10].Visible = false;
                grdTemplateList.Columns[11].Visible = false;
                grdTemplateList.Columns[12].Visible = false;
                grdTemplateList.Columns[13].Visible = false;
                grdTemplateList.Columns[14].Visible = false;
                grdTemplateList.Columns[15].Visible = false;
                grdTemplateList.Columns[16].Visible = false;

                if (dsgetAppointmentTemplate.Tables[0].Rows.Count >= 14)
                {
                    grdTemplateList.Columns[6].Width = 400;
                }
                else
                {
                    grdTemplateList.Columns[6].Width = 300;
                }
            }
            else
            {
                grdTemplateList.Columns.Clear();
                grdTemplateList.DataSource = null;

            }
        }

        public static string str_BodyAppoint, str_TitleAppoint, str_SubmitAppoint, str_ID, str_AppointmentType, str_TwilioSay = string.Empty;

        public void ClearAppointment()
        {
            str_TitleAppoint = "";
            str_BodyAppoint = "";
            str_ID = "";
            str_AppointmentType = "";
            str_TwilioSay = "";
            str_SubmitAppoint = "Save";
        }

        public void RemoveAddedTempate(string SelectFirmCode)
        {
            DataSet dsgetA1Details = new DataSet();
            string s = "exec Sp_insertTemplete 'Remove','','','','" + SelectFirmCode + "'";
            dsgetA1Details = dal.GetData(s, Application.StartupPath);
            if (dsgetA1Details.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                GetTempleteList();
            }
            else
            {

            }
        }

        private void grdTemplateList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                if (MessageBox.Show("Are you sure , you want to delete this Template ?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    RemoveAddedTempate(grdTemplateList.Rows[e.RowIndex].Cells[5].Value.ToString());
                }
            }
        }

        private void btnappointmentRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                GetTempleteList();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }


        public void Clean()
        {
            App_SetValueForLocation = "";
            App_SetValueForTitle = "";
            App_SetValueForEventBody = "";
            App_SetValueForEventHTMLBody = "";
            App_SetValueForLocation = "";
            App_SetValueForTimestart = "";
            App_SetValueForTimestartHour = "";
            App_SetValueForTimestartMinute = "";
            App_SetValueForTimeEnd = "";
            App_SetValueForTimeEndHour = "";
            App_SetValueForTimeEndMinute = "";
            App_SetValueForFname = "";
            App_SetValueForLname = "";
            App_WhenSMS = "";
            App_SetValueForFirmCode = "";
            App_SetValueForEventNo = "";
            App_SetValueForBtnsubmit = "Update Event";
            App_SMS = "";
            APP_Call = "";
            APP_Email = "";
            App_MainWhere = "";
            App_AMPM = "";
            App_Private = "";
            App_IsConfirmable = "";
            App_TwilioLanguage = "";
        }

        public void CleanCalender()
        {
            DemoForm.App_SetValueForLocation = "";
            DemoForm.App_SetValueForTitle = "";
            DemoForm.App_SetValueForEventBody = "";
            DemoForm.App_SetValueForEventHTMLBody = "";
            DemoForm.App_SetValueForLocation = "";
            DemoForm.App_SetValueForTimestart = "";
            DemoForm.App_SetValueForTimestartHour = "";
            DemoForm.App_SetValueForTimestartMinute = "";
            DemoForm.App_SetValueForTimeEnd = "";
            DemoForm.App_SetValueForTimeEndHour = "";
            DemoForm.App_SetValueForTimeEndMinute = "";
            DemoForm.App_SetValueForFname = "";
            DemoForm.App_SetValueForLname = "";
            DemoForm.App_WhenSMS = "";
            DemoForm.App_SetValueForFirmCode = "";
            DemoForm.App_SetValueForEventNo = "";
            DemoForm.App_SetValueForBtnsubmit = "Update Event";
            DemoForm.App_SMS = "";
            DemoForm.APP_Call = "";
            DemoForm.APP_Email = "";
        }

        private void grdAgendadata_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
            //try
            //{
            //    Clean();
            //}
            //catch
            //{
            //}

            //if (e.ColumnIndex != 28)
            //{
            //    try
            //    {

            //        string TitleMain = grdAgendadata.Rows[e.RowIndex].Cells[5].Value.ToString();
            //        string[] values = TitleMain.Split(':');

            //        App_SetValueForTitle = values[0].ToString().Trim();
            //        App_SetValueForEventBody = values[1].ToString().Trim();


            //        TitleMain = grdAgendadata.Rows[e.RowIndex].Cells[28].Value.ToString();
            //        values = TitleMain.Split(':');

            //        App_SetValueForEventHTMLBody = values[1].ToString().Trim();

            //    }
            //    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            //    try
            //    {


            //        App_MainWhere = "MainPage";
            //        App_SetValueForLocation = grdAgendadata.Rows[e.RowIndex].Cells[8].Value.ToString();

            //        App_SetValueForTimestart = grdAgendadata.Rows[e.RowIndex].Cells[23].Value.ToString();
            //        App_SetValueForTimestartHour = grdAgendadata.Rows[e.RowIndex].Cells[14].Value.ToString();
            //        App_SetValueForTimestartMinute = grdAgendadata.Rows[e.RowIndex].Cells[15].Value.ToString();

            //        App_SetValueForTimeEnd = grdAgendadata.Rows[e.RowIndex].Cells[23].Value.ToString();
            //        App_SetValueForTimeEndHour = grdAgendadata.Rows[e.RowIndex].Cells[14].Value.ToString();
            //        App_SetValueForTimeEndMinute = grdAgendadata.Rows[e.RowIndex].Cells[15].Value.ToString();

            //        App_SetValueForFname = grdAgendadata.Rows[e.RowIndex].Cells[6].Value.ToString();
            //        App_SetValueForLname = grdAgendadata.Rows[e.RowIndex].Cells[7].Value.ToString();


            //        App_SetValueForFirmCode = grdAgendadata.Rows[e.RowIndex].Cells[2].Value.ToString();
            //        App_SetValueForEventNo = grdAgendadata.Rows[e.RowIndex].Cells[1].Value.ToString();

            //        App_SetValueForBtnsubmit = "Update Event";

            //        App_SMS = grdAgendadata.Rows[e.RowIndex].Cells[9].Value.ToString();
            //        APP_Call = grdAgendadata.Rows[e.RowIndex].Cells[10].Value.ToString();
            //        APP_Email = grdAgendadata.Rows[e.RowIndex].Cells[11].Value.ToString();

            //        App_PartiesCode = grdAgendadata.Rows[e.RowIndex].Cells[16].Value.ToString();

            //        App_SetValueForCaseNo = grdAgendadata.Rows[e.RowIndex].Cells[18].Value.ToString();

            //        App_WhenSMS = grdAgendadata.Rows[e.RowIndex].Cells[19].Value.ToString();
            //        App_WhenCALL = grdAgendadata.Rows[e.RowIndex].Cells[20].Value.ToString();
            //        App_WhenEMAIL = grdAgendadata.Rows[e.RowIndex].Cells[21].Value.ToString();
            //    }
            //    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            //    try
            //    {
            //        AddAppointment App = new AddAppointment();
            //        App.ShowDialog();
            //    }
            //    catch
            //    { }
            //}

            //Cursor.Current = Cursors.Default;
        }

        public void RemoveEvent(string Eventno)
        {
            DataSet dsRemoveEvent = new DataSet();
            string s = "exec RemoveEvent '" + Eventno + "'";
            dsRemoveEvent = dal.GetData(s, Application.StartupPath);
            if (dsRemoveEvent.Tables[0].Rows[0]["Result"].ToString() == "1")
            {

            }
            else
            {

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {

                List<DataGridViewRow> selectedRows = (from row in grdAgendadata.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[37].Value) == true
                                                      select row).ToList();
                if (selectedRows.Count <= 0)
                {
                    MessageBox.Show("Please select atleast one record.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    if (MessageBox.Show("Are you sure , you want to delete this Event ?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (DataGridViewRow row in selectedRows)
                        {
                            RemoveEvent(row.Cells[1].Value.ToString());

                            string ProductKey = string.Empty;
                            ProductKey = dal.GetClientKey();

                            CloudSyncReminderDelete(ProductKey, row.Cells[1].Value.ToString());
                        }

                    }
                    else
                    {

                    }
                }
                try
                {
                    RefreshGrid();
                }
                catch
                { }
            }
            catch
            {

            }
            Cursor.Current = Cursors.Default;
        }

        private void txtsearchValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtsearchValue.Text == "Search Email, FirstName ,LastName, Email or Case No #")
            {
                txtsearchValue.Text = "";
                txtsearchValue.ForeColor = SystemColors.WindowText;
            }
        }

        public static string Con_SetValueForSuffex, Con_SetValueForFName, Con_SetValueForMName, Con_SetValueForLName, Con_SetValueForCountryCode, Con_SetValueForPhoneNo,
               Con_SetValueForEmail, Con_SetValueForDOB, Con_SetValueForZipCode, Con_SetValueForNote, Con_SetValueForButton, Con_SetValueForMobileType,
               Con_SetValueForEmailType, Con_SetValueForCardCode, Con_SetValueForFrimCode = string.Empty;

        public void ClearContact()
        {
            Con_SetValueForCardCode = "";
            Con_SetValueForFrimCode = "";
            Con_SetValueForSuffex = "";
            Con_SetValueForFName = "";
            Con_SetValueForMName = "";
            Con_SetValueForLName = "";
            Con_SetValueForEmail = "";
            Con_SetValueForPhoneNo = "";
            Con_SetValueForDOB = "";
            Con_SetValueForZipCode = "";
            Con_SetValueForNote = "";
            Con_SetValueForCountryCode = "";
            Con_SetValueForMobileType = "";
            Con_SetValueForEmailType = "";
            Con_SetValueForButton = "Create Contact";
        }

        private void button1_Click_2(object sender, EventArgs e)
        {

        }

        private void dgvAddressBook_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnAddNewAddBook_Click(object sender, EventArgs e)
        {
            if (txtAddBookType.Text.Trim().Length > 0)
            {
                string strQry = string.Empty;
                if (lblAddBookTypeId.Text.Length == 0)
                {
                    strQry = "exec SP_LRS_AddressBook 0,'" + txtAddBookType.Text.Trim() + "','" + txtAddBookType.Text.Trim() + "',1";
                }
                else if (lblAddBookTypeId.Text.Length > 0)
                {
                    strQry = "exec SP_LRS_AddressBook;2 " + lblAddBookTypeId.Text + ",'" + txtAddBookType.Text.Trim() + "','" + txtAddBookType.Text.Trim() + "',1";
                }
                if (strQry.Length > 0)
                {
                    dal.GetData(strQry, Application.StartupPath);
                    txtAddBookType.Text = string.Empty;
                    lblAddBookTypeId.Text = string.Empty;
                    GetAddressBook();
                    btnAddNewAddBook.Text = "+ Add";
                    txtAddBookType.Focus();
                }
            }
        }


        private void Contactgridview_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                try
                {
                    ClearContact();

                }
                catch
                { }
                try
                {

                    Con_SetValueForCardCode = Contactgridview.Rows[e.RowIndex].Cells[17].Value.ToString();
                    Con_SetValueForFrimCode = Contactgridview.Rows[e.RowIndex].Cells[2].Value.ToString();

                    Con_SetValueForSuffex = Contactgridview.Rows[e.RowIndex].Cells[9].Value.ToString();
                    Con_SetValueForFName = Contactgridview.Rows[e.RowIndex].Cells[7].Value.ToString();
                    Con_SetValueForMName = Contactgridview.Rows[e.RowIndex].Cells[10].Value.ToString();
                    Con_SetValueForLName = Contactgridview.Rows[e.RowIndex].Cells[8].Value.ToString();

                    Con_SetValueForEmail = Contactgridview.Rows[e.RowIndex].Cells[5].Value.ToString();


                    Con_SetValueForPhoneNo = Contactgridview.Rows[e.RowIndex].Cells[6].Value.ToString();

                    Con_SetValueForDOB = Contactgridview.Rows[e.RowIndex].Cells[11].Value.ToString();
                    Con_SetValueForZipCode = Contactgridview.Rows[e.RowIndex].Cells[13].Value.ToString();

                    Con_SetValueForNote = Contactgridview.Rows[e.RowIndex].Cells[12].Value.ToString();
                    Con_SetValueForCountryCode = Contactgridview.Rows[e.RowIndex].Cells[14].Value.ToString();
                    Con_SetValueForMobileType = Contactgridview.Rows[e.RowIndex].Cells[15].Value.ToString();
                    Con_SetValueForEmailType = Contactgridview.Rows[e.RowIndex].Cells[16].Value.ToString();
                    Con_SetValueForButton = "Update Contact";
                }
                catch
                { }

                try
                {
                    AddContact Contact = new AddContact();
                    Contact.ShowDialog();
                }
                catch
                { }
            }

            if (e.ColumnIndex == 18)
            {
                try
                {
                    if (MessageBox.Show("Are you sure, you want to delete this Contact ?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        RemoveContact(Contactgridview.Rows[e.RowIndex].Cells[17].Value.ToString());


                        try
                        {
                            GetContact();
                        }
                        catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                    }
                    else
                    { }
                }
                catch
                { }
            }
        }

        public void RemoveContact(string Codecard)
        {
            DataSet dsRemoveContact = new DataSet();

            string s = "EXEC Sproc_Remove_RS_Contact '" + Codecard + "'";
            dsRemoveContact = dal.GetData(s, Application.StartupPath);
            if (dsRemoveContact != null && dsRemoveContact.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                MessageBox.Show("Contact Remove Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Error occur(s) during remove Contact.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtSearchAddBook_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                string FinalEmptyAddbook = string.Empty;

                FinalEmptyAddbook = txtSearchAddBook.Text.Trim();

                if (FinalEmptyAddbook == "" || FinalEmptyAddbook == string.Empty)
                {

                    MessageBox.Show("Please Enter Keyword First.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        Application.DoEvents();
                        dgvAddressBook.Enabled = false;


                        GetAddressBookDetails("0");
                        dgvAddressBook.Enabled = true;

                        chkAllAddBook.Visible = dgvAddressBook.Rows.Count > 0;
                        Cursor.Current = Cursors.Default;
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                }
            }
        }

        private void btnSaveAddBook_Click(object sender, EventArgs e)
        {
            string addBookTypeId = string.Empty;

            addBookTypeId = lblAddBookTypeId.Text;

            if (addBookTypeId.Length == 0)
            {
                MessageBox.Show("Please select atleast one address book type", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }
            List<DataGridViewRow> selectedAddbookcontact = (from row in dgvAddressBook.Rows.Cast<DataGridViewRow>()
                                                            where Convert.ToBoolean(row.Cells[0].Value) == true
                                                            select row).ToList();
            if (selectedAddbookcontact.Count <= 0)
            {
                MessageBox.Show("Please Checked atleast one address book Contact", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                foreach (DataGridViewRow item in dgvAddressBook.Rows)
                {
                    if (Convert.ToBoolean(item.Cells[0].Value) == true)
                    {
                        string a = item.Cells[17].Value.ToString();
                        string strQry = "exec SP_LRS_AddressBookDetails 0," + addBookTypeId + ",'" + a + "'";
                        dal.GetData(strQry, Application.StartupPath);
                    }
                }

                MessageBox.Show("Record saved successfully", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnAddNewAddBook.Text = "+&Add";
                GetAddressBook();
            }
        }

        private void chkAllAddBook_CheckedChanged(object sender, EventArgs e)
        {

            for (int j = 0; j <= dgvAddressBook.Rows.Count - 1; j++)
            {
                (dgvAddressBook.Rows[j].Cells[0] as DataGridViewCheckBoxCell).Value = chkAllAddBook.Checked;

                dgvAddressBook.Rows[j].Selected = chkAllAddBook.Checked;
            }

        }

        private void btnLoadAllAddressBook_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();
                dgvAddressBook.Enabled = false;

                GetAddressBookDetails("0");
                dgvAddressBook.Enabled = true;

                chkAllAddBook.Visible = dgvAddressBook.Rows.Count > 0;
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void dgvAddressBookList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor.Current = Cursors.WaitCursor;
                string addId = dgvAddressBookList.Rows[e.RowIndex].Cells[1].Value.ToString();
                try
                {
                    if (Convert.ToInt16(addId) == 0)
                    {
                        addId = dgvAddressBookList.Rows[e.RowIndex].Cells[0].Value.ToString();
                    }
                }
                catch
                {
                    addId = dgvAddressBookList.Rows[e.RowIndex].Cells[0].Value.ToString();
                }

                txtAddBookType.Text = dgvAddressBookList.Rows[e.RowIndex].Cells[2].Value.ToString();
                lblAddBookTypeId.Text = addId;
                btnAddNewAddBook.Text = "Update";
                txtAddBookType.Focus();
                txtSearchAddBook.Text = string.Empty;
                GetAddressBookDetails(addId);

                chkAllAddBook.Visible = dgvAddressBook.Rows.Count > 0;
                Cursor.Current = Cursors.Default;
            }
        }

        private void dgvAddressBookList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                DialogResult dr = MessageBox.Show("Are your sure to delete this record?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                if (dr == DialogResult.Yes)
                {
                    string id = dgvAddressBookList.Rows[e.RowIndex].Cells[0].Value.ToString();
                    string strQry = "exec SP_LRS_AddressBookDetails;4 " + id;
                    dal.GetData(strQry, Application.StartupPath);

                    MessageBox.Show("Records successfully deleted", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GetAddressBook();
                }
            }
            if (e.ColumnIndex == 2 || e.ColumnIndex == 1)
            {
                string addId = dgvAddressBookList.Rows[e.RowIndex].Cells[0].Value.ToString();
                //       txtAddBookType.Text = dgvAddressBookList.Rows[e.RowIndex].Cells[2].Value.ToString();
                lblAddBookTypeId.Text = addId;
            }
        }

        private void btnDeleteAddBook_Click(object sender, EventArgs e)
        {
            if (lblAddBookTypeId.Text.Length == 0)
            {
                MessageBox.Show("Please select atleast one address book type", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            List<DataGridViewRow> selectedAddbookcontact = (from row in dgvAddressBook.Rows.Cast<DataGridViewRow>()
                                                            where Convert.ToBoolean(row.Cells[0].Value) == true
                                                            select row).ToList();

            if (selectedAddbookcontact.Count <= 0)
            {
                MessageBox.Show("Please Checked atleast one address book Contact", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }
            else
            {
                foreach (DataGridViewRow item in dgvAddressBook.Rows)
                {
                    if (Convert.ToBoolean(item.Cells[0].Value) == true)
                    {
                        string a = item.Cells[17].Value.ToString();
                        string strQry = "exec SP_LRS_AddressBookDetails;5 " + lblAddBookTypeId.Text + ",'" + a + "'";
                        dal.GetData(strQry, Application.StartupPath);
                    }
                }
                MessageBox.Show("Record deleted successfully", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                GetAddressBookDetails(lblAddBookTypeId.Text);
            }
        }

        private void btnrefreshContact_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            string FinalEmptyCheck = string.Empty;

            FinalEmptyCheck = searchtxt.Text.Trim();

            if (FinalEmptyCheck == "" || FinalEmptyCheck == string.Empty)
            {

            }
            else
            {

                try
                {
                    GetContact();
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }

            Cursor.Current = Cursors.Default;
        }

        string AccountSIDLog, AuthoTokenLog = string.Empty;

        private void btnRefeshTwilioLog_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                SynchTwilioLog();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {
                TwilioRefreshGrid();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            try
            {
                GetPanelColorLogCode();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            Cursor.Current = Cursors.Default;

        }

        private void SynchTwilioLog()
        {
            #region Push Data to Cloud
            try
            {
                rsWebRequest objRequest = new rsWebRequest();

                wsTwilioLogResponse objResponse = new wsTwilioLogResponse();

                objRequest.postData = "{'ClientKey':'" + clsGlobalDeclaration.ClientKey + "'}";
                objRequest.webURIServiceName = "TwilioLog/wsGetTwilioLog";
                objResponse = JsonConvert.DeserializeObject<wsTwilioLogResponse>(objRequest.getWebResponse());

                System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                dateInfo.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

                if (!objResponse.Success)
                {
                    MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
                else
                {


                    DataTable dtTwiliolog = new DataTable();
                    dtTwiliolog.Columns.Add("FromNumber");
                    dtTwiliolog.Columns.Add("ToNumber");
                    dtTwiliolog.Columns.Add("Direction");
                    dtTwiliolog.Columns.Add("Duration");
                    dtTwiliolog.Columns.Add("StartDate");
                    dtTwiliolog.Columns.Add("EndDate");
                    dtTwiliolog.Columns.Add("Status");
                    dtTwiliolog.Columns.Add("CreatedDttm");
                    dtTwiliolog.Columns.Add("MdifiedDttm");
                    dtTwiliolog.Columns.Add("SID");
                    dtTwiliolog.Columns.Add("LogType");


                    DataSet dsTwilioStaus = new DataSet();
                    dsTwilioStaus.Clear();
                    //StringBuilder sbSQLQuery = new StringBuilder();
                    //sbSQLQuery.AppendLine("DELETE FROM tbl_TwilioLog");
                    foreach (wsTwilioLog vStatus in objResponse.ResponseDetail)
                    {
                        dtTwiliolog.Rows.Add(vStatus.FromNumber, vStatus.ToNumber, vStatus.Direction, vStatus.Duration, Convert.ToDateTime(vStatus.StartDate, dateInfo).ToString("MM/dd/yyyy HH:mm:ss"), Convert.ToDateTime(vStatus.EndDate, dateInfo).ToString("MM/dd/yyyy HH:mm:ss"), vStatus.Status, Convert.ToDateTime(vStatus.CreatedDttm, dateInfo).ToString("MM/dd/yyyy HH:mm:ss"), Convert.ToDateTime(vStatus.MdifiedDttm, dateInfo).ToString("MM/dd/yyyy HH:mm:ss"), vStatus.SID, vStatus.LogType);
                    }
                    //dsTwilioStaus = dal.GetData(sbSQLQuery.ToString(), Application.StartupPath);

                    if (dtTwiliolog != null)
                    {
                        SqlParameter[] p = new SqlParameter[1];

                        p[0] = new SqlParameter("@UploadTwilioLog", dtTwiliolog);



                        DataSet dtResult = dal.GetData("Sproc_BulkTwilioLog", p, Application.StartupPath);
                        if (dtResult.Tables[0].Rows[0]["Result"].ToString() == "Inserted")
                        {

                            //MessageBox.Show("Log Updated Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                        }

                    }






                }

            }
            catch (Exception Ex)
            {

                //MessageBox.Show(Ex.Message.ToString(), Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                throw;
            }
            #endregion Push Data to Cloud
        }

        public static string SQLFix(string str)
        {
            if (str == "NA")
                str = "";
            return str.Replace("&nbsp;", "").Replace("'", "''");
        }

        private void SettingSubtab_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                if (SettingSubtab.SelectedIndex == 1)
                {
                    try
                    {
                        GetTimeSlotForCombo();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                    try
                    {
                        GetNotificationSettingFill();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                }

                if (SettingSubtab.SelectedIndex == 2)
                {
                    try
                    {
                        GetEmailDetails();
                    }
                    catch
                    { }
                }

                if (SettingSubtab.SelectedIndex == 3)
                {
                    try
                    {
                        GetColorBind_Reminder();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                    try
                    {
                        GetColorBind_Log();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                    try
                    {
                        GetHoursBind();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                    try
                    {

                        drptimeSlot.SelectedIndex = 0;
                        GetTimeSlot();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                    try
                    {
                        GetSigna();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                    try
                    {
                        GetBulkType();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                    try
                    {

                        GetUpdateBulkReminder();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                    try
                    {
                        GetRecallInterval();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                    try
                    {
                        GetHMT();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                    try
                    {
                        GetA1LawSyncStatus();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                    try
                    {
                        GetAPIStatus();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                }

                if (SettingSubtab.SelectedIndex == 4)
                {
                    GetRegistrationDetails();
                }

                if (SettingSubtab.SelectedIndex == 5)
                {
                    try
                    {
                        DateTime FromDate = DateTime.Parse(DateTime.Today.AddDays(-7).ToString());
                        DateTime ToDate = DateTime.Parse(System.DateTime.Now.ToString());

                        strtdateTimePickerReport.Text = FromDate.ToString();
                        enddateTimePickerReport.Text = ToDate.ToString();
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            Cursor.Current = Cursors.Default;
        }

        public void GetColorBind_Reminder()
        {
            try
            {

                DataSet dsColorReminder = new DataSet();
                string s = "exec Sp_GetColorcode_Reminder 'Reminder'";

                dsColorReminder = dal.GetData(s, Application.StartupPath);

                if (dsColorReminder != null && dsColorReminder.Tables != null && dsColorReminder.Tables[0].Rows.Count > 0)
                {
                    grdReminderColor.Columns.Clear();

                    grdReminderColor.DataSource = dsColorReminder.Tables[0];
                    grdReminderColor.CurrentCell.Selected = false;

                    grdReminderColor.Columns[0].Visible = false;
                    grdReminderColor.Columns[2].Visible = false;
                    grdReminderColor.Columns[3].Visible = false;
                    grdReminderColor.Columns[4].Visible = false;
                    grdReminderColor.Columns[5].Visible = false;

                    DataGridViewImageColumn dgvEditColor = new DataGridViewImageColumn();
                    dgvEditColor.Image = SynergyNotificationSystem.Properties.Resources.Edit;
                    dgvEditColor.HeaderText = "";
                    grdReminderColor.Columns.Insert(6, dgvEditColor);

                    grdReminderColor.Columns[1].Width = 224;
                    grdReminderColor.Columns[6].Width = 100;
                }
                else
                {
                    grdReminderColor.Columns.Clear();
                    grdReminderColor.DataSource = null;
                }
            }
            catch
            { }
        }

        public void GetColorBind_Log()
        {
            try
            {
                DataSet dsColorLog = new DataSet();
                string s = "exec Sp_GetColorcode_Reminder 'Log'";

                dsColorLog = dal.GetData(s, Application.StartupPath);

                if (dsColorLog != null && dsColorLog.Tables != null && dsColorLog.Tables[0].Rows.Count > 0)
                {
                    grdLogColor.Columns.Clear();

                    grdLogColor.DataSource = dsColorLog.Tables[0];
                    grdLogColor.CurrentCell.Selected = false;

                    grdLogColor.Columns[0].Visible = false;
                    grdLogColor.Columns[2].Visible = false;
                    grdLogColor.Columns[3].Visible = false;
                    grdLogColor.Columns[4].Visible = false;
                    grdLogColor.Columns[5].Visible = false;

                    DataGridViewImageColumn dgvEditColor = new DataGridViewImageColumn();
                    dgvEditColor.Image = SynergyNotificationSystem.Properties.Resources.Edit;
                    dgvEditColor.HeaderText = "";
                    grdLogColor.Columns.Insert(6, dgvEditColor);

                    grdLogColor.Columns[1].Width = 224;
                    grdLogColor.Columns[6].Width = 100;

                    grdLogColor.CurrentCell.Selected = false;
                }
                else
                {
                    grdLogColor.Columns.Clear();
                    grdLogColor.DataSource = null;
                }
            }
            catch
            { }
        }

        public void GetTimeSlot()
        {
            try
            {
                DataSet dsTime = new DataSet();
                string s = "exec Sp_GetTimeSlot";

                dsTime = dal.GetData(s, Application.StartupPath);

                if (dsTime != null && dsTime.Tables != null && dsTime.Tables[0].Rows.Count > 0)
                {
                    grdtimeSlot.Columns.Clear();

                    grdtimeSlot.DataSource = dsTime.Tables[0];
                    grdtimeSlot.CurrentCell.Selected = false;

                    grdtimeSlot.Columns[1].Visible = false;

                    DataGridViewImageColumn dgvRemoveTime = new DataGridViewImageColumn();
                    dgvRemoveTime.Image = SynergyNotificationSystem.Properties.Resources.Remove;
                    dgvRemoveTime.HeaderText = "";
                    grdtimeSlot.Columns.Insert(3, dgvRemoveTime);
                    grdtimeSlot.Columns[0].Width = 50;
                    grdtimeSlot.Columns[2].Width = 174;
                    grdtimeSlot.Columns[3].Width = 100;
                }
                else
                {
                    grdtimeSlot.DataSource = null;
                }
            }
            catch
            { }
        }

        DataSet dsgetTwiilioLog = new DataSet();

        public void GetTwilioFilterDetails(string Flag, string Fromdt, string todate, string Filter)
        {
            string s = "exec Sp_TwilioLog_Filter '" + Flag + "','" + Fromdt + "','" + todate + "','" + Filter + "'";

            dsgetTwiilioLog = dal.GetData(s, Application.StartupPath);

            if (dsgetTwiilioLog != null && dsgetTwiilioLog.Tables != null && dsgetTwiilioLog.Tables[0].Rows.Count > 0)
            {
                grdTwilioLog.Columns.Clear();

                DataGridViewImageColumn dgvView = new DataGridViewImageColumn();
                dgvView.Image = SynergyNotificationSystem.Properties.Resources.View;
                dgvView.HeaderText = "";
                grdTwilioLog.Columns.Add(dgvView);

                DataGridViewImageColumn dgvretry = new DataGridViewImageColumn();
                dgvretry.Image = SynergyNotificationSystem.Properties.Resources.RetryCall;
                dgvretry.HeaderText = "";
                grdTwilioLog.Columns.Add(dgvretry);

                grdTwilioLog.DataSource = dsgetTwiilioLog.Tables[0];

                grdTwilioLog.Columns[0].Width = 50;
                grdTwilioLog.Columns[1].Width = 50;
                grdTwilioLog.Columns[2].Width = 50;
                grdTwilioLog.Columns[3].Width = 130;
                grdTwilioLog.Columns[4].Width = 130;
                grdTwilioLog.Columns[6].Width = 80;
                grdTwilioLog.Columns[7].Width = 170;
                grdTwilioLog.Columns[8].Width = 170;
                grdTwilioLog.Columns[9].Width = 110;

                grdTwilioLog.Columns[12].Visible = false;
                grdTwilioLog.Columns[13].Visible = false;

                grdTwilioLog.Columns[15].Visible = false;
                grdTwilioLog.Columns[16].Visible = false;
                grdTwilioLog.Columns[17].Visible = false;

                grdTwilioLog.Columns[8].Visible = false;
                grdTwilioLog.Columns[3].Visible = false;

                grdTwilioLog.Columns[4].Name = "Cell Number";
                grdTwilioLog.Columns[7].Name = "Date Time";

                grdTwilioLog.CurrentCell.Selected = false;
            }
            else
            {
                grdTwilioLog.Columns.Clear();
                grdTwilioLog.DataSource = null;
            }
        }

        private void drpogFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                TwilioRefreshGrid();
            }
            catch
            { }
            Cursor.Current = Cursors.Default;
        }

        public void TwilioRefreshGrid()
        {
            try
            {
                if (drpogFilter.SelectedItem.ToString() == "Upcoming")
                {
                    txtTwilioFilterFrom.Visible = false;
                    txtTwilioFilterTo.Visible = false;
                    btnDateSearchLog.Visible = false;
                    lblTwilioTO.Visible = false;

                    try
                    {
                        GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), "", "", txtTwilioLogsearch.Text);
                    }
                    catch
                    {
                    }
                }
                else if (drpogFilter.SelectedItem.ToString() == "Today")
                {
                    txtTwilioFilterFrom.Visible = false;
                    txtTwilioFilterTo.Visible = false;
                    lblTwilioTO.Visible = false;
                    btnDateSearchLog.Visible = false;
                    try
                    {
                        GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), "", "", txtTwilioLogsearch.Text);
                    }
                    catch
                    {
                    }
                }
                else if (drpogFilter.SelectedItem.ToString() == "Tomorrow")
                {
                    txtTwilioFilterFrom.Visible = false;
                    txtTwilioFilterTo.Visible = false;
                    lblTwilioTO.Visible = false;
                    btnDateSearchLog.Visible = false;

                    try
                    {
                        GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), "", "", txtTwilioLogsearch.Text);
                    }
                    catch
                    {
                    }
                }
                else if (drpogFilter.SelectedItem.ToString() == "Yesterday")
                {
                    txtTwilioFilterFrom.Visible = false;
                    txtTwilioFilterTo.Visible = false;
                    lblTwilioTO.Visible = false;
                    btnDateSearchLog.Visible = false;

                    try
                    {
                        GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), "", "", txtTwilioLogsearch.Text);
                    }
                    catch
                    {
                    }
                }
                else if (drpogFilter.SelectedItem.ToString() == "Past Week")
                {
                    txtTwilioFilterFrom.Visible = false;
                    txtTwilioFilterTo.Visible = false;
                    lblTwilioTO.Visible = false;
                    btnDateSearchLog.Visible = false;

                    try
                    {
                        GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), "", "", txtTwilioLogsearch.Text);
                    }
                    catch
                    {
                    }
                }

                else if (drpogFilter.SelectedItem.ToString() == "Specific Date")
                {
                    txtTwilioFilterFrom.Visible = false;
                    txtTwilioFilterTo.Visible = false;
                    lblTwilioTO.Visible = false;
                    btnDateSearchLog.Visible = false;

                    try
                    {
                        DateTime FromDate = DateTime.Parse(DateTime.Today.ToString());
                        txtTwilioFilterFrom.Text = FromDate.ToString();
                    }
                    catch
                    {
                    }

                    txtTwilioFilterFrom.Visible = true;
                    btnDateSearchLog.Visible = true;
                    btnDateSearchLog.Location = new System.Drawing.Point(266, 24);

                    try
                    {
                        GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), txtTwilioFilterFrom.Text, "", txtTwilioLogsearch.Text);
                    }
                    catch
                    {
                    }
                }
                else if (drpogFilter.SelectedItem.ToString() == "Custom Range")
                {
                    txtTwilioFilterFrom.Visible = false;
                    txtTwilioFilterTo.Visible = false;
                    lblTwilioTO.Visible = false;
                    btnDateSearchLog.Visible = false;

                    txtTwilioFilterFrom.Visible = true;
                    txtTwilioFilterTo.Visible = true;
                    lblTwilioTO.Visible = true;
                    btnDateSearchLog.Visible = true;
                    btnDateSearchLog.Location = new System.Drawing.Point(406, 24);

                    try
                    {
                        DateTime FromDate = DateTime.Parse(DateTime.Today.AddDays(-7).ToString());
                        DateTime ToDate = DateTime.Parse(DateTime.Today.ToString());

                        txtTwilioFilterFrom.Text = FromDate.ToString();
                        txtTwilioFilterTo.Text = ToDate.ToString();
                    }
                    catch
                    {
                    }

                    try
                    {
                        GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), txtTwilioFilterFrom.Text, txtTwilioFilterTo.Text, txtTwilioLogsearch.Text);
                    }
                    catch
                    {
                    }
                }
                else
                {
                    txtTwilioFilterFrom.Visible = false;
                    txtTwilioFilterTo.Visible = false;
                    lblTwilioTO.Visible = false;

                    try
                    {
                        GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), "", "", txtTwilioLogsearch.Text);
                    }
                    catch
                    {
                    }
                }
                grdTwilioLog.Focus();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void txtTwilioFilterTo_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtTwilioFilterFrom_ValueChanged(object sender, EventArgs e)
        {

        }

        private void grdAgendadata_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                Clean();
            }
            catch
            {
            }

            if (e.ColumnIndex != 24 && e.RowIndex != -1 && e.ColumnIndex != 36)
            {
                try
                {

                    string TitleMain = grdAgendadata.Rows[e.RowIndex].Cells[5].Value.ToString();
                    string[] values = TitleMain.Split(':');

                    App_SetValueForTitle = values[0].ToString().Trim();
                    App_SetValueForEventBody = values[1].ToString().Trim();

                    string TitleMainHTMl = grdAgendadata.Rows[e.RowIndex].Cells[28].Value.ToString();
                    string[] valuesHTML = TitleMainHTMl.Split(':');

                    App_SetValueForEventHTMLBody = valuesHTML[1].ToString().Trim();
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                App_MainWhere = "MainPage";
                App_SetValueForLocation = grdAgendadata.Rows[e.RowIndex].Cells[8].Value.ToString();

                App_SetValueForTimestart = grdAgendadata.Rows[e.RowIndex].Cells[23].Value.ToString();
                App_SetValueForTimestartHour = grdAgendadata.Rows[e.RowIndex].Cells[14].Value.ToString();
                App_SetValueForTimestartMinute = grdAgendadata.Rows[e.RowIndex].Cells[15].Value.ToString();

                App_SetValueForTimeEnd = grdAgendadata.Rows[e.RowIndex].Cells[23].Value.ToString();
                App_SetValueForTimeEndHour = grdAgendadata.Rows[e.RowIndex].Cells[14].Value.ToString();
                App_SetValueForTimeEndMinute = grdAgendadata.Rows[e.RowIndex].Cells[15].Value.ToString();

                App_AMPM = grdAgendadata.Rows[e.RowIndex].Cells[29].Value.ToString();

                App_SetValueForFname = grdAgendadata.Rows[e.RowIndex].Cells[6].Value.ToString();
                App_SetValueForLname = grdAgendadata.Rows[e.RowIndex].Cells[7].Value.ToString();


                App_SetValueForFirmCode = grdAgendadata.Rows[e.RowIndex].Cells[2].Value.ToString();
                App_SetValueForEventNo = grdAgendadata.Rows[e.RowIndex].Cells[1].Value.ToString();

                App_SetValueForBtnsubmit = "Update Event";

                App_SMS = grdAgendadata.Rows[e.RowIndex].Cells[9].Value.ToString();
                APP_Call = grdAgendadata.Rows[e.RowIndex].Cells[10].Value.ToString();
                APP_Email = grdAgendadata.Rows[e.RowIndex].Cells[11].Value.ToString();

                App_PartiesCode = grdAgendadata.Rows[e.RowIndex].Cells[16].Value.ToString();

                App_SetValueForCaseNo = grdAgendadata.Rows[e.RowIndex].Cells[18].Value.ToString();

                App_WhenSMS = grdAgendadata.Rows[e.RowIndex].Cells[19].Value.ToString();
                App_WhenCALL = grdAgendadata.Rows[e.RowIndex].Cells[20].Value.ToString();
                App_WhenEMAIL = grdAgendadata.Rows[e.RowIndex].Cells[21].Value.ToString();

                App_Private = grdAgendadata.Rows[e.RowIndex].Cells[30].Value.ToString();
                App_IsConfirmable = grdAgendadata.Rows[e.RowIndex].Cells[32].Value.ToString();
                App_AppointMentTempType = grdAgendadata.Rows[e.RowIndex].Cells[34].Value.ToString();

                App_TwilioLanguage = grdAgendadata.Rows[e.RowIndex].Cells[35].Value.ToString();
                try
                {
                    AddAppointment App = new AddAppointment();
                    App.ShowDialog();
                }
                catch
                { }
            }

            Cursor.Current = Cursors.Default;
        }

        private void grdTwilioLog_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                foreach (DataGridViewRow Filterrow in grdTwilioLog.Rows)
                {
                    if (Filterrow.Cells[5].Value.ToString() == "completed")
                    {
                        Filterrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Filterrow.Cells[15].Value), Convert.ToInt32(Filterrow.Cells[16].Value), Convert.ToInt32(Filterrow.Cells[17].Value));
                    }
                    else if (Filterrow.Cells[5].Value.ToString() == "undelivered")
                    {
                        Filterrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Filterrow.Cells[15].Value), Convert.ToInt32(Filterrow.Cells[16].Value), Convert.ToInt32(Filterrow.Cells[17].Value));
                    }
                    else if (Filterrow.Cells[5].Value.ToString() == "no-answer")
                    {
                        Filterrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Filterrow.Cells[15].Value), Convert.ToInt32(Filterrow.Cells[16].Value), Convert.ToInt32(Filterrow.Cells[17].Value));
                    }
                    else if (Filterrow.Cells[5].Value.ToString() == "delivered")
                    {
                        Filterrow.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(Filterrow.Cells[15].Value), Convert.ToInt32(Filterrow.Cells[16].Value), Convert.ToInt32(Filterrow.Cells[17].Value));
                    }
                    else
                    {

                    }
                }
            }
            catch
            { }
        }

        //public void getTwilioSMSLogs(string AccountSid, string AuthToken)
        //{



        //    var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);
        //    var options = new Twilio.MessageListRequest();

        //    DataSet dsGetMaxDateSMS = new DataSet();
        //    string strMAX = "exec Sp_GetTwilioLogMAXDate 'SMS'";
        //    dsGetMaxDateSMS = dal.GetData(strMAX, Application.StartupPath);
        //    if (dsGetMaxDateSMS != null && dsGetMaxDateSMS.Tables != null && dsGetMaxDateSMS.Tables[0].Rows.Count > 0)
        //    {
        //        DateTime MAXDATE = Convert.ToDateTime(dsGetMaxDateSMS.Tables[0].Rows[0]["MAXDT"].ToString());
        //        options.DateSent = MAXDATE;
        //    }

        //    var messages = twilio.ListMessages(options);

        //    foreach (var calllog in messages.Messages)
        //    {


        //        try
        //        {

        //            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
        //            dateInfo.ShortDatePattern = "MM/dd/yyyy";

        //            DataSet dsLogSMS = new DataSet();
        //            SqlParameter[] p = new SqlParameter[9];

        //            p[0] = new SqlParameter("@FromNumber", calllog.From);
        //            p[1] = new SqlParameter("@ToNumber", calllog.To);
        //            p[2] = new SqlParameter("@Direction", calllog.Direction);
        //            p[3] = new SqlParameter("@Duration", 0);
        //            p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(calllog.DateSent) == "" ? Convert.ToString(calllog.DateSent) : Convert.ToDateTime(calllog.DateSent, dateInfo).ToString("MM/dd/yyyy")));
        //            p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(calllog.DateSent) == "" ? Convert.ToString(calllog.DateSent) : Convert.ToDateTime(calllog.DateSent, dateInfo).ToString("MM/dd/yyyy")));
        //            p[6] = new SqlParameter("@Status", calllog.Status);
        //            p[7] = new SqlParameter("@SID", calllog.Sid);
        //            p[8] = new SqlParameter("@LogType", "SMS");



        //            string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
        //            dsLogSMS = dal.GetData(s, Application.StartupPath);
        //            if (dsLogSMS != null && dsLogSMS.Tables != null && dsLogSMS.Tables[0].Rows.Count > 0)
        //            {
        //            }
        //            else
        //            { }
        //        }
        //        catch
        //        { }
        //    }
        //}

        //public void getTwilioCallLogs(string AccountSid, string AuthToken)
        //{



        //    var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);
        //    var options = new Twilio.CallListRequest();




        //    var call = twilio.ListCalls(options);

        //    foreach (var calllog in call.Calls)
        //    {


        //        try
        //        {
        //            DataSet dsLogCALL = new DataSet();
        //            SqlParameter[] p = new SqlParameter[9];


        //            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
        //            dateInfo.ShortDatePattern = "MM/dd/yyyy";

        //            p[0] = new SqlParameter("@FromNumber", calllog.From);
        //            p[1] = new SqlParameter("@ToNumber", calllog.To);
        //            p[2] = new SqlParameter("@Direction", calllog.Direction);
        //            p[3] = new SqlParameter("@Duration", calllog.Duration);
        //            p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(calllog.StartTime) == "" ? Convert.ToString(calllog.StartTime) : Convert.ToDateTime(calllog.StartTime, dateInfo).ToString("MM/dd/yyyy")));
        //            p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(calllog.EndTime) == "" ? Convert.ToString(calllog.EndTime) : Convert.ToDateTime(calllog.EndTime, dateInfo).ToString("MM/dd/yyyy")));

        //            p[6] = new SqlParameter("@Status", calllog.Status);
        //            p[7] = new SqlParameter("@SID", calllog.Sid);
        //            p[8] = new SqlParameter("@LogType", "CALL");



        //            string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
        //            dsLogCALL = dal.GetData(s, Application.StartupPath);
        //            if (dsLogCALL != null && dsLogCALL.Tables != null && dsLogCALL.Tables[0].Rows.Count > 0)
        //            {
        //            }
        //            else
        //            { }
        //        }
        //        catch
        //        { }
        //    }
        //}


        // Milind Pasi change In Pagination --START

        public static int totalNumOfRec = 0;

        public void getTwilioSMSLogs(string AccountSid, string AuthToken)
        {
            var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);
            var options = new Twilio.MessageListRequest();

            //DataSet dsGetMaxDateSMS = new DataSet();
            //string strMAX = "exec Sp_GetTwilioLogMAXDate 'SMS'";
            //dsGetMaxDateSMS = dal.GetData(strMAX, Application.StartupPath);
            //if (dsGetMaxDateSMS != null && dsGetMaxDateSMS.Tables != null && dsGetMaxDateSMS.Tables[0].Rows.Count > 0)
            //{
            //    DateTime MAXDATE = Convert.ToDateTime(dsGetMaxDateSMS.Tables[0].Rows[0]["MAXDT"].ToString());
            //    options.DateSentComparison = Twilio.ComparisonType.GreaterThanOrEqualTo;
            //    options.DateSent = MAXDATE;
            //}

            var messages = twilio.ListMessages(options);

            foreach (var calllog in messages.Messages)
            {
                try
                {
                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

                    DataSet dsLogSMS = new DataSet();
                    SqlParameter[] p = new SqlParameter[9];

                    p[0] = new SqlParameter("@FromNumber", calllog.From);
                    p[1] = new SqlParameter("@ToNumber", calllog.To);
                    p[2] = new SqlParameter("@Direction", calllog.Direction);
                    p[3] = new SqlParameter("@Duration", 0);
                    p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(calllog.DateSent) == "" ? Convert.ToString(calllog.DateSent) : Convert.ToDateTime(calllog.DateSent, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(calllog.DateSent) == "" ? Convert.ToString(calllog.DateSent) : Convert.ToDateTime(calllog.DateSent, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[6] = new SqlParameter("@Status", calllog.Status);
                    p[7] = new SqlParameter("@SID", calllog.Sid);
                    p[8] = new SqlParameter("@LogType", "SMS");

                    string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
                    dsLogSMS = dal.GetData(s, Application.StartupPath);
                    if (dsLogSMS != null && dsLogSMS.Tables != null && dsLogSMS.Tables[0].Rows.Count > 0)
                    {
                    }
                    else
                    { }
                }
                catch
                { }
            }

            string nextPageURI = messages.NextPageUri.ToString();

            int totalRecs = 0;
            if (nextPageURI != null && Convert.ToString(nextPageURI) != "")
            {
                string nextPageUriForNextData = "https://api.twilio.com" + messages.NextPageUri.ToString();
                totalRecs = twilioSMSLogPagination(nextPageUriForNextData, 1);
            }
        }

        public int twilioSMSLogPagination(string nextPageUri, int count)
        {
            var client = new RestSharp.RestClient(nextPageUri);
            client.Authenticator = new RestSharp.Authenticators.HttpBasicAuthenticator("ACaab6cd4373c0287b3b81cf957ba4f398", "537fce18a5579c7df939fdc51761e39e");

            var request = new RestSharp.RestRequest(RestSharp.Method.GET);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("cache-control", "no-cache");
            RestSharp.IRestResponse response = client.Execute(request);
            RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

            var JSONObj = deserial.Deserialize<SMSLogCompleteObj>(response);

            var callLogResTest = JSONObj.messages;

            foreach (var individualCallRecord in callLogResTest)
            {
                try
                {
                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

                    DataSet dsLogSMS = new DataSet();
                    SqlParameter[] p = new SqlParameter[9];

                    p[0] = new SqlParameter("@FromNumber", individualCallRecord.from);
                    p[1] = new SqlParameter("@ToNumber", individualCallRecord.to);
                    p[2] = new SqlParameter("@Direction", individualCallRecord.direction);
                    p[3] = new SqlParameter("@Duration", 0);
                    p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(individualCallRecord.date_sent) == "" ? Convert.ToString(individualCallRecord.date_sent) : Convert.ToDateTime(individualCallRecord.date_sent, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(individualCallRecord.date_sent) == "" ? Convert.ToString(individualCallRecord.date_sent) : Convert.ToDateTime(individualCallRecord.date_sent, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[6] = new SqlParameter("@Status", individualCallRecord.status);
                    p[7] = new SqlParameter("@SID", individualCallRecord.sid);
                    p[8] = new SqlParameter("@LogType", "SMS");


                    string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
                    dsLogSMS = dal.GetData(s, Application.StartupPath);
                    if (dsLogSMS != null && dsLogSMS.Tables != null && dsLogSMS.Tables[0].Rows.Count > 0)
                    {
                    }
                    else
                    { }
                }
                catch
                { }
            }

            string nextPageUriFromResponse = "";

            if (JSONObj.next_page_uri != null && Convert.ToString(JSONObj.next_page_uri) != "")
            {
                nextPageUriFromResponse = JSONObj.next_page_uri.ToString();
            }

            if (nextPageUriFromResponse != null && Convert.ToString(nextPageUriFromResponse) != "")
            {
                string nextRequestURI = "https://api.twilio.com" + nextPageUriFromResponse;
                return twilioSMSLogPagination(nextRequestURI, ++count);
            }
            else
            {
                return count;
            }
        }

        public void getTwilioCallLogs(string AccountSid, string AuthToken)
        {
            var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);
            var options = new Twilio.CallListRequest();

            //DataSet dsGetMaxDateCALL = new DataSet();
            //string strMAX = "exec Sp_GetTwilioLogMAXDate 'CALL'";
            //dsGetMaxDateCALL = dal.GetData(strMAX, Application.StartupPath);
            //if (dsGetMaxDateCALL != null && dsGetMaxDateCALL.Tables != null && dsGetMaxDateCALL.Tables[0].Rows.Count > 0)
            //{
            //    DateTime MAXDATE = Convert.ToDateTime(dsGetMaxDateCALL.Tables[0].Rows[0]["MAXDT"].ToString());
            //    options.StartTimeComparison = Twilio.ComparisonType.GreaterThanOrEqualTo;
            //    options.StartTime = MAXDATE;
            //}


            var call = twilio.ListCalls(options);

            foreach (var calllog in call.Calls)
            {
                try
                {
                    DataSet dsLogCALL = new DataSet();
                    SqlParameter[] p = new SqlParameter[9];

                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

                    p[0] = new SqlParameter("@FromNumber", calllog.From);
                    p[1] = new SqlParameter("@ToNumber", calllog.To);
                    p[2] = new SqlParameter("@Direction", calllog.Direction);
                    p[3] = new SqlParameter("@Duration", calllog.Duration);
                    p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(calllog.DateCreated) == "" ? Convert.ToString(calllog.DateCreated) : Convert.ToDateTime(calllog.DateCreated, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(calllog.DateUpdated) == "" ? Convert.ToString(calllog.DateUpdated) : Convert.ToDateTime(calllog.DateUpdated, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[6] = new SqlParameter("@Status", calllog.Status);
                    p[7] = new SqlParameter("@SID", calllog.Sid);
                    p[8] = new SqlParameter("@LogType", "CALL");

                    string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
                    dsLogCALL = dal.GetData(s, Application.StartupPath);
                    if (dsLogCALL != null && dsLogCALL.Tables != null && dsLogCALL.Tables[0].Rows.Count > 0)
                    {
                    }
                    else
                    { }
                }
                catch
                { }
            }

            // checking for pagination in the response of twilio.
            int totalRecs = 0;
            if (call.NextPageUri != null && Convert.ToString(call.NextPageUri) != "")
            {
                string nextPageURI = call.NextPageUri.ToString();
                if (nextPageURI != null && Convert.ToString(nextPageURI) != "")
                {
                    string nextPageUriForNextData = "https://api.twilio.com" + call.NextPageUri.ToString();

                    totalRecs += twilioCallLogPagination(nextPageUriForNextData, 1);
                }
            }
        }

        public int twilioCallLogPagination(string nextPageUri, int count)
        {
            var client = new RestSharp.RestClient(nextPageUri);
            client.Authenticator = new RestSharp.Authenticators.HttpBasicAuthenticator("ACaab6cd4373c0287b3b81cf957ba4f398", "537fce18a5579c7df939fdc51761e39e");

            var request = new RestSharp.RestRequest(RestSharp.Method.GET);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("cache-control", "no-cache");
            RestSharp.IRestResponse response = client.Execute(request);
            RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

            var JSONObj = deserial.Deserialize<CallLogCompleteObj>(response);


            var call = JSONObj.calls;

            string MAINOUTPUT2 = "";

            foreach (var calllog in call)
            {

                totalNumOfRec = totalNumOfRec + 1;

                var test = calllog.from;
                try
                {
                    DataSet dsLogCALL = new DataSet();
                    SqlParameter[] p = new SqlParameter[9];


                    MAINOUTPUT2 += totalNumOfRec + " ::::: START_DATE : " + calllog.start_time + "FROM : " + calllog.from + " ====> TO :: " + calllog.to + " =====> SID : " + calllog.sid + " <br /> ";

                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

                    p[0] = new SqlParameter("@FromNumber", calllog.from);
                    p[1] = new SqlParameter("@ToNumber", calllog.to);
                    p[2] = new SqlParameter("@Direction", calllog.direction);
                    p[3] = new SqlParameter("@Duration", calllog.duration);
                    p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(calllog.date_created) == "" ? Convert.ToString(calllog.date_created) : Convert.ToDateTime(calllog.date_created, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(calllog.date_updated) == "" ? Convert.ToString(calllog.date_updated) : Convert.ToDateTime(calllog.date_updated, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[6] = new SqlParameter("@Status", calllog.status);
                    p[7] = new SqlParameter("@SID", calllog.sid);
                    p[8] = new SqlParameter("@LogType", "CALL");

                    string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
                    dsLogCALL = dal.GetData(s, Application.StartupPath);
                    if (dsLogCALL != null && dsLogCALL.Tables != null && dsLogCALL.Tables[0].Rows.Count > 0)
                    {
                    }
                    else
                    { }
                }
                catch
                { }
            }


            string nextPageUriFromResponse = "";
            if (JSONObj.next_page_uri != null && Convert.ToString(JSONObj.next_page_uri) != "")
            {
                nextPageUriFromResponse = JSONObj.next_page_uri.ToString();
            }

            if (nextPageUriFromResponse != null && Convert.ToString(nextPageUriFromResponse) != "")
            {
                string nextRequestURI = "https://api.twilio.com" + nextPageUriFromResponse;
                return twilioCallLogPagination(nextRequestURI, ++count);
            }
            else
            {
                return totalNumOfRec;
            }
        }

        public static string ret_SetValueForBtnsubmit, ret_SetValueForFname, ret_SetValueForEventNo,
        ret_SetValueForLname, ret_SetValueForCaseNo, ret_SetValueForFirmCode, ret_SetValueForTitle,
        ret_SetValueForTimestart, ret_SetValueForTimestartHour, ret_SetValueForTimestartMinute, ret_SetValueForTimeEnd,
        ret_SetValueForTimeEndHour, ret_SetValueForTimeEndMinute, ret_SetValueForTimeZone, ret_SetValueForLocation, ret_SetValueForEventBody,
            ret_SetValueForEventHTMLBody,
        ret_SMS, ret_Call, ret_Email, ret_PartiesFirmcode, ret_PartiesCode, ret_WhenSMS, ret_WhenCALL, ret_WhenEMAIL, ret_TwilioLog, ret_AMPM, ret_Private,
        ret_IsConfirmable, ret_TwilioLang = string.Empty;

        private void grdTwilioLog_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                //---Start

                Cursor.Current = Cursors.WaitCursor;

                DataSet dsretry = new DataSet();
                string s = "exec Sp_GetA1LawDetailsCalendar '" + grdTwilioLog.Rows[e.RowIndex].Cells[12].Value.ToString() + "'";

                dsretry = dal.GetData(s, Application.StartupPath);

                if (dsretry != null && dsretry.Tables != null && dsretry.Tables[0].Rows.Count > 0)
                {


                    try
                    {

                        string TitleMain = dsretry.Tables[0].Rows[0]["Event Details"].ToString();
                        string[] values = TitleMain.Split(':');

                        ret_SetValueForTitle = values[0].ToString().Trim();
                        ret_SetValueForEventBody = values[1].ToString().Trim();

                        string TitleMainHTML = dsretry.Tables[0].Rows[0]["EventHTML"].ToString();
                        string[] valuesHTML = TitleMainHTML.Split(':');

                        ret_SetValueForEventHTMLBody = valuesHTML[1].ToString().Trim();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                    ret_TwilioLog = "TwilioLogPage";
                    ret_SetValueForLocation = dsretry.Tables[0].Rows[0]["Location"].ToString();

                    ret_SetValueForTimestart = dsretry.Tables[0].Rows[0]["BindDate"].ToString();
                    ret_SetValueForTimestartHour = dsretry.Tables[0].Rows[0]["Hours"].ToString();
                    ret_SetValueForTimestartMinute = dsretry.Tables[0].Rows[0]["Minute"].ToString();

                    ret_SetValueForTimeEnd = dsretry.Tables[0].Rows[0]["BindDate"].ToString();
                    ret_SetValueForTimeEndHour = dsretry.Tables[0].Rows[0]["Hours"].ToString();
                    ret_SetValueForTimeEndMinute = dsretry.Tables[0].Rows[0]["Minute"].ToString();

                    ret_SetValueForFname = dsretry.Tables[0].Rows[0]["First Name"].ToString();
                    ret_SetValueForLname = dsretry.Tables[0].Rows[0]["Last Name"].ToString();


                    ret_SetValueForFirmCode = dsretry.Tables[0].Rows[0]["CardcodeFK"].ToString();
                    ret_SetValueForEventNo = dsretry.Tables[0].Rows[0]["eventno"].ToString();

                    ret_SetValueForBtnsubmit = "Cancel";

                    ret_SMS = dsretry.Tables[0].Rows[0]["SMS_Status"].ToString();
                    ret_Call = dsretry.Tables[0].Rows[0]["CALL_Status"].ToString();
                    ret_Email = dsretry.Tables[0].Rows[0]["Email_Status"].ToString();

                    ret_PartiesCode = dsretry.Tables[0].Rows[0]["EventParties"].ToString();

                    ret_SetValueForCaseNo = dsretry.Tables[0].Rows[0]["caseno"].ToString();

                    ret_WhenSMS = dsretry.Tables[0].Rows[0]["whenSMS"].ToString();
                    ret_WhenCALL = dsretry.Tables[0].Rows[0]["whenCALL"].ToString();
                    ret_WhenEMAIL = dsretry.Tables[0].Rows[0]["whenEMAIL"].ToString();
                    ret_AMPM = dsretry.Tables[0].Rows[0]["AMPM"].ToString();

                    ret_Private = dsretry.Tables[0].Rows[0]["IsPrivate"].ToString();
                    ret_IsConfirmable = dsretry.Tables[0].Rows[0]["IsConfirmable"].ToString();

                    ret_TwilioLang = dsretry.Tables[0].Rows[0]["TwilioLang"].ToString();

                    try
                    {
                        AddAppointment App = new AddAppointment();
                        App.ShowDialog();
                    }
                    catch
                    { }
                }

                Cursor.Current = Cursors.Default;
                //---End
            }

            if (e.ColumnIndex == 1)
            {
                Cursor.Current = Cursors.WaitCursor;

                DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Are you sure reschedule Reminder After 2 Minute ?", "Re-schedule Confirmation", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)  // error is here
                {

                    string strTimingID = string.Empty;
                    strTimingID = grdTwilioLog.Rows[e.RowIndex].Cells[13].Value.ToString();

                    try
                    {
                        DataSet dsSID = new DataSet();
                        string strSID = "exec Sp_Update_reschedule '" + strTimingID + "'";
                        dsSID = dal.GetData(strSID, Application.StartupPath);

                        if (dsSID.Tables[0].Rows[0]["Result"].ToString() == "1")
                        {

                            try
                            {
                                CloudSyncReminderAdd();
                            }
                            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                            MessageBox.Show("Reschedule Reminder Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    catch
                    { }
                }
                else
                { }

                Cursor.Current = Cursors.Default;

            }

        }

        public void CloudSyncReminderAdd()
        {
            try
            {
                DataSet dsCloudSync = new DataSet();
                string strCloud = "Exec Get_CloudSyncDate_ForLog"; //Change For Log Store proc.
                dsCloudSync = dal.GetData(strCloud, Application.StartupPath);

                if (dsCloudSync.Tables[0].Rows.Count > 0)
                {


                    var postData = string.Empty;



                    var request = (HttpWebRequest)WebRequest.Create(ConfigReader.GetCloudServiceHost() + "Reminder/wsAddReminder");

                    request.ContentType = "application/json";
                    request.Method = "POST";





                    postData = "[";
                    for (int i = 0; i <= dsCloudSync.Tables[0].Rows.Count - 1; i++)
                    {

                        postData += "{";
                        postData += "'RID':'" + dsCloudSync.Tables[0].Rows[i]["RID"] + "',";
                        postData += "'EventID':'" + dsCloudSync.Tables[0].Rows[i]["EventID"] + "',";
                        postData += "'Event':'" + dsCloudSync.Tables[0].Rows[i]["Event"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'EventTitle':'" + dsCloudSync.Tables[0].Rows[i]["EventTitle"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'Phone':'" + dsCloudSync.Tables[0].Rows[i]["Phone"] + "',";
                        postData += "'Email':'" + dsCloudSync.Tables[0].Rows[i]["Email"] + "',";
                        postData += "'FromPhoneNo':'" + dsCloudSync.Tables[0].Rows[i]["FromPhoneNo"] + "',";
                        postData += "'AccountSID':'" + dsCloudSync.Tables[0].Rows[i]["AccountSID"] + "',";
                        postData += "'AuthToken':'" + dsCloudSync.Tables[0].Rows[i]["AuthToken"] + "',";
                        postData += "'EventDatetime':'" + dsCloudSync.Tables[0].Rows[i]["EventDatetime"] + "',";
                        postData += "'SMSStatus':'" + dsCloudSync.Tables[0].Rows[i]["SMSStatus"] + "',";
                        postData += "'EmailStatus':'" + dsCloudSync.Tables[0].Rows[i]["EmailStatus"] + "',";
                        postData += "'CallStatus':'" + dsCloudSync.Tables[0].Rows[i]["CallStatus"] + "',";
                        postData += "'ClientKey':'" + dsCloudSync.Tables[0].Rows[i]["ClientKey"] + "',";
                        postData += "'RedirectLink':'" + dsCloudSync.Tables[0].Rows[i]["RedirectLink"] + "',";
                        postData += "'EmailFrom':'" + dsCloudSync.Tables[0].Rows[i]["EmailFrom"] + "',";
                        postData += "'EmailPassword':'" + dsCloudSync.Tables[0].Rows[i]["EmailPassword"] + "',";
                        postData += "'EmailFromTitle':'" + dsCloudSync.Tables[0].Rows[i]["EmailFromTitle"] + "',";
                        postData += "'EmailPort':'" + dsCloudSync.Tables[0].Rows[i]["EmailPort"] + "',";
                        postData += "'EmailSMTP':'" + dsCloudSync.Tables[0].Rows[i]["EmailSMTP"] + "',";
                        postData += "'CompanyLOGO':'" + dsCloudSync.Tables[0].Rows[i]["CompanyLOGO"] + "',";
                        postData += "'IsStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsStatus"] + "',";
                        postData += "'MainEventID':'" + dsCloudSync.Tables[0].Rows[i]["MainEventID"] + "',";
                        postData += "'IsTwilioStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsTwilioStatus"] + "',";
                        postData += "'AppointmentDatetime':'" + dsCloudSync.Tables[0].Rows[i]["AppointmentDatetime"] + "',";
                        postData += "'IsConfirmable':'" + dsCloudSync.Tables[0].Rows[i]["IsConfirmable"] + "',";
                        postData += "'TwilioLang':'" + dsCloudSync.Tables[0].Rows[i]["TwilioLang"] + "'";


                        postData += "},";

                    }
                    postData = postData.Remove(postData.Length - 1);
                    postData += "]";

                    var data = Encoding.ASCII.GetBytes(postData);



                    request.ContentLength = data.Length;


                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    ReminderSystem.wsReminderResponse WsCloud = JsonConvert.DeserializeObject<ReminderSystem.wsReminderResponse>(responseString);

                    if (WsCloud.Success == true)
                    {



                        DataTable dtAddConfirm = new DataTable();
                        dtAddConfirm.Columns.AddRange(new DataColumn[2] { new DataColumn("MainEventID", typeof(string)),
                        new DataColumn("ClientKey", typeof(string))});

                        for (int i = 0; i <= WsCloud.ResponseDetail.Count - 1; i++)
                        {
                            dtAddConfirm.Rows.Add(WsCloud.ResponseDetail[i].EventID, WsCloud.ResponseDetail[i].ClientKey);
                        }


                        if (dtAddConfirm != null)
                        {
                            DataSet dsAddSyncupdate = new DataSet();
                            SqlParameter[] p = new SqlParameter[1];
                            p[0] = new SqlParameter("@SyncCloudStatus", dtAddConfirm);
                            dsAddSyncupdate = dal.GetData("[Cloud_UpdateSync]", p, Application.StartupPath);
                        }



                    }
                }

            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ColorDialog dlg = new ColorDialog();
            dlg.ShowDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string str = null;
                str = dlg.Color.R.ToString() + ":" + dlg.Color.G.ToString() + ":" + dlg.Color.B.ToString();
                MessageBox.Show(str);
            }
        }

        private void grdReminderColor_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow MyColor in grdReminderColor.Rows)
            {
                if (Convert.ToInt32(MyColor.Cells[5].Value) == 1)
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }
                else if (Convert.ToInt32(MyColor.Cells[5].Value) == 2)
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }
                else if (Convert.ToInt32(MyColor.Cells[5].Value) == 3)
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }
                else if (Convert.ToInt32(MyColor.Cells[5].Value) == 4)
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }
                else if (Convert.ToInt32(MyColor.Cells[5].Value) == 5)
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }

                else
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }
            }

        }
        ColorDialog dlg = new ColorDialog();
        private void grdReminderColor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                DialogResult dres1 = dlg.ShowDialog();


                if (dres1 == DialogResult.OK)
                {
                    UpdateColorStaus(grdReminderColor.Rows[e.RowIndex].Cells[0].Value.ToString(), dlg.Color.R.ToString(), dlg.Color.G.ToString(), dlg.Color.B.ToString());
                }
            }
        }

        public void UpdateColorStaus(string ColorID, string R, string G, string B)
        {
            try
            {
                DataSet dsupdateColorReminder = new DataSet();
                string s = "exec Sp_Update_Color 'Reminder','" + ColorID + "','" + R + "','" + G + "','" + B + "'";
                dsupdateColorReminder = dal.GetData(s, Application.StartupPath);
                if (dsupdateColorReminder.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    try
                    {
                        GetColorBind_Reminder();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                }
                else
                { }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }

        ColorDialog dlglog = new ColorDialog();
        private void grdLogColor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                DialogResult dresLog = dlglog.ShowDialog();


                if (dresLog == DialogResult.OK)
                {
                    UpdateLogColorstatus(grdLogColor.Rows[e.RowIndex].Cells[0].Value.ToString(), dlglog.Color.R.ToString(), dlglog.Color.G.ToString(), dlglog.Color.B.ToString());
                }
            }
        }
        public void UpdateLogColorstatus(string ColorID, string R, string G, string B)
        {
            try
            {
                DataSet dsupLog = new DataSet();
                string s = "exec Sp_Update_Color 'Log','" + ColorID + "','" + R + "','" + G + "','" + B + "'";
                dsupLog = dal.GetData(s, Application.StartupPath);
                if (dsupLog.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    try
                    {
                        GetColorBind_Log();
                    }
                    catch
                    { }
                }
                else
                { }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }

        private void grdLogColor_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow MyColor in grdLogColor.Rows)
            {


                if (MyColor.Cells[5].Value.ToString() == "completed")
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }
                else if (MyColor.Cells[5].Value.ToString() == "undelivered")
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }
                else if (MyColor.Cells[5].Value.ToString() == "no-answer")
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }
                else if (MyColor.Cells[5].Value.ToString() == "delivered")
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }

                else
                {
                    MyColor.DefaultCellStyle.BackColor = Color.FromArgb(Convert.ToInt32(MyColor.Cells[2].Value), Convert.ToInt32(MyColor.Cells[3].Value), Convert.ToInt32(MyColor.Cells[4].Value));
                }
            }
        }
        OpenFileDialog fdlogo = new OpenFileDialog();
        private void btnCompanyLogo_Click(object sender, EventArgs e)
        {

            fdlogo.Filter = "image files|*.jpg;*.png;*.gif;*.icon;.*;";



            DialogResult dres1 = fdlogo.ShowDialog();
            if (dres1 == DialogResult.Abort)
                return;
            if (dres1 == DialogResult.Cancel)
                return;

            try
            {
                txtCompanyLogo.Text = fdlogo.FileName;

                if (txtCompanyLogo.Text != "")
                {
                    lblPriviewLogo.Visible = true;
                    pblogo.Visible = true;
                    btnRemoveLogo.Visible = true;

                    var onlyFileName = System.IO.Path.GetFullPath(fdlogo.FileName);
                    pblogo.ImageLocation = onlyFileName;
                }
                else
                {
                    lblPriviewLogo.Visible = false;
                    pblogo.Visible = false;
                    btnRemoveLogo.Visible = false;

                }

                lblImagename.Text = Path.GetFileName(txtCompanyLogo.Text);
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }



        private void btnUpload_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            string Finalbase64 = string.Empty;
            if (txtCompanyLogo.Text != "")
            {

                try
                {
                    DirectoryInfo di = new DirectoryInfo(Application.StartupPath + "\\Upload");

                    if (Directory.Exists(Application.StartupPath + "\\Upload"))
                    {

                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                    }
                    else
                    {
                        if (Directory.Exists(Application.StartupPath + "\\Upload") == false)
                        {
                            Directory.CreateDirectory(Application.StartupPath + "\\Upload");
                        }

                    }


                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                try
                {
                    Bitmap b = new Bitmap(txtCompanyLogo.Text);
                    b.Save(Application.StartupPath + "\\Upload\\" + lblImagename.Text);

                }
                catch
                {

                }


                string folder = Application.StartupPath + @"\Upload\";
                var path = Path.Combine(folder, Path.GetFileName(lblImagename.Text));

                Resize_Picture(path, path.Replace(Path.GetFileName(lblImagename.Text), "thumb_" + Path.GetFileName(lblImagename.Text)), 0, 200, 100);

                Finalbase64 = ImageToBase64(path.Replace(Path.GetFileName(lblImagename.Text), "thumb_" + Path.GetFileName(lblImagename.Text)));
                FileInfo f = new FileInfo(path.Replace(Path.GetFileName(lblImagename.Text), "thumb_" + Path.GetFileName(lblImagename.Text)));

                long s1 = (f.Length / 1024);
                if (s1 > 5120)
                {
                    MessageBox.Show("File size should not be greater than 5 MB", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


            }
            else
            {
                Finalbase64 = "";
            }



            try
            {
                DataSet dsupdateSignacture = new DataSet();
                string s = "exec Sp_updateSignature '" + Finalbase64 + "','" + txtsignature.Text + "'";

                #region Push Data to Cloud
                try
                {
                    rsWebRequest objRequest = new rsWebRequest();
                    wsEmail_SettingResponse objResponse = new wsEmail_SettingResponse();
                    wsEmail_Setting businessObject = new wsEmail_Setting()
                    {
                        CompanyLogo = Finalbase64,
                        Signature = txtsignature.Text,
                        ClientKey = new DalBase().GetClientKey()
                    };

                    objRequest.postData = JsonConvert.SerializeObject(businessObject);
                    objRequest.webURIServiceName = "EmailSetting/wsSyncCompanyLogoSetting";
                    objResponse = JsonConvert.DeserializeObject<wsEmail_SettingResponse>(objRequest.getWebResponse());

                    if (!objResponse.Success)
                    {
                        MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }

                }
                catch (Exception Ex)
                {

                    throw;
                }
                #endregion Push Data to Cloud


                dsupdateSignacture = dal.GetData(s, Application.StartupPath);
                if (dsupdateSignacture.Tables[0].Rows[0]["Result"].ToString() == "1")
                {

                    MessageBox.Show("Upload Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Error Occur(s) During Save.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            Cursor.Current = Cursors.Default;


        }

        public void Resize_Picture(string Org, string Des, int FinalWidth, int FinalHeight, int ImageQuality)
        {
            System.Drawing.Bitmap NewBMP;
            System.Drawing.Graphics graphicTemp;
            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(Org);

            int iWidth;
            int iHeight;
            if ((FinalHeight == 0) && (FinalWidth != 0))
            {
                iWidth = FinalWidth;
                iHeight = (bmp.Size.Height * iWidth / bmp.Size.Width);
            }
            else if ((FinalHeight != 0) && (FinalWidth == 0))
            {
                iHeight = FinalHeight;
                iWidth = (bmp.Size.Width * iHeight / bmp.Size.Height);
            }
            else
            {
                iWidth = FinalWidth;
                iHeight = FinalHeight;
            }

            NewBMP = new System.Drawing.Bitmap(iWidth, iHeight);
            graphicTemp = System.Drawing.Graphics.FromImage(NewBMP);
            graphicTemp.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;
            graphicTemp.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphicTemp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphicTemp.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphicTemp.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            graphicTemp.DrawImage(bmp, 0, 0, iWidth, iHeight);
            graphicTemp.Dispose();
            System.Drawing.Imaging.EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters();
            System.Drawing.Imaging.EncoderParameter encoderParam = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, ImageQuality);
            encoderParams.Param[0] = encoderParam;
            System.Drawing.Imaging.ImageCodecInfo[] arrayICI = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
            for (int fwd = 0; fwd <= arrayICI.Length - 1; fwd++)
            {
                if (arrayICI[fwd].FormatDescription.Equals("JPEG"))
                {
                    NewBMP.Save(Des, arrayICI[fwd], encoderParams);
                }
            }

            NewBMP.Dispose();
            bmp.Dispose();
        }

        public string ImageToBase64(string ImagePath)
        {
            string path = ImagePath;
            using (System.Drawing.Image image = System.Drawing.Image.FromFile(path))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }

        public static string AutoSearchComplete = string.Empty;

        private void txtsearchValue_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.KeyCode == Keys.Enter)
            {

                string FinalEmpty = string.Empty;

                FinalEmpty = txtsearchValue.Text.Trim();

                if (FinalEmpty == "" || FinalEmpty == string.Empty)
                {
                    MessageBox.Show("Please Enter Keyword First.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {


                    Cursor.Current = Cursors.WaitCursor;

                    AutoSearchComplete = txtsearchValue.Text;
                    try
                    {
                        AddAppointment App = new AddAppointment();
                        App.txtSearch.Text = AutoSearchComplete;

                        App.ShowDialog();


                    }
                    catch
                    { }

                    txtsearchValue.Text = "";
                    Cursor.Current = Cursors.Default;
                }


            }

            AutoSearchComplete = string.Empty;

        }

        private void btnAddtime_Click(object sender, EventArgs e)
        {
            try
            {

                DataSet dsInsertTime = new DataSet();
                SqlParameter[] p = new SqlParameter[3];

                string Timing = string.Empty;
                Timing = txtTime.Text + " " + drptimeSlot.SelectedItem;

                p[0] = new SqlParameter("@Flag", "Insert");
                p[1] = new SqlParameter("@TimeSlot", Timing);
                p[2] = new SqlParameter("@TimeID", "");



                string s = "Exec Sp_InsertTimeSlot '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "'";
                dsInsertTime = dal.GetData(s, Application.StartupPath);
                if (dsInsertTime.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    MessageBox.Show("Timeslot Insert Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                    try
                    {
                        GetTimeSlot();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                }
                else if (dsInsertTime.Tables[0].Rows[0]["Result"].ToString() == "2")
                {
                    MessageBox.Show("Maximum No.8 Timeslot Save . Please Remove First !.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Error Occur(s) During Save.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void grdtimeSlot_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                RemoveTimeSlot(grdtimeSlot.Rows[e.RowIndex].Cells[1].Value.ToString());
            }
        }
        public void RemoveTimeSlot(string TimeslotID)
        {

            DialogResult dialogResult = MessageBox.Show("Are you sure you want to delete ?", "Synergy Notification System", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {

                DataSet dsremoveTimeSlot = new DataSet();
                string s = "exec Sp_InsertTimeSlot 'Update','','" + TimeslotID + "'";
                dsremoveTimeSlot = dal.GetData(s, Application.StartupPath);
                if (dsremoveTimeSlot.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    try
                    {
                        GetTimeSlot();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                }
                else
                {

                }
            }

        }

        private void txtTime_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        public void GetTimeSlotForCombo()
        {
            try
            {
                DataSet dsComboFill = new DataSet();
                string s = "exec Sp_GetTimeSlot";

                dsComboFill = dal.GetData(s, Application.StartupPath);


                if (dsComboFill != null && dsComboFill.Tables[0].Rows.Count > 0)
                {
                    drpEmailNotication.Items.Clear();
                    drpSMSNotication.Items.Clear();

                    for (int i = 0; i < dsComboFill.Tables[0].Rows.Count; i++)
                    {

                        drpEmailNotication.Items.Add(dsComboFill.Tables[0].Rows[i][2].ToString());
                        drpSMSNotication.Items.Add(dsComboFill.Tables[0].Rows[i][2].ToString());
                    }
                }

                drpEmailNotication.SelectedIndex = 0;
                drpSMSNotication.SelectedIndex = 0;
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }



        private void Savebtn_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            try
            {

                if (txtEmailSendTo.Text == "")
                {
                    MessageBox.Show("Please Enter Email", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmailSendTo.Focus();
                    return;
                }
                if (txtSMSSendTo.Text == "")
                {
                    MessageBox.Show("Please Enter Mobile Number", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtSMSSendTo.Focus();
                    return;
                }


                DataSet dsInsertNotifi = new DataSet();
                SqlParameter[] p = new SqlParameter[8];

                string EmailCan, EmailRe, EmailWhen, SMSCan, SMSRe, SMSWhen = string.Empty;

                //Email
                if (chkEmailCancel.Checked == true)
                {
                    EmailCan = "1";
                }
                else
                {
                    EmailCan = "0";
                }
                if (chkEmailReschedule.Checked == true)
                {
                    EmailRe = "1";
                }
                else
                {
                    EmailRe = "0";
                }
                if (chkEmailWhen.Checked == true)
                {
                    EmailWhen = drpEmailNotication.SelectedItem.ToString();
                }
                else
                {
                    EmailWhen = "";
                }
                //SMS
                if (ChkSMSCancel.Checked == true)
                {
                    SMSCan = "1";
                }
                else
                {
                    SMSCan = "0";
                }
                if (chkSMSReschedule.Checked == true)
                {
                    SMSRe = "1";
                }
                else
                {
                    SMSRe = "0";
                }
                if (chkSMSWhen.Checked == true)
                {
                    SMSWhen = drpSMSNotication.SelectedItem.ToString();
                }
                else
                {
                    SMSWhen = "";
                }



                p[0] = new SqlParameter("@EmailSend", txtEmailSendTo.Text);
                p[1] = new SqlParameter("@EmailCancel", EmailCan);
                p[2] = new SqlParameter("@EmailReschedule", EmailRe);
                p[3] = new SqlParameter("@EmailWhen", EmailWhen);
                p[4] = new SqlParameter("@SMSSend", txtSMSSendTo.Text);
                p[5] = new SqlParameter("@SMSCancel", SMSCan);
                p[6] = new SqlParameter("@SMSReschedule", SMSRe);
                p[7] = new SqlParameter("@SMSWhen", SMSWhen);


                #region Push Data to Cloud
                try
                {
                    rsWebRequest objRequest = new rsWebRequest();
                    wsLRS_NotificationSettingResponse objResponse = new wsLRS_NotificationSettingResponse();
                    wsLRS_NotificationSetting businessObject = new wsLRS_NotificationSetting()
                    {
                        EmailSend = txtEmailSendTo.Text,
                        EmailCancel = Convert.ToInt32(EmailCan),
                        EmailReschedule = Convert.ToInt32(EmailRe),
                        EmailWhen = EmailWhen,
                        SMSSend = txtSMSSendTo.Text,
                        SMSCancel = Convert.ToInt32(SMSCan),
                        SMSReschedule = Convert.ToInt32(SMSRe),
                        SMSWhen = SMSWhen,
                        Isactive = 1,
                        CreatedDatetime = DateTime.Now,
                        ModifiedDatetime = DateTime.Now,
                        ClientKey = new DalBase().GetClientKey()
                    };

                    objRequest.postData = JsonConvert.SerializeObject(businessObject);
                    objRequest.webURIServiceName = "NotificationSetting/wsSyncNotificationSetting";
                    objResponse = JsonConvert.DeserializeObject<wsLRS_NotificationSettingResponse>(objRequest.getWebResponse());

                    if (!objResponse.Success)
                    {
                        MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }

                }
                catch (Exception Ex)
                {

                    throw;
                }
                #endregion Push Data to Cloud

                string s = "Exec sp_InsertNotification '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "'";
                dsInsertNotifi = dal.GetData(s, Application.StartupPath);
                if (dsInsertNotifi.Tables[0].Rows[0]["Result"].ToString() == "1")
                {

                    MessageBox.Show("Notificaton Setting Updated Successfully.");
                }
                else
                {

                    MessageBox.Show("Erroe Occur(s) during Update.");
                }
            }
            catch
            {
            }

            Cursor.Current = Cursors.Default;
        }

        public void GetNotificationSettingFill()
        {
            try
            {
                DataSet dsGetFill = new DataSet();
                string s = "exec Sp_GetNotificationSettings";

                dsGetFill = dal.GetData(s, Application.StartupPath);
                if (dsGetFill != null && dsGetFill.Tables[0].Rows.Count > 0)
                {
                    txtEmailSendTo.Text = dsGetFill.Tables[0].Rows[0]["EmailSend"].ToString();
                    if (dsGetFill.Tables[0].Rows[0]["EmailCancel"].ToString() == "1")
                    {
                        chkEmailCancel.Checked = true;
                    }
                    else
                    {
                        chkEmailCancel.Checked = false;
                    }
                    if (dsGetFill.Tables[0].Rows[0]["EmailReschedule"].ToString() == "1")
                    {
                        chkEmailReschedule.Checked = true;
                    }
                    else
                    {
                        chkEmailReschedule.Checked = false;
                    }
                    if (dsGetFill.Tables[0].Rows[0]["EmailWhen"].ToString() != "")
                    {
                        chkEmailWhen.Checked = true;
                        drpEmailNotication.SelectedItem = dsGetFill.Tables[0].Rows[0]["EmailWhen"].ToString();


                    }
                    else
                    {
                        chkEmailWhen.Checked = false;
                    }

                    txtSMSSendTo.Text = dsGetFill.Tables[0].Rows[0]["SMSSend"].ToString();

                    if (dsGetFill.Tables[0].Rows[0]["SMSCancel"].ToString() == "1")
                    {
                        ChkSMSCancel.Checked = true;
                    }
                    else
                    {
                        ChkSMSCancel.Checked = false;
                    }
                    if (dsGetFill.Tables[0].Rows[0]["SMSReschedule"].ToString() == "1")
                    {
                        chkSMSReschedule.Checked = true;
                    }
                    else
                    {
                        chkSMSReschedule.Checked = false;
                    }

                    if (dsGetFill.Tables[0].Rows[0]["SMSWhen"].ToString() != "")
                    {
                        chkSMSWhen.Checked = true;

                        drpSMSNotication.SelectedItem = dsGetFill.Tables[0].Rows[0]["SMSWhen"].ToString();

                    }
                    else
                    {
                        chkSMSWhen.Checked = false;
                    }

                    Savebtn.Text = "Update Changes";



                }


            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void grdAgendadata_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
            //try
            //{
            //    Clean();
            //}
            //catch
            //{
            //}

            //if (e.ColumnIndex != 28)
            //{
            //    try
            //    {

            //        string TitleMain = grdAgendadata.Rows[e.RowIndex].Cells[5].Value.ToString();
            //        string[] values = TitleMain.Split(':');

            //        App_SetValueForTitle = values[0].ToString().Trim();
            //        App_SetValueForEventBody = values[1].ToString().Trim();

            //        TitleMain = grdAgendadata.Rows[e.RowIndex].Cells[28].Value.ToString();
            //        values = TitleMain.Split(':');

            //        App_SetValueForEventHTMLBody = values[1].ToString().Trim();
            //    }
            //    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            //    try
            //    {


            //        App_MainWhere = "MainPage";
            //        App_SetValueForLocation = grdAgendadata.Rows[e.RowIndex].Cells[8].Value.ToString();

            //        App_SetValueForTimestart = grdAgendadata.Rows[e.RowIndex].Cells[23].Value.ToString();
            //        App_SetValueForTimestartHour = grdAgendadata.Rows[e.RowIndex].Cells[14].Value.ToString();
            //        App_SetValueForTimestartMinute = grdAgendadata.Rows[e.RowIndex].Cells[15].Value.ToString();

            //        App_SetValueForTimeEnd = grdAgendadata.Rows[e.RowIndex].Cells[23].Value.ToString();
            //        App_SetValueForTimeEndHour = grdAgendadata.Rows[e.RowIndex].Cells[14].Value.ToString();
            //        App_SetValueForTimeEndMinute = grdAgendadata.Rows[e.RowIndex].Cells[15].Value.ToString();

            //        App_SetValueForFname = grdAgendadata.Rows[e.RowIndex].Cells[6].Value.ToString();
            //        App_SetValueForLname = grdAgendadata.Rows[e.RowIndex].Cells[7].Value.ToString();


            //        App_SetValueForFirmCode = grdAgendadata.Rows[e.RowIndex].Cells[2].Value.ToString();
            //        App_SetValueForEventNo = grdAgendadata.Rows[e.RowIndex].Cells[1].Value.ToString();

            //        App_SetValueForBtnsubmit = "Update Event";

            //        App_SMS = grdAgendadata.Rows[e.RowIndex].Cells[9].Value.ToString();
            //        APP_Call = grdAgendadata.Rows[e.RowIndex].Cells[10].Value.ToString();
            //        APP_Email = grdAgendadata.Rows[e.RowIndex].Cells[11].Value.ToString();

            //        App_PartiesCode = grdAgendadata.Rows[e.RowIndex].Cells[16].Value.ToString();

            //        App_SetValueForCaseNo = grdAgendadata.Rows[e.RowIndex].Cells[18].Value.ToString();

            //        App_WhenSMS = grdAgendadata.Rows[e.RowIndex].Cells[19].Value.ToString();
            //        App_WhenCALL = grdAgendadata.Rows[e.RowIndex].Cells[20].Value.ToString();
            //        App_WhenEMAIL = grdAgendadata.Rows[e.RowIndex].Cells[21].Value.ToString();
            //    }
            //    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            //    try
            //    {
            //        AddAppointment App = new AddAppointment();
            //        App.ShowDialog();
            //    }
            //    catch
            //    { }
            //}

            //Cursor.Current = Cursors.Default;
        }


        private void btnDateSearchLog_Click(object sender, EventArgs e)
        {
            if (txtTwilioFilterFrom.Text != "" && drpogFilter.SelectedItem.ToString() == "Specific Date")
            {

                try
                {
                    GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), txtTwilioFilterFrom.Text, "", "");
                }
                catch
                {
                }
            }
            if (txtTwilioFilterFrom.Text != "" && txtTwilioFilterTo.Text != "" && drpogFilter.SelectedItem.ToString() == "Custom Range")
            {
                DateTime fromdate = DateTime.Parse(Convert.ToDateTime(txtTwilioFilterFrom.Value.Date).ToShortDateString());
                DateTime todate = DateTime.Parse(Convert.ToDateTime(txtTwilioFilterTo.Value.Date).ToShortDateString());

                if (fromdate > todate)
                {

                    try
                    {
                        grdTwilioLog.Columns.Clear();
                        grdTwilioLog.DataSource = null;

                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                    MessageBox.Show("from date is greater than to date");

                }
                else
                {


                    try
                    {
                        GetTwilioFilterDetails(drpogFilter.SelectedItem.ToString(), txtTwilioFilterFrom.Text, txtTwilioFilterTo.Text, "");
                    }
                    catch
                    {
                    }
                }
            }
        }

        public void GetSigna()
        {

            DataSet dsSign = new DataSet();
            string s = "exec Sp_GetSigAndLogo";

            dsSign = dal.GetData(s, Application.StartupPath);

            if (dsSign != null && dsSign.Tables != null && dsSign.Tables[0].Rows.Count > 0)
            {
                txtsignature.Text = dsSign.Tables[0].Rows[0]["Signature"].ToString();
                try
                {
                    Base64ToImage(dsSign.Tables[0].Rows[0]["CompanyLogo"].ToString());
                }
                catch
                { }

                var FilePathCompany = Application.StartupPath + "\\Download\\" + "Company.jpg";
                if (dsSign.Tables[0].Rows[0]["CompanyLogo"].ToString() == "")
                {
                    FilePathCompany = "";
                }
                else
                {
                    pblogo.ImageLocation = FilePathCompany;
                    txtCompanyLogo.Text = FilePathCompany;
                    lblImagename.Text = Path.GetFileName(FilePathCompany);

                }
                pblogo.Visible = true;
                lblPriviewLogo.Visible = true;
                btnRemoveLogo.Visible = true;


            }

        }
        public Image Base64ToImage(string base64)
        {


            byte[] imageBytes = Convert.FromBase64String(base64);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image CompanyImage = System.Drawing.Image.FromStream(ms, true);

            string FilePath = Application.StartupPath + "\\Download\\" + "Company.jpg";

            string FilePathDirectory = Application.StartupPath + "\\Download";

            try
            {
                DirectoryInfo di = new DirectoryInfo(FilePath);

                if (Directory.Exists(FilePath))
                {

                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                }
                else
                {
                    if (Directory.Exists(FilePathDirectory) == false)
                    {
                        Directory.CreateDirectory(FilePathDirectory);
                    }

                }


            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            CompanyImage.Save(FilePath);

            return CompanyImage;
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnserchfirst_Click(object sender, EventArgs e)
        {

            if (strtdateTimePicker.Text != "" && Datecombo.SelectedItem.ToString() == "Specific Date")
            {
                strtdateTimePicker.Format = DateTimePickerFormat.Custom;
                strtdateTimePicker.CustomFormat = Helper.UniversalDateFormat;

                enddateTimePicker.Text = "";
                try
                {
                    GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), strtdateTimePicker.Text, "", "");
                }
                catch
                {
                }
            }

            if (strtdateTimePicker.Text != "" && enddateTimePicker.Text != "" && Datecombo.SelectedItem.ToString() == "Custom Range")
            {

                DateTime fromdate = DateTime.Parse(Convert.ToDateTime(strtdateTimePicker.Value.Date).ToShortDateString());
                DateTime todate = DateTime.Parse(Convert.ToDateTime(enddateTimePicker.Value.Date).ToShortDateString());

                if (fromdate > todate)
                {
                    try
                    {
                        grdAgendadata.DataSource = null;
                        grdAgendadata.Columns.Clear();

                    }
                    catch
                    { }
                    MessageBox.Show("from date is greater than to date");


                }
                else
                {


                    enddateTimePicker.Format = DateTimePickerFormat.Custom;
                    enddateTimePicker.CustomFormat = Helper.UniversalDateFormat;

                    try
                    {
                        GetA1LawBasicDetails(Datecombo.SelectedItem.ToString(), strtdateTimePicker.Text, enddateTimePicker.Text, "");
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void searchtxt_KeyUp(object sender, KeyEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;


            if (e.KeyCode == Keys.Enter)
            {
                string FinalEmptyCheck = string.Empty;

                FinalEmptyCheck = searchtxt.Text.Trim();

                if (FinalEmptyCheck == "" || FinalEmptyCheck == string.Empty)
                {
                    MessageBox.Show("Please Enter Keyword First.");
                }
                else
                {
                    try
                    {
                        GetContact();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                }



            }
            Cursor.Current = Cursors.Default;
        }

        private void txtTwilioLogsearch_KeyUp(object sender, KeyEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;


            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    TwilioRefreshGrid();
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            }
            Cursor.Current = Cursors.Default;
        }

        private void btnpasswordChange_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtOldpassword.Text != "" && txtNewpassword.Text != "" && txtConfirmPassword.Text != "")
                {
                    if (txtNewpassword.Text == txtConfirmPassword.Text)
                    {

                        DataSet dsChangePassword = new DataSet();
                        string s = "exec Sp_Change_Password '" + txtOldpassword.Text.Trim() + "','" + txtNewpassword.Text + "'";

                        dsChangePassword = dal.GetData(s, Application.StartupPath);
                        if (dsChangePassword.Tables[0].Rows[0]["Result"].ToString() == "1")
                        {
                            MessageBox.Show("Password update Successfully.");

                            txtOldpassword.Text = "";
                            txtNewpassword.Text = "";
                            txtConfirmPassword.Text = "";
                        }
                        else if (dsChangePassword.Tables[0].Rows[0]["Result"].ToString() == "2")
                        {
                            MessageBox.Show("Old Password is Incorrect.");

                        }
                    }
                    else
                    {
                        MessageBox.Show("Confirm Password Mismatch");
                    }
                }
                else
                {
                    MessageBox.Show("Please Enter Valid Details.");

                }
            }
            catch
            { }
        }

        private void btnRemoveLogo_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to Remove ?", "Synergy Notification System", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                string FilePath = Application.StartupPath + "\\Upload\\" + "Company.jpg";



                try
                {
                    DirectoryInfo di = new DirectoryInfo(FilePath);

                    if (File.Exists(FilePath))
                    {
                        File.Delete(FilePath);
                    }
                    else
                    {
                    }



                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                lblImagename.Text = "";
                txtCompanyLogo.Text = "";
                pblogo.ImageLocation = "";

                pblogo.Visible = false;
                lblPriviewLogo.Visible = false;
                btnRemoveLogo.Visible = false;

            }
        }


        private void pbhelp_Click(object sender, EventArgs e)
        {
            try
            {

                Process.Start("http://tcconlineservices.com:81/helptext/index.html");


            }
            catch
            { }
        }
        private void WriteFile(byte[] byteServerFile, string _strFilePath)
        {
            FileStream fsServerFile = new FileStream(_strFilePath, FileMode.Create);
            fsServerFile.Write(byteServerFile, 0, byteServerFile.Length);
            fsServerFile.Close();
            fsServerFile = null;


            MessageBox.Show("File downloaded successfully.");
        }

        private void InstallFile(string strPath)
        {
            Process pInstall = new Process();
            pInstall.StartInfo.FileName = strPath;
            pInstall.Start();
        }

        private void Calendartab_Leave(object sender, EventArgs e)
        {

        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        public void CloudSyncReminderDelete(string ClientKey, string EventID)
        {
            try
            {
                var postData = string.Empty;


                //var request = (HttpWebRequest)WebRequest.Create("http://62.151.183.51:83/Reminder/wsDeleteReminder");

                //var request = (HttpWebRequest)WebRequest.Create("http://50.21.182.203:83/Reminder/wsDeleteReminder");



                var request = (HttpWebRequest)WebRequest.Create(ConfigReader.GetCloudServiceHost() + "Reminder/wsDeleteReminder");


                request.ContentType = "application/json";
                request.Method = "POST";






                postData += "{";
                postData += "'ClientKey':'" + ClientKey + "',";
                postData += "'MainEventID':'" + EventID + "'";
                postData += "}";


                var data = Encoding.ASCII.GetBytes(postData);



                request.ContentLength = data.Length;


                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }

        private void btnsync_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;



            //if (clsGlobalDeclaration.ClientKey.ToString() == "83B7-2CE8-95DF-4F46-845C" || clsGlobalDeclaration.ClientKey.ToString() == "C73F-DDDA-BB96-4ACF-BA6D" || clsGlobalDeclaration.ClientKey.ToString() == "A819-0B08-CBA2-4BEE-B199" || clsGlobalDeclaration.ClientKey.ToString() == "618E-BD10-63A5-4042-BA19" || clsGlobalDeclaration.ClientKey.ToString() == "99D3-14B0-5C0D-4E2B-92F0" || clsGlobalDeclaration.ClientKey.ToString() == "CD72-5768-75FD-4884-86CF") /* GBLAW : Two way Sync process Work  */
            //{



            //    try
            //    {
            //        Autopushcloud();
            //    }
            //    catch
            //    { }

            //    try
            //    {
            //        Cloud_TwilioStatus_TwowaySync();
            //    }
            //    catch
            //    {

            //    }
            //}
            if (clsGlobalDeclaration.ClientKey.ToString() == "931E-6EF7-E047-42C5-9B53")  /* API : Only CSV upload Functionality Work  */
            {


                var date = DateTime.Now;
                if (Convert.ToInt32(date.Hour) > 9 && Convert.ToInt32(date.Hour) < 17)
                {
                    try
                    {
                        GetPathForAPI();
                    }
                    catch
                    {

                    }
                }
                else
                {
                    if (MessageBox.Show("You are trying to send this message outside of 9:00 am to 5:00 pm window. This will result in immediate calls to clients. Are you sure you want to continue ?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        try
                        {
                            GetPathForAPI();
                        }
                        catch
                        {

                        }
                    }
                    else
                    {

                    }


                }
            }
            else // for other server 
            {
                try
                {
                    Autopushcloud();
                }
                catch
                { }

                try
                {
                    Cloud_TwilioStatus_TwowaySync();
                }
                catch
                {

                }
            }

            Cursor.Current = Cursors.Default;
        }

        public void Cloud_TwilioStatus()
        {
            #region Push Data to Cloud
            try
            {
                System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                dateInfo.ShortDatePattern = Helper.UniversalDateFormat;

                rsWebRequest objRequest = new rsWebRequest();
                wsReminderStatusResponse objResponse = new wsReminderStatusResponse();

                objRequest.postData = "{'ClientKey':'" + clsGlobalDeclaration.ClientKey + "'}";
                objRequest.webURIServiceName = "Reminder/wsReminderStatus";
                objResponse = JsonConvert.DeserializeObject<wsReminderStatusResponse>(objRequest.getWebResponse());

                if (!objResponse.Success)
                {
                    MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
                else
                {

                    DataTable dtTwilioBulkUpdate = new DataTable();
                    dtTwilioBulkUpdate.Columns.AddRange(new DataColumn[7] { new DataColumn("ClientKey", typeof(string)),
                    new DataColumn("MainEventID", typeof(string)),
                    new DataColumn("EventID",typeof(string)),
                    new DataColumn("IsTwilioStatus",typeof(string)),
                    new DataColumn("SID",typeof(string)),
                    new DataColumn("ConfirmDatetime",typeof(string)),
                    new DataColumn("Confirmtime",typeof(string))}
                );

                    foreach (wsReminderStatusDetails vStatus in objResponse.ResponseDetail)
                    {
                        string SID = string.Empty;
                        if (vStatus.SID == null)
                        {
                            SID = "";
                        }
                        else
                        {
                            SID = vStatus.SID.ToString();
                        }

                        try
                        {

                            dtTwilioBulkUpdate.Rows.Add(vStatus.ClientKey.ToString(),
                            vStatus.MainEventID.ToString(),
                            vStatus.EventID.ToString(),
                            vStatus.IsTwilioStatus.ToString(),
                            SID,
                            SQLFix(vStatus.ConfirmDatetime.ToString() == "" ? Convert.ToString(vStatus.ConfirmDatetime) : Convert.ToDateTime(vStatus.ConfirmDatetime, dateInfo).ToString(Helper.UniversalDateFormat)),
                            vStatus.Confirmtime);

                        }
                        catch
                        {
                        }





                    }

                    if (dtTwilioBulkUpdate != null)
                    {
                        DataSet dsTwilioStaus = new DataSet();
                        SqlParameter[] p = new SqlParameter[1];
                        p[0] = new SqlParameter("@tblBulkTwilioUpdate", dtTwilioBulkUpdate);
                        dsTwilioStaus = dal.GetData("[sproc_UpdateTwilioIVRStatus]", p, Application.StartupPath);
                    }






                }





            }
            catch (Exception Ex)
            {

                throw;
            }
            #endregion Push Data to Cloud

        }



        private void btnChangeTimeZone_Click(object sender, EventArgs e)
        {
            DataSet dsTimeZone = new DataSet();
            string s = "exec Insert_TimeZone '" + drpTimeZone.SelectedItem + "'";
            dsTimeZone = dal.GetData(s, Application.StartupPath);
            if (dsTimeZone.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                MessageBox.Show("TimeZone Update Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Error Occur(s) During Update.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void GetHoursBind()
        {
            DataSet dsTimeZone = new DataSet();
            dsTimeZone.Clear();
            string s = "exec Get_TimeZone '' ";
            dsTimeZone = dal.GetData(s, Application.StartupPath);
            if (dsTimeZone != null)
            {
                drpTimeZone.SelectedItem = dsTimeZone.Tables[0].Rows[0]["TimeHours"].ToString();

            }

        }

        private void grdTemplateList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 0)
            {
                str_TitleAppoint = grdTemplateList.Rows[e.RowIndex].Cells[2].Value.ToString();
                str_BodyAppoint = grdTemplateList.Rows[e.RowIndex].Cells[4].Value.ToString();
                str_ID = grdTemplateList.Rows[e.RowIndex].Cells[5].Value.ToString();
                str_AppointmentType = grdTemplateList.Rows[e.RowIndex].Cells[7].Value.ToString();
                str_TwilioSay = grdTemplateList.Rows[e.RowIndex].Cells[8].Value.ToString();


                str_SubmitAppoint = "Update";
                CreateAppointment CreateAppointment = new CreateAppointment();
                CreateAppointment.ShowDialog();
            }
        }

        private void btnEmailSettings_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (txtEmailID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please enter email id", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmailID.Focus();
                return;
            }

            try
            {
                var test = new MailAddress(txtEmailID.Text);
            }
            catch
            {
                MessageBox.Show("Please enter valid email id", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmailID.Focus();
                return;
            }

            if (txtEmailSubject.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please enter email subject", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEmailSubject.Focus();
                return;
            }


            try
            {
                UPdateEmailDetails();
            }
            catch
            {

            }

            Cursor.Current = Cursors.Default;
        }

        public void GetEmailDetails()
        {


            DataSet dsGetList = new DataSet();
            string strEmailDetails = "exec Sp_GetEmailSMSDetails";
            dsGetList = dal.GetData(strEmailDetails, Application.StartupPath);
            if (dsGetList != null && dsGetList.Tables != null && dsGetList.Tables[0].Rows.Count > 0)
            {

                txtEmailID.Text = dsGetList.Tables[0].Rows[0]["EmailFrom"].ToString().Trim();
                txtPassword.Text = dsGetList.Tables[0].Rows[0]["EmailPassword"].ToString().Trim();
                txtEmailSubject.Text = dsGetList.Tables[0].Rows[0]["EmailFromTitle"].ToString().Trim();
                txtSMTPhost.Text = dsGetList.Tables[0].Rows[0]["EmailSMTP"].ToString().Trim();
                txtSMTPPort.Text = dsGetList.Tables[0].Rows[0]["EmailPort"].ToString().Trim();
                txtSMSSenderNo.Text = dsGetList.Tables[0].Rows[0]["Twilio_SMS"].ToString().Trim();
                txtCallNo.Text = dsGetList.Tables[0].Rows[0]["Twilio_CALL"].ToString().Trim();
                txtAccountSID.Text = dsGetList.Tables[0].Rows[0]["Twilio_AccountSid"].ToString().Trim();
                txtAuthoToken.Text = dsGetList.Tables[0].Rows[0]["Twilio_AuthToken"].ToString().Trim();

                txtClientKey.Text = new DalBase().GetClientKey();
            }
            else
            {


            }
        }

        public void UPdateEmailDetails()
        {
            //DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Are you sure Update Details ?", "Update Confirmation", MessageBoxButtons.YesNo);

            //if (dialogResult == DialogResult.Yes || true)  // error is here
            if (true)  // error is here
            {
                DataSet dsGetList = new DataSet();

                SqlParameter[] p = new SqlParameter[9];

                p[0] = new SqlParameter("@EmailFrom", txtEmailID.Text);
                p[1] = new SqlParameter("@EmailPassword", txtPassword.Text);
                p[2] = new SqlParameter("@EmailFromTitle", txtEmailSubject.Text);
                p[3] = new SqlParameter("@EmailPort", txtSMTPPort.Text);
                p[4] = new SqlParameter("@EmailSMTP", txtSMTPhost.Text);
                p[5] = new SqlParameter("@Twilio_CALL", txtCallNo.Text);
                p[6] = new SqlParameter("@Twilio_SMS", txtSMSSenderNo.Text);
                p[7] = new SqlParameter("@Twilio_AccountSid", txtAccountSID.Text);
                p[8] = new SqlParameter("@Twilio_AuthToken", txtAuthoToken.Text);


                #region Push Data to Cloud
                try
                {
                    rsWebRequest objRequest = new rsWebRequest();
                    wsEmail_SettingResponse objResponse = new wsEmail_SettingResponse();
                    wsEmail_Setting businessObject = new wsEmail_Setting()
                    {
                        EmailFrom = txtEmailID.Text,
                        EmailPassword = txtPassword.Text,
                        EmailFromTitle = txtEmailSubject.Text,
                        EmailPort = txtSMTPPort.Text,
                        EmailSMTP = txtSMTPhost.Text,
                        Twilio_CALL = txtCallNo.Text,
                        Twilio_SMS = txtSMSSenderNo.Text,
                        Twilio_AccountSid = txtAccountSID.Text,
                        Twilio_AuthToken = txtAuthoToken.Text,
                        CompanyLogo = "",
                        Signature = "",
                        ClientKey = new DalBase().GetClientKey()
                    };

                    objRequest.postData = JsonConvert.SerializeObject(businessObject);
                    objRequest.webURIServiceName = "EmailSetting/wsSyncEmailSetting";
                    objResponse = JsonConvert.DeserializeObject<wsEmail_SettingResponse>(objRequest.getWebResponse());

                    if (!objResponse.Success)
                    {
                        MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }

                }
                catch (Exception Ex)
                {

                    throw;
                }
                #endregion Push Data to Cloud


                string s = "exec Sp_UpdateEmailSMSDetails '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";

                dsGetList = dal.GetData(s, Application.StartupPath);
                if (dsGetList != null && dsGetList.Tables != null && dsGetList.Tables[0].Rows.Count > 0)
                {
                    System.Windows.MessageBox.Show("Record Update Successfully .");
                    picEmailSend.Visible = false;
                }
                else
                {

                    picEmailSend.Visible = false;
                }
            }
            else
            {
                MessageBox.Show("Please Enter Details First.");
            }


        }

        private void btnsignin_Click(object sender, EventArgs e)
        {
            try
            {
                SiginSetting();
            }
            catch
            { }
        }
        public void SiginSetting()
        {

            if (txtpasswordSetting.Text != "" && txtusername.Text != "")
            {
                DataSet dsEmailsign = new DataSet();

                SqlParameter[] p = new SqlParameter[2];

                p[0] = new SqlParameter("@EmailFrom", txtusername.Text);
                p[1] = new SqlParameter("@EmailPassword", txtpasswordSetting.Text);



                string s = "exec Sp_CheckEmailLogin '" + p[0].Value + "','" + p[1].Value + "'";

                dsEmailsign = dal.GetData(s, Application.StartupPath);

                if (dsEmailsign.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    try
                    {
                        pnlPassword.Visible = false;
                        pnlSetting.Visible = false;

                        pnlSetting.Visible = true;
                        pnlSetting.Size = new Size(976, 550);
                        pnlSetting.Dock = DockStyle.Fill;
                        pnlSetting.Location = new Point(0, 0);
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                    Helper.AppLog.Info("Entered to the Setting section");

                    txtusername.Text = "";
                    txtpasswordSetting.Text = "";
                }

                else
                {

                    MessageBox.Show("Please Enter Valid Username & password.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {


                MessageBox.Show("Please Enter Username & Password.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void btncancel_Click(object sender, EventArgs e)
        {
            txtusername.Text = "";
            txtpasswordSetting.Text = "";

        }

        private void btnTestMail_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (txtEmailID.Text != "" && txtPassword.Text != "" && txtSMTPhost.Text != "" && txtSMTPPort.Text != "")
            {


                try
                {

                    MailMessage ms = new MailMessage();
                    ms.To.Add(txtEmailID.Text);
                    ms.From = new MailAddress(txtEmailID.Text, "LRS - Test Mail.");
                    ms.Subject = "LRS Test Mail";
                    ms.Body = "Test LRS Body:";
                    ms.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient(txtSMTPhost.Text, Convert.ToInt32(txtSMTPPort.Text));
                    smtp.Host = txtSMTPhost.Text;
                    smtp.Credentials = new System.Net.NetworkCredential(txtEmailID.Text, txtPassword.Text);
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = false;
                    smtp.EnableSsl = true;
                    smtp.Send(ms);
                    picEmailSend.Visible = true;
                    picEmailSend.Image = SynergyNotificationSystem.Properties.Resources.Emailsend;
                }
                catch
                {
                    picEmailSend.Visible = true;
                    picEmailSend.Image = SynergyNotificationSystem.Properties.Resources.EmailCancel;
                }
            }
            else
            {
                MessageBox.Show("Please Fill Details First..!");
            }

            Cursor.Current = Cursors.Default;
        }

        private void grdAgendadata_Paint(object sender, PaintEventArgs e)
        {
            DataGridView sndr = (DataGridView)sender;
            if (sndr.Rows.Count == 0)
            {
                using (Graphics grfx = e.Graphics)
                {
                    grfx.FillRectangle(Brushes.White, new Rectangle(new Point(410, 150), new Size(200, 25)));
                    grfx.DrawString("No record(s) found.", new Font("Microsoft Sans Serif", 10), Brushes.DarkGray, new PointF(410, 150));
                }
            }
        }

        private void Contactgridview_Paint(object sender, PaintEventArgs e)
        {
            DataGridView sndr = (DataGridView)sender;
            if (sndr.Rows.Count == 0)
            {
                using (Graphics grfx = e.Graphics)
                {
                    grfx.FillRectangle(Brushes.White, new Rectangle(new Point(410, 150), new Size(200, 25)));
                    grfx.DrawString("No record(s) found.", new Font("Microsoft Sans Serif", 10), Brushes.DarkGray, new PointF(410, 150));
                }
            }
        }

        private void grdTwilioLog_Paint(object sender, PaintEventArgs e)
        {
            DataGridView sndr = (DataGridView)sender;
            if (sndr.Rows.Count == 0)
            {
                using (Graphics grfx = e.Graphics)
                {
                    grfx.FillRectangle(Brushes.White, new Rectangle(new Point(410, 150), new Size(200, 25)));
                    grfx.DrawString("No record(s) found.", new Font("Microsoft Sans Serif", 10), Brushes.DarkGray, new PointF(410, 150));
                }
            }
        }

        private void grdTemplateList_Paint(object sender, PaintEventArgs e)
        {
            DataGridView sndr = (DataGridView)sender;
            if (sndr.Rows.Count == 0)
            {
                using (Graphics grfx = e.Graphics)
                {
                    grfx.FillRectangle(Brushes.White, new Rectangle(new Point(410, 150), new Size(200, 25)));
                    grfx.DrawString("No record(s) found.", new Font("Microsoft Sans Serif", 10), Brushes.DarkGray, new PointF(410, 150));
                }
            }
        }

        private void tmrAppointment_Tick(object sender, EventArgs e)
        {
            try
            {
                Cloud_TwilioStatus();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {
                RefreshGrid();
            }
            catch
            { }

            try { GetPanelColorCode(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void tmrTwiliolog_Tick(object sender, EventArgs e)
        {
            try
            {
                SynchTwilioLog();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {
                TwilioRefreshGrid();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            try
            {
                GetPanelColorLogCode();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        public void GetBulkType()
        {
            pnlbulkTiming.Visible = false;
            DataSet dsbulktype = new DataSet();
            string s = "EXEC GetListBulktype";
            dsbulktype = dal.GetData(s, Application.StartupPath);
            if (dsbulktype != null && dsbulktype.Tables[0].Rows.Count > 0)
            {
                drpBulkType.Items.Clear();
                for (int i = 0; i < dsbulktype.Tables[0].Rows.Count; i++)
                {
                    DrpBulkItemBind itembind = new DrpBulkItemBind();
                    itembind.Text = dsbulktype.Tables[0].Rows[i][1].ToString();
                    itembind.Value = dsbulktype.Tables[0].Rows[i][0].ToString();
                    drpBulkType.Items.Add(itembind);
                }

                drpBulkType.SelectedIndex = 0;
            }
            else
            {

            }
        }

        public class DrpBulkItemBind
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        public void GetTimingCheckbox()
        {
            DataSet dsgetA1DetailsTiming = new DataSet();
            string s = "exec GetSMStiming";
            dsgetA1DetailsTiming = dal.GetData(s, Application.StartupPath);
            if (dsgetA1DetailsTiming != null && dsgetA1DetailsTiming.Tables != null && dsgetA1DetailsTiming.Tables[0].Rows.Count > 0)
            {
                //SMS Grid Bind
                try
                {
                    grdapplyAll.Columns.Clear();
                    grdSMS.Columns.Clear();
                    grdCALL.Columns.Clear();
                    grdEMAIL.Columns.Clear();
                }
                catch
                { }

                try
                {
                    grdapplyAll.DataSource = dsgetA1DetailsTiming.Tables[0];

                    grdapplyAll.Columns[0].Visible = false;
                    grdapplyAll.Columns[1].Visible = false;
                    grdapplyAll.Columns[1].Width = 70;
                    grdapplyAll.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                    checkBoxColumn.HeaderText = "";
                    checkBoxColumn.Width = 30;
                    checkBoxColumn.Name = "";
                    checkBoxColumn.ValueType = typeof(bool);
                    grdapplyAll.Columns.Insert(0, checkBoxColumn);

                }
                catch
                { }

                try
                {
                    grdSMS.DataSource = dsgetA1DetailsTiming.Tables[0];
                    grdSMS.CurrentCell.Selected = false;
                    grdSMS.Columns[0].Visible = false;
                    grdSMS.Columns[1].Width = 70;
                    grdSMS.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                    checkBoxColumn.HeaderText = "";
                    checkBoxColumn.Width = 30;
                    checkBoxColumn.Name = "";
                    checkBoxColumn.ValueType = typeof(bool);
                    grdSMS.Columns.Insert(0, checkBoxColumn);

                }
                catch
                { }
                //CALL Grid Bind
                try
                {
                    grdCALL.DataSource = dsgetA1DetailsTiming.Tables[0];
                    grdCALL.CurrentCell.Selected = false;
                    grdCALL.Columns[0].Visible = false;
                    grdCALL.Columns[1].Width = 70;
                    grdCALL.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn1 = new DataGridViewCheckBoxColumn();
                    checkBoxColumn1.HeaderText = "";
                    checkBoxColumn1.Width = 30;
                    checkBoxColumn1.Name = "";
                    checkBoxColumn1.ValueType = typeof(bool);
                    grdCALL.Columns.Insert(0, checkBoxColumn1);
                }
                catch
                { }
                //EMAIL Grid Bind

                try
                {
                    grdEMAIL.DataSource = dsgetA1DetailsTiming.Tables[0];
                    grdEMAIL.CurrentCell.Selected = false;
                    grdEMAIL.Columns[0].Visible = false;
                    grdEMAIL.Columns[1].Width = 70;
                    grdEMAIL.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn2 = new DataGridViewCheckBoxColumn();
                    checkBoxColumn2.HeaderText = "";
                    checkBoxColumn2.Width = 30;
                    checkBoxColumn2.Name = "";
                    checkBoxColumn2.ValueType = typeof(bool);
                    grdEMAIL.Columns.Insert(0, checkBoxColumn2);
                }
                catch
                { }
            }
            else
            {

            }
        }

        private void grdapplyAll_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {

                DataGridViewCheckBoxCell checkbox = (DataGridViewCheckBoxCell)grdapplyAll.CurrentCell;
                try
                {
                    bool isChecked = (bool)checkbox.EditedFormattedValue;

                    if (isChecked == true)
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = true;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = true;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = true;
                    }
                    else
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = false;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = false;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = false;
                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            }
        }

        private void grdapplyAll_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                Cursor.Current = Cursors.WaitCursor;

                DataGridViewCheckBoxCell checkbox = (DataGridViewCheckBoxCell)grdapplyAll.CurrentCell;


                try
                {
                    bool isChecked = (bool)checkbox.EditedFormattedValue;

                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    if (isChecked == true)
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = true;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = true;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = true;
                    }
                    else
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = false;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = false;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = false;
                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                Cursor.Current = Cursors.Default;

            }
        }

        private void grdSMS_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {

                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;

                    if (isSMS == true)
                    {
                        if (isCALL == true && isEMAIL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }
                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                try //SMS Values GET
                {

                    List<DataGridViewRow> selectedSMS = (from row in grdSMS.Rows.Cast<DataGridViewRow>()
                                                         where Convert.ToBoolean(row.Cells[0].Value) == true
                                                         select row).ToList();


                    if (selectedSMS.Count > 0)
                    {
                        chkSMS.Checked = true;
                    }
                    else
                    {
                        chkSMS.Checked = false;
                    }
                }
                catch
                {

                }

            }

        }

        private void grdCALL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {


                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;


                    if (isCALL == true)
                    {
                        if (isSMS == true && isEMAIL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }


                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                try
                {

                    List<DataGridViewRow> selectedCALL = (from row in grdCALL.Rows.Cast<DataGridViewRow>()
                                                          where Convert.ToBoolean(row.Cells[0].Value) == true
                                                          select row).ToList();


                    if (selectedCALL.Count > 0)
                    {
                        chkCALL.Checked = true;
                    }
                    else
                    {
                        chkCALL.Checked = false;
                    }


                }
                catch
                {

                }
            }
        }

        private void grdEMAIL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {


                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;


                    if (isEMAIL == true)
                    {
                        if (isSMS == true && isCALL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }
                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                try
                {

                    List<DataGridViewRow> selectedEMAIL = (from row in grdEMAIL.Rows.Cast<DataGridViewRow>()
                                                           where Convert.ToBoolean(row.Cells[0].Value) == true
                                                           select row).ToList();


                    if (selectedEMAIL.Count > 0)
                    {
                        chkEMAIL.Checked = true;
                    }
                    else
                    {
                        chkEMAIL.Checked = false;
                    }


                }
                catch
                {

                }
            }

        }

        private void grdSMS_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in this.grdSMS.Rows)
                {
                    if (((bool)grdSMS.Rows[e.RowIndex].Cells[0].Value))
                    {
                        chkSMS.Checked = true;
                    }

                }
            }
            catch
            { }


        }

        private void grdSMS_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {

                if (grdSMS.IsCurrentCellDirty)
                {
                    grdSMS.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            { }
        }

        private void grdCALL_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in this.grdCALL.Rows)
                {
                    if (((bool)grdCALL.Rows[e.RowIndex].Cells[0].Value))
                    {
                        chkCALL.Checked = true;
                    }

                }
            }
            catch
            { }
        }

        private void grdCALL_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {

                if (grdCALL.IsCurrentCellDirty)
                {
                    grdCALL.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            { }
        }

        private void grdEMAIL_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in this.grdEMAIL.Rows)
                {
                    if (((bool)grdEMAIL.Rows[e.RowIndex].Cells[0].Value))
                    {
                        chkEMAIL.Checked = true;
                    }

                }
            }
            catch
            { }
        }

        private void grdEMAIL_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {

                if (grdEMAIL.IsCurrentCellDirty)
                {
                    grdEMAIL.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch
            { }
        }

        private void grdSMS_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {

                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;

                    if (isSMS == true)
                    {
                        if (isCALL == true && isEMAIL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }
                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                try //SMS Values GET
                {

                    List<DataGridViewRow> selectedSMS = (from row in grdSMS.Rows.Cast<DataGridViewRow>()
                                                         where Convert.ToBoolean(row.Cells[0].Value) == true
                                                         select row).ToList();


                    if (selectedSMS.Count > 0)
                    {
                        chkSMS.Checked = true;
                    }
                    else
                    {
                        chkSMS.Checked = false;
                    }


                }
                catch
                {

                }

            }
        }

        private void grdCALL_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {


                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;


                    if (isCALL == true)
                    {
                        if (isSMS == true && isEMAIL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }


                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                try
                {

                    List<DataGridViewRow> selectedCALL = (from row in grdCALL.Rows.Cast<DataGridViewRow>()
                                                          where Convert.ToBoolean(row.Cells[0].Value) == true
                                                          select row).ToList();


                    if (selectedCALL.Count > 0)
                    {
                        chkCALL.Checked = true;
                    }
                    else
                    {
                        chkCALL.Checked = false;
                    }


                }
                catch
                {

                }
            }
        }

        private void grdEMAIL_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {


                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;


                    if (isEMAIL == true)
                    {
                        if (isSMS == true && isCALL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }
                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                try
                {

                    List<DataGridViewRow> selectedEMAIL = (from row in grdEMAIL.Rows.Cast<DataGridViewRow>()
                                                           where Convert.ToBoolean(row.Cells[0].Value) == true
                                                           select row).ToList();


                    if (selectedEMAIL.Count > 0)
                    {
                        chkEMAIL.Checked = true;
                    }
                    else
                    {
                        chkEMAIL.Checked = false;
                    }


                }
                catch
                {

                }
            }
        }

        private void drpBulkType_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {

                pnlbulkTiming.Visible = true;

                chkSMS.Checked = false;
                chkCALL.Checked = false;
                chkEMAIL.Checked = false;

                try
                {

                    GetTimingCheckbox();
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                try
                {

                    GetUpdateBulkReminder();
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }
            catch
            {

            }
        }

        public void GetAllTimingWithComma()
        {

            SMSTiming = string.Empty;
            CALLTiming = string.Empty;
            EMailTiming = string.Empty;


            try //SMS Values GET
            {

                List<DataGridViewRow> selectedRows = (from row in grdSMS.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[0].Value) == true
                                                      select row).ToList();
                string StrSMS = string.Empty;

                foreach (DataGridViewRow row in selectedRows)
                {
                    StrSMS += row.Cells[1].Value.ToString() + ",";
                    SMSTiming = StrSMS;
                    SMSTiming = SMSTiming.Remove(SMSTiming.Length - 1);

                }
            }
            catch
            {

            }
            try //CALL Values GET
            {

                List<DataGridViewRow> selectedRows = (from row in grdCALL.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[0].Value) == true
                                                      select row).ToList();
                string StrCALL = string.Empty;

                foreach (DataGridViewRow row in selectedRows)
                {
                    StrCALL += row.Cells[1].Value.ToString() + ",";
                    CALLTiming = StrCALL;
                    CALLTiming = CALLTiming.Remove(CALLTiming.Length - 1);

                }
            }
            catch
            {

            }
            try //EMAIL Values GET
            {

                List<DataGridViewRow> selectedRows = (from row in grdEMAIL.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[0].Value) == true
                                                      select row).ToList();
                string StrEMAIL = string.Empty;

                foreach (DataGridViewRow row in selectedRows)
                {
                    StrEMAIL += row.Cells[1].Value.ToString() + ",";
                    EMailTiming = StrEMAIL;
                    EMailTiming = EMailTiming.Remove(EMailTiming.Length - 1);

                }
            }
            catch
            {

            }

        }
        string SMSTiming, CALLTiming, EMailTiming = string.Empty;
        private void btnUpdateBulk_Click(object sender, EventArgs e)
        {
            int SMS, CALL, EMAIL, IsPrivate = 0;

            if (chkSMS.Checked == true)
            {
                SMS = 1;
            }
            else
            {
                SMS = 0;
            }
            if (chkCALL.Checked == true)
            {
                CALL = 1;
            }
            else
            {
                CALL = 0;
            }
            if (chkEMAIL.Checked == true)
            {
                EMAIL = 1;
            }
            else
            {
                EMAIL = 0;
            }

            try
            {
                GetAllTimingWithComma();
            }
            catch
            { }

            DataSet dsInsertBulkAppointment = new DataSet();
            SqlParameter[] p = new SqlParameter[7];
            DrpBulkItemBind itembind = new DrpBulkItemBind();

            p[0] = new SqlParameter("@SMS", SMS);
            p[1] = new SqlParameter("@CALL", CALL);
            p[2] = new SqlParameter("@EMAIL", EMAIL);
            p[3] = new SqlParameter("@SMSTiming", SMSTiming);
            p[4] = new SqlParameter("@CALLTiming", CALLTiming);
            p[5] = new SqlParameter("@EMAILTiming", EMailTiming);
            p[6] = new SqlParameter("@TID", (drpBulkType.SelectedItem as DrpBulkItemBind).Value.ToString());

            string s = "Exec InsertBulkTiming '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "'";
            dsInsertBulkAppointment = dal.GetData(s, Application.StartupPath);
            if (dsInsertBulkAppointment.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                MessageBox.Show("Update Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Error occur(s) during Update.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }


        }


        public void GetUpdateBulkReminder()
        {
            DrpBulkItemBind itembind = new DrpBulkItemBind();


            DataSet dsbulktypeForUpdate = new DataSet();
            string s = "EXEC GetBulkTimingAsID '" + (drpBulkType.SelectedItem as DrpBulkItemBind).Value.ToString() + "'";
            dsbulktypeForUpdate = dal.GetData(s, Application.StartupPath);
            if (dsbulktypeForUpdate != null && dsbulktypeForUpdate.Tables[0].Rows.Count > 0)
            {
                string SMSUpdate, CALLUpdate, EMAILUpdate = string.Empty;



                SMSUpdate = dsbulktypeForUpdate.Tables[0].Rows[0]["SMSWhen"].ToString();
                CALLUpdate = dsbulktypeForUpdate.Tables[0].Rows[0]["CALLWhen"].ToString();
                EMAILUpdate = dsbulktypeForUpdate.Tables[0].Rows[0]["EMAILWhen"].ToString();


                if (dsbulktypeForUpdate.Tables[0].Rows[0]["SMS"].ToString() == "1")
                {
                    chkSMS.Checked = true;
                }
                else
                {
                    chkSMS.Checked = false;
                }

                if (dsbulktypeForUpdate.Tables[0].Rows[0]["CALL"].ToString() == "1")
                {
                    chkCALL.Checked = true;
                }
                else
                {
                    chkCALL.Checked = false;
                }
                if (dsbulktypeForUpdate.Tables[0].Rows[0]["EMAIL"].ToString() == "1")
                {
                    chkEMAIL.Checked = true;
                }
                else
                {
                    chkEMAIL.Checked = false;
                }



                try
                {

                    if (SMSUpdate != "")
                    {
                        string[] SMS = SMSUpdate.Split(',');

                        foreach (string word in SMS)
                        {

                            foreach (DataGridViewRow row in grdSMS.Rows)
                            {
                                if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word))
                                {
                                    row.Cells[0].Value = true;
                                }


                            }
                        }

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                try
                {

                    if (CALLUpdate != "")
                    {
                        string[] CALL = CALLUpdate.Split(',');

                        foreach (string word1 in CALL)
                        {

                            foreach (DataGridViewRow row in grdCALL.Rows)
                            {
                                if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word1))
                                {
                                    row.Cells[0].Value = true;
                                }


                            }
                        }

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                try
                {

                    if (EMAILUpdate != "")
                    {
                        string[] CALL = EMAILUpdate.Split(',');

                        foreach (string word2 in CALL)
                        {

                            foreach (DataGridViewRow row in grdEMAIL.Rows)
                            {
                                if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word2))
                                {
                                    row.Cells[0].Value = true;
                                }


                            }
                        }

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            }
            else
            {

            }

        }

        private void chkprivateFilter_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                RefreshGrid();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void txtgridSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {


                try
                {
                    RefreshGrid();
                }
                catch
                { }

            }
            else if (txtgridSearch.Text == "")
            {
                try
                {
                    RefreshGrid();
                }
                catch
                { }
            }
        }

        public void GetSystemUserName(string UserMACID)
        {
            try
            {
                DataSet dsUserMACID = new DataSet();
                string s = "exec GetsystemUserName '" + UserMACID + "'";

                dsUserMACID = dal.GetData(s, Application.StartupPath);

                if (dsUserMACID != null && dsUserMACID.Tables != null && dsUserMACID.Tables[0].Rows.Count > 0)
                {
                    txtgridSearch.Text = dsUserMACID.Tables[0].Rows[0]["Name"].ToString();
                }
                else
                {
                    txtgridSearch.Text = "";
                }
            }
            catch
            {

            }
        }



        public void GetRegistrationDetails()
        {
            try
            {
                lblRegistrationMac.Text = dal.GetClientMAC();



                DataSet dsgetRegDetails = new DataSet();
                string s = "exec sproc_UpdateRegistrationDetails 'GET','','','','" + lblRegistrationMac.Text + "'";
                dsgetRegDetails = dal.GetData(s, Application.StartupPath);
                if (dsgetRegDetails != null && dsgetRegDetails.Tables != null && dsgetRegDetails.Tables[0].Rows.Count > 0)
                {


                    txtCompanyName.Text = dsgetRegDetails.Tables[0].Rows[0]["CompanyName"].ToString();
                    txtMobileNo.Text = dsgetRegDetails.Tables[0].Rows[0]["Mobile"].ToString();
                    txtEmail.Text = dsgetRegDetails.Tables[0].Rows[0]["Email"].ToString();
                    txtproductkey.Text = dsgetRegDetails.Tables[0].Rows[0]["ProductKey"].ToString();
                }
                else
                {

                }
            }
            catch
            {

            }
        }

        private void txtpasswordSetting_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    SiginSetting();
                }
                catch
                { }
            }
        }
        public class RegistrationResponse
        {

            public RegistrationResponse() { }

            private bool _success;

            public bool Success
            {
                get { return _success; }
                set { _success = value; }
            }

            private string _message;

            public string Message
            {
                get { return _message; }
                set { _message = value; }
            }

        }
        private void btnregistrationUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                DataSet dsUpdateReg = new DataSet();
                SqlParameter[] p = new SqlParameter[5];

                p[0] = new SqlParameter("@Flag", "Update");
                p[1] = new SqlParameter("@CompanyName", txtCompanyName.Text);
                p[2] = new SqlParameter("@Mobile", txtMobileNo.Text);
                p[3] = new SqlParameter("@Email", txtEmail.Text);
                p[4] = new SqlParameter("@MACID", lblRegistrationMac.Text);


                string s = "Exec sproc_UpdateRegistrationDetails '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "'";
                dsUpdateReg = dal.GetData(s, Application.StartupPath);
                if (dsUpdateReg.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    MessageBox.Show("Registration Details Update Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);



                    try
                    {
                        ApplicationUpgrade objRequest = new ApplicationUpgrade();
                        RegistrationResponse objResponse = new RegistrationResponse();
                        UpdateRegistrationDetails businessObject = new UpdateRegistrationDetails()
                        {
                            CompanyName = txtCompanyName.Text,
                            Mobile = txtMobileNo.Text,
                            Email = txtEmail.Text,
                            MacID = lblRegistrationMac.Text


                        };

                        objRequest.postData = JsonConvert.SerializeObject(businessObject);
                        objRequest.webURIServiceName = "UpdateRegistrationDetails";
                        objResponse = JsonConvert.DeserializeObject<RegistrationResponse>(objRequest.getWebResponse());

                        if (!objResponse.Success)
                        {
                            MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Cursor.Current = Cursors.Default;
                            return;
                        }

                    }
                    catch (Exception Ex)
                    {
                        throw;
                    }



                }

                else
                {
                    MessageBox.Show("Error Occur(s) During Update.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        public static string Cont_FirmCode, Cont_FirstName, Cont_LastName, Cont_CaseNo, Cont_Flag = string.Empty;
        private void Contactgridview_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex != 0)
            {
                Cursor.Current = Cursors.WaitCursor;

                AutoSearchComplete = txtsearchValue.Text;
                try
                {
                    AddAppointment App = new AddAppointment();
                    Cont_FirmCode = Contactgridview.Rows[e.RowIndex].Cells[2].Value.ToString();
                    Cont_FirstName = Contactgridview.Rows[e.RowIndex].Cells[7].Value.ToString();
                    Cont_LastName = Contactgridview.Rows[e.RowIndex].Cells[8].Value.ToString();
                    Cont_CaseNo = Contactgridview.Rows[e.RowIndex].Cells[17].Value.ToString();
                    Cont_Flag = "RecordAddFromContact";
                    App.ShowDialog();


                }
                catch
                { }

                Cursor.Current = Cursors.Default;
            }
        }



        private void btnReportExport_Click(object sender, EventArgs e)
        {
            try
            {


                if (strtdateTimePickerReport.Text != "" && enddateTimePickerReport.Text != "")
                {

                    DateTime fromdate = DateTime.Parse(Convert.ToDateTime(strtdateTimePickerReport.Value.Date).ToShortDateString());
                    DateTime todate = DateTime.Parse(Convert.ToDateTime(enddateTimePickerReport.Value.Date).ToShortDateString());

                    if (fromdate > todate)
                    {

                        MessageBox.Show("from date is greater than to date.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                    else
                    {
                        strtdateTimePickerReport.Format = DateTimePickerFormat.Custom;
                        strtdateTimePickerReport.CustomFormat = Helper.UniversalDateFormat;


                        enddateTimePickerReport.Format = DateTimePickerFormat.Custom;
                        enddateTimePickerReport.CustomFormat = Helper.UniversalDateFormat;

                        try
                        {
                            GetMonthlyUseDetails(strtdateTimePickerReport.Text, enddateTimePickerReport.Text);
                        }
                        catch
                        {
                        }
                    }
                }
            }
            catch
            {

            }
        }

        public void GetMonthlyUseDetails(string Fromdt, string todate)
        {
            try
            {
                string s = "exec Sproc_Generate_Report_Monthly_Useges '" + Fromdt + "','" + todate + "'";

                DataSet dsgetmothlyreport = new DataSet();

                dsgetmothlyreport.Clear();

                dsgetmothlyreport = dal.GetData(s, Application.StartupPath);

                if (dsgetmothlyreport != null && dsgetmothlyreport.Tables != null && dsgetmothlyreport.Tables[0].Rows.Count > 0)
                {

                    StreamWriter wr = new StreamWriter(Application.StartupPath + "\\Upload\\MonthlyUsageReport.xls");
                    // Write Columns to excel file
                    for (int i = 0; i < dsgetmothlyreport.Tables[0].Columns.Count; i++)
                    {
                        wr.Write(dsgetmothlyreport.Tables[0].Columns[i].ToString().ToUpper() + "\t");
                    }
                    wr.WriteLine();
                    //write rows to excel file
                    for (int i = 0; i < (dsgetmothlyreport.Tables[0].Rows.Count); i++)
                    {
                        for (int j = 0; j < dsgetmothlyreport.Tables[0].Columns.Count; j++)
                        {
                            if (dsgetmothlyreport.Tables[0].Rows[i][j] != null)
                            {
                                wr.Write(Convert.ToString(dsgetmothlyreport.Tables[0].Rows[i][j]) + "\t");
                            }
                            else
                            {
                                wr.Write("\t");
                            }
                        }
                        wr.WriteLine();
                    }
                    wr.Close();

                    try
                    {

                        var fbd = new FolderBrowserDialog();
                        if (fbd.ShowDialog() == DialogResult.OK)
                        {

                            var localPath = Path.Combine(fbd.SelectedPath, Application.StartupPath + "\\Upload\\MonthlyUsageReport.xls");


                            File.Copy(Application.StartupPath + @"\Upload\MonthlyUsageReport.xls", fbd.SelectedPath + "\\" + "MonthlyUsageReport " + Convert.ToString(System.DateTime.Now.ToString("yyyyMMddHHmmss")) + "'.xls", true);





                            MessageBox.Show("Download Complete.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                }
                else
                {

                }
            }
            catch (Exception ex)
            {


                MessageBox.Show(ex.Message.ToString(), Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        int iConfirm = 0;
        private void Main_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                try
                {
                    InsertBadgeDate();
                }
                catch
                {
                }

                notifyIcon1.Text = "Synergy Notification System status";
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(1000, "Synergy Notification System confirm status", "Total updated reminder : " + iConfirm + "", ToolTipIcon.Info);
                notifyIcon1.DoubleClick += new EventHandler(notifyIcon1_DoubleClick);


            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }


        }
        void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                iConfirm = GetbadgeCount();
                notifyIcon1.Text = "Synergy Notification System status";
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(1000, "Synergy Notification System confirm status", "Total updated reminder : " + iConfirm + "", ToolTipIcon.Info);
            }
            catch
            {


            }

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notifyIcon1.Visible = false;
        }

        public int GetbadgeCount()
        {

            int ResultCount = 0;
            try
            {

                string UserMac = string.Empty;
                UserMac = dal.GetClientMAC();

                string s = "exec sproc_Get_BadgeCount '" + UserMac + "'";

                DataSet dsgetbadgeCount = new DataSet();
                dsgetbadgeCount.Clear();
                dsgetbadgeCount = dal.GetData(s, Application.StartupPath);
                if (dsgetbadgeCount != null && dsgetbadgeCount.Tables != null && dsgetbadgeCount.Tables[0].Rows.Count > 0)
                {
                    ResultCount = Convert.ToInt32(dsgetbadgeCount.Tables[0].Rows[0]["Total"].ToString());
                }
                else
                {
                    ResultCount = 0;
                }


            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            return ResultCount;
        }

        public void InsertBadgeDate()
        {
            try
            {
                string UserMac = string.Empty;
                UserMac = dal.GetClientMAC();

                DataSet dsInsertbadgeCount = new DataSet();

                string s = "exec Sproc_Insert_BadgeTime '" + UserMac + "'";

                dsInsertbadgeCount.Clear();

                dsInsertbadgeCount = dal.GetData(s, Application.StartupPath);

                if (dsInsertbadgeCount != null && dsInsertbadgeCount.Tables[0].Rows.Count > 0)
                {

                }
                else
                {

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void tmrbadgenotification_Tick(object sender, EventArgs e)
        {
            try
            {
                iConfirm = GetbadgeCount();
            }
            catch
            {
            }



        }



        public void Autopushcloud()
        {
            try
            {
                string ClientKEY = string.Empty;
                ClientKEY = dal.GetClientKey();
                DataSet dsAutopush = new DataSet();
                string s = "Sproc_A1lawPullRS_A1Law '" + ClientKEY + "','" + clsGlobalDeclaration.ClientMac + "'";

                dsAutopush = dal.GetData(s, Application.StartupPath);


                if (dsAutopush.Tables[0].Rows[0]["Result"].ToString() == "1")
                {

                    try
                    {
                        AutoCloudSyncReminderAdd();
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                    try
                    {
                        DataSet ClearParties = new DataSet();
                        ClearParties = dal.GetData("Exec Sp_ClearAddparties '" + clsGlobalDeclaration.ClientMac + "'", Application.StartupPath);
                    }
                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                }
            }
            catch
            {

            }
        }

        public void AutoCloudSyncReminderAdd()
        {
            try
            {
                DataSet dsCloudSync = new DataSet();
                string strCloud = "Exec Get_CloudSyncDate";
                dsCloudSync = dal.GetData(strCloud, Application.StartupPath);

                if (dsCloudSync.Tables[0].Rows.Count > 0)
                {


                    var postData = string.Empty;


                    var request = (HttpWebRequest)WebRequest.Create(ConfigReader.GetCloudServiceHost() + "Reminder/wsAddReminder");


                    request.ContentType = "application/json";
                    request.Method = "POST";





                    postData = "[";
                    for (int i = 0; i <= dsCloudSync.Tables[0].Rows.Count - 1; i++)
                    {

                        postData += "{";
                        postData += "'RID':'" + dsCloudSync.Tables[0].Rows[i]["RID"] + "',";
                        postData += "'EventID':'" + dsCloudSync.Tables[0].Rows[i]["EventID"] + "',";
                        postData += "'Event':'" + dsCloudSync.Tables[0].Rows[i]["Event"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'EventTitle':'" + dsCloudSync.Tables[0].Rows[i]["EventTitle"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'Phone':'" + dsCloudSync.Tables[0].Rows[i]["Phone"] + "',";
                        postData += "'Email':'" + dsCloudSync.Tables[0].Rows[i]["Email"] + "',";
                        postData += "'FromPhoneNo':'" + dsCloudSync.Tables[0].Rows[i]["FromPhoneNo"] + "',";
                        postData += "'AccountSID':'" + dsCloudSync.Tables[0].Rows[i]["AccountSID"] + "',";
                        postData += "'AuthToken':'" + dsCloudSync.Tables[0].Rows[i]["AuthToken"] + "',";
                        postData += "'EventDatetime':'" + dsCloudSync.Tables[0].Rows[i]["EventDatetime"] + "',";
                        postData += "'SMSStatus':'" + dsCloudSync.Tables[0].Rows[i]["SMSStatus"] + "',";
                        postData += "'EmailStatus':'" + dsCloudSync.Tables[0].Rows[i]["EmailStatus"] + "',";
                        postData += "'CallStatus':'" + dsCloudSync.Tables[0].Rows[i]["CallStatus"] + "',";
                        postData += "'ClientKey':'" + dsCloudSync.Tables[0].Rows[i]["ClientKey"] + "',";
                        postData += "'RedirectLink':'" + dsCloudSync.Tables[0].Rows[i]["RedirectLink"] + "',";
                        postData += "'EmailFrom':'" + dsCloudSync.Tables[0].Rows[i]["EmailFrom"] + "',";
                        postData += "'EmailPassword':'" + dsCloudSync.Tables[0].Rows[i]["EmailPassword"] + "',";
                        postData += "'EmailFromTitle':'" + dsCloudSync.Tables[0].Rows[i]["EmailFromTitle"] + "',";
                        postData += "'EmailPort':'" + dsCloudSync.Tables[0].Rows[i]["EmailPort"] + "',";
                        postData += "'EmailSMTP':'" + dsCloudSync.Tables[0].Rows[i]["EmailSMTP"] + "',";
                        postData += "'CompanyLOGO':'" + dsCloudSync.Tables[0].Rows[i]["CompanyLOGO"] + "',";
                        postData += "'IsStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsStatus"] + "',";
                        postData += "'MainEventID':'" + dsCloudSync.Tables[0].Rows[i]["MainEventID"] + "',";
                        postData += "'IsTwilioStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsTwilioStatus"] + "',";
                        postData += "'AppointmentDatetime':'" + dsCloudSync.Tables[0].Rows[i]["AppointmentDatetime"] + "',";
                        postData += "'IsConfirmable':'" + dsCloudSync.Tables[0].Rows[i]["IsConfirmable"] + "',";
                        postData += "'TwilioLang':'" + dsCloudSync.Tables[0].Rows[i]["TwilioLang"] + "'";


                        postData += "},";

                    }
                    postData = postData.Remove(postData.Length - 1);
                    postData += "]";

                    var data = Encoding.ASCII.GetBytes(postData);



                    request.ContentLength = data.Length;


                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    ReminderSystem.wsReminderResponse WsCloud = JsonConvert.DeserializeObject<ReminderSystem.wsReminderResponse>(responseString);

                    if (WsCloud.Success == true)
                    {



                        DataTable dtAutoConfirm = new DataTable();
                        dtAutoConfirm.Columns.AddRange(new DataColumn[2] { new DataColumn("MainEventID", typeof(string)),
                        new DataColumn("ClientKey", typeof(string))});

                        for (int i = 0; i <= WsCloud.ResponseDetail.Count - 1; i++)
                        {
                            dtAutoConfirm.Rows.Add(WsCloud.ResponseDetail[i].EventID, WsCloud.ResponseDetail[i].ClientKey);
                        }


                        if (dtAutoConfirm != null)
                        {
                            DataSet dsAutoSyncupdate = new DataSet();
                            SqlParameter[] p = new SqlParameter[1];
                            p[0] = new SqlParameter("@SyncCloudStatus", dtAutoConfirm);
                            dsAutoSyncupdate = dal.GetData("[Cloud_UpdateSync]", p, Application.StartupPath);
                        }


                    }
                }

            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }


        public void Reset()
        {
            CaseNo = "";
            Event = "";
            Category = "";
            ColorCal = "";
            Staff = "";
            SEvent = "";
            LineFirst = "";
        }

        public static string CaseNo, Event, Category, ColorCal, Staff, SEvent, LineFirst = string.Empty;

        public void CreateEventFIle(string Createfilename, string strCaseNoCal, string strEventCal, string strCategoryCal, string strColorCal, string strStaffCal, string strSEventCal)
        {


            string path = @"C:\LawLocal\" + Createfilename + ".txt";


            try
            {


                Reset();

                if (File.Exists(path))
                {
                    File.Delete(path);
                }


                CaseNo = strCaseNoCal;
                Event = strEventCal;
                Category = strCategoryCal;
                ColorCal = strColorCal;
                Staff = strStaffCal;
                SEvent = strSEventCal;

                LineFirst = CaseNo + " " + Event + " " + Category + " " + ColorCal + " " + Staff;



                using (FileStream fsS = File.Create(path))
                {


                    Byte[] title = new UTF8Encoding(true).GetBytes(LineFirst + "\r\n");
                    fsS.Write(title, 0, title.Length);

                    byte[] Event1 = new UTF8Encoding(true).GetBytes(SEvent + "\r\n");
                    fsS.Write(Event1, 0, Event1.Length);

                    byte[] Category1 = new UTF8Encoding(true).GetBytes(Category + "\r\n");
                    fsS.Write(Category1, 0, Category1.Length);

                    byte[] Color1 = new UTF8Encoding(true).GetBytes(ColorCal + "\r\n");
                    fsS.Write(Color1, 0, Color1.Length);

                    byte[] Staff1 = new UTF8Encoding(true).GetBytes(Staff);
                    fsS.Write(Staff1, 0, Staff1.Length);

                    fsS.Close();
                    fsS.Dispose();
                }
            }
            catch
            {

            }



        }

        public static void MainLocatin(string Filename)
        {
            try
            {



                string strCmdText;
                strCmdText = @"Outlk1 c:\lawlocal perfiller c:\lawlocal\" + Filename + ".txt";

                //Process pCalaudit = new Process();

                //pCalaudit.Refresh();

                //pCalaudit.StartInfo.FileName = "cmd.exe";
                //pCalaudit.StartInfo.UseShellExecute = false;
                //pCalaudit.StartInfo.RedirectStandardOutput = true;
                //pCalaudit.StartInfo.RedirectStandardInput = true;
                //pCalaudit.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                //pCalaudit.Start();

                //pCalaudit.StandardInput.WriteLine(@"cd c:\lawlocal");


                //pCalaudit.StandardInput.WriteLine(strCmdText);

                //pCalaudit.CloseMainWindow();
                //pCalaudit.Close();


                /* Start new Command*/

                try
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = "cmd.exe";
                    startInfo.RedirectStandardOutput = true;
                    startInfo.RedirectStandardError = true;
                    startInfo.RedirectStandardInput = true;
                    startInfo.UseShellExecute = false;
                    startInfo.CreateNoWindow = true;

                    Process processTemp = new Process();
                    processTemp.StartInfo = startInfo;
                    processTemp.EnableRaisingEvents = true;
                    try
                    {
                        processTemp.Start();

                        using (StreamWriter sr = processTemp.StandardInput)
                        {

                            sr.WriteLine(@"cd c:\lawlocal");
                            sr.WriteLine(strCmdText);
                            sr.Close();
                            sr.Flush();
                        }


                        processTemp.CloseMainWindow();
                        processTemp.Close();

                    }
                    catch
                    {

                    }
                }
                catch
                {

                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }



        }



        private void btnA1sync_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Are you sure you want to Sync A1Law events to Synergy Notification System ?", "Synergy Notification System", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {


                int isA1sync = 0;
                if (chkA1toRS.Checked == true)
                {
                    isA1sync = 1;

                    tmrTwoSync.Enabled = true;
                    tmrTwoSync.Interval = 300000;
                }
                else
                {
                    isA1sync = 0;

                    tmrTwoSync.Enabled = false;
                    tmrTwoSync.Interval = 300000;
                }

                try
                {
                    DataSet dsA1sync = new DataSet();


                    string s = "exec Sproc_A1synctoRS '" + isA1sync + "'";

                    dsA1sync = dal.GetData(s, Application.StartupPath);

                    if (dsgetA1Details != null && dsgetA1Details.Tables != null && dsgetA1Details.Tables[0].Rows.Count > 0)
                    {

                    }


                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }
            else
            {

            }

        }





        public void GetA1LawSyncStatus()
        {

            DataSet dssyncA1 = new DataSet();
            string s = "EXEC Sproc_Get_A1lawSyncStatus";
            dssyncA1 = dal.GetData(s, Application.StartupPath);
            if (dssyncA1 != null && dssyncA1.Tables[0].Rows.Count > 0)
            {

                if (dssyncA1.Tables[0].Rows[0]["A1Sync"].ToString() == "1")
                {
                    chkA1toRS.Checked = true;
                }
                else
                {
                    chkA1toRS.Checked = false;
                }

                if (dssyncA1.Tables[0].Rows[0]["ReCallEnable"].ToString() == "1")
                {
                    chkrecall.Checked = true;
                }
                else
                {
                    chkrecall.Checked = false;
                    btnrecallUpdate.Location = new Point(15, 56);
                }

                drpHMT.SelectedIndex = drpHMT.FindStringExact(dssyncA1.Tables[0].Rows[0]["RecallHMT"].ToString());
                drpReinterval.SelectedIndex = drpReinterval.FindStringExact(dssyncA1.Tables[0].Rows[0]["RecallInterval"].ToString());


            }
            else
            {

            }

        }


        public void GetRecallInterval()
        {

            DataSet dsInterval = new DataSet();
            string s = "EXEC Sproc_GetRecallInterval";
            dsInterval = dal.GetData(s, Application.StartupPath);
            if (dsInterval != null && dsInterval.Tables[0].Rows.Count > 0)
            {
                drpReinterval.Items.Clear();
                for (int i = 0; i < dsInterval.Tables[0].Rows.Count; i++)
                {
                    DrpBulkItemBind itembind = new DrpBulkItemBind();
                    itembind.Text = dsInterval.Tables[0].Rows[i][1].ToString();
                    itembind.Value = dsInterval.Tables[0].Rows[i][0].ToString();
                    drpReinterval.Items.Add(itembind);


                }

                drpReinterval.SelectedIndex = -1;
            }
            else
            {

            }

        }


        public void GetHMT()
        {

            DataSet dsHMT = new DataSet();
            string s = "EXEC Sproc_GetRecallTime";
            dsHMT = dal.GetData(s, Application.StartupPath);
            if (dsHMT != null && dsHMT.Tables[0].Rows.Count > 0)
            {
                drpHMT.Items.Clear();
                for (int i = 0; i < dsHMT.Tables[0].Rows.Count; i++)
                {
                    DrpBulkItemBind itembind = new DrpBulkItemBind();
                    itembind.Text = dsHMT.Tables[0].Rows[i][1].ToString();
                    itembind.Value = dsHMT.Tables[0].Rows[i][0].ToString();
                    drpHMT.Items.Add(itembind);
                }
                drpHMT.SelectedIndex = -1;
            }
            else
            {

            }
        }

        private void chkrecall_CheckedChanged(object sender, EventArgs e)
        {

            if (chkrecall.Checked == true)
            {
                label56.Visible = true;
                label57.Visible = true;
                drpHMT.Visible = true;
                drpReinterval.Visible = true;
                btnrecallUpdate.Location = new Point(15, 123);
            }
            else
            {
                label56.Visible = false;
                label57.Visible = false;
                drpHMT.Visible = false;
                drpReinterval.Visible = false;
                ////drpHMT.SelectedIndex = -1;
                ////drpReinterval.SelectedIndex = -1;
                btnrecallUpdate.Location = new Point(15, 56);
            }
        }

        private void btnrecallUpdate_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            int RecallCheck = 0;

            if (chkrecall.Checked == true)
            {
                RecallCheck = 1;
            }
            else
            {
                RecallCheck = 0;
            }
            DataSet dsrecallupdate = new DataSet();
            SqlParameter[] p = new SqlParameter[3];

            if (chkrecall.Checked == true && drpHMT.SelectedItem != null && drpReinterval.SelectedItem != null)
            {
                dsrecallupdate.Clear();

                p[0] = new SqlParameter("@RecallEnable", RecallCheck);
                p[1] = new SqlParameter("@HMT", drpHMT.SelectedItem.ToString());
                p[2] = new SqlParameter("@Inteval", drpReinterval.SelectedItem.ToString());

                string s = "EXEC Sproc_UpdateRecall_Details '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "'";

                dsrecallupdate = dal.GetData(s, Application.StartupPath);
                if (dsrecallupdate.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    try
                    {
                        CloudSyncRecallDetails(dsrecallupdate.Tables[0].Rows[0]["ReCallEnable"].ToString(), dsrecallupdate.Tables[0].Rows[0]["RecallHMT"].ToString(), dsrecallupdate.Tables[0].Rows[0]["RecallInterval"].ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString(), Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    MessageBox.Show("Recall setting updated successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                }
            }
            else if (chkrecall.Checked == false)
            {
                dsrecallupdate.Clear();

                p[0] = new SqlParameter("@RecallEnable", RecallCheck);
                p[1] = new SqlParameter("@HMT", "");
                p[2] = new SqlParameter("@Inteval", "");

                string s = "EXEC Sproc_UpdateRecall_Details '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "'";

                dsrecallupdate = dal.GetData(s, Application.StartupPath);
                if (dsrecallupdate.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    try
                    {
                        CloudSyncRecallDetails(dsrecallupdate.Tables[0].Rows[0]["ReCallEnable"].ToString(), dsrecallupdate.Tables[0].Rows[0]["RecallHMT"].ToString(), dsrecallupdate.Tables[0].Rows[0]["RecallInterval"].ToString());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString(), Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    MessageBox.Show("Recall setting updated successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                }
            }

            else
            {
                MessageBox.Show("Select time and interval first.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }

        public void CloudSyncRecallDetails(string reEnable, string reHMT, string reInterval)
        {
            var request = (HttpWebRequest)WebRequest.Create("http://www.snsapp.in/Upgrade/rs_UpgradeApplication.svc/UpdateRecall");
            var postData = string.Empty;
            request.ContentType = "application/json";
            request.Method = "POST";
            string ClientproductKey = string.Empty;
            ClientproductKey = dal.GetClientKey();

            postData = "{\"ClientKey\":\"" + ClientproductKey + "\",\"ReCallEnable\":\"" + reEnable + "\",\"RecallHMT\":\"" + reHMT + "\",\"RecallInterval\":\"" + reInterval + "\"}";

            var data = Encoding.ASCII.GetBytes(postData);
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            StreamReader responsereader = new StreamReader(response.GetResponseStream());
            string responsedata = responsereader.ReadToEnd();
        }

        //API server Code

        private void tmrAPI_Tick(object sender, EventArgs e)
        {
            try
            {
                var date = DateTime.Now;
                if (Convert.ToInt32(date.Hour) > 9 && Convert.ToInt32(date.Hour) <= 17)
                {
                    try
                    {
                        GetPathForAPI();
                    }
                    catch
                    {

                    }
                }
                else
                { }
            }
            catch
            { }
        }

        public void ExecBatFile(string BATFilePath)
        {
            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = BATFilePath;
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                startInfo.UseShellExecute = false;
                startInfo.CreateNoWindow = true;

                Process processTemp = new Process();
                processTemp.StartInfo = startInfo;
                processTemp.EnableRaisingEvents = true;
                try
                {
                    processTemp.Start();
                    processTemp.WaitForExit();
                }
                catch
                {

                }
            }
            catch
            {

            }
        }

        private void InsertCSVRecords(string CSVFilePath)
        {
            try
            {
                string csvPath = CSVFilePath;

                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[12] { new DataColumn("Account", typeof(string)),
            new DataColumn("Last Name", typeof(string)),
            new DataColumn("First Name",typeof(string)), 
            new DataColumn("Home Phone", typeof(string)),
            new DataColumn("Work Phone", typeof(string)),
            new DataColumn("Appointment Phone", typeof(string)),
            new DataColumn("Appt date", typeof(string)),
            new DataColumn("Appt Time", typeof(string)),
            new DataColumn("Appt Doctor", typeof(string)),
            new DataColumn("Appt Room", typeof(string)),
            new DataColumn("Appt Room Location", typeof(string)),
            new DataColumn("Appt Room Class", typeof(string))            
            
            });

                string csvData = File.ReadAllText(csvPath);
                csvData = csvData.Replace("\"", "");
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;
                        foreach (string cell in row.Split(','))
                        {
                            try
                            {
                                dt.Rows[dt.Rows.Count - 1][i] = cell;
                                i++;
                            }
                            catch
                            { }
                        }
                    }
                }

                if (dt != null)
                {
                    DataSet dtResultAPIUpload = new DataSet();

                    SqlParameter[] p = new SqlParameter[2];

                    p[0] = new SqlParameter("@UploadAppointment", dt);
                    p[1] = new SqlParameter("@CreatedUserFK", clsGlobalDeclaration.ClientMac);



                    dtResultAPIUpload = dal.GetData("[Sproc_Import_API_Reminder]", p, Application.StartupPath);
                    if (dtResultAPIUpload.Tables[0].Rows[0]["Result"].ToString() == "Inserted")
                    {
                        try
                        {
                            APIserverReminderAddToCloud();
                        }
                        catch
                        {
                            RemoveUnsyncData();
                        }
                    }
                    else
                    {


                    }

                }
            }
            catch
            {

            }
        }

        public void RemoveUnsyncData()
        {
            try
            {

                DataSet dsRemovesync = new DataSet();
                string s = "EXEC sproc_RemoveWhenNoCloud";

                dsRemovesync = dal.GetData(s, Application.StartupPath);
                if (dsRemovesync.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    MessageBox.Show("Error occur(s) during reminder sync to cloud.");
                }
                else
                {

                }
            }
            catch
            {

            }
        }

        public void GetPathForAPI()
        {
            DataSet dsAPI = new DataSet();
            string s = "EXEC GetAPIPathDetails";

            dsAPI = dal.GetData(s, Application.StartupPath);
            if (dsAPI != null)
            {
                try
                {
                    ExecBatFile(dsAPI.Tables[0].Rows[0]["BatfilePath"].ToString());
                }
                catch
                {
                }

                try
                {
                    InsertCSVRecords(dsAPI.Tables[0].Rows[0]["CSVfilePath"].ToString());
                }
                catch
                {
                }
            }
            else
            { }
        }

        private void Browsebtn_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.Default;
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = @"C:\";
                openFileDialog1.Title = "Browse Text Files";
                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;
                openFileDialog1.Multiselect = false;
                openFileDialog1.DefaultExt = "CSV files (*.csv)|*.csv";
                openFileDialog1.Filter = "All files (*.*)|*.*|CSV files (*.csv)|*.csv";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                openFileDialog1.ReadOnlyChecked = true;
                openFileDialog1.ShowReadOnly = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    txtbrowseFile.Text = openFileDialog1.FileName;
                }

                lblcsvFilePath.Text = openFileDialog1.FileName.ToString();

                txtbrowseFile.Text = "";

            }
            catch
            { }
        }

        private void chkAPI_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAPI.Checked == true)
            {
                txtbrowseFile.Visible = true;
                Browsebtn.Visible = true;
                lblcsvFilePath.Visible = true;
                btnCSVupload.Location = new Point(14, 167);
                lblbatFilePath.Visible = true;
                txtAPIbatfile.Visible = true;
                btnuploadBatfile.Visible = true;

            }
            else
            {
                txtbrowseFile.Visible = false;
                Browsebtn.Visible = false;
                lblcsvFilePath.Visible = false;
                btnCSVupload.Location = new Point(14, 58);
                lblcsvFilePath.Text = "";
                lblbatFilePath.Text = "";
                lblbatFilePath.Visible = false;
                txtAPIbatfile.Visible = false;
                btnuploadBatfile.Visible = false;
            }
        }

        private void btnCSVupload_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;



            int APICheck = 0;

            if (chkAPI.Checked == true)
            {
                APICheck = 1;
            }
            else
            {
                APICheck = 0;
            }
            DataSet dsAPIupdate = new DataSet();
            SqlParameter[] p = new SqlParameter[4];

            if (chkAPI.Checked == true && lblcsvFilePath.Text != "" && lblbatFilePath.Text != "")
            {
                dsAPIupdate.Clear();

                p[0] = new SqlParameter("@Flag", "UPDATE");
                p[1] = new SqlParameter("@IsCSVstatus", APICheck);
                p[2] = new SqlParameter("@CSVfilePath", lblcsvFilePath.Text);
                p[3] = new SqlParameter("@BatfilePath", lblbatFilePath.Text);




                string s = "EXEC Sproc_Get_APIserverDetails '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "'";

                dsAPIupdate = dal.GetData(s, Application.StartupPath);
                if (dsAPIupdate.Tables[0].Rows[0]["Result"].ToString() == "1")
                {
                    tmrAPI.Enabled = true;
                    tmrAPI.Interval = 3600000;

                    MessageBox.Show("Setting updated successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {

                }
            }
            else if (chkAPI.Checked == false)
            {
                dsAPIupdate.Clear();

                p[0] = new SqlParameter("@Flag", "UPDATE");
                p[1] = new SqlParameter("@IsCSVstatus", APICheck);
                p[2] = new SqlParameter("@CSVfilePath", "");
                p[3] = new SqlParameter("@BatfilePath", "");



                string s = "EXEC Sproc_Get_APIserverDetails '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "'";

                dsAPIupdate = dal.GetData(s, Application.StartupPath);
                if (dsAPIupdate.Tables[0].Rows[0]["Result"].ToString() == "1")
                {

                    MessageBox.Show("Setting updated successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {

                }
            }
            else
            {
                MessageBox.Show("Please Upload at least one *.CSV file and *.BAT file.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




            Cursor.Current = Cursors.Default;
        }

        public void GetAPIStatus()
        {

            DataSet dssyncA1 = new DataSet();
            string s = "EXEC Sproc_Get_APIserverDetails 'GET'";
            dssyncA1 = dal.GetData(s, Application.StartupPath);
            if (dssyncA1 != null && dssyncA1.Tables[0].Rows.Count > 0)
            {

                if (dssyncA1.Tables[0].Rows[0]["IsCSVstatus"].ToString() == "1")
                {
                    chkAPI.Checked = true;
                }
                else
                {
                    chkAPI.Checked = false;
                    btnCSVupload.Location = new Point(14, 58);
                }
                lblcsvFilePath.Text = dssyncA1.Tables[0].Rows[0]["CSVfilePath"].ToString();
                lblbatFilePath.Text = dssyncA1.Tables[0].Rows[0]["BatfilePath"].ToString();

            }
            else
            {

            }

        }

        private void btnuploadBatfile_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.Default;
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = @"C:\";
                openFileDialog1.Title = "Browse Text Files";
                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;
                openFileDialog1.Multiselect = false;
                openFileDialog1.DefaultExt = "bat files (*.bat)|*.bat";
                openFileDialog1.Filter = "All files (*.*)|*.*|bat files (*.bat)|*.bat";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                openFileDialog1.ReadOnlyChecked = true;
                openFileDialog1.ShowReadOnly = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    txtAPIbatfile.Text = openFileDialog1.FileName;
                }


                lblbatFilePath.Text = openFileDialog1.FileName.ToString();

                txtAPIbatfile.Text = "";

            }
            catch
            { }
        }


        public void GetAPITimerOnOff()
        {

            DataSet dsAPItimerOnOff = new DataSet();
            string s = "EXEC Sproc_Get_APIserverDetails 'GET'";
            dsAPItimerOnOff = dal.GetData(s, Application.StartupPath);
            if (dsAPItimerOnOff != null && dsAPItimerOnOff.Tables[0].Rows.Count > 0)
            {

                if (dsAPItimerOnOff.Tables[0].Rows[0]["IsCSVstatus"].ToString() == "1")
                {
                    tmrAPI.Enabled = true;
                    tmrAPI.Interval = 3600000;
                }
                else
                {

                }

            }
            else
            {

            }

        }

        public void APIserverReminderAddToCloud()
        {
            try
            {
                DataSet dsCloudSync = new DataSet();
                string strCloud = "Exec Get_CloudSyncDate_API";
                dsCloudSync = dal.GetData(strCloud, Application.StartupPath);

                if (dsCloudSync.Tables[0].Rows.Count > 0)
                {


                    StringBuilder postData = new StringBuilder();

                    postData.Append("[");
                    for (int i = 0; i <= dsCloudSync.Tables[0].Rows.Count - 1; i++)
                    {

                        postData.Append("{");
                        postData.Append("'RID':'" + dsCloudSync.Tables[0].Rows[i]["RID"] + "',");
                        postData.Append("'EventID':'" + dsCloudSync.Tables[0].Rows[i]["EventID"] + "',");
                        postData.Append("'Event':'" + dsCloudSync.Tables[0].Rows[i]["Event"].ToString().Replace(@"'", @"\'") + "',");
                        postData.Append("'EventTitle':'" + dsCloudSync.Tables[0].Rows[i]["EventTitle"].ToString().Replace(@"'", @"\'") + "',");
                        postData.Append("'Phone':'" + dsCloudSync.Tables[0].Rows[i]["Phone"] + "',");
                        postData.Append("'Email':'" + dsCloudSync.Tables[0].Rows[i]["Email"] + "',");
                        postData.Append("'FromPhoneNo':'" + dsCloudSync.Tables[0].Rows[i]["FromPhoneNo"] + "',");
                        postData.Append("'AccountSID':'" + dsCloudSync.Tables[0].Rows[i]["AccountSID"] + "',");
                        postData.Append("'AuthToken':'" + dsCloudSync.Tables[0].Rows[i]["AuthToken"] + "',");
                        postData.Append("'EventDatetime':'" + dsCloudSync.Tables[0].Rows[i]["EventDatetime"] + "',");
                        postData.Append("'SMSStatus':'" + dsCloudSync.Tables[0].Rows[i]["SMSStatus"] + "',");
                        postData.Append("'EmailStatus':'" + dsCloudSync.Tables[0].Rows[i]["EmailStatus"] + "',");
                        postData.Append("'CallStatus':'" + dsCloudSync.Tables[0].Rows[i]["CallStatus"] + "',");
                        postData.Append("'ClientKey':'" + dsCloudSync.Tables[0].Rows[i]["ClientKey"] + "',");
                        postData.Append("'RedirectLink':'" + dsCloudSync.Tables[0].Rows[i]["RedirectLink"] + "',");
                        postData.Append("'EmailFrom':'" + dsCloudSync.Tables[0].Rows[i]["EmailFrom"] + "',");
                        postData.Append("'EmailPassword':'" + dsCloudSync.Tables[0].Rows[i]["EmailPassword"] + "',");
                        postData.Append("'EmailFromTitle':'" + dsCloudSync.Tables[0].Rows[i]["EmailFromTitle"] + "',");
                        postData.Append("'EmailPort':'" + dsCloudSync.Tables[0].Rows[i]["EmailPort"] + "',");
                        postData.Append("'EmailSMTP':'" + dsCloudSync.Tables[0].Rows[i]["EmailSMTP"] + "',");
                        postData.Append("'CompanyLOGO':'" + dsCloudSync.Tables[0].Rows[i]["CompanyLOGO"] + "',");
                        postData.Append("'IsStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsStatus"] + "',");
                        postData.Append("'MainEventID':'" + dsCloudSync.Tables[0].Rows[i]["MainEventID"] + "',");
                        postData.Append("'IsTwilioStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsTwilioStatus"] + "',");
                        postData.Append("'AppointmentDatetime':'" + dsCloudSync.Tables[0].Rows[i]["AppointmentDatetime"] + "',");
                        postData.Append("'IsConfirmable':'" + dsCloudSync.Tables[0].Rows[i]["IsConfirmable"] + "',");
                        postData.Append("'TwilioLang':'" + dsCloudSync.Tables[0].Rows[i]["TwilioLang"] + "',");
                        postData.Append("'ConfirmDatetime':'',");
                        postData.Append("'Confirmtime':''");


                        if (i == dsCloudSync.Tables[0].Rows.Count - 1)
                            postData.Append("}]");
                        else
                        {
                            postData.Append("},");
                        }

                    }

                    rsWebRequest objRequest = new rsWebRequest();

                    objRequest.postData = postData.ToString();
                    objRequest.webURIServiceName = "Reminder/wsAddReminder";
                    ReminderSystem.wsReminderResponse objResponse = JsonConvert.DeserializeObject<ReminderSystem.wsReminderResponse>(objRequest.getWebResponse());

                    if (!objResponse.Success)
                    {
                        MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }


                    if (objResponse.Success == true)
                    {

                        DataTable dtCLoudConfirm = new DataTable();
                        dtCLoudConfirm.Columns.AddRange(new DataColumn[2] { new DataColumn("MainEventID", typeof(string)),
                        new DataColumn("ClientKey", typeof(string))});

                        for (int i = 0; i <= objResponse.ResponseDetail.Count - 1; i++)
                        {
                            dtCLoudConfirm.Rows.Add(objResponse.ResponseDetail[i].EventID, objResponse.ResponseDetail[i].ClientKey);
                        }

                        if (dtCLoudConfirm != null)
                        {
                            DataSet dsCloudSyncupdate = new DataSet();
                            SqlParameter[] p = new SqlParameter[1];
                            p[0] = new SqlParameter("@SyncCloudStatus", dtCLoudConfirm);
                            dsCloudSyncupdate = dal.GetData("[Cloud_UpdateSync]", p, Application.StartupPath);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.ToString());
            }

        }

        private void btnPromotionals_Click(object sender, EventArgs e)
        {

        }


        private void btnbrowsepramotionals_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.Default;
                OpenFileDialog openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = @"C:\";
                openFileDialog1.Title = "Browse Text Files";
                openFileDialog1.CheckFileExists = true;
                openFileDialog1.CheckPathExists = true;
                openFileDialog1.Multiselect = false;
                openFileDialog1.DefaultExt = "CSV files (*.csv)|*.csv";
                openFileDialog1.Filter = "All files (*.*)|*.*|CSV files (*.csv)|*.csv";
                openFileDialog1.FilterIndex = 2;
                openFileDialog1.RestoreDirectory = true;

                openFileDialog1.ReadOnlyChecked = true;
                openFileDialog1.ShowReadOnly = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    txtCSVpathpramotionals.Text = openFileDialog1.FileName;
                }






            }
            catch
            { }
        }

        private void btnTriggerPramotionlas_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (txtCSVpathpramotionals.Text.Trim() != "")
            {
                InsertpromotionalCSV(txtCSVpathpramotionals.Text.Trim());
            }
            else
            {

                MessageBox.Show("Please Upload *.CSV file", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Cursor.Current = Cursors.Default;
        }


        private void InsertpromotionalCSV(string CSVFilePath)
        {
            try
            {


                string csvPath = CSVFilePath;

                DataTable dtPromo = new DataTable();

                dtPromo.Columns.AddRange(new DataColumn[12] { new DataColumn("Account", typeof(string)),
            new DataColumn("Last Name", typeof(string)),
            new DataColumn("First Name",typeof(string)), 
            new DataColumn("Home Phone", typeof(string)),
            new DataColumn("Work Phone", typeof(string)),
            new DataColumn("Appointment Phone", typeof(string)),
            new DataColumn("Appt date", typeof(string)),
            new DataColumn("Appt Time", typeof(string)),
            new DataColumn("Appt Doctor", typeof(string)),
            new DataColumn("Appt Room", typeof(string)),
            new DataColumn("Appt Room Location", typeof(string)),
            new DataColumn("Appt Room Class", typeof(string))
            
            
            });


                string csvData = File.ReadAllText(csvPath);
                csvData = csvData.Replace("\"", "");
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dtPromo.Rows.Add();
                        int i = 0;
                        foreach (string cell in row.Split(','))
                        {
                            try
                            {
                                dtPromo.Rows[dtPromo.Rows.Count - 1][i] = cell;
                                i++;
                            }
                            catch
                            { }
                        }
                    }
                }

                if (dtPromo != null)
                {
                    if (dtPromo.Rows.Count <= 100)
                    {

                        DataSet dtResultAPIUploadPromo = new DataSet();

                        SqlParameter[] p = new SqlParameter[2];

                        p[0] = new SqlParameter("@UploadAppointmentpromotional", dtPromo);
                        p[1] = new SqlParameter("@CreatedUserFK", clsGlobalDeclaration.ClientMac);



                        dtResultAPIUploadPromo = dal.GetData("[Sproc_promotional_API_Reminder]", p, Application.StartupPath);
                        if (dtResultAPIUploadPromo.Tables[0].Rows[0]["Result"].ToString() == "Inserted")
                        {
                            try
                            {

                                API_Promotional();
                            }
                            catch
                            {

                            }

                            MessageBox.Show("Promotional SMS/CALL triggered successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        else
                        {


                        }



                    }
                    else
                    {
                        MessageBox.Show("You can only upload 100 promotional messages at a time. Please check the file and remove extra records to upload this file.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }



                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }


        }

        public void API_Promotional()
        {
            try
            {
                DataSet dsCloudSync = new DataSet();
                string strCloud = "Exec Get_CloudSyncDate_promotional";
                dsCloudSync = dal.GetData(strCloud, Application.StartupPath);

                if (dsCloudSync.Tables[0].Rows.Count > 0)
                {


                    StringBuilder postData = new StringBuilder();

                    postData.Append("[");
                    for (int i = 0; i <= dsCloudSync.Tables[0].Rows.Count - 1; i++)
                    {

                        postData.Append("{");
                        postData.Append("'RID':'" + dsCloudSync.Tables[0].Rows[i]["RID"] + "',");
                        postData.Append("'EventID':'" + dsCloudSync.Tables[0].Rows[i]["EventID"] + "',");
                        postData.Append("'Event':'" + dsCloudSync.Tables[0].Rows[i]["Event"].ToString().Replace(@"'", @"\'") + "',");
                        postData.Append("'EventTitle':'" + dsCloudSync.Tables[0].Rows[i]["EventTitle"].ToString().Replace(@"'", @"\'") + "',");
                        postData.Append("'Phone':'" + dsCloudSync.Tables[0].Rows[i]["Phone"] + "',");
                        postData.Append("'Email':'" + dsCloudSync.Tables[0].Rows[i]["Email"] + "',");
                        postData.Append("'FromPhoneNo':'" + dsCloudSync.Tables[0].Rows[i]["FromPhoneNo"] + "',");
                        postData.Append("'AccountSID':'" + dsCloudSync.Tables[0].Rows[i]["AccountSID"] + "',");
                        postData.Append("'AuthToken':'" + dsCloudSync.Tables[0].Rows[i]["AuthToken"] + "',");
                        postData.Append("'EventDatetime':'" + dsCloudSync.Tables[0].Rows[i]["EventDatetime"] + "',");
                        postData.Append("'SMSStatus':'" + dsCloudSync.Tables[0].Rows[i]["SMSStatus"] + "',");
                        postData.Append("'EmailStatus':'" + dsCloudSync.Tables[0].Rows[i]["EmailStatus"] + "',");
                        postData.Append("'CallStatus':'" + dsCloudSync.Tables[0].Rows[i]["CallStatus"] + "',");
                        postData.Append("'ClientKey':'" + dsCloudSync.Tables[0].Rows[i]["ClientKey"] + "',");
                        postData.Append("'RedirectLink':'" + dsCloudSync.Tables[0].Rows[i]["RedirectLink"] + "',");
                        postData.Append("'EmailFrom':'" + dsCloudSync.Tables[0].Rows[i]["EmailFrom"] + "',");
                        postData.Append("'EmailPassword':'" + dsCloudSync.Tables[0].Rows[i]["EmailPassword"] + "',");
                        postData.Append("'EmailFromTitle':'" + dsCloudSync.Tables[0].Rows[i]["EmailFromTitle"] + "',");
                        postData.Append("'EmailPort':'" + dsCloudSync.Tables[0].Rows[i]["EmailPort"] + "',");
                        postData.Append("'EmailSMTP':'" + dsCloudSync.Tables[0].Rows[i]["EmailSMTP"] + "',");
                        postData.Append("'CompanyLOGO':'" + dsCloudSync.Tables[0].Rows[i]["CompanyLOGO"] + "',");
                        postData.Append("'IsStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsStatus"] + "',");
                        postData.Append("'MainEventID':'" + dsCloudSync.Tables[0].Rows[i]["MainEventID"] + "',");
                        postData.Append("'IsTwilioStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsTwilioStatus"] + "',");
                        postData.Append("'AppointmentDatetime':'" + dsCloudSync.Tables[0].Rows[i]["AppointmentDatetime"] + "',");
                        postData.Append("'IsConfirmable':'" + dsCloudSync.Tables[0].Rows[i]["IsConfirmable"] + "',");
                        postData.Append("'TwilioLang':'" + dsCloudSync.Tables[0].Rows[i]["TwilioLang"] + "'");

                        if (i == dsCloudSync.Tables[0].Rows.Count - 1)
                            postData.Append("}]");
                        else
                        {
                            postData.Append("},");
                        }

                    }

                    rsWebRequest objRequest = new rsWebRequest();


                    objRequest.postData = postData.ToString();
                    objRequest.webURIServiceName = "Reminder/wsAddReminder";
                    ReminderSystem.wsReminderResponse objResponse = JsonConvert.DeserializeObject<ReminderSystem.wsReminderResponse>(objRequest.getWebResponse());

                    if (!objResponse.Success)
                    {
                        MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Cursor.Current = Cursors.Default;
                        return;
                    }


                    if (objResponse.Success == true)
                    {



                        DataTable dtPromoConfirm = new DataTable();
                        dtPromoConfirm.Columns.AddRange(new DataColumn[2] { new DataColumn("MainEventID", typeof(string)),
                        new DataColumn("ClientKey", typeof(string))});

                        for (int i = 0; i <= objResponse.ResponseDetail.Count - 1; i++)
                        {
                            dtPromoConfirm.Rows.Add(objResponse.ResponseDetail[i].EventID, objResponse.ResponseDetail[i].ClientKey);
                        }


                        if (dtPromoConfirm != null)
                        {
                            DataSet dsPromoSyncupdate = new DataSet();
                            SqlParameter[] p = new SqlParameter[1];
                            p[0] = new SqlParameter("@SyncCloudStatus", dtPromoConfirm);
                            dsPromoSyncupdate = dal.GetData("[Cloud_UpdateSync]", p, Application.StartupPath);
                        }


                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException.ToString());
            }

        }


        public void Cloud_TwilioStatus_TwowaySync()
        {
            #region Push Data to Cloud
            try
            {
                System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                dateInfo.ShortDatePattern = Helper.UniversalDateFormat;

                rsWebRequest objRequest = new rsWebRequest();
                wsReminderStatusResponse objResponse = new wsReminderStatusResponse();

                objRequest.postData = "{'ClientKey':'" + clsGlobalDeclaration.ClientKey + "'}";
                objRequest.webURIServiceName = "Reminder/wsReminderStatus";
                objResponse = JsonConvert.DeserializeObject<wsReminderStatusResponse>(objRequest.getWebResponse());

                if (!objResponse.Success)
                {
                    MessageBox.Show(objResponse.Message.ToString(), "Error while Sync settings to Cloude", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Cursor.Current = Cursors.Default;
                    return;
                }
                else
                {



                    foreach (wsReminderStatusDetails vStatus in objResponse.ResponseDetail)
                    {
                        try
                        {

                            string SID = string.Empty;
                            if (vStatus.SID == null)
                            {
                                SID = "";
                            }
                            else
                            {
                                SID = vStatus.SID.ToString();
                            }



                            /* 2 way sync process Start */
                            try
                            {
                                DataSet dsCalaudit = new DataSet();
                                dsCalaudit.Clear();



                                string s = "exec sproc_UpdateTwilioIVRStatusinCalaudit '" + vStatus.MainEventID.ToString() + "','" + clsGlobalDeclaration.ClientMac + "'";
                                dsCalaudit = dal.GetData(s, Application.StartupPath);
                                if (dsCalaudit != null && dsCalaudit.Tables[0].Rows.Count > 0)
                                {

                                    try
                                    {
                                        if (dsCalaudit.Tables[0].Rows.Count > 1)
                                        {
                                            CreateEventFIle(dsCalaudit.Tables[0].Rows[0]["Eventno"].ToString(), dsCalaudit.Tables[0].Rows[0]["Caseno"].ToString(), dsCalaudit.Tables[0].Rows[0]["EventT"].ToString(), dsCalaudit.Tables[0].Rows[0]["Category"].ToString(), dsCalaudit.Tables[0].Rows[0]["Color"].ToString(), dsCalaudit.Tables[0].Rows[0]["Staff"].ToString(), dsCalaudit.Tables[0].Rows[0]["FullEvent"].ToString());
                                        }
                                        else
                                        {
                                            CreateEventFIle(dsCalaudit.Tables[1].Rows[0]["Eventno"].ToString(), dsCalaudit.Tables[1].Rows[0]["Caseno"].ToString(), dsCalaudit.Tables[1].Rows[0]["EventT"].ToString(), dsCalaudit.Tables[1].Rows[0]["Category"].ToString(), dsCalaudit.Tables[1].Rows[0]["Color"].ToString(), dsCalaudit.Tables[1].Rows[0]["Staff"].ToString(), dsCalaudit.Tables[1].Rows[0]["FullEvent"].ToString());
                                        }
                                        //CreateEventFIle(dsCalaudit.Tables[0].Rows[0]["Eventno"].ToString(), dsCalaudit.Tables[0].Rows[0]["Caseno"].ToString(), dsCalaudit.Tables[0].Rows[0]["EventT"].ToString(), dsCalaudit.Tables[0].Rows[0]["Category"].ToString(), dsCalaudit.Tables[0].Rows[0]["Color"].ToString(), dsCalaudit.Tables[0].Rows[0]["Staff"].ToString(), dsCalaudit.Tables[0].Rows[0]["FullEvent"].ToString());
                                    }
                                    catch
                                    { }

                                    try
                                    {
                                        //MainLocatin(dsCalaudit.Tables[0].Rows[0]["Eventno"].ToString());
                                        if (dsCalaudit.Tables[0].Rows.Count > 1)
                                        {
                                            MainLocatin(dsCalaudit.Tables[0].Rows[0]["Eventno"].ToString());
                                        }
                                        else
                                        {
                                            MainLocatin(dsCalaudit.Tables[1].Rows[0]["Eventno"].ToString());
                                        }
                                    }
                                    catch
                                    { }


                                    try
                                    {
                                        UpdateAfterSync(vStatus.MainEventID.ToString());
                                    }
                                    catch
                                    {

                                    }
                                }
                                else
                                {
                                }

                            }
                            catch
                            {
                            }

                            /* 2 way sync process End */

                        }
                        catch
                        {

                        }
                    }
                }







            }
            catch (Exception Ex)
            {

                throw;
            }
            #endregion Push Data to Cloud

        }

        private void tmrTwoSync_Tick(object sender, EventArgs e)
        {
            try
            {
                Autopushcloud();
            }
            catch
            { }


            try
            {
                Cloud_TwilioStatus_TwowaySync();
            }
            catch
            {

            }
        }

        public void TwowayTimerOnOff()
        {
            DataSet dssyncA1 = new DataSet();
            string s = "EXEC Sproc_Get_A1lawSyncStatus";
            dssyncA1 = dal.GetData(s, Application.StartupPath);
            if (dssyncA1 != null && dssyncA1.Tables[0].Rows.Count > 0)
            {

                if (dssyncA1.Tables[0].Rows[0]["A1Sync"].ToString() == "1")
                {
                    chkA1toRS.Checked = true;

                    tmrTwoSync.Enabled = true;
                    tmrTwoSync.Interval = 300000;

                }
                else
                {
                    chkA1toRS.Checked = false;
                }

            }
        }

        private void grdAgendadata_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 36)
            {
                //App_SetValueForEventNo = grdAgendadata.Rows[e.RowIndex].Cells[1].Value.ToString();

                //ConfirmEvent objConfirmEvent = new ConfirmEvent();
                //objConfirmEvent.ShowDialog();
            }
        }

        public void UpdateAfterSync(string EventID)
        {

            DataSet dsUpdateAfter = new DataSet();
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@EventID", EventID);
            p[1] = new SqlParameter("@MACID", clsGlobalDeclaration.ClientMac);

            string s = "exec Sproc_Update_AfterTwoWaysync '" + p[0].Value + "','" + p[1].Value + "'";
            dsUpdateAfter = dal.GetData(s, Application.StartupPath);

        }

        private void btnPendingEvent_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                DataSet dsCloudSync = new DataSet();
                string strCloud = "Exec sp_get_future_reminder";
                dsCloudSync = dal.GetData(strCloud, Application.StartupPath);

                if (dsCloudSync.Tables[0].Rows.Count > 0)
                {
                    var postData = string.Empty;

                    var request = (HttpWebRequest)WebRequest.Create(ConfigReader.GetCloudServiceHost() + "Reminder/wsAddReminder");

                    request.ContentType = "application/json";
                    request.Method = "POST";

                    postData = "[";
                    for (int i = 0; i <= dsCloudSync.Tables[0].Rows.Count - 1; i++)
                    {
                        postData += "{";
                        postData += "'RID':'" + dsCloudSync.Tables[0].Rows[i]["RID"] + "',";
                        postData += "'EventID':'" + dsCloudSync.Tables[0].Rows[i]["EventID"] + "',";
                        postData += "'Event':'" + dsCloudSync.Tables[0].Rows[i]["Event"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'EventTitle':'" + dsCloudSync.Tables[0].Rows[i]["EventTitle"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'Phone':'" + dsCloudSync.Tables[0].Rows[i]["Phone"] + "',";
                        postData += "'Email':'" + dsCloudSync.Tables[0].Rows[i]["Email"] + "',";
                        postData += "'FromPhoneNo':'" + dsCloudSync.Tables[0].Rows[i]["FromPhoneNo"] + "',";
                        postData += "'AccountSID':'" + dsCloudSync.Tables[0].Rows[i]["AccountSID"] + "',";
                        postData += "'AuthToken':'" + dsCloudSync.Tables[0].Rows[i]["AuthToken"] + "',";
                        postData += "'EventDatetime':'" + dsCloudSync.Tables[0].Rows[i]["EventDatetime"] + "',";
                        postData += "'SMSStatus':'" + dsCloudSync.Tables[0].Rows[i]["SMSStatus"] + "',";
                        postData += "'EmailStatus':'" + dsCloudSync.Tables[0].Rows[i]["EmailStatus"] + "',";
                        postData += "'CallStatus':'" + dsCloudSync.Tables[0].Rows[i]["CallStatus"] + "',";
                        postData += "'ClientKey':'" + dsCloudSync.Tables[0].Rows[i]["ClientKey"] + "',";
                        postData += "'RedirectLink':'" + dsCloudSync.Tables[0].Rows[i]["RedirectLink"] + "',";
                        postData += "'EmailFrom':'" + dsCloudSync.Tables[0].Rows[i]["EmailFrom"] + "',";
                        postData += "'EmailPassword':'" + dsCloudSync.Tables[0].Rows[i]["EmailPassword"] + "',";
                        postData += "'EmailFromTitle':'" + dsCloudSync.Tables[0].Rows[i]["EmailFromTitle"] + "',";
                        postData += "'EmailPort':'" + dsCloudSync.Tables[0].Rows[i]["EmailPort"] + "',";
                        postData += "'EmailSMTP':'" + dsCloudSync.Tables[0].Rows[i]["EmailSMTP"] + "',";
                        postData += "'CompanyLOGO':'" + dsCloudSync.Tables[0].Rows[i]["CompanyLOGO"] + "',";
                        postData += "'IsStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsStatus"] + "',";
                        postData += "'MainEventID':'" + dsCloudSync.Tables[0].Rows[i]["MainEventID"] + "',";
                        postData += "'IsTwilioStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsTwilioStatus"] + "',";
                        postData += "'AppointmentDatetime':'" + dsCloudSync.Tables[0].Rows[i]["AppointmentDatetime"] + "',";
                        postData += "'IsConfirmable':'" + dsCloudSync.Tables[0].Rows[i]["IsConfirmable"] + "',";
                        postData += "'TwilioLang':'" + dsCloudSync.Tables[0].Rows[i]["TwilioLang"] + "'";

                        postData += "},";

                    }
                    postData = postData.Remove(postData.Length - 1);
                    postData += "]";

                    var data = Encoding.ASCII.GetBytes(postData);

                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    ReminderSystem.wsReminderResponse WsCloud = JsonConvert.DeserializeObject<ReminderSystem.wsReminderResponse>(responseString);

                    if (WsCloud.Success == true)
                    {
                        DataTable dtAutoConfirm = new DataTable();
                        dtAutoConfirm.Columns.AddRange(new DataColumn[2] { new DataColumn("MainEventID", typeof(string)),
                        new DataColumn("ClientKey", typeof(string))});

                        for (int i = 0; i <= WsCloud.ResponseDetail.Count - 1; i++)
                        {
                            dtAutoConfirm.Rows.Add(WsCloud.ResponseDetail[i].EventID, WsCloud.ResponseDetail[i].ClientKey);
                        }

                        if (dtAutoConfirm != null)
                        {
                            DataSet dsAutoSyncupdate = new DataSet();
                            SqlParameter[] p = new SqlParameter[1];
                            p[0] = new SqlParameter("@SyncCloudStatus", dtAutoConfirm);
                            dsAutoSyncupdate = dal.GetData("[Cloud_UpdateSync]", p, Application.StartupPath);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void tooltipPenddingreminder_Popup(object sender, PopupEventArgs e)
        {

        }

        private void btnSetLanguage_Click(object sender, EventArgs e)
        {
            string SqlQuery = "UPDATE lst_TwilioLanguage SET ISDEFAULTLANG =0; UPDATE lst_TwilioLanguage SET ISDEFAULTLANG =1 WHERE LID=" + cbLanguage.SelectedValue.ToString();

            dal.Execute_NonQuery(SqlQuery);

            MessageBox.Show("Default Language Updated Successfully.");
        }
    }
}



