﻿namespace AppToto
{
    partial class EmailPassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmailPassword));
            this.btnEmailSettings = new System.Windows.Forms.Button();
            this.grbCall = new System.Windows.Forms.GroupBox();
            this.txtAuthoToken = new System.Windows.Forms.TextBox();
            this.txtCallNo = new System.Windows.Forms.TextBox();
            this.txtAccountSID = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.grbSMS = new System.Windows.Forms.GroupBox();
            this.txtSMSSenderNo = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.grbEmail = new System.Windows.Forms.GroupBox();
            this.picEmailSend = new System.Windows.Forms.PictureBox();
            this.btnTestMail = new System.Windows.Forms.Button();
            this.txtSMTPPort = new System.Windows.Forms.TextBox();
            this.txtSMTPhost = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtEmailSubject = new System.Windows.Forms.TextBox();
            this.txtEmailID = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtClientKey = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grbCall.SuspendLayout();
            this.grbSMS.SuspendLayout();
            this.grbEmail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmailSend)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEmailSettings
            // 
            this.btnEmailSettings.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnEmailSettings.Location = new System.Drawing.Point(444, 429);
            this.btnEmailSettings.Name = "btnEmailSettings";
            this.btnEmailSettings.Size = new System.Drawing.Size(75, 29);
            this.btnEmailSettings.TabIndex = 15;
            this.btnEmailSettings.Text = "Update";
            this.btnEmailSettings.UseVisualStyleBackColor = true;
            this.btnEmailSettings.Click += new System.EventHandler(this.btnEmailSettings_Click);
            // 
            // grbCall
            // 
            this.grbCall.Controls.Add(this.txtAuthoToken);
            this.grbCall.Controls.Add(this.txtCallNo);
            this.grbCall.Controls.Add(this.txtAccountSID);
            this.grbCall.Controls.Add(this.label26);
            this.grbCall.Controls.Add(this.label27);
            this.grbCall.Controls.Add(this.label28);
            this.grbCall.Location = new System.Drawing.Point(489, 91);
            this.grbCall.Name = "grbCall";
            this.grbCall.Size = new System.Drawing.Size(476, 218);
            this.grbCall.TabIndex = 8;
            this.grbCall.TabStop = false;
            this.grbCall.Text = "Call";
            // 
            // txtAuthoToken
            // 
            this.txtAuthoToken.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAuthoToken.Location = new System.Drawing.Point(15, 172);
            this.txtAuthoToken.Name = "txtAuthoToken";
            this.txtAuthoToken.Size = new System.Drawing.Size(375, 23);
            this.txtAuthoToken.TabIndex = 14;
            // 
            // txtCallNo
            // 
            this.txtCallNo.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCallNo.Location = new System.Drawing.Point(15, 41);
            this.txtCallNo.Name = "txtCallNo";
            this.txtCallNo.Size = new System.Drawing.Size(375, 23);
            this.txtCallNo.TabIndex = 12;
            // 
            // txtAccountSID
            // 
            this.txtAccountSID.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAccountSID.Location = new System.Drawing.Point(15, 107);
            this.txtAccountSID.Name = "txtAccountSID";
            this.txtAccountSID.Size = new System.Drawing.Size(375, 23);
            this.txtAccountSID.TabIndex = 13;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label26.Location = new System.Drawing.Point(11, 21);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(87, 15);
            this.label26.TabIndex = 12;
            this.label26.Text = "CALL Number";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label27.Location = new System.Drawing.Point(11, 148);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(106, 15);
            this.label27.TabIndex = 11;
            this.label27.Text = "Twilio AuthToken";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label28.Location = new System.Drawing.Point(11, 85);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(108, 15);
            this.label28.TabIndex = 10;
            this.label28.Text = "Twilio AccountSid";
            // 
            // grbSMS
            // 
            this.grbSMS.Controls.Add(this.txtSMSSenderNo);
            this.grbSMS.Controls.Add(this.label25);
            this.grbSMS.Location = new System.Drawing.Point(489, 12);
            this.grbSMS.Name = "grbSMS";
            this.grbSMS.Size = new System.Drawing.Size(473, 73);
            this.grbSMS.TabIndex = 7;
            this.grbSMS.TabStop = false;
            this.grbSMS.Text = "SMS";
            // 
            // txtSMSSenderNo
            // 
            this.txtSMSSenderNo.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSMSSenderNo.Location = new System.Drawing.Point(15, 40);
            this.txtSMSSenderNo.Name = "txtSMSSenderNo";
            this.txtSMSSenderNo.Size = new System.Drawing.Size(375, 23);
            this.txtSMSSenderNo.TabIndex = 11;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label25.Location = new System.Drawing.Point(11, 20);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(130, 15);
            this.label25.TabIndex = 10;
            this.label25.Text = "SMS Sender Number";
            // 
            // grbEmail
            // 
            this.grbEmail.Controls.Add(this.picEmailSend);
            this.grbEmail.Controls.Add(this.btnTestMail);
            this.grbEmail.Controls.Add(this.txtSMTPPort);
            this.grbEmail.Controls.Add(this.txtSMTPhost);
            this.grbEmail.Controls.Add(this.txtPassword);
            this.grbEmail.Controls.Add(this.label17);
            this.grbEmail.Controls.Add(this.txtEmailSubject);
            this.grbEmail.Controls.Add(this.txtEmailID);
            this.grbEmail.Controls.Add(this.label24);
            this.grbEmail.Controls.Add(this.label23);
            this.grbEmail.Controls.Add(this.label22);
            this.grbEmail.Controls.Add(this.label4);
            this.grbEmail.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.grbEmail.Location = new System.Drawing.Point(12, 12);
            this.grbEmail.Name = "grbEmail";
            this.grbEmail.Size = new System.Drawing.Size(461, 388);
            this.grbEmail.TabIndex = 6;
            this.grbEmail.TabStop = false;
            this.grbEmail.Text = "Email";
            // 
            // picEmailSend
            // 
            this.picEmailSend.Image = global::SynergyNotificationSystem.Properties.Resources.Emailsend;
            this.picEmailSend.Location = new System.Drawing.Point(363, 301);
            this.picEmailSend.Name = "picEmailSend";
            this.picEmailSend.Size = new System.Drawing.Size(32, 32);
            this.picEmailSend.TabIndex = 10;
            this.picEmailSend.TabStop = false;
            this.picEmailSend.Visible = false;
            // 
            // btnTestMail
            // 
            this.btnTestMail.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnTestMail.Location = new System.Drawing.Point(259, 302);
            this.btnTestMail.Name = "btnTestMail";
            this.btnTestMail.Size = new System.Drawing.Size(75, 29);
            this.btnTestMail.TabIndex = 4;
            this.btnTestMail.Text = "Test Mail";
            this.btnTestMail.UseVisualStyleBackColor = true;
            this.btnTestMail.Click += new System.EventHandler(this.btnTestMail_Click);
            // 
            // txtSMTPPort
            // 
            this.txtSMTPPort.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSMTPPort.Location = new System.Drawing.Point(22, 306);
            this.txtSMTPPort.Name = "txtSMTPPort";
            this.txtSMTPPort.Size = new System.Drawing.Size(214, 23);
            this.txtSMTPPort.TabIndex = 9;
            // 
            // txtSMTPhost
            // 
            this.txtSMTPhost.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSMTPhost.Location = new System.Drawing.Point(22, 241);
            this.txtSMTPhost.Name = "txtSMTPhost";
            this.txtSMTPhost.Size = new System.Drawing.Size(431, 23);
            this.txtSMTPhost.TabIndex = 8;
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(20, 175);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(431, 23);
            this.txtPassword.TabIndex = 6;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label17.Location = new System.Drawing.Point(16, 151);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 15);
            this.label17.TabIndex = 1;
            this.label17.Text = "Password";
            // 
            // txtEmailSubject
            // 
            this.txtEmailSubject.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailSubject.Location = new System.Drawing.Point(21, 116);
            this.txtEmailSubject.Name = "txtEmailSubject";
            this.txtEmailSubject.Size = new System.Drawing.Size(431, 23);
            this.txtEmailSubject.TabIndex = 7;
            // 
            // txtEmailID
            // 
            this.txtEmailID.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmailID.Location = new System.Drawing.Point(22, 50);
            this.txtEmailID.Name = "txtEmailID";
            this.txtEmailID.Size = new System.Drawing.Size(431, 23);
            this.txtEmailID.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label24.Location = new System.Drawing.Point(18, 282);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 15);
            this.label24.TabIndex = 4;
            this.label24.Text = "SMTP Port";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label23.Location = new System.Drawing.Point(18, 219);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 15);
            this.label23.TabIndex = 3;
            this.label23.Text = "SMTP Host";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label22.Location = new System.Drawing.Point(17, 92);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 15);
            this.label22.TabIndex = 2;
            this.label22.Text = "Email Subject";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label4.Location = new System.Drawing.Point(18, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Email ID";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtClientKey);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(489, 315);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(473, 85);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Product Key";
            // 
            // txtClientKey
            // 
            this.txtClientKey.Font = new System.Drawing.Font("Malgun Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClientKey.Location = new System.Drawing.Point(15, 40);
            this.txtClientKey.Name = "txtClientKey";
            this.txtClientKey.Size = new System.Drawing.Size(375, 23);
            this.txtClientKey.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label1.Location = new System.Drawing.Point(11, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 15);
            this.label1.TabIndex = 10;
            this.label1.Text = "Key";
            // 
            // EmailPassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 470);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnEmailSettings);
            this.Controls.Add(this.grbCall);
            this.Controls.Add(this.grbSMS);
            this.Controls.Add(this.grbEmail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmailPassword";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Email Login";
            this.Load += new System.EventHandler(this.EmailPassword_Load);
            this.grbCall.ResumeLayout(false);
            this.grbCall.PerformLayout();
            this.grbSMS.ResumeLayout(false);
            this.grbSMS.PerformLayout();
            this.grbEmail.ResumeLayout(false);
            this.grbEmail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picEmailSend)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEmailSettings;
        private System.Windows.Forms.GroupBox grbCall;
        private System.Windows.Forms.TextBox txtAuthoToken;
        private System.Windows.Forms.TextBox txtCallNo;
        private System.Windows.Forms.TextBox txtAccountSID;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox grbSMS;
        private System.Windows.Forms.TextBox txtSMSSenderNo;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox grbEmail;
        private System.Windows.Forms.PictureBox picEmailSend;
        private System.Windows.Forms.Button btnTestMail;
        private System.Windows.Forms.TextBox txtSMTPPort;
        private System.Windows.Forms.TextBox txtSMTPhost;
        private System.Windows.Forms.TextBox txtEmailSubject;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtEmailID;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtClientKey;
        private System.Windows.Forms.Label label1;

    }
}