﻿using System;
using System.Data;
using System.Windows.Forms;
using DataLayer;
using System.Configuration;
using ReminderSystem.Classes;
using System.Management;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;

namespace ReminderSystem
{
    public partial class SplashScreen : Form
    {

        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        public SplashScreen()
        {
            InitializeComponent();
        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {
        }

        private void ApplicationStartUp()
        {



            updateProgress("Checking System Security");


            try
            {
                string strMac = string.Empty;
                strMac =dal.GetClientMAC();

                

                DataSet dscheckValiduser = new DataSet();
                string s = "Exec CheckRegistration '" + strMac + "'";
                dscheckValiduser = dal.GetData(s, Application.StartupPath);
                if (dscheckValiduser.Tables[0].Rows.Count == 0)
                {
                    ShowRegistration();
                }
                else
                {
                    clsGlobalDeclaration.ClientKey = dscheckValiduser.Tables[0].Rows[0]["ProductKey"].ToString();
                    clsGlobalDeclaration.ClientMac = dscheckValiduser.Tables[0].Rows[0]["MacID"].ToString();



                    try
                    {
                        CloudAPIGetDetails();
                    }
                    catch
                    {

                    }

                    try
                    {
                        CloudSyncReminderAdd();
                    }
                    catch
                    {

                    }


                }
            }
            catch
            {

            }





            #region Database Connection Check

            updateProgress("Checking Data Connectivity");
            try
            {
                DataSet dsconnectivity = new DataSet();
                string s = "SELECT GETDATE() CURRENTDATE";

                dsconnectivity = dal.GetData(s, Application.StartupPath);
                object objDate = null;
                if (dsconnectivity.Tables[0].Rows.Count > 0)
                {
                    objDate = dsconnectivity.Tables[0].Rows[0]["CURRENTDATE"].ToString();
                }


                if (string.IsNullOrEmpty(objDate.ToString()))
                {
                    MessageBox.Show("Unable to Connect Data Server_1", "Database Connectivity", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                    this.Dispose();
                    Application.Exit();
                    Application.ExitThread();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to Connect Data Server_2", "Database Connectivity", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                this.Dispose();
                Application.Exit();
                Application.ExitThread();
            }


            #endregion Database Connection Check

            #region Application Update Check
            updateProgress("Checking Data Connectivity");
            //TODO: Need to set code for Auto Update Application
            #endregion Application Update Check

            this.Visible = false;
            new AppToto.Main().Show();
            this.Visible = false;
        }

        private void ShowRegistration()
        {
            this.Hide();
            new Registration().ShowDialog();
            this.Close();
        }

        private void updateProgress(string strMessage)
        {
            lblProgress.Text = strMessage;
            Application.DoEvents();
        }

        private void tmStart_Tick(object sender, EventArgs e)
        {
            tmStart.Enabled = false;
            ApplicationStartUp();
        }

        

        public void CloudAPIGetDetails()
        {


            updateProgress("Checking System Upgrade");
            var request = (HttpWebRequest)WebRequest.Create("http://www.snsapp.in/Upgrade/rs_UpgradeApplication.svc/GetVersion");
            var postData = string.Empty;
            request.ContentType = "application/json";
            request.Method = "POST";
            string ClientproductKey = string.Empty;
            ClientproductKey = dal.GetClientKey();

            postData = "{\"ClientKey\":\"" + ClientproductKey + "\"}";


            var data = Encoding.ASCII.GetBytes(postData);
            request.ContentLength = data.Length;
            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            var response = (HttpWebResponse)request.GetResponse();
            StreamReader responsereader = new StreamReader(response.GetResponseStream());
            string responsedata = responsereader.ReadToEnd();

            string _App_Version, _App_Path = string.Empty;
            List<ApplicationUpgrade> list = JsonConvert.DeserializeObject<List<ApplicationUpgrade>>(responsedata);

            _App_Path = list[0].ApplicationPath.ToString();
            _App_Version = list[0].ApplicationVersion.ToString();

            if (_App_Version != GeyApplicationVersion())
            {
                //CloudDownloadSetup(_App_Path);
            }
            else
            {

            }



        }

        public void CloudSyncReminderAdd()
        {
            try
            {

                string ClientproductKey, ClientInstelledVersion = string.Empty;


                ClientproductKey = dal.GetClientKey();
                ClientInstelledVersion = GeyApplicationVersion();

                var postData = string.Empty;



                var request = (HttpWebRequest)WebRequest.Create("http://www.snsapp.in/Upgrade/rs_UpgradeApplication.svc/setClientVersion");

                request.ContentType = "application/json";
                request.Method = "POST";
                
                postData = "{\"UserProductKey\":\"" + ClientproductKey + "\",\"UserInstalledVersion\":\"" + ClientInstelledVersion + "\"}";
                
                var data = Encoding.ASCII.GetBytes(postData);
                request.ContentLength = data.Length;
                
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }

            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }
        public string GeyApplicationVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;
            return version;
        }
        public void CloudDownloadSetup(string ApplicationPath)
        {
            try
            {
                updateProgress("Processing Upgrade Application");

                Cursor.Current = Cursors.WaitCursor;

                string remoteUri = ApplicationPath;
                string fileName = "setup.exe", msifileName = "SynergyNotificationSystem.msi", myStringWebResource = null, MsimyStringWebResource = null;

                string SystemDrivePath, FinalLocation, FinalLocationForCopy, MsiFinalLocationForCopy = string.Empty;

                WebClient myWebClient = new WebClient();
                myStringWebResource = remoteUri + fileName;
                MsimyStringWebResource = remoteUri + msifileName;

                myWebClient.DownloadFile(myStringWebResource, fileName);
                myWebClient.DownloadFile(MsimyStringWebResource, msifileName);

                SystemDrivePath = Path.GetPathRoot(Environment.SystemDirectory);
                FinalLocation = SystemDrivePath + "SynergyNotificationSystem";

                FinalLocationForCopy = SystemDrivePath + "SynergyNotificationSystem\\" + fileName;
                MsiFinalLocationForCopy = SystemDrivePath + "SynergyNotificationSystem\\" + msifileName;

                try
                {
                    DirectoryInfo di = new DirectoryInfo(FinalLocation);

                    if (Directory.Exists(FinalLocation))
                    {

                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                    }
                    else
                    {
                        if (Directory.Exists(FinalLocation) == false)
                        {
                            Directory.CreateDirectory(FinalLocation);
                        }
                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                try
                {
                    File.Copy(Application.StartupPath + @"\setup.exe", FinalLocationForCopy, true);
                }
                catch
                { }
                try
                {
                    File.Copy(Application.StartupPath + @"\SynergyNotificationSystem.msi", MsiFinalLocationForCopy, true);
                }
                catch
                { }

                try
                {
                    InstallFile(FinalLocationForCopy, MsiFinalLocationForCopy);
                }
                catch
                {

                }

                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void InstallFile(string EXEpath, string MSIpath)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                Process pInstall = new Process();
                pInstall.StartInfo.FileName = EXEpath;
                pInstall.Start();
                Close();
            }
            catch (Exception ex)
            { }

            Cursor.Current = Cursors.Default;

            updateProgress("Successfully Upgrade Your Application.");
        }




    }
}
