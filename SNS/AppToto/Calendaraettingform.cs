﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppToto
{
    public partial class Calendaraettingform : Form
    {
        public Calendaraettingform()
        {
            InitializeComponent();
            GetTimeZone();
        }
        public void GetTimeZone()
        {
            timecombo.DataSource = TimeZoneInfo.GetSystemTimeZones();//binding time zone list in drop down list
            timecombo.ValueMember = "Id";
            timecombo.DisplayMember = "DisplayName";
        }

       


    }
}
