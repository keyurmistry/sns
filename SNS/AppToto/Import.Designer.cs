﻿namespace AppToto
{
    partial class Import
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbrowseFile = new System.Windows.Forms.TextBox();
            this.Browsebtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lnkdownload = new System.Windows.Forms.LinkLabel();
            this.btnUpload = new System.Windows.Forms.Button();
            this.lblHead = new System.Windows.Forms.Label();
            this.btnExporttoexcelError = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtbrowseFile
            // 
            this.txtbrowseFile.Font = new System.Drawing.Font("Open Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbrowseFile.Location = new System.Drawing.Point(205, 176);
            this.txtbrowseFile.Name = "txtbrowseFile";
            this.txtbrowseFile.Size = new System.Drawing.Size(301, 24);
            this.txtbrowseFile.TabIndex = 19;
            // 
            // Browsebtn
            // 
            this.Browsebtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Browsebtn.BackColor = System.Drawing.Color.SteelBlue;
            this.Browsebtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Browsebtn.ForeColor = System.Drawing.Color.White;
            this.Browsebtn.Location = new System.Drawing.Point(509, 172);
            this.Browsebtn.Name = "Browsebtn";
            this.Browsebtn.Size = new System.Drawing.Size(89, 30);
            this.Browsebtn.TabIndex = 20;
            this.Browsebtn.Text = "browse...";
            this.Browsebtn.UseVisualStyleBackColor = false;
            this.Browsebtn.Click += new System.EventHandler(this.Browsebtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = global::SynergyNotificationSystem.Properties.Resources.up_arrow;
            this.pictureBox1.Location = new System.Drawing.Point(351, 84);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(62, 68);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::SynergyNotificationSystem.Properties.Resources.downloadxls;
            this.pictureBox2.Location = new System.Drawing.Point(205, 205);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(18, 18);
            this.pictureBox2.TabIndex = 44;
            this.pictureBox2.TabStop = false;
            // 
            // lnkdownload
            // 
            this.lnkdownload.AutoSize = true;
            this.lnkdownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkdownload.Location = new System.Drawing.Point(229, 208);
            this.lnkdownload.Name = "lnkdownload";
            this.lnkdownload.Size = new System.Drawing.Size(126, 15);
            this.lnkdownload.TabIndex = 43;
            this.lnkdownload.TabStop = true;
            this.lnkdownload.Text = "Download sample file";
            this.lnkdownload.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkdownload_LinkClicked);
            // 
            // btnUpload
            // 
            this.btnUpload.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUpload.BackColor = System.Drawing.Color.SteelBlue;
            this.btnUpload.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnUpload.ForeColor = System.Drawing.Color.White;
            this.btnUpload.Location = new System.Drawing.Point(292, 242);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(89, 36);
            this.btnUpload.TabIndex = 42;
            this.btnUpload.Text = "Upload";
            this.btnUpload.UseVisualStyleBackColor = false;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // lblHead
            // 
            this.lblHead.BackColor = System.Drawing.Color.SteelBlue;
            this.lblHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHead.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead.ForeColor = System.Drawing.Color.White;
            this.lblHead.Location = new System.Drawing.Point(0, 0);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(784, 39);
            this.lblHead.TabIndex = 45;
            this.lblHead.Text = "Select Excel  file to be uploaded.(*.xlsx,*.xls)";
            this.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnExporttoexcelError
            // 
            this.btnExporttoexcelError.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExporttoexcelError.BackColor = System.Drawing.Color.SteelBlue;
            this.btnExporttoexcelError.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.btnExporttoexcelError.ForeColor = System.Drawing.Color.White;
            this.btnExporttoexcelError.Location = new System.Drawing.Point(390, 242);
            this.btnExporttoexcelError.Name = "btnExporttoexcelError";
            this.btnExporttoexcelError.Size = new System.Drawing.Size(149, 36);
            this.btnExporttoexcelError.TabIndex = 47;
            this.btnExporttoexcelError.Text = "Export error record";
            this.btnExporttoexcelError.UseVisualStyleBackColor = false;
            this.btnExporttoexcelError.Click += new System.EventHandler(this.btnExporttoexcelError_Click);
            // 
            // Import
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(784, 412);
            this.Controls.Add(this.btnExporttoexcelError);
            this.Controls.Add(this.lblHead);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lnkdownload);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.Browsebtn);
            this.Controls.Add(this.txtbrowseFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "Import";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Import";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtbrowseFile;
        private System.Windows.Forms.Button Browsebtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.LinkLabel lnkdownload;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Label lblHead;
        private System.Windows.Forms.Button btnExporttoexcelError;

    }
}