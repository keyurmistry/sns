﻿namespace AppToto
{
    partial class ImportAddressBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportAddressBook));
            this.Importlink = new System.Windows.Forms.LinkLabel();
            this.Outlooklink = new System.Windows.Forms.LinkLabel();
            this.googlelink = new System.Windows.Forms.LinkLabel();
            this.pbxlsimport = new System.Windows.Forms.PictureBox();
            this.pbOutlook = new System.Windows.Forms.PictureBox();
            this.pbgoogle = new System.Windows.Forms.PictureBox();
            this.browser = new System.Windows.Forms.WebBrowser();
            this.lblHead = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbxlsimport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOutlook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbgoogle)).BeginInit();
            this.SuspendLayout();
            // 
            // Importlink
            // 
            this.Importlink.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Importlink.Font = new System.Drawing.Font("Malgun Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Importlink.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Importlink.Image = global::SynergyNotificationSystem.Properties.Resources.import;
            this.Importlink.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Importlink.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.Importlink.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.Importlink.Location = new System.Drawing.Point(554, 82);
            this.Importlink.Name = "Importlink";
            this.Importlink.Padding = new System.Windows.Forms.Padding(5);
            this.Importlink.Size = new System.Drawing.Size(124, 106);
            this.Importlink.TabIndex = 7;
            this.Importlink.TabStop = true;
            this.Importlink.Text = "Excel Import";
            this.Importlink.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Importlink.Paint += new System.Windows.Forms.PaintEventHandler(this.Importlink_Paint);
            // 
            // Outlooklink
            // 
            this.Outlooklink.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Outlooklink.Font = new System.Drawing.Font("Malgun Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.Outlooklink.Image = global::SynergyNotificationSystem.Properties.Resources.outook;
            this.Outlooklink.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Outlooklink.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.Outlooklink.LinkColor = System.Drawing.Color.Navy;
            this.Outlooklink.Location = new System.Drawing.Point(338, 82);
            this.Outlooklink.Name = "Outlooklink";
            this.Outlooklink.Padding = new System.Windows.Forms.Padding(5);
            this.Outlooklink.Size = new System.Drawing.Size(120, 106);
            this.Outlooklink.TabIndex = 6;
            this.Outlooklink.TabStop = true;
            this.Outlooklink.Text = "Outlook";
            this.Outlooklink.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Outlooklink.Paint += new System.Windows.Forms.PaintEventHandler(this.Outlooklink_Paint);
            // 
            // googlelink
            // 
            this.googlelink.ActiveLinkColor = System.Drawing.Color.OrangeRed;
            this.googlelink.Cursor = System.Windows.Forms.Cursors.Hand;
            this.googlelink.Font = new System.Drawing.Font("Malgun Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.googlelink.Image = global::SynergyNotificationSystem.Properties.Resources.calendar;
            this.googlelink.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.googlelink.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.googlelink.LinkColor = System.Drawing.Color.Maroon;
            this.googlelink.Location = new System.Drawing.Point(111, 82);
            this.googlelink.Name = "googlelink";
            this.googlelink.Padding = new System.Windows.Forms.Padding(5);
            this.googlelink.Size = new System.Drawing.Size(120, 106);
            this.googlelink.TabIndex = 5;
            this.googlelink.TabStop = true;
            this.googlelink.Text = "google";
            this.googlelink.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.googlelink.VisitedLinkColor = System.Drawing.Color.Yellow;
            this.googlelink.Paint += new System.Windows.Forms.PaintEventHandler(this.googlelink_Paint_1);
            // 
            // pbxlsimport
            // 
            this.pbxlsimport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxlsimport.Image = global::SynergyNotificationSystem.Properties.Resources.import;
            this.pbxlsimport.Location = new System.Drawing.Point(582, 84);
            this.pbxlsimport.Name = "pbxlsimport";
            this.pbxlsimport.Size = new System.Drawing.Size(64, 64);
            this.pbxlsimport.TabIndex = 8;
            this.pbxlsimport.TabStop = false;
            this.pbxlsimport.Click += new System.EventHandler(this.pbxlsimport_Click);
            // 
            // pbOutlook
            // 
            this.pbOutlook.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbOutlook.Image = global::SynergyNotificationSystem.Properties.Resources.outook;
            this.pbOutlook.Location = new System.Drawing.Point(366, 84);
            this.pbOutlook.Name = "pbOutlook";
            this.pbOutlook.Size = new System.Drawing.Size(64, 64);
            this.pbOutlook.TabIndex = 9;
            this.pbOutlook.TabStop = false;
            this.pbOutlook.Click += new System.EventHandler(this.pbOutlook_Click);
            // 
            // pbgoogle
            // 
            this.pbgoogle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbgoogle.Image = global::SynergyNotificationSystem.Properties.Resources.calendar;
            this.pbgoogle.Location = new System.Drawing.Point(139, 85);
            this.pbgoogle.Name = "pbgoogle";
            this.pbgoogle.Size = new System.Drawing.Size(64, 64);
            this.pbgoogle.TabIndex = 10;
            this.pbgoogle.TabStop = false;
            this.pbgoogle.Click += new System.EventHandler(this.pbgoogle_Click);
            // 
            // browser
            // 
            this.browser.Location = new System.Drawing.Point(0, 212);
            this.browser.MinimumSize = new System.Drawing.Size(20, 20);
            this.browser.Name = "browser";
            this.browser.Size = new System.Drawing.Size(784, 349);
            this.browser.TabIndex = 11;
            this.browser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.browser_DocumentCompleted);
            // 
            // lblHead
            // 
            this.lblHead.BackColor = System.Drawing.Color.SteelBlue;
            this.lblHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHead.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead.ForeColor = System.Drawing.Color.White;
            this.lblHead.Location = new System.Drawing.Point(0, 0);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(784, 39);
            this.lblHead.TabIndex = 12;
            this.lblHead.Text = "Add Address Book";
            this.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ImportAddressBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(784, 563);
            this.Controls.Add(this.lblHead);
            this.Controls.Add(this.browser);
            this.Controls.Add(this.pbgoogle);
            this.Controls.Add(this.pbOutlook);
            this.Controls.Add(this.pbxlsimport);
            this.Controls.Add(this.Importlink);
            this.Controls.Add(this.Outlooklink);
            this.Controls.Add(this.googlelink);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ImportAddressBook";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ImportAddressBook";
            ((System.ComponentModel.ISupportInitialize)(this.pbxlsimport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbOutlook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbgoogle)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.LinkLabel Importlink;
        private System.Windows.Forms.LinkLabel Outlooklink;
        private System.Windows.Forms.LinkLabel googlelink;
        private System.Windows.Forms.PictureBox pbxlsimport;
        private System.Windows.Forms.PictureBox pbOutlook;
        private System.Windows.Forms.PictureBox pbgoogle;
        private System.Windows.Forms.WebBrowser browser;
        private System.Windows.Forms.Label lblHead;
    }
}