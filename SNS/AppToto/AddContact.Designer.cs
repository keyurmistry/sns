﻿namespace AppToto
{
    partial class AddContact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddContact));
            this.label3 = new System.Windows.Forms.Label();
            this.NewContactPanel = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.txtdob = new System.Windows.Forms.DateTimePicker();
            this.drpCountryCode = new System.Windows.Forms.ComboBox();
            this.txtmiddle = new System.Windows.Forms.TextBox();
            this.txtLast = new System.Windows.Forms.TextBox();
            this.drpprefix = new System.Windows.Forms.ComboBox();
            this.Createbtn = new System.Windows.Forms.Button();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtZipcode = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.drpEmail = new System.Windows.Forms.ComboBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.drpphone = new System.Windows.Forms.ComboBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFirst = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblHead = new System.Windows.Forms.Label();
            this.NewContactPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Gainsboro;
            this.label3.Location = new System.Drawing.Point(-108, 40);
            this.label3.MaximumSize = new System.Drawing.Size(1000, 2);
            this.label3.MinimumSize = new System.Drawing.Size(1000, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1000, 2);
            this.label3.TabIndex = 18;
            this.label3.Text = "label3";
            // 
            // NewContactPanel
            // 
            this.NewContactPanel.AutoScroll = true;
            this.NewContactPanel.Controls.Add(this.label15);
            this.NewContactPanel.Controls.Add(this.txtdob);
            this.NewContactPanel.Controls.Add(this.drpCountryCode);
            this.NewContactPanel.Controls.Add(this.txtmiddle);
            this.NewContactPanel.Controls.Add(this.txtLast);
            this.NewContactPanel.Controls.Add(this.drpprefix);
            this.NewContactPanel.Controls.Add(this.Createbtn);
            this.NewContactPanel.Controls.Add(this.txtNote);
            this.NewContactPanel.Controls.Add(this.label14);
            this.NewContactPanel.Controls.Add(this.txtZipcode);
            this.NewContactPanel.Controls.Add(this.label13);
            this.NewContactPanel.Controls.Add(this.drpEmail);
            this.NewContactPanel.Controls.Add(this.txtEmail);
            this.NewContactPanel.Controls.Add(this.label10);
            this.NewContactPanel.Controls.Add(this.drpphone);
            this.NewContactPanel.Controls.Add(this.txtPhone);
            this.NewContactPanel.Controls.Add(this.label7);
            this.NewContactPanel.Controls.Add(this.txtFirst);
            this.NewContactPanel.Controls.Add(this.label5);
            this.NewContactPanel.Location = new System.Drawing.Point(0, 43);
            this.NewContactPanel.Name = "NewContactPanel";
            this.NewContactPanel.Size = new System.Drawing.Size(784, 447);
            this.NewContactPanel.TabIndex = 19;
            this.NewContactPanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.NewContactPanel_MouseClick);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(202, 147);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 19);
            this.label15.TabIndex = 89;
            this.label15.Text = "DOB :";
            // 
            // txtdob
            // 
            this.txtdob.CustomFormat = "dd-MMM-yyyy";
            this.txtdob.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtdob.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtdob.Location = new System.Drawing.Point(262, 147);
            this.txtdob.MaximumSize = new System.Drawing.Size(167, 25);
            this.txtdob.Name = "txtdob";
            this.txtdob.Size = new System.Drawing.Size(167, 23);
            this.txtdob.TabIndex = 9;
            this.txtdob.Value = new System.DateTime(2016, 4, 15, 19, 41, 0, 0);
            // 
            // drpCountryCode
            // 
            this.drpCountryCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpCountryCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpCountryCode.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drpCountryCode.FormattingEnabled = true;
            this.drpCountryCode.IntegralHeight = false;
            this.drpCountryCode.ItemHeight = 15;
            this.drpCountryCode.Location = new System.Drawing.Point(262, 63);
            this.drpCountryCode.MaxDropDownItems = 5;
            this.drpCountryCode.Name = "drpCountryCode";
            this.drpCountryCode.Size = new System.Drawing.Size(91, 23);
            this.drpCountryCode.TabIndex = 5;
            this.drpCountryCode.Leave += new System.EventHandler(this.drpCountryCode_Leave);
            // 
            // txtmiddle
            // 
            this.txtmiddle.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtmiddle.Location = new System.Drawing.Point(481, 26);
            this.txtmiddle.Name = "txtmiddle";
            this.txtmiddle.Size = new System.Drawing.Size(73, 23);
            this.txtmiddle.TabIndex = 3;
            this.txtmiddle.Enter += new System.EventHandler(this.Txt_middle_Enter);
            this.txtmiddle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_middle_KeyPress);
            this.txtmiddle.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Txt_middle_KeyUp);
            this.txtmiddle.Leave += new System.EventHandler(this.Txt_middle_Leave);
            // 
            // txtLast
            // 
            this.txtLast.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtLast.Location = new System.Drawing.Point(560, 26);
            this.txtLast.Name = "txtLast";
            this.txtLast.Size = new System.Drawing.Size(107, 23);
            this.txtLast.TabIndex = 4;
            this.txtLast.Enter += new System.EventHandler(this.Txt_Last_Enter);
            this.txtLast.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Last_KeyPress);
            this.txtLast.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Txt_Last_KeyUp);
            this.txtLast.Leave += new System.EventHandler(this.Txt_Last_Leave);
            // 
            // drpprefix
            // 
            this.drpprefix.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpprefix.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpprefix.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drpprefix.FormattingEnabled = true;
            this.drpprefix.Items.AddRange(new object[] {
            "Mr.",
            "Ms.",
            "Miss",
            "Mrs.",
            "Dr.",
            "Prof. "});
            this.drpprefix.Location = new System.Drawing.Point(262, 26);
            this.drpprefix.Name = "drpprefix";
            this.drpprefix.Size = new System.Drawing.Size(91, 23);
            this.drpprefix.TabIndex = 1;
            this.drpprefix.Leave += new System.EventHandler(this.drpprefix_Leave);
            // 
            // Createbtn
            // 
            this.Createbtn.BackColor = System.Drawing.Color.SteelBlue;
            this.Createbtn.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Createbtn.ForeColor = System.Drawing.Color.White;
            this.Createbtn.Location = new System.Drawing.Point(262, 352);
            this.Createbtn.Name = "Createbtn";
            this.Createbtn.Size = new System.Drawing.Size(119, 33);
            this.Createbtn.TabIndex = 12;
            this.Createbtn.Text = "Create Contact";
            this.Createbtn.UseVisualStyleBackColor = false;
            this.Createbtn.Click += new System.EventHandler(this.Createbtn_Click);
            // 
            // txtNote
            // 
            this.txtNote.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtNote.Location = new System.Drawing.Point(262, 228);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(405, 96);
            this.txtNote.TabIndex = 11;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label14.Location = new System.Drawing.Point(193, 228);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 19);
            this.label14.TabIndex = 85;
            this.label14.Text = "Notes :";
            // 
            // txtZipcode
            // 
            this.txtZipcode.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtZipcode.Location = new System.Drawing.Point(262, 186);
            this.txtZipcode.MaxLength = 15;
            this.txtZipcode.Name = "txtZipcode";
            this.txtZipcode.Size = new System.Drawing.Size(167, 23);
            this.txtZipcode.TabIndex = 10;
            this.txtZipcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtZipcode_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label13.Location = new System.Drawing.Point(170, 186);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 19);
            this.label13.TabIndex = 83;
            this.label13.Text = "Zip Code :";
            // 
            // drpEmail
            // 
            this.drpEmail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.drpEmail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpEmail.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drpEmail.FormattingEnabled = true;
            this.drpEmail.Items.AddRange(new object[] {
            "Home ",
            "Work",
            "Other"});
            this.drpEmail.Location = new System.Drawing.Point(558, 103);
            this.drpEmail.Name = "drpEmail";
            this.drpEmail.Size = new System.Drawing.Size(109, 23);
            this.drpEmail.TabIndex = 8;
            this.drpEmail.Leave += new System.EventHandler(this.drpEmail_Leave);
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtEmail.Location = new System.Drawing.Point(262, 103);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(292, 23);
            this.txtEmail.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.Location = new System.Drawing.Point(136, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 19);
            this.label10.TabIndex = 78;
            this.label10.Text = "Email Address :";
            // 
            // drpphone
            // 
            this.drpphone.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.drpphone.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drpphone.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.drpphone.FormattingEnabled = true;
            this.drpphone.Items.AddRange(new object[] {
            "Mobile",
            "Home",
            "Office",
            "Personal",
            "Other"});
            this.drpphone.Location = new System.Drawing.Point(558, 63);
            this.drpphone.Name = "drpphone";
            this.drpphone.Size = new System.Drawing.Size(109, 23);
            this.drpphone.TabIndex = 6;
            this.drpphone.Leave += new System.EventHandler(this.drpphone_Leave);
            // 
            // txtPhone
            // 
            this.txtPhone.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtPhone.Location = new System.Drawing.Point(359, 63);
            this.txtPhone.MaxLength = 18;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(195, 23);
            this.txtPhone.TabIndex = 5;
            this.txtPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPhone_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(127, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 19);
            this.label7.TabIndex = 73;
            this.label7.Text = "Phone Number :";
            // 
            // txtFirst
            // 
            this.txtFirst.Font = new System.Drawing.Font("Malgun Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.txtFirst.Location = new System.Drawing.Point(359, 26);
            this.txtFirst.Name = "txtFirst";
            this.txtFirst.Size = new System.Drawing.Size(116, 23);
            this.txtFirst.TabIndex = 2;
            this.txtFirst.Enter += new System.EventHandler(this.Txt_first_Enter);
            this.txtFirst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_first_KeyPress);
            this.txtFirst.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Txt_first_KeyUp);
            this.txtFirst.Leave += new System.EventHandler(this.Txt_first_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Malgun Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(192, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 19);
            this.label5.TabIndex = 70;
            this.label5.Text = "Name :";
            // 
            // lblHead
            // 
            this.lblHead.BackColor = System.Drawing.Color.SteelBlue;
            this.lblHead.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblHead.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblHead.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHead.ForeColor = System.Drawing.Color.White;
            this.lblHead.Location = new System.Drawing.Point(0, 0);
            this.lblHead.Name = "lblHead";
            this.lblHead.Size = new System.Drawing.Size(784, 39);
            this.lblHead.TabIndex = 21;
            this.lblHead.Text = "New Contact Info";
            this.lblHead.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AddContact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(784, 493);
            this.Controls.Add(this.lblHead);
            this.Controls.Add(this.NewContactPanel);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddContact";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Contact";
            this.Load += new System.EventHandler(this.AddContact_Load);
            this.NewContactPanel.ResumeLayout(false);
            this.NewContactPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel NewContactPanel;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtZipcode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox drpEmail;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox drpphone;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFirst;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Createbtn;
        private System.Windows.Forms.TextBox txtLast;
        private System.Windows.Forms.ComboBox drpprefix;
        private System.Windows.Forms.TextBox txtmiddle;
        private System.Windows.Forms.ComboBox drpCountryCode;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker txtdob;
        private System.Windows.Forms.Label lblHead;
    }
}