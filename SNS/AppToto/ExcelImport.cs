﻿using DataLayer;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for ExcelImport
/// </summary>
public class ExcelImport
{
    DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());
    
    public ExcelImport()
    { 
    }

    public DataTable ExcelImportSQL(string ExcelFileName, string path)
    {

        //string mainFile = path + @"\" + ExcelFileName.ToString(); ;
        //mainFile = mainFile.Replace(@"\\", @"\");
        ////Random rd = new Random(5);
        //string uploadFile = mainFile;
        //FileInfo fname = new FileInfo(uploadFile);


        //return GetSpreadsheetData(uploadFile);

        return new DataTable();
    }

    //private static DataTable GetSpreadsheetData(string filePath)
    //{
    //    List<ExpandoObject> data = new List<ExpandoObject>();

    //    using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(filePath, false))
    //    {
    //        // Get the worksheet we are working with
    //        WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
    //        IEnumerable<Sheet> sheets = spreadsheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
    //        WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(sheets.First().Id);
    //        Worksheet worksheet = worksheetPart.Worksheet;
    //        SharedStringTablePart sstPart = spreadsheetDocument.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
    //        SharedStringTable ssTable = sstPart.SharedStringTable;
    //        WorkbookStylesPart workbookStylesPart = spreadsheetDocument.WorkbookPart.GetPartsOfType<WorkbookStylesPart>().First();
    //        CellFormats cellFormats = (CellFormats)workbookStylesPart.Stylesheet.CellFormats;

    //        ExtractRowsData(data, worksheet, ssTable, cellFormats);
    //    }

    //    return ToDataTable(data);
    //}

    //public static DataTable ToDataTable(List<ExpandoObject> items)
    //{
    //    var data = items.ToArray();
    //    if (data.Count() == 0) return null;

    //    var dt = new DataTable();
    //    foreach (var key in ((IDictionary<string, object>)data[0]).Keys)
    //    {
    //        dt.Columns.Add(key);
    //    }
    //    foreach (var d in data)
    //    {
    //        dt.Rows.Add(((IDictionary<string, object>)d).Values.ToArray());
    //    }
    //    return dt;
    //}

    //private static void ExtractRowsData(List<ExpandoObject> data, Worksheet worksheet, SharedStringTable ssTable, CellFormats cellFormats)
    //{
    //    var columnHeaders = worksheet.Descendants<Row>().First().Descendants<Cell>().Select(c => Convert.ToString(ProcessCellValue(c, ssTable, cellFormats))).ToArray();
    //    var columnHeadersCellReference = worksheet.Descendants<Row>().First().Descendants<Cell>().Select(c => c.CellReference.InnerText.Replace("1", string.Empty)).ToArray();
    //    var spreadsheetData = from row in worksheet.Descendants<Row>()
    //                          where row.RowIndex > 1
    //                          select row;

    //    foreach (var dataRow in spreadsheetData)
    //    {
    //        dynamic row = new ExpandoObject();
    //        Cell[] rowCells = dataRow.Descendants<Cell>().ToArray();
    //        for (int i = 0; i < columnHeaders.Length; i++)
    //        {
    //            // Find and add the correct cell to the row object
    //            Cell cell = dataRow.Descendants<Cell>().Where(c => c.CellReference == columnHeadersCellReference[i] + dataRow.RowIndex).FirstOrDefault();
    //            //((IDictionary<String, Object>)row).Add(new KeyValuePair<String, Object>("Cell" + i.ToString(), ProcessCellValue(cell, ssTable, cellFormats)));

    //            if (cell != null)
    //            {
    //                //if (((IDictionary<String, Object>)row).Contains(new KeyValuePair<String, Object>(columnHeaders[i], ProcessCellValue(cell, ssTable, cellFormats))))
    //                //{
    //                //    cell.CellValue = new CellValue(cell.CellValue.Text.ToString() +"1");
    //                //}                    
    //                if (columnHeaders[i].Contains(columnHeaders[i].ToString()))
    //                {
    //                    ((IDictionary<String, Object>)row).Add(new KeyValuePair<String, Object>(columnHeaders[i] + i.ToString(), ProcessCellValue(cell, ssTable, cellFormats)));
    //                }
    //                else
    //                    ((IDictionary<String, Object>)row).Add(new KeyValuePair<String, Object>(columnHeaders[i], ProcessCellValue(cell, ssTable, cellFormats)));
    //            }
    //            else
    //            {
    //                ((IDictionary<String, Object>)row).Add("NewProp" + i, string.Empty);
    //            }
    //        }
    //        data.Add(row);

    //    }
    //}


    ///// <summary>
    ///// Process the valus of a cell and return a .NET value
    ///// </summary>
    ///// 

    //static Func<Cell, SharedStringTable, CellFormats, Object> ProcessCellValue = (c, ssTable, cellFormats) =>
    //{
    //    // If there is no data type, this must be a string that has been formatted as a number
    //    if (c.DataType == null)
    //    {
    //        CellFormat cf;
    //        if (c.StyleIndex == null)
    //        {
    //            cf = cellFormats.Descendants<CellFormat>().ElementAt<CellFormat>(0);
    //        }
    //        else
    //        {
    //            cf = cellFormats.Descendants<CellFormat>().ElementAt<CellFormat>(Convert.ToInt32(c.StyleIndex.Value));
    //        }
    //        ////if (cf.NumberFormatId >= 0 && cf.NumberFormatId <= 13) // This is a number
    //        ////{
    //        ////    if (c.CellValue != null)
    //        ////        return Convert.ToDecimal(c.CellValue.Text);
    //        ////    return string.Empty;
    //        ////}
    //        ////else
    //        if (cf.NumberFormatId >= 14 && cf.NumberFormatId <= 22) // This is a date
    //        {
    //            if (c.CellValue != null)
    //                return DateTime.FromOADate(Convert.ToDouble(c.CellValue.Text));
    //            return string.Empty;
    //        }
    //        else
    //        {
    //            if (c.CellValue != null)
    //                return c.CellValue.Text;
    //            return string.Empty;
    //        }
    //    }

    //    switch (c.DataType.Value)
    //    {
    //        case CellValues.SharedString:
    //            return ssTable.ChildElements[Convert.ToInt32(c.CellValue.Text)].InnerText;
    //        case CellValues.Boolean:
    //            return c.CellValue.Text == "1" ? true : false;
    //        case CellValues.Date:
    //            return DateTime.FromOADate(Convert.ToDouble(c.CellValue.Text));
    //        case CellValues.Number:
    //            return Convert.ToDecimal(c.CellValue.Text);
    //        default:
    //            if (c.CellValue != null)
    //                return c.CellValue.Text;
    //            return string.Empty;
    //    }
    //};
}