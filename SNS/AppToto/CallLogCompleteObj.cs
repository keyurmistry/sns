﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppToto
{
    class CallLogCompleteObj
    {
        public string first_page_uri { get; set; }
        public int end { get; set; }
        public List<callRecordBlock> calls { get; set; }
        public string previous_page_uri { get; set; }
        public string uri { get; set; }
        public int page_size { get; set; }
        public int start { get; set; }
        public string next_page_uri { get; set; }
        public int page { get; set; }
    }
}
