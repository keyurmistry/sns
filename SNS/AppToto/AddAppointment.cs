﻿using CalendarDemo;
using DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using ReminderSystem;
using ReminderSystem.Classes;

namespace AppToto
{
    public partial class AddAppointment : Form
    {
        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

        string today = DateTime.Now.ToString(Helper.UniversalDateFormat);

        string SMSTiming, CALLTiming, EMailTiming = string.Empty;


        public AddAppointment()
        {
            InitializeComponent();

        }

        public object GetComboBoxItem(int index)
        {
            if (index >= drphours.Items.Count)
                return null;

            return drphours.Items[index];
        }

        //public void GetTimeZone()
        //{
        //    TimeZone.DataSource = TimeZoneInfo.GetSystemTimeZones();//binding time zone list in drop down list
        //    TimeZone.ValueMember = "Id";
        //    TimeZone.DisplayMember = "DisplayName";
        //}

        private void AddParticipantbtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                DataGridViewSelectedRowCollection drs = grdParties.SelectedRows;

                foreach (DataGridViewRow item in drs)
                {


                    lblCaseNo.Text = item.Cells[0].Value.ToString();
                    lblselectionNo.Text = item.Cells[8].Value.ToString();
                    lblpartiesFName.Text = item.Cells[6].Value.ToString();
                    lblpartiesLName.Text = item.Cells[7].Value.ToString();

                    if (lblselectionNo.Text != "" && lblpartiesFName.Text != "" && lblpartiesLName.Text != "" && lblCaseNo.Text != "")
                    {

                        try
                        {
                            AddPartytoDirect(lblselectionNo.Text, lblpartiesFName.Text, lblpartiesLName.Text, lblCaseNo.Text);
                        }
                        catch (Exception ex)
                        {
                            Helper.AppLog.Error(ex.Message.ToString(), ex);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Please select at least one participants");
                    }
                }

            }
            catch (Exception ex)
            {
                Helper.AppLog.Error(ex.Message.ToString(), ex);
            }

            Cursor.Current = Cursors.Default;
        }

        public void AddPartytoDirect(string firmcode, string Name, string LName, string Caseno)
        {
            //* Note : firmcode = Cardcode

            DataSet dsInsert = new DataSet();
            SqlParameter[] p = new SqlParameter[5];



            p[0] = new SqlParameter("@firmcode", String.IsNullOrWhiteSpace(firmcode) ? (object)DBNull.Value : firmcode);
            p[1] = new SqlParameter("@Name", String.IsNullOrWhiteSpace(Name) ? (object)DBNull.Value : Name);
            p[2] = new SqlParameter("@LName", String.IsNullOrWhiteSpace(LName) ? (object)DBNull.Value : LName);
            p[3] = new SqlParameter("@Caseno", String.IsNullOrWhiteSpace(Caseno) ? (object)DBNull.Value : Caseno);
            p[4] = new SqlParameter("@MACID", clsGlobalDeclaration.ClientMac);




            string s = "Exec SP_TempPartiesADD '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "'";
            dsInsert = dal.GetData(s, Application.StartupPath);
            if (dsInsert.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                try
                {
                    GetAddedParties();
                }
                catch (Exception ex)
                {
                    Helper.AppLog.Error(ex.Message.ToString(), ex);
                }
            }
            else
            {
            }


        }



        public void GetParties()
        {

            DataSet dsgetA1Details = new DataSet();
            string s = "exec SP_GetEvent_Parties '" + txtSearch.Text + "'";
            dsgetA1Details = dal.GetData(s, Application.StartupPath);
            if (dsgetA1Details != null && dsgetA1Details.Tables != null && dsgetA1Details.Tables[0].Rows.Count > 0)
            {
                grdParties.DataSource = dsgetA1Details.Tables[0];
                grdParties.CurrentCell.Selected = false;

                grdParties.Columns[1].Visible = false;
                grdParties.Columns[2].Visible = false;
                grdParties.Columns[6].Visible = false;
                grdParties.Columns[7].Visible = false;
                grdParties.Columns[8].Visible = false;


                grdParties.Columns[0].Width = 50;
                //grdParties.Columns[2].Width = 50;
                //grdParties.Columns[3].Width = 150;
                //grdParties.Columns[4].Width = 150;
                //grdParties.Columns[5].Width = 150;
            }
            else
            {
                grdParties.DataSource = null;

            }


        }



        public void GetAddedParties()
        {
            DataSet dsgetA1 = new DataSet();
            string s = "exec SP_GetAdded_Parties 'Get','','" + clsGlobalDeclaration.ClientMac + "'";
            dsgetA1 = dal.GetData(s, Application.StartupPath);
            if (dsgetA1 != null && dsgetA1.Tables != null && dsgetA1.Tables[0].Rows.Count > 0)
            {
                grdaddedparties.Columns.Clear();

                DataGridViewImageColumn dgvremove = new DataGridViewImageColumn();
                dgvremove.Image = SynergyNotificationSystem.Properties.Resources.Remove;
                dgvremove.Width = 10;
                dgvremove.HeaderText = "";


                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                checkBoxColumn.HeaderText = "";
                checkBoxColumn.Width = 30;
                checkBoxColumn.Name = "";
                checkBoxColumn.ValueType = typeof(bool);


                grdaddedparties.Columns.Add(dgvremove);
                grdaddedparties.Columns.Add(checkBoxColumn);



                grdaddedparties.DataSource = dsgetA1.Tables[0];

                grdaddedparties.Columns[1].Visible = false;
                grdaddedparties.Columns[2].Visible = false;


                //DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                //checkBoxColumn.HeaderText = "";
                //checkBoxColumn.Width = 30;
                //checkBoxColumn.Name = "";
                //checkBoxColumn.ValueType = typeof(bool);
                //grdaddedparties.Columns.Insert(0, checkBoxColumn);

                for (int j = 0; j <= dsgetA1.Tables[0].Rows.Count - 1; j++)
                {
                    (grdaddedparties.Rows[j].Cells[1] as DataGridViewCheckBoxCell).Value = true;

                }


            }
            else
            {
                grdaddedparties.Columns.Clear();
                grdaddedparties.DataSource = null;

            }


        }

        private void grdParties_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            lblCaseNo.Text = grdParties.CurrentRow.Cells[0].Value.ToString();
            lblselectionNo.Text = grdParties.CurrentRow.Cells[8].Value.ToString();
            lblpartiesFName.Text = grdParties.CurrentRow.Cells[6].Value.ToString();
            lblpartiesLName.Text = grdParties.CurrentRow.Cells[7].Value.ToString();

        }

        private void grdParties_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            lblCaseNo.Text = grdParties.CurrentRow.Cells[0].Value.ToString();
            lblselectionNo.Text = grdParties.CurrentRow.Cells[8].Value.ToString();
            lblpartiesFName.Text = grdParties.CurrentRow.Cells[6].Value.ToString();
            lblpartiesLName.Text = grdParties.CurrentRow.Cells[7].Value.ToString();





        }

        private void grdParties_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                DataGridViewSelectedRowCollection drs = grdParties.SelectedRows;

                foreach (DataGridViewRow item in drs)
                {


                    lblCaseNo.Text = item.Cells[0].Value.ToString();
                    lblselectionNo.Text = item.Cells[8].Value.ToString();
                    lblpartiesFName.Text = item.Cells[6].Value.ToString();
                    lblpartiesLName.Text = item.Cells[7].Value.ToString();

                    if (lblselectionNo.Text != "" && lblpartiesFName.Text != "" && lblpartiesLName.Text != "" && lblCaseNo.Text != "")
                    {

                        try
                        {
                            AddPartytoDirect(lblselectionNo.Text, lblpartiesFName.Text, lblpartiesLName.Text, lblCaseNo.Text);
                        }
                        catch (Exception ex)
                        {
                            Helper.AppLog.Error(ex.Message.ToString(), ex);
                        }

                    }
                    else
                    {
                        MessageBox.Show("Please select at least one participants");
                    }
                }

            }
            catch (Exception ex)
            {
                Helper.AppLog.Error(ex.Message.ToString(), ex);
            }


            Cursor.Current = Cursors.Default;

        }

        private void grdaddedparties_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 1)
            {

                string IsstatusCheckValue = string.Empty;
                DataGridViewCheckBoxCell ch1 = new DataGridViewCheckBoxCell();
                ch1 = (DataGridViewCheckBoxCell)grdaddedparties.Rows[grdaddedparties.CurrentRow.Index].Cells[1];
                if (ch1.Value == null)
                {
                    ch1.Value = false;

                    if (ch1.Value.ToString() == "True")
                    {
                        IsstatusCheckValue = "0";
                    }
                    else
                    {
                        IsstatusCheckValue = "1";

                    }
                }

                UpdateAddedParties(grdaddedparties.Rows[e.RowIndex].Cells[2].Value.ToString(), IsstatusCheckValue);
            }
            if (e.ColumnIndex == 0)
            {
                RemoveAddedParties(grdaddedparties.Rows[e.RowIndex].Cells[2].Value.ToString());
            }

        }

        public void RemoveAddedParties(string SelectFirmCode)
        {
            DataSet dsgetA1DetailsRemove = new DataSet();
            string s = "exec SP_GetAdded_Parties 'Remove','" + SelectFirmCode + "','" + clsGlobalDeclaration.ClientMac + "'";
            dsgetA1DetailsRemove = dal.GetData(s, Application.StartupPath);
            if (dsgetA1DetailsRemove.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                GetAddedParties();
            }
            else
            {

            }


        }

        public void UpdateAddedParties(string UpdateFirmCode, string Status)
        {
            DataSet dssetparties = new DataSet();
            string s = "exec Sp_UpdateIsstatus_Parties '" + UpdateFirmCode + "','" + Status + "'";
            dssetparties = dal.GetData(s, Application.StartupPath);
            if (dssetparties.Tables[0].Rows[0]["Result"].ToString() == "1")
            {
                //GetAddedParties();
            }
            else
            {

            }


        }

        public static string SQLFix(string str)
        {
            if (str == "NA")
                str = "";
            return str.Replace("&nbsp;", "").Replace("'", "''");
        }

        private void Submiteventbtn_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;


            if ((txtEventBody.InnerText == null || txtEventBody.InnerText.Trim().Length == 0))
            {
                MessageBox.Show("Please enter reminder message", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEventBody.Focus();
                return;
            }

            if (!chkCALL.Checked && !chkEMAIL.Checked && !chkSMS.Checked)
            {
                MessageBox.Show("Please select atleast one reminder type", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                chkSMS.Focus();
                return;
            }
            if (drphours.SelectedIndex < 0)
            {
                MessageBox.Show("Please select Hours.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSearch.Focus();
                return;
            }
            if (drpMinute.SelectedIndex < 0)
            {
                MessageBox.Show("Please select Minutes.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSearch.Focus();
                return;
            }

            string TimeTT = string.Empty;
            TimeTT = GethourBind();

            if (TimeTT != "23")
            {
                if (drpAMPM.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select AM/PM Format.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtSearch.Focus();
                    return;
                }


            }
            else
            {

            }





            if (grdaddedparties.Rows.Count == 0)
            {
                MessageBox.Show("Please select atleast one participant", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtSearch.Focus();
                return;
            }

            bool isPartySelected = false;
            for (int i = 0; i < grdaddedparties.Rows.Count; i++)
            {
                if (Convert.ToBoolean(grdaddedparties.Rows[i].Cells[1].Value) == true)
                {
                    isPartySelected = true;
                    break;
                }
            }
            if (isPartySelected == false)
            {
                MessageBox.Show("Please select atleast one participant", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                grdaddedparties.Focus();
                return;
            }

            int SMS, CALL, EMAIL, IsPrivate, IsConfirmable = 0;

            if (chkSMS.Checked == true)
            {
                SMS = 1;
            }
            else
            {
                SMS = 0;
            }
            if (chkCALL.Checked == true)
            {
                CALL = 1;
            }
            else
            {
                CALL = 0;
            }
            if (chkEMAIL.Checked == true)
            {
                EMAIL = 1;
            }
            else
            {
                EMAIL = 0;
            }
            if (chkprivate.Checked == true)
            {
                IsPrivate = 1;
            }
            else
            {
                IsPrivate = 0;
            }
            if (chkIsConfirmable.Checked == true)
            {
                IsConfirmable = 1;
            }
            else
            {
                IsConfirmable = 0;
            }



            try
            {
                GetAllTimingWithComma();
            }
            catch (Exception ex)
            {
                Helper.AppLog.Error(ex.Message.ToString(), ex);
            }
            string ProductKey = string.Empty;
            try
            {


                ProductKey = dal.GetClientKey();

            }
            catch (Exception ex)
            {
                Helper.AppLog.Error(ex.Message.ToString(), ex);
            }

            if (Submiteventbtn.Text == "Submit Event")
            {
                // For Insert -------Start
                try
                {
                    if (fromdatepicker.Text != "")
                    {


                        DataSet dsInsertAppointment = new DataSet();
                        SqlParameter[] p = new SqlParameter[18];

                        System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                        //dateInfo.ShortDatePattern = "MM/dd/yyyy";
                        dateInfo.ShortDatePattern = Helper.UniversalDateFormat;

                        string TimeZoneTT, StartDatetime, EndDatetime = string.Empty;
                        TimeZoneTT = GethourBind();
                        if (TimeZoneTT == "12")
                        {
                            StartDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString() + " " + drpAMPM.SelectedItem.ToString();
                            EndDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString() + " " + drpAMPM.SelectedItem.ToString();
                        }
                        else
                        {   
                            StartDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString();
                            EndDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString();

                        }





                        p[0] = new SqlParameter("@EventStartDate", StartDatetime);
                        p[1] = new SqlParameter("@EventEndDate", EndDatetime);
                        p[2] = new SqlParameter("@Event", txtEventBody.InnerText.Replace(@"'", @"''"));
                        p[3] = new SqlParameter("@Location", locationtxt.Text.Replace(@"'", @"''"));
                        p[4] = new SqlParameter("@EventTitle", Titletxt.Text.Replace(@"'", @"''"));
                        p[5] = new SqlParameter("@SMS", SMS);
                        p[6] = new SqlParameter("@CALL", CALL);
                        p[7] = new SqlParameter("@EMAIL", EMAIL);
                        p[8] = new SqlParameter("@WhenSMS", SMSTiming);
                        p[9] = new SqlParameter("@WhenCALL", CALLTiming);
                        p[10] = new SqlParameter("@WhenEMAIL", EMailTiming);
                        p[11] = new SqlParameter("@EventHTML", txtEventBody.InnerHtml.Replace(@"'", @"''"));
                        p[12] = new SqlParameter("@ClientKey", ProductKey);
                        p[13] = new SqlParameter("@IsPrivate", IsPrivate);
                        p[14] = new SqlParameter("@CreatedUserFK", clsGlobalDeclaration.ClientMac);
                        p[15] = new SqlParameter("@IsConfirmable", IsConfirmable);
                        p[16] = new SqlParameter("@AppointmentType", txtappointmentType.Text.Replace(@"'", @"''"));
                        p[17] = new SqlParameter("@TwilioLang", (drpTwilioLang.SelectedItem as drpTwilioLangBind).Value.ToString());





                        string s = "Exec SP_Insesrt_Event '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "','" + p[9].Value + "','" + p[10].Value + "','" + p[11].Value + "','" + p[12].Value + "','" + p[13].Value + "','" + p[14].Value + "','" + p[15].Value + "','" + p[16].Value + "','" + p[17].Value + "'";
                        dsInsertAppointment = dal.GetData(s, Application.StartupPath);
                        if (dsInsertAppointment.Tables[0].Rows[0]["Result"].ToString() == "1")
                        {

                            try
                            {
                                CloudSyncReminderAdd();
                            }
                            catch (Exception ex)
                            {
                                Helper.AppLog.Error(ex.Message.ToString(), ex);
                            }

                            try
                            {
                                DataSet ClearParties = new DataSet();
                                ClearParties = dal.GetData("Exec Sp_ClearAddparties '" + clsGlobalDeclaration.ClientMac + "'", Application.StartupPath);
                            }
                            catch (Exception ex)
                            {
                                Helper.AppLog.Error(ex.Message.ToString(), ex);
                            }



                            MessageBox.Show("Event Save Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);


                            this.Close();

                            try
                            {
                                Main master = (Main)Application.OpenForms["Main"];
                                master.btnRefresh.PerformClick();
                            }
                            catch (Exception ex)
                            {
                                Helper.AppLog.Error(ex.Message.ToString(), ex);
                            }

                        }
                        else
                        {
                            MessageBox.Show("Error occur(s) during save Event.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);


                        }

                        this.Close();
                    }
                    else
                    {

                        MessageBox.Show("Please Fill the Details.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                catch (Exception ex)
                {
                    Helper.AppLog.Error(ex.Message.ToString(), ex);
                }

            }
            // For Insert -------END
            else if (Submiteventbtn.Text == "Update Event")
            {
                // For Update -------Start

                try
                {
                    if (fromdatepicker.Text != "")
                    {


                        DataSet dsUpdateAppointment = new DataSet();
                        SqlParameter[] p = new SqlParameter[20];

                        System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                        dateInfo.ShortDatePattern = Helper.UniversalDateFormat;


                        string TimeZoneTT, StartDatetime, EndDatetime = string.Empty;
                        TimeZoneTT = GethourBind();
                        if (TimeZoneTT == "12")
                        {
                            StartDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString() + " " + drpAMPM.SelectedItem.ToString();
                            EndDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString() + " " + drpAMPM.SelectedItem.ToString();
                        }
                        else
                        {
                            StartDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString();
                            EndDatetime = SQLFix(fromdatepicker.Text == "" ? Convert.ToString(fromdatepicker.Text) : Convert.ToDateTime(fromdatepicker.Text, dateInfo).ToString(Helper.UniversalDateFormat)) + " " + drphours.SelectedItem.ToString() + ":" + drpMinute.SelectedItem.ToString();

                        }




                        p[0] = new SqlParameter("@EventStartDate", StartDatetime);
                        p[1] = new SqlParameter("@EventEndDate", EndDatetime);
                        p[2] = new SqlParameter("@Event", txtEventBody.InnerText.Replace(@"'", @"''"));
                        p[3] = new SqlParameter("@Location", locationtxt.Text.Replace(@"'", @"''"));
                        p[4] = new SqlParameter("@EventTitle", Titletxt.Text.Replace(@"'", @"''"));
                        p[5] = new SqlParameter("@EventNo", Universal_EventNo);
                        p[6] = new SqlParameter("@CaseNoChange", Universal_PartiesCode);
                        p[7] = new SqlParameter("@SMS", SMS);
                        p[8] = new SqlParameter("@CALL", CALL);
                        p[9] = new SqlParameter("@EMAIL", EMAIL);
                        p[10] = new SqlParameter("@WhenSMS", SMSTiming);
                        p[11] = new SqlParameter("@WhenCALL", CALLTiming);
                        p[12] = new SqlParameter("@WhenEMAIL", EMailTiming);
                        p[13] = new SqlParameter("@EventHTML", txtEventBody.InnerHtml.Replace(@"'", @"''"));
                        p[14] = new SqlParameter("@ClientKey", ProductKey);
                        p[15] = new SqlParameter("@IsPrivate", IsPrivate);
                        p[16] = new SqlParameter("@CreatedUserFK", clsGlobalDeclaration.ClientMac);
                        p[17] = new SqlParameter("@IsConfirmable", IsConfirmable);
                        p[18] = new SqlParameter("@AppointmentType", txtappointmentType.Text.Replace(@"'", @"''"));
                        p[19] = new SqlParameter("@TwilioLang", (drpTwilioLang.SelectedItem as drpTwilioLangBind).Value.ToString());




                        string s = "Exec SP_Update_Event '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "','" + p[9].Value + "','" + p[10].Value + "','" + p[11].Value + "','" + p[12].Value + "','" + p[13].Value + "','" + p[14].Value + "','" + p[15].Value + "','" + p[16].Value + "','" + p[17].Value + "','" + p[18].Value + "','" + p[19].Value + "'";
                        dsUpdateAppointment = dal.GetData(s, Application.StartupPath);
                        if (dsUpdateAppointment.Tables[0].Rows[0]["Result"].ToString() == "1")
                        {

                            try
                            {
                                CloudSyncReminderUpdate();
                            }
                            catch (Exception ex)
                            {
                                Helper.AppLog.Error(ex.Message.ToString(), ex);
                            }


                            try
                            {
                                DataSet ClearParties = new DataSet();
                                ClearParties = dal.GetData("Sp_ClearAddparties '" + clsGlobalDeclaration.ClientMac + "'", Application.StartupPath);
                            }
                            catch (Exception ex)
                            {
                                Helper.AppLog.Error(ex.Message.ToString(), ex);
                            }

                            MessageBox.Show("Event update Successfully.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Information);



                            try
                            {
                                Main master = (Main)Application.OpenForms["Main"];
                                master.btnRefresh.PerformClick();
                            }
                            catch (Exception ex)
                            {
                                Helper.AppLog.Error(ex.Message.ToString(), ex);
                            }


                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Error occur(s) during Update Event.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Please Fill the Details.", Program.projectName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                catch (Exception ex)
                {
                    Helper.AppLog.Error(ex.Message.ToString(), ex);
                }


                // For Update -------End


            }

            else if (Submiteventbtn.Text == "Cancel")
            {

                try
                {
                    DataSet ClearParties = new DataSet();
                    ClearParties = dal.GetData("Sp_ClearAddparties '" + clsGlobalDeclaration.ClientMac + "'", Application.StartupPath);
                }
                catch (Exception ex)
                {
                    Helper.AppLog.Error(ex.Message.ToString(), ex);
                }

                this.Close();
            }

            Cursor.Current = Cursors.Default;
        }

        public static string Universal_PartiesCode, Universal_EventNo = string.Empty;

        private void AddAppointment_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            try { AddHour(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {
                GetTimingCheckbox();
            }
            catch (Exception ex)
            {
                Helper.AppLog.Error(ex.Message.ToString(), ex);
            }

            try { AddMinute(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            try { GetAddressbookDetails(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            try { getEmailTemplete(); }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

           

            try
            {
                getTwilioLang();
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }

            try
            {
                DataSet ClearParties = new DataSet();
                ClearParties = dal.GetData("Sp_ClearAddparties '" + clsGlobalDeclaration.ClientMac + "'", Application.StartupPath);
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {
                DateTime FromDate = System.DateTime.Now;
                DateTime ToDate = System.DateTime.Now;

                //int Findhour = FromDate.Hour;
                //int FindTohour = ToDate.Hour;
                int Findhour = ((FromDate.Hour + 11) % 12) + 1; // convert date 24 hours to 12 hour format
                int FindTohour = ((ToDate.Hour + 11) % 12) + 1; // convert date 24 hours to 12 hour format

                string FindAmPm = FromDate.ToString("tt");

                int FindMinute = FromDate.Minute;
                int FindToMinute = ToDate.Minute;

                fromdatepicker.Text = FromDate.ToString();
                enddatepicker.Text = ToDate.ToString();

                drphours.SelectedIndex = Findhour;
                drpToHour.SelectedIndex = FindTohour;

                drpMinute.SelectedIndex = FindMinute;
                drpToMinute.SelectedIndex = FindToMinute;

                //drpAMPM.SelectedValue = FindAmPm;
                drpAMPM.SelectedIndex = drpAMPM.Items.IndexOf(FindAmPm);
                //drpAMPM.SelectedText = FindAmPm;


            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }



            try
            {
                getTemplateField();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {
                GetTimingCheckbox();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);};


            if (Main.App_MainWhere == "MainPage")
            {
                try
                {
                    GetEdit();
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }


            if (DemoForm.App_DemoWhere == "DemoPage")
            {
                try
                {
                    GetEditByCalendar();
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }

            if (Main.ret_TwilioLog == "TwilioLogPage")
            {
                try
                {
                    GetEditByTwilioLog();
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }


            try
            {
                Main.App_MainWhere = "";
                DemoForm.App_DemoWhere = "";
                Main.ret_TwilioLog = "";
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            if (Main.AutoSearchComplete != "")
            {
                try
                {
                    GetParties();
                
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
            }


            if (Main.Cont_Flag == "RecordAddFromContact")
            {
                try
                {
                    AddPartytoDirect(Main.Cont_FirmCode, Main.Cont_FirstName, Main.Cont_LastName, Main.Cont_CaseNo);

                    Clear_ContactDetails();
                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
            }


            Cursor.Current = Cursors.Default;

        }
        public void Clear_ContactDetails()
        {
            Main.Cont_FirmCode = "";
            Main.Cont_FirstName = "";
            Main.Cont_LastName = "";
            Main.Cont_CaseNo = "";

        }

        //public void AddHour()
        //{
        //    for (int i = 0; i <= 23; i++)
        //    {
        //        string Hour = string.Empty;
        //        if (i < 10)
        //        {
        //            Hour = Convert.ToString("0" + i);
        //        }
        //        else
        //        {
        //            Hour = Convert.ToString(i);
        //        }

        //        drphours.Items.Add(Hour);
        //        drpToHour.Items.Add(Hour);


        //    }
        //}
        public void AddHour()
        {
            int HourBind = 0;
            string TimeTT = string.Empty;

            TimeTT = GethourBind();

            if (TimeTT != "")
            {
                HourBind = Convert.ToInt32(TimeTT);

                if (HourBind == 23)
                {

                    lblAMPM.Visible = false;
                    drpAMPM.SelectedValue = "";
                    drpAMPM.Visible = false;
                }
                else
                {
                    lblAMPM.Visible = true;
                    drpAMPM.Visible = true;
                }

            }
            else
            {

            }

            for (int i = 0; i <= HourBind; i++)
            {
                string Hour = string.Empty;
                if (i < 10)
                {
                    Hour = Convert.ToString("0" + i);
                }
                else
                {
                    Hour = Convert.ToString(i);
                }

                drphours.Items.Add(Hour);
                drpToHour.Items.Add(Hour);


            }
        }

        public void AddMinute()
        {
            for (int i = 0; i <= 59; i++)
            {
                string Minute = string.Empty;
                if (i < 10)
                {
                    Minute = Convert.ToString("0" + i);
                }
                else
                {
                    Minute = Convert.ToString(i);
                }

                drpMinute.Items.Add(Minute);
                drpToMinute.Items.Add(Minute);


            }
        }

        public void GetEdit()
        {
            //Added by MIP on 11-Jun-2016
            this.Text = "Update Reminder";
            Titletxt.Text = Main.App_SetValueForTitle;

            locationtxt.Text = Main.App_SetValueForLocation;

            try
            {
                GetTimingCheckbox();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);};

            fromdatepicker.Text = Main.App_SetValueForTimestart;
            drphours.SelectedIndex = Convert.ToInt32(Main.App_SetValueForTimestartHour);
            drpMinute.SelectedIndex = Convert.ToInt32(Main.App_SetValueForTimestartMinute);


            enddatepicker.Text = Main.App_SetValueForTimeEnd;
            drpToHour.SelectedIndex = Convert.ToInt32(Main.App_SetValueForTimeEndHour);
            drpToMinute.SelectedIndex = Convert.ToInt32(Main.App_SetValueForTimeEndMinute);

            //drpAMPM.SelectedText = Main.App_AMPM;
            drpAMPM.SelectedIndex = drpAMPM.FindStringExact(Main.App_AMPM);

            Submiteventbtn.Text = Main.App_SetValueForBtnsubmit;

            lblselectionNo.Text = Main.App_SetValueForFirmCode;
            lblpartiesFName.Text = Main.App_SetValueForFname;
            lblpartiesLName.Text = Main.App_SetValueForLname;
            lblCaseNo.Text = Main.App_SetValueForCaseNo;

            bodytxt.Text = Main.App_SetValueForEventBody;
            txtEventBody.InnerHtml = Main.App_SetValueForEventHTMLBody;


            Universal_PartiesCode = Main.App_PartiesCode;
            Universal_EventNo = Main.App_SetValueForEventNo;

            drpTwilioLang.SelectedIndex = drpTwilioLang.FindStringExact(Main.App_TwilioLanguage);
            txtappointmentType.Text = Main.App_AppointMentTempType;




            if (Main.App_SMS == "1")
            {
                chkSMS.Checked = true;
            }
            else
            {
                chkSMS.Checked = false;
            }

            if (Main.APP_Call == "1")
            {
                chkCALL.Checked = true;
            }
            else
            {
                chkCALL.Checked = false;
            }
            if (Main.APP_Email == "1")
            {
                chkEMAIL.Checked = true;
            }
            else
            {
                chkEMAIL.Checked = false;
            }

            if (Main.App_Private == "1")
            {
                chkprivate.Checked = true;
            }
            else
            {
                chkprivate.Checked = false;
            }

            if (Main.App_IsConfirmable == "1")
            {
                chkIsConfirmable.Checked = true;
            }
            else
            {
                chkIsConfirmable.Checked = false;
            }




            try
            {

                if (Main.App_WhenSMS != "")
                {
                    string[] SMS = Main.App_WhenSMS.Split(',');

                    foreach (string word in SMS)
                    {

                        foreach (DataGridViewRow row in grdSMS.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (Main.App_WhenCALL != "")
                {
                    string[] CALL = Main.App_WhenCALL.Split(',');

                    foreach (string word1 in CALL)
                    {

                        foreach (DataGridViewRow row in grdCALL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word1))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (Main.App_WhenEMAIL != "")
                {
                    string[] CALL = Main.App_WhenEMAIL.Split(',');

                    foreach (string word2 in CALL)
                    {

                        foreach (DataGridViewRow row in grdEMAIL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word2))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            try
            {
                AddPartytoDirect(lblselectionNo.Text, lblpartiesFName.Text, lblpartiesLName.Text, lblCaseNo.Text);
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }


        }

        private void getEmailTemplete()
        {
            DataSet dsTemplateField = new DataSet();
            string s = "EXEC GetTemplete";
            dsTemplateField = dal.GetData(s, Application.StartupPath);
            if (dsTemplateField != null && dsTemplateField.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsTemplateField.Tables[0].Rows.Count; i++)
                {

                    locationtxt.Items.Add(dsTemplateField.Tables[0].Rows[i][1].ToString());
                }
            }
            else
            {

            }

        }

        private void getWithTempleteBody(string TempleteName)
        {
            TempleteName = TempleteName.Replace(@"'", @"''");



            DataSet dsTemplateField = new DataSet();

            dsTemplateField.Clear();



            string s = "EXEC GetTempleteWithBOdy '" + TempleteName + "'";
            dsTemplateField = dal.GetData(s, Application.StartupPath);
            if (dsTemplateField != null && dsTemplateField.Tables[0].Rows.Count > 0)
            {
                bodytxt.Text = dsTemplateField.Tables[0].Rows[0][0].ToString();
                txtEventBody.InnerHtml = dsTemplateField.Tables[0].Rows[0][0].ToString();
                txtappointmentType.Text = dsTemplateField.Tables[0].Rows[0][1].ToString();
                drpTwilioLang.SelectedIndex = drpTwilioLang.FindStringExact(dsTemplateField.Tables[0].Rows[0][2].ToString());


                if (dsTemplateField.Tables[0].Rows[0]["isSMS"].ToString() == "1")
                {
                    chkSMS.Checked = true;
                }
                else
                {
                    chkSMS.Checked = false;
                }

                if (dsTemplateField.Tables[0].Rows[0]["isCALL"].ToString() == "1")
                {
                    chkCALL.Checked = true;
                }
                else
                {
                    chkCALL.Checked = false;
                }
                if (dsTemplateField.Tables[0].Rows[0]["isEMAIL"].ToString() == "1")
                {
                    chkEMAIL.Checked = true;
                }
                else
                {
                    chkEMAIL.Checked = false;
                }

                if (dsTemplateField.Tables[0].Rows[0]["IsReschedule"].ToString() == "1")
                {
                    chkIsConfirmable.Checked = true;
                }
                else
                {
                    chkIsConfirmable.Checked = false;
                }

                try
                {
                    GetUpdatetime(dsTemplateField.Tables[0].Rows[0]["whenSMS"].ToString(), dsTemplateField.Tables[0].Rows[0]["whenCALL"].ToString(), dsTemplateField.Tables[0].Rows[0]["whenEMAIL"].ToString());
                }

                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }

            }
            else
            {

            }



        }

        public void GetUpdatetime(string SMSComma, string CALLComma, string EMAILComma)
        {

            try
            {

                if (SMSComma != "")
                {
                    string[] SMS = SMSComma.Split(',');

                    foreach (string word in SMS)
                    {

                        foreach (DataGridViewRow row in grdSMS.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (CALLComma != "")
                {
                    string[] CALL = CALLComma.Split(',');

                    foreach (string word1 in CALL)
                    {

                        foreach (DataGridViewRow row in grdCALL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word1))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (EMAILComma != "")
                {
                    string[] CALL = EMAILComma.Split(',');

                    foreach (string word2 in CALL)
                    {

                        foreach (DataGridViewRow row in grdEMAIL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word2))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }

        private void locationtxt_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetTimingCheckbox();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            try
            {
                getWithTempleteBody(locationtxt.SelectedItem.ToString());
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        public void AddCasePartiesall()
        {

            if (Main.App_SetValueForCaseNo != "" && Main.App_SetValueForTimestart != "")
            {
                DataSet dscaseallparties = new DataSet();
                string s = "EXEC Sp_CaseWiseParties '" + Main.App_SetValueForCaseNo + "','" + Main.App_SetValueForTimestart + "'";
                dscaseallparties = dal.GetData(s, Application.StartupPath);
                if (dscaseallparties != null && dscaseallparties.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i <= dscaseallparties.Tables[0].Rows.Count - 1; i++)
                    {


                        try
                        {
                            AddPartytoDirect(dscaseallparties.Tables[0].Rows[i]["firmcode"].ToString(), dscaseallparties.Tables[0].Rows[i]["Name"].ToString(), dscaseallparties.Tables[0].Rows[i]["LName"].ToString(), dscaseallparties.Tables[0].Rows[i]["CaseNo"].ToString());
                        }
                        catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
                    }




                }
                else
                {
                    grdaddedparties.DataSource = null;
                }
            }
            else
            { }

            try
            {
                GetAddedParties();
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
        }

        private void getTemplateField()
        {
            DataSet dsTemplateField = new DataSet();
            string s = "EXEC GetTemplate";
            dsTemplateField = dal.GetData(s, Application.StartupPath);
            if (dsTemplateField != null && dsTemplateField.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsTemplateField.Tables[0].Rows.Count; i++)
                {
                    drptemplete.Items.Add(dsTemplateField.Tables[0].Rows[i][1].ToString());

                }
            }

            else
            {

            }

        }

        private void drptemplete_SelectedIndexChanged(object sender, EventArgs e)
        {
            bodytxt.Text += drptemplete.Text;
            txtEventBody.InnerHtml += drptemplete.Text;
        }



        private void label15_Click(object sender, EventArgs e)
        {

        }

        public void GetTimingCheckbox()
        {

            DataSet dsgetA1DetailsTiming = new DataSet();
            string s = "exec GetSMStiming";
            dsgetA1DetailsTiming = dal.GetData(s, Application.StartupPath);
            if (dsgetA1DetailsTiming != null && dsgetA1DetailsTiming.Tables != null && dsgetA1DetailsTiming.Tables[0].Rows.Count > 0)
            {
                //SMS Grid Bind
                try
                {
                    grdapplyAll.Columns.Clear();
                    grdSMS.Columns.Clear();
                    grdCALL.Columns.Clear();
                    grdEMAIL.Columns.Clear();
                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }

                try
                {
                    grdapplyAll.DataSource = dsgetA1DetailsTiming.Tables[0];

                    grdapplyAll.Columns[0].Visible = false;
                    grdapplyAll.Columns[1].Visible = false;
                    grdapplyAll.Columns[1].Width = 70;
                    grdapplyAll.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                    checkBoxColumn.HeaderText = "";
                    checkBoxColumn.Width = 30;
                    checkBoxColumn.Name = "";
                    checkBoxColumn.ValueType = typeof(bool);
                    grdapplyAll.Columns.Insert(0, checkBoxColumn);

                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }


                try
                {
                    grdSMS.DataSource = dsgetA1DetailsTiming.Tables[0];
                    grdSMS.CurrentCell.Selected = false;
                    grdSMS.Columns[0].Visible = false;
                    grdSMS.Columns[1].Width = 70;
                    grdSMS.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                    checkBoxColumn.HeaderText = "";
                    checkBoxColumn.Width = 30;
                    checkBoxColumn.Name = "";
                    checkBoxColumn.ValueType = typeof(bool);
                    grdSMS.Columns.Insert(0, checkBoxColumn);

                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
                //CALL Grid Bind
                try
                {
                    grdCALL.DataSource = dsgetA1DetailsTiming.Tables[0];
                    grdCALL.CurrentCell.Selected = false;
                    grdCALL.Columns[0].Visible = false;
                    grdCALL.Columns[1].Width = 70;
                    grdCALL.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn1 = new DataGridViewCheckBoxColumn();
                    checkBoxColumn1.HeaderText = "";
                    checkBoxColumn1.Width = 30;
                    checkBoxColumn1.Name = "";
                    checkBoxColumn1.ValueType = typeof(bool);
                    grdCALL.Columns.Insert(0, checkBoxColumn1);
                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
                //EMAIL Grid Bind

                try
                {
                    grdEMAIL.DataSource = dsgetA1DetailsTiming.Tables[0];
                    grdEMAIL.CurrentCell.Selected = false;
                    grdEMAIL.Columns[0].Visible = false;
                    grdEMAIL.Columns[1].Width = 70;
                    grdEMAIL.Columns[1].ReadOnly = true;
                    DataGridViewCheckBoxColumn checkBoxColumn2 = new DataGridViewCheckBoxColumn();
                    checkBoxColumn2.HeaderText = "";
                    checkBoxColumn2.Width = 30;
                    checkBoxColumn2.Name = "";
                    checkBoxColumn2.ValueType = typeof(bool);
                    grdEMAIL.Columns.Insert(0, checkBoxColumn2);
                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }


            }
            else
            {

            }



        }
        public void GetAllTimingWithComma()
        {
            try //SMS Values GET
            {

                List<DataGridViewRow> selectedRows = (from row in grdSMS.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[0].Value) == true
                                                      select row).ToList();
                string StrSMS = string.Empty;

                foreach (DataGridViewRow row in selectedRows)
                {
                    StrSMS += row.Cells[1].Value.ToString() + ",";
                    SMSTiming = StrSMS;
                    SMSTiming = SMSTiming.Remove(SMSTiming.Length - 1);

                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
            try //CALL Values GET
            {

                List<DataGridViewRow> selectedRows = (from row in grdCALL.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[0].Value) == true
                                                      select row).ToList();
                string StrCALL = string.Empty;

                foreach (DataGridViewRow row in selectedRows)
                {
                    StrCALL += row.Cells[1].Value.ToString() + ",";
                    CALLTiming = StrCALL;
                    CALLTiming = CALLTiming.Remove(CALLTiming.Length - 1);

                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
            try //EMAIL Values GET
            {

                List<DataGridViewRow> selectedRows = (from row in grdEMAIL.Rows.Cast<DataGridViewRow>()
                                                      where Convert.ToBoolean(row.Cells[0].Value) == true
                                                      select row).ToList();
                string StrEMAIL = string.Empty;

                foreach (DataGridViewRow row in selectedRows)
                {
                    StrEMAIL += row.Cells[1].Value.ToString() + ",";
                    EMailTiming = StrEMAIL;
                    EMailTiming = EMailTiming.Remove(EMailTiming.Length - 1);

                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }

        }

        private void grdapplyAll_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {

                DataGridViewCheckBoxCell checkbox = (DataGridViewCheckBoxCell)grdapplyAll.CurrentCell;
                try
                {
                    bool isChecked = (bool)checkbox.EditedFormattedValue;

                    if (isChecked == true)
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = true;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = true;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = true;
                    }
                    else
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = false;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = false;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = false;
                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            }



        }

        private void grdSMS_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {

                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;

                    if (isSMS == true)
                    {
                        if (isCALL == true && isEMAIL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }
                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

                try //SMS Values GET
                {

                    List<DataGridViewRow> selectedSMS = (from row in grdSMS.Rows.Cast<DataGridViewRow>()
                                                         where Convert.ToBoolean(row.Cells[0].Value) == true
                                                         select row).ToList();


                    if (selectedSMS.Count > 0)
                    {
                        chkSMS.Checked = true;
                    }
                    else
                    {
                        chkSMS.Checked = false;
                    }


                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }

            }






        }

        private void grdCALL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {


                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;


                    if (isCALL == true)
                    {
                        if (isSMS == true && isEMAIL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }


                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                try
                {

                    List<DataGridViewRow> selectedCALL = (from row in grdCALL.Rows.Cast<DataGridViewRow>()
                                                          where Convert.ToBoolean(row.Cells[0].Value) == true
                                                          select row).ToList();


                    if (selectedCALL.Count > 0)
                    {
                        chkCALL.Checked = true;
                    }
                    else
                    {
                        chkCALL.Checked = false;
                    }


                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
            }


        }

        private void grdEMAIL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {


                try
                {
                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    bool isSMS = (bool)checkboxSMS.EditedFormattedValue;
                    bool isCALL = (bool)checkboxCALL.EditedFormattedValue;
                    bool isEMAIL = (bool)checkboxEMAIL.EditedFormattedValue;


                    if (isEMAIL == true)
                    {
                        if (isSMS == true && isCALL == true)
                        {
                            grdapplyAll.Rows[e.RowIndex].Cells[0].Value = true;
                        }
                    }
                    else
                    {
                        grdapplyAll.Rows[e.RowIndex].Cells[0].Value = false;

                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


                try
                {

                    List<DataGridViewRow> selectedEMAIL = (from row in grdEMAIL.Rows.Cast<DataGridViewRow>()
                                                           where Convert.ToBoolean(row.Cells[0].Value) == true
                                                           select row).ToList();


                    if (selectedEMAIL.Count > 0)
                    {
                        chkEMAIL.Checked = true;
                    }
                    else
                    {
                        chkEMAIL.Checked = false;
                    }


                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
            }



        }

        public void GetEditByCalendar()
        {
            //Added by MIP on 11-Jun-2016
            this.Text = "Update Reminder";
            Titletxt.Text = DemoForm.App_SetValueForTitle;

            locationtxt.Text = DemoForm.App_SetValueForLocation;

            try
            {
                GetTimingCheckbox();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);};

            fromdatepicker.Text = DemoForm.App_SetValueForTimestart;
            drphours.SelectedIndex = Convert.ToInt32(DemoForm.App_SetValueForTimestartHour);
            drpMinute.SelectedIndex = Convert.ToInt32(DemoForm.App_SetValueForTimestartMinute);


            enddatepicker.Text = DemoForm.App_SetValueForTimeEnd;
            drpToHour.SelectedIndex = Convert.ToInt32(DemoForm.App_SetValueForTimeEndHour);
            drpToMinute.SelectedIndex = Convert.ToInt32(DemoForm.App_SetValueForTimeEndMinute);

            drpAMPM.SelectedItem = DemoForm.App_AMPM;

            Submiteventbtn.Text = DemoForm.App_SetValueForBtnsubmit;

            lblselectionNo.Text = DemoForm.App_SetValueForFirmCode;
            lblpartiesFName.Text = DemoForm.App_SetValueForFname;
            lblpartiesLName.Text = DemoForm.App_SetValueForLname;
            lblCaseNo.Text = DemoForm.App_SetValueForCaseNo;


            txtEventBody.InnerHtml = DemoForm.App_SetValueForEventHTMLBody;


            Universal_PartiesCode = DemoForm.App_PartiesCode;
            Universal_EventNo = DemoForm.App_SetValueForEventNo;


            drpTwilioLang.SelectedIndex = drpTwilioLang.FindStringExact(DemoForm.App_TwilioLang);

            if (DemoForm.App_SMS == "1")
            {
                chkSMS.Checked = true;
            }
            else
            {
                chkSMS.Checked = false;
            }

            if (DemoForm.APP_Call == "1")
            {
                chkCALL.Checked = true;
            }
            else
            {
                chkCALL.Checked = false;
            }
            if (DemoForm.APP_Email == "1")
            {
                chkEMAIL.Checked = true;
            }
            else
            {
                chkEMAIL.Checked = false;
            }

            if (DemoForm.App_Private == "1")
            {
                chkprivate.Checked = true;
            }
            else
            {
                chkprivate.Checked = false;
            }
            if (DemoForm.App_IsConfirmable == "1")
            {
                chkIsConfirmable.Checked = true;
            }
            else
            {
                chkIsConfirmable.Checked = false;
            }




            try
            {

                if (DemoForm.App_WhenSMS != "")
                {
                    string[] SMS = DemoForm.App_WhenSMS.Split(',');

                    foreach (string word in SMS)
                    {

                        foreach (DataGridViewRow row in grdSMS.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (DemoForm.App_WhenCALL != "")
                {
                    string[] CALL = DemoForm.App_WhenCALL.Split(',');

                    foreach (string word1 in CALL)
                    {

                        foreach (DataGridViewRow row in grdCALL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word1))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (DemoForm.App_WhenEMAIL != "")
                {
                    string[] CALL = DemoForm.App_WhenEMAIL.Split(',');

                    foreach (string word2 in CALL)
                    {

                        foreach (DataGridViewRow row in grdEMAIL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word2))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            try
            {
                AddPartytoDirect(lblselectionNo.Text, lblpartiesFName.Text, lblpartiesLName.Text, lblCaseNo.Text);
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }


        }


        public void GetEditByTwilioLog()
        {

            Titletxt.Text = Main.ret_SetValueForTitle;

            locationtxt.Text = Main.ret_SetValueForLocation;

            try
            {
                GetTimingCheckbox();
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);};

            fromdatepicker.Text = Main.ret_SetValueForTimestart;
            drphours.SelectedIndex = Convert.ToInt32(Main.ret_SetValueForTimestartHour);
            drpMinute.SelectedIndex = Convert.ToInt32(Main.ret_SetValueForTimestartMinute);


            enddatepicker.Text = Main.ret_SetValueForTimeEnd;
            drpToHour.SelectedIndex = Convert.ToInt32(Main.ret_SetValueForTimeEndHour);
            drpToMinute.SelectedIndex = Convert.ToInt32(Main.ret_SetValueForTimeEndMinute);
            drpAMPM.SelectedItem = Main.ret_AMPM;

            Submiteventbtn.Text = Main.ret_SetValueForBtnsubmit;
            Submiteventbtn.Visible = false;
            lblselectionNo.Text = Main.ret_SetValueForFirmCode;
            lblpartiesFName.Text = Main.ret_SetValueForFname;
            lblpartiesLName.Text = Main.ret_SetValueForLname;
            lblCaseNo.Text = Main.ret_SetValueForCaseNo;

            //bodytxt.Text = Main.ret_SetValueForEventBody;
            //bodytxt.Text = Main.ret_SetValueForEventBody;
            txtEventBody.InnerHtml = Main.ret_SetValueForEventHTMLBody;



            drpTwilioLang.SelectedIndex = drpTwilioLang.FindStringExact(Main.ret_TwilioLang);

            if (Main.ret_SMS == "1")
            {
                chkSMS.Checked = true;
            }
            else
            {
                chkSMS.Checked = false;
            }

            if (Main.ret_Call == "1")
            {
                chkCALL.Checked = true;
            }
            else
            {
                chkCALL.Checked = false;
            }
            if (Main.ret_Email == "1")
            {
                chkEMAIL.Checked = true;
            }
            else
            {
                chkEMAIL.Checked = false;
            }
            if (Main.ret_Private == "1")
            {
                chkprivate.Checked = true;
            }
            else
            {
                chkprivate.Checked = false;
            }
            if (Main.ret_IsConfirmable == "1")
            {
                chkIsConfirmable.Checked = true;
            }
            else
            {
                chkIsConfirmable.Checked = false;
            }



            try
            {

                if (Main.ret_WhenSMS != "")
                {
                    string[] SMS = Main.ret_WhenSMS.Split(',');

                    foreach (string word in SMS)
                    {

                        foreach (DataGridViewRow row in grdSMS.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (Main.ret_WhenCALL != "")
                {
                    string[] CALL = Main.ret_WhenCALL.Split(',');

                    foreach (string word1 in CALL)
                    {

                        foreach (DataGridViewRow row in grdCALL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word1))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

            try
            {

                if (Main.ret_WhenEMAIL != "")
                {
                    string[] CALL = Main.ret_WhenEMAIL.Split(',');

                    foreach (string word2 in CALL)
                    {

                        foreach (DataGridViewRow row in grdEMAIL.Rows)
                        {
                            if (Convert.ToInt32(row.Cells[1].Value) == Convert.ToInt32(word2))
                            {
                                row.Cells[0].Value = true;
                            }


                        }
                    }

                }
            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}


            try
            {
                AddPartytoDirect(lblselectionNo.Text, lblpartiesFName.Text, lblpartiesLName.Text, lblCaseNo.Text);
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }


        }

        private void grdapplyAll_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                Cursor.Current = Cursors.WaitCursor;

                DataGridViewCheckBoxCell checkbox = (DataGridViewCheckBoxCell)grdapplyAll.CurrentCell;


                try
                {
                    bool isChecked = (bool)checkbox.EditedFormattedValue;

                    DataGridViewCheckBoxCell checkboxSMS = (DataGridViewCheckBoxCell)grdSMS.CurrentCell;
                    DataGridViewCheckBoxCell checkboxCALL = (DataGridViewCheckBoxCell)grdCALL.CurrentCell;
                    DataGridViewCheckBoxCell checkboxEMAIL = (DataGridViewCheckBoxCell)grdEMAIL.CurrentCell;

                    if (isChecked == true)
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = true;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = true;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = true;
                    }
                    else
                    {
                        grdSMS.Rows[e.RowIndex].Cells[0].Value = false;
                        grdCALL.Rows[e.RowIndex].Cells[0].Value = false;
                        grdEMAIL.Rows[e.RowIndex].Cells[0].Value = false;
                    }
                }
                catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                Cursor.Current = Cursors.Default;

            }
        }

        private void grdSMS_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in this.grdSMS.Rows)
                {
                    if (((bool)grdSMS.Rows[e.RowIndex].Cells[0].Value))
                    {
                        chkSMS.Checked = true;
                    }

                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }




        }

        private void grdSMS_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {

                if (grdSMS.IsCurrentCellDirty)
                {
                    grdSMS.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }

        }

        private void grdCALL_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in this.grdCALL.Rows)
                {
                    if (((bool)grdCALL.Rows[e.RowIndex].Cells[0].Value))
                    {
                        chkCALL.Checked = true;
                    }

                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
        }

        private void grdCALL_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {

                if (grdCALL.IsCurrentCellDirty)
                {
                    grdCALL.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
        }

        private void chkSMS_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSMS.Checked == false)
            {
                for (int i = 0; i < grdSMS.Rows.Count - 1; i++)
                {
                    grdSMS.Rows[i].Cells[0].Value = false;
                }
            }
        }

        private void chkCALL_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCALL.Checked == false)
            {
                for (int i = 0; i < grdCALL.Rows.Count - 1; i++)
                {
                    grdCALL.Rows[i].Cells[0].Value = false;
                }
            }
        }

        private void chkEMAIL_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEMAIL.Checked == false)
            {
                for (int i = 0; i < grdEMAIL.Rows.Count - 1; i++)
                {
                    grdEMAIL.Rows[i].Cells[0].Value = false;
                }
            }
        }

        private void grdEMAIL_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                foreach (DataGridViewRow row in this.grdEMAIL.Rows)
                {
                    if (((bool)grdEMAIL.Rows[e.RowIndex].Cells[0].Value))
                    {
                        chkEMAIL.Checked = true;
                    }

                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
        }

        private void grdEMAIL_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            try
            {

                if (grdEMAIL.IsCurrentCellDirty)
                {
                    grdEMAIL.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
        }

        private void txtEventBody_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = (e.KeyChar == (char)Keys.Space) || (e.KeyChar == '\'');
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;


            if (e.KeyCode == Keys.Enter)
            {

                string FinalADD = string.Empty;

                FinalADD = txtSearch.Text.Trim();

                if (FinalADD == "" || FinalADD == string.Empty)
                {
                    MessageBox.Show("Please Enter Keyword First.");
                }
                else
                {

                    try
                    {
                        GetParties();

                    }


                    catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
                }
            }
            Cursor.Current = Cursors.Default;
        }

        public void ClearContact()
        {
            Main.Con_SetValueForCardCode = "";
            Main.Con_SetValueForFrimCode = "";
            Main.Con_SetValueForSuffex = "";
            Main.Con_SetValueForFName = "";
            Main.Con_SetValueForMName = "";
            Main.Con_SetValueForLName = "";
            Main.Con_SetValueForEmail = "";
            Main.Con_SetValueForPhoneNo = "";
            Main.Con_SetValueForDOB = "";
            Main.Con_SetValueForZipCode = "";
            Main.Con_SetValueForNote = "";
            Main.Con_SetValueForCountryCode = "";
            Main.Con_SetValueForMobileType = "";
            Main.Con_SetValueForEmailType = "";
            Main.Con_SetValueForButton = "Create Contact";
        }
        private void lnkAddContact_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                ClearContact();
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }

            try
            {
                AddContact addcontact = new AddContact();
                addcontact.ShowDialog();
            }


            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        public void CloudSyncReminderAdd()
        {
            try
            {
                DataSet dsCloudSync = new DataSet();
                string strCloud = "Exec Get_CloudSyncDate";
                dsCloudSync = dal.GetData(strCloud, Application.StartupPath);

                if (dsCloudSync.Tables[0].Rows.Count > 0)
                {


                    var postData = string.Empty;




                    var request = (HttpWebRequest)WebRequest.Create(ConfigReader.GetCloudServiceHost() + "Reminder/wsAddReminder");

                    request.ContentType = "application/json";
                    request.Method = "POST";





                    postData = "[";
                    for (int i = 0; i <= dsCloudSync.Tables[0].Rows.Count - 1; i++)
                    {

                        postData += "{";
                        postData += "'RID':'" + dsCloudSync.Tables[0].Rows[i]["RID"] + "',";
                        postData += "'EventID':'" + dsCloudSync.Tables[0].Rows[i]["EventID"] + "',";
                        postData += "'Event':'" + dsCloudSync.Tables[0].Rows[i]["Event"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'EventTitle':'" + dsCloudSync.Tables[0].Rows[i]["EventTitle"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'Phone':'" + dsCloudSync.Tables[0].Rows[i]["Phone"] + "',";
                        postData += "'Email':'" + dsCloudSync.Tables[0].Rows[i]["Email"] + "',";
                        postData += "'FromPhoneNo':'" + dsCloudSync.Tables[0].Rows[i]["FromPhoneNo"] + "',";
                        postData += "'AccountSID':'" + dsCloudSync.Tables[0].Rows[i]["AccountSID"] + "',";
                        postData += "'AuthToken':'" + dsCloudSync.Tables[0].Rows[i]["AuthToken"] + "',";
                        postData += "'EventDatetime':'" + dsCloudSync.Tables[0].Rows[i]["EventDatetime"] + "',";
                        postData += "'SMSStatus':'" + dsCloudSync.Tables[0].Rows[i]["SMSStatus"] + "',";
                        postData += "'EmailStatus':'" + dsCloudSync.Tables[0].Rows[i]["EmailStatus"] + "',";
                        postData += "'CallStatus':'" + dsCloudSync.Tables[0].Rows[i]["CallStatus"] + "',";
                        postData += "'ClientKey':'" + dsCloudSync.Tables[0].Rows[i]["ClientKey"] + "',";
                        postData += "'RedirectLink':'" + dsCloudSync.Tables[0].Rows[i]["RedirectLink"] + "',";
                        postData += "'EmailFrom':'" + dsCloudSync.Tables[0].Rows[i]["EmailFrom"] + "',";
                        postData += "'EmailPassword':'" + dsCloudSync.Tables[0].Rows[i]["EmailPassword"] + "',";
                        postData += "'EmailFromTitle':'" + dsCloudSync.Tables[0].Rows[i]["EmailFromTitle"] + "',";
                        postData += "'EmailPort':'" + dsCloudSync.Tables[0].Rows[i]["EmailPort"] + "',";
                        postData += "'EmailSMTP':'" + dsCloudSync.Tables[0].Rows[i]["EmailSMTP"] + "',";
                        postData += "'CompanyLOGO':'" + dsCloudSync.Tables[0].Rows[i]["CompanyLOGO"] + "',";
                        postData += "'IsStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsStatus"] + "',";
                        postData += "'MainEventID':'" + dsCloudSync.Tables[0].Rows[i]["MainEventID"] + "',";
                        postData += "'IsTwilioStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsTwilioStatus"] + "',";
                        postData += "'AppointmentDatetime':'" + dsCloudSync.Tables[0].Rows[i]["AppointmentDatetime"] + "',";
                        postData += "'IsConfirmable':'" + dsCloudSync.Tables[0].Rows[i]["IsConfirmable"] + "',";
                        postData += "'TwilioLang':'" + dsCloudSync.Tables[0].Rows[i]["TwilioLang"] + "',";
                        postData += "'IsSync':'0'";


                        postData += "},";

                    }
                    postData = postData.Remove(postData.Length - 1);
                    postData += "]";

                    var data = Encoding.ASCII.GetBytes(postData);



                    request.ContentLength = data.Length;


                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    wsReminderResponse WsCloud = JsonConvert.DeserializeObject<wsReminderResponse>(responseString);

                    if (WsCloud.Success == true)
                    {

                        

                        DataTable dtAutoConfirm = new DataTable();
                        dtAutoConfirm.Columns.AddRange(new DataColumn[2] { new DataColumn("MainEventID", typeof(string)),
                        new DataColumn("ClientKey", typeof(string))});

                        for (int i = 0; i <= WsCloud.ResponseDetail.Count - 1; i++)
                        {
                            dtAutoConfirm.Rows.Add(WsCloud.ResponseDetail[i].EventID, WsCloud.ResponseDetail[i].ClientKey);
                        }


                        if (dtAutoConfirm != null)
                        {
                            DataSet dsAutoSyncupdate = new DataSet();
                            SqlParameter[] p = new SqlParameter[1];
                            p[0] = new SqlParameter("@SyncCloudStatus", dtAutoConfirm);
                            dsAutoSyncupdate = dal.GetData("[Cloud_UpdateSync]", p, Application.StartupPath);
                        }




                    }
                }

            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }

        public void CloudSyncReminderUpdate()
        {
            try
            {
                DataSet dsCloudSync = new DataSet();
                string strCloud = "Exec Get_CloudSyncDate";
                dsCloudSync = dal.GetData(strCloud, Application.StartupPath);

                if (dsCloudSync.Tables[0].Rows.Count > 0)
                {


                    var postData = string.Empty;


                    //var request = (HttpWebRequest)WebRequest.Create("http://62.151.183.51:83/Reminder/wsUpdateReminder");

                    //var request = (HttpWebRequest)WebRequest.Create("http://50.21.182.203:83/Reminder/wsUpdateReminder");

                    var request = (HttpWebRequest)WebRequest.Create(ConfigReader.GetCloudServiceHost() + "Reminder/wsUpdateReminder");

                    request.ContentType = "application/json";
                    request.Method = "POST";





                    postData = "[";
                    for (int i = 0; i <= dsCloudSync.Tables[0].Rows.Count - 1; i++)
                    {

                        postData += "{";
                        postData += "'RID':'" + dsCloudSync.Tables[0].Rows[i]["RID"] + "',";
                        postData += "'EventID':'" + dsCloudSync.Tables[0].Rows[i]["EventID"] + "',";
                        postData += "'Event':'" + dsCloudSync.Tables[0].Rows[i]["Event"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'EventTitle':'" + dsCloudSync.Tables[0].Rows[i]["EventTitle"].ToString().Replace(@"'", @"\'") + "',";
                        postData += "'Phone':'" + dsCloudSync.Tables[0].Rows[i]["Phone"] + "',";
                        postData += "'Email':'" + dsCloudSync.Tables[0].Rows[i]["Email"] + "',";
                        postData += "'FromPhoneNo':'" + dsCloudSync.Tables[0].Rows[i]["FromPhoneNo"] + "',";
                        postData += "'AccountSID':'" + dsCloudSync.Tables[0].Rows[i]["AccountSID"] + "',";
                        postData += "'AuthToken':'" + dsCloudSync.Tables[0].Rows[i]["AuthToken"] + "',";
                        postData += "'EventDatetime':'" + dsCloudSync.Tables[0].Rows[i]["EventDatetime"] + "',";
                        postData += "'SMSStatus':'" + dsCloudSync.Tables[0].Rows[i]["SMSStatus"] + "',";
                        postData += "'EmailStatus':'" + dsCloudSync.Tables[0].Rows[i]["EmailStatus"] + "',";
                        postData += "'CallStatus':'" + dsCloudSync.Tables[0].Rows[i]["CallStatus"] + "',";
                        postData += "'ClientKey':'" + dsCloudSync.Tables[0].Rows[i]["ClientKey"] + "',";
                        postData += "'RedirectLink':'" + dsCloudSync.Tables[0].Rows[i]["RedirectLink"] + "',";
                        postData += "'EmailFrom':'" + dsCloudSync.Tables[0].Rows[i]["EmailFrom"] + "',";
                        postData += "'EmailPassword':'" + dsCloudSync.Tables[0].Rows[i]["EmailPassword"] + "',";
                        postData += "'EmailFromTitle':'" + dsCloudSync.Tables[0].Rows[i]["EmailFromTitle"] + "',";
                        postData += "'EmailPort':'" + dsCloudSync.Tables[0].Rows[i]["EmailPort"] + "',";
                        postData += "'EmailSMTP':'" + dsCloudSync.Tables[0].Rows[i]["EmailSMTP"] + "',";
                        postData += "'CompanyLOGO':'" + dsCloudSync.Tables[0].Rows[i]["CompanyLOGO"] + "',";
                        postData += "'IsStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsStatus"] + "',";
                        postData += "'MainEventID':'" + dsCloudSync.Tables[0].Rows[i]["MainEventID"] + "',";
                        postData += "'IsTwilioStatus':'" + dsCloudSync.Tables[0].Rows[i]["IsTwilioStatus"] + "',";
                        postData += "'AppointmentDatetime':'" + dsCloudSync.Tables[0].Rows[i]["AppointmentDatetime"] + "',";
                        postData += "'IsConfirmable':'" + dsCloudSync.Tables[0].Rows[i]["IsConfirmable"] + "',";
                        postData += "'TwilioLang':'" + dsCloudSync.Tables[0].Rows[i]["TwilioLang"] + "',";
                        postData += "'IsSync':'0'";

                        postData += "},";

                    }
                    postData = postData.Remove(postData.Length - 1);
                    postData += "]";

                    var data = Encoding.ASCII.GetBytes(postData);



                    request.ContentLength = data.Length;


                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    wsReminderResponse WsCloud = JsonConvert.DeserializeObject<wsReminderResponse>(responseString);

                    if (WsCloud.Success == true)
                    {

                        DataTable dtUpdateConfirm = new DataTable();
                        dtUpdateConfirm.Columns.AddRange(new DataColumn[2] { new DataColumn("MainEventID", typeof(string)),
                        new DataColumn("ClientKey", typeof(string))});

                        for (int i = 0; i <= WsCloud.ResponseDetail.Count - 1; i++)
                        {
                            dtUpdateConfirm.Rows.Add(WsCloud.ResponseDetail[i].EventID, WsCloud.ResponseDetail[i].ClientKey);
                        }


                        if (dtUpdateConfirm != null)
                        {
                            DataSet dsUpdateSyncupdate = new DataSet();
                            SqlParameter[] p = new SqlParameter[1];
                            p[0] = new SqlParameter("@SyncCloudStatus", dtUpdateConfirm);
                            dsUpdateSyncupdate = dal.GetData("[Cloud_UpdateSync]", p, Application.StartupPath);
                        }


                    }
                }

            }
            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}

        }


        public void GetAddressbookDetails()
        {
            DataSet dsgetAddressBook = new DataSet();
            string s = "exec SP_LRS_AddressBook;3";
            dsgetAddressBook = dal.GetData(s, Application.StartupPath);

            if (dsgetAddressBook.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < dsgetAddressBook.Tables[0].Rows.Count; i++)
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = dsgetAddressBook.Tables[0].Rows[i][1].ToString();
                    item.Value = dsgetAddressBook.Tables[0].Rows[i][0].ToString();

                    drpAddressBookList.Items.Add(item);
                }
            }
            else
            { }

        }

        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        private void drpAddressBookList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet dsgetAddressBookparties = new DataSet();
            string s = "exec AddressBooktoParties '" + ((ComboboxItem)drpAddressBookList.SelectedItem).Value + "'";
            dsgetAddressBookparties = dal.GetData(s, Application.StartupPath);

            if (dsgetAddressBookparties.Tables[0].Rows.Count > 0)
            {
                grdParties.DataSource = dsgetAddressBookparties.Tables[0];
                grdParties.CurrentCell.Selected = false;

                grdParties.Columns[1].Visible = false;


                grdParties.Columns[6].Visible = false;
                grdParties.Columns[7].Visible = false;
                grdParties.Columns[8].Visible = false;


                try
                {


                    foreach (DataGridViewRow item in grdParties.Rows)
                    {


                        lblCaseNo.Text = item.Cells[0].Value.ToString();
                        lblselectionNo.Text = item.Cells[8].Value.ToString();
                        lblpartiesFName.Text = item.Cells[6].Value.ToString();
                        lblpartiesLName.Text = item.Cells[7].Value.ToString();

                        if (lblselectionNo.Text != "" && lblpartiesFName.Text != "" && lblpartiesLName.Text != "" && lblCaseNo.Text != "")
                        {

                            try
                            {
                                AddPartytoDirect(lblselectionNo.Text, lblpartiesFName.Text, lblpartiesLName.Text, lblCaseNo.Text);
                            }
                            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }

                        }
                        else
                        {
                            MessageBox.Show("Please select at least one participants");
                        }
                    }

                }
                catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }

            }
            else
            {
                grdParties.DataSource = null;
            }
        }
        public string GethourBind()
        {
            string TT = string.Empty;
            DataSet dsTimeZone = new DataSet();
            dsTimeZone.Clear();
            string s = "exec Get_TimeZone 'APP'";
            dsTimeZone = dal.GetData(s, Application.StartupPath);
            if (dsTimeZone != null)
            {
                TT = dsTimeZone.Tables[0].Rows[0]["TimeHours"].ToString();
            }

            return TT;
        }

        private void lnkImortAppointment_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            try
            {
                Import ImportAppointment = new Import();
                ImportAppointment.ShowDialog();
            }


            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }

        private void drphours_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drphours.Items.Contains(drphours.Text))
                {
                    drphours.SelectedIndex = 0;
                    drphours.SelectedItem = null;
                    drphours.SelectedText = "";
                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
        }

        private void drpMinute_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drpMinute.Items.Contains(drpMinute.Text))
                {
                    drpMinute.SelectedIndex = 0;
                    drpMinute.SelectedItem = null;
                    drpMinute.SelectedText = "";
                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
        }

        private void drpAMPM_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!drpAMPM.Items.Contains(drpAMPM.Text))
                {
                    drpAMPM.SelectedIndex = 0;
                    drpAMPM.SelectedItem = null;
                    drpAMPM.SelectedText = "";
                }
            }
            catch (Exception ex) { Helper.AppLog.Error(ex.Message.ToString(), ex); }
        }

        private void grdParties_Paint(object sender, PaintEventArgs e)
        {
            DataGridView sndr = (DataGridView)sender;
            if (sndr.Rows.Count == 0)
            {
                using (Graphics grfx = e.Graphics)
                {
                    grfx.FillRectangle(Brushes.White, new Rectangle(new Point(10, 10), new Size(10, 10)));
                    grfx.DrawString("No record(s) found.", new Font("Microsoft Sans Serif", 10), Brushes.DarkGray, new PointF(120, 100));
                }
            }
        }

        private void grdaddedparties_Paint(object sender, PaintEventArgs e)
        {
            DataGridView sndr = (DataGridView)sender;
            if (sndr.Rows.Count == 0)
            {
                using (Graphics grfx = e.Graphics)
                {
                    grfx.FillRectangle(Brushes.White, new Rectangle(new Point(10, 10), new Size(10, 10)));
                    grfx.DrawString("No record(s) found.", new Font("Microsoft Sans Serif", 10), Brushes.DarkGray, new PointF(120, 100));
                }
            }
        }

        private void getTwilioLang()
        {

            DataSet dsbulktype = new DataSet();
            string s = "EXEC GetTwilioLanguage";
            string DefaultLangID = "";

            dsbulktype = dal.GetData(s, Application.StartupPath);
            if (dsbulktype != null && dsbulktype.Tables[0].Rows.Count > 0)
            {
                drpTwilioLang.Items.Clear();
                for (int i = 0; i < dsbulktype.Tables[0].Rows.Count; i++)
                {
                    drpTwilioLangBind itembind = new drpTwilioLangBind();
                    itembind.Text = dsbulktype.Tables[0].Rows[i][1].ToString();
                    itembind.Value = dsbulktype.Tables[0].Rows[i][0].ToString();

                    if (dsbulktype.Tables[0].Rows[i][3].ToString() == "True")
                    {
                        DefaultLangID = dsbulktype.Tables[0].Rows[i][1].ToString();
                    }

                    drpTwilioLang.Items.Add(itembind);


                }

                drpTwilioLang.SelectedIndex = drpTwilioLang.FindStringExact(DefaultLangID);
            }
            else
            {

            }

        }

        public class drpTwilioLangBind
        {
            public string Text { get; set; }
            public object Value { get; set; }

            public override string ToString()
            {
                return Text;
            }
        }

        private void lnkrecurring_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {



            try
            {
                Recurring Recurring = new Recurring();
                Recurring.ShowDialog();
            }


            catch (Exception ex){Helper.AppLog.Error(ex.Message.ToString(), ex);}
        }
    }
}
