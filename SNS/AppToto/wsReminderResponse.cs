﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReminderSystem
{
    public class wsReminderResonseDetail
    {
        public int ApplicationEventID { get; set; }
        public int CloudeEventID { get; set; }
        public int EventID { get; set; }
        public string ClientKey { get; set; }
    }


    public class wsReminderResponse
    {

        public wsReminderResponse()
        {
            ResponseDetail = new List<wsReminderResonseDetail>();
        }

        public List<wsReminderResonseDetail> ResponseDetail { get; set; }

        private bool _success;

        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

    }
}
