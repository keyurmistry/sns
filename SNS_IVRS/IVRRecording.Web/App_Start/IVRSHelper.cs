﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IVRRecording.Web
{
    public static class IVRSHelper
    {
        public static readonly string IVRS_Voice = "man";

        public static readonly log4net.ILog AppLog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}