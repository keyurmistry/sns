﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace IVRRecording
{
    public static class ConfigReader
    {
        private static AppSettingsReader oASR = new AppSettingsReader();

        public static string GetString(string keyName)
        {
            return (string)oASR.GetValue(keyName, typeof(string));
        }

        public static int GetInt(string keyName)
        {
            return (int)oASR.GetValue(keyName, typeof(int));
        }

        public static double GetDouble(string keyName)
        {
            return (double)oASR.GetValue(keyName, typeof(double));
        }

        public static bool GetBool(string keyName)
        {
            return (bool)oASR.GetValue(keyName, typeof(bool));
        }

        public static string ApplicationMode()
        {
            return GetString("Application.Mode");
        }

        public static string GetTwilioCallBackURL()
        {
            return GetString("Twilio.CallBackURL." + ApplicationMode());
        }
    }
}