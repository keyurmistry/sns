﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.TwiML;
using Twilio.TwiML.Mvc;

namespace IVRRecording.Web.Controllers
{

    public class TwilioIVRController : Controller
    {
        DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["TwilioConfig"].ToString());

        static string StrEventNo, strVoiceReminder = string.Empty;


        // GET: /TwilioIVR/Test2
        public ActionResult Test2()
        {
            try
            {
                GetAllIVRS_Details();
            }
            catch (Exception ex) { IVRSHelper.AppLog.Error(ex.Message.ToString(), ex); }
            return View();
        }


        public void GetAllIVRS_Details()
        {
            DataSet dsgetA1Details = new DataSet();
            string s = "exec Sp_IVRS_Reminder_New";
            dsgetA1Details = dal.GetData(s);

            IVRSHelper.AppLog.Info("Heartbeat Check");

            if (dsgetA1Details != null && dsgetA1Details.Tables != null && dsgetA1Details.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i <= dsgetA1Details.Tables[0].Rows.Count - 1; i++)
                {
                    try
                    {
                        string FromCall, ToCall, VoiceURL, accountSID, authToken, EventID, ReminderLang = string.Empty;

                        FromCall = dsgetA1Details.Tables[0].Rows[i]["FormCall"].ToString();
                        ToCall = dsgetA1Details.Tables[0].Rows[i]["phone"].ToString();
                        VoiceURL = dsgetA1Details.Tables[0].Rows[i]["EventTitle"].ToString();
                        accountSID = dsgetA1Details.Tables[0].Rows[i]["accountSID"].ToString();
                        authToken = dsgetA1Details.Tables[0].Rows[i]["authToken"].ToString();
                        EventID = dsgetA1Details.Tables[0].Rows[i]["EventID"].ToString();
                        StrEventNo = dsgetA1Details.Tables[0].Rows[i]["EventID"].ToString();
                        ReminderLang = dsgetA1Details.Tables[0].Rows[i]["LanguageValue"].ToString();

                        IVRSHelper.AppLog.Info("Call IVRS Event Received | EventID=" + EventID + " | ToCall=" + ToCall);

                        if (ToCall != "" && ToCall != null && ToCall != string.Empty)
                        {

                            try
                            {
                                var cl = new TwilioRestClient(accountSID, authToken);

                                CallOptions ops = new CallOptions();
                                ops.From = FromCall;
                                ops.To = ToCall;
                                ops.Url = String.Format("{0}TwilioIVR/InteractiveHello/{1}?TwiLang={2}", ConfigReader.GetTwilioCallBackURL(), StrEventNo, ReminderLang);
                                //ops.Url = Url.Action("InteractiveHello/" + StrEventNo + "/" + ReminderLang,
                                //  null,
                                //  null,
                                //  Request.Url.Scheme);
                                var call = cl.InitiateOutboundCall(ops);


                                try
                                {
                                    DataSet dsSID = new DataSet();
                                    string strSID = "exec Sp_InsertTwilioSID '" + StrEventNo + "','" + call.Sid + "'";

                                    dsSID = dal.GetData(strSID);
                                }
                                catch (Exception ex) { IVRSHelper.AppLog.Error(ex.Message.ToString(), ex); }
                            }
                            catch (Exception ex) { IVRSHelper.AppLog.Error(ex.Message.ToString(), ex); }

                        }
                        else
                        {
                            IVRSHelper.AppLog.Warn("ToCall is empty for EventNumber=" + StrEventNo);
                        }


                    }
                    catch (Exception ex) { IVRSHelper.AppLog.Error(ex.Message.ToString(), ex); }



                }


            }
            else
            {

            }

        }


        public ActionResult InteractiveHello(string eventID, string TwiLang)
        {

            TwilioVoicespeech Speech = new TwilioVoicespeech();
            string strVoiceReminder = string.Empty;
            string strConfirmable = string.Empty;
            string strTwilioSpeechLang = string.Empty;


            try
            {

                IVRSHelper.AppLog.Info(String.Format("InteractiveHello Started Event={0}", eventID));

                DataSet dsEventSpeech = new DataSet();
                string strSID = "exec Sp_IVRS_Reminder_EventSpeechGet '" + eventID + "'";
                dsEventSpeech = dal.GetData(strSID);
                if (dsEventSpeech != null && dsEventSpeech.Tables != null && dsEventSpeech.Tables[0].Rows.Count > 0)
                {
                    IVRSHelper.AppLog.Info(String.Format("InteractiveHello Event={0} | Received data from Database", eventID));
                    strVoiceReminder = dsEventSpeech.Tables[0].Rows[0]["EventTitle"].ToString();
                    strConfirmable = dsEventSpeech.Tables[0].Rows[0]["IsConfirmable"].ToString();
                    strTwilioSpeechLang = dsEventSpeech.Tables[0].Rows[0]["LanguageValue"].ToString();
                   
                    int FirstIndex = strVoiceReminder.IndexOf(":");

                    if (FirstIndex > 0)
                    {
                        strVoiceReminder = strVoiceReminder.Substring(FirstIndex + 1, strVoiceReminder.Length - FirstIndex - 1);
                    }
                }
                else
                { IVRSHelper.AppLog.Info(String.Format("InteractiveHello Event={0} | No Data Received Database", eventID)); }

            }
            catch (Exception ex) { IVRSHelper.AppLog.Error(ex.Message.ToString(), ex); }


            var res = new TwilioResponse();
            res = res.BeginGather(
              new
              {
                  //action = Url.Action("RespondToUser/" + eventID, null, null, Request.Url.Scheme),
                  action = String.Format("{0}TwilioIVR/RespondToUser/{1}", ConfigReader.GetTwilioCallBackURL(), eventID),
                  numDigits = "1"

              });

            IVRSHelper.AppLog.Info(String.Format("{0}TwilioIVR/RespondToUser/{1}", ConfigReader.GetTwilioCallBackURL(), eventID));

            Speech.voice = IVRSHelper.IVRS_Voice;
            Speech.language = TwiLang;

            //if (strTwilioSpeechLang != "en-US")
            //{
            //    Speech.language = "es-MX";
            //}
            //else
            //{
            //    Speech.language = "en-US";
            //}










            res = res.Say(strVoiceReminder.ToString(), Speech).Pause(2);

            if (strConfirmable != "1")
            {
                IVRSHelper.AppLog.Info(String.Format("InteractiveHello Event={0} | Not Confirmable", eventID));

                res = res.Say("This is Reminder System Confirmation Call.", Speech).Pause(2);
                res = res.Say("if you Confirm Appointment, press 1.", Speech).Pause(1);
                res = res.Say("if you Cancel Appointment, press 2.", Speech).Pause(1);
                res = res.Say("If you Reschedule Appointment, press 3.", Speech).Pause(1);
                res = res.Say("If you want operater to call you back , press 4.", Speech).Pause(1);
                res = res.Say("Press 9 to repeat this message again.", Speech).Pause(1);
            }
            else
            {

                IVRSHelper.AppLog.Info(String.Format("InteractiveHello Event={0} | Confirmable", eventID));

                res = res.Say("This is Reminder System Confirmation Call.", Speech).Pause(2);
                res = res.Say("if you Confirm Appointment, press 1.", Speech).Pause(1);
                res = res.Say("If you want operater to call you back , press 4.", Speech).Pause(1);
                res = res.Say("Press 9 to repeat this message again.", Speech).Pause(1);
            }

            res = res.EndGather();
            res = res.Say("Sorry, I didn't get your response.", Speech);



            return new TwiMLResult(res);


        }

        public class TwilioVoicespeech
        {
            public string voice { get; set; }
            public string language { get; set; }
        }


        public ActionResult RespondToUser(string eventID)
        {
            TwilioVoicespeech Speech = new TwilioVoicespeech();

            IVRSHelper.AppLog.Info(String.Format("RespondToUser Event={0} | Started", eventID));

            int userinput = 0;

            try
            {
                userinput = int.Parse(Request["Digits"]);
            }
            catch (Exception ex)
            {
                IVRSHelper.AppLog.Error("RespondToUser | " + ex.Message.ToString(), ex);
                userinput = 5;
                
            }

            if (Request["Digits"].ToString() != "" || Request["Digits"].ToString() != string.Empty)
            {
               
            }
            else
            {
                
            }

            IVRSHelper.AppLog.Info(String.Format("RespondToUser Event={0} | UserInput {1}", eventID, userinput));

            var res = new TwilioResponse();
            string StrTwiLang = string.Empty;


            try
            {

                DataSet dsEventSpeech = new DataSet();
                string strSID = "exec Sp_IVRS_Reminder_EventSpeechGet '" + eventID + "'";
                dsEventSpeech = dal.GetData(strSID);
                if (dsEventSpeech != null && dsEventSpeech.Tables != null && dsEventSpeech.Tables[0].Rows.Count > 0)
                {
                    StrTwiLang = dsEventSpeech.Tables[0].Rows[0]["LanguageValue"].ToString();
                }
                else
                {

                }

            }
            catch (Exception ex) { IVRSHelper.AppLog.Error(ex.Message.ToString(), ex); }

            Speech.voice = IVRSHelper.IVRS_Voice;

            if (StrTwiLang != "en-US")
            {
                Speech.language = "es-MX";
            }
            else
            {
                Speech.language = "en-US";
            }




            if (userinput == 1)
            {
                UpdateIVRSEntry(eventID, Convert.ToString(userinput));

            }
            else if (userinput == 2)
            {
                UpdateIVRSEntry(eventID, Convert.ToString(userinput));

            }
            else if (userinput == 3)
            {
                UpdateIVRSEntry(eventID, Convert.ToString(userinput));

            }
            else if (userinput == 4)
            {
                UpdateIVRSEntry(eventID, Convert.ToString(userinput));
            }
            else if (userinput == 9)
            {
                return InteractiveHello(eventID, Speech.language);
            }
            else
            {
                UpdateIVRSEntry(eventID, "5");
            }

            if (StrTwiLang == "en-US")
            {
                res.Say("Thanks for your response.");
                res.Hangup();
            }
            else
            {
                res.Say("Gracias por su respuesta.");
                res.Hangup();
            }
            IVRSHelper.AppLog.Info(String.Format("RespondToUser Event={0} | End", eventID));
            return new TwiMLResult(res);
        }


        public void UpdateIVRSEntry(string EventIDForupdate, string UserIVRS)
        {


            DataSet dsIVRS = new DataSet();
            SqlParameter[] p = new SqlParameter[2];

            p[0] = new SqlParameter("@EventID", EventIDForupdate);
            p[1] = new SqlParameter("@CallValue", UserIVRS);

            string s = "exec SP_TwilioStatus_New '" + p[0].Value + "','" + p[1].Value + "'";
            dsIVRS = dal.GetData(s);
            if (dsIVRS.Tables[0].Rows[0]["Result"].ToString() == "1")
            {

            }
            else
            {

            }


        }


        public ActionResult AskForlangSelection(string eventID)
        {

            TwilioVoicespeech Speech = new TwilioVoicespeech();




            var res = new TwilioResponse();
            res = res.BeginGather(
              new
              {


                  //action = Url.Action("AskForRespondToUser/" + eventID, null, null, Request.Url.Scheme),
                  action = String.Format("{0}TwilioIVR/AskForRespondToUser/{1}", ConfigReader.GetTwilioCallBackURL(), eventID),
                  numDigits = "1"

              });


            Speech.voice = IVRSHelper.IVRS_Voice;
            Speech.language = "es-MX";



            res = res.Say("This is Reminder System Confirmation Call.", Speech).Pause(5);
            res = res.Say("Press 1 For Spanish", Speech);
            res = res.Pause(10);


            res = res.EndGather();
            res = res.Redirect(String.Format("{0}TwilioIVR/InteractiveHello/{1}?TwiLang=en-US", ConfigReader.GetTwilioCallBackURL(), eventID));
            //res = res.Redirect(Url.Action("InteractiveHello/" + eventID + "/en-US",
            //                         null,
            //                         null,
            //                         Request.Url.Scheme));

            return new TwiMLResult(res);


        }


        public ActionResult AskForRespondToUser(string eventID)
        {
            TwilioVoicespeech Speech = new TwilioVoicespeech();

            Speech.voice = IVRSHelper.IVRS_Voice;
            Speech.language = "es-MX";

            int userinput = 0;
            if (Request["Digits"].ToString() != "" || Request["Digits"].ToString() != string.Empty)
            {
                userinput = int.Parse(Request["Digits"]);
            }
            else
            {

            }

            var res = new TwilioResponse();
            string StrTwiLang = string.Empty;


            if (userinput == 1)
            {
                res.Say("Thanks for language Spanish ", Speech);
                res = res.Pause(5);
                res = res.Redirect(String.Format("{0}TwilioIVR/InteractiveHello/{1}?TwiLang=es-MX", ConfigReader.GetTwilioCallBackURL(), eventID));
                //res = res.Redirect(Url.Action("InteractiveHello/" + eventID + "/es-MX",
                //                     null,
                //                     null,
                //                     Request.Url.Scheme));


            }



            res.Hangup();
            return new TwiMLResult(res);
        }



    }
}
