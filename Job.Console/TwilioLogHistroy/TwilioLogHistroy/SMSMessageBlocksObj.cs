﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwilioLogHistroy
{
    class SMSMessageBlocksObj
    {
        public string sid { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_updated { get; set; }
        public DateTime date_sent { get; set; }
        public string account_sid { get; set; }
        public string to { get; set; }
        public string from { get; set; }
        public string messaging_service_sid { get; set; }
        public string body { get; set; }
        public string status { get; set; }
        public int num_segments { get; set; }
        public int num_media { get; set; }
        public string direction { get; set; }
        public string api_version { get; set; }
        public string price { get; set; }
        public string price_unit { get; set; }
        public string error_code { get; set; }
        public string error_message { get; set; }
        public string uri { get; set; }
        public List<SMSSubResourceURI> subresource_uris { get; set; }
    }
}
