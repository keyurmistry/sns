﻿using DataLayer;
using ReminderSystem.Business.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwilioLogHistroy
{
    class Program
    {
        public static string AccountSIDLog, AuthoTokenLog = string.Empty;
        private static DateTime dtLastLogAccess;

        private static int mintCallCount, mintMessageCount;

        private static CreateLog mLog = new CreateLog();

        static void Main(string[] args)
        {
            DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

            mLog.LogMessage("Log Execution Started");

            try
            {
                DataSet dsgetTwilioCredencial = new DataSet();
                string strEmailDetails = "exec Sp_GetEmailSMSDetails";
                dsgetTwilioCredencial = dal.GetData(strEmailDetails);

                if (dsgetTwilioCredencial != null && dsgetTwilioCredencial.Tables != null && dsgetTwilioCredencial.Tables[0].Rows.Count > 0)
                {
                    AccountSIDLog = dsgetTwilioCredencial.Tables[0].Rows[0]["Twilio_AccountSid"].ToString().Trim();
                    AuthoTokenLog = dsgetTwilioCredencial.Tables[0].Rows[0]["Twilio_AuthToken"].ToString().Trim();
                }

                if (!DateTime.TryParse(dsgetTwilioCredencial.Tables[1].Rows[0][0].ToString(), out dtLastLogAccess))
                    dtLastLogAccess = DateTime.MinValue;

                mLog.LogMessage("Last Log Access DateTime: " + dtLastLogAccess.ToString());
            }
            catch (Exception ex)
            {
                mLog.LogEror(ex);
            }
            try
            {
                getTwilioSMSLogs(AccountSIDLog, AuthoTokenLog);
            }
            catch (Exception ex)
            {
                mLog.LogEror(ex);
            }
            try
            {
                getTwilioCallLogs(AccountSIDLog, AuthoTokenLog);
            }
            catch (Exception ex)
            {
                mLog.LogEror(ex);
            }

            updateLastLogAccess();

        }
        public static int totalNumOfRec = 0;

        public static void getTwilioSMSLogs(string AccountSid, string AuthToken)
        {
            mLog.LogMessage("STARTED [getTwilioSMSLogs]");

            DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

            var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);
            var options = new Twilio.MessageListRequest()
            {
                DateSent = dtLastLogAccess,
                DateSentComparison = Twilio.ComparisonType.GreaterThanOrEqualTo
            };

            var messages = twilio.ListMessages(options);

            foreach (var calllog in messages.Messages)
            {
                try
                {
                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

                    DataSet dsLogSMS = new DataSet();
                    SqlParameter[] p = new SqlParameter[9];

                    p[0] = new SqlParameter("@FromNumber", calllog.From);
                    p[1] = new SqlParameter("@ToNumber", calllog.To);
                    p[2] = new SqlParameter("@Direction", calllog.Direction);
                    p[3] = new SqlParameter("@Duration", 0);
                    p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(calllog.DateSent) == "" ? Convert.ToString(calllog.DateSent) : Convert.ToDateTime(calllog.DateSent, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(calllog.DateSent) == "" ? Convert.ToString(calllog.DateSent) : Convert.ToDateTime(calllog.DateSent, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[6] = new SqlParameter("@Status", calllog.Status);
                    p[7] = new SqlParameter("@SID", calllog.Sid);
                    p[8] = new SqlParameter("@LogType", "SMS");

                    string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
                    dsLogSMS = dal.GetData(s);
                    if (dsLogSMS != null && dsLogSMS.Tables != null && dsLogSMS.Tables[0].Rows.Count > 0)
                    {
                    }
                    else
                    { }
                }
                catch(Exception ex)
                {
                    mLog.LogEror(ex);
                }
            }

            int totalRecs = 0;

            if (messages.NextPageUri != null)
            {
                string nextPageURI = messages.NextPageUri.ToString();
                if (nextPageURI != null && Convert.ToString(nextPageURI) != "")
                {
                    string nextPageUriForNextData = "https://api.twilio.com" + messages.NextPageUri.ToString();
                    totalRecs = twilioSMSLogPagination(nextPageUriForNextData, 1);
                }

                  
            }

            mintMessageCount = totalRecs + messages.Messages.Count; 
            mLog.LogMessage("COMPLETED [getTwilioSMSLogs]");
        }

        public static string SQLFix(string str)
        {
            if (str == "NA")
                str = "";
            return str.Replace("&nbsp;", "").Replace("'", "''");
        }
        public static int twilioSMSLogPagination(string nextPageUri, int count)
        {
            DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

            var client = new RestSharp.RestClient(nextPageUri);
            client.Authenticator = new RestSharp.Authenticators.HttpBasicAuthenticator(AccountSIDLog, AuthoTokenLog);

            var request = new RestSharp.RestRequest(RestSharp.Method.GET);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("cache-control", "no-cache");
            RestSharp.IRestResponse response = client.Execute(request);
            RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

            var JSONObj = deserial.Deserialize<SMSLogCompleteObj>(response);

            var callLogResTest = JSONObj.messages;

            foreach (var individualCallRecord in callLogResTest)
            {
                try
                {
                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

                    DataSet dsLogSMS = new DataSet();
                    SqlParameter[] p = new SqlParameter[9];

                    p[0] = new SqlParameter("@FromNumber", individualCallRecord.from);
                    p[1] = new SqlParameter("@ToNumber", individualCallRecord.to);
                    p[2] = new SqlParameter("@Direction", individualCallRecord.direction);
                    p[3] = new SqlParameter("@Duration", 0);
                    p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(individualCallRecord.date_sent) == "" ? Convert.ToString(individualCallRecord.date_sent) : Convert.ToDateTime(individualCallRecord.date_sent, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(individualCallRecord.date_sent) == "" ? Convert.ToString(individualCallRecord.date_sent) : Convert.ToDateTime(individualCallRecord.date_sent, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[6] = new SqlParameter("@Status", individualCallRecord.status);
                    p[7] = new SqlParameter("@SID", individualCallRecord.sid);
                    p[8] = new SqlParameter("@LogType", "SMS");


                    string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
                    dsLogSMS = dal.GetData(s);
                    if (dsLogSMS != null && dsLogSMS.Tables != null && dsLogSMS.Tables[0].Rows.Count > 0)
                    {
                    }
                    else
                    { }
                }
                catch(Exception ex)
                {
                    mLog.LogEror(ex);
                }
            }


            string nextPageUriFromResponse = "";

            if (JSONObj.next_page_uri != null && Convert.ToString(JSONObj.next_page_uri) != "")
            {
                nextPageUriFromResponse = JSONObj.next_page_uri.ToString();
            }

            if (nextPageUriFromResponse != null && Convert.ToString(nextPageUriFromResponse) != "")
            {
                string nextRequestURI = "https://api.twilio.com" + nextPageUriFromResponse;
                return twilioSMSLogPagination(nextRequestURI, ++count);
            }
            else
            {
                return count;
            }
        }

        public static void getTwilioCallLogs(string AccountSid, string AuthToken)
        {

            mLog.LogMessage("STARTED [getTwilioCallLogs]");

            DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

            var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);
            var options = new Twilio.CallListRequest()
            {
                StartTimeComparison = Twilio.ComparisonType.GreaterThanOrEqualTo,
                StartTime = dtLastLogAccess
            };


            var call = twilio.ListCalls(options);

            foreach (var calllog in call.Calls)
            {
                try
                {
                    DataSet dsLogCALL = new DataSet();
                    SqlParameter[] p = new SqlParameter[9];

                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

                    p[0] = new SqlParameter("@FromNumber", calllog.From);
                    p[1] = new SqlParameter("@ToNumber", calllog.To);
                    p[2] = new SqlParameter("@Direction", calllog.Direction);
                    p[3] = new SqlParameter("@Duration", calllog.Duration);
                    p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(calllog.DateCreated) == "" ? Convert.ToString(calllog.DateCreated) : Convert.ToDateTime(calllog.DateCreated, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(calllog.DateUpdated) == "" ? Convert.ToString(calllog.DateUpdated) : Convert.ToDateTime(calllog.DateUpdated, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[6] = new SqlParameter("@Status", calllog.Status);
                    p[7] = new SqlParameter("@SID", calllog.Sid);
                    p[8] = new SqlParameter("@LogType", "CALL");

                    string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
                    dsLogCALL = dal.GetData(s);
                    if (dsLogCALL != null && dsLogCALL.Tables != null && dsLogCALL.Tables[0].Rows.Count > 0)
                    {
                    }
                    else
                    { }
                }
                catch (Exception Ex)
                {
                    mLog.LogEror(Ex);
                }
            }

            // checking for pagination in the response of twilio.
            int totalRecs = 0;

            if (call.NextPageUri != null)
            {
                if (call.NextPageUri != null && Convert.ToString(call.NextPageUri) != "")
                {
                    string nextPageURI = call.NextPageUri.ToString();
                    if (nextPageURI != null && Convert.ToString(nextPageURI) != "")
                    {
                        string nextPageUriForNextData = "https://api.twilio.com" + call.NextPageUri.ToString();

                        totalRecs += twilioCallLogPagination(nextPageUriForNextData, 1);
                    }
                }

                
            }
            mintCallCount = totalRecs + call.Calls.Count;
            mLog.LogMessage("COMPLETED [getTwilioCallLogs]");
        }

        public static int twilioCallLogPagination(string nextPageUri, int count)
        {
            DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());

            var client = new RestSharp.RestClient(nextPageUri);
            client.Authenticator = new RestSharp.Authenticators.HttpBasicAuthenticator(AccountSIDLog, AuthoTokenLog);

            var request = new RestSharp.RestRequest(RestSharp.Method.GET);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddHeader("cache-control", "no-cache");
            RestSharp.IRestResponse response = client.Execute(request);
            RestSharp.Deserializers.JsonDeserializer deserial = new RestSharp.Deserializers.JsonDeserializer();

            var JSONObj = deserial.Deserialize<CallLogCompleteObj>(response);


            var call = JSONObj.calls;

            string MAINOUTPUT2 = "";

            foreach (var calllog in call)
            {

                totalNumOfRec = totalNumOfRec + 1;

                var test = calllog.from;
                try
                {
                    DataSet dsLogCALL = new DataSet();
                    SqlParameter[] p = new SqlParameter[9];


                    MAINOUTPUT2 += totalNumOfRec + " ::::: START_DATE : " + calllog.start_time + "FROM : " + calllog.from + " ====> TO :: " + calllog.to + " =====> SID : " + calllog.sid + " <br /> ";

                    System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
                    dateInfo.ShortDatePattern = "MM/dd/yyyy HH:mm:ss";

                    p[0] = new SqlParameter("@FromNumber", calllog.from);
                    p[1] = new SqlParameter("@ToNumber", calllog.to);
                    p[2] = new SqlParameter("@Direction", calllog.direction);
                    p[3] = new SqlParameter("@Duration", calllog.duration);
                    p[4] = new SqlParameter("@StartDate", SQLFix(Convert.ToString(calllog.date_created) == "" ? Convert.ToString(calllog.date_created) : Convert.ToDateTime(calllog.date_created, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[5] = new SqlParameter("@EndDate", SQLFix(Convert.ToString(calllog.date_updated) == "" ? Convert.ToString(calllog.date_updated) : Convert.ToDateTime(calllog.date_updated, dateInfo).ToString("MM/dd/yyyy HH:mm:ss")));
                    p[6] = new SqlParameter("@Status", calllog.status);
                    p[7] = new SqlParameter("@SID", calllog.sid);
                    p[8] = new SqlParameter("@LogType", "CALL");

                    string s = "exec Sp_InsertTwilioLog '" + p[0].Value + "','" + p[1].Value + "','" + p[2].Value + "','" + p[3].Value + "','" + p[4].Value + "','" + p[5].Value + "','" + p[6].Value + "','" + p[7].Value + "','" + p[8].Value + "'";
                    dsLogCALL = dal.GetData(s);
                    if (dsLogCALL != null && dsLogCALL.Tables != null && dsLogCALL.Tables[0].Rows.Count > 0)
                    {
                    }
                    else
                    { }
                }
                catch (Exception Ex)
                {
                    mLog.LogEror(Ex);
                }
            }


            string nextPageUriFromResponse = "";
            if (JSONObj.next_page_uri != null && Convert.ToString(JSONObj.next_page_uri) != "")
            {
                nextPageUriFromResponse = JSONObj.next_page_uri.ToString();
            }

            if (nextPageUriFromResponse != null && Convert.ToString(nextPageUriFromResponse) != "")
            {
                string nextRequestURI = "https://api.twilio.com" + nextPageUriFromResponse;
                return twilioCallLogPagination(nextRequestURI, ++count);
            }
            else
            {
                return totalNumOfRec;
            }
        }

        public static void updateLastLogAccess()
        {
            try
            {
                mLog.LogMessage("UPDATE LAST ACCESS DATETIME");
                DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());
                dal.Execute_NonQuery(string.Format("INSERT INTO tbl_Twilio_LogAccess (LogAccessDateTime,MessageLogCount,CallLogCount) VALUES (GETDATE(),{0},{1})", mintMessageCount, mintCallCount));
            }
            catch (Exception ex)
            {
                //TODO: Log errors
                new CreateLog().LogEror(ex);
            }
        }

    }
}
