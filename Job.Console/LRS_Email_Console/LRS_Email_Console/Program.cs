﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;


namespace LRS_Email_Console
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                GetA1LawReminderDetailsEmail();
            }
            catch
            {

            }

        }

        public static void GetA1LawReminderDetailsEmail()
        {
            DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());
            DataSet dsgetA1Details = new DataSet();
            string s = "exec Sp_ReminderEmail_New";
            dsgetA1Details = dal.GetData(s);
            if (dsgetA1Details != null && dsgetA1Details.Tables != null && dsgetA1Details.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i <= dsgetA1Details.Tables[0].Rows.Count - 1; i++)
                {
                    string subjectEmail, Email, BodyEmail, EmailFrom, EmailPassword, EmailFromTitle, EmailSMTP, EmailPort, EventBody, EventRedirect, EventLogo, headerTitle, buttonTitle, SendLogoStatus = string.Empty;

                    Email = dsgetA1Details.Tables[0].Rows[i]["Email"].ToString();
                    subjectEmail = dsgetA1Details.Tables[0].Rows[i]["Subject"].ToString();
                    EventBody = dsgetA1Details.Tables[0].Rows[i]["EventTitle"].ToString();
                    EventRedirect = dsgetA1Details.Tables[0].Rows[i]["EventRedirect"].ToString();
                    EventLogo = dsgetA1Details.Tables[0].Rows[i]["EmailCompanyLogo"].ToString();
                    EmailFrom = dsgetA1Details.Tables[0].Rows[i]["EmailFrom"].ToString();
                    EmailPassword = dsgetA1Details.Tables[0].Rows[i]["EmailPassword"].ToString();
                    EmailFromTitle = dsgetA1Details.Tables[0].Rows[i]["EmailFromTitle"].ToString();
                    EmailSMTP = dsgetA1Details.Tables[0].Rows[i]["EmailSMTP"].ToString();
                    EmailPort = dsgetA1Details.Tables[0].Rows[i]["EmailPort"].ToString();
                    headerTitle = dsgetA1Details.Tables[0].Rows[i]["headerTitle"].ToString();
                    buttonTitle = dsgetA1Details.Tables[0].Rows[i]["buttonTitle"].ToString();

                    SendLogoStatus = dsgetA1Details.Tables[0].Rows[i]["LogoSendStatus"].ToString();



                    if (Email != "" && subjectEmail != "")
                    {
                        try
                        {



                            MailMessage ms = new MailMessage();

                            ms.To.Add(Email);
                            ms.From = new MailAddress(EmailFrom, EmailFromTitle);
                            ms.Subject = subjectEmail;

                            ms.IsBodyHtml = true;

                            BodyEmail = string.Empty;

                            try
                            {
                                Base64ToImage(EventLogo);
                            }
                            catch { }


                            Attachment inlineLogo = new Attachment(AppDomain.CurrentDomain.BaseDirectory + "CompanyLogo.jpg");
                            ms.Attachments.Add(inlineLogo);
                            string contentID = "Image";
                            inlineLogo.ContentId = contentID;

                            inlineLogo.ContentDisposition.Inline = true;
                            inlineLogo.ContentDisposition.DispositionType = DispositionTypeNames.Inline;

                            //string headerTitle = "Your Appointment reminder";
                            //string buttonTitle = "View Your Appointment Details";


                            BodyEmail = "<!DOCTYPE html><html lang='en'><head><title>Reminder System</title></head><body style='background-color: #d3d3d3;background-repeat: no-repeat;font-family: oxygen,sans-serif; height:100%'>";
                            BodyEmail += "<div style='margin-left: auto; margin-right: auto; padding-left: 15px; padding-bottom: 15px; padding-right: 15px; width: 750px; background: #ddd;'><div><div style='border-bottom: 1px solid transparent;border-top-left-radius: 3px;border-top-right-radius: 3px;padding: 10px 15px;'>";
                            BodyEmail += "<div style='text-align:center'><img width='100' src=\"cid:" + contentID + "\"></div></div><div style='border-bottom: 1px solid transparent;border-top-left-radius: 3px;border-top-right-radius: 3px;padding: 10px 15px;text-align: center;'>";
                            BodyEmail += "<hr style='color: #fff;width: 30%;' /><h5>" + headerTitle + "</h5><hr style='color: #fff;width: 30%;' /></div> <div style='background-color: #fff; border-radius: 2px; box-shadow: 0 2px 2px rgba(0, 0, 0, 0.3); margin: 0 auto; width: 570px; padding: 40px;'><form method='post' action='#'>";
                            BodyEmail += "<div style='text-align: left;'><span>" + EventBody + " </span></div>";

                            if (EventRedirect == "Notification")
                            {

                            }
                            else
                            {
                                BodyEmail += "<div style='text-align: center;'><a href='" + EventRedirect + "' target='_blank'><button type='button' style='-moz-user-select: none;background-image: none;border: 1px solid transparent;border-radius: 4px;cursor: pointer;display: inline-block;font-size: 14px;font-weight: 400;line-height: 1.42857;margin-top: 15px;padding: 6px 12px;text-align: center;vertical-align: middle;white-space: nowrap;background-color: #337ab7;border-color: #2e6da4;color: #fff;'>" + buttonTitle + "</button></a></div>";

                            }
                            BodyEmail += " </form></div></div></div></body></html>";



                            ms.Body = BodyEmail;

                            SmtpClient oSmtp = new SmtpClient(EmailSMTP, Convert.ToInt32(EmailPort));
                            oSmtp.EnableSsl = true;
                            oSmtp.UseDefaultCredentials = false;
                            oSmtp.Credentials = new System.Net.NetworkCredential(EmailFrom, EmailPassword);                            
                            oSmtp.Send(ms);


                            

                           
                        }
                        catch (Exception ex)
                        {

                            

                            string filePath = @"C:\Error.txt";

                            using (StreamWriter writer = new StreamWriter(filePath, true))
                            {
                                writer.WriteLine("Message :" + ex.Message + "<br/>" + Environment.NewLine + "StackTrace :" + ex.StackTrace +
                                   "" + Environment.NewLine + "Date :" + DateTime.Now.ToString() + "<br/>" + "Message :" + ex.InnerException);
                                writer.WriteLine(Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine);
                            }
                        }



                    }

                }
            }
        }

        public static Image Base64ToImage(string base64)
        {


            byte[] imageBytes = Convert.FromBase64String(base64);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image CompanyImage = System.Drawing.Image.FromStream(ms, true);

            string FilePath = AppDomain.CurrentDomain.BaseDirectory + "CompanyLogo.jpg";

            if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + "CompanyLogo.jpg"))
            {
                System.IO.File.Delete(AppDomain.CurrentDomain.BaseDirectory + "CompanyLogo.jpg");
            }
            CompanyImage.Save(FilePath);

            return CompanyImage;
        }

    }
}
