﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using System.Security.AccessControl;
using System.Security.Principal;
using System.IO;
using System.Xml;

namespace LRS_Reminder
{
    class TwilioCallMsg
    {

        public void TwilioCall(string FromNumber, string ToNumber, string VoiceSMSUrl, string accountSID, string authToken)
        {

            TwilioRestClient client;
            client = new TwilioRestClient(accountSID, authToken);

            try
            {
                CallOptions options = new CallOptions();
                options.From = FromNumber;
                options.To = ToNumber;
                options.Url = VoiceSMSUrl;


                var call = client.InitiateOutboundCall(options);
            }
            catch
            {

            }
        }

    }
}
