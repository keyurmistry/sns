﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using System.Configuration;
using LRS_Reminder;
using Twilio;

namespace Twilio_Call_Console
{
    class Program
    {
        
        static void Main(string[] args)
        {

            try
            {
                GetA1LawReminderDetails();
            }
            catch
            {
            }

             
            
        }
      
        public static void GetA1LawReminderDetails()
        {
            DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());
            DataSet dsgetA1Details = new DataSet();
            string s = "exec Sp_ReminderCallMessage_New";
            dsgetA1Details = dal.GetData(s);
            if (dsgetA1Details != null && dsgetA1Details.Tables != null && dsgetA1Details.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i <= dsgetA1Details.Tables[0].Rows.Count - 1; i++)
                {
                    try
                    {
                        TwilioCallMsg CallReminder = new TwilioCallMsg();
                        string FromCall, ToCall, VoiceURL, accountSID, authToken = string.Empty;

                        FromCall = dsgetA1Details.Tables[0].Rows[i]["FormCall"].ToString();
                        ToCall = dsgetA1Details.Tables[0].Rows[i]["phone"].ToString();
                        VoiceURL = dsgetA1Details.Tables[0].Rows[i]["EventTitle"].ToString();
                        accountSID = dsgetA1Details.Tables[0].Rows[i]["accountSID"].ToString();
                        authToken = dsgetA1Details.Tables[0].Rows[i]["authToken"].ToString();


                        if (ToCall != "" && ToCall != null && ToCall != string.Empty)
                        {
                            try
                            {
                                CallReminder.TwilioCall(FromCall, ToCall, VoiceURL, accountSID, authToken);
                            }
                            catch
                            {

                            }
                            
                        }
                        else
                        {

                        }
                    }
                    catch
                    {

                    }
                }
            }
            else
            {
                
            }


        }
    }
}
