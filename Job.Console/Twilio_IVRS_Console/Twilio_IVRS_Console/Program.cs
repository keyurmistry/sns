﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Twilio_IVRS_Console
{
    class Program
    {



        static void Main(string[] args)
        {

            try
            {
               // GetAPIDetail("http://108.222.150.123/IVRS/");
                GetAPIDetail("http://staging.snsapp.in/IVRS");
            }
            catch
            {
            }




        }



        protected static void GetAPIDetail(string apiPath)
        {
            string s_ResponseString = string.Empty;

            try
            {
                s_ResponseString = string.Empty;
                HttpWebRequest HWR_Request = (HttpWebRequest)WebRequest.Create(apiPath);


                HWR_Request.Method = "POST";
                HWR_Request.MediaType = "HTTP/1.1";
                HWR_Request.ContentType = "text/xml";
                HWR_Request.UserAgent = "SNIP_CLIENT/1.0";


                //Attach data to the Web-Request
                Stream S_DataStream = HWR_Request.GetRequestStream();
                S_DataStream.Close();

                HttpWebResponse HWR_Response = (HttpWebResponse)HWR_Request.GetResponse();
                S_DataStream = HWR_Response.GetResponseStream();
                StreamReader SR_DataStream = new StreamReader(S_DataStream, Encoding.UTF8);
                s_ResponseString = SR_DataStream.ReadToEnd();

                SR_DataStream.Close();
                HWR_Response.Close();

            }
            catch
            {


            }

        }

    }
}
