﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using System.Security.AccessControl;
using System.Security.Principal;
using System.IO;
using System.Xml;
using LRS_Reminder_SMS;
using DataLayer;
using System.Configuration;
using System.Data.SqlClient;


namespace Twilio_SMS_Console
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                GetA1LawReminderDetailsSMS();
            }
            catch
            {
            }
        }
        public static void GetA1LawReminderDetailsSMS()
        {
            DalBase dal = new DalBase(ConfigurationManager.ConnectionStrings["Apptotoconfig"].ToString());
            DataSet dsgetA1Details = new DataSet();
            string s = "exec Sp_ReminderSMS_promotional";
            dsgetA1Details = dal.GetData(s);
            if (dsgetA1Details != null && dsgetA1Details.Tables != null && dsgetA1Details.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i <= dsgetA1Details.Tables[0].Rows.Count - 1; i++)
                {
                    try
                    {
                        TwilioCallMsg CallReminder = new TwilioCallMsg();
                        string FromCall, ToCall, VoiceURL, accountSID, authToken, EventID = string.Empty;

                        FromCall = dsgetA1Details.Tables[0].Rows[i]["FormCall"].ToString();
                        ToCall = dsgetA1Details.Tables[0].Rows[i]["phone"].ToString();
                        VoiceURL = dsgetA1Details.Tables[0].Rows[i]["EventTitle"].ToString();
                        accountSID = dsgetA1Details.Tables[0].Rows[i]["accountSID"].ToString();
                        authToken = dsgetA1Details.Tables[0].Rows[i]["authToken"].ToString();
                        EventID = dsgetA1Details.Tables[0].Rows[i]["EventID"].ToString();


                        if (ToCall != "" && ToCall != null && ToCall != string.Empty)
                        {
                            try
                            {
                                CallReminder.TwilioSMS(FromCall, ToCall, VoiceURL, accountSID, authToken, EventID);

                                
                            }
                            catch
                            {

                            }
                           
                        }
                        else
                        {

                        }
                    }
                    catch
                    {

                    }
                }

                

            }
            else
            {

            }


        }

        
    }
}
