﻿namespace DataLayer
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.Windows;

    public class DalBase
    {
        private int _CommandTimeout;
        private SqlConnection con;
        private bool ErrorThrow;

        public DalBase(string ConString)
        {
            this.con = new SqlConnection();
            this.ErrorThrow = false;
            this._CommandTimeout = 0;
            this.con.ConnectionString = ConString;
        }

        public DalBase(string ConString, bool throwError)
        {
            this.con = new SqlConnection();
            this.ErrorThrow = false;
            this._CommandTimeout = 0;
            this.con.ConnectionString = ConString;
            this.ErrorThrow = throwError;
        }

        private bool Con_Close()
        {
            if (this.con.State == ConnectionState.Open)
            {
                this.con.Close();
            }
            return true;
        }

        private bool Con_Open()
        {
            if (this.con.State == ConnectionState.Closed)
            {
                this.con.Open();
            }
            return true;
        }

        public bool Execute_NonQuery(string Query)
        {
            bool flag;
            try
            {
                this.Con_Open();
                new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.Text,
                    CommandText = Query
                }.ExecuteNonQuery();
                this.Con_Close();
                flag = true;
            }
            catch
            {
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                flag = false;
            }
            finally
            {
                this.Con_Close();
            }
            return flag;
        }

        public bool Execute_NonQuery(string SpName, SqlParameter[] p)
        {
            bool flag;
            try
            {
                this.Con_Open();
                SqlCommand command = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SpName
                };
                command.Parameters.AddRange(p);
                command.ExecuteNonQuery();
                this.Con_Close();
                flag = true;
            }
            catch
            {
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                flag = false;
            }
            finally
            {
                this.Con_Close();
            }
            return flag;
        }

        public bool Execute_NonQuery(string Query, ref int RowAffected)
        {
            bool flag;
            try
            {
                this.Con_Open();
                RowAffected = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.Text,
                    CommandText = Query
                }.ExecuteNonQuery();
                this.Con_Close();
                flag = true;
            }
            catch
            {
                this.Con_Close();
                RowAffected = 0;
                if (this.ErrorThrow)
                {
                    throw;
                }
                flag = false;
            }
            finally
            {
                this.Con_Close();
            }
            return flag;
        }

        public bool Execute_NonQuery(string SpName, SqlParameter[] p, ref int RowAffected)
        {
            bool flag;
            try
            {
                this.Con_Open();
                SqlCommand command = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SpName
                };
                command.Parameters.AddRange(p);
                RowAffected = command.ExecuteNonQuery();
                this.Con_Close();
                flag = true;
            }
            catch
            {
                this.Con_Close();
                RowAffected = 0;
                if (this.ErrorThrow)
                {
                    throw;
                }
                flag = false;
            }
            finally
            {
                this.Con_Close();
            }
            return flag;
        }

        public bool Execute_Reader(string Query)
        {
            bool flag;
            try
            {
                this.Con_Open();
                SqlCommand command = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.Text,
                    CommandText = Query
                };
                if (command.ExecuteReader().Read())
                {
                    return true;
                }
                flag = false;
            }
            catch
            {
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                flag = false;
            }
            finally
            {
                this.Con_Close();
            }
            return flag;
        }

        public object Execute_Scaler(string Query)
        {
            object obj2;
            object obj3;
            try
            {
                this.Con_Open();
                obj2 = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.Text,
                    CommandText = Query
                }.ExecuteScalar();
                this.Con_Close();
                obj3 = obj2;
            }
            catch
            {
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                obj3 = obj2 = "";
            }
            finally
            {
                this.Con_Close();
            }
            return obj3;
        }

        public object Execute_Scaler(string SpName, SqlParameter[] p)
        {
            object obj2;
            object obj3;
            try
            {
                this.Con_Open();
                SqlCommand command = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SpName
                };
                command.Parameters.AddRange(p);
                obj2 = command.ExecuteScalar();
                this.Con_Close();
                obj3 = obj2;
            }
            catch
            {
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                obj3 = obj2 = "";
            }
            finally
            {
                this.Con_Close();
            }
            return obj3;
        }

        public DataSet GetData(string Query)
        {
            DataSet set2;
            DataSet dataSet = new DataSet();
            try
            {

                new SqlDataAdapter(Query, this.con).Fill(dataSet);
                set2 = dataSet;
            }
            catch
            {
                dataSet = null;
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                set2 = dataSet;
            }
            finally
            {
                this.Con_Close();
            }
            return set2;
        }

        public DataSet GetData(string SpName, SqlParameter[] p)
        {
            DataSet set2;
            DataSet dataSet = new DataSet();
            try
            {
                this.Con_Open();
                SqlCommand selectCommand = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SpName
                };
                selectCommand.Parameters.AddRange(p);
                new SqlDataAdapter(selectCommand).Fill(dataSet);
                set2 = dataSet;
            }
            catch
            {
                dataSet = null;
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                set2 = dataSet;
            }
            finally
            {
                this.Con_Close();
            }
            return set2;
        }

        public DataTable GetRecords(string Query)
        {
            DataTable table2;
            DataTable table = new DataTable();
            try
            {
                this.Con_Open();
                SqlCommand selectCommand = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.Text,
                    CommandText = Query
                };
                SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);
                table = dataSet.Tables[0];
                if ((table != null) && (table.Rows.Count > 0))
                {
                    this.Con_Close();
                    return table;
                }
                table = null;
                this.Con_Close();
                table2 = table;
            }
            catch
            {
                table = null;
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                table2 = table;
            }
            finally
            {
                this.Con_Close();
            }
            return table2;
        }

        public bool GetRecords(string Query, ref DataTable dt)
        {
            bool flag;
            try
            {
                this.Con_Open();
                SqlCommand selectCommand = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.Text,
                    CommandText = Query
                };
                SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);
                dt = dataSet.Tables[0];
                if ((dt != null) && (dt.Rows.Count > 0))
                {
                    this.Con_Close();
                    return true;
                }
                dt = null;
                this.Con_Close();
                flag = false;
            }
            catch
            {
                dt = null;
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                flag = false;
            }
            finally
            {
                this.Con_Close();
            }
            return flag;
        }

        public bool GetRecords(string SpName, SqlParameter[] p, ref DataTable dt)
        {
            bool flag;
            try
            {
                this.Con_Open();
                SqlCommand selectCommand = new SqlCommand
                {
                    Connection = this.con,
                    CommandTimeout = this._CommandTimeout,
                    CommandType = CommandType.StoredProcedure,
                    CommandText = SpName
                };
                selectCommand.Parameters.AddRange(p);
                SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
                DataSet dataSet = new DataSet();
                adapter.Fill(dataSet);
                dt = dataSet.Tables[0];
                if ((dt != null) && (dt.Rows.Count > 0))
                {
                    this.Con_Close();
                    return true;
                }
                dt = null;
                this.Con_Close();
                flag = false;
            }
            catch
            {
                dt = null;
                this.Con_Close();
                if (this.ErrorThrow)
                {
                    throw;
                }
                flag = false;
            }
            finally
            {
                this.Con_Close();
            }
            return flag;
        }


    }
}

