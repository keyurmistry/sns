﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReminderSystem.Business.Base;

namespace ReminderSystem.Services.wsSyncReminder.Controllers
{
    public class RegistrationController : Controller
    {
        //
        // GET: /Registration/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult wsSyncRegistration(wsRegistration businessObject)
        {
            wsRegistrationFactory objFac = new wsRegistrationFactory();
            wsRegistrationResponse objResponse = new wsRegistrationResponse();

            try
            {
                objFac.Update(businessObject);
                objResponse.Success = true;
                objResponse.Message = "Success";
            }
            catch (Exception Ex)
            {
                objResponse.Success = false;
                objResponse.Message = Ex.Message.ToString();
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }
    }
}
