﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReminderSystem.Business.Base;

namespace ReminderSystem.Services.wsSyncReminder.Controllers
{
    public class EmailSettingController : Controller
    {
        //
        // GET: /EmailSetting/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult wsSyncEmailSetting(wsEmail_Setting businessObject)
        {
            wsEmail_SettingFactory objFac = new wsEmail_SettingFactory();
            wsEmail_SettingResponse objResponse = new wsEmail_SettingResponse();

            try
            {
                objFac.Update(businessObject);
                objResponse.Success = true;
                objResponse.Message = "Success";
            }
            catch (Exception Ex)
            {
                objResponse.Success = false;
                objResponse.Message = Ex.Message.ToString();
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult wsSyncCompanyLogoSetting(wsEmail_Setting businessObject)
        {
            wsEmail_SettingFactory objFac = new wsEmail_SettingFactory();
            wsEmail_SettingResponse objResponse = new wsEmail_SettingResponse();

            try
            {
                objFac.UpdateCompanyLogo(businessObject);
                objResponse.Success = true;
                objResponse.Message = "Success";
            }
            catch (Exception Ex)
            {
                objResponse.Success = false;
                objResponse.Message = Ex.Message.ToString();
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }
    }
}
