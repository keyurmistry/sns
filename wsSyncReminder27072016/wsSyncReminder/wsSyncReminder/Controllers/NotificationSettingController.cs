﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReminderSystem.Business.Base;

namespace ReminderSystem.Services.wsSyncReminder.Controllers
{
    public class NotificationSettingController : Controller
    {
        //
        // GET: /EmailSetting/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult wsSyncNotificationSetting(wsLRS_NotificationSetting businessObject)
        {
            wsLRS_NotificationSettingFactory objFac = new wsLRS_NotificationSettingFactory();
            wsLRS_NotificationSettingResponse objResponse = new wsLRS_NotificationSettingResponse();

            try
            {
                objFac.Update(businessObject);
                objResponse.Success = true;
                objResponse.Message = "Success";
            }
            catch (Exception Ex)
            {
                objResponse.Success = false;
                objResponse.Message = Ex.Message.ToString();
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }
    }
}
