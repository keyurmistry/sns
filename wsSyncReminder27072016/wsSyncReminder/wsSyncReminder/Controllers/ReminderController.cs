﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReminderSystem.Business.Base;

namespace ReminderSystem.Services.wsSyncReminder.Controllers
{
    public class ReminderController : Controller
    {
        //
        // GET: /Reminder/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult wsAddReminder(List<wsReminder> businessObject)
        {
            wsReminderFactory objFac = new wsReminderFactory();
            wsReminderResponse objResponse = new wsReminderResponse();

            try
            {
                foreach (wsReminder vReminder in businessObject)
                {
                    objFac.Insert(vReminder);
                    wsReminderResonseDetail objResDet = new wsReminderResonseDetail()
                    {
                        ApplicationEventID = Convert.ToInt32(vReminder.EventID.ToString()),
                        CloudeEventID = vReminder.RID,
                        ClientKey = vReminder.ClientKey,
                        EventID = vReminder.MainEventID
                    };

                    try
                    {
                        objResponse.ResponseDetail.Add(objResDet);
                    }
                    catch
                    { }
                }
                objResponse.Success = true;
                objResponse.Message = "Success";
            }
            catch (Exception Ex)
            {
                foreach (wsReminderResonseDetail vReminder in objResponse.ResponseDetail)
                {
                    objFac.Delete(vReminder.ClientKey, vReminder.EventID);
                    break;
                }

                objResponse.Success = false;
                objResponse.Message = Ex.Message.ToString();
                objResponse.ResponseDetail = new List<wsReminderResonseDetail>();
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult wsUpdateReminder(List<wsReminder> businessObject)
        {
            wsReminderFactory objFac = new wsReminderFactory();
            wsReminderResponse objResponse = new wsReminderResponse();
            List<wsReminder> objBackup = objFac.GetAllBy(businessObject[0].ClientKey, businessObject[0].MainEventID);

            try
            {
                if (objFac.Delete(businessObject[0].ClientKey, businessObject[0].MainEventID))
                {
                    foreach (wsReminder vReminder in businessObject)
                    {
                        objFac.Insert(vReminder);
                        wsReminderResonseDetail objResDet = new wsReminderResonseDetail()
                        {
                            ApplicationEventID = Convert.ToInt32(vReminder.EventID.ToString()),
                            CloudeEventID = vReminder.RID,
                            ClientKey = vReminder.ClientKey,
                            EventID = vReminder.MainEventID
                        };
                        objResponse.ResponseDetail.Add(objResDet);
                    }
                    objResponse.Success = true;
                    objResponse.Message = "Success";
                }
                else
                {
                    objResponse.Success = false;
                    objResponse.Message = "Error while updating the reminder";
                }
            }
            catch (Exception Ex)
            {
                foreach (wsReminderResonseDetail vReminder in objResponse.ResponseDetail)
                {
                    objFac.Delete(vReminder.ClientKey, vReminder.EventID);
                    break;
                }
                foreach (wsReminder vReminder in objBackup)
                {
                    objFac.Insert(vReminder);
                }

                objResponse.Success = false;
                objResponse.Message = Ex.Message.ToString();
                objResponse.ResponseDetail = new List<wsReminderResonseDetail>();
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult wsDeleteReminder(int MainEventID, string ClientKey)
        {
            wsReminderFactory objFac = new wsReminderFactory();
            wsReminderResponse objResponse = new wsReminderResponse();

            try
            {
                objFac.Delete(ClientKey, MainEventID);
                objResponse.Success = true;
                objResponse.Message = "Success";
            }
            catch (Exception Ex)
            {
                objResponse.Success = false;
                objResponse.Message = Ex.Message.ToString();
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult wsReminderStatus(string ClientKey)
        {
            wsReminderFactory objFac = new wsReminderFactory();
            wsReminderStatusResponse objResponse = new wsReminderStatusResponse();

            try
            {
                objResponse.Success = true;
                objResponse.Message = "Success";
                objResponse.ResponseDetail = objFac.getReminderStatus(ClientKey);
            }
            catch (Exception ex)
            {
                objResponse.Success = false;
                objResponse.Message = ex.Message.ToString();
                objResponse.ResponseDetail = new List<wsReminderStatusDetails>();
            }

            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }
    }
}
