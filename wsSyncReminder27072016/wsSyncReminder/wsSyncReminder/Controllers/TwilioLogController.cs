﻿using ReminderSystem.Business.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace wsSyncReminder.Controllers
{
    public class TwilioLogController : Controller
    {
        //
        // GET: /TwilioLog/
        public ActionResult Index()
        {
            return View();
        }

       
        [HttpPost]
        public JsonResult wsGetTwilioLog(string ClientKey)
        {
            wsTwilioLogFactory objFac = new wsTwilioLogFactory();
            wsTwilioLogResponse objResponse = new wsTwilioLogResponse();

            try
            {
                objResponse.ResponseDetail = objFac.GetAllByClientKey(ClientKey);
                objResponse.Success = true;
                objResponse.Message = "Success";
            }
            catch (Exception Ex)
            {
                objResponse.Success = false;
                objResponse.Message = Ex.Message.ToString();
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }
    }
}
