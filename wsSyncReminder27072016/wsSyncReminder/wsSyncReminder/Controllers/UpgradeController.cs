﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReminderSystem.Business.Base;
using ReminderSystem.Business.BusinessLayer;

namespace wsSyncReminder.Controllers
{
    public class UpgradeController : Controller
    {
        //
        // GET: /Upgrade/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult wsSyncRegistration(wsUpgrade businessObject)
        {
            wsUpgradeFactory objFac = new wsUpgradeFactory();
            wsUpgradeResponse objResponse = new wsUpgradeResponse();

            try
            {
                objFac.Update(businessObject);
                objResponse.Success = true;
                objResponse.Message = "Success";
            }
            catch (Exception Ex)
            {
                objResponse.Success = false;
                objResponse.Message = Ex.Message.ToString();
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }
    }
}
