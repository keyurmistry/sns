﻿using System.Web;
using System.Web.Mvc;

namespace ReminderSystem.Services.wsSyncReminder
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}