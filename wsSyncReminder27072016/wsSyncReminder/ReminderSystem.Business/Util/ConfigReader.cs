using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReminderSystem.Business.Utility
{
    public class ConfigReader
    {
        private AppSettingsReader oASR = new AppSettingsReader();

        public string GetString(string keyName)
        {
            return (string)oASR.GetValue(keyName, typeof(string));
        }

        public int GetInt(string keyName)
        {
            return (int)oASR.GetValue(keyName, typeof(int));
        }

        public double GetDouble(string keyName)
        {
            return (double)oASR.GetValue(keyName, typeof(double));
        }

        public bool GetBool(string keyName)
        {
            return (bool)oASR.GetValue(keyName, typeof(bool));
        }

    }
}
