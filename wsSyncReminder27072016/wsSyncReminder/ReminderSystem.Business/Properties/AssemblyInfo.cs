﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ReminderSystem.Business")]
[assembly: AssemblyDescription("Developed By Maulik A. Dusara")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mystic Business Solutions")]
[assembly: AssemblyProduct("ReminderSystem.Business")]
[assembly: AssemblyCopyright("Copyright © Maulik Dusara 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("bbfdf0ef-25da-4ec3-96ce-bacc905cb52b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2016.07.27.01")]
[assembly: AssemblyFileVersion("2016.07.27.01")]
