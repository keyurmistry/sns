using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using ReminderSystem.Business.Base;

namespace ReminderSystem.Business.Data
{
	/// <summary>
	/// Data access layer class for wsLRS_NotificationSetting
	/// </summary>
	class wsLRS_NotificationSettingSql : DataLayerBase 
	{

        #region Constructor

		/// <summary>
		/// Class constructor
		/// </summary>
		public wsLRS_NotificationSettingSql()
		{
			// Nothing for now.
		}

        #endregion

        #region Public Methods

         /// <summary>
        /// update row in the table
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <returns>true for successfully updated</returns>
        public bool Update(wsLRS_NotificationSetting businessObject)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_wsLRS_NotificationSetting_Update]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {
                
				sqlCommand.Parameters.Add(new SqlParameter("@NotificationID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.NotificationID));
				sqlCommand.Parameters.Add(new SqlParameter("@EmailSend", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailSend));
				sqlCommand.Parameters.Add(new SqlParameter("@EmailCancel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailCancel));
				sqlCommand.Parameters.Add(new SqlParameter("@EmailReschedule", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailReschedule));
				sqlCommand.Parameters.Add(new SqlParameter("@EmailWhen", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailWhen));
				sqlCommand.Parameters.Add(new SqlParameter("@SMSSend", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.SMSSend));
				sqlCommand.Parameters.Add(new SqlParameter("@SMSCancel", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.SMSCancel));
				sqlCommand.Parameters.Add(new SqlParameter("@SMSReschedule", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.SMSReschedule));
				sqlCommand.Parameters.Add(new SqlParameter("@SMSWhen", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.SMSWhen));
				sqlCommand.Parameters.Add(new SqlParameter("@isactive", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Isactive));
				sqlCommand.Parameters.Add(new SqlParameter("@CreatedDatetime", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.CreatedDatetime));
				sqlCommand.Parameters.Add(new SqlParameter("@ModifiedDatetime", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.ModifiedDatetime));
                sqlCommand.Parameters.Add(new SqlParameter("@ClientKey", SqlDbType.NVarChar, 1000, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.ClientKey));

                MainConnection.Open();

                sqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ;
            }
            finally
            {
                MainConnection.Close();
                sqlCommand.Dispose();
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Populate business object from data reader
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <param name="dataReader">data reader</param>
        internal void PopulateBusinessObjectFromReader(wsLRS_NotificationSetting businessObject, IDataReader dataReader)
        {


				businessObject.NotificationID = dataReader.GetInt32(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.NotificationID.ToString()));

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.EmailSend.ToString())))
				{
					businessObject.EmailSend = dataReader.GetString(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.EmailSend.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.EmailCancel.ToString())))
				{
					businessObject.EmailCancel = dataReader.GetInt32(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.EmailCancel.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.EmailReschedule.ToString())))
				{
					businessObject.EmailReschedule = dataReader.GetInt32(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.EmailReschedule.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.EmailWhen.ToString())))
				{
					businessObject.EmailWhen = dataReader.GetString(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.EmailWhen.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.SMSSend.ToString())))
				{
					businessObject.SMSSend = dataReader.GetString(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.SMSSend.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.SMSCancel.ToString())))
				{
					businessObject.SMSCancel = dataReader.GetInt32(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.SMSCancel.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.SMSReschedule.ToString())))
				{
					businessObject.SMSReschedule = dataReader.GetInt32(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.SMSReschedule.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.SMSWhen.ToString())))
				{
					businessObject.SMSWhen = dataReader.GetString(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.SMSWhen.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.Isactive.ToString())))
				{
					businessObject.Isactive = dataReader.GetInt32(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.Isactive.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.CreatedDatetime.ToString())))
				{
					businessObject.CreatedDatetime = dataReader.GetDateTime(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.CreatedDatetime.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.ModifiedDatetime.ToString())))
				{
					businessObject.ModifiedDatetime = dataReader.GetDateTime(dataReader.GetOrdinal(wsLRS_NotificationSetting.wsLRS_NotificationSettingFields.ModifiedDatetime.ToString()));
				}


        }

        /// <summary>
        /// Populate business objects from the data reader
        /// </summary>
        /// <param name="dataReader">data reader</param>
        /// <returns>list of wsLRS_NotificationSetting</returns>
        internal List<wsLRS_NotificationSetting> PopulateObjectsFromReader(IDataReader dataReader)
        {

            List<wsLRS_NotificationSetting> list = new List<wsLRS_NotificationSetting>();

            while (dataReader.Read())
            {
                wsLRS_NotificationSetting businessObject = new wsLRS_NotificationSetting();
                PopulateBusinessObjectFromReader(businessObject, dataReader);
                list.Add(businessObject);
            }
            return list;

        }

        #endregion

	}
}
