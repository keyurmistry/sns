using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using ReminderSystem.Business.Base;

namespace ReminderSystem.Business.Data
{
	/// <summary>
	/// Data access layer class for wsEmail_Setting
	/// </summary>
	class wsEmail_SettingSql : DataLayerBase 
	{

        #region Constructor

		/// <summary>
		/// Class constructor
		/// </summary>
		public wsEmail_SettingSql()
		{
			// Nothing for now.
		}

        #endregion

        #region Public Methods

         /// <summary>
        /// update row in the table
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <returns>true for successfully updated</returns>
        public bool Update(wsEmail_Setting businessObject)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_wsEmail_Setting_Update]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {
				sqlCommand.Parameters.Add(new SqlParameter("@EmailID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailID));
				sqlCommand.Parameters.Add(new SqlParameter("@EmailFrom", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailFrom));
				sqlCommand.Parameters.Add(new SqlParameter("@EmailPassword", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailPassword));
				sqlCommand.Parameters.Add(new SqlParameter("@EmailFromTitle", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailFromTitle));
				sqlCommand.Parameters.Add(new SqlParameter("@EmailPort", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailPort));
				sqlCommand.Parameters.Add(new SqlParameter("@EmailSMTP", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailSMTP));
				sqlCommand.Parameters.Add(new SqlParameter("@Twilio_CALL", SqlDbType.NVarChar, 100, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Twilio_CALL));
				sqlCommand.Parameters.Add(new SqlParameter("@Twilio_SMS", SqlDbType.NVarChar, 100, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Twilio_SMS));
				sqlCommand.Parameters.Add(new SqlParameter("@Twilio_AccountSid", SqlDbType.NVarChar, 100, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Twilio_AccountSid));
				sqlCommand.Parameters.Add(new SqlParameter("@Twilio_AuthToken", SqlDbType.NVarChar, 100, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Twilio_AuthToken));
                sqlCommand.Parameters.Add(new SqlParameter("@ClientKey", SqlDbType.NVarChar, 1000, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.ClientKey));
                
                MainConnection.Open();

                sqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw ;
            }
            finally
            {
                MainConnection.Close();
                sqlCommand.Dispose();
            }
        }

        public bool UpdateCompanyLogo(wsEmail_Setting businessObject)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_wsEmail_CompanyLogoSignature_Update]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {
                sqlCommand.Parameters.Add(new SqlParameter("@CompanyLogo", SqlDbType.NVarChar, 2147483647, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.CompanyLogo));
                sqlCommand.Parameters.Add(new SqlParameter("@Signature", SqlDbType.NVarChar, 2147483647, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Signature));
                sqlCommand.Parameters.Add(new SqlParameter("@ClientKey", SqlDbType.NVarChar, 1000, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.ClientKey));

                MainConnection.Open();

                sqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                MainConnection.Close();
                sqlCommand.Dispose();
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Populate business object from data reader
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <param name="dataReader">data reader</param>
        internal void PopulateBusinessObjectFromReader(wsEmail_Setting businessObject, IDataReader dataReader)
        {


				businessObject.EmailID = dataReader.GetInt32(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailID.ToString()));

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailFrom.ToString())))
				{
					businessObject.EmailFrom = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailFrom.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailPassword.ToString())))
				{
					businessObject.EmailPassword = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailPassword.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailFromTitle.ToString())))
				{
					businessObject.EmailFromTitle = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailFromTitle.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailPort.ToString())))
				{
					businessObject.EmailPort = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailPort.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailSMTP.ToString())))
				{
					businessObject.EmailSMTP = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailSMTP.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Twilio_CALL.ToString())))
				{
					businessObject.Twilio_CALL = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Twilio_CALL.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Twilio_SMS.ToString())))
				{
					businessObject.Twilio_SMS = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Twilio_SMS.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Twilio_AccountSid.ToString())))
				{
					businessObject.Twilio_AccountSid = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Twilio_AccountSid.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Twilio_AuthToken.ToString())))
				{
					businessObject.Twilio_AuthToken = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Twilio_AuthToken.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.CompanyLogo.ToString())))
				{
					businessObject.CompanyLogo = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.CompanyLogo.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Signature.ToString())))
				{
					businessObject.Signature = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.Signature.ToString()));
				}


        }

        /// <summary>
        /// Populate business objects from the data reader
        /// </summary>
        /// <param name="dataReader">data reader</param>
        /// <returns>list of wsEmail_Setting</returns>
        internal List<wsEmail_Setting> PopulateObjectsFromReader(IDataReader dataReader)
        {

            List<wsEmail_Setting> list = new List<wsEmail_Setting>();

            while (dataReader.Read())
            {
                wsEmail_Setting businessObject = new wsEmail_Setting();
                PopulateBusinessObjectFromReader(businessObject, dataReader);
                list.Add(businessObject);
            }
            return list;

        }

        #endregion

	}
}
