﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using ReminderSystem.Business.Base;
using ReminderSystem.Business.Data;
using ReminderSystem.Business.BusinessLayer;

namespace ReminderSystem.Business.DataLayer
{

    class wsUpgradeSql : DataLayerBase
    {

        #region Constructor

        /// <summary>
        /// Class constructor
        /// </summary>
        public wsUpgradeSql()
        {
            // Nothing for now.
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// update row in the table
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <returns>true for successfully updated</returns>
        public bool Update(wsUpgrade businessObject)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_wsRS_Upgrade_Details]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {
               

                MainConnection.Open();

                IDataReader dataReader = sqlCommand.ExecuteReader();

                return PopulateObjectsFromReader(dataReader);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex);
            }
            finally
            {

                MainConnection.Close();
                sqlCommand.Dispose();
            }











        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Populate business object from data reader
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <param name="dataReader">data reader</param>
        internal void PopulateBusinessObjectFromReader(wsUpgrade businessObject, IDataReader dataReader)
        {

            businessObject.ApplicationVersion = dataReader.GetString(dataReader.GetOrdinal(wsEmail_Setting.wsEmail_SettingFields.EmailPort.ToString()));
            

        }

        /// <summary>
        /// Populate business objects from the data reader
        /// </summary>
        /// <param name="dataReader">data reader</param>
        /// <returns>list of wsEmail_Setting</returns>
        internal List<wsTwilioLog> PopulateObjectsFromReader(IDataReader dataReader)
        {

            List<wsTwilioLog> list = new List<wsTwilioLog>();

            while (dataReader.Read())
            {
                wsTwilioLog businessObject = new wsTwilioLog();
                PopulateBusinessObjectFromReader(businessObject, dataReader);
                list.Add(businessObject);
            }
            return list;

        }

        #endregion

    }

}
