using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using ReminderSystem.Business.Base;
using ReminderSystem.Business.Utility;

namespace ReminderSystem.Business.Data
{
    /// <summary>
    /// Data access layer class for wsTbl_LRS_Reminder
    /// </summary>
    class wsReminderSql : DataLayerBase
    {

        #region Constructor

        /// <summary>
        /// Class constructor
        /// </summary>
        public wsReminderSql()
        {
            // Nothing for now.
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// insert new row in the table
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <returns>true of successfully insert</returns>
        public bool Insert(wsReminder businessObject)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_FinalReminderDetails_Insert]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;
            try
            {
                sqlCommand.Parameters.Add(new SqlParameter("@RID", SqlDbType.Int, 4, ParameterDirection.Output, false, 0, 0, "", DataRowVersion.Proposed, businessObject.RID));
                sqlCommand.Parameters.Add(new SqlParameter("@EventID", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EventID));
                sqlCommand.Parameters.Add(new SqlParameter("@Event", SqlDbType.NVarChar, 2147483647, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Event));
                sqlCommand.Parameters.Add(new SqlParameter("@EventTitle", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EventTitle));
                sqlCommand.Parameters.Add(new SqlParameter("@Phone", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Phone));
                sqlCommand.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Email));
                sqlCommand.Parameters.Add(new SqlParameter("@FromPhoneNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.FromPhoneNo));
                sqlCommand.Parameters.Add(new SqlParameter("@AccountSID", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.AccountSID));
                sqlCommand.Parameters.Add(new SqlParameter("@AuthToken", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.AuthToken));
                sqlCommand.Parameters.Add(new SqlParameter("@EventDatetime", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EventDatetime));
                sqlCommand.Parameters.Add(new SqlParameter("@SMSStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.SMSStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@CallStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.CallStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@ClientKey", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.ClientKey));
                sqlCommand.Parameters.Add(new SqlParameter("@RedirectLink", SqlDbType.NVarChar, 2147483647, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.RedirectLink));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailFrom", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailFrom));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailPassword", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailPassword));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailFromTitle", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailFromTitle));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailPort", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailPort));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailSMTP", SqlDbType.NVarChar, 100, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailSMTP));
                sqlCommand.Parameters.Add(new SqlParameter("@CompanyLOGO", SqlDbType.NVarChar, 2147483647, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.CompanyLOGO));
                sqlCommand.Parameters.Add(new SqlParameter("@IsStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.IsStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@MainEventID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.MainEventID));
                sqlCommand.Parameters.Add(new SqlParameter("@IsTwilioStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.IsTwilioStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@AppointmentDatetime", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.AppointmentDatetime));
                sqlCommand.Parameters.Add(new SqlParameter("@IsConfirmable", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.IsConfirmable));
                sqlCommand.Parameters.Add(new SqlParameter("@TwilioLang", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.TwilioLang));

                MainConnection.Open();


                sqlCommand.ExecuteNonQuery();
                businessObject.RID = (int)sqlCommand.Parameters["@RID"].Value;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex);
            }
            finally
            {
                MainConnection.Close();
                sqlCommand.Dispose();
            }
        }

        /// <summary>
        /// update row in the table
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <returns>true for successfully updated</returns>
        public bool Update(wsReminder businessObject)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_FinalReminderDetails_Update]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {

                sqlCommand.Parameters.Add(new SqlParameter("@RID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.RID));
                sqlCommand.Parameters.Add(new SqlParameter("@EventID", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EventID));
                sqlCommand.Parameters.Add(new SqlParameter("@Event", SqlDbType.NVarChar, 2147483647, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Event));
                sqlCommand.Parameters.Add(new SqlParameter("@EventTitle", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EventTitle));
                sqlCommand.Parameters.Add(new SqlParameter("@Phone", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Phone));
                sqlCommand.Parameters.Add(new SqlParameter("@Email", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.Email));
                sqlCommand.Parameters.Add(new SqlParameter("@FromPhoneNo", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.FromPhoneNo));
                sqlCommand.Parameters.Add(new SqlParameter("@AccountSID", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.AccountSID));
                sqlCommand.Parameters.Add(new SqlParameter("@AuthToken", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.AuthToken));
                sqlCommand.Parameters.Add(new SqlParameter("@EventDatetime", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EventDatetime));
                sqlCommand.Parameters.Add(new SqlParameter("@SMSStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.SMSStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@CallStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.CallStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@ClientKey", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.ClientKey));
                sqlCommand.Parameters.Add(new SqlParameter("@RedirectLink", SqlDbType.NVarChar, 2147483647, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.RedirectLink));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailFrom", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailFrom));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailPassword", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailPassword));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailFromTitle", SqlDbType.NVarChar, 500, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailFromTitle));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailPort", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailPort));
                sqlCommand.Parameters.Add(new SqlParameter("@EmailSMTP", SqlDbType.NVarChar, 100, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.EmailSMTP));
                sqlCommand.Parameters.Add(new SqlParameter("@CompanyLOGO", SqlDbType.NVarChar, 2147483647, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.CompanyLOGO));
                sqlCommand.Parameters.Add(new SqlParameter("@IsStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.IsStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@MainEventID", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.MainEventID));
                sqlCommand.Parameters.Add(new SqlParameter("@IsTwilioStatus", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.IsTwilioStatus));
                sqlCommand.Parameters.Add(new SqlParameter("@AppointmentDatetime", SqlDbType.DateTime, 8, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.AppointmentDatetime));
                sqlCommand.Parameters.Add(new SqlParameter("@IsConfirmable", SqlDbType.Int, 4, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.IsConfirmable));
                sqlCommand.Parameters.Add(new SqlParameter("@TwilioLang", SqlDbType.NVarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, businessObject.TwilioLang));
                MainConnection.Open();

                sqlCommand.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("wsReminder::Update::Error occured.", ex);
            }
            finally
            {
                MainConnection.Close();
                sqlCommand.Dispose();
            }
        }

        /// <summary>
        /// Select all rescords
        /// </summary>
        /// <returns>list of wsReminder</returns>
        public List<wsReminder> SelectAll()
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_FinalReminderDetails_SelectAll]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {

                MainConnection.Open();

                IDataReader dataReader = sqlCommand.ExecuteReader();

                return PopulateObjectsFromReader(dataReader);

            }
            catch (Exception ex)
            {
                throw new Exception("wsReminder::SelectAll::Error occured.", ex);
            }
            finally
            {
                MainConnection.Close();
                sqlCommand.Dispose();
            }

        }

        /// <summary>
        /// Select records by field
        /// </summary>
        /// <param name="fieldName">name of field</param>
        /// <param name="value">value of field</param>
        /// <returns>list of wsReminder</returns>
        public List<wsReminder> SelectByField(string ClientKey, int MainEventID)
        {

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_FinalReminderDetails_SelectByField]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {
                sqlCommand.Parameters.Add(new SqlParameter("@ClientKey", ClientKey));
                sqlCommand.Parameters.Add(new SqlParameter("@MainEventID", MainEventID));

                MainConnection.Open();

                IDataReader dataReader = sqlCommand.ExecuteReader();

                return PopulateObjectsFromReader(dataReader);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex);
            }
            finally
            {

                MainConnection.Close();
                sqlCommand.Dispose();
            }

        }

        /// <summary>
        /// Delete records by field
        /// </summary>
        /// <param name="fieldName">name of field</param>
        /// <param name="value">value of field</param>
        /// <returns>true for successfully deleted</returns>
        public bool DeleteByField(string ClientKey, int MainEventID)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_FinalReminderDetails_DeleteByField]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {
                sqlCommand.Parameters.Add(new SqlParameter("@ClientKey", ClientKey));
                sqlCommand.Parameters.Add(new SqlParameter("@MainEventID", MainEventID));

                MainConnection.Open();

                sqlCommand.ExecuteNonQuery();

                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex);
            }
            finally
            {
                MainConnection.Close();
                sqlCommand.Dispose();
            }

        }

        internal List<wsReminderStatusDetails> getReminderStatus(string ClientKey)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_FinalReminderDetails_Status]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {
                sqlCommand.Parameters.Add(new SqlParameter("@ClientKey", ClientKey));

                MainConnection.Open();

                IDataReader dataReader = sqlCommand.ExecuteReader();

                return PopulateResponseObject(dataReader);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex);
            }
            finally
            {

                MainConnection.Close();
                sqlCommand.Dispose();
            }

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Populate business object from data reader
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <param name="dataReader">data reader</param>
        internal void PopulateBusinessObjectFromReader(wsReminder businessObject, IDataReader dataReader)
        {


            businessObject.RID = dataReader.GetInt32(dataReader.GetOrdinal(wsReminder.wsReminderFields.RID.ToString()));

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EventID.ToString())))
            {
                businessObject.EventID = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.EventID.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.Event.ToString())))
            {
                businessObject.Event = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.Event.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EventTitle.ToString())))
            {
                businessObject.EventTitle = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.EventTitle.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.Phone.ToString())))
            {
                businessObject.Phone = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.Phone.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.Email.ToString())))
            {
                businessObject.Email = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.Email.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.FromPhoneNo.ToString())))
            {
                businessObject.FromPhoneNo = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.FromPhoneNo.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.AccountSID.ToString())))
            {
                businessObject.AccountSID = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.AccountSID.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.AuthToken.ToString())))
            {
                businessObject.AuthToken = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.AuthToken.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EventDatetime.ToString())))
            {
                businessObject.EventDatetime = dataReader.GetDateTime(dataReader.GetOrdinal(wsReminder.wsReminderFields.EventDatetime.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.SMSStatus.ToString())))
            {
                businessObject.SMSStatus = dataReader.GetInt32(dataReader.GetOrdinal(wsReminder.wsReminderFields.SMSStatus.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailStatus.ToString())))
            {
                businessObject.EmailStatus = dataReader.GetInt32(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailStatus.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.CallStatus.ToString())))
            {
                businessObject.CallStatus = dataReader.GetInt32(dataReader.GetOrdinal(wsReminder.wsReminderFields.CallStatus.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.ClientKey.ToString())))
            {
                businessObject.ClientKey = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.ClientKey.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.RedirectLink.ToString())))
            {
                businessObject.RedirectLink = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.RedirectLink.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailFrom.ToString())))
            {
                businessObject.EmailFrom = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailFrom.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailPassword.ToString())))
            {
                businessObject.EmailPassword = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailPassword.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailFromTitle.ToString())))
            {
                businessObject.EmailFromTitle = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailFromTitle.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailPort.ToString())))
            {
                businessObject.EmailPort = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailPort.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailSMTP.ToString())))
            {
                businessObject.EmailSMTP = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.EmailSMTP.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.CompanyLOGO.ToString())))
            {
                businessObject.CompanyLOGO = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.CompanyLOGO.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.IsStatus.ToString())))
            {
                businessObject.IsStatus = dataReader.GetInt32(dataReader.GetOrdinal(wsReminder.wsReminderFields.IsStatus.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.MainEventID.ToString())))
            {
                businessObject.MainEventID = dataReader.GetInt32(dataReader.GetOrdinal(wsReminder.wsReminderFields.MainEventID.ToString()));
            }

            if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.IsTwilioStatus.ToString())))
            {
                businessObject.IsTwilioStatus = dataReader.GetInt32(dataReader.GetOrdinal(wsReminder.wsReminderFields.IsTwilioStatus.ToString()));
            }


        }

        /// <summary>
        /// Populate business objects from the data reader
        /// </summary>
        /// <param name="dataReader">data reader</param>
        /// <returns>list of wsReminder</returns>
        internal List<wsReminder> PopulateObjectsFromReader(IDataReader dataReader)
        {

            List<wsReminder> list = new List<wsReminder>();

            while (dataReader.Read())
            {
                wsReminder businessObject = new wsReminder();
                PopulateBusinessObjectFromReader(businessObject, dataReader);
                list.Add(businessObject);
            }
            return list;

        }


        internal List<wsReminderStatusDetails> PopulateResponseObject(IDataReader dataReader)
        {
            List<wsReminderStatusDetails> objList = new List<wsReminderStatusDetails>();

            while (dataReader.Read())
            {
                wsReminderStatusDetails businessObject = new wsReminderStatusDetails();

                if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.EventID.ToString())))
                {
                    businessObject.EventID = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.EventID.ToString()));
                }

                if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.IsTwilioStatus.ToString())))
                {
                    businessObject.IsTwilioStatus = dataReader.GetInt32(dataReader.GetOrdinal(wsReminder.wsReminderFields.IsTwilioStatus.ToString()));
                }

                if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.ClientKey.ToString())))
                {
                    businessObject.ClientKey = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.ClientKey.ToString()));
                }

                if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.MainEventID.ToString())))
                {
                    businessObject.MainEventID = dataReader.GetInt32(dataReader.GetOrdinal(wsReminder.wsReminderFields.MainEventID.ToString()));
                }

                if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.SID.ToString())))
                {
                    businessObject.SID = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.SID.ToString()));
                }

                if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.ConfirmDatetime.ToString())))
                {
                    businessObject.ConfirmDatetime = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.ConfirmDatetime.ToString()));
                }

                if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsReminder.wsReminderFields.Confirmtime.ToString())))
                {
                    businessObject.Confirmtime = dataReader.GetString(dataReader.GetOrdinal(wsReminder.wsReminderFields.Confirmtime.ToString()));
                }


                objList.Add(businessObject);
            }

            return objList;
        }

        #endregion



    }
}
