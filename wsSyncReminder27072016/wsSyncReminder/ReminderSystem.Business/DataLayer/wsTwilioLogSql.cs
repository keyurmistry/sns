using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Collections.Generic;
using ReminderSystem.Business.Base;

namespace ReminderSystem.Business.Data
{
	/// <summary>
	/// Data access layer class for wsTwilioLog
	/// </summary>
	class wsTwilioLogSql : DataLayerBase 
	{

        #region Constructor

		/// <summary>
		/// Class constructor
		/// </summary>
		public wsTwilioLogSql()
		{
			// Nothing for now.
		}

        #endregion

        #region Public Methods

        
        /// <summary>
        /// Select records by field
        /// </summary>
        /// <param name="fieldName">name of field</param>
        /// <param name="value">value of field</param>
        /// <returns>list of wsTwilioLog</returns>
        public List<wsTwilioLog> SelectByClientKey(string ClientKey)
        {

            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = "dbo.[sproc_wsTwilioLog_SelectByField]";
            sqlCommand.CommandType = CommandType.StoredProcedure;

            // Use connection object of base class
            sqlCommand.Connection = MainConnection;

            try
            {
                sqlCommand.Parameters.Add(new SqlParameter("@ClientKey", ClientKey));

                MainConnection.Open();
                
                IDataReader dataReader = sqlCommand.ExecuteReader();

                return PopulateObjectsFromReader(dataReader);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString(), ex);
            }
            finally
            {

                MainConnection.Close();
                sqlCommand.Dispose();
            }

        }
       
        #endregion

        #region Private Methods

        /// <summary>
        /// Populate business object from data reader
        /// </summary>
        /// <param name="businessObject">business object</param>
        /// <param name="dataReader">data reader</param>
        internal void PopulateBusinessObjectFromReader(wsTwilioLog businessObject, IDataReader dataReader)
        {


				businessObject.LID = dataReader.GetInt32(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.LID.ToString()));

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.FromNumber.ToString())))
				{
					businessObject.FromNumber = dataReader.GetString(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.FromNumber.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.ToNumber.ToString())))
				{
					businessObject.ToNumber = dataReader.GetString(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.ToNumber.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.Direction.ToString())))
				{
					businessObject.Direction = dataReader.GetString(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.Direction.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.Duration.ToString())))
				{
					businessObject.Duration = dataReader.GetString(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.Duration.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.StartDate.ToString())))
				{
					businessObject.StartDate = dataReader.GetDateTime(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.StartDate.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.EndDate.ToString())))
				{
					businessObject.EndDate = dataReader.GetDateTime(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.EndDate.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.Status.ToString())))
				{
					businessObject.Status = dataReader.GetString(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.Status.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.CreatedDttm.ToString())))
				{
					businessObject.CreatedDttm = dataReader.GetDateTime(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.CreatedDttm.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.MdifiedDttm.ToString())))
				{
					businessObject.MdifiedDttm = dataReader.GetDateTime(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.MdifiedDttm.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.SID.ToString())))
				{
					businessObject.SID = dataReader.GetString(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.SID.ToString()));
				}

				if (!dataReader.IsDBNull(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.LogType.ToString())))
				{
					businessObject.LogType = dataReader.GetString(dataReader.GetOrdinal(wsTwilioLog.wsTwilioLogFields.LogType.ToString()));
				}


        }

        /// <summary>
        /// Populate business objects from the data reader
        /// </summary>
        /// <param name="dataReader">data reader</param>
        /// <returns>list of wsTwilioLog</returns>
        internal List<wsTwilioLog> PopulateObjectsFromReader(IDataReader dataReader)
        {

            List<wsTwilioLog> list = new List<wsTwilioLog>();

            while (dataReader.Read())
            {
                wsTwilioLog businessObject = new wsTwilioLog();
                PopulateBusinessObjectFromReader(businessObject, dataReader);
                list.Add(businessObject);
            }
            return list;

        }

        #endregion

	}
}
