using System;
using System.Collections.Generic;
using System.Text;
namespace ReminderSystem.Business.Base
{
    public class wsRegistration
	{

        public int RID { get; set; }
        public string RSClientID { get; set; }
        public string CompanyName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string ProductKey { get; set; }
        public string MacID { get; set; }
        public string ISActivated { get; set; }
        public string LogoImage { get; set; }
        public DateTime CreatedDtTm { get; set; }
        public DateTime ModifiedDtTm { get; set; }
	}

    public class wsRegistrationResponse
    {

        private bool _success;

        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

    }
}
