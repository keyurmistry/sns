using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ReminderSystem.Business.Data;

namespace ReminderSystem.Business.Base
{
    public class wsLRS_NotificationSettingFactory
    {

        #region data Members

        wsLRS_NotificationSettingSql _dataObject = null;

        #endregion

        #region Constructor

        public wsLRS_NotificationSettingFactory()
        {
            _dataObject = new wsLRS_NotificationSettingSql();
        }

        #endregion


        #region Public Methods

       
        /// <summary>
        /// Update existing wsLRS_NotificationSetting
        /// </summary>
        /// <param name="businessObject">wsLRS_NotificationSetting object</param>
        /// <returns>true for successfully saved</returns>
        public bool Update(wsLRS_NotificationSetting businessObject)
        {
            return _dataObject.Update(businessObject);
        }

        
        #endregion

    }
}
