﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ReminderSystem.Business.Data;
using ReminderSystem.Business.DataLayer;

namespace ReminderSystem.Business.BusinessLayer
{



    public class wsUpgradeFactory
    {

        #region data Members

        wsUpgradeSql _dataObject = null;

        #endregion

        #region Constructor

        public wsUpgradeFactory()
        {
            _dataObject = new wsUpgradeSql();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update existing wsEmail_Setting
        /// </summary>
        /// <param name="businessObject">wsEmail_Setting object</param>
        /// <returns>true for successfully saved</returns>
        public bool Update(wsUpgrade businessObject)
        {
            return _dataObject.Update(businessObject);
        }

        #endregion

    }
}
