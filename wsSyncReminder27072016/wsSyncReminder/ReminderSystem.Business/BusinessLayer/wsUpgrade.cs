﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReminderSystem.Business.BusinessLayer
{

    public enum wsUpgrade_SettingFields
    {
        EmailID,
        EmailFrom,
        EmailPassword,
        EmailFromTitle,
        EmailPort,
        EmailSMTP,
        Twilio_CALL,
        Twilio_SMS,
        Twilio_AccountSid,
        Twilio_AuthToken,
        CompanyLogo,
        Signature,
        ClientKey
    }
    public class wsUpgrade
    {
        public int UID { get; set; }
        public string ApplicationVersion { get; set; }
        public string ApplicationPath { get; set; }
        public string isActive { get; set; } 
        public DateTime CreatedDatetime { get; set; }
        public DateTime ModifiedDatetime { get; set; }
    }

    public class wsUpgradeResponse
    {

        private bool _success;

        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

    }
}
