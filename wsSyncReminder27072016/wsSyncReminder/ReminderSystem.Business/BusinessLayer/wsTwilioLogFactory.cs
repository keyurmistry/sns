using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ReminderSystem.Business.Data;

namespace ReminderSystem.Business.Base
{
    public class wsTwilioLogFactory
    {

        #region data Members

        wsTwilioLogSql _dataObject = null;

        #endregion

        #region Constructor

        public wsTwilioLogFactory()
        {
            _dataObject = new wsTwilioLogSql();
        }

        #endregion


        #region Public Methods

       
        /// <summary>
        /// get list of Tbl_TwilioLog by field
        /// </summary>
        /// <param name="fieldName">field name</param>
        /// <param name="value">value</param>
        /// <returns>list</returns>
        public List<wsTwilioLog> GetAllByClientKey(string clientKey)
        {
            return _dataObject.SelectByClientKey(clientKey);  
        }

       
        #endregion

    }
}
