using System;
using System.Collections.Generic;
using System.Text;
namespace ReminderSystem.Business.Base
{
	public class wsLRS_NotificationSetting
	{

		#region InnerClass
		public enum wsLRS_NotificationSettingFields
		{
			NotificationID,
			EmailSend,
			EmailCancel,
			EmailReschedule,
			EmailWhen,
			SMSSend,
			SMSCancel,
			SMSReschedule,
			SMSWhen,
			Isactive,
			CreatedDatetime,
			ModifiedDatetime,
            ClientKey
		}
		#endregion

		#region Data Members

			int _notificationID;
			string _emailSend;
			int? _emailCancel;
			int? _emailReschedule;
			string _emailWhen;
			string _sMSSend;
			int? _sMSCancel;
			int? _sMSReschedule;
			string _sMSWhen;
			int? _isactive;
			DateTime? _createdDatetime;
			DateTime? _modifiedDatetime;
            string _clientKey;

		#endregion

		#region Properties

		public int  NotificationID
		{
			 get { return _notificationID; }
			 set
			 {
				 if (_notificationID != value)
				 {
					_notificationID = value;
				 }
			 }
		}

		public string  EmailSend
		{
			 get { return _emailSend; }
			 set
			 {
				 if (_emailSend != value)
				 {
					_emailSend = value;
				 }
			 }
		}

		public int?  EmailCancel
		{
			 get { return _emailCancel; }
			 set
			 {
				 if (_emailCancel != value)
				 {
					_emailCancel = value;
				 }
			 }
		}

		public int?  EmailReschedule
		{
			 get { return _emailReschedule; }
			 set
			 {
				 if (_emailReschedule != value)
				 {
					_emailReschedule = value;
				 }
			 }
		}

		public string  EmailWhen
		{
			 get { return _emailWhen; }
			 set
			 {
				 if (_emailWhen != value)
				 {
					_emailWhen = value;
				 }
			 }
		}

		public string  SMSSend
		{
			 get { return _sMSSend; }
			 set
			 {
				 if (_sMSSend != value)
				 {
					_sMSSend = value;
				 }
			 }
		}

		public int?  SMSCancel
		{
			 get { return _sMSCancel; }
			 set
			 {
				 if (_sMSCancel != value)
				 {
					_sMSCancel = value;
				 }
			 }
		}

		public int?  SMSReschedule
		{
			 get { return _sMSReschedule; }
			 set
			 {
				 if (_sMSReschedule != value)
				 {
					_sMSReschedule = value;
				 }
			 }
		}

		public string  SMSWhen
		{
			 get { return _sMSWhen; }
			 set
			 {
				 if (_sMSWhen != value)
				 {
					_sMSWhen = value;
				 }
			 }
		}

		public int?  Isactive
		{
			 get { return _isactive; }
			 set
			 {
				 if (_isactive != value)
				 {
					_isactive = value;
				 }
			 }
		}

		public DateTime?  CreatedDatetime
		{
			 get { return _createdDatetime; }
			 set
			 {
				 if (_createdDatetime != value)
				 {
					_createdDatetime = value;
				 }
			 }
		}

		public DateTime?  ModifiedDatetime
		{
			 get { return _modifiedDatetime; }
			 set
			 {
				 if (_modifiedDatetime != value)
				 {
					_modifiedDatetime = value;
				 }
			 }
		}

         public string ClientKey
        {
            get { return _clientKey; }
            set { _clientKey = value; }
        }

		#endregion

	}

    public class wsLRS_NotificationSettingResponse
    {

        public wsLRS_NotificationSettingResponse() { }

        private bool _success;

        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

    }
}
