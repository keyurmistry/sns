using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ReminderSystem.Business.Data;

namespace ReminderSystem.Business.Base
{
    public class wsEmail_SettingFactory
    {

        #region data Members

        wsEmail_SettingSql _dataObject = null;

        #endregion

        #region Constructor

        public wsEmail_SettingFactory()
        {
            _dataObject = new wsEmail_SettingSql();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update existing wsEmail_Setting
        /// </summary>
        /// <param name="businessObject">wsEmail_Setting object</param>
        /// <returns>true for successfully saved</returns>
        public bool Update(wsEmail_Setting businessObject)
        {
            return _dataObject.Update(businessObject);
        }

        public bool UpdateCompanyLogo(wsEmail_Setting businessObject)
        {
            return _dataObject.UpdateCompanyLogo(businessObject);
        }

        #endregion

    }
}
