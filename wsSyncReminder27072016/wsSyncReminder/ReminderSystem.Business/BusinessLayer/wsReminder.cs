using System;
using System.Collections.Generic;
using System.Text;
using ReminderSystem.Business;
namespace ReminderSystem.Business.Base
{
	#region Business Base Class
	/// <summary>
	/// 
	/// </summary>
	public class wsReminder
	{

        #region InnerClass
        public enum wsReminderFields
        {
            RID,
            EventID,
            Event,
            EventTitle,
            Phone,
            Email,
            FromPhoneNo,
            AccountSID,
            AuthToken,
            EventDatetime,
            SMSStatus,
            EmailStatus,
            CallStatus,
            ClientKey,
            RedirectLink,
            EmailFrom,
            EmailPassword,
            EmailFromTitle,
            EmailPort,
            EmailSMTP,
            CompanyLOGO,
            IsStatus,
            MainEventID,
            IsTwilioStatus,
            AppointmentDatetime,
            IsConfirmable,
            SID,
            TwilioLang,
            IsSync,
            ConfirmDatetime,
            Confirmtime
        }
        #endregion

        #region Data Members

        int _rID;
        string _eventID;
        string _event;
        string _eventTitle;
        string _phone;
        string _email;
        string _fromPhoneNo;
        string _accountSID;
        string _authToken;
        DateTime? _eventDatetime;
        int? _sMSStatus;
        int? _emailStatus;
        int? _callStatus;
        string _clientKey;
        string _redirectLink;
        string _emailFrom;
        string _emailPassword;
        string _emailFromTitle;
        string _emailPort;
        string _emailSMTP;
        string _companyLOGO;
        int? _isStatus;
        int _mainEventID;
        int? _isTwilioStatus;
        DateTime? _appointmentDatetime;
        int? _isConfirmable;
        string _twilioLang;
        string _confirmDatetime;
        string _confirmtime;
        
        #endregion

        #region Properties

        public int RID
        {
            get { return _rID; }
            set
            {
                if (_rID != value)
                {
                    _rID = value;
                }
            }
        }

        public string EventID
        {
            get { return _eventID; }
            set
            {
                if (_eventID != value)
                {
                    _eventID = value;
                }
            }
        }

        public string Event
        {
            get { return _event; }
            set
            {
                if (_event != value)
                {
                    _event = value;
                }
            }
        }

        public string EventTitle
        {
            get { return _eventTitle; }
            set
            {
                if (_eventTitle != value)
                {
                    _eventTitle = value;
                }
            }
        }

        public string Phone
        {
            get { return _phone; }
            set
            {
                if (_phone != value)
                {
                    _phone = value;
                }
            }
        }

        public string Email
        {
            get { return _email; }
            set
            {
                if (_email != value)
                {
                    _email = value;
                }
            }
        }

        public string FromPhoneNo
        {
            get { return _fromPhoneNo; }
            set
            {
                if (_fromPhoneNo != value)
                {
                    _fromPhoneNo = value;
                }
            }
        }

        public string AccountSID
        {
            get { return _accountSID; }
            set
            {
                if (_accountSID != value)
                {
                    _accountSID = value;
                }
            }
        }

        public string AuthToken
        {
            get { return _authToken; }
            set
            {
                if (_authToken != value)
                {
                    _authToken = value;
                }
            }
        }

        public DateTime? EventDatetime
        {
            get { return _eventDatetime; }
            set
            {
                if (_eventDatetime != value)
                {
                    _eventDatetime = value;
                }
            }
        }

        public int? SMSStatus
        {
            get { return _sMSStatus; }
            set
            {
                if (_sMSStatus != value)
                {
                    _sMSStatus = value;
                }
            }
        }

        public int? EmailStatus
        {
            get { return _emailStatus; }
            set
            {
                if (_emailStatus != value)
                {
                    _emailStatus = value;
                }
            }
        }

        public int? CallStatus
        {
            get { return _callStatus; }
            set
            {
                if (_callStatus != value)
                {
                    _callStatus = value;
                }
            }
        }

        public string ClientKey
        {
            get { return _clientKey; }
            set
            {
                if (_clientKey != value)
                {
                    _clientKey = value;
                }
            }
        }

        public string RedirectLink
        {
            get { return _redirectLink; }
            set
            {
                if (_redirectLink != value)
                {
                    _redirectLink = value;
                }
            }
        }

        public string EmailFrom
        {
            get { return _emailFrom; }
            set
            {
                if (_emailFrom != value)
                {
                    _emailFrom = value;
                }
            }
        }

        public string EmailPassword
        {
            get { return _emailPassword; }
            set
            {
                if (_emailPassword != value)
                {
                    _emailPassword = value;
                }
            }
        }

        public string EmailFromTitle
        {
            get { return _emailFromTitle; }
            set
            {
                if (_emailFromTitle != value)
                {
                    _emailFromTitle = value;
                }
            }
        }

        public string EmailPort
        {
            get { return _emailPort; }
            set
            {
                if (_emailPort != value)
                {
                    _emailPort = value;
                }
            }
        }

        public string EmailSMTP
        {
            get { return _emailSMTP; }
            set
            {
                if (_emailSMTP != value)
                {
                    _emailSMTP = value;
                }
            }
        }

        public string CompanyLOGO
        {
            get { return _companyLOGO; }
            set
            {
                if (_companyLOGO != value)
                {
                    _companyLOGO = value;
                }
            }
        }

        public int? IsStatus
        {
            get { return _isStatus; }
            set
            {
                if (_isStatus != value)
                {
                    _isStatus = value;
                }
            }
        }

        public int MainEventID
        {
            get { return _mainEventID; }
            set
            {
                if (_mainEventID != value)
                {
                    _mainEventID = value;
                }
            }
        }

        public int? IsTwilioStatus
        {
            get { return _isTwilioStatus; }
            set
            {
                if (_isTwilioStatus != value)
                {
                    _isTwilioStatus = value;
                }
            }
        }

        public DateTime? AppointmentDatetime
        {
            get { return _appointmentDatetime; }
            set
            {
                if (_appointmentDatetime != value)
                {
                    _appointmentDatetime = value;
                }
            }
        }

        public int? IsConfirmable
        {
            get { return _isConfirmable; }
            set
            {
                if (_isConfirmable != value)
                {
                    _isConfirmable = value;
                }
            }
        }


        public string TwilioLang
        {
            get { return _twilioLang; }
            set
            {
                if (_twilioLang != value)
                {
                    _twilioLang = value;
                }
            }
        }




        public string ConfirmDatetime
        {
            get { return _confirmDatetime; }
            set
            {
                if (_confirmDatetime != value)
                {
                    _confirmDatetime = value;
                }
            }
        }

        public string Confirmtime
        {
            get { return _confirmtime; }
            set
            {
                if (_confirmtime != value)
                {
                    _confirmtime = value;
                }
            }
        }

        #endregion
        
	}

    public class wsReminderResonseDetail
    {
        public int ApplicationEventID { get; set; }
        public int CloudeEventID { get; set; }
        public int EventID { get; set; }
        public string ClientKey { get; set; }
    }

    public class wsReminderResponse
    {

        public wsReminderResponse()
        {
            ResponseDetail = new List<wsReminderResonseDetail>();
        }

        public List<wsReminderResonseDetail> ResponseDetail { get; set; }

        private bool _success;

        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

    }

    public class wsReminderStatusDetails
    {
        public string ClientKey { get; set; }
        public int MainEventID { get; set; }
        public string EventID { get; set; }
        public int IsTwilioStatus { get; set; }
        public string SID { get; set; }
        public string ConfirmDatetime { get; set; }
        public string Confirmtime { get; set; }
      
    }

    public class wsReminderStatusResponse
    {
        public wsReminderStatusResponse()
        {
            ResponseDetail = new List<wsReminderStatusDetails>();
        }

        public List<wsReminderStatusDetails> ResponseDetail { get; set; }

        private bool _success;

        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
    }

    #endregion Business Base Class

}
