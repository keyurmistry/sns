using System;
using System.Collections.Generic;
using System.Text;
namespace ReminderSystem.Business.Base
{
	public class wsEmail_Setting
	{

		#region InnerClass
		public enum wsEmail_SettingFields
		{
			EmailID,
			EmailFrom,
			EmailPassword,
			EmailFromTitle,
			EmailPort,
			EmailSMTP,
			Twilio_CALL,
			Twilio_SMS,
			Twilio_AccountSid,
			Twilio_AuthToken,
			CompanyLogo,
			Signature,
            ClientKey
		}
		#endregion

		#region Data Members

			int _emailID;
			string _emailFrom;
			string _emailPassword;
			string _emailFromTitle;
			string _emailPort;
			string _emailSMTP;
			string _twilio_CALL;
			string _twilio_SMS;
			string _twilio_AccountSid;
			string _twilio_AuthToken;
			string _companyLogo;
			string _signature;
            string _clientKey;

		#endregion

		#region Properties

		public int  EmailID
		{
			 get { return _emailID; }
			 set
			 {
				 if (_emailID != value)
				 {
					_emailID = value;
				 }
			 }
		}

		public string  EmailFrom
		{
			 get { return _emailFrom; }
			 set
			 {
				 if (_emailFrom != value)
				 {
					_emailFrom = value;
				 }
			 }
		}

		public string  EmailPassword
		{
			 get { return _emailPassword; }
			 set
			 {
				 if (_emailPassword != value)
				 {
					_emailPassword = value;
				 }
			 }
		}

		public string  EmailFromTitle
		{
			 get { return _emailFromTitle; }
			 set
			 {
				 if (_emailFromTitle != value)
				 {
					_emailFromTitle = value;
				 }
			 }
		}

		public string  EmailPort
		{
			 get { return _emailPort; }
			 set
			 {
				 if (_emailPort != value)
				 {
					_emailPort = value;
				 }
			 }
		}

		public string  EmailSMTP
		{
			 get { return _emailSMTP; }
			 set
			 {
				 if (_emailSMTP != value)
				 {
					_emailSMTP = value;
				 }
			 }
		}

		public string  Twilio_CALL
		{
			 get { return _twilio_CALL; }
			 set
			 {
				 if (_twilio_CALL != value)
				 {
					_twilio_CALL = value;
				 }
			 }
		}

		public string  Twilio_SMS
		{
			 get { return _twilio_SMS; }
			 set
			 {
				 if (_twilio_SMS != value)
				 {
					_twilio_SMS = value;
				 }
			 }
		}

		public string  Twilio_AccountSid
		{
			 get { return _twilio_AccountSid; }
			 set
			 {
				 if (_twilio_AccountSid != value)
				 {
					_twilio_AccountSid = value;
				 }
			 }
		}

		public string  Twilio_AuthToken
		{
			 get { return _twilio_AuthToken; }
			 set
			 {
				 if (_twilio_AuthToken != value)
				 {
					_twilio_AuthToken = value;
				 }
			 }
		}

		public string  CompanyLogo
		{
			 get { return _companyLogo; }
			 set
			 {
				 if (_companyLogo != value)
				 {
					_companyLogo = value;
				 }
			 }
		}

		public string  Signature
		{
			 get { return _signature; }
			 set
			 {
				 if (_signature != value)
				 {
					_signature = value;
				 }
			 }
		}

        public string ClientKey
        {
            get { return _clientKey; }
            set { _clientKey = value; }
        }
        

		#endregion

	}

    public class wsEmail_SettingResponse
    {

        public wsEmail_SettingResponse(){ }

        private bool _success;

        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

    }
}
