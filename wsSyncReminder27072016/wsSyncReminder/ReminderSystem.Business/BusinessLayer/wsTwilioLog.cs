using System;
using System.Collections.Generic;
using System.Text;
namespace ReminderSystem.Business.Base
{
	public class wsTwilioLog
	{

		#region InnerClass
		public enum wsTwilioLogFields
		{
			LID,
			FromNumber,
			ToNumber,
			Direction,
			Duration,
			StartDate,
			EndDate,
			Status,
			CreatedDttm,
			MdifiedDttm,
			SID,
			LogType
		}
		#endregion

		#region Data Members

			int _lID;
			string _fromNumber;
			string _toNumber;
			string _direction;
			string _duration;
			DateTime? _startDate;
			DateTime? _endDate;
			string _status;
			DateTime? _createdDttm;
			DateTime? _mdifiedDttm;
			string _sID;
			string _logType;

		#endregion

		#region Properties

		public int  LID
		{
			 get { return _lID; }
			 set
			 {
				 if (_lID != value)
				 {
					_lID = value;
				 }
			 }
		}

		public string  FromNumber
		{
			 get { return _fromNumber; }
			 set
			 {
				 if (_fromNumber != value)
				 {
					_fromNumber = value;
				 }
			 }
		}

		public string  ToNumber
		{
			 get { return _toNumber; }
			 set
			 {
				 if (_toNumber != value)
				 {
					_toNumber = value;
				 }
			 }
		}

		public string  Direction
		{
			 get { return _direction; }
			 set
			 {
				 if (_direction != value)
				 {
					_direction = value;
				 }
			 }
		}

		public string  Duration
		{
			 get { return _duration; }
			 set
			 {
				 if (_duration != value)
				 {
					_duration = value;
				 }
			 }
		}

		public DateTime?  StartDate
		{
			 get { return _startDate; }
			 set
			 {
				 if (_startDate != value)
				 {
					_startDate = value;
				 }
			 }
		}

		public DateTime?  EndDate
		{
			 get { return _endDate; }
			 set
			 {
				 if (_endDate != value)
				 {
					_endDate = value;
				 }
			 }
		}

		public string  Status
		{
			 get { return _status; }
			 set
			 {
				 if (_status != value)
				 {
					_status = value;
				 }
			 }
		}

		public DateTime?  CreatedDttm
		{
			 get { return _createdDttm; }
			 set
			 {
				 if (_createdDttm != value)
				 {
					_createdDttm = value;
				 }
			 }
		}

		public DateTime?  MdifiedDttm
		{
			 get { return _mdifiedDttm; }
			 set
			 {
				 if (_mdifiedDttm != value)
				 {
					_mdifiedDttm = value;
				 }
			 }
		}

		public string  SID
		{
			 get { return _sID; }
			 set
			 {
				 if (_sID != value)
				 {
					_sID = value;
				 }
			 }
		}

		public string  LogType
		{
			 get { return _logType; }
			 set
			 {
				 if (_logType != value)
				 {
					_logType = value;
				 }
			 }
		}


		#endregion


	}

    public class wsTwilioLogResponse
    {

        public wsTwilioLogResponse()
        {
            ResponseDetail = new List<wsTwilioLog>();
        }

        public List<wsTwilioLog> ResponseDetail { get; set; }

        private bool _success;

        public bool Success
        {
            get { return _success; }
            set { _success = value; }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

    }
}
