using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ReminderSystem.Business.Data;

namespace ReminderSystem.Business.Base
{
    public class wsRegistrationFactory
    {

        #region data Members

        wsRegistrationSql _dataObject = null;

        #endregion

        #region Constructor

        public wsRegistrationFactory()
        {
            _dataObject = new wsRegistrationSql();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update existing wsEmail_Setting
        /// </summary>
        /// <param name="businessObject">wsEmail_Setting object</param>
        /// <returns>true for successfully saved</returns>
        public bool Update(wsRegistration businessObject)
        {
            return _dataObject.Update(businessObject);
        }

        #endregion

    }
}
