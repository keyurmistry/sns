using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ReminderSystem.Business.Data;
using ReminderSystem.Business.Utility;

namespace ReminderSystem.Business.Base
{
    public class wsReminderFactory
    {

        #region data Members

        wsReminderSql _dataObject = null;

        #endregion

        #region Constructor

        public wsReminderFactory()
        {
            _dataObject = new wsReminderSql();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Insert new wsReminder
        /// </summary>
        /// <param name="businessObject">wsReminder object</param>
        /// <returns>true for successfully saved</returns>
        public bool Insert(wsReminder businessObject)
        {

            return _dataObject.Insert(businessObject);

        }

        /// <summary>
        /// Update existing wsReminder
        /// </summary>
        /// <param name="businessObject">wsReminder object</param>
        /// <returns>true for successfully saved</returns>
        public bool Update(wsReminder businessObject)
        {

            return _dataObject.Update(businessObject);
        }

        /// <summary>
        /// get list of all wsReminders
        /// </summary>
        /// <returns>list</returns>
        public List<wsReminder> GetAll()
        {
            return _dataObject.SelectAll();
        }

        /// <summary>
        /// get list of wsReminder by field
        /// </summary>
        /// <param name="fieldName">field name</param>
        /// <param name="value">value</param>
        /// <returns>list</returns>
        public List<wsReminder> GetAllBy(string ClientKey,int MainEventID)
        {
            return _dataObject.SelectByField(ClientKey,MainEventID);
        }

        public List<wsReminderStatusDetails> getReminderStatus(string ClientKey)
        {
            return _dataObject.getReminderStatus(ClientKey);
        }

        /// <summary>
        /// delete by primary key
        /// </summary>
        /// <param name="keys">primary key</param>
        /// <returns>true for succesfully deleted</returns>

        /// <summary>
        /// delete wsReminder by field.
        /// </summary>
        /// <param name="fieldName">field name</param>
        /// <param name="value">value</param>
        /// <returns>true for successfully deleted</returns>
        public bool Delete(string ClientKey, int MainEventID)
        {
            return _dataObject.DeleteByField(ClientKey, MainEventID);
        }

        #endregion



    }
}
