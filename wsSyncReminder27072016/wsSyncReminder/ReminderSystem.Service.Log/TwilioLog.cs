﻿using System;
using System.ServiceProcess;

namespace ReminderSystem.Service.TwilioLog
{
    public class TwilioLog : ServiceBase
    {
        public TwilioLog()
        {
            this.ServiceName = "ReminderSystem.Service.TwilioLog";
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;
        }

        private void InitializeComponent()
        {
            // 
            // TwilioLog
            // 
            this.ServiceName = "ReminderSystem.Service.TwilioLog";

        }

        protected override void OnStart(string[] args)
        {
            // TODO: add startup stuff
            System.IO.File.Create("myservice" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt");
        }

        protected override void OnStop()
        {
            // TODO: add shutdown stuff
        }
    }
}
