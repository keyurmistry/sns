﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ReminderSystem.Service.Log")]
[assembly: AssemblyDescription("Developed By Maulik A. Dusara")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mystic Business Solution Pvt Ltd")]
[assembly: AssemblyProduct("ReminderSystem.Service.Log")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("Mystic Business Solution Pvt Ltd")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("901e5db5-b712-41dd-9cd1-bc7a187c7571")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2016.07.27.01")]
[assembly: AssemblyFileVersion("2016.07.27.01")]
