﻿using System;
using System.ServiceProcess;

namespace ReminderSystem.Service.TwilioLog
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            System.ServiceProcess.ServiceBase.Run(new TwilioLog());
        }
    }
}